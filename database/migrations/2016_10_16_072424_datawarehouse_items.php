<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DatawarehouseItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('datawarehouse_items', function (Blueprint $table) {

            $table->increments('id');
            $table->decimal('quantityinstock');
            $table->integer('item_id')->unsigned();
            $table->integer('datawarehouse_id')->unsigned();
           $table->foreign('item_id')
              ->references('id')->on('items')
              ->onDelete('cascade')
              ->onupdate('cascade');
            $table->foreign('datawarehouse_id')
              ->references('id')->on('datawarehouses')
              ->onDelete('cascade')
              ->onupdate('cascade');
              $table->timestamps();

                    

        });
            }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('datawarehouse_items');
    }
}
