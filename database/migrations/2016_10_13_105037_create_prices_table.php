<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prices', function (Blueprint $table) {
                $table->increments('id')->unsigned();
                $table->date('Date');
                $table->float('Price')->nullable();
                $table->string('Government', 150);
                $table->string('District', 150);
         

          $table->integer('Product_id')->unsigned();

          $table->foreign('product_id')
              ->references('id')->on('products')
              ->onDelete('cascade')
              ->onupdate('cascade');
    $table->integer('User_id')->unsigned();
             $table->foreign('User_id')
                ->references('id')
                ->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');


            $table->unique(array('Government', 'District', 'product_id','Price','Date'));

             $table->timestamps();

            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('prices');
    }
}
