<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activities', function (Blueprint $table) {
            $table->increments('id')->unsigned();
             $table->string('Government', 150)->nullable();
             $table->string('District', 150)->nullable();
             $table->date('Start_date');
             $table->string('Duration',150);
              $table->string('Activity_type', 150);
              $table->string('Description', 150)->nullable();
              $table->binary('Img')->nullable();
             
          $table->integer('company_id')->unsigned();
             $table->foreign('company_id')
            ->references('id')->on('companies')
            ->onDelete('cascade')
            ->onupdate('cascade');
             $table->integer('User_id')->unsigned()->nullable();
             $table->foreign('User_id')
                ->references('id')
                ->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');










         $table->unique(array('Government','District','Start_date','Activity_type'));
            $table->timestamps();
             $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('activities');
    }
}
