<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
          $table->increments('id')->unsigned();
          $table->string('Name',150)->unique();
          $table->string('EnName',150)->unique();

          $table->string('Government', 150)->nullable();
          $table->string('District', 150)->nullable();
          $table->string('Address',150)->nullable();
          $table->string('Head Office Gov',150);
          $table->string('Head Office Addre',150);
          $table->integer('Production Lines');
          $table->integer('Monthly Capacity')->nullable();
          $table->softDeletes();
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('companies');
    }
}
