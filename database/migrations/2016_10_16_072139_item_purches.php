<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ItemPurches extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('item_purches', function (Blueprint $table) {

            $table->increments('id');
            $table->decimal('quantity');
            $table->integer('purches_id')->unsigned();
            $table->integer('item_id')->unsigned();
            $table->foreign('purches_id')
            ->references('id')->on('purches')
            ->onDelete('cascade')
            ->onupdate('cascade');
           $table->foreign('item_id')
              ->references('id')->on('items')
              ->onDelete('cascade')
              ->onupdate('cascade');
         
              $table->timestamps();

                    

        });    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
                Schema::drop('item_purches');
    }
}
