<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReviewretailesTable extends Migration
{
     public function up()
    {
        Schema::create('reviews_retailers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('Has_Truncks')->nullable();
            $table->string('Workers')->nullable();            
            $table->string('No_Truncks')->nullable();
            $table->string('Has_Used_Computer')->nullable();
            $table->string('Equipment')->nullable();
            $table->string('Status',50)->nullable();
            $table->string('Call_Status',50)->nullable();
            $table->string('Area',50)->nullable();
            $table->string('Cont_Type',50)->nullable();
            $table->timestamps();          
            $table->integer('Retailer_Id')->unsigned();
            $table->foreign('Retailer_Id')->references('id')->on('retailers')->onDelete('cascade')->onUpdate('cascade');

             });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reviews_retailers');
    }
}
