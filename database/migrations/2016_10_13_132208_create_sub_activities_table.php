<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sub_activities', function (Blueprint $table) {
            $table->increments('id');
            $table->string('Name',250)->unique();
            $table->string('EngName',250)->unique();

            $table->integer('marketingactivity_id')->unsigned();
            $table->foreign('marketingactivity_id')
              ->references('id')->on('marketing_activities')
              ->onDelete('cascade')
              ->onupdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sub_activities');
    }
}
