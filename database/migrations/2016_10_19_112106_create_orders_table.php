<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');

             $table->integer('SaleProduct_id')->unsigned()->nullable();
             $table->integer('Product_id')->unsigned()->nullable();
             $table->integer('Amount')->unsigned();
             $table->string('Type', 150);
             $table->tinyInteger('IsDone');
             $table->string('Order_Status',50);
             $table->string('Notes',250);
             $table->foreign('SaleProduct_id')
                  ->references('id')->on('sale_products')
                  ->onDelete('cascade')
                  ->onupdate('cascade');
                   
              $table->foreign('Product_id')
                  ->references('id')->on('products')
                  ->onDelete('cascade')
                  ->onupdate('cascade');

             $table->integer('gps_id')->unsigned();
          
            $table->foreign('gps_id')
                  ->references('id')->on('gps')
                  ->onDelete('cascade')
                  ->onupdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('orders');
    }
}
