<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFrontOfficesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('front_offices', function (Blueprint $table) {
            $table->increments('id')->unsigned();
                $table->string('Location',255)->nullable();
                $table->integer('Telephone')->unique();
                $table->string('Government', 150)->nullable();
                $table->string('District', 150)->nullable();
                $table->string('Address',150)->nullable();
                $table->integer('company_id')->unsigned();
                $table->foreign('company_id')
                ->references('id')->on('companies')
                ->onDelete('cascade')
                ->onupdate('cascade');
                $table->timestamps();
                $table->softDeletes();

    
          
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('front_offices');
    }
}
