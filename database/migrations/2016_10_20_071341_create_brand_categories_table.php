<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBrandCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brand_categories', function (Blueprint $table) {
            $table->increments('id');
               $table->integer('category_id')->unsigned();
              $table->foreign('category_id')
            ->references('id')->on('categories')
            ->onDelete('cascade')
            ->onupdate('cascade');

            $table->integer('brand_id')->unsigned();
            $table->foreign('brand_id')
            ->references('id')->on('brands')
            ->onDelete('cascade')
            ->onupdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('brand_categories');
    }
}
