<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UsersTasks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_tasks', function (Blueprint $table) {

                        $table->increments('id');

               $table->integer('users_id')->unsigned();
               $table->integer('task_id')->unsigned();


                $table->integer('Target');
                $table->integer('Achieved');
                $table->date('Date');
                $table->time('Time');

                $table->string('Status',300);


                    $table->foreign('users_id')
                  ->references('id')->on('users')
                  ->onDelete('cascade')
                  ->onupdate('cascade');

                  

                   $table->foreign('task_id')
                  ->references('id')->on('tasks')
                  ->onDelete('cascade')
                  ->onupdate('cascade');

                   
                   $table->timestamps();

                    
          
              });    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
                Schema::drop('users_tasks');
    }
}
