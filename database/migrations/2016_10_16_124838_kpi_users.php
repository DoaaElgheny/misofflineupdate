<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class KpiUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('kpi_users', function (Blueprint $table) {

            $table->increments('id');
            $table->decimal('Performance');
            $table->date('From_Date');
            $table->date('To_Date');
            $table->integer('user_id')->unsigned();
            $table->integer('kpi_id')->unsigned();
         
           $table->foreign('user_id')
              ->references('id')->on('users')
              ->onDelete('cascade')
              ->onupdate('cascade');

            $table->foreign('kpi_id')
              ->references('id')->on('kpis')
              ->onDelete('cascade')
              ->onupdate('cascade');

              $table->timestamps();

                    

        });
            }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('kpi_users');
    }
}
