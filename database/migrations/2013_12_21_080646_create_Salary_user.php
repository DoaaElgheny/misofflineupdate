<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalaryUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('Salary_user', function (Blueprint $table) {

              $table->increments('id');
               $table->integer('DailyWeage');
               $table->date('Date');
                $table->decimal('Performance');
                $table->decimal('BackCheck');
                $table->decimal('FinalSalary');
                $table->integer('users_id')->unsigned();
                  $table->foreign('users_id')
                  ->references('id')->on('users')
                  ->onDelete('cascade')
                  ->onupdate('cascade');
                   $table->timestamps();

                    
          
              }); 
   }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
