<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
             $table->increments('id')->unsigned();
             $table->string('Name', 150);
             $table->string('Enname', 150);
             $table->string('Username', 32)->unique();
             $table->string('Government', 150);
             $table->string('District', 150);
             $table->string('Code', 150)->unique();
             $table->integer('Tele');
             $table->string('Email', 150)->unique();
             $table->string('Password', 100);
             $table->integer('Experince')->nullable();
             $table->date('Start_Date')->nullable();
             $table->integer('Salary')->nullable();
             $table->string('role', 64);
             $table->integer('Supervisor')->unsigned()->nullable();
             $table->foreign('Supervisor')
                ->references('id')
                ->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');

             $table->softDeletes();
             $table->rememberToken();
             $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
