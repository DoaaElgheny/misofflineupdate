<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purches', function (Blueprint $table) {
            $table->increments('id');
           $table->date('Date');
            $table->integer('users_id')->unsigned();
            $table->integer('ActivityitemsID')->unsigned();
            $table->integer('Warehouse_iD')->unsigned();

            $table->string('Type');
            $table->binary('RelatedDecomentLink');
            $table->string('PurchesNo',250);
            $table->foreign('users_id')
            ->references('id')->on('users')
            ->onDelete('cascade')
            ->onupdate('cascade');
            $table->foreign('ActivityitemsID')
            ->references('id')->on('activity_items')
            ->onDelete('cascade')
            ->onupdate('cascade');
        $table->foreign('Warehouse_iD')
            ->references('id')->on('datawarehouses')
            ->onDelete('cascade')
            ->onupdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('purches');
    }
}
