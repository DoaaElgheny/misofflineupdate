<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProductsReviews extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('products_reviews', function (Blueprint $table) {
             $table->increments('id')->unsigned();
              $table->float('Value')->default(0);
              
              $table->integer('product_id')->unsigned();
              $table->foreign('product_id')
            ->references('id')->on('products')
            ->onDelete('cascade')
            ->onupdate('cascade');

              $table->integer('review_id')->unsigned();
             $table->foreign('review_id')
            ->references('id')->on('reviews')
            ->onDelete('cascade')
            ->onupdate('cascade');
            $table->timestamps();
        });     }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
                Schema::drop('products_reviews');
    }
}
