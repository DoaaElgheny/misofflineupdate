<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
             $table->increments('id')->unsigned();
             $table->string('Cement_Name', 150)->unique();
             $table->string('Enname',150)->unique();
            $table->string('Type',50);

          $table->integer('Weight');
          $table->float('Rank');

        $table->string('Specifications', 150)->nullable();
        $table->date('Producation_date')->nullable();
        $table->date('Packing_date')->nullable();
        $table->string('Guidlines', 150)->nullable();
        $table->string('Security', 150)->nullable();
        $table->string('Others', 150)->nullable();
        $table->binary('Frontimg')->nullable();
        $table->binary('Backimg')->nullable();

             $table->integer('Company_id')->unsigned();
             $table->foreign('Company_id')
            ->references('id')->on('companies')
            ->onDelete('cascade')
            ->onupdate('cascade');

         $table->unique(array('company_id', 'Cement_Name'));

            $table->timestamps();
           $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('products');
    }
}
