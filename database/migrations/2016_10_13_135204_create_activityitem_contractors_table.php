<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivityitemContractorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activityitem_contractors', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('activityitem_id')->unsigned();
             $table->integer('contractor_id')->unsigned();
            $table->foreign('activityitem_id')
              ->references('id')->on('activity_items')
              ->onDelete('cascade')
              ->onupdate('cascade');
               $table->foreign('contractor_id')
              ->references('id')->on('contractors')
              ->onDelete('cascade')
              ->onupdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('activityitem_contractors');
    }
}
