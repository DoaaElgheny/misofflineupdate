<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDatawarehousesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('datawarehouses', function (Blueprint $table) {
            $table->increments('id');
             $table->string('Name',250)->unique();
             $table->string('EngName',250)->unique();
               $table->string('Government', 150);
             $table->string('District', 150);
            $table->string('Location',150)->nullable();

            $table->string('address');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('datawarehouses');
    }
}
