<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWarehousesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_warehouses', function (Blueprint $table) {
            $table->increments('id')->unsigned();
             $table->string('Government', 150);
             $table->string('District', 150);
             $table->string('Address',150)->nullable();
             $table->string('Location',255)->nullable();
                   
             $table->integer('company_id')->unsigned();
             $table->foreign('company_id')
             ->references('id')->on('companies')
             ->onDelete('cascade')
             ->onupdate('cascade');

            $table->unique(array('Government','District','Address','company_id'));

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('warehouses');
    }
}
