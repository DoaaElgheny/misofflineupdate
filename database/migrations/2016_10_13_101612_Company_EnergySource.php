<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CompanyEnergySource extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_energysources', function (Blueprint $table) {
             $table->increments('id')->unsigned();
              $table->float('Value');
              $table->integer('company_id')->unsigned();
              $table->foreign('company_id')
            ->references('id')->on('companies')
            ->onDelete('cascade')
            ->onupdate('cascade');
              $table->integer('energysources_id')->unsigned();
             $table->foreign('energysources_id')
            ->references('id')->on('energy_sources')
            ->onDelete('cascade')
            ->onupdate('cascade');
            $table->timestamps();
        });    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('company_energysources');
    }
}
