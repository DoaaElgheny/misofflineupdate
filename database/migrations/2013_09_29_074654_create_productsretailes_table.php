<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsretailesTable extends Migration
{
       public function up()
    {
        Schema::create('products_retailers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('Quantity');
            
            $table->timestamps();          
            $table->integer('Retailer_Id')->unsigned();
            $table->foreign('Retailer_Id')->references('id')->on('retailers')->onDelete('cascade')->onUpdate('cascade');
             $table->integer('Product_Id')->unsigned();
            $table->foreign('Product_Id')->references('id')->on('products')->onDelete('cascade')->onUpdate('cascade');
            

             });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('products_retailers');
    }
}
