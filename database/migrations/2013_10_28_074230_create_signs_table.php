<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('signs', function (Blueprint $table) {
            $table->increments('id');
             $table->string('Code')->unique();
             $table->string('Government', 150);
             $table->string('District', 150);
             $table->string('Address',300);
             $table->string('Longitude', 150);
             $table->string('Latitude', 150);
             $table->string('Trader_Name', 200);
             $table->string('Contractor_Name', 200);
             $table->string('Status', 100);
             $table->string('Project_Type', 200);
             $table->date('Date');
             $table->integer('subactivity_id')->unsigned();
             $table->foreign('subactivity_id')
              ->references('id')->on('sub_activities')
              ->onDelete('cascade')
              ->onupdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
