<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FrontofficeProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
             Schema::create('frontoffice_products', function (Blueprint $table) {
                $table->softDeletes();

                  $table->integer('product_id')->unsigned();
                  $table->integer('frontoffice_id')->unsigned();
 

                    $table->foreign('product_id')
                          ->references('id')->on('products')
                          ->onDelete('cascade')
                          ->onupdate('cascade');

                    $table->foreign('frontoffice_id')
                          ->references('id')->on('front_offices')
                          ->onDelete('cascade')
                          ->onupdate('cascade');

                 
                 
             $table->primary(['product_id', 'frontoffice_id']);
    });    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::drop('frontoffice_products');
    }
}
