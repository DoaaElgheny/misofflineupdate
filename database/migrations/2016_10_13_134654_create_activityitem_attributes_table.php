<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivityitemAttributesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activityitem_attributes', function (Blueprint $table) {
            $table->increments('id');
             $table->string('Value');     
             $table->integer('activityitem_id')->unsigned();
             $table->integer('attrbuti_id')->unsigned();
            $table->foreign('activityitem_id')
              ->references('id')->on('activity_items')
              ->onDelete('cascade')
              ->onupdate('cascade');
               $table->foreign('attrbuti_id')
              ->references('id')->on('attributes')
              ->onDelete('cascade')
              ->onupdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('activityitem_attributes');
    }
}
