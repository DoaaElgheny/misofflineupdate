<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivityItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activity_items', function (Blueprint $table) {
            $table->increments('id');
             $table->string('Code')->unique();
             $table->integer('subactivity_id')->unsigned();
             $table->foreign('subactivity_id')
              ->references('id')->on('sub_activities')
              ->onDelete('cascade')
              ->onupdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('activity_items');
    }
}
