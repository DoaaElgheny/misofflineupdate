@extends('masternew')
@section('content')
<style type="text/css">

.error{
  color: #a51c1c;
}
</style>

<section class="panel-body">
 <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                 <h2><i class="fa fa-lightbulb-o"></i> Roles </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                     
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
<!-- end x-title -->
                  <div class="x_content">

<div class="panel-body">

  <div class="row">
      {!! Form::open(['action'=>'RoleController@role_permission','method'=>'post','id'=>'form','files'=>'true']) !!}
           
            <input type="hidden"  name="id" value="{{$id}}"/>

   @foreach($permissions as $permission)
      <div class="col-sm-3 col-sm-offset-1">

          <div class="list-group" id="list1">
          <a href="#" class="list-group-item active">{{$permission['Table']}}  <input title="toggle all" type="checkbox" class="all pull-left"></a>
          @foreach($permission['permissions'] as $item)

          <a class="list-group-item" ><input type="checkbox" name="item[]"  value="{{$item['id']}}"
          {{ ($item['checked'] === 'On')? 'checked' : '' }} class="pull-left">{{$item['name']}}</a>
      @endforeach
        </div>
        </div>    

 @endforeach
    <button type="submit" class="btn btn-primary "style="float:right" name="login_form_submit" value="Save">Save</button>
       {!!Form::close() !!}
   <div class="form-group  text-center">
            <a href="/roles" class="btn btn-default btn-sm " style="    float: left;"><span class="glyphicon glyphicon-chevron-left"></span></a><td>
         
          
          </div>

</div>
</div>
</div>
    </div>
    </div>
      
  @section('other_scripts')
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
<script type="text/javascript">

var editor;

  $(document).ready(function(){




/* toggle all checkboxes in group */
$('.all').click(function(e){
  e.stopPropagation();
  var $this = $(this);
    if($this.is(":checked")) {
      $this.parents('.list-group').find("[type=checkbox]").prop("checked",true);
    }
    else {
      $this.parents('.list-group').find("[type=checkbox]").prop("checked",false);
        $this.prop("checked",false);
    }
});

$('[type=checkbox]').click(function(e){
  e.stopPropagation();
});

/* toggle checkbox when list group item is clicked */
$('.list-group a').click(function(e){
  
    e.stopPropagation();
  
    var $this = $(this).find("[type=checkbox]");
    if($this.is(":checked")) {
      $this.prop("checked",false);
    }
    else {
      $this.prop("checked",true);
    }
  
    if ($this.hasClass("all")) {
      $this.trigger('click');
    }
});



 });
  </script>


@stop
@stop
