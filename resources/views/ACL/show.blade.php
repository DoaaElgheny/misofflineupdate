@extends('masternew')
@section('content')
<style type="text/css">

.error{
  color: #a51c1c;
}
</style>

<section class="panel-body">
 <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                 <h2><i class="fa fa-lightbulb-o"></i> Roles </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                     
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
<!-- end x-title -->
                  <div class="x_content">

<div class="panel-body">

  <div class="row">
       
      @foreach($permissions as $permission)
      <div class="col-sm-3 col-sm-offset-1">

          <div class="list-group" id="list1">
          <a href="#" class="list-group-item active">{{$permission['Table']}}</a>

          @foreach($permission['permissions'] as $item)

          <a class="list-group-item" disabled readonly>
          <input type="checkbox" name="item[]" value="{{$item['id']}}" class="pull-left" checked="{{$item['name']}}" disabled readonly>{{$item['name']}}</a>
      @endforeach
       </div>
        </div>    

 @endforeach
   <div class="form-group  text-center">
            <a href="/offlineneew/public/roles" class="btn btn-default btn-sm " style="    float: left;"><span class="glyphicon glyphicon-chevron-left"></span></a><td>
         
          
          </div>

</div>
</div>
</div>
    </div>
    </div>
      
  @section('other_scripts')

  


<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>


  <script type="text/javascript">


  </script>


@stop
@stop