@extends('masternew')
@section('content')
 <section class="panel-body">
<style type="text/css">
  input[type="file"] {
    display: none;
}
.custom-file-upload {
    border: 1px solid #ccc;
    display: inline-block;
    padding: 6px 12px;
    cursor: pointer;
}

</style>

 <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                     <h2 ><i class="fa fa-tasks" ></i>
Import Data to offline system </h2>
                        <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                     
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
<div>
{!!Form::open(['action'=>'SuperadminPanelController@importnewgps','method' => 'post','files'=>true])!!}

<label for="file-uploadnewgps" class="custom-file-upload">
    <i class="fa fa-cloud-upload"></i> Select File
</label>
<input id="file-uploadnewgps"  name="newgps" type="file"/>
   
    <input type="submit" name="newgps" value="Upload Gps" class="btn btn-primary"/>




    {!!Form::close()!!}

{!!Form::open(['action'=>'SuperadminPanelController@importorder','method' => 'post','files'=>true])!!}

<label for="file-uploadorder" class="custom-file-upload">
    <i class="fa fa-cloud-upload"></i> Select File
</label>
<input id="file-uploadorder"  name="order" type="file"/>   
 <input type="submit" name="order" value="Upload order" class="btn btn-primary"/>  
{!!Form::close()!!}









{!!Form::open(['action'=>'SuperadminPanelController@importcafesign','method' => 'post','files'=>true])!!}

<label for="file-uploadcafesign" class="custom-file-upload">
    <i class="fa fa-cloud-upload"></i> Select File
</label>
<input id="file-uploadcafesign"  name="cafesign" type="file"/>    <input type="submit" name="cafesign" value="Upload sign&cafe" class="btn btn-primary"/>  
{!!Form::close()!!}

{!!Form::open(['action'=>'SuperadminPanelController@ImportActivity','method' => 'post','files'=>true])!!}

<label for="file-upload3" class="custom-file-upload">
    <i class="fa fa-cloud-upload"></i> Select File
</label>
<input id="file-upload3"  name="Activity" type="file"/>    <input type="submit" name="Activity" value="Upload Activity" class="btn btn-primary"/>  
{!!Form::close()!!}

{!!Form::open(['action'=>'SuperadminPanelController@ImportnewPrice','method' => 'post','files'=>true])!!}

<label for="file-upload4" class="custom-file-upload">
    <i class="fa fa-cloud-upload"></i> Select File
</label>
<input id="file-upload4"  name="Price" type="file"/>    <input type="submit" name="Price" value="Upload Price" class="btn btn-primary"/>  
{!!Form::close()!!}</div>






<div style="margin-top: 50px;">
<h4> Export Data to online system</h4>

{!!Form::open(['action'=>'SuperadminPanelController@exportproduct','method' => 'post','files'=>true,'class'=>'form'])!!}
    <input type="submit" name="product" value="Export Product" class="btn btn-primary"/>  
{!!Form::close()!!}
{!!Form::open(['action'=>'ReportController@exportreportpro','method' => 'post','files'=>true,'class'=>'form'])!!}
    <input type="submit" name="pro" value="Export Sign" class="btn btn-primary"/>  
{!!Form::close()!!}
{!!Form::open(['action'=>'SuperadminPanelController@ExportMultiProduct','method' => 'post','files'=>true,'class'=>'form'])!!}
    <input type="submit" name="MultiProduct" value="Export  Multi Product" class="btn btn-primary"/>  
{!!Form::close()!!}
{!!Form::open(['action'=>'SuperadminPanelController@ExportCafe','method' => 'post','files'=>true,'class'=>'form'])!!}
    <input type="submit" name="Cafe" value="Export Cafe" class="btn btn-primary"/>  
{!!Form::close()!!}
{!!Form::open(['action'=>'SuperadminPanelController@exportlocalcompany','method' => 'post','files'=>true,'class'=>'form'])!!}
    <input type="submit" name="company" value="Export Company" class="btn btn-primary"/>  
{!!Form::close()!!}
</div>


                  </div>

                </div>
              </div>



<div style="margin-top: 50px;">
<h4> import Data inventory to online system</h4>
{!!Form::open(['action'=>'ExportInventoryController@importItems','method' => 'post','files'=>true])!!}

<label for="file-upload4" class="custom-file-upload">
    <i class="fa fa-cloud-upload"></i> Select File
</label>
<input id="file-upload4"  name="Price" type="file"/>    <input type="submit" name="Price" value="Upload SubCategory" class="btn btn-primary"/>  
{!!Form::close()!!}</div>



{!!Form::open(['action'=>'ExportInventoryController@importItems','method' => 'post','files'=>true])!!}

<label for="file-upload4" class="custom-file-upload">
    <i class="fa fa-cloud-upload"></i> Select File
</label>
<input id="file-upload4"  name="Price" type="file"/>    <input type="submit" name="Price" value="Upload Items" class="btn btn-primary"/>  
{!!Form::close()!!}</div>


{!!Form::open(['action'=>'ExportInventoryController@importItems','method' => 'post','files'=>true])!!}

<label for="file-upload4" class="custom-file-upload">
    <i class="fa fa-cloud-upload"></i> Select File
</label>
<input id="file-upload4"  name="Price" type="file"/>    <input type="submit" name="Price" value="Upload Purches" class="btn btn-primary"/>  
{!!Form::close()!!}</div>


{!!Form::open(['action'=>'ExportInventoryController@importItems','method' => 'post','files'=>true])!!}

<label for="file-upload4" class="custom-file-upload">
    <i class="fa fa-cloud-upload"></i> Select File
</label>
<input id="file-upload4"  name="Price" type="file"/>    <input type="submit" name="Price" value="Upload Item Purches" class="btn btn-primary"/>  
{!!Form::close()!!}</div>


{!!Form::open(['action'=>'ExportInventoryController@importItems','method' => 'post','files'=>true])!!}

<label for="file-upload4" class="custom-file-upload">
    <i class="fa fa-cloud-upload"></i> Select File
</label>
<input id="file-upload4"  name="Price" type="file"/>    <input type="submit" name="Price" value="Upload Quantity" class="btn btn-primary"/>  
{!!Form::close()!!}</div>


</div>


              
 
  @section('other_scripts')



@stop

@stop
