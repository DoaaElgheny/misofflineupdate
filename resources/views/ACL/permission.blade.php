@extends('master')
@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}" />
<style type="text/css">

.error{
  color: #a51c1c;
}
</style>

<section class="panel panel-primary">

<div class="panel-body">

  <h1><i class="fa fa-lightbulb-o" aria-hidden="true"></i> Permission </h1>

  <a type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal1" style="margin-bottom: 20px;"> Add New Permission </a>
<!-- model -->
<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
     <div class="modal-content">
    
          <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                 <h4 class="modal-title span7 text-center" id="myModalLabel"><span class="title">Add New Permission</span></h4>

          </div>
    <div class="modal-body">

        {!! Form::open(['route'=>'permissions.store','method'=>'post','id'=>'form','files'=>'true']) !!}
             <input type="hidden" name="_token" value="{{ csrf_token() }}">

       
            <div class="form-group col-md-12">
                <label for="input_role" class="control-label col-md-3">Name</label>
                <div class="col-md-9">
                    <input type="text" class="form-control" id="name" name="name" placeholder="enter Permission Name">
                </div>
            </div>
            <div class="form-group col-md-12">
                <label for="input_display_name" class="control-label col-md-3"> Display Name</label>
                <div class="col-md-9">
                    <input type="text" class="form-control" id="display_name" name="display_name" placeholder="enter Permission Display Name">
                </div>
            </div>
            <div class="form-group col-md-12">
                <label for="input_point" class="control-label col-md-3">Description</label>
                <div class="col-md-9">
                    <input type="text"  step="any" class="form-control" id="description" name="description" placeholder="Description">
                </div>
            </div>
                            <hr/>
              <button type="submit" class="btn btn-primary "style="float:right" name="login_form_submit" value="Save">Save</button>
             <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

      {!!Form::close() !!}
    </div>
 

         </div>
     </div>
  </div>
<!-- ENDModal -->
          @if(Session::has('error'))
           
          <div class="alert alert-danger" style="text-align:center;" >
         <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
                  <strong>Whoops!</strong> {{ Session::get('error') }}<br><br>

    
    </div>
    @endif
<table class="table table-hover table-bordered  dt-responsive nowrap display permissions" cellspacing="0" width="100%">
  <thead>
              <th>No</th>
                <th>Name</th>
                     <th>Display Name</th>
                      <th> Description</th>
                           
                         <th>Action</th>            
  </thead>
    <tfoot>
        
                  <th></th> 
                      <th>Name</th>
                     <th>Display Name</th>
                      <th> Description</th>
                            <th></th>
            
                  </tfoot>
               

<tbody style="text-align:center;">
  <?php $i=1;?>
  @foreach($permissions as $permission)
    <tr>
    
     <td>{{$i++}}</td>
         <td>
          <a  class="testEdit" data-type="text" data-name="name" data-pk="{{ $permission->id }}">{{$permission->name}}</a></td>
         
         <td>
               <a class="testEdit" data-type="text"  data-name="display_name" data-pk="{{$permission->id }}">{{$permission->display_name}}</a>
         </td> 

          <td>
               <a class="testEdit" data-type="text"   data-name="description" data-pk="{{$permission->id }}">{{$permission->description}}</a>
          </td>
           <td>
   
            <a href="/cemexmarketingsystem/public/permissions/destroy/{{$permission->id}}"class="btn btn-default btn-sm" ><span class="glyphicon glyphicon-trash"></span>  </a>


             </td>
   </tr>
     @endforeach
</tbody>
</table>

</div>
    
    </section>
  


<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>


  <script type="text/javascript">

var editor;

  $(document).ready(function(){
     var table= $('.permissions').DataTable({
  
   
    responsive: true,
    "order":[[0,"asc"]],
    'searchable':true,
    "scrollCollapse":true,
    "paging":true,
    "pagingType": "simple",
      dom: 'lBfrtip',
        buttons: [
           
            
            { extend: 'excel', classname: 'btn btn-primary dtb' }
            
            
        ],


   

fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
$.fn.editable.defaults.send = "always";

$.fn.editable.defaults.mode = 'inline';

$('.testEdit').editable({

      validate: function(value) {
        name=$(this).editable().data('name');
        if(name=="name")
        {
                if($.trim(value) == '') {

                    return 'Value is required.';
                  }
         }
        if(name=="name")
        {

              var regexp = /^[a-zA-Z\u0600-\u06FF,-][\sa-zA-Z\u0600-\u06FF,-]/;           

                  if (!regexp.test(value)) {
                        return 'This field is not valid';
                    }
        
         }

        },

    url:'{{URL::to("/")}}/permission_update',
  
     ajaxOptions: {
     type: 'get',
    sourceCache: 'false',
     dataType: 'json'

   },

       params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');

            // console.log(params);
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'This Data Already Exist,Enter Correct Data.';
            } else {
                return response.responseText;
            }
        }


});

}
});

 



   $('.permissions tfoot th').each(function () {



      var title = $('.permissions thead th').eq($(this).index()).text();
               
 if($(this).index()>=1 && $(this).index()<=3)
        {

           $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
}
        

    });
  table.columns().every( function () {
  var that = this;
 $(this.footer()).find('input').on('keyup change', function () {
          that.search(this.value).draw();
            if (that.search(this.value) ) {
               that.search(this.value).draw();
           }
        });
      
    });




$.validator.addMethod("English", function(value, element) {

    return this.optional(element) || /^[\w&_\-'\s"\\,.\/]*$/.test(value);
 });


 $("#form").validate({
        rules: {

            name: {
                        
                        required: true,
               
                          English:true,
                          remote: {
                                      url: "./permission_name",
                                     type: "get",
                                      data: {
                                      name: function() {
                                           return $( "#name" ).val();
                                           }
                                          },
                                      beforeSend: function (request) {
                                      return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                                         }
                                      }
                            },
             display_name: {
                        
               
                          English:true,
                  
                            }              
        },

        messages: {
            name: {
                required: "Please enter permission name",
                alpha: "permission name must be at least 3 characters",
                remote:jQuery.validator.format("This permission name {0} is Already Existed")

            },
              display_name: {
                English: "permission display name must be English characters"

            }  

        }
    });















 });
  </script>











@stop

