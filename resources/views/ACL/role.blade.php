@extends('masternew')
@section('content')
<style type="text/css">
.error{
  color: #a51c1c;
}
</style>
<section class="panel-body">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2><i class="fa fa-lightbulb-o"></i> Roles </h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
          <li><a class="close-link"><i class="fa fa-close"></i></a></li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <a type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal1" style="margin-bottom: 20px;"> Add New Roles </a>
          <!-- model -->
          <div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                  <h4 class="modal-title span7 text-center" id="myModalLabel"><span class="title">Add New Roles</span></h4>
                </div>
                <div class="modal-body">
                  <div class="tabbable"> <!-- Only required for left/right tabs -->
                    <ul class="nav nav-tabs"></ul>
                    <div class="tab-content">
                      <div class="tab-pane active" id="tab1">
                        <div class="row">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                              <div class="x_content">
                                <br />
                                {!! Form::open(['action' => "RoleController@store",'method'=>'post','id'=>'form','class'=>'form-horizontal','files'=>'true']) !!} 
                                  <fieldset style="    padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;">
                                    <legend style="width: fit-content;border: 0px;">Main Date:</legend>
                                    
                                      <div class="form-group">
                                        <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                                          <input type="text" class="form-control has-feedback-left" id="display_name" name="display_name" placeholder="enter Role Display Name" required>
                                          <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                                          <input type="text" class="form-control has-feedback-left" id="name" name="name" placeholder="enter Role Name" required>
                                          <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                                        </div>
                                      </div>
                                      <div class="form-group">
                                        <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                                          <input type="text"  step="any" class="form-control has-feedback-left" id="description" name="description" placeholder="Description" required>
                                          <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                                        </div>
                                      </div>        
                                  </fieldset>
                                  <div class="ln_solid"></div>
                                  <div class="form-group">
                                    <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                                      <button type="button" class="btn btn-primary " data-dismiss="modal">Cancel</button>
                                      <button class="btn btn-primary" type="reset">Reset</button>
                                      <button type="submit"  name="login_form_submit" class="btn btn-success" value="Save" disabled>Submit</button>
                                    </div>
                                  </div>
                                {!!Form::close() !!}                
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        <div class="panel-body">
          @if(Session::has('error'))
            <div class="alert alert-danger" style="text-align:center;" >
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <strong>Whoops!</strong> {{ Session::get('error') }}<br><br>
            </div>
          @endif
          <table class="table table-hover table-bordered  dt-responsive nowrap  roles" cellspacing="0" width="100%">
            <thead>
              <th>No</th>
              <th>Name</th>
              <th>Display Name</th>
              <th> Description</th>
              <th>Show Permissions</th>  
              <th>Add Permissions</th>
              <th>Action</th>        
            </thead>
            <tfoot>
              <th></th> 
              <th>Name</th>
              <th>Display Name</th>
              <th> Description</th>
              <th></th>
              <th></th>
              <th></th>
            </tfoot>
            <tbody style="text-align:center;">
              <?php $i=1;?>
              @foreach($roles as $role)
                <tr>           
                  <td>{{$i++}}</td>
                  <td><a  class="testEdit" data-type="text" data-name="name" data-pk="{{ $role->id }}">{{$role->name}}</a></td>
                  <td><a class="testEdit" data-type="text"  data-name="display_name" data-pk="{{$role->id }}">{{$role->display_name}}</a></td> 
                  <td><a class="testEdit" data-type="text"   data-name="description" data-pk="{{$role->id}}">{{$role->description}}</a></td>
                  <td><a href="/offlineneew/public/chose/{{$role->id}}"class="btn btn-default btn-sm" ><span class="glyphicon glyphicon-plus">Add/Edit Permission</span>  </a></td>
                  <td><a href="/offlineneew/public/roles/{{$role->id}}"class="btn btn-default btn-sm" ><span class="glyphicon glyphicon-eye-open">Show Permission</span>  </a></td>
                  <td><a href="/offlineneew/public/roles/destroy/{{$role->id}}"class="btn btn-default btn-sm" ><span class="glyphicon glyphicon-trash"></span>  </a></td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</section>
@section('other_scripts')


<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>


  <script type="text/javascript">

var editor;

  $(document).ready(function(){



  $(".Permissions").chosen({ 
                   width: '100%',
                   no_results_text: "لا توجد نتيجه",
                   allow_single_deselect: true, 
                   search_contains:true, });
 $(".Permissions").trigger("chosen:updated");







     var table= $('.roles').DataTable({
  
   
    responsive: true,
    "order":[[0,"asc"]],
    'searchable':true,
    "scrollCollapse":true,
    "paging":true,
    "pagingType": "simple",
     dom: 'lBfrtip',
   "iDisplayLength": 50,
        buttons: [
           
            
            { extend: 'excel', className: 'btn btn-primary dtb' }
            
            
        ],


   

fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
$.fn.editable.defaults.send = "always";

$.fn.editable.defaults.mode = 'inline';

$('.testEdit').editable({

      validate: function(value) {
        name=$(this).editable().data('name');
        if(name=="name")
        {
                if($.trim(value) == '') {

                    return 'Value is required.';
                  }
         }
        if(name=="name")
        {

              var regexp = /^[a-zA-Z\u0600-\u06FF,-][\sa-zA-Z\u0600-\u06FF,-]/;           

                  if (!regexp.test(value)) {
                        return 'This field is not valid';
                    }
        
         }

        },

    url:'{{URL::to("/")}}/role_update',
  
     ajaxOptions: {
     type: 'get',
    sourceCache: 'false',
     dataType: 'json'

   },

       params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');

            // console.log(params);
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'This Data Already Exist,Enter Correct Data.';
            } else {
                return response.responseText;
            }
        }


});

}
});

 



   $('.roles tfoot th').each(function () {



      var title = $('.roles thead th').eq($(this).index()).text();
               
 if($(this).index()>=1 && $(this).index()<=3)
        {

           $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
}
        

    });
  table.columns().every( function () {
  var that = this;
 $(this.footer()).find('input').on('keyup change', function () {
          that.search(this.value).draw();
            if (that.search(this.value) ) {
               that.search(this.value).draw();
           }
        });
      
    });




$.validator.addMethod("English", function(value, element) {

    return this.optional(element) || /^[\w&_\-'\s"\\,.\/]*$/.test(value);
 });
 $('#form')
        .formValidation({
            framework: 'bootstrap',

            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                'name': {
                    validators: {
                             remote: {
                        url: '/offlineneew/public/role_name',
                        message: 'The  Name is not available',
                        type: 'GET',
                         data: {
                                      name: function() {
                                           return $( "#name" ).val();
                                           }
                                          }
                    },
                              notEmpty:{
                    message:"this field is required"
                  }
                     ,
                    regexp: {
                        regexp: /^[A-z\s]+$/,
                        message: 'The Name can only consist of alphabetical and spaces'
                    }
                    }
                },
                'display_name':{
                   validators: {
                         notEmpty:{
                    message:"this field is required"
                  }
                     ,
                    regexp: {
                        regexp: /^[A-z\s]+$/,
                        message: 'The Display Name can only consist of alphabetical and spaces'
                    } 
                    }
                }
               
            } ,submitHandler: function(form) {
        form.submit();
    }
        })
        .on('success.form.fv', function(e) {
   // e.preventDefault();
    var $form = $(e.target);

    // Enable the submit button
   
    $form.formValidation('disableSubmitButtons', false);
});


 // $("#form").validate({
 //        rules: {

 //            name: {
                        
 //                        required: true,
               
 //                          English:true,
 //                          remote: {
 //                                      url: "/role_name",
 //                                     type: "get",
 //                                      data: {
 //                                      name: function() {
 //                                           return $( "#name" ).val();
 //                                           }
 //                                          },
 //                                      beforeSend: function (request) {
 //                                      return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
 //                                         }
 //                                      }
 //                            },
 //             display_name: {
                        
               
 //                          English:true
                        
 //                            }              
 //        },

 //        messages: {
 //            name: {
 //                required: "Please enter role name",
 //                alpha: "role name must be at alpha characters",
 //                remote:jQuery.validator.format("This role name {0} is Already Existed")

 //            },
 //              display_name: {
               
 //                English: "role display name must be English characters"

 //            }  

 //        }
 //    });
 });
  </script>
@endsection
@endsection