
@extends('masternew')
@section('content')
<!DOCTYPE html>
<meta name="csrf-token" content="{{ csrf_token() }}" />

<section class="panel panel-primary">
<!-- <style type="text/css">
  input[type="file"] {
    display: none;
}
.custom-file-upload {
    border: 1px solid #ccc;
    display: inline-block;
    padding: 6px 12px;
    cursor: pointer;
}
.form{
    display: inline-block;
    padding: 5px;
}
</style> -->
<style type="text/css">
/*  input[type="file"] {
   
}*/

.custom-file-upload {
    border: 1px solid #ccc;
    display: inline-block;
    padding: 6px 12px;
    cursor: pointer;
}
.form{
    display: inline-block;
    padding: 5px;
}
</style>

<div class="panel-body">

<div><h4> Import Data to offline system</h4>
{!!Form::open(['action'=>'SuperadminPanelController@importnewgps','method' => 'post','files'=>true])!!}

<label for="file-upload" class="custom-file-upload">
    <i class="fa fa-cloud-upload"></i> Select File
</label>
<input id="file-upload"  name="file" type="file"/>
   
    <input type="submit" name="submit" value="Upload Gps" class="btn btn-primary"/>




    {!!Form::close()!!}

{!!Form::open(['action'=>'SuperadminPanelController@importorder','method' => 'post','files'=>true])!!}

<label for="file-upload" class="custom-file-upload">
    <i class="fa fa-cloud-upload"></i> Select File
</label>
<input id="file2"  name="file" type="file"/>   
 <input type="submit" name="submit" value="Upload order" class="btn btn-primary"/>  
{!!Form::close()!!}








{!!Form::open(['action'=>'SuperadminPanelController@importcafesign','method' => 'post','files'=>true])!!}

<label for="file-upload" class="custom-file-upload">
    <i class="fa fa-cloud-upload"></i> Select File
</label>
<input id="file-upload"  name="file" type="file"/>    <input type="submit" name="submit" value="Upload sign&cafe" class="btn btn-primary"/>  
{!!Form::close()!!}

{!!Form::open(['action'=>'SuperadminPanelController@ImportActivity','method' => 'post','files'=>true])!!}

<label for="file-upload" class="custom-file-upload">
    <i class="fa fa-cloud-upload"></i> Select File
</label>
<input id="file-upload3"  name="file" type="file"/>    <input type="submit" name="submit" value="Upload Activity" class="btn btn-primary"/>  
{!!Form::close()!!}

{!!Form::open(['action'=>'SuperadminPanelController@ImportnewPrice','method' => 'post','files'=>true])!!}

<label for="file-upload" class="custom-file-upload">
    <i class="fa fa-cloud-upload"></i> Select File
</label>
<input id="file-upload4"  name="file" type="file"/>    <input type="submit" name="submit" value="Upload Price" class="btn btn-primary"/>  
{!!Form::close()!!}</div>

<div style="margin-top: 50px;">
<h4> Export Data to online system</h4>

{!!Form::open(['action'=>'SuperadminPanelController@exportproduct','method' => 'post','files'=>true,'class'=>'form'])!!}
    <input type="submit" name="submit" value="Export Product" class="btn btn-primary"/>  
{!!Form::close()!!}
{!!Form::open(['action'=>'ReportController@exportreportpro','method' => 'post','files'=>true,'class'=>'form'])!!}
    <input type="submit" name="submit" value="Export Sign" class="btn btn-primary"/>  
{!!Form::close()!!}
{!!Form::open(['action'=>'SuperadminPanelController@ExportMultiProduct','method' => 'post','files'=>true,'class'=>'form'])!!}
    <input type="submit" name="submit" value="Export  Multi Product" class="btn btn-primary"/>  
{!!Form::close()!!}
{!!Form::open(['action'=>'SuperadminPanelController@ExportCafe','method' => 'post','files'=>true,'class'=>'form'])!!}
    <input type="submit" name="submit" value="Export Cafe" class="btn btn-primary"/>  
{!!Form::close()!!}
{!!Form::open(['action'=>'SuperadminPanelController@exportlocalcompany','method' => 'post','files'=>true,'class'=>'form'])!!}
    <input type="submit" name="submit" value="Export Company" class="btn btn-primary"/>  
{!!Form::close()!!}
</div>
</section>
</div>

  @stop
