@extends('masternew')
@section('content')
 <section class="panel-body">




 <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">

 @if($header)
 <h2><i class="fa fa-tasks"></i> Orders</h2> 

  @else
 <h2><i class="fa fa-tasks"></i> Contractors Program </h2> 

 

@endif

                     
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                     
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                   
                  
                      <table class="table table-hover table-bordered  dt-responsive nowrap  users" cellspacing="0" width="100%" >
  <thead>

              <th>No</th>
              <th>Product Name</th> 
              <th>Promoter Name</th> 
              <th>Gps Date</th>
              <th>Gps Code</th>
              <th>Amount</th> 
              <th>Type</th> 
                @if($header)
              <th>IsDone</th> 
              <th>Order_Status</th> 
              <th>Notes</th>

               @endif
              <th>Process</th>    
</thead>

<tfoot>
<th></th>
             
              <th>Product Name</th> 
               
              <th>Promoter Name</th> 
              <th>Gps Date</th>
              <th>Gps Code</th>
              <th>Amount</th> 
              <th>Type</th> 
                @if($header)
              <th>IsDone</th> 
              <th>Order_Status</th> 
              <th>Notes</th>

               @endif
              <th></th> 
</tfoot>

<tbody style="text-align:center;">
<?php $i=1;?>
@foreach($orders as $order)
    <tr>
    
     <td>{{$i++}}</td> 
      
     @if($order->SaleProduct_id!=null)
     
     <td>{{$order->orderbelongtosaleproduct->Name}}</td>
      @else
      @if($order->Product_id!=null)
       <td>{{$order->orderbelongtoproduct->Cement_Name}}</td>
       @else
        <td></td>
       @endif
        @endif

     
  <td>{{$order->Name}}</td>
  <td> {{$order->orderbelongtogps->NDate}} </td>
  <td> {{$order->orderbelongtogps->Code}} </td>
  <td><a  class="testEdit" data-type="text" data-name="Amount"  data-pk="{{ $order->id }}">{{$order->Amount }}</a></td> 
   <td>{{$order->Type }}</td> 
  @if($header)     
     @if($order->IsDone==1)
                <td><input id="check" type="checkbox" class="check" data-name="IsDone"  data-pk="{{ $order->id }}" onclick="ActiveCheck(this,{{ $order->id }})" value="{{$order->IsDone}}"  checked/></td>
              @else
                <td><input id="check" type="checkbox" class="check" data-name="IsDone"  data-pk="{{ $order->id }}" onclick="ActiveCheck(this,{{ $order->id }})" value="{{$order->IsDone}}"  /></td>
              @endif

      
   <td><a  class="testEdit" data-type="select" data-value="{{$order->Order_Status}}"
   data-source ="[{'value':'Achived', 'text': 'Achived'}, {'value':'Pending', 'text': 'Pending'},{'value':'Reject', 'text': 'Reject'}]"
    data-name="Order_Status"  data-pk="{{ $order->id }}">{{$order->Order_Status }}</a></td> 
               
 
     
   <td><a  class="testEdit" data-type="text" data-name="Notes"  data-pk="{{ $order->id }}">{{$order->Notes}}</a></td>
 

 @endif

  <td>
     <a href="/offlineneew/public/orders/destroy/{{$order->id}}"class="btn btn-default btn-sm" ><span class="glyphicon glyphicon-trash"></span>
       </a>

</td>

   </tr>
     @endforeach
</tbody>
</table>


                  </div>
                </div>
              </div>
 
  @section('other_scripts')


<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>



  <script type="text/javascript">

var editor;

  $(document).ready(function(){
     
     var table= $('.users').DataTable({
  
    select:true,
    responsive: true,
    "order":[[0,"asc"]],
    'searchable':true,
    "scrollCollapse":true,
    "paging":true,
    "pagingType": "simple",
      dom: 'lBfrtip',
        buttons: [
           
            
            { extend: 'excel', className: 'btn btn-primary dtb' }
            
            
        ],

fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
$.fn.editable.defaults.send = "always";

$.fn.editable.defaults.mode = 'inline';

$('.testEdit').editable({

        validate: function(value) {
        name=$(this).editable().data('name');
        if(name=="Amount")
        {
                if($.trim(value) == '') {
                    return 'Value is required.';
                  }
                    }
      
        },

    placement: 'right',
    url:'{{URL::to("/")}}/updateorder',
  
     ajaxOptions: {
     type: 'get',
    sourceCache: 'false',
     dataType: 'json'

   },

       params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');

            // console.log(params);
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'This Data Already Exist,Enter Correct Data.';
            } else {
                return response.responseText;
            }
        }
        ,    success:function(response)
        {
            if(response.status==="You do not have permission.")
             {
              return 'You do not have permission.';
              }
        }


});

}
});
   $('.users tfoot th').each(function () {
      var title = $('.users thead th').eq($(this).index()).text();       
     if($(this).index()>=1 && $(this).index()<=8)
            {

           $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
}
        

    });
  table.columns().every( function () {
  var that = this;
 $(this.footer()).find('input').on('keyup change', function () {
          that.search(this.value).draw();
            if (that.search(this.value) ) {
               that.search(this.value).draw();
           }
        });
      
    });

 });
function ActiveCheck(e,id)
{
  var act;
  if(e.value==1)
    act=0;
  else
    act=1;

  $.ajax({
      url: "/offlineneew/public/updateactiveorder?active="+act+"&id="+id,
      type: "get",
      success: function(data){
      }
    }); 
}
</script>
@stop
@stop