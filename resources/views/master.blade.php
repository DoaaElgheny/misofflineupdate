<!DOCTYPE html>
<?php
header("Cache-Control: no-store, must-revalidate, max-age=0");
header("Pragma: no-cache");
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
?>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <title>Offline MIS</title>
  <!--   <html xmlns="http://www.w3.org/1999/xhtml" > -->
          <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
<!-- <link rel="stylesheet" href="/public/assets/bootstrap/css/bootstrap.min.css">
 -->
<!-- <SideMenu Links> -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Shoppy Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<!-- <link href="/assets/MenuFolder/css/bootstrap.css" rel="stylesheet" type="text/css" media="all"> -->
<!-- Custom Theme files -->
<link href="/public/assets/MenuFolder/css/style.css" rel="stylesheet" type="text/css" media="all"/>
<!--js-->
<script src="/public/assets/MenuFolder/js/jquery-2.1.1.min.js"></script> 
<!--icons-css-->
<link href="/public/assets/MenuFolder/css/font-awesome.css" rel="stylesheet"> 
<!--Google Fonts-->
<link href='//fonts.googleapis.com/css?family=Carrois+Gothic' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Work+Sans:400,500,600' rel='stylesheet' type='text/css'>
<!-- <End Side Menu Links> -->

<!-- Ionicons -->
  <link rel="stylesheet" href="/public/assets/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="/public/assets/dist/css/AdminLTE.min.css">
 
  <link rel="stylesheet" href="/public/assets/dist/css/skins/skin-blue.min.css">

<script type="text/javascript" src="/public/assets/js/jquery-2.2.3.min.js"></script>
<script src="/public/assets/dist/js/app.min.js"></script>
<!-- <script src="/public/assets/bootstrap/js/bootstrap.min.js"></script>
 -->
<script type="text/javascript" src="/public/assets/js/canvasjs.min.js"></script>



<script src="/public/assets/js/chosen.jquery.min.js"></script>

<link rel="stylesheet" href="/public/assets/css/chosen.min.css" />

  <link rel="stylesheet" href="/public/assets/css/bootstrap.min.css">

<script type="text/javascript" src="/public/assets/js/bootstrap.min.js"></script>

<script type="text/javascript" src="/public/assets/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/public/assets/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="/public/assets/js/dataTables.responsive.min.js"></script>
<script type="text/javascript" src="/public/assets/js/responsive.bootstrap.min.js"></script>


<link rel="stylesheet" type="text/css" href="/public/assets/css/datatables.min.css"/>
<!-- <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"/> -->
<link rel="stylesheet" type="text/css" href="/public/assets/css/dataTables.bootstrap.min.css"/>
<link rel="stylesheet" type="text/css" href="/public/assets/css/jquery.dataTables.min.css"/>


<link rel="stylesheet prefetch" href="/public/assets/css/dataTables.responsive.css">
<link rel="stylesheet" type="text/css" href="/public/assets/css/responsive.bootstrap.min.css"/>


<link rel="stylesheet" href="/public/assets/css/datepicker.min.css" />
<link rel="stylesheet" href="/public/assets/css/datepicker3.min.css"/>
<script src="/public/assets/js/bootstrap-datepicker.min.js"></script>

<link href="/public/assets/css/bootstrap-editable.css" rel="stylesheet"/>
<script src="/public/assets/js/bootstrap-editable.min.js"></script>


<script type="text/javascript" src="/public/assets/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="/public/assets/js/buttons.print.min.js"></script>
<script type="text/javascript" src="/public/assets/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="/public/assets/js/buttons.jqueryui.min.js"></script>
<script type="text/javascript" src="/public/assets/js/jszip.min.js"></script>
<script type="text/javascript" src="/public/assets/js/pdfmake.min.js"></script>
<link rel="stylesheet" type="text/css" href="/public/assets/css/buttons.jqueryui.min.css">

 <link rel="stylesheet" type="text/css" href="/public/assets/css/bootstrap-horizon.css"> 
<script src="/public/assets/js/jQuery.extendext.js"></script>
<script src="/public/assets/js/doT.min.js"></script>
<script src="/public/assets/js/query-builder.min.js"></script>

<link rel="stylesheet" type="text/css" href="/public/assets/css/query-builder.default.min.css">

<link rel="stylesheet" type="text/css" href="/public/assets/css/style.css">
<link href="/public/assets/css/bootstrap-editable.css" rel="stylesheet"/>
<script src="/public/assets/js/bootstrap-editable.min.js"></script>
<script type="text/javascript" src="/public/assets/js/jquery.validate.js"></script>
<script type="text/javascript" src="/public/assets/js/additional-methods.js"></script>
<script type="text/javascript" src="/public/assets/js/fnReloadAjax.js"></script>
<script type="text/javascript" src="/public/assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="/public/assets/js/additional-methods.min.js"></script>
<link rel="stylesheet" href="/public/assets/formvalidation/css/formValidation.min.css">
<script src="/public/assets/formvalidation/js/formValidation.min.js"></script>
<script src="/public/assets/formvalidation/js/framework/bootstrap.min.js"></script>
 <link href="/public/assets/css/jquerysctipttop.css" rel="stylesheet" type="text/css">   
 <script src="/public/assets/js/jquery.table2excel.js"></script>
</head>

<body class="hold-transition skin-blue sidebar-mini" >
<div class="page-container">

  <!-- Main Header -->
  <header class="main-header">

    <!-- Logo -->
   <!--  <a href="#" class="logo"> -->
      <!-- mini logo for sidebar mini 50x50 pixels -->
    <!--   <span class="logo-mini"><b>A</b>LT</span> -->
      <!-- logo for regular state and mobile devices -->
      <!-- <span class="logo-lg"><b>Admin</b>Panel</span> -->
  <!--   </a> -->

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation" style="position: fixed;width: 100%;">
      <!-- Sidebar toggle button-->
      <!-- <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a> -->
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
         
          <!-- /.messages-menu -->

          <!-- Notifications Menu -->
          <li class="dropdown notifications-menu">
             <!-- Menu toggle button -->
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell-o"></i>

              <span class="label label-danger" id="Notify"></span>
            </a>
            <ul class="dropdown-menu">
              <li class="header" id="headerNotification"></li>
              <li>
                <!-- Inner Menu: contains the notifications -->
                
              </li>
              <li class="footer"><a href="/public/MinLimtItems">View all</a></li>
            </ul>
          </li>
          <!-- Tasks Menu -->
          <li id="noti_Container" class="dropdown notifications-menu">
                <div id="noti_Counter"></div>   <!--SHOW NOTIFICATIONS COUNT.-->
                
                <!--A CIRCLE LIKE BUTTON TO DISPLAY NOTIFICATION DROPDOWN.-->
                <div id="noti_Button">
                <a  id="bell" class="fa fa-shopping-cart fa-2x" aria-hidden="true" style="color:white;"></a>
                </div>    

                <div id="notifications" style="margin-left: 0px;">
                    <h3>Notifications</h3>
                    <div style="height:auto;" id="pnotifications">
                    </div>
                    <div class="seeAll"  "><a href="#">No Notifications Else</a></div>
                </div>
            </li> 
          <!-- User Account Menu -->
          <li class="dropdown user user-menu">
            <!-- Menu Toggle Button -->
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <!-- The user image in the navbar-->
              <img src="/public/assets/img/Capture 1.JPG"  class="user-image" alt="User Image">
              <!-- hidden-xs hides the username on small devices so only the image appears. -->
              <span class="hidden-xs"></span>
            </a>
            <ul class="dropdown-menu">
              <!-- The user image in the menu -->
              <li class="user-header">
              <img src="/public/assets/img/Capture 1.JPG" class="img-circle" alt="User Image">
              <p>
           <?php if(!empty($_COOKIE['username']))  {
          echo "<p>";
          echo $_COOKIE['username'];
          echo " </p>";
            }
           ?>
              <small></small>
              </p>
              </li>
              <!-- Menu Body -->
              <li class="user-body">
                <!-- /.row -->
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <!-- <div class="pull-left">
                  <a href="#" class="btn btn-default btn-flat">Profile</a>
                </div> -->
                <div class="pull-right">
                  <a href="/public/logout" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <!-- <aside class="main-sidebar"> -->

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar" >

      <!-- Sidebar user panel (optional) -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="/public/assets/img/Capture 1.JPG" class="img-circle" alt="User Image">
        </div>
     
      </div>

      <!-- search form (Optional) -->
    <!--   <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form> -->
      <!-- /.search form -->

     <!--slider menu-->
    <div class="sidebar-menu" style="min-height: 100%;height: 2000px;position: absolute;" >
        <!-- <div class="logo"> <a href="#" class="sidebar-icon">
         <span class="fa fa-bars"></span> 
         </a> <a href="#"> <span id="logo" >
           
         </span> 
          
        </a> 
        </div>  -->

       <!--  <div class="menu"> -->
        <div style="margin-top:50px;width: 100%;height: 30px; ">
           <a href="#" class="sidebar-icon" style="margin-top: 0px;margin-right:30%">
         <span class="fa fa-bars"></span> 
         </a> <a href="#"> <span id="logo" >
           
         </span> 
            <!--<img id="logo" src="" alt="Logo"/>--> 
        </a> 
        </div>
       

        <br/>
        

          <ul id="menu" style="margin-top:0px;">
          @permission('show-DashBoard')
            <li id="menu-home" ><a href="/public/chartadmin"><i class="fa fa-tachometer"></i><span>Dashboard</span></a></li>
            @endpermission
             @permission('show-DashBoard')

                <li id="menu-home" ><a href="/public/showadminpanel"><i class="fa fa-tachometer"></i><span>SuperPanel</span></a></li>
                             
            @endpermission

      




            @permission('show-user')
            <li><a href="#"><i class="glyphicon glyphicon-user" style="color: white;"></i><span>Users</span><span class="" style="float: right"></span></a>
              <ul>
              <li id="menu-arquivos" style="background-color:black;color:#fdbb30; "><a href="#"  style="color:#fdbb30;">Users</a></li>

                <li><a href="/public/users">Outdoor Promoters</a></li>
                <li><a href="/public/OfficeUser">Office Users</a></li>                
              </ul>
            </li>
            @endpermission
            @permission('show-Tasks')
            <li id="menu-comunicacao" ><a href="#"><i class="fa fa-users" style="color: white;"></i><span> Tasks </span><span class="" style="float: right"></span></a>
             



              <ul id="menu-comunicacao-sub" >
                 <li id="menu-arquivos" style="background-color:black;color:#3c8dbc; "><a href="#"  style="color:#fdbb30;">Office Promoters Tasks</a></li>
                <li id="menu-arquivos" ><a href="/public/task">Task Mangment</a></li>
                <li id="menu-arquivos" ><a href="/public/tasks/admintask">Task Per Promoters</a></li>
                 <li id="menu-arquivos" ><a href="/public/charttask">Office Promoters DashBoard</a></li>
              </ul>
            </li>
             @endpermission 
            @permission('show-privilege')
              <li><a href="#"><i class="fa fa-cog"></i><span>Preivliges</span></a>
               <ul>
                 <li id="menu-arquivos" style="background-color:black;color:#3c8dbc; "><a href="#"  style="color:#fdbb30;">Preivliges</a></li>

                <li><a href="/public/roles">Roles</a></li>           
              </ul>
              </li>


           @endpermission 

@permission('show-Contractors')

            <li id="menu-academico" ><a href="#"><i class="fa fa-users"></i><span>Contractors </span><span class="" style="float: right"></span></a>
              <ul id="menu-academico-sub" >
              <li id="menu-arquivos" style="background-color:black;color:#3c8dbc; "><a href="#"  style="color:#fdbb30;">Contractors</a></li>

                 <li id="menu-academico-boletim" ><a href="/public/Contractors">Contractors Details</a></li>
                <li id="menu-academico-avaliacoes" ><a href="/public/Reviews">Reviews</a></li>
                <li id="menu-academico-avaliacoes" ><a href="/public/Contractorschart">Contractors charts</a></li>               
              </ul>
            </li>
         
     
    <li><a href="#"><i class="fa fa-shopping-cart"></i><span> Segmentation</span></a>
        <ul id="menu-academico-sub" >
        <li id="menu-arquivos" style="background-color:black;color:#3c8dbc; "><a href="/public/types"  style="color:#fdbb30;">segmentation1</a></li>
          <li id="menu-arquivos" style="background-color:black;color:#3c8dbc; "><a href="/public/subtypes"  style="color:#fdbb30;">segmentation2</a></li>
            <li id="menu-arquivos" style="background-color:black;color:#3c8dbc; "><a href="/public/endtypes"  style="color:#fdbb30;">segmentation3</a></li>

               
              </ul>
            </li>

            <li><a href="#"><i class="fa fa-shopping-cart"></i><span> Retailers</span></a>
        <ul id="menu-academico-sub" >
        <li id="menu-arquivos" style="background-color:black;color:#3c8dbc; "><a href="/public/retailers"  style="color:#fdbb30;">Retailers</a></li>

          <li id="menu-arquivos" style="background-color:black;color:#3c8dbc; "><a href="/public/RetailersReview"  style="color:#fdbb30;">Retailers Review</a>

               
              </ul>
            </li>
            @endpermission 
             @permission('show-cemexproduct')
            <li><a href="#"><i class="fa fa-shopping-cart"></i><span> Products</span></a>
        <ul id="menu-academico-sub" >
        <li id="menu-arquivos" style="background-color:black;color:#3c8dbc; "><a href="#"  style="color:#fdbb30;">CEMEX Products</a></li>

        @permission('show-sale_products')
                 <li id="menu-academico-boletim" ><a href="/public/sale_products">Multi Product</a></li>
                  @endpermission 
                   @permission('show-products')
                <li id="menu-academico-avaliacoes" ><a href="/public/ShowAllProducts">CEMEX Products</a></li>
                   @endpermission             
              </ul>
            </li>
            @endpermission 
            @permission('show-Marketing_Activity')
            <li><a href="#"><i class="fa fa-line-chart"></i><span>Marketing </span><span class="" style="float: right"></span></a>
                <ul id="menu-academico-sub" >
                 <li id="menu-arquivos" style="background-color:black;color:#3c8dbc; "><a href="#"  style="color:#fdbb30;">Marketing Activity</a></li>
                 <li id="menu-academico-boletim" ><a href="/public/marktingactitvities">Activity</a></li>
                <li id="menu-academico-avaliacoes" ><a href="/public/cafes">Coffee Shops</a></li>
                   <li id="menu-academico-avaliacoes" ><a href="/public/cafemap">Signs&Coffee Shops Maps</a></li>
                   <li id="menu-academico-avaliacoes" ><a href="/public/tests">Winner Cntractors</a></li>
                   <li id="menu-academico-avaliacoes" ><a href="/public/activitychart">Charts</a></li>
                   <li id="menu-academico-avaliacoes" ><a href="/public/showbestcont">Show loyal Contractors</a></li>
              </ul>
            </li>
            @endpermission 
             @permission('show-Calendar')
             <li><a href="#"><i class="fa fa-calendar" style="color:white;" ></i><span>Calander</span><span class="" style="float: right"></span></a>
               <ul id="menu-academico-sub" >
               <li id="menu-arquivos" style="background-color:black;color:#3c8dbc; "><a href="#"  style="color:#fdbb30;">Calander</a></li>

                  <li id="menu-academico-avaliacoes" ><a href="/public/calenderMarketingActivity">Calander Activity</a></li>
                  <li id="menu-academico-boletim" ><a href="/public/calenderTasks">Office Promoter</a></li>
                 </ul>
             </li>
             @endpermission
             @permission('show-Compatittors')
             <li><a href="#"><i class="fa fa-users"></i><span>Competitors</span><span class="" style="float: right"></span></a>
              <ul id="menu-academico-sub" >
              <li id="menu-arquivos" style="background-color:black;color:#3c8dbc; "><a href="#"  style="color:#fdbb30;">Competitors</a></li>

                  <li id="menu-academico-avaliacoes" ><a href="/public/Companies">Companies</a></li>
                  <li id="menu-academico-boletim" ><a href="/public/selleroffice">Front Office</a></li>
                  <li id="menu-academico-boletim" ><a href="/public/activity">Activity</a></li>
                  <li id="menu-academico-boletim" ><a href="/public/Competitors">Signs</a></li>
                  <li id="menu-academico-boletim" ><a href="/public/products">Products</a></li>
                   <li id="menu-academico-boletim" ><a href="/public/CompanyWarehouses">Warehouses</a></li>
                   
                   <li id="menu-academico-boletim" ><a href="/public/Energy_sources">Energy Sources</a></li>
                   <li id="menu-academico-boletim" ><a href="/public/price">Price</a></li>
                   <li id="menu-academico-boletim" ><a href="/public/chartprices">Price report</a></li>
                   <li id="menu-academico-boletim" ><a href="/public/showactivitycharts">Activity Report</a></li>

                 </ul>
             </li>
             @endpermission 
             @permission('show-AdminPanel')
              <li><a href="#"><i class="fa fa-users"></i><span> Promoter </span><span class="" style="float: right"></span></a>
              <ul id="menu-academico-sub" >
               <li id="menu-arquivos" style="background-color:black;color:#3c8dbc; "><a href="#"  style="color:#fdbb30;">Field Promoter Trackers </a></li>

              @permission('show-Visits')
                  <li id="menu-academico-avaliacoes" ><a href="/public/gps">Promoter Visits</a></li>
                  <li id="menu-academico-boletim" ><a href="/public/Signcafe">Cafe Shop $Signs Visits</a></li>

                  <li id="menu-academico-boletim" ><a href="/public/Contractors_Program">Contractor Program</a></li>
                    @endpermission
              @permission('show-Map')
                  <li id="menu-academico-boletim" ><a href="/public/map/show">Show Map</a></li>
                  @endpermission 
              @permission('show-orders')
                  <li id="menu-academico-boletim" ><a href="/public/orders">Orders</a></li>
                  @endpermission
                 
                @permission('show-ChartUpper')
                   <li id="menu-academico-boletim" ><a href="/public/ChartUpper">Sold Product</a></li>
                  @endpermission
                   @permission('show-ChartPrmotorcount')
                   <li id="menu-academico-boletim" ><a href="/public/ChartPrmotorcount">Contractor Covered Performance</a></li>
                    @endpermission 
                @permission('show-ChartQuanyity')
                   <li id="menu-academico-boletim" ><a href="/public/ChartQuanyity">% Partcipated Contractor</a></li>
                     @endpermission!

                 </ul>
             </li>
              @endpermission
              @permission('show-Inventory')
               <li><a href="#"><i class="fa fa-shopping-cart"></i><span>المخزن</span><span class="" style="float: right"></span></a>
              <ul id="menu-academico-sub" >
               <li id="menu-arquivos" style="background-color:black;color:#3c8dbc; "><a href="#"  style="color:#fdbb30;">المخزن</a></li>

                  <li id="menu-academico-avaliacoes" ><a href="/public/warehouse">المخازن</a></li>
                  <li id="menu-academico-boletim" ><a href="/public/categories">الاقسام</a></li>
                  <li id="menu-academico-boletim" ><a href="/public/Brands">الماركات</a></li>
                  <li id="menu-academico-boletim" ><a href="/public/Items">الاصناف</a></li>
                  <li id="menu-academico-boletim" ><a href="/public/purchase">اذن استلام</a></li>
                   <li id="menu-academico-boletim" ><a href="/public/BillOut">اذن صرف</a></li>
          
                 </ul>
             </li>
              @endpermission 
 
@permission('show-Inventory_Reports')
              <li><a href="#"><i class="fa fa-bar-chart" style="color:white;"></i><span>تقارير المخزن</span><span class="" style="float: right"></span></a>
              <ul id="menu-academico-sub" >
                  <li id="menu-arquivos" style="background-color:black;color:#3c8dbc; "><a href="#"  style="color:#fdbb30;">تقارير المخزن</a></li>

                  <li id="menu-academico-avaliacoes" ><a href="/public/PurchaseIn">تقرير ايصالات استلام</a></li>
                  <li id="menu-academico-boletim" ><a href="/public/Distrbuted">تقرير ايصالات صرف</a></li>
                  <li id="menu-academico-boletim" ><a href="/public/InventoryStock">جرد المخزن</a></li>
                  <li id="menu-academico-boletim" ><a href="/public/MinLimtItems">اصناف قاربت للانتهاء</a></li>
                 
          
                 </ul>
             </li>
             @endpermission 
                           @permission('Show-HR')

              <li><a href="#"><i class="fa fa-money custom" style="color:white"></i><span>KPI</span><span class="" style="float: right"></span></a>
              <ul id="menu-academico-sub" >
               <li id="menu-arquivos" style="background-color:black;color:#fdbb30; "><a href="#"  style="color:#fdbb30;">KPI</a></li>

                  <li id="menu-academico-avaliacoes" ><a href="/public/KPI">KPI</a></li>
                  <li id="menu-academico-boletim" ><a href="/public/KPIUsers">KPI Users</a></li>
                 
           <li id="menu-academico-boletim" ><a href="/public/userkpi">KPI Salary</a></li>
                 </ul>
             </li>

              <li><a href="#"><i class="fa fa-shopping-cart"></i><span>HR System</span><span class="" style="float: right"></span></a>
             
             </li>
             @endpermission 
             @permission('show-Places')
              <li><a href="#"><i class="fa fa-map-marker"  style="color:white"></i><span>Location</span><span class="" style="float: right"></span></a>
              <ul id="menu-academico-sub" >
              <li id="menu-arquivos" style="background-color:black;color:#fdbb30; "><a href="#"  style="color:#fdbb30;">Location</a></li>

                  <li id="menu-academico-avaliacoes" ><a href="/public/Governments">Government</a></li>
                  <li id="menu-academico-boletim" ><a href="/public/Districts">Districts</a></li>
                 
          
                 </ul>
             </li>
             @endpermission
          </ul>
       <!--  </div> -->
   </div>
  
  <!-- <div class="clearfix"> </div> -->
</div>
<!--slide bar menu end here-->
    </section>
    <!-- /.sidebar -->
<!--   </aside> -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper  test" style="min-height:1050px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>
    <!-- Main content -->
    <section class="content " id= "content1" style="margin-top:40px ">
     @yield('content')
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  

  <!-- Control Sidebar -->
 
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 2.2.3 -->
<div class="modal fade"  id="myModal" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
    <div class="modal-header" style="background-color:#425B90;">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 id="myModalLabel"  style="color:#fff;">Modal header</h4>
    </div>
    <div class="modal-body" id="modalbode">
        <p>One fine body…</p>
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">OK</button>
    </div>
     </div>
      </div>
</div>

<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
      
    </div>
    <!-- Default to the left -->
    <strong style="margin-left: 50px">   Copyright &copy; 2017 <a href="#">Company</a>.</strong> All rights reserved.
</footer>
<!-- script-for sticky-nav -->
   
    <!-- /script-for sticky-nav -->
<!--slide bar menu end here-->
<script>
var toggle = true;
            
$(".sidebar-icon").click(function() {                
  if (toggle)
  {
    
    $(".page-container").addClass("sidebar-collapsed").removeClass("sidebar-collapsed-back");
     $(".test").removeClass("content-wrapper");
    $(".test").addClass("contentsection");
    $("#menu span").css({"position":"absolute"});
  }
  else
  {
    $(".page-container").removeClass("sidebar-collapsed").addClass("sidebar-collapsed-back");
     $(".test").addClass("content-wrapper");
    $(".test").removeClass("contentsection");
    setTimeout(function() {
      $("#menu span").css({"position":"relative"});
    }, 400);
  }               
                toggle = !toggle;
            });
</script>
<!--scrolling js-->
   <!--  <script src="/assets/MenuFolder/js/jquery.nicescroll.js"></script>
    <script src="/assets/MenuFolder/js/scripts.js"></script> -->
    <!--//scrolling js-->
<!-- <script src="/assets/MenuFolder/js/bootstrap.js"> </script> -->

<script type="text/javascript">
  $(document).ready(function(){

 setInterval(function()
 {

      $.ajax({
      url: "/public/NotificationStock",
      type: "get",
      success: function(data){
    for (var ke in data) {
        if (data.hasOwnProperty(ke)) {
        $('#Notify').text(data[ke].CountNotification);
        $('#headerNotification').text('لديك اصناف قاربت علي الانتهاء ');
         }
       }
     }
    });
},5000);
 //notifications

//Approval Items



setInterval( function()
 {

       $.getJSON("/public/latest", function(data) {

       $count=Object.keys(data).length;

       $('#noti_Counter')
            .css({ opacity: 0 })
            .text($count)              // ADD DYNAMIC VALUE (YOU CAN EXTRACT DATA FROM DATABASE OR XML).
            .css({ top: '-10px' })
            .animate({ top: '-2px', opacity: 1 }, 500);

      

      $("#pnotifications").empty();

     var $notifications =$("#pnotifications");
   
     
     for (var i = 0; i < $count; i++) {
 

                
       $.each(data[i], function(index, value) {
       
        $notifications.append('<div class="item">'
       +' <span class="badge badge-important">'+index+'</span><a> Pormoter  '+value+'  want cement </a>'
       +'<span class="close" data-dismiss="alert" aria-label="Close" id="' +value+'" aria-hidden="true">&times;</span>'
 
       +'</div>');

         });
     }
      });

},5000);

 $('#noti_Button').click(function () {

            // TOGGLE (SHOW OR HIDE) NOTIFICATION WINDOW.
            $('#notifications').fadeToggle('fast', 'linear', function () {
                if ($('#notifications').is(':hidden')) {
                    $('#bell').css('color', '#2E467C');
                }
                else $('#bell').css('color', '#fff');        // CHANGE BACKGROUND COLOR OF THE BUTTON.
            });

                         // HIDE THE COUNTER.

            return false;
        });

        // HIDE NOTIFICATIONS WHEN CLICKED ANYWHERE ON THE PAGE.
        $(document).click(function () {
            $('#notifications').hide();


            // CHECK IF NOTIFICATION COUNTER IS HIDDEN.
            if ($('#noti_Counter').is(':hidden')) {
                // CHANGE BACKGROUND COLOR OF THE BUTTON.
                $('#bell').css('color', '#2E467C');
            }
        });

        $('#notifications').click(function () {
            return false;       // DO NOTHING WHEN CONTAINER IS CLICKED.
        });

   

        $("#notifications ").on("click",'span',function(event)
 {

      $id=event.target.id; 
      console.log($id);
     event.preventDefault();
     $(this).parent().remove();

        
  
  

 $.getJSON("cemexmarketingsystem/public/updatelatest/"+$id, function(data) {
  console.log(data);

      $count=Object.keys(data).length;

      console.log($count); 

           $("#myModalLabel").empty();


          $("#modalbode").empty();


       $name='';

       for (var i = 0; i < $count; i++) 
       {


  
      
 
        $name=data[i]['Name'];
          
        $("#modalbode").append('<div class="item ">'
        +'<span>Contractor Code '+data[i]['ContractorCode']+' Want <li>'+data[i]['Products']+'</li> In Date '+data[i]['Date']+'</span>'
        +'</div>');


   

     }

        $("#myModalLabel").append('Pormoter '+$name);

      $('#myModal').modal('show');
             





});

 });
var height = $('bod').height();
console.log(height);
$('.sidebar-menu').height(height);

  });
      // var navoffeset=$(".header-main").offset().top;
    //    $(window).scroll(function(){
    //     var scrollpos=$(window).scrollTop(); 
    //     if(scrollpos >=navoffeset){
    //       $(".header-main").addClass("fixed");
    //     }else{
    //       $(".header-main").removeClass("fixed");
    //     }
    //    });
</script>

</body>

</html>