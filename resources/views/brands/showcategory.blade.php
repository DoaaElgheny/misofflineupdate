@extends('master')
@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}" />

<section class="panel panel-primary">
<div class="panel-body">

  <h1><i class='fa fa-tasks'></i> Brands </h1>


<table class="table table-hover table-bordered  dt-responsive nowrap display Brands" cellspacing="0" width="100%">
  <thead>
              <th>No</th>
              <th>Name</th>
            <th>Active</th>
 
              <th>Process</th>          
  </thead>
    <tfoot>
        
                 <th></th>
              <th>Name</th>
            <th>Active</th>

              <th>Process</th> 
                    
                  </tfoot>
               

<tbody style="text-align:center;">
  <?php $i=1;?>
  @foreach($Category->getCategorybrands  as $brand)
    <tr>
    
     <td>{{$i++}}</td> 
      <td ><a  class="testEdit" data-type="text"  data-value="{{$brand->Name}}" data-name="Name"  data-pk="{{ $brand->id }}">{{$brand->Name}}</a></td>
 <td>
          @if($brand->Active==1)
         
      <input type="checkbox" id= "check" class="check" value='{{$brand->Active}} ' onclick="ActiveCheck(this,{{ $brand->id }})" checked  />
    @else 
         
  <input type="checkbox" id= "check" class="check" value='{{$brand->Active}}' onclick="ActiveCheck(this,{{ $brand->id }})" />
</td>
@endif
  <td>
     <a href="/cemexmarketingsystem/public/Brands/destroy/{{$brand->id}}"class="btn btn-default btn-sm" ><span class="glyphicon glyphicon-trash"></span>
       </a>

</td>

   </tr>
     @endforeach
</tbody>
</table>
</div>
    </section>
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>


  <script type="text/javascript">

var editor;

  $(document).ready(function(){
     $('#profileForm')
        .formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                'Name[]': {
                    err: '#messageContainer',
                    validators: {
                            remote: {
                        url: '/cemexmarketingsystem/public/checknamebrand',
                        data:function(validator, $field, value)
                        {return{Name:validator.getFieldElements('Name[]').val()};},
                        message: 'The Brand name is not available',
                        type: 'GET'
                    },
                        callback: {
                            callback: function(value, validator, $field) {
                                var $Names          = validator.getFieldElements('Name[]'),
                                    numEmails        = $Names.length,
                                    notEmptyCount    = 0,
                                    obj              = {},
                                    duplicateRemoved = [];

                                for (var i = 0; i < numEmails; i++) {
                                    var v = $Names.eq(i).val();
                                    if (v !== '') {
                                        obj[v] = 0;
                                        notEmptyCount++;
                                    }
                                }

                                for (i in obj) {
                                    duplicateRemoved.push(obj[i]);
                                }

                                if (duplicateRemoved.length === 0) {
                                    return {
                                        valid: false,
                                        message: 'You must fill at least one Name'
                                    };
                                } else if (duplicateRemoved.length !== notEmptyCount) {
                                    return {
                                        valid: false,
                                        message: 'The Name must be unique'
                                    };
                                }

                                validator.updateStatus('Name[]', validator.STATUS_VALID, 'callback');
                                return true;
                            }
                        }
                    }
                }
            } ,submitHandler: function(form) {
        form.submit();
    }
        })
        .on('success.form.fv', function(e) {
   // e.preventDefault();
    var $form = $(e.target);

    // Enable the submit button
   
    $form.formValidation('disableSubmitButtons', false);
})
        // Add button click handler
        .on('click', '.addButton', function() {
            var $template = $('#NameTemplate'),
                $clone    = $template
                                .clone()
                                .removeClass('hide')
                                .removeAttr('id')
                                .insertBefore($template),
                $Name    = $clone.find('[name="Name[]"]');

            // Add new field
            $('#profileForm').formValidation('addField', $Name);
        })

        // Remove button click handler
        .on('click', '.removeButton', function() {
            var $row   = $(this).closest('.form-group'),
                $Name = $row.find('[name="Name[]"]');

            // Remove element containing the Name
            $row.remove();

            // Remove field
            $('#profileForm').formValidation('removeField', $Name);
        });
     var table= $('.Brands').DataTable({
  
    select:true,
    responsive: true,
    "order":[[0,"asc"]],
    'searchable':true,
    "scrollCollapse":true,
    "paging":true,
    "pagingType": "simple",
      dom: 'lBfrtip',
        buttons: [
           
            
            { extend: 'excel', className: 'btn btn-primary dtb' }
            
            
        ],

fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
$.fn.editable.defaults.send = "always";

$.fn.editable.defaults.mode = 'inline';

$('.testEdit').editable({

      validate: function(value) {
        name=$(this).editable().data('name');
        if(name=="Name")
        {
                if($.trim(value) == '') {
                    return 'Value is required.';
                  }
                    }
      
        },

    placement: 'right',
    url:'{{URL::to("/")}}/updateBrands',
  
     ajaxOptions: {
     type: 'get',
    sourceCache: 'false',
     dataType: 'json'

   },

       params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');

            // console.log(params);
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'This Data Already Exist,Enter Correct Data.';
            } else {
                return response.responseText;
            }
        }


});

}
});
   $('.Brands tfoot th').each(function () {
      var title = $('.Brands thead th').eq($(this).index()).text();       
     if($(this).index()>=1 && $(this).index()<=6)
            {

           $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
}
        

    });
  table.columns().every( function () {
  var that = this;
 $(this.footer()).find('input').on('keyup change', function () {
          that.search(this.value).draw();
            if (that.search(this.value) ) {
               that.search(this.value).draw();
           }
        });
      
    });

 });

function ActiveCheck(e,id)
{
  var act;
  if(e.value==1)
    act=0;
  else
    act=1;

  $.ajax({
      url: "/cemexmarketingsystem/public/updateactiveBrand",
        data:{id:id,active:act},
      type: "get",
      success: function(data){
      }
    }); 
}

  </script>
@stop
