@extends('masternew')
@section('content')
<!DOCTYPE html>
<section class="panel-body">
 <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Front Office</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                     
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

 


  
 
<!-- ENDModal -->

<table id="selleroffice" style="width:100%" class="table table-hover table-bordered dt-responsive nowrap display selleroffice" cellspacing="0">
<thead>
      <tr>
      <th>No</th>
   <th>Location</th>
      <th>Telephone</th>
       <th>Government</th>
      <th>District</th>
       <th>Adress</th>
      
      <th>Company</th>
            <th>Process</th>
          
    </tr>
  </thead>
  <tfoot>
      <th></th>
 <th>Location</th>
      <th>Telephone</th>
       <th>Government</th>
      <th>District</th>
       <th>Adress</th>
      
      <th>Company</th>
            <th>Process</th>

  </tfoot>
  <tbody id="tbody">
    <?php $i=1;  ?>

    @foreach($selleroffice as $selleroffice)

      <tr> 

        <td>{{$i++}}</td>
        <td>{{$selleroffice->Location}}</td>
        <td>{{$selleroffice->Telephone}}</td>
        <td>{{$selleroffice->Government}}</td>
        <td>{{$selleroffice->District}}</td>
        <td>{{$selleroffice->Address}}</td>      
        <td >{{$selleroffice->getcompany->Name}}</td>
       <td>
       <a href="/offlineneew/public/prodoffdelete/{{$selleroffice->id}}/{{$id}}"class="btn btn-default btn-sm" ><span class="glyphicon glyphicon-trash"></span>  </a>
       </td>       
             
      </tr>    
    @endforeach 

    

  </tbody>

</table>
  </div>
                </div>
              </div>  
    </section>
@section('other_scripts')
<script type="text/javascript">

  $(document).ready(function(){
   var table= $('.selleroffice').DataTable({ 
   
    responsive: true,
    "order":[[0,"asc"]],
    'searchable':true,
    "scrollCollapse":true,
    "paging":true,
    "pagingType": "simple",
      dom: 'lBfrtip',
        buttons: [
           
            
            { extend: 'excel', className: 'btn btn-primary dtb' }
            
            
        ],

});


       




$('.product tfoot th').each(function () {

    var title = $('.product thead th').eq($(this).index()).text();

if($(this).index()>=1 && $(this).index()<=10)

        {

           $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
          }
});

table.columns().every( function () {

    var that = this;
  $(this.footer()).find('input').on('keyup change', function () {

          that.search(this.value).draw();

        if (that.search(this.value) ) {

            that.search(this.value).draw();
        }
    });     
    });

//validation
$.validator.addMethod("alpha", function(value, element) {
    return this.optional(element) || /^[a-zA-Z\u0600-\u06FF,-][\sa-zA-Z\u0600-\u06FF,-,'.']/.test(value);
 });




 });


</script>




@stop
@stop