@extends('masternew')
@section('content')
<!DOCTYPE html>

<style type="text/css">

.error{
  color: #a51c1c;
}
</style>
<style type="text/css">
  #uploadFrontFile{
    display: none;
}

  #uploadBackFile{
    display: none;
}
.custom-file-upload {
    border: 1px solid #ccc;
    display: inline-block;
    padding: 6px 12px;
    cursor: pointer;
}

</style>
<section class="panel-body">
 <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                  <h2><i class="fa fa-lightbulb-o"></i>Product</h2> 
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                     
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

 


   <a type="button"  class="btn btn-primary" data-toggle="modal" data-target="#myModal1" style="margin-bottom: 20px;"> Add New Product </a>
  
     <!-- model -->
 
<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
     <div class="modal-content">
    
          <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                 <h4 class="modal-title span7 text-center" id="myModalLabel"><span class="title">Add New Product</span></h4>
          </div>
    <div class="modal-body">
        <div class="tabbable"> <!-- Only required for left/right tabs -->
        <ul class="nav nav-tabs">
  

        
        </ul>
        <div class="tab-content">
        <div class="tab-pane active" id="tab1">
              <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                 
                  <div class="x_content">
                    <br />
{!!Form::open(['action'=>"ProductController@store",'method'=>'post','id'=>'profileForm','files'=>'true','class'=>'form-horizontal form-label-left  '])!!}



<fieldset style="    padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;">
  <legend style="width: fit-content;border: 0px;">Main Data:</legend>
   <div class="form-group">


      <div class="control-label col-md-6 col-sm-6 col-xs-12">

                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Cement Name
                        </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                           <input type="text" class="form-control" id="cement_name" name="cement_name" placeholder="Cement Name">
                        </div>
                        </div>

                         <div class="control-label col-md-6 col-sm-6 col-xs-12">

                        <label class="control-label col-md-3 col-sm-3 col-xs-12">English Name
                        </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                        <input type="text" class="form-control" id="Enname" name="Enname" placeholder="English Name">
                        </div>
                        </div>
             

   
                     

                      </div>


     
 <div class="form-group">



<div class="control-label col-md-6 col-sm-6 col-xs-12">

                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Comapny
                        </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                       {!!Form::select('company', ([null => 'Select Company name'] + $company->toArray()) ,null,['class'=>'form-control chosen-select company','id' => 'company'])!!}
                        </div>
                        </div>
   <div class="control-label col-md-6 col-sm-6 col-xs-12">

                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Code
                        </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                       <input type="number" class="form-control" id="Address" name="Code" placeholder="enter Code" required>
                        </div>
                        </div>


   
 </div>

 

                 
</fieldset>
        


             <fieldset style="    padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;">
  <legend style="width: fit-content;border: 0px;">Other Data:</legend>
    



  <div class="form-group">


      <div class="control-label col-md-6 col-sm-6 col-xs-12">

                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Weight
                        </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                
<input type="number" step="any" class="form-control span12" id="weight" name="weight" placeholder="weight" required>

                        </div>
                        </div>

                         <div class="control-label col-md-6 col-sm-6 col-xs-12">

                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Rank
                        </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <input type="number"  step="any" class="form-control" id="rank" name="rank" placeholder="rank">
                        </div>
                        </div>
             

   
                     

                      </div>


     
 <div class="form-group">

   <div class="control-label col-md-6 col-sm-6 col-xs-12">

                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Specifications
                        </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
          <input type="text" class="form-control" id="specifications" name="specifications" placeholder="specifications">
                        </div>
                        </div>


                        <div class="control-label col-md-6 col-sm-6 col-xs-12">

                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Guidlines
                        </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
    <input type="text" class="form-control" id="guidlines" name="guidlines" placeholder="guidlines">
                        </div>
                        </div>
   
 </div>
  <div class="form-group">

   <div class="control-label col-md-6 col-sm-6 col-xs-12">

                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Security
                        </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                    <input type="text" class="form-control" id="security" name="security" placeholder="Security">
         
                        </div>
                        </div>


                        <div class="control-label col-md-6 col-sm-6 col-xs-12">

                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Others
                        </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
       <input type="text" class="form-control" id="others" name="others" placeholder="others">
                        </div>
                        </div>
   
 </div>

   <div class="form-group">

   <div class="control-label col-md-6 col-sm-6 col-xs-12">

                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Packing Date
                        </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                  <input type="text" class="form-control has-feedback-left" id="packing_date" name="packing_date" >
                  <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
         
                        </div>
                        </div>


                        <div class="control-label col-md-6 col-sm-6 col-xs-12">

                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Producatin Date
                        </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
       <input type="text" class="form-control has-feedback-left" id="producation_date" name="producation_date" >
       <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                        </div>
                        </div>
   
 </div>

                 
</fieldset>
        

             

             <fieldset style="    padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;">
  <legend style="width: fit-content;border: 0px;">Product Image:</legend>

   <div class="form-group">

   <div class="control-label col-md-6 col-sm-6 col-xs-12">

                  <!--       <label class="control-label col-md-3 col-sm-3 col-xs-12">Front Image
                        </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                 {!! Form::file('frontimg', ['class' => 'form-control','id'=>'uploadFrontFile','name'=>'frontimg']) !!}


         
                        </div>
                        </div> -->

<label for="uploadFrontFile" class="custom-file-upload" >
    <i class="fa fa-cloud-upload"></i> Front Image
</label>
<input id="uploadFrontFile"  name="frontimg" type="file"/>

</div>
                        <div class="control-label col-md-6 col-sm-6 col-xs-12">

                     <!--    <label class="control-label col-md-3 col-sm-3 col-xs-12">Back Image
                        </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
       {!! Form::file('backimg', ['class' => 'form-control','id'=>'uploadBackFile','name'=>'backimg']) !!}


                        </div> -->


                        <label for="uploadBackFile" class="custom-file-upload" >
                          <i class="fa fa-cloud-upload"></i> Back Image
                      </label>
                      <input id="uploadBackFile"  name="backimg" type="file"/>
                                              </div>
                         
                       </div>

  </fieldset>       

                        
                       


                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                          <button type="button" class="btn btn-primary " data-dismiss="modal">Cancel</button>
                         <button class="btn btn-primary" type="reset">Reset</button>
                          <button type="submit"  name="login_form_submit" class="btn btn-success" value="Save">Submit</button>
                        </div>
                      </div>

                   
        {!!Form::close() !!}                
        </div>
                </div>
                   </div>
                </div>
        </div>
        </div>
        </div>

    </div>
 

         </div>
     </div>
  </div>
<!-- ENDModal -->
 
     @if(Session::has('error'))
           
    <div class="alert alert-info" style="text-align:center;" >
         <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
                  <strong>Whoops!</strong> {{ Session::get('error') }}<br><br>

    
    </div>
    @endif


 
 <table id="product"  class="table table-hover table-bordered  dt-responsive nowrap display product" cellspacing="0" width="100%">
        
<thead>
      <tr>
       <th>No</th>
         <th>Cement Name</th>
          <th>Code</th> 
         <!-- <th> English Name</th>  -->
       <th> Type</th> 
        <th>Cement Weight</th>
       <th>Cement Rank</th>
       <th>Cement Specifications</th>
       <th>Cement Producation Date</th>
       <th>Cement Packing Date</th>
       <th>Cement Guidlines</th>
       <th>Cement Security</th>
       <th>Cement Others</th>
       <th>Cement Front Image</th>
       <th>Cement Back image</th>
       <th>Comapny Name</th>
       <th>Show All Details</th>
       <th>Show Photo</th>
       <th>Show All Front Offices</th>
       <th> Upload</th>

       <th>Download</th>

       <th>Open</th>

       <th>Process</th>           


        
    </tr>
  </thead>

  <tbody id="tbody">
    <?php $i=1;  ?>

    @foreach($product as $product)
      <tr> 

        <td>{{$i++}}</td>
      <td>{{$product->Cement_Name}}</td>
      <td>{{$product->Code}}</td>
      <!-- <td>{{$product->Enname}}</td> -->
      <td>{{$product->Type}}</td>
      <td>{{$product->Weight}}</td>
      <td>{{$product->Rank}}</td>
      <td>{{$product->Specifications}}</td>
      <td>{{$product->Producation_date}}</td>
      <td>{{$product->Packing_date}}</td>
      <td>{{$product->Guidlines}}</td>
      <td>{{$product->Security}}</td>
      <td>{{$product->Others}}</td>
       <td>
        @if($product->Frontimg!==null)
        <!-- <img src="./product/frontimg/{{$product->id}}" style="width:60px;hight:60px;"> -->
        <img src="{{$product->Frontimg}}" style="width:60px;hight:60px;">
        @endif
      </td>         
      <td>
        @if($product->Backimg!==null)
      <!--   <img src="./product/backimg/{{$product->id}}" style="width:60px;hight:60px;"> -->
        <img src="{{$product->Backimg}}" style="width:60px;hight:60px;">
        @endif
      </td>    
<td>{{$product->getcompanyproduct->Name}}</td>
    <td> <a href="/offlineneew/public/showproductdetails/{{$product->id}}" class="btn btn-default btn-sm" ><span class="glyphicon glyphicon-plus"></span>  </a></td> 
<td> <a href="/offlineneew/public/showproductphoto/{{$product->id}}" class="btn btn-default btn-sm" ><span class="glyphicon glyphicon-plus"></span>  </a></td> 
<td> <a href="/offlineneew/public/showprodoff/{{$product->id}}" class="btn btn-default btn-sm" ><span class="glyphicon glyphicon-plus"></span>  </a></td> 



<td >

<a href="/offlineneew/public/ShowFilesdatasheet/{{$product->id}}" class="btn btn-default btn-sm" ><span class="glyphicon glyphicon-open"></span></a>



</td>
<td >
<a href="/offlineneew/public/getdatasheet/{{$product->id}}" class="btn btn-default btn-sm" ><span class="glyphicon glyphicon-save"></span></a>


</td>
<td>
<a href="/offlineneew/public/ViewPDF/{{$product->id}}" class="btn btn-default btn-sm" ><span class="glyphicon glyphicon-folder-open"></span></a>

</td>

             
      <td>
            <small>Edit </small> <a href="/offlineneew/public/products/{{$product->id}}/edit" class="btn btn-default btn-sm" ><span class="glyphicon glyphicon-edit"></span>  </a>

        <small>Delete </small> <a href="/offlineneew/public/products/destroy/{{$product->id}}" class="btn btn-default btn-sm" ><span class="glyphicon glyphicon-trash"></span>  </a>
      </td>
     @endforeach      

  </tbody>

  <tfoot>
      <th></th>
       <th>Cement Name</th> 
        <th>Code</th> 
     <!--   <th> English Name</th>  -->
       <th> Type</th> 
        <th>Cement Weight</th>
       <th>Cement Rank</th>
       <th>Cement Specifications</th>
       <th>Cement Producation Date</th>
        <th> Cement Packing Date</th>
       <th>Cement Guidlines</th>
      <th>Cement Security</th>
      <th>Cement Others</th>
      <th>Cement Front Image</th>
      <th>Cement Back image</th>
      <th>Comapny Name</th> 
      <th></th>
      <th></th>
       <th></th>
      <th></th> 
  </tfoot>

</table>
{!!Form::open(['action'=>'ProductController@importproduct','method' => 'post','files'=>true])!!}
  <label for="file-upload" class="custom-file-upload">
    <i class="fa fa-cloud-upload"></i> Select File
</label>
<input id="file-upload"  name="file" type="file"/>

    <input type="submit" name="submit" value="Import File" class="btn btn-primary" />  
{!!Form::close()!!}
  </div>
                </div>
              </div>  
    </section>
@section('other_scripts')
<script type="text/javascript">

  $(document).ready(function(){
      $('#packing_date').daterangepicker({
        singleDatePicker: true,
        singleClasses: "picker_4",
         locale: {
            format: 'YYYY-MM-DD'
        }
      }, function(start) {
        console.log(start.toISOString(), 'Hadeel');
      });
       $('#producation_date').daterangepicker({
        singleDatePicker: true,
        singleClasses: "picker_4",
         locale: {
            format: 'YYYY-MM-DD'
        }
      }, function(start) {
        console.log(start.toISOString(), 'Hadeel');
      });
//      $('#profileForm')
//             .find('[name="company"]')
//             .chosen({
//                 width: '100%',
//                 inherit_select_classes: true
//             })
//             // Revalidate the color when it is changed
//             .change(function(e) {
//                 $('#profileForm').formValidation('revalidateField', 'company');
//             })
//             .end()
//         .formValidation({
//             framework: 'bootstrap',
//             icon: {
//                 valid: 'glyphicon glyphicon-ok',
//                 invalid: 'glyphicon glyphicon-remove',
//                 validating: 'glyphicon glyphicon-refresh'
//             },
//             fields: {
//                company :{                           
//                     validators: {
//                         callback: {
//                             message: 'Please choose Government',
//                             callback: function(value, validator, $field) {
//                               console.log("shhhhhhhhhhhhhhhh");
//                                 // Get the selected options
//                                 var options = validator.getFieldElements('company').val();
                              
//                                 return (options !== '' );
//                             }
//                         }
//                     }
//                 },

             
                
//             } ,submitHandler: function(form) {
//         form.submit();
//     }
//         })
//         .on('success.form.fv', function(e) {
//    // e.preventDefault();
//     var $form = $(e.target);

//     // Enable the submit button
   
//     $form.formValidation('disableSubmitButtons', false);
// });
$('#profileForm')
        .find('[name="company"]')
            .chosen({
                width: '100%',
                inherit_select_classes: true
            })
            // Revalidate the color when it is changed
            .change(function(e) {
                $('#profileForm').formValidation('revalidateField', 'company');
            })
            .end()
        .formValidation({
            framework: 'bootstrap',
            excluded: ':disabled',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                company: {
                    validators: {
                        callback: {
                            message: 'Please choose 2-4 color you like most',
                            callback: function(value, validator, $field) {
                                // Get the selected options
                                var options = validator.getFieldElements('company').val();
                                console.log(options);
                                return (options != "");
                            }
                        }
                    }
                },
                                'cement_name': {
                  
                    validators: {
                                   remote: {
                        url: '/offlineneew/public/checkProductname',
                        message: 'The cement_name  is not available',
                        type: 'GET'
                    },
                  
                  notEmpty:{
                    message:"the cement_name is required"
                  },
                    regexp: {
                        regexp: /^[\u0600-\u06FF\s]+$/,
                        message: 'The cement_name can only consist of alphabetical and spaces'
                    }
                    }
                },
                     'Enname': {
                  
                    validators: {
                                      remote: {
                        url: '/offlineneew/public/checkproductenglishname',
                        message: 'The Enname  is not available',
                        type: 'GET'
                    },
                 
                  notEmpty:{
                    message:"the english name is required"
                  },
                    regexp: {
                        regexp: /^[a-zA-Z\s]+$/,
                        message: 'The english name can only consist of alphabetical and spaces'
                    }
                    }
                }, 'Code': {
                    validators: {
                         remote: {
                        url: '/offlineneew/public/checkProductCode',
    
                        message:'product Already Exist',
                        type: 'GET'
                    } 
                    }
                },
                     'Type': {
                  
                    validators: {
                 
               
                    regexp: {
                        regexp: /^[\u0600-\u06FFa-zA-Z\s]+$/,
                        message: 'The Type  can only consist of alphabetical and spaces'
                    }
                    }
                },
                     'weight': {
                  
                    validators: {
                 
                  notEmpty:{
                    message:"the Weight  is required"
                  },
                    numric: {
                        message: 'The Weight must be number'
                    }
                    }
                },
                     'rank': {
                  
                    validators: {
                 
                  notEmpty:{
                    message:"the Rank  is required"
                  },
                    numric: {
                        message: 'The Rank must be number'
                    }
                    }
                },
                     'specifications': {
                  
                    validators: {
                
                    regexp: {
                        regexp:/^[\u0600-\u06FFa-zA-Z\s]+$/,
                        message: 'The Specifications can only consist of alphabetical and spaces'
                    }
                    }
                },
                  
                     'guidlines': {
                  
                    validators: {
                 
               
                    regexp: {
                        regexp: /^[\u0600-\u06FFa-zA-Z\s]+$/,
                        message: 'The guidlines  can only consist of alphabetical and spaces'
                    }

                    }
                }
                ,
                     'security': {
                  
                    validators: {
                 
               
                    regexp: {
                        regexp: /^[\u0600-\u06FFa-zA-Z\s]+$/,
                        message: 'The Security  can only consist of alphabetical and spaces'
                    }

                    }
                }
                ,
                     'others': {
                  
                    validators: {
                 
              
                    regexp: {
                        regexp: /^[\u0600-\u06FFa-zA-Z\s]+$/,
                        message: 'The Others  can only consist of alphabetical and spaces'
                    }

                    }
                }
                 
            }
        });
    var table= $('.product').DataTable({ 
   
    responsive: true,
    "order":[[0,"asc"]],
    'searchable':true,
    "scrollCollapse":true,
    "paging":true,
    "pagingType": "simple",
      dom: 'lBfrtip',
        buttons: [
           
            
            { extend: 'excel', className: 'btn btn-primary dtb' }
            
            
        ],

});


       




$('.product tfoot th').each(function () {

    var title = $('.product thead th').eq($(this).index()).text();

if($(this).index()>=1 && $(this).index()<=10)

        {

           $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
          }
});

table.columns().every( function () {

    var that = this;
  $(this.footer()).find('input').on('keyup change', function () {

          that.search(this.value).draw();

        if (that.search(this.value) ) {

            that.search(this.value).draw();
        }
    });     
    });

//validation
$.validator.addMethod("alpha", function(value, element) {
    return this.optional(element) || /^[a-zA-Z\u0600-\u06FF,-][\sa-zA-Z\u0600-\u06FF,-,'.']/.test(value);
 });



// $(".company").chosen({
//                                    width: '100%',
//                                    no_results_text:"No Result Found !!..",
//                                    allow_single_deselect: true, 
//                                    search_contains:true, });

//                                  $(".company").trigger("chosen:updated");


 });

</script>


@stop
@stop