@extends('masternew')


@section('content')

<style type="text/css">

.error{
  color: #a51c1c;
}
</style>
<style type="text/css">
  #uploadFrontFile{
    display: none;
}

  #uploadBackFile{
    display: none;
}
.custom-file-upload {
    border: 1px solid #ccc;
    display: inline-block;
    padding: 6px 12px;
    cursor: pointer;
}

</style>
<div class="row">

<div class="col-md-2 col-xs-12 ">
 </div>

              <div class="col-md-8 col-xs-12 ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Edit Product</h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
    {!! Form::open(['action' => 'ProductController@UpdateProductall','class' => 'form-horizontal form-label-left','method'=>'post','id'=>'profileForm','files'=>'true']) !!}    

                    
  <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" id="id" name="id" value="{{ $product['id'] }}">

       <fieldset style="    padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;">
  <legend style="width: fit-content;border: 0px;">Main Data:</legend>

                     <div class="form-group">


      <div class="control-label col-md-6 col-sm-6 col-xs-12">

                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Cement Name
                        </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <input type="text" class="form-control" id="cement_name" value="{{ $product['Cement_Name'] }}" name="cement_name" placeholder="Cement Name">
                        </div>
                        </div>

                         <div class="control-label col-md-6 col-sm-6 col-xs-12">

                        <label class="control-label col-md-3 col-sm-3 col-xs-12">English Name
                        </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                        <input type="text" class="form-control" value="{{ $product['Enname'] }}" id="Enname" name="Enname" placeholder="English Name">
                        </div>
                        </div>
             

   
                     

                      </div>
                             <div class="control-label col-md-6 col-sm-6 col-xs-12">

                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Company
                        </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                           {!!Form::select('company', ([null => 'Select Company name'] + $company->toArray()) ,$product['Company_id'],['class'=>'form-control chosen-select company','id' => 'prettify','name'=>'company'])!!}
                        </div>
                        </div>

                               <div class="control-label col-md-6 col-sm-6 col-xs-12">

                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Code
                        </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                        <input type="text" class="form-control" id="Code" name="Code" value="{{ $product['Code'] }}" placeholder="Code">
                        </div>
                        </div>
                          </fieldset>

                          <fieldset style="    padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;">
  <legend style="width: fit-content;border: 0px;">Other Data:</legend>

                     <div class="form-group">


      <div class="control-label col-md-6 col-sm-6 col-xs-12">

                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Weight
                        </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <input type="number" step="any" class="form-control span12" id="weight" value="{{ $product['Weight'] }}" name="weight" placeholder="weight">
                        </div>
                        </div>

                         <div class="control-label col-md-6 col-sm-6 col-xs-12">

                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Rank
                        </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                     <input type="number" step="any" class="form-control" id="rank" name="rank" placeholder="rank" value="{{ $product['Rank'] }}">
                        </div>
                        </div>
             

   
                     

                      </div>
                          <div class="form-group">


                             <div class="control-label col-md-6 col-sm-6 col-xs-12">

                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Specifications
                        </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                                    <input type="text" class="form-control" id="specifications" name="specifications" value="{{ $product['Specifications'] }}" placeholder="specifications">
                        </div>
                        </div>

                               <div class="control-label col-md-6 col-sm-6 col-xs-12">

                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Guidlines
                        </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                        <input type="text" class="form-control" id="guidlines" name="guidlines" value="{{ $product['Guidlines'] }}" placeholder="guidlines">
                        </div>
                        </div>
                          </div>
                             <div class="form-group">


                             <div class="control-label col-md-6 col-sm-6 col-xs-12">

                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Security
                        </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                           <input type="text" class="form-control" id="security" name="security" value="{{ $product['Security'] }}" placeholder="Security">
                        </div>
                        </div>

                               <div class="control-label col-md-6 col-sm-6 col-xs-12">

                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Others
                        </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                        <input type="text" class="form-control" id="others" name="others" value="{{ $product['Others'] }}" placeholder="others">
                        </div>
                        </div>
                          </div>
                             <div class="form-group">


                             <div class="control-label col-md-6 col-sm-6 col-xs-12">

                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Packing Date
                        </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                           <input type="text" class="form-control has-feedback-left" id="packing_date" name="packing_date" value="{{ $product['Packing_date'] }}">
                  <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                        </div>
                        </div>

                               <div class="control-label col-md-6 col-sm-6 col-xs-12">

                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Producation Date
                        </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                      <input type="text" class="form-control has-feedback-left" id="producation_date" name="producation_date" value="{{ $product['Producation_date'] }}">
       <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                        </div>
                        </div>
                          </div>
                          </fieldset>


                                <fieldset style="    padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;">
  <legend style="width: fit-content;border: 0px;">Other Data:</legend>

                     <div class="form-group">


      <div class="control-label col-md-6 col-sm-6 col-xs-12">

                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Front Image
</label>
                        </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <label for="uploadFrontFile" class="custom-file-upload" >
    <i class="fa fa-cloud-upload"></i> Front Image
</label>
<input id="uploadFrontFile"  name="frontimg" type="file">
<div id="blah">
                                  
                                                    <img src="{{$product['Frontimg']}}"  width="100" hieght="100"/>
                                                  </div>                        </div>
                        </div>

                         <div class="control-label col-md-6 col-sm-6 col-xs-12">

                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Back Image
                        </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <label for="uploadBackFile" class="custom-file-upload" >
                          <i class="fa fa-cloud-upload"></i> Back Image
                      </label>
                      <input id="uploadBackFile"  name="backimg" type="file"/>
                    <div id="blahview">
                                                     <img src="{{$product['Backimg']}}"  width="150" hieght="150"/>
                                                  </div>
                        </div>
                        </div>
             

   
                     

                      </div>
                    
                         
                            
                          </fieldset>

                    
            

                  
               
                    


            
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                          <button type="button" class="btn btn-primary " href="/users" >Cancel</button>
                           
                         <button class="btn btn-primary" type="reset">Reset</button>
                          <button type="submit"  name="login_form_submit" class="btn btn-success" value="Save">Submit</button>
                        </div>
                      </div>

      {!!Form::close() !!} 
                  </div>
                </div>
                 </div>
                
                 <div class="col-md-2 col-xs-12 ">
                 </div>
                </div>

@section('other_scripts')
<script type="text/javascript">

  $(document).ready(function(){
    $('#packing_date').daterangepicker({
        singleDatePicker: true,
        singleClasses: "picker_4",
         locale: {
            format: 'YYYY-MM-DD'
        }
      }, function(start) {
        console.log(start.toISOString(), 'Hadeel');
      });
       $('#producation_date').daterangepicker({
        singleDatePicker: true,
        singleClasses: "picker_4",
         locale: {
            format: 'YYYY-MM-DD'
        }
      }, function(start) {
        console.log(start.toISOString(), 'Hadeel');
      });
     $('#profileForm')
        .formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                     'cement_name': {
                  
                    validators: {
                    remote: {
                        url: '/offlineneew/public/checkProductname',
                        data:function(validator, $field, value)
                        {return{id:validator.getFieldElements('id').val()};},
                        message: 'The cement_name is not available',
                        type: 'GET'
                    },
                  
                  notEmpty:{
                    message:"the cement_name is required"
                  },
                    regexp: {
                        regexp: /^[\u0600-\u06FF\s]+$/,
                        message: 'The cement_name can only consist of alphabetical and spaces'
                    }
                    }
                },
                     'Enname': {
                  
                    validators: {
                          remote: {
                        url: '/offlineneew/public/checkproductenglishname',
                        data:function(validator, $field, value)
                        {return{id:validator.getFieldElements('id').val()};},
                        message: 'The english name is not available',
                        type: 'GET'
                    },
                 
                  notEmpty:{
                    message:"the english name is required"
                  },
                    regexp: {
                        regexp: /^[a-zA-Z\s]+$/,
                        message: 'The english name can only consist of alphabetical and spaces'
                    }
                    }
                },
                     'Type': {
                  
                    validators: {
                 
               
                    regexp: {
                        regexp: /^[\u0600-\u06FFa-zA-Z\s]+$/,
                        message: 'The Type  can only consist of alphabetical and spaces'
                    }
                    }
                },
                     'weight': {
                  
                    validators: {
                 
                  notEmpty:{
                    message:"the Weight  is required"
                  },
                    numric: {
                        message: 'The Weight must be number'
                    }
                    }
                },
                     'rank': {
                  
                    validators: {
                 
                  notEmpty:{
                    message:"the Rank  is required"
                  },
                    numric: {
                        message: 'The Rank must be number'
                    }
                    }
                },
                     'specifications': {
                  
                    validators: {
                
                    regexp: {
                        regexp:/^[\u0600-\u06FFa-zA-Z\s]+$/,
                        message: 'The Specifications can only consist of alphabetical and spaces'
                    }
                    }
                },
                  
                     'guidlines': {
                  
                    validators: {
                 
               
                    regexp: {
                        regexp:/^[\u0600-\u06FFa-zA-Z\s]+$/,
                        message: 'The guidlines  can only consist of alphabetical and spaces'
                    }

                    }
                }
                ,
                     'security': {
                  
                    validators: {
                 
               
                    regexp: {
                        regexp: /^[\u0600-\u06FFa-zA-Z\s]+$/,
                        message: 'The Security  can only consist of alphabetical and spaces'
                    }

                    }
                }
                ,
                     'others': {
                  
                    validators: {
                 
              
                    regexp: {
                        regexp: /^[\u0600-\u06FFa-zA-Z\s]+$/,
                        message: 'The Others  can only consist of alphabetical and spaces'
                    }

                    }
                }
                ,
                     'company': {
                  
                    validators: {
                 
                  notEmpty:{
                    message:"the  company is required"
                  }
                    }
                }
             
                
            }  ,submitHandler: function(form) {
        form.submit();
    }
        })
        .on('success.form.fv', function(e) {
   // e.preventDefault();
    var $form = $(e.target);

    // Enable the submit button
   
    $form.formValidation('disableSubmitButtons', false);
});
    $(".company").chosen({
                                   width: '100%',
                                   no_results_text:"No Result Found !!..",
                                   allow_single_deselect: true, 
                                   search_contains:true, });

                                 $(".company").trigger("chosen:updated");
});

</script>



</div>
</section>
@stop
@stop