@extends('masternew')
@section('content')
<section class="panel-body">
 <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Upload Data sheet Product</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                     
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

  <br/>
   <br/>
   <div class="row">
 
        {!!Form::open(['action'=>'ProductController@add','method' => 'post','files'=>true])!!}

        <input type="hidden" name="id" value="{{$id}}" />  
      <label for="file-upload" class="custom-file-upload">
            <i class="fa fa-cloud-upload"></i> Custom Upload
          </label>
          <input id="file-upload"  name="filefield" type="file"/>
      <input  name="asdasd" value="add sheet" class="btn btn-primary" type="submit" /> 
{!!Form::close()!!}
    </div>
  
    <br/>
    
  </div>
  </div>
  </div>
</section>
  

@section('other_scripts')

<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
@stop
@stop
