@extends('masternew')
@section('content')
<style type="text/css">
.flip {
  -webkit-perspective: 800;
perspective: 800;

    position: relative;
 text-align: center;
}
.flip .card.flipped {
  -webkit-transform: rotatey(-180deg);
    transform: rotatey(-180deg);
}
.flip .card {

  height: 100%;
  -webkit-transform-style: preserve-3d;
  -webkit-transition: 0.5s;
    transform-style: preserve-3d;
    transition: 0.5s;
}
.flip .card .face {

  -webkit-backface-visibility: hidden ;
    backface-visibility: hidden ;
  z-index: 2;
   
}
.flip .card .front {
  position: absolute;
   width: 100%;
  z-index: 1;

}
.flip .card .back {
  -webkit-transform: rotatey(-180deg);
    transform: rotatey(-180deg);
}
  .inner{margin:0px !important;}
</style>
<section class="panel-body">
 <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Product</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                     
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

  <br/>
   <br/>
   <div class="row">
    <div class="flip" style=" display: inline-block;">
        <div class="card"> 
          <div class="face front" style=" width:500px;height:550px;"> 
            <div class="well well-sm inner" style="height:50px;">{{ $product->Cement_Name }}</div>
               <img src="{{$product->Frontimg}}" style="width:100%;height:500px;">
          </div> 
          <div class="face back"> 
            <div class="well well-sm inner" style=" width:500px;height:550px;background-color:white;">
                   <img src="{{$product->Backimg}}" style="width:100%;height:550px;">
            </div>
          </div>
        </div>   
      </div> 
    </div>
  
    <br/>
    
  </div>
  </div>
  </div>
</section>
  

@section('other_scripts')

<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>


  <script type="text/javascript">
    $('.flip').click(function(){
        $(this).find('.card').toggleClass('flipped');

    });
  </script>
@stop
@stop
