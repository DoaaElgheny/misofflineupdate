@extends('master')
@section('content')
<!DOCTYPE html>
<meta name="csrf-token" content="{{ csrf_token() }}" />
<section class="panel panel-primary">
  <div class="panel-body">
    <h1><i class='fa fa-tasks'></i>License</h1>
    <a type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal1" style="margin-bottom: 20px;"> Add New License</a>
    <!-- model -->
    <div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <h4 class="modal-title span7 text-center" id="myModalLabel"><span class="title">Add New License</span></h4>
          </div>
          <div class="modal-body">
            {!! Form::open(['action' => "LicenseController@storeLicense",'method'=>'post','id'=>'profileForm']) !!}  
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <div class="form-group col-md-12"> 
                <label  class="control-label col-md-3">License Name:</label>        
                <div class="col-md-9"> 
                  <input type="text" class="form-control" id="NameLicence" name="NameLicence" placeholder="Enter Name Licence" required>
                </div>
              </div>
              <div class="form-group col-md-12">
                <label for="input_cement_name" class="control-label col-md-3">License Number:</label>
                <div class="col-md-9">
                  <input type="number" class="form-control" id="LicenseNumber" name="LicenseNumber" placeholder="enter License Number" required>
                </div>
              </div>
              <div class="form-group col-md-12">
                <label for="input_cement_name" class="control-label col-md-3">Creation Date:</label>
                <div class="col-md-9">
                  <input type="text" class="form-control" id="Creation_Date" name="Creation_Date" placeholder="enter Creation Date" required>
                </div>
              </div>
              <div class="form-group col-md-12">
                <label for="input_cement_name" class="control-label col-md-3">Government:</label>
                <div class="col-md-9">
                  {!!Form::select('Government',([null => 'please chooose'] + $Government->toArray()) ,null,['class'=>'form-control  Government','id' => 'Government','required'=>'required'])!!}
                </div>
              </div>
              <div class="form-group col-md-12">
                <label for="input_cement_name" class="control-label col-md-3">District:</label>
                <div class="col-md-9">
                  {!!Form::select('district', ([null => 'Select District ']) ,null,['class'=>'form-control District','id' => 'prettify','required'=>'required'])!!}
                </div>
              </div>
              <hr/>
              <button type="submit" class="btn btn-primary "style="float:right" name="login_form_submit" value="Save">Save</button>
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            {!!Form::close()!!}
          </div>
        </div>
      </div>
    </div>
    <!-- ENDModal -->
    <table class="table table-hover table-bordered  dt-responsive nowrap display tasks" cellspacing="0" width="100%">
      <thead>
        <th>No</th>
        <th>Retailer Name</th> 
        <th>License Name</th>
        <th>License Number</th> 
        <th>Creation Date</th>
        <th>Government</th> 
        <th>District</th>
        <th>Process</th>          
      </thead>
      <tfoot>
        <th></th>
        <th>Retailer Name</th> 
        <th>License Name</th>
        <th>License Number</th> 
        <th>Creation Date</th>
        <th>Government</th> 
        <th>District</th>
        <th></th>        
      </tfoot>               
      <tbody style="text-align:center;">
        <?php $i=1;?>
        @foreach($Licenses as $License)
          <tr>
            <td>{{$i++}}</td>   
            <td>{{$License->HasRetailer->Name}}</td>
            <td><a  class="testEdit" data-type="text" data-name="Name"  data-pk="{{ $License->id }}">{{$License->Name }}</a></td> 
            <td><a  class="testEdit" data-type="number" data-name="License_Number"  data-pk="{{ $License->id }}">{{$License->License_Number}}</a></td> 
            <td><a  class="testEdit" data-type="text" data-name="Creation_Date"  data-pk="{{ $License->id }}">{{$License->Creation_Date}}</a></td>
            <td>
              <a class="testEdit" data-type="select"  data-value="{{$Government}}" data-source ="{{$Government}}" data-name="Government_Id"  data-pk="{{$License->id}}"> 
                {{$License->HasGovernment->Name}}
              </a>
            </td>
            <td>
              <a class="testEdit" data-type="select"  data-value="{{$District}}" data-source ="{{$District}}" data-name="District_Id"  data-pk="{{$License->id}}"> 
                {{$License->HasDistrict->Name}}
              </a>
            </td>
            <td><a href="/cemexmarketingsystem/public/license/destroy/{{$License->id}}"class="btn btn-default btn-sm" ><span class="glyphicon glyphicon-trash"></span></a></td>
          </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</section>  
<script type="text/javascript">
  $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });
</script>
<script type="text/javascript">
  var editor;
  $(document).ready(function()
    {   
        $('#Creation_Date').datepicker({ language: 'ar',
          format: "yyyy-mm-dd"
        });
        $('#profileForm')
        .formValidation({
        framework: 'bootstrap',
        icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
              },
              fields: {      
                        'LicenseNumber': 
                        {
                          validators: 
                          {
                            numeric:
                            {
                              message: 'The value is not a number'
                            },
                            remote:
                            {
                              url: '/cemexmarketingsystem/public/checkLicenseNumber',
                              message: 'This  License Number is Already Exist',
                              type: 'GET'
                            },
                            notEmpty:
                            {
                              message:"It is required"
                            }
                          }
                        },
                        'NameLicence':
                        {
                          validators: 
                          {
                            notEmpty:
                            {
                              message:"It is required"
                            },
                            regexp:
                            {
                              regexp: /^[\u0600-\u06FFA-Za-z\s]+$/,
                              message: 'The Name Licence can only consist of Alphabit'
                            }
                          }
                        },
                        'Government': 
                        {
                          validators: 
                          {
                            notEmpty:
                            {
                              message:"It is required"
                            }
                          }
                        },
                        'District': 
                        {
                          validators: 
                          {
                            notEmpty:
                            {
                              message:"It is required"
                            }
                          }
                        }       
              } ,submitHandler: function(form) {
        form.submit();
    }
        })
        .on('success.form.fv', function(e) {
   // e.preventDefault();
    var $form = $(e.target);

    // Enable the submit button
   
    $form.formValidation('disableSubmitButtons', false);
});

      
     
        var table= $('.tasks').DataTable
        ({
          select:true,
          responsive: true,
          "order":[[0,"asc"]],
          'searchable':true,
          "scrollCollapse":true,
          "paging":true,
          "pagingType": "simple",
          dom: 'lBfrtip',
          buttons: [  
            { extend: 'excel', className: 'btn btn-primary dtb' }      
          ],
          fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull)
          {
            $.fn.editable.defaults.send = "always";
            $.fn.editable.defaults.mode = 'inline';
            $('.testEdit').editable
            ({
              validate: function(value)
              {
                name=$(this).editable().data('name');
                if(name=="Name" || name=="License_Number" || name== "Creation_Date" || name=="Government_Id" || name== "District_Id" )
                {
                  if($.trim(value) == '') 
                  {
                    return 'Value is required.';
                  }
                }
                if(name=="Name")
                {
                  var regexp =/^[\u0600-\u06FFA-Za-z\s]+$/;    
                  if (!regexp.test(value))
                  {
                    return 'This field is not valid';
                  }               
                }
                if(name=="License_Number")
                {
                  var regexp =/^[0-9]+$/;    
                  if (!regexp.test(value))
                  {
                    return 'This field is not valid';
                  }               
                }
                if(name=="Creation_Date")
                {
                  var regexp =/^(\d{4})-(\d{1,2})-(\d{1,2})$/;    
                  if (!regexp.test(value))
                  {
                    return 'This field is not valid';
                  }               
                }
              },
              placement: 'right',
              url:'{{URL::to("/")}}/updatelicense',      
              ajaxOptions:
              {
                type: 'get',
                sourceCache: 'false',
                dataType: 'json',
              },
              params: function(params)
              {
                // add additional params from data-attributes of trigger element
                params.name = $(this).editable().data('name');
                // console.log(params);
                return params;
              },
              error: function(response, newValue)
              {
                if(response.status === 500) 
                {
                  return 'This Data Already Exist,Enter Correct Data.';
                }
                else 
                {
                  return response.responseText;
                }
              },
              success:function(response)
              {
                if(response.status==="You do not have permission.")
                {
                  return 'You do not have permission.';
                }
              }
            });
          }
        });
        $('.tasks tfoot th').each(function ()
          {
            var title = $('.tasks thead th').eq($(this).index()).text();             
            if($(this).index()>=1 && $(this).index()<=6)
            {
              $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
            }
          });
        table.columns().every( function ()
        {
          var that = this;
          $(this.footer()).find('input').on('keyup change', function ()
          {
            that.search(this.value).draw();
            if (that.search(this.value) )
            {
              that.search(this.value).draw();
            }
          }); 
        });
        $(".Government").change(function() 
        {
          $.getJSON("/cemexmarketingsystem/public/government/district/" + $(".Government").val(), function(data)
          {
            var $contractors = $(".District");
            $(".District").chosen("destroy");
            $contractors.empty();
            $.each(data, function(index, value) 
            {
              $contractors.append('<option value="' + index +'">' + value + '</option>');
            }); 
            $('.District').chosen
            ({
                width: '100%',
                no_results_text:'No Results'
            });
            $(".District").trigger("chosen:updated");
            $(".District").trigger("change");
          });
        });
    });
</script>
@stop
