@extends('master')


@section('content')


<meta name="csrf-token" content="{{ csrf_token() }}" />
<section class="panel panel-primary">
<div class="panel-body">

                
{!!Form::open(['action' =>'RetailerController@store','method' => 'Post','id'=>'profileForm'])!!}
          <input type="hidden" name="_token" value="{{ csrf_token() }}">

 <div class="col-md-12">
<fieldset>
                    <legend style="color:red;">Retailer Main Data</legend>

 <div class="col-md-12">
                    <label for="input_product" class="control-label col-md-3">Name</label>
                    <div class="col-md-9"> 
                      <input type="text" class="form-control" id="Name" name="Name" placeholder="enter Name">
                    </div>
                  </div>
                  <div class="col-md-12">
                    <label for="input_product" class="control-label col-md-3">English Name</label>
                    <div class="col-md-9"> 
                      <input type="text" class="form-control" id="Name" name="EngName" placeholder="enter Eng Name" required>
                    </div>
                  </div>
                 

                  <div class="col-md-12">
                    <label for="input_product" class="control-label col-md-3">Type</label>
                    <div class="col-md-9"> 

                      {!!Form::select('Type_Id', ([null => 'Select Type ']+ $types->toArray()) ,null,['class'=>'form-control','id' => 'Typeid','required'=>'required'])!!}

                      
                   </div>
                  </div>
                  <div class="col-md-12">
                    <label for="input_product" class="control-label col-md-3">Sub Type</label>
                    <div class="col-md-9"> 
                      {!!Form::select('SubType_Id',([null => 'please chooose Sub Type']),null,['class'=>'form-control','id' => 'SubtypeId','name'=>'SubType_Id','required'=>'required'])!!}

                   </div>
                  </div>
                  <div class="col-md-12">
                    <label for="input_product" class="control-label col-md-3">Finally Type</label>
                    <div class="col-md-9">

                      {!!Form::select('EndType_Id',([null => 'please chooose Finally Type']),null,['class'=>'form-control','id' => 'EndSubTypeid','name'=>'EndType_Id','required'=>'required'])!!}
                   </div>
                  </div>
                  <div class="col-md-12">
                    <label for="input_product" class="control-label col-md-3">Code</label>
                    <div class="col-md-9"> 
                      <input type="Number" class="form-control" id="Name" name="Code" placeholder="enter Code" required >
                    </div>
                  </div>
                  <div class="col-md-12">
                    <label for="input_product" class="control-label col-md-3">Email</label>
                    <div class="col-md-9"> 
                      <input type="email" class="form-control" id="Name" name="Email" placeholder="enter  Email" >
                    </div>
                  </div>
                   <div class="col-md-12">
                    <label for="input_product" class="control-label col-md-3">Address</label>
                    <div class="col-md-9"> 
                      <input type="text" class="form-control" id="Name" name="Address" placeholder="enter  Address" >
                    </div>
                  </div>
                  <div class="col-md-12">
                    <label for="input_product" class="control-label col-md-3">Telephone</label>
                    <div class="col-md-9"> 
                      <input type="Number" class="form-control" id="Name" name="Telephone" placeholder="enter Telephone" required>
                    </div>
                  </div>
                    <div class="col-md-12">
                    <label for="input_product" class="control-label col-md-3">Promoter</label>
                    <div class="col-md-9">  
                      {!!Form::select('Promoter',([null => 'please chooose'] + $promoter->toArray()) ,null,['class'=>'form-control subuser_list','id' => 'promoter','required'=>'required'])!!}
                    </div>
                  </div>
                  </fieldset>
                  </div>
                   <div class="col-md-12">
                  <fieldset>
                    <legend style="color:red;">Location Retailers</legend>
                       
                         <div class="form-group  col-xs-12">
                            
                            <div class="col-xs-3" >
                                <input class="form-control" type="number" id="Lan" step="any" name="Lan[]" placeholder="Langitude" />
                            </div>

                            <div class="col-xs-3" >
                                <input class="form-control" type="number" id="Lat" step="any" name="Latn[]" placeholder="Latitude" />
                            </div>
                            <div class="col-xs-3" >
                                <input class="form-control" type="text" id="Location" name="Location[]" placeholder="Location" required/>
                            </div>
                            <div class="col-xs-2" >
                                <button type="button" class="btn btn-default addButton"><i class="fa fa-plus"></i></button>
                            </div>
                             
                        </div>

                        <!-- The template containing an email field and a Remove button -->
                        <div class="form-group hide col-xs-12" id="emailTemplate">   
                             <div class="col-xs-3" >
                                <input class="form-control" type="number" id="Lan" step="any" name="Lan[]" placeholder="Langitude" />
                            </div>

                            <div class="col-xs-3" >
                                <input class="form-control" type="number" id="Lat" step="any" name="Lat[]" placeholder="Latuitde" />
                            </div>
                            <div class="col-xs-3" >
                                <input class="form-control" type="text" id="Location" name="Location[]" placeholder="Location" required/>
                            </div>
                             <div class="col-xs-2" >
                                <button type="button" class="btn btn-default removeButton"><i class="fa fa-minus"></i></button>
                            </div>
                        </div>
                    </fieldset>
                  </div>
                <div class="col-md-12">
                  <fieldset>
                    <legend style="color:red;">Licences Retailer</legend>
                    <div>
                <div class="col-md-2"> 
                <input type="text" class="form-control" id="NameLicence" name="NameLicence[]" placeholder="Enter Name Licence" required>
                </div>

                <div class="col-md-2">  
                {!!Form::select('Government[]',([null => 'please chooose'] + $governments->toArray()) ,null,['class'=>'form-control subuser_list Government','id' => 'Government','required'=>'required'])!!}
                </div>
                <div class="col-md-2"> 

                {!!Form::select('District[]', ([null => 'Select District ']) ,null,['class'=>'form-control District','id' => 'District','required'=>'required'])!!}

                </div>
                <div class="col-md-2"> 
                <input type="number" class="form-control" id="LicenseNumber" name="LicenseNumber[]" placeholder="enter License Number" required>
                </div>
                <div class="col-md-2"> 
                <input type="text" class="form-control Creation_Date" id="Creation_Date" name="Creation_Date[]" placeholder="enter Creation Date" required>
                </div>
                <div class="col-xs-2" >
                <button type="button" class="btn btn-default addButton1"><i class="fa fa-plus"></i></button>
                </div>
                </div>
                 <div class="form-group hide col-xs-12" id="LicenceTemplate">   
                           <div class="col-md-2"> 
                <input type="text" class="form-control" id="NameLicence" name="NameLicence[]" placeholder="Enter Name Licence" >
                </div>
                <div class="col-md-2">  
                {!!Form::select('Government[]',([null => 'please chooose'] + $governments->toArray()) ,null,['class'=>'form-control subuser_list Government','id' => 'Government','required'=>'required'])!!}
                </div>
                <div class="col-md-2"> 

                {!!Form::select('District[]', ([null => 'Select District ']) ,null,['class'=>'form-control District','id' => 'District','required'=>'required'])!!}

                </div>
                <div class="col-md-2"> 
                <input type="number" class="form-control" id="LicenseNumber" name="LicenseNumber[]" placeholder="enter License Number" required>
                </div>
                <div class="col-md-2"> 
                <input type="text" class="form-control Creation_Date" id="Creation_Date" name="Creation_Date[]" placeholder="enter Creation Date" required>
                </div>
                <div class="col-xs-2" >
                <button type="button" class="btn btn-default minusButton1"><i class="fa fa-minus"></i></button>
                </div>
                        </div>
                    </fieldset>
                </div>
               <!-- Message container -->
  <div class="form-group">
                            <div class="col-xs-9 col-xs-offset-3">
                                <div id="messageContainer"></div>
                            </div>
  </div>  

                        <div class="form-group">
                            <div class="col-xs-5 col-xs-offset-3">
                                <button type="submit" name="submitButton" class="btn btn-info" >Submit</button>  
                            </div>
                        </div>
                
                  </tr>  
             </table> 
  
          {!!Form::close()!!}


<script type="text/javascript">

  $(document).ready(function(){
     var $i=0;
      $('#Creation_Date').datepicker({ language: 'ar',
                     format: "yyyy-mm-dd"
                    });
      $('#Typeid').change(function(){
        $.get("{{ url('Type/typedropdown')}}", 
        { option: $(this).val() }, 
        function(data) {
            $('#SubtypeId').empty(); 
            $.each(data, function(key, element) {
                $('#SubtypeId').append("<option value='" + key +"'>" + element + "</option>");
            });
        });
    });
       $('#SubtypeId').change(function(){
        $.get("{{ url('EndType/Endtypedropdown')}}", 
        { option: $(this).val() }, 
        function(data) {
            $('#EndSubTypeid').empty(); 
            $.each(data, function(key, element) {
                $('#EndSubTypeid').append("<option value='" + key +"'>" + element + "</option>");
            });
        });
    });

    $('#profileForm')
        .formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                  'Name': {
                  
                       validators: {
                            remote: {
                        url: '/offlineneew/public/checkRetailerName',
                        
                        message: 'This  Name is not available',
                        type: 'GET'
                    },
                    
                     notEmpty:{
                    message:"It is required"
                  },
                    regexp:
                          {
                            regexp: /^[\u0600-\u06FF\s]+$/,
                            message: 'The Retailer name can only consist of alphabetical and spaces'
                          }
}
                    }
                    , 
                  'EngName': 
                      {
                        validators: 
                        {
                          
                          regexp:
                          {
                            regexp: /^[a-zA-Z\s]+$/,
                            message: 'The Retailer English name can only consist of alphabetical and spaces'
                          }
                        }
                      }
                      ,  

                 'Code': {
                    validators: {
                         numeric: {
                            message: 'The value is not a number'
                        },

                            remote: {
                        url: '/offlineneew/public/checCodeRetailers',
                        message: 'This  Code is not available',
                        type: 'GET'
                    },
                      notEmpty:{
                    message:"It is required"
                  }
                    
                   
                    }
                },


                       'Telephone': {
                    validators: {
                         numeric: {
                            message: 'The value is not a number'
                        },
                         regexp:
                          {
                            regexp: /^[0-9]{11}$/,
                            message: 'The Retailer Telephone can only consist of Number'
                          },

                            remote: {
                        url: '/offlineneew/public/checkRetailerTele',
                        message: 'This  Telephone is not available',
                        type: 'GET'
                    },
                      notEmpty:{
                    message:"It is required"
                  }
                    
                   
                    }
                },
                  'Email': {
                    validators: {
                            remote: {
                        url: '/offlineneew/public/checkRetailerEmail',
                        message: 'This  Email is not available',
                        type: 'GET'
                    }
                    
                   
                    }
                },
        
                       'Type_Id': {
                  
                       validators: {
                     notEmpty:{
                    message:"It is required"
                  }
                  }
                    }
                    ,   'SubType_Id': {
                  
                       validators: {
              
                   
                    
notEmpty:{
                    message:"It is required"
                  }
                }
                    },
                       'EndType_Id': {
                  
                       validators: {
                      notEmpty:{
                    message:"It is required"
                  }
                }
                    },
                    'Promoter': {
                  
                       validators: {
                      notEmpty:{
                    message:"It is required"
                  }
                }
                    },

                      'LicenseNumber[]': {
                  
                       validators: {
                         callback: {
                            callback: function(value, validator, $field) {
                                var $emails          = validator.getFieldElements('LicenseNumber[]'),
                                    numEmails        = $emails.length,
                                    notEmptyCount    = 0,
                                    obj              = {},
                                    duplicateRemoved = [];

                                for (var i = 0; i < numEmails; i++) {
                                    var v = $emails.eq(i).val();
                                    if (v !== '') {
                                        obj[v] = 0;
                                        notEmptyCount++;
                                    }
                                }

                                for (i in obj) {
                                    duplicateRemoved.push(obj[i]);
                                }

                                if (duplicateRemoved.length === 0) {
                                    return {
                                        valid: false,
                                        message: 'You must fill at least one License Number'
                                    };
                                } else if (duplicateRemoved.length !== notEmptyCount) {
                                    return {
                                        valid: false,
                                        message: 'The License Number must be unique'
                                    };
                                }

                                validator.updateStatus('LicenseNumber[]', validator.STATUS_VALID, 'callback');
                                return true;
                            }
                        },
                         numeric: {
                            message: 'The value is not a number'
                        },
                                 remote: {
                        url: '/offlineneew/public/checkLicenseNumber',
                        message: 'This  License Number is not available',
                        type: 'GET'
                    },

                      notEmpty:{
                    message:"It is required"
                  }
                }
                    },
                       'NameLicence': {
                       validators: {
                           notEmpty:{
                    message:"It is required"
                  }
                    }
                    },
                      'Location': {
                       validators: {
                   notEmpty:{
                    message:"It is required"
                  }
                }
                    },
                     'Government': {
                       validators: {
                    
                    notEmpty:{
                    message:"It is required"
                             }
                           }
                    },
                'District': {
                  
                    validators: {
                    
                   notEmpty:{
                    message:"It is required"
                           }
                         }
                    }

           
              } ,submitHandler: function(form) {
        form.submit();
    }
        })
        .on('success.form.fv', function(e) {
   // e.preventDefault();
    var $form = $(e.target);

    // Enable the submit button
   
    $form.formValidation('disableSubmitButtons', false);
})
          .on('click', '.addButton', function() {
            var $template = $('#emailTemplate'),
                $clone    = $template
                                .clone()
                                .removeClass('hide')
                                .removeAttr('id')
                                .insertBefore($template),
                $Lat    = $clone.find('[name="Lat[]"]'),
                $Lan    = $clone.find('[name="Lan[]"]');
                $Location    = $clone.find('[name="Location[]"]');

            // Add new field
            $('#profileForm').formValidation('addField', $Lat).formValidation('addField', $Lan).formValidation('addField', $Location);
        })

        // Remove button click handler
        .on('click', '.removeButton', function() {

            var $row   = $(this).closest('.form-group'),
                 $Lat    = $row.find('[name="Lat[]"]'),
                $Lan    = $row.find('[name="Lan[]"]'),
                $Location    = $row.find('[name="Location[]"]');
            // Remove element containing the email
            $row.remove();

            // Remove field
            $('#profileForm').formValidation('removeField', $Lat).formValidation('removeField', $Lan).formValidation('removeField', $Location);
        })
        
  .on('click', '.addButton1', function() {
$i++;
            var $template = $('#LicenceTemplate'),
                $clone    = $template
                                .clone()
                                .removeClass('hide')
                                .removeAttr('id')
                                .insertBefore($template),
                $LicenseNumber  = $clone.find('[name="LicenseNumber[]"]'),
                $NameLicence    = $clone.find('[name="NameLicence[]"]');
                 $District    = $clone.find('[name="District[]"]')
                                       .attr("id",'District'+$i)
                                       .chosen({ 
                                                   width: '100%',
                                                   no_results_text: "No Results",
                                                   allow_single_deselect: true, 
                                                   search_contains:true, })
                                                  .trigger("chosen:updated");
                $Government    = $clone.find('[name="Government[]"]')
                                    
                                       .attr("id",'Government'+$i)
                                     
                                       .chosen({ 
                                                   width: '100%',
                                                   no_results_text: "No Results",
                                                   allow_single_deselect: true, 
                                                   search_contains:true, })
                                    
                                       .trigger("chosen:updated")
                                      
                                        .change(function() {
                                              console.log($(this).val());
                                          $.getJSON("/government/district/" + $(this).val(), function(data) {
 var Districtname="#District"+($i);
 console.log(Districtname);
                var $District = $(Districtname);
                   $(Districtname).chosen("destroy");
                $District.empty();
                $.each(data, function(index, value) {
                    $District.append('<option value="' + index +'">' + value + '</option>');
                });
                    
                    $District.chosen({ 
                                                   width: '100%',
                                                   no_results_text: "No Results",
                                                   allow_single_deselect: true, 
                                                   search_contains:true, })
                                                  .trigger("chosen:updated");
})});
 
                $Creation_Date    = $clone.find('[name="Creation_Date[]"]');
                $('.Creation_Date').datepicker({ language: 'ar',
                     format: "yyyy-mm-dd"
                    });


            // Add new field
            $('#profileForm').formValidation('addField', $LicenseNumber).formValidation('addField', $NameLicence).formValidation('addField', $Government).formValidation('addField', $District).formValidation('addField', $Creation_Date);
      

        })

        // Remove button click handler
        .on('click', '.minusButton1', function() {

            var $row   = $(this).closest('.form-group'),
                 $LicenseNumber= $row.find('[name="LicenseNumber[]"]'),
                $NameLicence= $row.find('[name="NameLicence[]"]'),
                $Government    = $row.find('[name="Government[]"]');
                $District    = $row.find('[name="District[]"]'),
                $Creation_Date    = $row.find('[name="Creation_Date[]"]');
            // Remove element containing the email
            $row.remove();

            // Remove field
            $('#profileForm').formValidation('removeField', $LicenseNumber).formValidation('removeField', $NameLicence).formValidation('removeField', $Government).formValidation('removeField', $District).formValidation('removeField', $Creation_Date);
        });
     

    $('[data-toggle="popover"]').popover({ trigger: "hover" }); 

 $("#Government").chosen({ 
                   width: '100%',
                   no_results_text: "No Results",
                   allow_single_deselect: true, 
                   search_contains:true, });
 $("#Government").trigger("chosen:updated");

    $('[data-toggle="popover"]').popover({ trigger: "hover" }); 

 });
  $('#District').chosen({
                width: '100%',
                no_results_text:'No Results'
            });
                $("#District").trigger("chosen:updated");

 $("#Government").change(function() {
       
            $.getJSON("/government/district/" + $("#Government").val(), function(data) {
 
                var $contractors = $("#District");
                   $("#District").chosen("destroy");
                $contractors.empty();
                $.each(data, function(index, value) {
                    $contractors.append('<option value="' + index +'">' + value + '</option>');
                });
                  
        
             $('#District').chosen({
                width: '100%',
                no_results_text:'No Results'
            });
                $("#District").trigger("chosen:updated");
            $("#District").trigger("change");
            });
  
    
});

 $("#Government1").change(function() {
       console.log($("#Government1").val());
            $.getJSON("/government/district/" + $("#Government1").val(), function(data) {
 
                var $contractors = $("#District");
                   $("#District").chosen("destroy");
                $contractors.empty();
                $.each(data, function(index, value) {
                    $contractors.append('<option value="' + index +'">' + value + '</option>');
                });
                  
        
             $('#District').chosen({
                width: '100%',
                no_results_text:'No Results'
            });
                $("#District").trigger("chosen:updated");
            $("#District").trigger("change");
            });
  
    
});














</script>



</div>
</section>
@stop