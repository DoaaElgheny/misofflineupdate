@extends('master')


@section('content')


<meta name="csrf-token" content="{{ csrf_token() }}" />
<section class="panel panel-primary">
<div class="panel-body">

                
{!!Form::open(['action' =>'RetailerController@updateretailers','method' => 'Post','id'=>'profileForm'])!!}
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
<input type="hidden" name="id" value="{{$Retailers->id}}">
 <div class="col-md-12">
<fieldset>
                    <legend style="color:red;">Retailer Main Data</legend>

 <div class="col-md-12">
                    <label for="input_product" class="control-label col-md-3">Name</label>
                    <div class="col-md-9"> 
                      <input type="text" class="form-control" id="Name" name="Name" value="{{$Retailers->Name}}" placeholder="enter Name">
                    </div>
                  </div>
                  <div class="col-md-12">
                    <label for="input_product" class="control-label col-md-3">English Name</label>
                    <div class="col-md-9"> 
                      <input type="text" class="form-control" id="Name" name="EngName" value="{{$Retailers->EngName}}" placeholder="enter Eng Name" required>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <label for="input_product" class="control-label col-md-3" > Type </label>
                    <div class="col-md-9"> 
                      {!!Form::select('Type_Id', ([null => 'Select Type']+$types->toArray()),$Retailers->Type_Id,['class'=>'form-control','id' => 'Typeid','required'=>'required'])!!}                      
                   </div>
                  </div>
                  <div class="col-md-12">
                    <label for="input_product" class="control-label col-md-3">Sub Type</label>
                    <div class="col-md-9"> 
                      {!!Form::select('SubType_Id',([null => 'please chooose Sub Type']+$SubTypese->toArray()),$Retailers->SubType_Id,['class'=>'form-control','id' => 'SubtypeId','name'=>'SubType_Id','required'=>'required'])!!}

                   </div>
                  </div>
                  <div class="col-md-12">
                    <label for="input_product" class="control-label col-md-3">Finally Type</label>
                    <div class="col-md-9">

                      {!!Form::select('EndType_Id',([null => 'please chooose Finally Type']+$EndTypese->toArray()),$Retailers->EndType_Id,['class'=>'form-control','id' => 'EndSubTypeid','name'=>'EndType_Id','required'=>'required'])!!}
                   </div>
                  </div>
                
                  <div class="col-md-12">
                    <label for="input_product" class="control-label col-md-3">Email</label>
                    <div class="col-md-9"> 
                      <input type="email" class="form-control" id="Name" name="Email" value="{{$Retailers->Email}}" placeholder="enter  Email" >
                    </div>
                  </div>
                   <div class="col-md-12">
                    <label for="input_product" class="control-label col-md-3">Address</label>
                    <div class="col-md-9"> 
                      <input type="text" class="form-control" id="Name" name="Address" value="{{$Retailers->Address}}" placeholder="enter  Address" >
                    </div>
                  </div>
                  <div class="col-md-12">
                    <label for="input_product" class="control-label col-md-3">Telephone</label>
                    <div class="col-md-9"> 
                      <input type="Number" class="form-control" id="Name" name="Telephone" value="{{$Retailers->Telephone}}" placeholder="enter Telephone" required>
                    </div>
                  </div>
                  </fieldset>
                  </div>
               
                
               <!-- Message container -->
  <div class="form-group">
                            <div class="col-xs-9 col-xs-offset-3">
                                <div id="messageContainer"></div>
                            </div>
  </div>  

                        <div class="form-group">
                            <div class="col-xs-5 col-xs-offset-3">
                                <button type="submit" name="submitButton" class="btn btn-info" >Submit</button>  
                            </div>
                        </div>
                
                  </tr>  
             </table> 
  
          {!!Form::close()!!}


<script type="text/javascript">

  $(document).ready(function(){
      $('#Typeid').change(function(){
        $.get("{{ url('Type/typedropdown')}}", 
        { option: $(this).val() }, 
        function(data) {
            $('#SubtypeId').empty(); 
            $.each(data, function(key, element) {
                $('#SubtypeId').append("<option value='" + key +"'>" + element + "</option>");
            });
        });
    });
       $('#SubtypeId').change(function(){
        $.get("{{ url('EndType/Endtypedropdown')}}", 
        { option: $(this).val() }, 
        function(data) {
            $('#EndSubTypeid').empty(); 
            $.each(data, function(key, element) {
                $('#EndSubTypeid').append("<option value='" + key +"'>" + element + "</option>");
            });
        });
    });

    $('#profileForm')
        .formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                  'Name': {
                  
                       validators: {
                            remote: {
                        url: '/offlineneew/public/checkRetailerName',
                         data:function(validator, $field, value)
                        {return{id:validator.getFieldElements('id').val()};},
                        message: 'This  Name is not available',
                        type: 'GET'
                    },
                    
                     notEmpty:{
                    message:"It is required"
                  },
                    regexp:
                          {
                            regexp: /^[\u0600-\u06FF\s]+$/,
                            message: 'The Retailer name can only consist of alphabetical and spaces'
                          }
}
                    }
                    , 
                  'EngName': 
                      {
                        validators: 
                        {
                          
                          regexp:
                          {
                            regexp: /^[a-zA-Z\s]+$/,
                            message: 'The Retailer English name can only consist of alphabetical and spaces'
                          }
                        }
                      }
                      ,'Telephone': {
                    validators: {
                         numeric: {
                            message: 'The value is not a number'
                        },
                         regexp:
                          {
                            regexp: /^[0-9]{11}$/,
                            message: 'The Retailer Telephone can only consist of Number'
                          },

                            remote: {
                        url: '/offlineneew/public/checkRetailerTele',
                         data:function(validator, $field, value)
                        {return{id:validator.getFieldElements('id').val()};},
                        message: 'This  Telephone is not available',
                        type: 'GET'
                    },
                      notEmpty:{
                    message:"It is required"
                  }
                    
                   
                    }
                },
                  'Email': {
                    validators: {
                            remote: {
                        url: '/offlineneew/public/checkRetailerEmail',
                         data:function(validator, $field, value)
                        {return{id:validator.getFieldElements('id').val()};},
                        message: 'This  Email is not available',
                        type: 'GET'
                    }
                    
                   
                    }
                },
        
                       'Type_Id': {
                  
                       validators: {
                     notEmpty:{
                    message:"It is required"
                  }
                  }
                    }
                    ,   'SubType_Id': {
                  
                       validators: {
              
                   
                    
notEmpty:{
                    message:"It is required"
                  }
                }
                    },
                       'EndType_Id': {
                  
                       validators: {
                      notEmpty:{
                    message:"It is required"
                  }
                }
                    },
                    'Promoter': {
                  
                       validators: {
                      notEmpty:{
                    message:"It is required"
                  }
                }
                    }

           
              } ,submitHandler: function(form) {
        form.submit();
    }
        })
        .on('success.form.fv', function(e) {
   // e.preventDefault();
    var $form = $(e.target);

    // Enable the submit button
   
    $form.formValidation('disableSubmitButtons', false);
})
          .on('click', '.addButton', function() {
            var $template = $('#emailTemplate'),
                $clone    = $template
                                .clone()
                                .removeClass('hide')
                                .removeAttr('id')
                                .insertBefore($template),
                $Lat    = $clone.find('[name="Lat[]"]'),
                $Lan    = $clone.find('[name="Lan[]"]');
                $Location    = $clone.find('[name="Location[]"]');

            // Add new field
            $('#profileForm').formValidation('addField', $Lat).formValidation('addField', $Lan).formValidation('addField', $Location);
        })

        // Remove button click handler
        .on('click', '.removeButton', function() {

            var $row   = $(this).closest('.form-group'),
                 $Lat    = $row.find('[name="Lat[]"]'),
                $Lan    = $row.find('[name="Lan[]"]'),
                $Location    = $row.find('[name="Location[]"]');
            // Remove element containing the email
            $row.remove();

            // Remove field
            $('#profileForm').formValidation('removeField', $Lat).formValidation('removeField', $Lan).formValidation('removeField', $Location);
        })
  .on('click', '.addButton1', function() {
 
            var $template = $('#LicenceTemplate'),
                $clone    = $template
                                .clone()
                                .removeClass('hide')
                                .removeAttr('id')
                                .insertBefore($template),
                $LicenseNumber    = $clone.find('[name="LicenseNumber[]"]'),
                $NameLicence    = $clone.find('[name="NameLicence[]"]');
                $Government    = $clone.find('[name="Government[]"]');
                  $District    = $clone.find('[name="District[]"]');
                $Creation_Date    = $clone.find('[name="Creation_Date[]"]');

            // Add new field
            $('#profileForm').formValidation('addField', $LicenseNumber).formValidation('addField', $NameLicence).formValidation('addField', $Government).formValidation('addField', $District).formValidation('addField', $Creation_Date);
        })

        // Remove button click handler
        .on('click', '.minusButton1', function() {

            var $row   = $(this).closest('.form-group'),
                 $LicenseNumber= $row.find('[name="LicenseNumber[]"]'),
                $NameLicence= $row.find('[name="NameLicence[]"]'),
                $Government    = $row.find('[name="Government[]"]');
                $District    = $row.find('[name="District[]"]'),
                $Creation_Date    = $row.find('[name="Creation_Date[]"]');
            // Remove element containing the email
            $row.remove();

            // Remove field
            $('#profileForm').formValidation('removeField', $LicenseNumber).formValidation('removeField', $NameLicence).formValidation('removeField', $Government).formValidation('removeField', $District).formValidation('removeField', $Creation_Date);
        });
     

    $('[data-toggle="popover"]').popover({ trigger: "hover" }); 

 $(".Government").chosen({ 
                   width: '100%',
                   no_results_text: "No Results",
                   allow_single_deselect: true, 
                   search_contains:true, });
 $(".Government").trigger("chosen:updated");

    $('[data-toggle="popover"]').popover({ trigger: "hover" }); 
 });

 $(".Government").change(function() {
       
            $.getJSON("/offlineneew/public/government/district/" + $(".Government").val(), function(data) {
 
                var $contractors = $(".District");
                   $(".District").chosen("destroy");
                $contractors.empty();
                $.each(data, function(index, value) {
                    $contractors.append('<option value="' + index +'">' + value + '</option>');
                });
                  
        
             $('.District').chosen({
                width: '100%',
                no_results_text:'No Results'
            });
                $(".District").trigger("chosen:updated");
            $(".District").trigger("change");
            });
  
    
});

</script>



</div>
</section>
@stop