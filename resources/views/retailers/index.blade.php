@extends('masternew')
@section('content')
 <section class="panel-body">
<style type="text/css">
  input[type="file"] {
    display: none;
}
.custom-file-upload {
    border: 1px solid #ccc;
    display: inline-block;
    padding: 6px 12px;
    cursor: pointer;
}

</style>

 <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                     <h2 ><i class="fa fa-tasks" ></i>Retailers </h2>
                        <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                     
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

  

                    <table class="table table-hover table-bordered  dt-responsive nowrap Brands" cellspacing="0" width="100%" >
  <thead>
  <th></th>
        <th>Code</th>
     <!--    <th>Name</th>
        <th>English Name</th>
        <th>Telephone</th>
        <th>Segmentation1</th>
        <th>Segmentation2</th>
        <th>Segmentation3</th>
        <th>Address</th>
        <th>Email</th>
        
        <th>Show License</th>
        <th>Show Location</th>  -->
     <!--    <th>Edit</th> -->
        <th>Process</th> 
      </thead>



<tbody style="text-align:center;">
<?php $i=1;?>
@foreach($Retailers as $Retailer)
            <tr>
              <td>{{$i++}}</td> 
              <td>{{$Retailer->Code}}</td>
              <td><a href="/offlineneew/public/Retailer/destroy/{{$Retailer->id}}" class="btn btn-default btn-sm" ><span class="glyphicon glyphicon-trash"></span></a></td>
            </tr>
@endforeach
</tbody>
</table>


                  </div>
{!!Form::open(['action'=>'RetailerController@importretailers','method' => 'post','files'=>true])!!}

<label for="file-upload" class="custom-file-upload">
<i class="fa fa-cloud-upload"></i> Select File
</label>
<input id="file-upload"  name="file" type="file"/>

<input type="submit" name="submit" value="Import File" class="btn btn-primary" />  
{!!Form::close()!!}
                </div>
              </div>
 
  @section('other_scripts')


<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
    <script type="text/javascript">

  var editor;

    $(document).ready(function(){
          $('#Government').change(function(){
          $.get("{{ url('api/dropdown')}}", 
          { option: $(this).val() }, 
          function(data) {
              $('#district').empty(); 
              $.each(data, function(key, element) {
                  $('#district').append("<option value='" + key +"'>" + element + "</option>");
              });
          });
      });

     
       var table= $('.Brands').DataTable({
    
      select:true,
      responsive: true,
      "order":[[0,"asc"]],
      'searchable':true,
      "scrollCollapse":true,
      "paging":true,
      "pagingType": "simple",
        dom: 'lBfrtip',
          buttons: [
             
              
              { extend: 'excel', className: 'btn btn-primary dtb' }
              
              
          ],

  fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
  $.fn.editable.defaults.send = "always";

  $.fn.editable.defaults.mode = 'inline';

  $('.testEdit').editable({

        validate: function(value) {
          name=$(this).editable().data('name');
          if(name=="Name")
          {
                  if($.trim(value) == '') {
                      return 'Value is required.';
                    }
                      }
        
          },

      placement: 'right',
      url:'{{URL::to("/")}}/updateContractors',
    
       ajaxOptions: {
       type: 'get',
      sourceCache: 'false',
       dataType: 'json'

     },

         params: function(params) {
              // add additional params from data-attributes of trigger element
              params.name = $(this).editable().data('name');

              // console.log(params);
              return params;
          },
          error: function(response, newValue) {
              if(response.status === 500) {
                  return 'This Data Already Exist,Enter Correct Data.';
              } else {
                  return response.responseText;
              }
          }


  });

  }
  });
     $('.Brands tfoot th').each(function () {
        var title = $('.Brands thead th').eq($(this).index()).text();       
       if($(this).index()>=1 && $(this).index()<=1)
              {

             $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
  }
          

      });
    table.columns().every( function () {
    var that = this;
   $(this.footer()).find('input').on('keyup change', function () {
            that.search(this.value).draw();
              if (that.search(this.value) ) {
                 that.search(this.value).draw();
             }
          });
        
      });

   });
</script>
@stop

@stop
