@extends('masternew')
@section('content')
<!DOCTYPE html>
<section class="panel-body">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Energy Sources </h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
          <li><a class="close-link"><i class="fa fa-close"></i></a></li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <a type="button"  class="btn btn-primary" data-toggle="modal" data-target="#myModal1" style="margin-bottom: 20px;"> Add New Energy Sources </a>
        <!-- model --> 
        <div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title span7 text-center" id="myModalLabel"><span class="title">Add New Energy Sources</span></h4>
              </div>
              <div class="modal-body">
                <div class="tabbable"> <!-- Only required for left/right tabs -->
                  <ul class="nav nav-tabs"></ul>
                  <div class="tab-content">
                    <div class="tab-pane active" id="tab1">
                      <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <div class="x_panel">         
                            <div class="x_content">
                  
                              {!! Form::open(['action' => "Energy_sourcesController@store",'method'=>'post','id'=>'profileForm', 'class'=>'form-horizontal form-label-left  ']) !!}  
                                <div class="form-group">
                                    <label class="col-xs-3 control-label"> Energy store 
                                    </label> 
                                    <div class="col-xs-5">
                                        <input type="text" class="form-control" name="name[]" />
                                    </div>
                                    <div class="col-xs-4">
                                        <button type="button" class="btn btn-default addButton"><i class="fa fa-plus"></i></button>
                                    </div>
                                </div>
                                <!-- The template containing an email field and a Remove button -->
                                <div class="form-group hide col-xs-12" id="emailTemplate">
                                    <!-- <div class="col-xs-offset-3 col-xs-5">//Doaa Elgheny -->
                                   <label class="col-xs-3 control-label">
                                    </label> 
                                    <div class="col-xs-5" id="S">
                                        <input class="form-control" type="text" name="name[]" required />
                                    </div>
                                    <div class="col-xs-4">
                                        <button type="button" class="btn btn-default removeButton"><i class="fa fa-minus"></i></button>
                                    </div>
                                </div>
                                <!-- Message container -->
                                <div class="form-group">
                                    <div class="col-xs-9 col-xs-offset-3">
                                        <div id="messageContainer"></div>
                                    </div>
                                </div>
                                <div class="ln_solid"></div>
                                <div class="form-group">
                                  <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                                    <button type="button" class="btn btn-primary " data-dismiss="modal">Cancel</button>
                                   <button class="btn btn-primary" type="reset">Reset</button>
                                    <button type="submit"  name="login_form_submit" class="btn btn-success" value="Save" disabled="disabled">Submit</button>
                                  </div>
                                </div>
                              {!!Form::close()!!}               
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- ENDModal -->
 
<!-- ENDModal -->


<table class="table table-hover table-bordered  dt-responsive nowrap display Brands" cellspacing="0" width="100%">
  <thead>
              <th>No</th>
              <th>Name</th>
              <th>Process</th>          
  </thead>

               

<tbody style="text-align:center;">
  <?php $i=1;?>
  @foreach($energy_sources as $energy_sources)
    <tr>
    
     <td>{{$i++}}</td> 
      <td ><a  class="testEdit" data-type="text"  data-value="{{$energy_sources->name}}" data-name="name"  data-pk="{{ $energy_sources->id }}">{{$energy_sources->name}}</a></td>
  <td>
     <a href="/offlineneew/public/Energy_sources/destroy/{{$energy_sources->id}}"class="btn btn-default btn-sm" ><span class="glyphicon glyphicon-trash"></span>
       </a>
</td>

   </tr>
     @endforeach
</tbody>
    <tfoot>
        
                 <th></th>
              <th>Name</th>

              <th>Process</th> 
                    
                  </tfoot>
</table> 
  </div>
                </div>
              </div>  
    </section>
@section('other_scripts')
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>


  <script type="text/javascript">

var editor;

  $(document).ready(function(){

     $('#profileForm')
        .formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                'name[]': {
                    err: '#messageContainer',
                    validators: {
                            remote: {
                        url: '/offlineneew/public/checkEnergySource',
                        data:function(validator, $field, value)
                        {return{Name:validator.getFieldElements('name[]').val()};},
                        message: 'The Energy source name is not available',
                        type: 'GET'
                    },
                    regexp: {
                        regexp: /^[\u0600-\u06FFA-Za-z\s]+$/,
                        message: 'The full name can only consist of alphabetical and spaces'
                    },
                        callback: {
                            callback: function(value, validator, $field) {
                                var $Names          = validator.getFieldElements('name[]'),
                                    numEmails        = $Names.length,
                                    notEmptyCount    = 0,
                                    obj              = {},
                                    duplicateRemoved = [];

                                for (var i = 0; i < numEmails; i++) {
                                    var v = $Names.eq(i).val();
                                    if (v !== '') {
                                        obj[v] = 0;
                                        notEmptyCount++;
                                    }
                                }

                                for (i in obj) {
                                    duplicateRemoved.push(obj[i]);
                                }

                                if (duplicateRemoved.length === 0) {
                                    return {
                                        valid: false,
                                        message: 'You must fill at least one Name'
                                    };
                                } else if (duplicateRemoved.length !== notEmptyCount) {
                                    return {
                                        valid: false,
                                        message: 'The Name must be unique'
                                    };
                                }

                                validator.updateStatus('name[]', validator.STATUS_VALID, 'callback');
                                return true;
                            }
                        }
                    }
                }
            } ,submitHandler: function(form) {
        form.submit();
    }
        })
        .on('success.form.fv', function(e) {
   // e.preventDefault();
    var $form = $(e.target);

    // Enable the submit button
   
    $form.formValidation('disableSubmitButtons', false);
})
        // Add button click handler
        .on('click', '.addButton', function() {
            var $template = $('#emailTemplate'),
                $clone    = $template
                                .clone()
                                .removeClass('hide')
                                .removeAttr('id')
                                .insertBefore($template),
                $Name    = $clone.find('[name="name[]"]');

            // Add new field
            $('#profileForm').formValidation('addField', $Name);
        })

        // Remove button click handler
        .on('click', '.removeButton', function() {
            var $row   = $(this).closest('.form-group'),
                $Name = $row.find('[name="name[]"]');

            // Remove element containing the Name
            $row.remove();

            // Remove field
            $('#profileForm').formValidation('removeField', $Name);
        });


     var table= $('.Brands').DataTable({
  
    select:true,
    responsive: true,
    "order":[[0,"asc"]],
    'searchable':true,
    "scrollCollapse":true,
    "paging":true,
    "pagingType": "simple",
      dom: 'lBfrtip',
        buttons: [
           
            
            { extend: 'excel', className: 'btn btn-primary dtb' }
            
            
        ],

fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
$.fn.editable.defaults.send = "always";

$.fn.editable.defaults.mode = 'inline';

$('.testEdit').editable({

      validate: function(value) {
        name=$(this).editable().data('name');
        if(name=="name")
        {
                if($.trim(value) == '') {
                    return 'Value is required.';
                  }
                    }
                        if(name=="name")
        {

           var regexp = /^[\u0600-\u06FFA-Za-z\s]+$/;        

           if (!regexp.test(value)) 
           {
                return 'This field is not valid';
           }
            
         }
      
        },

    placement: 'right',
    url:'{{URL::to("/")}}/updateEnergysources',
  
     ajaxOptions: {
     type: 'get',
    sourceCache: 'false',
     dataType: 'json'

   },

       params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');

            // console.log(params);
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'This Data Already Exist,Enter Correct Data.';
            } else {
                return response.responseText;
            }
        }
        ,
        success:function(response)
        {
            if(response.status==="You do not have permission.")
             {
              return 'You do not have permission.';
              }
        }


});

}
});
   $('.Brands tfoot th').each(function () {
      var title = $('.Brands thead th').eq($(this).index()).text();       
     if($(this).index()>=1 && $(this).index()<=6)
            {

           $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
}
        

    });
  table.columns().every( function () {
  var that = this;
 $(this.footer()).find('input').on('keyup change', function () {
          that.search(this.value).draw();
            if (that.search(this.value) ) {
               that.search(this.value).draw();
           }
        });
      
    });
  $.ajaxPrefilter(function( options, original_Options, jqXHR ) {
    options.async = false;
});

 });




  </script>




@stop
@stop