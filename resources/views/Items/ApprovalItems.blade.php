@extends('master')
@section('content')
<!DOCTYPE html>
<meta name="csrf-token" content="{{ csrf_token() }}" />
<style type="text/css">
  input[type=file] {
    display: block;}

}
</style>
<section class="panel panel-primary">
<div class="panel-body">

  <h1 dir="rtl"><i class='fa fa-tasks'></i> الاصناف </h1>




          @if(Session::has('error'))
           
          <div class="alert alert-danger" style="text-align:center;" >
         <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
                  <strong>Whoops!</strong> {{ Session::get('error') }}<br><br>

    
    </div>
    @endif


 <table class="table table-hover table-bordered  dt-responsive nowrap display tasks" cellspacing="0" width="100%" dir="rtl">
  <thead>
              <th>العدد </th>
              <th>الاسم </th>
              <th>الاسم بالانجليزي</th>
             
            <!--   <th>الكمية</th> -->
              <th>اسم القسم</th>
              <th>اسم الماركة</th>
            <th>الحد الادني</th>
              <th>الكود</th>
             
              <!--  <th>صورة  الصنف</th> -->
              <th>Approval</th>          
  </thead>
    <tfoot>
     <th></th>
               
              <th>الاسم </th>
              <th>الاسم بالانجليزي</th>
              
            <!--   <th>الكمية</th> -->
              <th>اسم القسم</th>
              <th>اسم الماركة</th>
            <th>الحد الادني</th>
              <th>الكود</th>
             
              <th>Approval</th>     
                  </tfoot>  
 
               

<tbody style="text-align:center;">
  <?php $i=1;?>
  @foreach($items as $item)
    <tr>
     <td>{{$i++}}</td>
         <td ><a  class="testEdit" data-type="text"  data-value="{{$item->Name}}" data-name="Name"  data-pk="{{ $item->id }}">{{$item->Name}}</a></td>
         
          <td ><a  class="testEdit" data-type="text"  data-value="{{$item->EngName}}" data-name="EngName"  data-pk="{{ $item->id }}">{{$item->EngName}}</a></td>

         <!-- <td ><a  class="testEdit" data-type="number"  data-value="{{$item->price}}" data-name="price"  data-pk="{{ $item->id }}">{{$item->price}}</a></td> -->
         
<!-- <td >{{$item->totalmstock}}</td> -->

 <td > <a class="testEdit" data-type="select"  data-value="{{$item->getitemCategory->Name}}" data-source ="{{$category}}" data-name="category_id"  data-pk="{{$item->id}}"> 
  {{$item->getitemCategory->Name}}
  </a>
 </td>
 <td > <a class="testEdit" data-type="select"  data-value="{{$item->getitemBrand->Name}}" data-source ="{{$brand}}" data-name="brand_id"  data-pk="{{$item->id}}"> 
  {{$item->getitemBrand->Name}}
  </a>
 </td>

<td ><a  class="testEdit" data-type="number"  data-value="{{$item->price}}" data-name="MinLimit"  data-pk="{{ $item->id }}">{{$item->MinLimit}}</a></td>



         <td >{{$item->Code}}</td> 
        
  <td>
     <a href="/cemexmarketingsystem/public/checkapproval/{{$item->id}}"class="btn btn-default btn-sm" ><span class="glyphicon glyphicon-check"></span>
       </a>

</td>
   </tr>
     @endforeach


</tbody>
</table>
</div>
    
    </section>  
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
  <script type="text/javascript">
var editor;

  $(document).ready(function(){
   
       $('#profileForm')
        .formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },

            fields: {
              

                 
                 
                   'Code': {
                  
                    validators: {
                     

             notEmpty:{
                    message:"يجب ادخال الكود ."
                  }
                    
                        
                    }
                }

            
            },submitHandler: function(form) {
        form.submit();
    }
        }).on('success.form.fv', function(e) {
   // e.preventDefault();
    var $form = $(e.target);

    // Enable the submit button
   
    $form.formValidation('disableSubmitButtons', false);
});
     var table= $('.tasks').DataTable({
  
    select:true,
    responsive: true,
    "order":[[0,"asc"]],
    'searchable':true,
    "scrollCollapse":true,
    "paging":true,
    "pagingType": "simple",
      dom: 'lBfrtip',
        buttons: [
            { extend: 'excel', className: 'btn btn-primary dtb' }
        ],

fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
$.fn.editable.defaults.send = "always";

$.fn.editable.defaults.mode = 'inline';

$('.testEdit').editable({

      validate: function(value) {
        name=$(this).editable().data('name');
        if(name=="Name" || name=="totalmstock" || name=="MinLimit")
        {
        if($.trim(value) == '') {
                    return 'يجب ادخال القيمة .';
                  }
                    }
//         if(name=="Name")
//         {

//        var regexp = /^[\u0600-\u06FF\s]+$/;          
// // var regexp = /^[\u0600-\u06FF\s]+$/;  
//            if (!regexp.test(value)) 
//            {
//                 return 'هذا الادخال غير صحيح.';
//            }
            
//          } 
      
      
        },

    placement: 'right',
    url:'{{URL::to("/")}}/updateItems',
  
     ajaxOptions: {
     type: 'get',
    sourceCache: 'false',
     dataType: 'json'

   },

       params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');

            // console.log(params);
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'This Data Already Exist,Enter Correct Data.';
            } else {
                return response.responseText;
            }
        }
        ,    success:function(response)
        {
            if(response.status==="You do not have permission.")
             {
              return 'You do not have permission.';
              }
        }


});

}
});

   $('.tasks tfoot th').each(function () {
      var title = $('.item thead th').eq($(this).index()).text();
               
 if($(this).index()>=1 && $(this).index()<=7)
        {

           $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
        }
        

    });
  table.columns().every( function () {
  var that = this;
 $(this.footer()).find('input').on('keyup change', function () {
          that.search(this.value).draw();
            if (that.search(this.value) ) {
               that.search(this.value).draw();
           }
        });
      
    });

 }); 

  </script>


@stop
