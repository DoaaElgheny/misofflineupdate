@extends('masternew')
@section('content')
<!DOCTYPE html>
<section class="panel-body">
 <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                  <h2><i class="fa fa-lightbulb-o"></i>Prices</h2> 
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                     
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

 


   <a type="button"  class="btn btn-primary" data-toggle="modal" data-target="#myModal1" style="margin-bottom: 20px;"> Add New Prices </a>
  
     <!-- model -->
 
<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
     <div class="modal-content">
    
          <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                 <h4 class="modal-title span7 text-center" id="myModalLabel"><span class="title">Add New Prices</span></h4>
          </div>
    <div class="modal-body">
        <div class="tabbable"> <!-- Only required for left/right tabs -->
        <ul class="nav nav-tabs">
  

        
        </ul>
        <div class="tab-content">
        <div class="tab-pane active" id="tab1">
              <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                 
                  <div class="x_content">
                    <br />
{!! Form::open(['route' => "price.store",'method'=>'post','id'=>'form', 'class'=>'form-horizontal form-label-left  ']) !!} 



<fieldset style="    padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;">
  <legend style="width: fit-content;border: 0px;">Main Data:</legend>
   <div class="form-group">


      <div class="control-label col-md-6 col-sm-6 col-xs-12">

                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Company Name
                        </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                           {!!Form::select('company', ([null => 'Select  company'] + $company->toArray()) ,null,['class'=>' chosen-select ','id' => 'company'])!!}
                        </div>
                        </div>

                         <div class="control-label col-md-6 col-sm-6 col-xs-12">

                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Product Name
                        </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                             {!!Form::select('Product_id', ([null => 'Select  product'] + $ProductShow->toArray()) ,null,['class'=>'chosen-select product ','id' => 'product'])!!}
                        </div>
                        </div>
             

   
                     

                      </div>


     
 <div class="form-group">

   <div class="control-label col-md-6 col-sm-6 col-xs-12">

                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Price
                        </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                           <input type="number"  class="form-control" id="price" name="price" placeholder="price" required>
                        </div>
                        </div>
   
 </div>

 

                 
</fieldset>
        


             <fieldset style="    padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;">
  <legend style="width: fit-content;border: 0px;">Location:</legend>
    



  <div class="form-group">


      <div class="control-label col-md-6 col-sm-6 col-xs-12">

                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Government
                        </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          {!!Form::select('Government', ([null => 'Select  Government'] + $governments->toArray()) ,null,['class'=>' chosen-select ','id' => 'Government','required' =>'required'])!!}
                        </div>
                        </div>

                         <div class="control-label col-md-6 col-sm-6 col-xs-12">

                        <label class="control-label col-md-3 col-sm-3 col-xs-12">District
                        </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                             {!!Form::select('District', ([null => 'Select  Name'] + $districts->toArray()) ,null,['class'=>'form-control chosen-select District ','id' => 'prettify'])!!}
                        </div>
                        </div>
             

   
                     

                      </div>


     
 <div class="form-group">

   <div class="control-label col-md-6 col-sm-6 col-xs-12">

                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Date
                        </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
      <input type="text"  name="Date" placeholder="Date"  class="form-control has-feedback-left Date" id="Date">
      <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                        </div>
                        </div>
   
 </div>
 

                 
</fieldset>
        

                    

                        
                       


                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                          <button type="button" class="btn btn-primary " data-dismiss="modal">Cancel</button>
                         <button class="btn btn-primary" type="reset">Reset</button>
                          <button type="submit"  name="login_form_submit" class="btn btn-success" value="Save" disabled="disabled">Submit</button>
                        </div>
                      </div>

                   
        {!!Form::close() !!}                
        </div>
                </div>
                   </div>
                </div>
        </div>
        </div>
        </div>

    </div>
 

         </div>
     </div>
  </div>
<!-- ENDModal -->
 
     @if(Session::has('error'))
           
    <div class="alert alert-info" style="text-align:center;" >
         <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
                  <strong>Whoops!</strong> {{ Session::get('error') }}<br><br>

    
    </div>
    @endif


 
<table id="prices"  class="table table-hover table-bordered  dt-responsive nowrap display  prices" cellspacing="0" width="100%">
        
<thead>
      <tr>
      <th>No</th>
      <th> Date</th> 
    <th>Price</th>
    <th>Government</th>
    <th>District</th>
    <th>User Name</th>
    <th>Product Name</th>
    <th>Process</th>

    </tr>
</thead>

<tbody>
  <?php $i=1; ?>
   @foreach($prices as $price)
   <tr> 
      <td>{{$i++}}</td>
      <td>
        <a class="testEdit" data-type="text" data-name="Date" data-pk="{{$price->id}}">{{$price->Date}}</a>
      </td>

            <td>
               <a class="testEdit" data-type="number"   data-name="Price" data-pk="{{$price->id}}">{{$price->Price}}</a>
       </td>


    <td>   <a class="testEdit" data-type="select"  data-value="{{$governments}}" data-source ="{{$governments}}" data-name="Government"  data-pk="{{$price->id}}"> 
 {{$price->Government}}
  
  </a></td>

       <td>   <a class="testEdit" data-type="select"  data-value="{{$districts}}" data-source ="{{$districts}}" data-name="District"  data-pk="{{$price->id}}"> 
     {{$price->District}}
      
      </a></td>
      <td></td>

      <td > <a class="testEdit" data-type="select"  data-value="{{$price->getpricesProduct->Cement_Name}}" data-source ="{{$ProductShow}}" data-name="Product_id"  data-pk="{{$price->id}}"> 
      {{$price->getpricesProduct->Cement_Name}}
      </a>
     </td>

      <td><a href="/offlineneew/public/price/destroy/{{$price->id}}"class="btn btn-default btn-sm" ><span class="glyphicon glyphicon-trash"></span>  </a></td>
    </tr>
     @endforeach    
</tbody>

<tfoot>
    <th>No</th>
<th> Date</th> 
    <th>Price</th>
    <th>Government</th>
    <th>District</th>
    <th>User Name</th>
    <th>Product Name</th>
    <th>Process</th>
</tfoot>

</table>

  </div>
                </div>
              </div>  
    </section>
@section('other_scripts')
<script>
$(document).ready(function() {

  $('#Date').daterangepicker({
        singleDatePicker: true,
        singleClasses: "picker_4",
         locale: {
            format: 'YYYY-MM-DD'
        }
      }, function(start) {
        console.log(start.toISOString(), 'Hadeel');
      });
         $('.product').chosen({
                width: '100%',
                no_results_text:'No Results'
            });
                $(".product").trigger("chosen:updated");
            $(".product").trigger("change");
  
    $('#form')
        .find('[name="Government"]')
            .chosen({
                width: '100%',
                inherit_select_classes: true
            })
            // Revalidate the color when it is changed
            .change(function(e) {

                $('#form').formValidation('revalidateField', 'Government');

  $.getJSON("/offlineneew/public/government/district/" + $("#Government").val(), function(data) {
 
                var $contractors = $(".District");
                   $(".District").chosen("destroy");
                $contractors.empty();
                $.each(data, function(index, value) {
                    $contractors.append('<option value="' + index +'">' + value + '</option>');
                });
                  
        
             $('.District').chosen({
                width: '100%',
                no_results_text:'No Results'
            });
                $(".District").trigger("chosen:updated");
            $(".District").trigger("change");
            });
            })
            .end()
                  .find('[name="company"]')
            .chosen({
                width: '100%',
                inherit_select_classes: true
            })
            // Revalidate the color when it is changed
            .change(function(e) {
                         $.getJSON("/company/product/" + $("#company").val(), function(data) {
 
                var $product = $(".product");
                   $(".product").chosen("destroy");
                $product.empty();
                $.each(data, function(index, value) {
                    $product.append('<option value="' + index +'">' + value + '</option>');
                });
                  
        
             $('.product').chosen({
                width: '100%',
                no_results_text:'No Results'
            });
                $(".product").trigger("chosen:updated");
            $(".product").trigger("change");
   });

                $('#form').formValidation('revalidateField', 'company');
            })
            .end()
        .formValidation({
            framework: 'bootstrap',
            excluded: ':disabled',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                Government: {
                    validators: {
                        callback: {
                            message: 'Please choose Government',
                            callback: function(value, validator, $field) {
                                // Get the selected options
                                var options = validator.getFieldElements('Government').val();
                              
                                return (options !== '' );
                            }
                        }
                    }
                },
                     company: {
                    validators: {
                        callback: {
                            message: 'Please choose company',
                            callback: function(value, validator, $field) {
                                // Get the selected options
                                var options = validator.getFieldElements('company').val();
                              
                                return (options !== '' );
                            }
                        }
                    }
                },'Date': {
                    validators: {
                         notEmpty:{
                            message:"the Date is required"
                          }
                  }
                },
                 
                     'price': {
                    validators: {
             notEmpty:{
                    message:"the price is required"
                  }   
                    }
                },
                     'District': {
                    validators: {
             notEmpty:{
                    message:"the District is required"
                  }
                   
                    }
                }
                  ,
                     'Product_id': {
                    validators: {
                 

             notEmpty:{
                    message:"the Product is required"
                  }
                   
                        
                    }
                }
            },submitHandler: function(form) {
        form.submit();
    }
        }).on('success.form.fv', function(e) {
    e.preventDefault();
    var $form = $(e.target);

    // Enable the submit button
   
    $form.formValidation('disableSubmitButtons', false);
});

});
</script>


<script type="text/javascript">
$(document).ready(function(){
    var table= $('.prices').DataTable({   
        responsive: true,
        "order":[[0,"asc"]],
        'searchable':true,
        "scrollCollapse":true,
        "paging":true,
        "pagingType": "simple",
        dom: 'Bfrtip',
          buttons: [
               { extend: 'excel', className: 'btn btn-primary dtb' }
          ],
           fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {

      $.fn.editable.defaults.send = "always";
      $.fn.editable.defaults.mode = 'inline';

      $('.testEdit').editable({ 
       
        url:'{{URL::to("/")}}/priceupdate',
        ajaxOptions: {
            type: 'get',
            sourceCache: 'false',
            dataType: 'json'
        }, 
        validate: function(value) {

          
          name=$(this).editable().data('name');
         if(name=="Date"||name=="Price"||name=="Government"||name=="District"||name=="Product_id")
          {
                if($.trim(value) == '') {
                    return 'Value is required.';
                  }
           }

          if(name=="Date")
        {

           var regexp = /^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/;          

           if (!regexp.test(value)) 
           {
                return 'This field is not valid';
           }
            
         } 


            },
        params: function(params) {
            params.name = $(this).editable().data('name');
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'Wrong.. Please Try again';
            } else {
                  return response.responseText;
            }
        }
          ,    success:function(response)
        {
            if(response.status==="You do not have permission.")
             {
              return 'You do not have permission.';
              }
        }

      }); 
      }
  });

  $('.prices tfoot th').each(function () {
      var title = $('.prices thead th').eq($(this).index()).text();
          $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
          
  });

  table.columns().every( function () {
      var that = this;
    $(this.footer()).find('input').on('keyup change', function () {
            that.search(this.value).draw();
          if (that.search(this.value) ) {
              that.search(this.value).draw();
          }
      });     
      });
   $(".company").change(function() {


$.getJSON("/price/products/" + $(".company").val(), function(data) {

                                   var $product = $(".product");
             
                  
                                   $(".product").chosen("destroy");
                
                                   $product.empty();
                                   $product.append('<option value="0">Pleas Select Product</option>');
                                   $.each(data, function(index, value) {
                                   $product.append('<option value="' + index +'">'+value+'</option>');
                                    });

                                 $(".product").chosen({
                                   width: '100%',
                                   no_results_text:"No Result Found !!..",
                                   allow_single_deselect: true, 
                                   search_contains:true, });

                                 $(".product").trigger("chosen:updated");

                                 });
});

//create pop-up modal
$.validator.addMethod("alpha", function(value, element) {
    return this.optional(element) || /^[a-zA-Z\u0600-\u06FF,-][\sa-zA-Z\u0600-\u06FF,-]/i.test(value);
 });

 $(".Government").trigger("chosen:updated");

    $('[data-toggle="popover"]').popover({ trigger: "hover" }); 
  
 $(".company").trigger("chosen:updated");

    $('[data-toggle="popover"]').popover({ trigger: "hover" }); 

});




</script>


@stop
@stop