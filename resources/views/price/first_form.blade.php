@extends('master')
@section('content')

<div class="col-md-2"></div>
<div class="col-md-8">
<h3><b>Prices Report</b></h3><br/>
{!! Form::open(array('url' => 'FirstResult')) !!}

 <div class="form-group">
                <label for="input_goverment" class="control-label col-md-3">Goverment</label>
 {!!Form::select('goverment', array(
                         '0' => 'برجاء أختيار محافظه',
                        'أسيوط' => 'أسيوط', 
                        'المنيا' => 'المنيا',
                        'الإسكندرية' => 'الإسكندرية',
                        'الإسماعيلية' => 'الإسماعيلية',
                        'أسوان' => 'أسوان',
                        'الأقصر' => 'الأقصر',
                        'البحر الأحمر' => 'البحر الأحمر',
                        'البحيرة' => 'البحيرة',
                        'بني سويف' => 'بني سويف',
                        'بورسعيد' => 'بورسعيد',
                        'جنوب سيناء' => 'جنوب سيناء',
                        'الجيزة' => 'الجيزة',
                        'الدقهلية' => 'الدقهلية',
                        'دمياط' => 'دمياط',
                        'سوهاج' => 'سوهاج',
                        'السويس' => 'السويس',
                        'الشرقية' => 'الشرقية',
                        'شمال سيناء' => 'شمال سيناء',
                        'الغربية' => 'الغربية',
                        'الفيوم' => 'الفيوم',
                        'القاهرة' => 'القاهرة',
                        'القليوبية' => 'القليوبية',
                        'قنا' => 'قنا',
                        'كفر الشيخ' => 'كفر الشيخ',
                        'مطروح' => 'مطروح',
                        'المنوفية' => 'المنوفية',
                        'الوادي الجديد' => 'الوادي الجديد',
                       ),null,['class'=>'form-control chosen-select government','id' => 'goverment']);!!}
</div>

<div class="form-group">
    <label for="cement_name">Cement Name</label>
    <select style="width:400px;" id="cement_name" name="cement_name" class="chosen">
      <option value ="">Chose Cement Name</option>
        @foreach ($products as $product)
          <option value ="{{$product->id}}">{{$product->cement_name}}</option>
        @endforeach
    </select>
    <span class="label label-danger">{{ $errors->first('cement_name') }}</span></td>
</div>

<div class="form-group"> 
<label for="from">Date From</label>
      <input type="date" name="from" class="form-control">
      <span class="label label-danger">{{ $errors->first('from') }}</span></td>
</div>

<div class="form-group"> 
<label for="to">Date From</label>
      <input type="date" name="to" class="form-control">
      <span class="label label-danger">{{ $errors->first('to') }}</span></td>
</div>

<div class="form-group"> 
  <input type="submit" class="btn btn-primary" value="Generate Report" ></td>
</div>


 {!! Form::close() !!}

</div>
<div class="col-md-2"></div>


<script type="text/javascript">       
    $(document).ready(function() {     
      $('#cement_name')  .chosen({
              width: '100%',
              no_results_text: 'لا توجد نتيجة',
              search_contains:true,
              allow_single_deselect: true
        });

      $('#goverment')  .chosen({
              width: '100%',
              no_results_text: 'لا توجد نتيجة',
              search_contains:true,
              allow_single_deselect: true
        });

    });
</script>


@stop

