@extends('masternew')
@section('content')

<section class="panel-body">
 <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                   <h2><i class="fa fa-tasks"></i> Price Report</h2> 
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                     
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

    <h1><i class='fa fa-tasks'></i> Report </h1>


<!--  {!! Form::open(['id'=>'profileForm', 'class'=>'form-horizontal form-label-left  ']) !!}  
 -->{!!Form::open(['action'=>'PriceController@chartpricesanalysis','method' => 'post','files'=>true,'class'=>'form'])!!}


<fieldset style="    padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;">
  <legend style="width: fit-content;border: 0px;"> Price Report :</legend>

                       
<div class="form-group">
        <div class="control-label col-md-6 col-sm-6 col-xs-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"> Start Date</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                                    
     <input type='text' name="StartDate"  placeholder=" Start Date" class="form-control has-feedback-left StartDate" id="StartDate"  >
     <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                        </div>
                     </div>


                       <div class="control-label col-md-6 col-sm-6 col-xs-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"> End Date</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                                    
<input type='text' name="EndDate"  placeholder=" Start Date" class="form-control has-feedback-left EndDate" id="EndDate"  >
     <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                        </div>
                     </div>
</div>
</br>
</br>
</br>
  <div class="form-group">
        <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
            <label class="col-xs-3 control-label">Choose Region:</label>
              <div class="col-xs-9">
                <select  placeholder="Choose Region"  class="form-control" name="Region" id="Region">
                    <option>Upper</option>
                    <option>Lower</option>                 
                </select>
               </div>
              </div>
        </div>
 
<div class="row" style="margin-top: 60px;" align="center" >
                                <div  >
              <button type="button" onclick="ShowMReport()" id="Search" class="btn btn-primary">Show Report</button>
         <input type="submit" name="product" value="Download Report" class="btn btn-primary"/>  

            </div>  
            </div>

                 
</fieldset>
        
      {!!Form::close() !!} 

         </div>
    <div class="col-md-12" id='Charts' >
          
              </div>

                </div>
              </div>
         @section('other_scripts') 
 <script type="text/javascript">
 
$(function(){

  $('#StartDate').daterangepicker({
        singleDatePicker: true,
        singleClasses: "picker_4",
         locale: {
            format: 'YYYY-MM-DD'
        }
      }, function(start) {
        // console.log(start.toISOString(), 'Hadeel');
      });
  $('#EndDate').daterangepicker({
        singleDatePicker: true,
        singleClasses: "picker_4",
         locale: {
            format: 'YYYY-MM-DD'
        }
      }, function(start) {
        // console.log(start.toISOString(), 'Hadeel');
      });
});


 function ShowMReport()
 {
   $("#Charts" ).empty(); 
 $.ajax({
      url: "/offlineneew/public/ShChartPrice",
        data:{StartDate:$('#StartDate').val(),EndDate:$('#EndDate').val(),Region:$('#Region').val()},
       type: "Get",

       success: function(data){



var canvas_data =[];
        console.log( data.data.length); 


        for (var i = 0; i < data.data.length; i++) 
        {
               $( "#Charts" ).append( `<div class="x_panel">
                  <div class="x_title">
                    <h2> ${data.data[i]['Gov']} </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                   
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content" >
                    <div class="demo-container" style="height:280px" > 
                    <div id="canvasMPrice${i}" class="demo-placeholder" ></div></div>
                  </div>
                </div>` );
               var canvas_data =[];

             var Prices=data.data[i];
             console.log(Prices,Prices['price']);
          for (var j = 0; j< Prices['price'].length; j++) 
                      {
                  console.log(Prices['price'][j].data );

                      canvas_data.push({
                       type: "column", 
                       name: Prices['price'][j].name,
                       indexLabel: "{y}",
                       indexLabelOrientation:"vertical",
                       showInLegend: true, 
                       indexLabelFontSize: 16,
                      dataPoints:Prices['price'][j].data });
        }
        var id="canvasMPrice"+i;
         CanvasJS.addColorSet("greenShades",
                [
                "#adabab",
                "#c6d9f1",
                "#8eb4e3",
                "#558ed5",
                ]);
           var chart = new CanvasJS.Chart(id,
              {
                  theme: "theme3",
                  exportEnabled: true,
                   animationEnabled: true,
                   zoomEnabled: true,
                   axisX:{
                   labelFontColor: "black",
                   labelMaxWidth: 100,
                   labelAutoFit: true ,
                   labelFontWeight: "bold"
                 },
                   axisY:{ minimum: 600},

                   colorSet: "greenShades",
                   dataPointWidth:40,
                 toolTip: {
                        shared: true,
                        content: "{name}: {y}"
                  },          
                        dataPointWidth: 20,
                         data:canvas_data,

legend:{
fontSize: 15,
cursor:"pointer",
itemclick: function(e){
  
if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
e.dataSeries.visible = false;
}
else {
e.dataSeries.visible = true;
}
chart.render();
}
}
              });

      chart.render();
        }
           
   
          }
           

// updateChart();
//     setInterval(function(){updateChart()}, 20000);
    

});
}
</script>
@stop
@stop