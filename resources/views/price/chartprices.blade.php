@extends('masternew')
@section('content')

<section class="panel-body">

 <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                   <h2><i class="fa fa-tasks"></i> Weekly Price Report</h2> 
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                     
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">



<!--  {!! Form::open(['id'=>'profileForm', 'class'=>'form-horizontal form-label-left  ']) !!}  
 -->{!!Form::open(['action'=>'PriceController@chartpricesanalysis','method' => 'post','files'=>true,'class'=>'form'])!!}


<fieldset style="    padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;">
  <legend style="width: fit-content;border: 0px;"> Weekly Price:</legend>

                       
<div class="form-group">
        <div class="control-label col-md-6 col-sm-6 col-xs-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"> Start Date</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                                    
     <input type='text' name="StartDate"  placeholder=" Start Date" class="form-control has-feedback-left StartDate" id="StartDateweek"  >
     <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                        </div>
                     </div>


                       <div class="control-label col-md-6 col-sm-6 col-xs-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"> End Date</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                                    
<input type='text' name="EndDate"  placeholder=" Start Date" class="form-control has-feedback-left EndDate" id="EndDateweek"  >
     <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                        </div>
                     </div>
</div>
</br>
</br>
</br>
  <div class="form-group">
        <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
            <label class="col-xs-3 control-label">Choose Region:</label>
              <div class="col-xs-9">
                <select  placeholder="Choose Region"  class="form-control" name="Region" id="Region">
                    <option>Upper</option>
                    <option>Lower</option>                 
                </select>
               </div>
              </div>
        </div>
 
<div class="row" style="margin-top: 60px;" align="center" >
                                <div  >
              <button type="button" onclick="ShowWeeklyReport()" id="Search" class="btn btn-primary">Show Report</button>
         <input type="submit" name="product" value="Download Report" class="btn btn-primary"/>  

            </div>  
            </div>

                 
</fieldset>
        
      {!!Form::close() !!} 

         </div>
    <div class="col-md-12" id='Charts' >
          
              </div>

                </div>
              </div>


 <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                   <h2><i class="fa fa-tasks"></i> Weekly Price /Company/Government/Product Report</h2> 
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                     
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">




 {!! Form::open(['id'=>'profileForm', 'class'=>'form-horizontal form-label-left  ']) !!}  
<fieldset style="    padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;">
  <legend style="width: fit-content;border: 0px;">Weekly Price /Company/Government/Product :</legend>
    <div class="form-group">


      <div class="control-label col-md-6 col-sm-6 col-xs-12">

                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Government
                        </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                               {!!Form::select('Government',([null => 'please chooose governments'] + $governments->toArray()) ,null,['class'=>'form-control subuser_list chosen-select Government','id' => 'Government','required'=>'required'])!!}
                        </div>
                        </div>
                      <div class="control-label col-md-6 col-sm-6 col-xs-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"> District</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                                    
{!!Form::select('District',([null => 'please chooose districts'] + $districts->toArray()) ,null,['class'=>'form-control subuser_list  chosen-select District','id' => 'District'])!!}

 
</select>
                        </div>
                     </div>

   
                     

                      </div>
                      
<div class="form-group">
        <div class="control-label col-md-6 col-sm-6 col-xs-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"> Company</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                                    
     {!!Form::select('company[]', ( ['all' => 'All Companies'] +$company->toArray()) ,null,['class'=>'form-control chosen-select company ','id' => 'company','name'=>'company','multiple' => true,'required'=>'required'])!!}
                        </div>
                     </div>


                       <div class="control-label col-md-6 col-sm-6 col-xs-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"> Product</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                                    
{!!Form::select('product[]', ( $product->toArray()) ,null,['class'=>'form-control chosen-select product ','id' => 'product','name'=>'product','multiple' => true,'required'=>'required'])!!}
                        </div>
                     </div>
</div>
 
<div class="form-group">
        <div class="control-label col-md-6 col-sm-6 col-xs-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"> Start Date</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                                    
     <input type='text' name="StartDate"  placeholder=" Start Date" class="form-control has-feedback-left StartDate" id="StartDate"  >
     <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                        </div>
                     </div>


                       <div class="control-label col-md-6 col-sm-6 col-xs-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"> End Date</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                                    
<input type='text' name="EndDate"  placeholder=" Start Date" class="form-control has-feedback-left EndDate" id="EndDate"  >
     <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                        </div>
                     </div>
</div>
 
<div class="row" style="margin-top: 20px;" align="center" >
                                <div  >
              <button type="button" onclick="ShowReport()" id="Search" class="btn btn-primary">Show Report</button>
              
            </div>  
            </div>
                 
</fieldset>
        
      {!!Form::close() !!}                
</br>
</br>


    <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Weekly Price  Chart </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                   
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                     <div class="demo-container" style="height:280px" >   
                    <div id="canvasPrice" class="demo-placeholder" ></div>   
                                  
                    </div>
                  </div>
                </div>
              </div>

<!-- Price Per Month -->


 {!! Form::open(['id'=>'profileForm', 'class'=>'form-horizontal form-label-left  ']) !!}  
<fieldset style="    padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;">
  <legend style="width: fit-content;border: 0px;"> Annual Price /Company/Government/Product :</legend>
    <div class="form-group">


      <div class="control-label col-md-6 col-sm-6 col-xs-12">

                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Government
                        </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                               {!!Form::select('MGovernment',(['all' => 'All Governments']+ $governments->toArray()) ,null,['class'=>'form-control subuser_list chosen-select Government','id' => 'MGovernment','required'=>'required'])!!}
                        </div>
                        </div>
                      <div class="control-label col-md-6 col-sm-6 col-xs-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"> District</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                                    
{!!Form::select('MDistrict',([null => 'please chooose districts'] + $districts->toArray()) ,null,['class'=>'form-control subuser_list  chosen-select District','id' => 'MDistrict'])!!}

 
</select>
                        </div>
                     </div>

   
                     

                      </div>
                      
<div class="form-group">
        <div class="control-label col-md-6 col-sm-6 col-xs-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"> Company</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                                    
     {!!Form::select('Mcompany[]', ( ['all' => 'All Companies'] +$company->toArray()) ,null,['class'=>'form-control chosen-select Mcompany ','id' => 'Mcompany','name'=>'Mcompany','multiple' => true,'required'=>'required'])!!}
                        </div>
                     </div>


                       <div class="control-label col-md-6 col-sm-6 col-xs-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"> Product</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                                    
{!!Form::select('Mproduct[]', ( $product->toArray()) ,null,['class'=>'form-control chosen-select Mproduct ','id' => 'Mproduct','name'=>'Mproduct','multiple' => true,'required'=>'required'])!!}
                        </div>
                     </div>
</div>
 
  <div class="form-group">
              <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                <label class="col-xs-3 control-label">Choose Year:</label>
                <div class="col-xs-9">
                  <select  placeholder="Choose Year"  class="form-control" name="Start" id="Start">
                       <option>2018</option>
                    <option>2017</option>
                    <option>2016</option>
                 
                  </select>
                </div>
              </div>
            </div>
 
<div class="row" style="margin-top: 20px;" align="center" >
                                <div  >
              <button type="button" onclick="ShowMReport()" id="Search" class="btn btn-primary">Show Report</button>
              
            </div>  
            </div>
                 
</fieldset>
        
      {!!Form::close() !!}                
</br>
</br>


    <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2> Annual Price Chart </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                   
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                     <div class="demo-container" style="height:280px" >   
 <div id="canvasMPrice" class="demo-placeholder" ></div>   
    
                    </div>
                  </div>
                </div>
              </div>


<!-- 
               {!! Form::open(['id'=>'profileForm', 'class'=>'form-horizontal form-label-left  ']) !!}  
<fieldset style="    padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;">
  <legend style="width: fit-content;border: 0px;">Activity Report:</legend>
    <div class="form-group">


  
 
<div class="form-group">
        <div class="control-label col-md-6 col-sm-6 col-xs-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"> Start Date</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                                    
     <input type='text' name="StartDate1"  placeholder=" Start Date" class="form-control has-feedback-left StartDate" id="StartDate1"  >
     <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                        </div>
                     </div>


                       <div class="control-label col-md-6 col-sm-6 col-xs-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"> End Date</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                                    
<input type='text' name="EndDate1"  placeholder=" Start Date" class="form-control has-feedback-left EndDate" id="EndDate1"  >
     <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                        </div>
                     </div>
</div>
 
<div class="row" style="margin-top: 20px;" align="center" >
                                <div  >
             <button type="button" onclick="ShowReport1()" id="Search" class="btn btn-primary">Show Report</button>
              
            </div>  
            </div>
                 
</fieldset>
        
      {!!Form::close() !!}                
</br>
</br>

 -->
<!--     <div class="col-md-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Activity Chart </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                   
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                     <div class="demo-container" style="height:280px" >   
                    <div id="canvasActivity" class="demo-placeholder" ></div>   
                                  
                    </div>
                  </div>
                </div>
              </div> -->

                 </div>
                </div>
              </div>
         @section('other_scripts') 
 <script type="text/javascript">

$(function(){

  $('#StartDateweek').daterangepicker({
        singleDatePicker: true,
        singleClasses: "picker_4",
         locale: {
            format: 'YYYY-MM-DD'
        }
      }, function(start) {
        // console.log(start.toISOString(), 'Hadeel');
      });
  $('#EndDateweek').daterangepicker({
        singleDatePicker: true,
        singleClasses: "picker_4",
         locale: {
            format: 'YYYY-MM-DD'
        }
      }, function(start) {
        // console.log(start.toISOString(), 'Hadeel');
      });
});

 function ShowWeeklyReport()
 {
   $("#Charts" ).empty(); 
 $.ajax({
      url: "/offlineneew/public/ShChartPrice",
        data:{StartDate:$('#StartDateweek').val(),EndDate:$('#EndDateweek').val(),Region:$('#Region').val()},
       type: "Get",

       success: function(data){



var canvas_data =[];
        console.log( data.data.length); 


        for (var i = 0; i < data.data.length; i++) 
        {
               $( "#Charts" ).append( `<div class="x_panel">
                  <div class="x_title">
                    <h2> ${data.data[i]['Gov']} </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                   
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content" >
                    <div class="demo-container" style="height:280px" > 
                    <div id="canvasMPrice${i}" class="demo-placeholder" ></div></div>
                  </div>
                </div>` );
               var canvas_data =[];

             var Prices=data.data[i];
             console.log(Prices,Prices['price']);
          for (var j = 0; j< Prices['price'].length; j++) 
                      {
                  console.log(Prices['price'][j].data );

                      canvas_data.push({
                       type: "column", 
                       name: Prices['price'][j].name,
                       indexLabel: "{y}",
                       indexLabelOrientation:"vertical",
                       showInLegend: true, 
                       indexLabelFontSize: 16,
                      dataPoints:Prices['price'][j].data });
        }
        var id="canvasMPrice"+i;
         CanvasJS.addColorSet("greenShades",
                [
                "#adabab",
                "#c6d9f1",
                "#8eb4e3",
                "#558ed5",
                ]);
           var chart = new CanvasJS.Chart(id,
              {
                  theme: "theme3",
                  exportEnabled: true,
                   animationEnabled: true,
                   zoomEnabled: true,
                   axisX:{
                   labelFontColor: "black",
                   labelMaxWidth: 100,
                   labelAutoFit: true ,
                   labelFontWeight: "bold"
                 },
                   axisY:{ minimum: 600},

                   colorSet: "greenShades",
                   dataPointWidth:40,
                 toolTip: {
                        shared: true,
                        content: "{name}: {y}"
                  },          
                        dataPointWidth: 20,
                         data:canvas_data,

legend:{
fontSize: 15,
cursor:"pointer",
itemclick: function(e){
  
if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
e.dataSeries.visible = false;
}
else {
e.dataSeries.visible = true;
}
chart.render();
}
}
              });

      chart.render();
        }
           
   
          }
           

// updateChart();
//     setInterval(function(){updateChart()}, 20000);
    

});
}
    function ShowReport1()
{
 
 $.ajax({
      url: "/offlineneew/public/Activitycharts",
        data:{StartDate:$('#StartDate1').val(),EndDate:$('#EndDate1').val()},
       type: "Get",
       success: function(data){

        updateChart = function () {

 $.getJSON("/offlineneew/public/Activitycharts", function(result) {
    
                          
            
      var chart = new CanvasJS.Chart("canvasActivity",
              {
                   theme: "theme3",
                   colorSet: "greenShades",
                   exportEnabled: true,
                   animationEnabled: true,
                   zoomEnabled: true,
                   axisY:{
                    minimum:0,
                    interval:1
     

    } ,
                   axisX:{
                   labelFontColor: "black",
                   labelMaxWidth: 100,
                   labelAutoFit: true ,
                   labelFontWeight: "bold"


                 },
    
                 toolTip: {
                      shared: true
                  },          
                        dataPointWidth: 20,
          data: [ 
              {
                indexLabelFontSize: 16,
    indexLabelFontColor: "black",
                type: "column", 
                
                name: "Activity Number",
                legendText: "Activity Number",
                showInLegend: true, 
            
                              dataPoints:data.activity
                      }
                  
                  ]
              });
      chart.render();
          });
           
};
updateChart();
    setInterval(function(){updateChart()}, 20000);
    

}
});
}
 function ShowReport()
 {
 
 $.ajax({
      url: "/offlineneew/public/ShowReportPrice",
        data:{product:$('#product').val(),company:$('#company').val(),StartDate:$('#StartDate').val(),Government:$('#Government').val(),EndDate:$('#EndDate').val(),District:$('#District').val()},
       type: "Get",

       success: function(data){



var canvas_data =[];
        console.log(data); 

        for (var i = 0; i < data.price.length; i++) 
        {
// var name= data.price[i].name;
//                   console.log(name);
                      canvas_data.push({
                       type: "line", 
                
                       name: data.price[i].name,
              
                       showInLegend: true, 
            
                      dataPoints:data.price[i].data
          });
        
        }
           
      var chart = new CanvasJS.Chart("canvasPrice",
              {
                  theme: "theme3",
                  exportEnabled: true,
                   animationEnabled: true,
                   zoomEnabled: true,
                   axisX:{
                   labelFontColor: "black",
                  labelMaxWidth: 100,

                   labelAutoFit: true ,
   labelFontWeight: "bold"


                 },
                   axisY:{ minimum: 600},

    
                 toolTip: {
                      shared: true,
                        content: "{name}: {y}"
                  },          
                        dataPointWidth: 20,
          data:canvas_data
              });

      chart.render();
          }
           

// updateChart();
//     setInterval(function(){updateChart()}, 20000);
    

});
}

 function ShowMReport()
 {
 
 $.ajax({
      url: "/offlineneew/public/ShowReportMPrice",
        data:{product:$('#Mproduct').val(),company:$('#Mcompany').val(),Start:$('#Start').val(),Government:$('#MGovernment').val(),District:$('#MDistrict').val()},
       type: "Get",

       success: function(data){



var canvas_data =[];
        console.log(data); 

        for (var i = 0; i < data.price.length; i++) 
        {
// var name= data.price[i].name;
//                   console.log(name);
                      canvas_data.push({
                       type: "line", 
                
                       name: data.price[i].name,
              
                       showInLegend: true, 
            
                      dataPoints:data.price[i].data
          });
        
        }
           
      var chart = new CanvasJS.Chart("canvasMPrice",
              {
                  theme: "theme3",
                  exportEnabled: true,
                   animationEnabled: true,
                   zoomEnabled: true,
                   axisX:{
                   labelFontColor: "black",
                  labelMaxWidth: 100,

                   labelAutoFit: true ,
   labelFontWeight: "bold"


                 },
                   axisY:{ minimum: 600},

    
                 toolTip: {
                      shared: true,
                        content: "{name}: {y}"
                  },          
                        dataPointWidth: 20,
          data:canvas_data
              });

      chart.render();
          }
           

// updateChart();
//     setInterval(function(){updateChart()}, 20000);
    

});
}
$(function(){

  $(".Government").chosen({ 
                   width: '100%',
                   no_results_text: "No Results",
                   allow_single_deselect: true, 
                   search_contains:true, });
 $(".Government").trigger("chosen:updated");

  $(".District").chosen({ 
                   width: '100%',
                   no_results_text: "No Results",
                   allow_single_deselect: true, 
                   search_contains:true, });
 $(".District").trigger("chosen:updated");
  $('#StartDate').daterangepicker({
        singleDatePicker: true,
        singleClasses: "picker_4",
         locale: {
            format: 'YYYY-MM-DD'
        }
      }, function(start) {
        console.log(start.toISOString(), 'Hadeel');
      });
  $('#EndDate').daterangepicker({
        singleDatePicker: true,
        singleClasses: "picker_4",
         locale: {
            format: 'YYYY-MM-DD'
        }
      }, function(start) {
        console.log(start.toISOString(), 'Hadeel');
      });
    $('#StartDate1').daterangepicker({
        singleDatePicker: true,
        singleClasses: "picker_4",
         locale: {
            format: 'YYYY-MM-DD'
        }
      }, function(start) {
        console.log(start.toISOString(), 'Hadeel');
      });
  $('#EndDate1').daterangepicker({
        singleDatePicker: true,
        singleClasses: "picker_4",
         locale: {
            format: 'YYYY-MM-DD'
        }
      }, function(start) {
        console.log(start.toISOString(), 'Hadeel');
      });
  $('.product').chosen({
                width: '100%',
                no_results_text:'No Results'
            });
                $(".product").trigger("chosen:updated");
            $(".product").trigger("change");

  $('.Mproduct').chosen({
                width: '100%',
                no_results_text:'No Results'
            });
                $(".Mproduct").trigger("chosen:updated");
            $(".Mproduct").trigger("change");

 });

   $(document).ready(function(){

$('#Government').change(function(){
        $.get("{{ url('api/dropdown')}}", 
        { option: $(this).val() }, 
        function(data) {
           $('#District').empty();  
               $('#District').append("<option value='null'>choose District</option>");
            $.each(data, function(key, element) {
                $('#District').append("<option value='" + key +"'>" + element + "</option>");
            });
            $("#District").chosen({ 
                   width: '100%',
                   no_results_text: "No Results",
                   allow_single_deselect: true, 
                   search_contains:true, });
 $("#District").trigger("chosen:updated");

        });
    });


 $('#MGovernment').change(function(){
        $.get("{{ url('api/dropdown')}}", 
        { option: $(this).val() }, 
        function(data) {
           $('#MDistrict').empty();  
               $('#MDistrict').append("<option value='null'>choose District</option>");
            $.each(data, function(key, element) {
                $('#MDistrict').append("<option value='" + key +"'>" + element + "</option>");
            });
            $("#MDistrict").chosen({ 
                   width: '100%',
                   no_results_text: "No Results",
                   allow_single_deselect: true, 
                   search_contains:true, });
 $("#MDistrict").trigger("chosen:updated");

        });
    });



  $(".company").chosen({ 
                   width: '100%',
                   no_results_text: "No Results",
                   allow_single_deselect: true, 
                   search_contains:true, });
 $(".company").trigger("chosen:updated");

    $('[data-toggle="popover"]').popover({ trigger: "hover" }); 


 $(".company").change(function() {
       
            $.getJSON("/offlineneew/public/company/product/" + $(".company").val(), function(data) {
 
                var $product = $(".product");
                   $(".product").chosen("destroy");
                $product.empty();
                $.each(data, function(index, value) {
                    $product.append('<option value="' + index +'">' + value + '</option>');
                });
                  
        
             $('.product').chosen({
                width: '100%',
                no_results_text:'No Results'
            });
                $(".product").trigger("chosen:updated");
            $(".product").trigger("change");
   });
            });

//Chart Per Month

 $(".Mcompany").chosen({ 
                   width: '100%',
                   no_results_text: "No Results",
                   allow_single_deselect: true, 
                   search_contains:true, });
 $(".Mcompany").trigger("chosen:updated");

    $('[data-toggle="popover"]').popover({ trigger: "hover" }); 


 $(".Mcompany").change(function() {
       
            $.getJSON("/offlineneew/public/company/product/" + $(".Mcompany").val(), function(data) {
 
                var $Mproduct = $(".Mproduct");
                   $(".Mproduct").chosen("destroy");
                $Mproduct.empty();
                $.each(data, function(index, value) {
                    $Mproduct.append('<option value="' + index +'">' + value + '</option>');
                });
                  
        
             $('.Mproduct').chosen({
                width: '100%',
                no_results_text:'No Results'
            });
                $(".Mproduct").trigger("chosen:updated");
            $(".Mproduct").trigger("change");
   });
            });




   });
  </script>
@stop
@stop