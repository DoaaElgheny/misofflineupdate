@extends('master')
@section('content')

<div id="perf_div"></div>
{!! \Lava::render('LineChart', 'MyStocks', 'perf_div') !!}

<section class="panel panel-primary">
<div class="panel-body">

<table id="price" style="width:100%" class="table table-hover table-bordered dt-responsive nowrap display price" cellspacing="0">
        
<thead>
      <tr>
      <th>No</th>
        	<th>Price Date</th> 
            <th>Price</th>
            <th>Price Gov</th>
      		<th>Price City</th>
          	<th>Action</th>                 
    </tr>
</thead>

<tbody>
  <?php $x=1; ?>
  @foreach($PricesTable as $price)
    <tr> 
      	<td>{{$x++}}</td>
      	<td>{{$price->date}}</td>
		<td>{{$price->price}}</td>      
      	<td>{{$price->goverment}}</td>
      	<td>{{$price->city}}</td>
      	<td><a href="/offlineneew/public/price/destroy/{{$price->id}}"class="btn btn-default btn-sm" ><span class="glyphicon glyphicon-trash"></span>  </a></td>
    </tr>
  @endforeach     
</tbody>

<tfoot>
    <th>No</th>
    <th>Price Date</th>
    <th>Price</th> 
    <th>Price Gov</th>
    <th>Price City</th>
    <th>Action</th>
</tfoot>

</table>

</div>
</section>


<script type="text/javascript">
$(document).ready(function(){
    var table= $('.price').DataTable({   
		    responsive: true,
		    "order":[[0,"asc"]],
		    'searchable':true,
		    "scrollCollapse":true,
		    "paging":true,
		    dom: 'Bfrtip',
	        buttons: [
                { extend: 'excel', className: 'btn btn-primary dtb' }
        	]
	});

	$('.price tfoot th').each(function () {
	    var title = $('.price thead th').eq($(this).index()).text();
	        $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
	        
	});

	table.columns().every( function () {
	  	var that = this;
		$(this.footer()).find('input').on('keyup change', function () {
		        that.search(this.value).draw();
			    if (that.search(this.value) ) {
			        that.search(this.value).draw();
			    }
			});     
	    });

});



</script>

@stop
