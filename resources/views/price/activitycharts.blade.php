@extends('masternew')
@section('content')

<section class="panel-body">
 <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                   <h2><i class="fa fa-tasks"></i> Activity Report</h2> 
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                     
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

   <!--  <h1><i class='fa fa-tasks'></i> Report </h1>

 -->



 {!! Form::open(['id'=>'profileForm', 'class'=>'form-horizontal form-label-left  ']) !!}  
<fieldset style="    padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;">
  <legend style="width: fit-content;border: 0px;">Main Data:</legend>
    <div class="form-group">


  
 
<div class="form-group">
        <div class="control-label col-md-6 col-sm-6 col-xs-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"> Start Date</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                                    
     <input type='text' name="StartDate"  placeholder=" Start Date" class="form-control has-feedback-left StartDate" id="StartDate"  >
     <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                        </div>
                     </div>


                       <div class="control-label col-md-6 col-sm-6 col-xs-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"> End Date</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                                    
<input type='text' name="EndDate"  placeholder=" Start Date" class="form-control has-feedback-left EndDate" id="EndDate"  >
     <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                        </div>
                     </div>
</div>
 
<div class="row" style="margin-top: 20px;" align="center" >
                                <div  >
             <button type="button" onclick="ShowReport()" id="Search" class="btn btn-primary">Show Report</button>
              
            </div>  
            </div>
                 
</fieldset>
        
      {!!Form::close() !!}                
</br>
</br>


    <div class="col-md-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Activity Chart </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                   
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                     <div class="demo-container" style="height:280px" >   
                    <div id="canvasActivity" class="demo-placeholder" ></div>   
                                  
                    </div>
                  </div>
                </div>
              </div>

                 </div>
                </div>
              </div>

         @section('other_scripts') 
      <script type="text/javascript">
$(function(){
 
 $('#StartDate').daterangepicker({
        singleDatePicker: true,
        singleClasses: "picker_4",
         locale: {
            format: 'YYYY-MM-DD'
        }
      }, function(start) {
        console.log(start.toISOString(), 'Hadeel');
      });
  $('#EndDate').daterangepicker({
        singleDatePicker: true,
        singleClasses: "picker_4",
         locale: {
            format: 'YYYY-MM-DD'
        }
      }, function(start) {
        console.log(start.toISOString(), 'Hadeel');
      });

   });
    function ShowReport()
{
 
 $.ajax({
      url: "/offlineneew/public/Activitycharts",
        data:{StartDate:$('#StartDate').val(),EndDate:$('#EndDate').val()},
       type: "Get",
       success: function(data){

        updateChart = function () {

 $.getJSON("/offlineneew/public/Activitycharts", function(result) {
    
                          
            
      var chart = new CanvasJS.Chart("canvasActivity",
              {
                   theme: "theme3",
                   colorSet: "greenShades",
                   exportEnabled: true,
                   animationEnabled: true,
                   zoomEnabled: true,
                   axisY:{
                    minimum:0,
                    interval:1
     

    } ,
                   axisX:{
                   labelFontColor: "black",
                   labelMaxWidth: 100,
                   labelAutoFit: true ,
                   labelFontWeight: "bold"


                 },
    
                 toolTip: {
                      shared: true
                  },          
                        dataPointWidth: 20,
          data: [ 
              {
                indexLabelFontSize: 16,
    indexLabelFontColor: "black",
                type: "column", 
                
                name: "Activity Number",
                legendText: "Activity Number",
                showInLegend: true, 
            
                              dataPoints:data.activity
                      }
                  
                  ]
              });
      chart.render();
          });
           
};
updateChart();
    setInterval(function(){updateChart()}, 20000);
    

}
});
}
  
  </script>
@stop
@stop