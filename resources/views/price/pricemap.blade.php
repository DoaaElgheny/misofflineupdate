@extends('masternew')
@section('content')


    <style>
    
       #mapAC {
        width: 50%; 
        height: 70%;
      }
      #legend {
        font-family: Arial, sans-serif;
        background: #fff;
        padding: 10px;
        margin: 10px;
        border: 3px solid #000;
      }
      #legend h3 {
        margin-top: 0;
      }
      #legend img {
        vertical-align: middle;
      }
    </style>
 <section class="panel-body">
 <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                   <h2><i class="fa fa-group"></i> Gps Promoter Report</h2> 
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                     
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
   
 
 
    <div id="mapAC" style="height: 800px;width: 100%"></div>
    <div id="legend"><h3>Legend</h3></div>
    </div>
  </div>
    </div>
  

 

</section>
      @section('other_scripts')
    <script>
      var map;
      function initMap() {
        map = new google.maps.Map(document.getElementById('mapAC'), {
           zoom: 8,
              center: new google.maps.LatLng(28.000, 32.444),
          mapTypeId: 'roadmap'
        });

        var iconBase = 'https://maps.google.com/mapfiles/kml/shapes/';
        var icons = {
          parking: {
            name: 'Activity',
            icon: '/offlineneew/public/assets/img/truck.png'
          },
          library: {
            name: 'Library',
            icon: iconBase + 'library_maps.png'
          },
          Makwam: {
            name: 'Makwam',
            icon:'/offlineneew/public/assets/img/Makwam.png'
          },
          Sa3eed: {
            name: 'Sa3eed',
            icon:'/offlineneew/public/assets/img/Sa3eed.png'
          }
        };

         var jArrayprice = {!!json_encode($pricestotal)!!};
console.log(jArrayprice);
         var jArrayActivity = {!!json_encode($activitytotal)!!};
         var jArrayTotalContractor={!!json_encode($totalContractorsArray)!!};
   var jArrayCoverContractor={!!json_encode($CoveredContractorArray)!!};
var features =[];
var featuresActivity =[];
  var featuresTotalContractor =[];  
    var featuresCoverContractor =[];    
for (i = 0; i < jArrayprice.length; i++) 
{

     if(jArrayprice[i].product=='أسمنت المقاوم رتبه 32')
                  {
              features.push( 
               {

                
                      position: new google.maps.LatLng(jArrayprice[i].Glat, jArrayprice[i].Glong),
                      type: 'Makwam',
                      Name:jArrayprice[i].product,
                       Price:jArrayprice[i].price,

                    }
                  
              );

}

     if(jArrayprice[i].product=='أسمنت الصعيد')
                  {
              features.push( 
               {

                
                      position: new google.maps.LatLng(jArrayprice[i].Glat, jArrayprice[i].Glong),
                      type: 'Sa3eed',
                      Name:jArrayprice[i].product,
                       Price:jArrayprice[i].price,

                    }
                  
              );

}

}


for (i = 0; i < jArrayActivity.length; i++) 
{

    
              featuresActivity.push( 
               {

                
                      position: new google.maps.LatLng(jArrayActivity[i].Glat, jArrayActivity[i].Glong),
                      type: 'parking',
                      Total:jArrayActivity[i].Total

                    }
                  
              );




}


for (i = 0; i < jArrayCoverContractor.length; i++) 
{

    
              featuresCoverContractor.push( 
               {

                
                      position: new google.maps.LatLng(jArrayCoverContractor[i].Glat, jArrayCoverContractor[i].Glong),
                      type: 'library',
                      Total:jArrayCoverContractor[i].Total

                    }
                  
              );




}


for (i = 0; i < jArrayTotalContractor.length; i++) 
{

    
              featuresTotalContractor.push( 
               {

                 "Assuit":{
          center: {lat:parseFloat(jArrayTotalContractor[i].Glat), lng:parseFloat(jArrayTotalContractor[i].Glong)},
          population: jArrayTotalContractor[i].Total
        }
        

                    }
                  
              );




}
  
 for (var city in featuresTotalContractor) {
console.log(featuresTotalContractor[city]["Assuit"].population);
          // Add the circle for this city to the map.
          var cityCircle = new google.maps.Circle({
            strokeColor: '#FF0000',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: '#FF0000',
            fillOpacity: 0.35,
            map: map,
            center: featuresTotalContractor[city]["Assuit"].center,
            radius: Math.sqrt(featuresTotalContractor[city]["Assuit"].population) * 700
          });
        }




        // Create markers.

  var infowindow = new google.maps.InfoWindow();
   features.forEach(function(feature) {

          var markerprice = new google.maps.Marker({
            position: feature.position,
            icon: icons[feature.type].icon,
            map: map
          });

           google.maps.event.addListener(markerprice, 'click', (function(marker, i) {
          return function() {           
            infowindow.setContent('Product Name '+feature.Name+' <br/> Price '+ feature.Price);
            infowindow.open(map, markerprice);
          }
        })(markerprice, i));



        });
    featuresActivity.forEach(function(featuresActivity) {

          var markerActivity = new google.maps.Marker({
            position: featuresActivity.position,
            icon: icons[featuresActivity.type].icon,
            map: map
          });

           google.maps.event.addListener(markerActivity, 'click', (function(marker, i) {
          return function() {           
            infowindow.setContent('Count '+featuresActivity.Total);
            infowindow.open(map, markerActivity);
          }
        })(markerActivity, i));



        });


    featuresCoverContractor.forEach(function(featuresCoverContractor) {

          var markerCoverContractor = new google.maps.Marker({
            position: featuresCoverContractor.position,
            icon: icons[featuresCoverContractor.type].icon,
            map: map
          });

           google.maps.event.addListener(markerCoverContractor, 'click', (function(marker, i) {
          return function() {           
            infowindow.setContent('Count '+featuresCoverContractor.Total);
            infowindow.open(map, markerCoverContractor);
          }
        })(markerCoverContractor, i));



        });

        var legend = document.getElementById('legend');
        for (var key in icons) {

          var type = icons[key];
          var name = type.name;
          var icon = type.icon;
          var div = document.createElement('div');
          div.innerHTML = '<img src="' + icon + '"> ' + name;
          legend.appendChild(div);
        }

        map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(legend);


      }
    </script>
 <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCVlcsFShbXwxnPdklmXmHZiqhwL6JBaac&callback=initMap"
  type="text/javascript"></script>

@stop
@stop