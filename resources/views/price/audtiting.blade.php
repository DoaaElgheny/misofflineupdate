@extends('master')
@section('content')

<div class="container">
  <div class="row" >

    <div class="Absolute-Center is-Responsive"  >
                {!! Form::open(['action'=>'PriceController@history','method'=>'post']) !!}

      <legend>History</legend>
<div class="form-group">
                <label for="input_company" class="control-label col-md-3">company</label>
                <div class="col-md-9">
                  {!!Form::select('company', ([null => 'Select Company name'] + $company->toArray()) ,null,['class'=>'form-control chosen-select  company','id' => 'prettify','name'=>'company'])!!}
                  {!!$errors->first('company','<small class="label label-danger">:message</small>')!!}
                 </div>
            </div>

               <div class="form-group">
                <label for="input_product" class="control-label col-md-3">Product</label>
                <div class="col-md-9">
                <select class="form-control chosen-select product"  name="product" id="prettify" >
                <option value="0">Pleas Select Product</option>
              </select>
                  {!!$errors->first('product','<small class="label label-danger">:message</small>')!!}

                 </div>
            </div>
         <div class="form-group text-center">
    <button type="submit" class="btn btn-primary">Done</button>
  </div>
  {!!Form::close()!!}
          
      </div>  
    </div>    
  </div>
</div>
<script type="text/javascript">
$(document).ready(function(){


   $(".company").chang(function() {


      $.getJSON("/offlineneew/public/price/products/" + $(".company").val(), function(data) {

                                   var $product = $(".product");
             // console.log( $(".company").val();
                  
                                   $(".product").chosen("destroy");
                
                                   $product.empty();
                                   $product.append('<option value="0">Pleas Select Product</option>');
                                   $.each(data, function(index, value) {
                                   $product.append('<option value="' + index +'">'+value+'</option>');
                                    });

                                 $(".product").chosen({
                                   width: '100%',
                                   no_results_text:"No Result Found !!..",
                                   allow_single_deselect: true, 
                                   search_contains:true, });

                                 $(".product").trigger("chosen:updated");

                                 });
});
  });
</script>

@stop