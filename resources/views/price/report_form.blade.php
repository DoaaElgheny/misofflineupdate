@extends('master')
@section('content')

<div class="col-md-2"></div>
<div class="col-md-8">
<h3><b>Prices Report</b></h3><br/>
{!! Form::open(array('url' => 'GenerateReport')) !!}

<div class="form-group">
    <label for="cement_name">Cement Name</label>
    <select style="width:400px;" id="cement_name" name="cement_name" class="chosen">
      <option value ="">Chose Cement Name</option>
        @foreach ($products as $product)
          <option value ="{{$product->cement_name}}">{{$product->cement_name}}</option>
        @endforeach
    </select>
    <span class="label label-danger">{{ $errors->first('cement_name') }}</span></td>
</div>

<div class="form-group"> 
<label for="from">Date From</label>
      <input type="date" name="from" class="form-control">
      <span class="label label-danger">{{ $errors->first('from') }}</span></td>
</div>

<div class="form-group"> 
<label for="to">Date From</label>
      <input type="date" name="to" class="form-control">
      <span class="label label-danger">{{ $errors->first('to') }}</span></td>
</div>

<div class="form-group"> 
  <input type="submit" class="btn btn-primary" value="Generate Report" ></td>
</div>


 {!! Form::close() !!}

</div>
<div class="col-md-2"></div>
<script type="text/javascript">       
    $(document).ready(function() {     
      $('#cement_name')  .chosen({
              width: '100%',
              no_results_text: 'لا توجد نتيجة',
              search_contains:true,
              allow_single_deselect: true
        }); 
    });
</script>


@stop

