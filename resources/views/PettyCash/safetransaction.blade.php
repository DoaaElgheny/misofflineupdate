@extends('PettyCash.MenusidebarPettyCash')
@section('contentasd')

  <style type="text/css">
  #blah_Billout{
  background: url(assets/img/noimage.jpg) no-repeat;
  background-size: cover;
  height: 60px;
  width: 60px;
}
#rcorners1 {
    border-radius: 25px;
  
    padding: 20px; 
    width: 150px;
    height: 80px;    
}
</style>



 <section class="panel-body">
 <center><h1> حركات الخزينة </h1></center>
  <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                          <div class="x_content">
  <script src="/assets/js/bootstrap-datepicker.ar.js" ></script>

<a type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal1" style="margin-bottom: 20px; margin-left:85% " > اضافة حركة  جديدة  </a>
<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
     <div class="modal-content">
    
          <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                 <h4 class="modal-title span7 text-center" id="myModalLabel"><span class="title">اضافة حركة جديدة</span></h4>
          </div>


   {!! Form::open(['action' => "PettyCashController@storeDatsafetrans",'method'=>'post','id'=>'profileForm', 'files' => 'true']) !!}  
             
             <fieldset style="    padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;" dir="rtl">
  <legend style="width: fit-content;border: 0px;" dir="rtl">بيانات اساسية</legend>
 

    
       
<div class="form-group col-md-12" dir="rtl">
        <label class=" control-label">تاريخ الايصال</label>


<div class="col-md-9 col-sm-9 col-xs-12" dir="rtl">
                        <span class="fa fa-calendar-o form-control-feedback right  " aria-hidden="true"></span>

                          <input type="text" class="form-control has-feedback-right inventorytexbox" id="single_cal4" name="Date" placeholder="Date" required>
                        </div>

   
    </div>


          <div class="form-group col-md-12">
                
                <div class="col-md-9" dir="rtl">
                    <input type="text" class="form-control inventorytexbox" id="PNumer" name="PNumer" placeholder="ادخل المبلغ " >
                </div>
                <label for="input_product" class="control-label col-md-3">   المبلغ</label>
            </div>
         
           
         
             <div class="form-group col-md-12">
                
                <div class="col-md-9" dir="rtl">
               <input type="text" class="form-control inventorytexbox" id="Note" name="Note" placeholder="ادخل ملاحظات">
                </div>
                <label for="input_Enname" class="control-label  col-md-3"> ملاحظات </label>
            </div>



   <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12 " style="text-align: center;">
                          <button type="button" class="btn btn-danger " data-dismiss="modal">الغاء</button>
                         <button class="btn btn-info" type="reset">اعادة ادخال</button>
     <button type="submit"  name="login_form_submit" class="btn btn-success" value="save">حفظ</button>
                        </div>
                      </div>
            
 </fieldset>
             <fieldset style="    padding: .35em .625em .75em; margin: 0 2px; 
        
                      <div class="ln_solid"></div>
                   
                      <br><br><br>

      {!!Form::close() !!}  


        </div>
      </div>

    




     <table class="table table-hover table-bordered  dt-responsive nowrap  purchase" cellspacing="0" width="100%" dir="rtl" dir="rtl">
  <thead>
   
        <th></th>
              <th>التاريخ </th>
              <th>القيمة </th>  
         
              <th>ملاحظات</th> 
              <th></th>
      
                      
  </thead>
  <tfoot>
   
        
                    
              
</tfoot>


<tbody style="text-align:center;">
<?php $i=1;?>

  @foreach($safetransaction as $EXP)
    <tr>
    
        <td>{{$i++}}</td>
        <td >{{$EXP->Datetrans}}</td>
        <td >{{$EXP->Balance}}</td>
        
        <td >{{$EXP->Notes}}</td>
        <td>
        <a href="/offlineneew/public/Deletesafe/{{$EXP->id}}"  class="btn btn-default btn-sm" ><span class="glyphicon glyphicon-trash" ></span></a><td>
       
  
  
   </tr>
     @endforeach


<!--  -->
</tbody>
</table>


                  



                  </div>
                </div>
              </div>
 
  @section('other_scripts')


<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>



  <script type="text/javascript">

var editor;

  $(document).ready(function(){


    $("#BillOut-upload").on("change", function()
    {
    
        var files = !!this.files ? this.files : [];

        if (!files.length || !window.FileReader) 
        {
           $("#blah_Billout").css("background-image",'url(assets/img/noimage.jpg)');
        } // no file selected, or no FileReader support
 
        else if (/^image/.test( files[0].type)){ // only image file
            var reader = new FileReader(); // instance of the FileReader
            reader.readAsDataURL(files[0]); // read the local file
 
            reader.onloadend = function(){ // set image data as background of div
                $("#blah_Billout").css("background-image", "url("+this.result+")");
            }
        }
    });
/////////////////////

$('#startDatePicker').datepicker({
     language: 'ar',
     format: "yyyy-mm-dd"
}).on('changeDate', function(e) {
            // Revalidate the start date field
            $('#profileForm').formValidation('revalidateField', 'Pdate');
        });;


 

     var table= $('.purchase').DataTable({
  
    select:true,
    responsive: true,
    "order":[[0,"asc"]],
    'searchable':true,
    "scrollCollapse":true,
    "paging":true,
    "pagingType": "simple",
      dom: 'lBfrtip',
        buttons: [
           
            
            { extend: 'excel', className: 'btn btn-primary dtb' }
            
            
        ],

fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
$.fn.editable.defaults.send = "always";

$.fn.editable.defaults.mode = 'inline';

$('.testEdit').editable({

      validate: function(value) {
        name=$(this).editable().data('name');
        if(name=="PurchesNo"||name=="Date")
        {
                if($.trim(value) == '') {
                    return 'Value is required.';
                  }
                    }
      
       
         if(name=="Date")
        {

      var regexp = /^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/;            
      if (!regexp.test(value)) {
            return 'This field is not valid';
        }
        
        }
    },
       
    placement: 'right',
    url:'{{URL::to("/")}}/updatePurches',
  
     ajaxOptions: {
     type: 'get',
    sourceCache: 'false',
     dataType: 'json'

   },

       params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');

            // console.log(params);
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'This Data Already Exist,Enter Correct Data.';
            } else {
                return response.responseText;
            }
        }
        ,
        success:function(response)
        {
            if(response.status==="You do not have permission.")
             {
              return 'You do not have permission.';
              }
        }


});

}
});



   
 });


  </script>
@stop
@stop
