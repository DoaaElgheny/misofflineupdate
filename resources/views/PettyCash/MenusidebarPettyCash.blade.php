@extends('masternew')
@section('content')

  <style type="text/css">
  #blah_Billout{
  background: url(assets/img/noimage.jpg) no-repeat;
  background-size: cover;
  height: 60px;
  width: 60px;
}
#rcorners1 {
    border-radius: 25px;
  
    padding: 20px; 
    width: 150px;
    height: 80px;    
}
</style>
 <section class="panel-body">
	<div class="x_panel" style="background-color: steelblue;">
		<p style="color: white;
    font-weight: bold;
    font-size: 36px;
    margin-left: 50%;">رصيدك الحالي  :<strong>{{Auth::user()->total()}}</strong></p>
		
	</div>
</section>
 <section class="panel-body">
	<div class="x_panel">
		<div class="col-md-12">
			<div class="col-md-2"><a href="/offlineneew/public/safetransaction"><p id="rcorners1" style="background-color: #4CB175;color:white;font-size:20px;">الخزنة <br><strong style="margin-left:20%;font-size:bold">  {{Auth::user()->safetransaction()}} </strong></p></a></div>


@permission('permissionPettycash')

			<div class="col-md-2"><a href="/offlineneew/public/totalPetty"><p id="rcorners1" style="background-color: #D5cc46;color:white;font-size:20px">التسوية   <i class="fa fa-money"></i></p></a></div>

			<div class="col-md-2"><a href ="/offlineneew/public/showChartPetty"><p id="rcorners1" style="background-color: #413E2B;color:white;font-size:20px">رسم بياني   <i class="fa fa-money"></i></p></a></div>

			<div class="col-md-2"><a href ="/offlineneew/public/ShowReportPetty"><p id="rcorners1" style="background-color: #50677B;color:white;font-size:20px">تقرير  المصروفات   <i class="fa fa-money"></i></p></a>
			</div>
 @endpermission
			<div class="col-md-2"><a href="/offlineneew/public/PettyCash"><p id="rcorners1" style="background-color: #cc5959;color:white;font-size:20px">ادخال المصروفات  <i class="fa fa-money"></i></p></a></div>

@permission('permissionPettycash')
			<div class="col-md-2"><a href="/offlineneew/public/Delivery"><p id="rcorners1" style="background-color: #4CB175;color:white;font-size:18px">تسليم النقدية  <i class="fa fa-money"></i></p></a></div>

		</div>
		 @endpermission
		
	</div>
</section>

 <section class="panel-body">
 	       
  @yield('contentasd')
 </section>

@stop
