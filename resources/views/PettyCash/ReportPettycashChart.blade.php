@extends('PettyCash.MenusidebarPettyCash')
@section('contentasd')
<!DOCTYPE html>
<meta name="csrf-token" content="{{ csrf_token() }}" />

<section class="panel-body">
   <center><h1> تقرير  الرسم البياني  </h1></center>
 <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
               <h2><i class="fa fa-lightbulb-o"></i>Petty Cash Report</h2> 

                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                     
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">



         <fieldset style="padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;">
  <legend style="width:fit-content;border: 0px;">Enter Date:</legend>
 

 
                    <div class="form-group">

                      <div class="col-md-4 col-sm-4 col-xs-12 has-feedback">
                        <input type='text' name="Start_Date"  id="Start_Date" placeholder=" Start Date" class="form-control has-feedback-left Start_Date"  >


                        <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                         {!!$errors->first('Start_Date','<small class="label label-danger">:message</small>')!!}

                      </div>
                       <div class="col-md-4 col-sm-4 col-xs-12 has-feedback">
                        <input type='text' name="End_Date" id="End_Date" placeholder="End Date" class="form-control has-feedback-left End_Date">


                        <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                         {!!$errors->first('End_Date','<small class="label label-danger">:message</small>')!!}


                      </div>

    <div class="col-md-4 selectContainer">
    <div class="input-group">
        <span class="input-group-addon" style="background-color:#3c8dbc; "><i style="color:white;" class="glyphicon glyphicon-list"></i></span>

     {!!Form::select('Cat_iD',([null => 'please chooose Category'] + $ExpenseCat->toArray()) ,null,['class'=>'form-control selectpicker','id' => 'Cat_iD'])!!}
  </div>
</div>



                      </div>
  
                    
       <div >
        <label style="color:red" id="warn"></label> 
     </div>
 <div class="row" style="margin-top: 20px;" align="center" >
                                <div  >
            <input type="button" class="btn btn-primary" onclick="ShowReport();" id="Search" value=" Press To Show Report" title="Press To Show Report">
              
            </div>  
            </div> 
</fieldset>
 


   
                     <!--  <button class="btn btn-success" style="margin-top: 20px; " id="Export">Export</button> -->

   <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="dashboard_graph x_panel">
              <div class="row x_title">
                <div class="col-md-6">
                  <h3>Expense Category</h3>
                </div>
                <div class="col-md-6">
                </div>
              </div>
              <div class="x_content">
                <div class="demo-container" style="height:280px" >
                  <div id="chartContainer" class="demo-placeholder" ></div>               
                </div>
              </div>
            </div>
          </div>
        </div>
   
     <fieldset style="padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;">
  <legend style="width:fit-content;border: 0px;">Enter Date:</legend>
 

 
                    <div class="form-group">

                      <div class="col-md-4 col-sm-4 col-xs-12 has-feedback">
                        <input type='text' name="Start_Date2"  id="Start_Date2" placeholder=" Start Date" class="form-control has-feedback-left Start_Date"  >


                        <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                         {!!$errors->first('Start_Date','<small class="label label-danger">:message</small>')!!}

                      </div>
                       <div class="col-md-4 col-sm-4 col-xs-12 has-feedback">
                        <input type='text' name="End_Date2" id="End_Date2" placeholder="End Date" class="form-control has-feedback-left End_Date">


                        <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                         {!!$errors->first('End_Date','<small class="label label-danger">:message</small>')!!}


                      </div>

    <div class="col-md-4 selectContainer">
    <div class="input-group">
        <span class="input-group-addon" style="background-color:#3c8dbc; "><i style="color:white;" class="glyphicon glyphicon-list"></i></span>

     {!!Form::select('User_iD',([null => 'please chooose User'] + $Users->toArray()) ,null,['class'=>'form-control selectpicker','id' => 'User_iD'])!!}
  </div>
</div>



                      </div>
  
                    
       <div >
        <label style="color:red" id="warn"></label> 
     </div>
 <div class="row" style="margin-top: 20px;" align="center" >
                                <div  >
            <input type="button" class="btn btn-primary" onclick="ShowReportUser();" id="Search" value=" Press To Show Report" title="Press To Show Report">
              
            </div>  
            </div> 
</fieldset>
 <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="dashboard_graph x_panel">
              <div class="row x_title">
                <div class="col-md-6">
                  <h3>User Expenses </h3>
                </div>
                <div class="col-md-6">
                </div>
              </div>
              <div class="x_content">
                <div class="demo-container" style="height:280px" >
                  <div id="chartContainer2" class="demo-placeholder" ></div>               
                </div>
              </div>
            </div>
          </div>
        </div>
  
  </div>
    </div>

  </div>

</section>
  

 @section('other_scripts')
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>


  <script type="text/javascript">

var editor;
$(function(){
  $('#Start_Date').daterangepicker({
        singleDatePicker: true,
        singleClasses: "picker_4",
         locale: {
            format: 'YYYY-MM-DD'
        }
      }, function(start) {
        console.log(start.toISOString(), 'Hadeel');
      });
  $('#End_Date').daterangepicker({
        singleDatePicker: true,
        singleClasses: "picker_4",
         locale: {
            format: 'YYYY-MM-DD'
        }
      }, function(start) {
        console.log(start.toISOString(), 'Hadeel');
      });


 


 });
$(function(){
  $('#Start_Date2').daterangepicker({
        singleDatePicker: true,
        singleClasses: "picker_4",
         locale: {
            format: 'YYYY-MM-DD'
        }
      }, function(start) {
        console.log(start.toISOString(), 'Hadeel');
      });
  $('#End_Date2').daterangepicker({
        singleDatePicker: true,
        singleClasses: "picker_4",
         locale: {
            format: 'YYYY-MM-DD'
        }
      }, function(start) {
        console.log(start.toISOString(), 'Hadeel');
      });


 


 });

   function ShowReport()
{
  
 
    
     $.ajax({
      url: "/offlineneew/public/ChartPettyCash",
        data:{Cat_iD:$('#Cat_iD').val(),Start_Date:$('#Start_Date').val(),End_Date:$('#End_Date').val()},
      type: "Get",
     success: function(data){
                          
      var chart = new CanvasJS.Chart("chartContainer",
              {
                  theme: "theme3",
                  exportEnabled: true,
                   animationEnabled: true,
                   zoomEnabled: true,
                   axisX:{
                   labelFontColor: "black",
                  labelMaxWidth: 100,

                   labelAutoFit: true ,
                     labelFontWeight: "bold"


                 },
    
                 toolTip: {
                      shared: true
                  },          
                        dataPointWidth: 20,
          data: [ 
                 {
                type: "column", 
                name: " Expense Category",
                legendText: " Item",
                showInLegend: true, 
                 indexLabel: "{y}",
                   indexLabelFontSize: 16,
                indexLabelFontColor: "black",
                dataPoints:data.Items


              }                 
                  ]
              
        });
      chart.render();
}
     });







}
 function ShowReportUser()
{
  
 
    
     $.ajax({
      url: "/MIS/ChartPettyCashUser",
        data:{User_iD:$('#User_iD').val(),Start_Date2:$('#Start_Date2').val(),End_Date2:$('#End_Date2').val()},
      type: "Get",
     success: function(data){
                          
      var chart = new CanvasJS.Chart("chartContainer2",
              {
                  theme: "theme3",
                  exportEnabled: true,
                   animationEnabled: true,
                   zoomEnabled: true,
                   axisX:{
                   labelFontColor: "black",
                  labelMaxWidth: 100,

                   labelAutoFit: true ,
                     labelFontWeight: "bold"


                 },
    
                 toolTip: {
                      shared: true
                  },          
                        dataPointWidth: 20,
          data: [ 
                 {
                type: "column", 
                name: " Expense Category",
                legendText: " Item",
                showInLegend: true, 
                 indexLabel: "{y}",
                   indexLabelFontSize: 16,
                indexLabelFontColor: "black",
                dataPoints:data.Items


              }                 
                  ]
              
        });
      chart.render();
}
     });







}


  </script>
  <script>
  
      $(function() {
        $("#Export").click(function(){
        $("#tasks").table2excel({
          exclude: ".noExl",
          name: "Excel Document Name",
          filename: "Stock_In",
          fileext: ".xls",
          exclude_img: true,
          exclude_links: true,
          exclude_inputs: true
        });
         });
      });
    </script>
@stop
@stop
