@extends('masternew')
@section('content')
<section class="panel-body">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
            <li><a class="close-link"><i class="fa fa-close"></i></a></li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
        <div class="row" >
          <div class="col-md-12 col-sm-12 col-xs-12" >
           <fieldset style="padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;">
  <legend style="width: fit-content;border: 0px;"><h2><i class="fa fa-bar-chart"></i> Contractor Covered  Per Month</h2></legend> 
  <div class="row" >
    <div class="form-group col-md-offset-1 form-horizontal">
      <div class="col-md-3 col-sm-3 col-xs-12 has-feedback">
        <label class="control-label">Government:</label>
      </div>
      <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
        {!!Form::select('Government[]', ($Government->toArray()) ,null,['class'=>'form-control chosen-select Government ','id' => 'Government','name'=>'Government','required'=>'required','multiple' => true])!!}
      </div>
    </div>
  </div>
  <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: 20px;" align="center" >
    <div>
      <input type="button" class="btn btn-primary" id="Search" value=" Press To Show Report" title="Press To Show Report"  onclick="ShowReport()" >
    </div> 
    <label style="color:red" id="warn"></label>  
  </div>                       
</fieldset> 
</div>  
        </div>
        <div class="row" style="height:20px;">
        </div>
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="dashboard_graph x_panel">
              <div class="row x_title">
                <div class="col-md-6">
                  <h3>Contractor Covered  Per Month </h3>
                </div>
                <div class="col-md-6">

                </div>
              </div>
              <div class="x_content">
                <div class="demo-container" style="height:280px" >
                  <div id="canvaspromoter" class="demo-placeholder" ></div>               
                </div>
              </div>
            </div>
          </div>
        </div>
   <div class="row" >
          <div class="col-md-12 col-sm-12 col-xs-12" >
           <fieldset style="padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;">
  <legend style="width: fit-content;border: 0px;"><h2><i class="fa fa-bar-chart"></i> Contractor Covered  Per District</h2></legend> 
  <div class="row" >
    <div class="form-group col-md-offset-1 form-horizontal">
      <div class="col-md-3 col-sm-3 col-xs-12 has-feedback">
        <label class="control-label">Government:</label>
      </div>
      <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
        {!!Form::select('Government[]', ($Government->toArray()) ,null,['class'=>'form-control chosen-select Government ','id' => 'Government1','name'=>'Government1','required'=>'required','multiple' => true])!!}
      </div>
    </div>
  </div>
  <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: 20px;" align="center" >
    <div>
      <input type="button" class="btn btn-primary" id="Search1" value=" Press To Show Report" title="Press To Show Report"  onclick="ShowReport1()" >
    </div> 










    <label style="color:red" id="warn1"></label>  
  </div>                       
</fieldset> 
</div>  
        </div>
        <div class="row" style="height:20px;">
        </div>
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="dashboard_graph x_panel">
              <div class="row x_title">
                <div class="col-md-6">
                  <h3>Contractor Per District </h3>
                </div>
                <div class="col-md-6">
                </div>
              </div>
              <div class="x_content">
                <div class="demo-container" style="height:280px" >
                  <div id="canvaspromoter1" class="demo-placeholder" ></div>               

                </div>
              </div>
            </div>
          </div>
        </div>
         </div>  

                  </div>
                </div>
</section>
@section('other_scripts') 
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
   <script type="text/javascript">

 $("#Government").chosen({ 
                   width: '100%',
                   no_results_text: "No Results",
                   allow_single_deselect: true, 
                   search_contains:true, });
 $("#Government").trigger("chosen:updated");
$("#Government1").chosen({ 
                   width: '100%',
                   no_results_text: "No Results",
                   allow_single_deselect: true, 
                   search_contains:true, });
 $("#Government1").trigger("chosen:updated");
function ShowReport()
{
   if($('#Government').val()!="")
   {

    document.getElementById("warn").innerHTML="";

        $.ajax({
      url: "/offlineneew/public/showupperCountchart",data:{Government:$('#Government').val()},
       type: "Get",
         success: function(data){      
      var chart = new CanvasJS.Chart("canvaspromoter",
              {
                  theme: "theme3",
                  exportEnabled: true,
                   animationEnabled: true,
                   zoomEnabled: true,
                   axisX:{
                   labelFontColor: "black",
                   labelMaxWidth: 100,

                   labelAutoFit: true ,
                   labelFontWeight: "bold"


                 },
    
                 toolTip: {
                      shared: true
                  },          
                        dataPointWidth: 20,
          data: [ 
              {
                indexLabelFontSize: 16,
                indexLabelFontColor: "black",
                type: "column", 
                 indexLabel: "{y}  " ,
                name: " Total contractors",
                legendText: "Total contractors",
                showInLegend: true, 
            
                   dataPoints:data.TotalContractor
                      },
                       {
                indexLabelFontSize: 16,
                indexLabelFontColor: "black",
                type: "column", 
                 indexLabel: "{y}  " ,
                name: "Covered contractors by Promoter",
                legendText: "Covered contractors by Promoter",
                showInLegend: true, 
            
                  
                    dataPoints:data.TotalPromoter}
                //       },
                //       {
                // indexLabelFontSize: 16,
                // indexLabelFontColor: "black",
                // type: "column", 
                //  indexLabel: "{y}  " ,
                // name: " Total Promoter",
                // legendText: "Total Promoter",
                // showInLegend: true, 
            
                //    dataPoints:data.NoOfPromoters
                //       } 
                  
                  ]
              });//end chart

      chart.render();
        
           
//end function

 

}//end sucss

});
 }//end if


 else{
document.getElementById("warn").innerHTML="Please enter governments";
}
     } ;//end function
     function ShowReport1()
{
   if($('#Government1').val()!="")
   {

    document.getElementById("warn1").innerHTML="";

        $.ajax({
      url: "/offlineneew/public/showpromoterchart",data:{Government:$('#Government1').val()},
       type: "Get",
         success: function(data){           
      var chart = new CanvasJS.Chart("canvaspromoter1",
              {
                  theme: "theme3",
                  exportEnabled: true,
                   animationEnabled: true,
                   zoomEnabled: true,
                   axisX:{
                   labelFontColor: "black",
                   labelMaxWidth: 100,

                   labelAutoFit: true ,
                   labelFontWeight: "bold"


                 },
    
                 toolTip: {
                      shared: true
                  },          
                        dataPointWidth: 20,
          data: [ 
              {
                indexLabelFontSize: 16,
                indexLabelFontColor: "black",
                type: "column", 
                 indexLabel: "{y}  " ,
                name: " Total contractors",
                legendText: "Total contractors",
                showInLegend: true, 
            
                   dataPoints:data.Contractors
                      },
                       {
                indexLabelFontSize: 16,
                indexLabelFontColor: "black",
                type: "column", 
                 indexLabel: "{y}  " ,
                name: "Covered contractors",
                legendText: "Covered contractors",
                showInLegend: true, 
            
                  
                    dataPoints:data.PromotorsContractor
                      }
                  
                  ]
              });//end chart

      chart.render();
        
           
//end function

 

}//end sucss

});
 }//end if


 else{
document.getElementById("warn1").innerHTML="Please enter governments";
}
     } ;//end function
      $('.Government').chosen({
                width: '100%',
                no_results_text:'No Results'
            });
                $(".Government").trigger("chosen:updated");
            $(".Government").trigger("change");     
</script>

@stop
@stop