@extends('masternew')
@section('content')
<section class="panel-body">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2><i class="fa fa-tasks"></i>Charts</h2> 
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
            <li><a class="close-link"><i class="fa fa-close"></i></a></li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
        <div class="row" >
            <div class="col-sm-12 col-md-10 col-md-offset-1 form-horizontal"  > 
              <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                  <label class="col-xs-3 control-label">Government:</label>
                  <div class="col-xs-9">
                    {!!Form::select('Government[]', ($Government->toArray()) ,null,['class'=>'form-control chosen-select Government ','id' => 'Government','name'=>'Government','required'=>'required','multiple' => true])!!}
                  </div>
                </div>
              </div>     
              <div class="ln_solid"></div>
              <div class="form-group">
                <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                  <button type="button" onclick="ShowReport()" id="Search" name="login_form_submit" class="btn btn-success">Show Report</button>
                  <label style="color:red" id="warn"></label> 
                </div>
              </div>
            </div>  
          </div>
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="dashboard_graph x_panel">
              <div class="row x_title">
                <div class="col-md-6">
                  <h3>Contractor Per District </h3>
                </div>
                <div class="col-md-6">
                </div>
              </div>
              <div class="x_content">
                <div class="demo-container" style="height:280px" >
                  <div id="canvaspromoter" class="demo-placeholder" ></div>               

                </div>
              </div>
            </div>
          </div>
        </div>
         </div>
          </div>
           </div>

   
</section>
@section('other_scripts') 
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>

  <script type="text/javascript">
 $("#Government").chosen({ 
                   width: '100%',
                   no_results_text: "No Results",
                   allow_single_deselect: true, 
                   search_contains:true, });
 $("#Government").trigger("chosen:updated");

function ShowReport()
{
   if($('#Government').val()!="")
   {

    document.getElementById("warn").innerHTML="";

        $.ajax({
      url: "/offlineneew/public/showpromoterchart",data:{Government:$('#Government').val()},
       type: "Get",
         success: function(data){
       


               

            
      var chart = new CanvasJS.Chart("canvaspromoter",
              {
                  theme: "theme3",
                  exportEnabled: true,
                   animationEnabled: true,
                   zoomEnabled: true,
                   axisX:{
                   labelFontColor: "black",
                   labelMaxWidth: 100,

                   labelAutoFit: true ,
                   labelFontWeight: "bold"


                 },
    
                 toolTip: {
                      shared: true
                  },          
                        dataPointWidth: 20,
          data: [ 
              {
                indexLabelFontSize: 16,
                indexLabelFontColor: "black",
                type: "column", 
                 indexLabel: "{y}  " ,
                name: " Total contractors",
                legendText: "Total contractors",
                showInLegend: true, 
            
                   dataPoints:data.Contractors
                      },
                       {
                indexLabelFontSize: 16,
                indexLabelFontColor: "black",
                type: "column", 
                 indexLabel: "{y}  " ,
                name: "Covered contractors",
                legendText: "Covered contractors",
                showInLegend: true, 
            
                  
                    dataPoints:data.PromotorsContractor
                      }
                  
                  ]
              });//end chart

      chart.render();
        
           
//end function

 

}//end sucss

});
 }//end if


 else{
document.getElementById("warn").innerHTML="Please enter governments";
}
     } ;//end function
      $('.Government').chosen({
                width: '100%',
                no_results_text:'No Results'
            });
                $(".Government").trigger("chosen:updated");
            $(".Government").trigger("change");     
</script>





@stop
@stop