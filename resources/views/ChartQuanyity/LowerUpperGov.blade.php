@extends('masternew')
@section('content')
<section class="panel-body">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">          
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
            <li><a class="close-link"><i class="fa fa-close"></i></a></li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <div class="row" >
            <div class="col-md-12 col-sm-12 col-xs-12">             
              <h2><i class="fa fa-bar-chart"></i> Consumed Product</h2> 
              <div class="clearfix"></div>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">  
<!--             /*chart*/
 -->  
 <fieldset style="padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;">
  <legend style="width: fit-content;border: 0px;">Filter Attribute:</legend> 
  <div class="row" >
    <div class="form-group">
      <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
        <input type='text' name="StartDate"  placeholder=" Start Date" class="form-control has-feedback-left ClassDate" id="StartDate" required>
        <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
        {!!$errors->first('StartDate','<small class="label label-danger">:message</small>')!!}
      </div>
      <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
        <input type='text' name="EndDate"  placeholder=" EndDate" class="form-control has-feedback-left ClassDate" id="EndDate"  required>
        <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
        {!!$errors->first('EndDate','<small class="label label-danger">:message</small>')!!}
      </div>
    </div>
  </div>
  <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: 20px;" align="center" >
    <div>
      <input type="button" class="btn btn-primary" id="Search" value=" Press To Show Report" title="Press To Show Report"  onclick="ShowReport()" >
    </div> 
    <label style="color:red" id="warn"></label>  
  </div>                       
</fieldset> 
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12" style="height: 50px;">
        </div>
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="dashboard_graph x_panel">
              <div class="row x_title">
                <div class="col-md-6">
                  <h3>LOE(Lower) Consumed product</h3>

                </div>
                <div class="col-md-6">
                </div>
              </div>
              <div class="x_content">
                <div class="demo-container" style="height:280px" >
                  <div id="canvasquantityLower" class="demo-placeholder" ></div>               
                </div>
              </div>
            </div>
          </div>

         </div> 
     </div>
                </div>
              </section>
 
  @section('other_scripts')  
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>


  <script type="text/javascript">
         $(document).ready(function(){
           $('.ClassDate').daterangepicker({
        singleDatePicker: true,
        singleClasses: "picker_4",
         locale: {
            format: 'YYYY-MM-DD'
        }
      }, function(start) {
        console.log(start.toISOString(), 'Hadeel');
      });
  });

    function ShowReport()
{
     document.getElementById("warn").innerHTML="";
if($('#StartDate').val() <=$('#EndDate').val())
  {
   
   if($('#StartDate').val()!="" &&$('#EndDate').val()!="" )
   {
 
        $.getJSON("/offlineneew/public/showupperchart?StartDate="+$('#StartDate').val()+"&EndDate="+$('#EndDate').val(), function(result) {    
var chart = new CanvasJS.Chart("canvaspromoter",
              {
                  theme: "theme3",
                  exportEnabled: true,
                  animationEnabled: true,
                  zoomEnabled: true,
                  axisX:{
                  labelFontColor: "black",
                  labelMaxWidth: 100,

                   labelAutoFit: true ,
   labelFontWeight: "bold"


                 },
    
                 toolTip: {
                      shared: true
                  },          
                        dataPointWidth: 20,
          data: [ 
              {
                indexLabelFontSize: 16,
    indexLabelFontColor: "black",
                type: "column", 
                 indexLabel: "{y} Ton " ,
                name: "Total Consumed product",
                legendText: "Total Consumed product",
                showInLegend: true, 
            
                         dataPoints:result
                      }
                  
                  ]
              });
      chart.render();
          });


  $.getJSON("/offlineneew/public/showulowerchart?StartDate="+$('#StartDate').val()+"&EndDate="+$('#EndDate').val(), function(result) {
  
var chart = new CanvasJS.Chart("canvasquantityLower",
              {
                  theme: "theme3",
                  exportEnabled: true,
                  animationEnabled: true,
                  zoomEnabled: true,
                  axisX:{
                  labelFontColor: "black",
                  labelMaxWidth: 100,
                  labelAutoFit: true ,
                  labelFontWeight: "bold"
                 },
    
                 toolTip: {
                      shared: true
                  },          
                        dataPointWidth: 20,
          data: [ 
              {
                indexLabelFontSize: 16,
    indexLabelFontColor: "black",
                type: "column", 
                 indexLabel: "{y} Ton " ,
                name: "Total Consumed product",
                legendText: "Total Consumed product",
                showInLegend: true, 
            
                         dataPoints:result
                      }
                  
                  ]
              });
      chart.render();
          });
           


}
else

document.getElementById("warn").innerHTML="Please enter Dates";
}
else
document.getElementById("warn").innerHTML="Please enter end date after start date";
}
           
</script>


@stop
@stop