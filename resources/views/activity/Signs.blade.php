@extends('masternew')
@section('content')
 <section class="panel-body">



 

    
 <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                     <h2><i class="fa fa-lightbulb-o"></i> Competitors Signs</h2> 
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                     
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">                  
                    <table class="table table-hover table-bordered  dt-responsive nowrap display users" cellspacing="0" width="100%">
                      <thead>
                         <th>No</th>
                         <th>Goverment</th>
                         <th>City</th>
                         <th> Company</th>
                         <th>Longitude</th>                     
                         <th>Latitude</th>
                         <th>User</th>
                         <th>Type</th>
                         <th>Image</th>
                         <th>Action</th>  
                      </thead>
                      <tfoot>
                        <th></th>
                        <th>Goverment</th>
                        <th>City</th>
                        <th> Company</th>
                        <th>Longitude</th>
                        <th>Latitude</th>
                        <th>User</th>
                        <th>Type</th>
                        <th></th>
                        <th></th> 
                      </tfoot>
                      <tbody style="text-align:center;">
                        <?php $i=1;?>
                        @foreach($activity as $activity)
                          <tr>
                            <td>{{$i++}}</td>
                            <td><a class="testEdit" data-type="select"  data-value="{{$governments}}" data-source ="{{$governments}}" data-name="Government"  data-pk="{{$activity->id}}"> {{$activity->Government}}</a></td>
                            <td><a class="testEdit" data-type="select"  data-value="{{$districts}}" data-source ="{{$districts}}" data-name="District"  data-pk="{{$activity->id}}"> {{$activity->District}}</a></td>
                            <td><a class="testEdit" data-type="select"  data-value="{{$activity->company_id}}" data-source ="{{$company}}" data-name="company_id"  data-pk="{{$activity->id}}">{{$activity->getcompany->Name}}</td>
                            <td>{{$activity->Lat}}</td>
                            <td>{{$activity->Long}}</td>   
                            <td>{{$activity->getUser->Name}}</td>
                            <td><a class="testEdit" data-type="select"  data-value="{{$activity->Type}}" data-source ="[{'value':'مسابقات', 'text': 'مسابقات'}, {'value':'حفلات', 'text': 'حفلات'},{'value':'هدايا', 'text': 'هدايا'}, {'value':'يفط', 'text': 'يفط'}, {'value':'اخرى', 'text': 'اخرى'}]" data-name="Type"  data-pk="{{$activity->id}}">{{$activity->Type}}</td>
                            <td>
                            @if($activity->Img!==null)
                            <img src="/offlineneew/public/activity/{{$activity->id}}" style="width:60px;hight:60px;">
                            @endif
                            </td>
                            <td><a href="/offlineneew/public/activity/destroy/{{$activity->id}}" class="btn btn-default btn-sm" ><span class="glyphicon glyphicon-trash"></span>  </a></td>
                          </tr>
                        @endforeach
                      </tbody>
                      </table>

                  </div>
                </div>
              </div>
 
  @section('other_scripts')


<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>



  <script type="text/javascript">

var editor;

  $(document).ready(function(){

     var table= $('.users').DataTable({
  
    select:true,
    responsive: true,
    "order":[[0,"asc"]],
    'searchable':true,
    "scrollCollapse":true,
    "paging":true,
    "pagingType": "simple",
      dom: 'lBfrtip',
        buttons: [
           
            
            { extend: 'excel', className: 'btn btn-primary dtb' }
            
            
        ],

fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
$.fn.editable.defaults.send = "always";

$.fn.editable.defaults.mode = 'inline';

$('.testEdit').editable({

      validate: function(value) {
        name=$(this).editable().data('name');
      
       
        },

    placement: 'right',
    url:'{{URL::to("/")}}/ACupdate',
  
     ajaxOptions: {
     type: 'get',
    sourceCache: 'false',
     dataType: 'json'

   },

       params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');

            // console.log(params);
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'This Data Already Exist,Enter Correct Data.';
            } else {
                return response.responseText;
            }
        }


});

}
});

 



   $('.users tfoot th').each(function () {



      var title = $('.users thead th').eq($(this).index()).text();
               
 if($(this).index()>=1 && $(this).index()<=14)
        {

           $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
}
        

    });
  table.columns().every( function () {
  var that = this;
 $(this.footer()).find('input').on('keyup change', function () {
          that.search(this.value).draw();
            if (that.search(this.value) ) {
               that.search(this.value).draw();
           }
        });
      
 
   });

 

    $('[data-toggle="popover"]').popover({ trigger: "hover" }); 

 

    $('[data-toggle="popover"]').popover({ trigger: "hover" }); 
 });
  </script>
@stop
@stop
