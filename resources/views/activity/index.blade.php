@extends('masternew')
@section('content')
<?php
//File
  if(!empty($_COOKIE['FileError'])) {     
    echo "<div class='alert alert-block alert-danger fade in center'>";
    echo $_COOKIE['FileError'];
    echo "</div>";
  } 
//ActivityErr 
  if(!empty($_COOKIE['ActivityErr'])) {      
    echo "<div><div class='alert alert-block alert-danger fade in center'>";
    echo $_COOKIE['ActivityErr'];
    echo "</div> </div>";
  } 
//EmptyActivityErr 
  if(!empty($_COOKIE['EmptyActivityErr'])) {     
    echo "<div><div class='alert alert-block alert-danger fade in center'>";
    echo $_COOKIE['EmptyActivityErr'];
    echo "</div> </div>";
  } 

?>
<style type="text/css">
/*    input[type="file"] {
    display: block;
   }
*/
#bldah{
  background: url(assets/img/noimage.jpg) no-repeat;
  background-size: cover;
  height: 60px;
  width: 60px;
}
.error{
  color: #a51c1c;
}
.custom-file-upload {
    border: 1px solid #ccc;
    display: inline-block;
    padding: 6px 12px;
    cursor: pointer;
}
   input[type="file"] {
    display: none;
   }
</style>
<section class="panel-body">
 <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><i class="fa fa-lightbulb-o" aria-hidden="true"></i> Activity </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                     
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
<div class="panel-body">
  <a type="button" class="btn btn-primary" data-toggle="modal" data-target="#activityModal" style="margin-bottom: 20px;"> Add New activity </a>










<!-- model -->
<div class="modal fade" id="activityModal" tabindex="-1" role="dialog" aria-labelledby="activityModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
         <h4 class="modal-title span7 text-center" id="activityModalLabel">
         <span class="title">Add Activity</span></h4>

    </div>
    <div class="modal-body">

 <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                 
                  <div class="x_content">
                    <br />
        {!! Form::open(['route'=>'activity.store','method'=>'post','id'=>'form','files'=>'true','class'=>'form-horizontal form-label-left ']) !!}
       <!--  <input type="hidden" name="_token" value="{{ csrf_token() }}"> -->

   <fieldset style="    padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;">
   <legend style="width: fit-content;border: 0px;">Location:</legend>

        
           

      <div class="form-group">
                      <div class="control-label col-md-6 col-sm-6 col-xs-12">
                        <div >
                          {!!Form::select('Government', ([null => 'Select  Government'] + $governments->toArray()) ,null,['class'=>' chosen-select','id' => 'Government','required'=>'required'])!!}
                        </div>
                     </div>
                       <div class="control-label col-md-6 col-sm-6 col-xs-12">

                        <div>
                           {!!Form::select('District', ([null => 'Select  District']) ,null,['class'=>'form-control chosen-select  ','id' => 'District'])!!}
                        </div>
                        </div>

                      </div>

</fieldset>

   <fieldset style=" padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;">
   <legend style="width: fit-content;border: 0px;">Activity Details:</legend>

        
           

      <div class="form-group">

   <div class="control-label col-md-6 col-sm-6 col-xs-12">
                        <div>
                  
                          {!!Form::select('company', ([null => 'Select  company'] + $company->toArray()) ,null,['class'=>' chosen-select','id' => 'company','Name'=>'company'])!!}
                        </div>
                     </div>
                   
 <div class="col-md-6 col-sm-6 col-xs-12 ">
                           <input type="text" class="form-control has-feedback-left" id="activity_type" name="activity_type" placeholder="Activity Type">
                           <span class="fa fa-pencil form-control-feedback left" aria-hidden="true"></span>
                           </div>

                    
                   
                    </div>

                        <div class="form-group">
  <div class="col-md-6 col-sm-6 col-xs-12">
                        <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>

                          <input type="text" class="form-control has-feedback-left" id="single_cal4" name="start_date" placeholder="Start Date">
                        </div>


                          
                           <div class="col-md-6 col-sm-6 col-xs-12  ">
                           <input type="text" class="form-control has-feedback-left" id="duration" name="duration" placeholder="Duration">
                           <span class="fa fa-sort-numeric-asc form-control-feedback left" aria-hidden="true"></span>
                           </div>
                        </div>

      <div class="form-group">

                        <div class="control-label col-md-6 col-sm-6 col-xs-12">
                       
                        <div>
                 {!!Form::select('Target_Segmentation',array(null => 'Select Target Segmentation','مقاولين'=>'مقاولين','تجار'=>'تجار','طلاب هندسه'=>'طلاب هندسه','مهندسبن'=>'مهندسين')
                   ,null,['id' => 'SType','required' => 'required','class'=>'chosen-select  form-control']);!!} 
                        </div>
                        </div>

                        <div class="control-label col-md-6 col-sm-6 col-xs-12">
                        <div>
                  {!!Form::select('Type',array(null => 'Select Type','مسابقات'=>'مسابقات','حفلات'=>'حفلات','هدايا'=>'هدايا','يفط'=>'يفط','اخرى'=>'اخرى')
                  ,null,['id' => 'GType','required' => 'required','class'=>'chosen-select  form-control']);!!}  
                        </div>
                        </div>

                      </div>
   <div class="form-group">

                        <div class="control-label col-md-4 col-sm-4 col-xs-12">


                         <label for="description">Description (20 chars min, 100 max) :</label>

                          <textarea id="description" required="required" class="form-control" name="description" data-parsley-trigger="keyup" data-parsley-minlength="20" data-parsley-maxlength="100" data-parsley-minlength-message="Come on! You need to enter at least a 20 caracters long comment.."
                            data-parsley-validation-threshold="10"></textarea>
</div>
                        <div class="control-label col-md-4 col-sm-4 col-xs-12" align="center">

<!--                               {!! Form::file('pic', ['class' => 'form-control','id'=>'uploadFile','name'=>'pic']) !!}
 -->


<label for="file-uploadactivity" class="custom-file-upload" >
    <i class="fa fa-cloud-upload"></i> Image Upload
</label>
<input id="file-uploadactivity" class="Activitypic" name="pic" type="file"/>

  </div>




  <div class="col-md-4 col-sm-4 col-xs-12">
     <div id="bldah"></div>
   </div>
                    
 </div>

                     </fieldset>


   <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                          <button type="button" class="btn btn-primary " data-dismiss="modal">Cancel</button>
                         <button class="btn btn-primary" type="reset">Reset</button>
                          <button type="submit"  name="login_form_submit" class="btn btn-success" value="Save">Submit</button>
                        </div>
                      </div>

      {!!Form::close() !!}
    </div>
    </div>
     </div>
    </div>
    </div>
  </div>
  </div>
  </div>
<!-- ENDModal -->
          @if(Session::has('error'))
           
          <div class="alert alert-danger" style="text-align:center;" >
         <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
                  <strong>Whoops!</strong> {{ Session::get('error') }}<br><br>

    
    </div>
    @endif
<table class="table table-hover table-bordered  dt-responsive nowrap display activity" cellspacing="0" width="100%">
  <thead>
              <th>No</th>
                <th>Goverment</th>
                     <th>City</th>
                      <th> Company</th>
                           <th>Start Date</th>
                            <th>Duration</th>
                           <th>Activity Name</th>
                         <th>Description</th>
                         

                         <th>Target Segmentation</th>

                         <th>Type</th>
                         <th>User</th>
                         <th>Image</th>
                         <th>Action</th>            
  </thead>
    <tfoot>
        
                  <th></th> 
                <th>Goverment</th>
                    <th>City</th>
                      <th> Company</th>
                           <th>Start Date</th>
                            <th>Duration</th>
                           <th>Activity Name</th>
                         <th>Description</th>
                          
                           <th>Target Segmentation</th>
                         <th>Type</th>
                         <th>User</th>
                           <th>Image</th>
                            <th></th>
            
                  </tfoot>
               

<tbody style="text-align:center;">
  <?php $i=1;?>
  @foreach($activity as $activity)
    <tr>
    
     <td>{{$i++}}</td>
         <td>  
 {{$activity->Government}}
  
</td>

   <td> {{$activity->District}}
  
  </td>

         <td>{{$activity->getcompany->Name}}</td>


         <td>{{$activity->Start_date}}</td>

         <td>{{$activity->Duration}}</td>

         <td>{{$activity->Activity_type}}</td>

         <td>{{$activity->Description}}</td>

         

        <td>{{$activity->Target_Segmentation}}</td>
        <td>{{$activity->Type}}</td>

       <td>{{$activity->getUser->Name}}</td>
         <td>
          @if($activity->Img!==null)
          <img src="/offlineneew/public{{$activity->Img}}" style="width:60px;hight:60px;">
          @endif
        </td>
        
             <td>
   
       <a href="/offlineneew/public/activity/destroy/{{$activity->id}}" class="btn btn-default btn-sm" ><span class="glyphicon glyphicon-trash"></span>  </a>


  </td>
   </tr>
     @endforeach
</tbody>
</table> 
</div>
 </div>
 </div>   
    </section>
  
  @section('other_scripts')


<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

        // function readURL(input) {
        //     if (input.files && input.files[0]) {
        //         var reader = new FileReader();

        //         reader.onload = function (e) {
        //             $('#bldah').attr('src', e.target.result);
        //         }

        //         reader.readAsDataURL(input.files[0]);
        //     }
        // }
</script>


  <script type="text/javascript">

var editor;

  $(document).ready(function(){
//chosen for District
            $('#District').chosen({width: '100%',no_results_text:'No Results'});
            $("#District").trigger("chosen:updated");
            $("#District").trigger("change");

              $('#SType').chosen({
                width: '100%',
                no_results_text:'No Results'
            });
                $("#SType").trigger("chosen:updated");
            $("#SType").trigger("change");

              $('#GType').chosen({
                width: '100%',
                no_results_text:'No Results'
            });
                $("#GType").trigger("chosen:updated");
            $("#GType").trigger("change");





 $('#single_cal4').on('changeDate', function(e) {
            $('#eventForm').formValidation('revalidateField', 'start_date');
        });





 //change imgage    
$("#file-uploadactivity").on("change", function()
    {
    
        var files = !!this.files ? this.files : [];

        if (!files.length || !window.FileReader) 
        {
           $("#bldah").css("background-image",'url(assets/img/noimage.jpg)');
        } // no file selected, or no FileReader support
 
        else if (/^image/.test( files[0].type)){ // only image file
            var reader = new FileReader(); // instance of the FileReader
            reader.readAsDataURL(files[0]); // read the local file
 
            reader.onloadend = function(){ // set image data as background of div
                $("#bldah").css("background-image", "url("+this.result+")");
            }
        }
    });









     var table= $('.activity').DataTable({
  
   
    responsive: true,
    "order":[[0,"asc"]],
    'searchable':true,
    "scrollCollapse":true,
    "paging":true,
    "pagingType": "simple",
      dom: 'lBfrtip',
        buttons: [
           
            
            { extend: 'excel', className: 'btn btn-primary dtb' }
            
            
        ],


   

fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
$.fn.editable.defaults.send = "always";

$.fn.editable.defaults.mode = 'inline';










$('.testEdit').editable({

      validate: function(value) {
        name=$(this).editable().data('name');
        if(name=="goverment"||name=="city"||name=="start_date"||name=="company_id" ||name=="activity_type"||name=="Target_Segmentation"||name=="Type")
        {
                if($.trim(value) == '') {
                    return 'Value is required.';
                  }
                    }
        if(name=="goverment"||name=="city"||name=="activity_type"||name=="description"||name=="duration")
        {
              var regexp = /^[\u0600-\u06FF]+$/;           
              if (!regexp.test(value)) {
                return 'This field is not valid';
              }
        
        }
          if(name=="start_date")
        {

           var regexp = /^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/;          

           if (!regexp.test(value)) 
           {
                return 'This field is not valid';
           }
            
         } 


    },

    url:'{{URL::to("/")}}/ACupdate',
  
     ajaxOptions: {
     type: 'get',
    sourceCache: 'false',
     dataType: 'json'

   },

       params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');

            // console.log(params);
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'This Data Already Exist,Enter Correct Data.';
            } else {
                return response.responseText;
            }
        }
,
        success:function(response)
        {
            if(response.status==="You do not have permission.")
             {
              return 'You do not have permission.';
              }
        }

});

}
});

 



   $('.activity tfoot th').each(function () {



      var title = $('.activity thead th').eq($(this).index()).text();
               
 if($(this).index()>=1 && $(this).index()<=10)
        {

           $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
}
        

    });
  table.columns().every( function () {
  var that = this;
 $(this.footer()).find('input').on('keyup change', function () {
          that.search(this.value).draw();
            if (that.search(this.value) ) {
               that.search(this.value).draw();
           }
        });
      
    });


$.validator.addMethod("alpha", function(value, element) {
    return this.optional(element) || /^[\u0600-\u06FF]+$/.test(value);
 });




 $('#form').find('[name="Government"]')
            .chosen({
                width: '100%',
                inherit_select_classes: true
            })
            .change(function(){

             $('#from').formValidation('revalidateField', 'Government');

        $.get("{{ url('api/dropdown')}}", 
        { option: $(this).val() }, 
             function(data) {
              console.log(data);
            $('#District').empty(); 

            $.each(data, function(key, element) {

                $('#District').append("<option value='" + key +"'>" + element + "</option>");
            });
                  
        
             $('#District').chosen({
                width: '100%',
                no_results_text:'No Results'
            });
                $("#District").trigger("chosen:updated");
            $("#District").trigger("change");
            });
            }).end().find('[name="company"]')
            .chosen({
                width: '100%',
                inherit_select_classes: true
            })
            .change(function(){

             $('#from').formValidation('revalidateField', 'Company');})  
                        .end().formValidation({
            framework: 'bootstrap',
            excluded: ':disabled',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                Government: {
                    validators: {
                        callback: {
                            message: 'Please choose Government',
                            callback: function(value, validator, $field) {
                                // Get the selected options
                                var options = validator.getFieldElements('Government').val();
                              
                                return (options !== '' );
                            }
                        }
                    }
                }, 
              

                'company':{
                    validators: {
                      callback: {
                            message: 'Please choose Company',
                            callback: function(value, validator, $field) {
                                // Get the selected options
                                var options = validator.getFieldElements('company').val();
                              
                                return (options !== '' );
                            }
                        }
                    }
                },
                 'duration':{
                    validators: {
                    
                     
             notEmpty:{
                    message:"the duration is required"
                  } 
                    }
                },
                 
                   'activity_type': {
                    validators: {  
             notEmpty:{
                    message:"the activity type is required"
                  },
                     regexp: {
                        regexp: /^[\u0600-\u06FF\s]+$/,
                        message: 'The full name can only consist of Arabic alphabetical and spaces'
                     } 
                    }
                },
         
                    'District': {
                    validators: {
                     
        
             notEmpty:{
                    message:"Please select the District"
                  } 
                    }
                },
                       'description': {
                    validators: {
                     
        
             notEmpty:{
                    message:"the description is required"
                  } 
                    }
                },
                'start_date': {
                    validators: {
                      date: {
                        format: 'YYYY-MM-DD',
                        message: 'The value is not a valid date'
                    },
        
             notEmpty:{
                    message:"the Start Date is required"
                  } 
                    }
                }
            
         
            }
        });


    //         pic:{accept: "image/*"}
    //     },


 });
  </script>











@stop

@stop

