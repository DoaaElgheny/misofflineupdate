@extends('masternew')


@section('content')


<div class="row">

<div class="col-md-2 col-xs-12 ">
 </div>

              <div class="col-md-12 col-xs-12 ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Edit Contractors</h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
     {!!Form::open(['route' =>[ 'Reviews.update',$Review->id],'method' => 'put','class'=>'form-horizontal form-label-left 
                    ','id'=>'profileForm'])!!}

                    
  <input type="hidden" name="_token" value="{{ csrf_token() }}">

       <input type="hidden" id="id" name="id" value="{{ $Review['id'] }}">

 <fieldset style="    padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;" >
  <legend style="width: fit-content;border: 0px;" dir="rtl">Main Data</legend>
 


<div class="form-group">

<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
{!!Form::select('Contractor_Id',([null => 'please chooose Contractors'] + $Contractors->toArray()) ,$Review['Contractor_Id'],['class'=>'form-control subuser_list','id' => 'prettify'])!!}
</div>


</div>

                     
</fieldset>


<fieldset style="    padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;" >
  <legend style="width: fit-content;border: 0px;" dir="rtl">Basic info</legend>

<fieldset style="    padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;" >
  <legend style="width: fit-content;border: 0px;" dir="rtl">Main Data</legend>
 

<!-- <div class="form-group">

<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
<input type="text" class="form-control has-feedback-left"  id="Name" name="Name" placeholder="Name">
<span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
</div>

</div> -->

<div class="form-group">

<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
 <input type="text" class="form-control has-feedback-left" id="single_cal4" name="Birthday" placeholder="Enter Birthday">
<span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
</div>

<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
<input type="number" class="form-control has-feedback-left" id="Name" name="Tele1" placeholder="enter Tele1" value="{{$ContractorMain->Tele1}}">
<span class="fa fa-phone form-control-feedback left" aria-hidden="true"></span>
</div>

</div>

<div class="form-group">

<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
<input type="text" class="form-control has-feedback-left"  id="Name" name="Nickname" placeholder="Enter Nickname" value="{{$ContractorMain->Nickname}}">
<span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
</div>

<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">

 <input type="text" class="form-control has-feedback-left" id="Name" name="Religion" placeholder="Enter Religion" value="{{$ContractorMain->Religion}}">
<span class="fa fa-bars form-control-feedback left" aria-hidden="true"></span>
</div>
</div>
<div class="form-group">



<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">

 <input type="text" class="form-control has-feedback-left" id="Name" minlength="7" maxlength="11" name="Home_Phone" placeholder="Enter Home Phone"> 
<span class="fa fa-phone form-control-feedback left" aria-hidden="true"></span>
</div>
</div>



<div class="form-group">



<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
<input type="number" class="form-control has-feedback-left" id="Name" name="Tele2" placeholder="enter Tele2" >
<span class="fa fa-phone form-control-feedback left" aria-hidden="true"></span>
</div>
</div>
                     
</fieldset>
 
<fieldset style="    padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;" >
  <legend style="width: fit-content;border: 0px;" dir="rtl">Address</legend>
 

<div class="form-group">

<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">


{!!Form::select('Government',([null => 'please chooose Government'] + $governments->toArray()) ,$ContractorMain->Government,['class'=>'form-control subuser_list','id' => 'prettify'])!!}





</div>

<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">


{!!Form::select('district',([null => 'please chooose District'] + $districts->toArray()) ,$ContractorMain->District,['class'=>'form-control subuser_list','id' => 'prettify'])!!}


</div>
</div>
  
<div class="form-group">

<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
<input type="text" class="form-control has-feedback-left"  id="Name" name="Address" placeholder="enter Address">
<span class="fa fa-map-marker form-control-feedback left" aria-hidden="true"></span>
</div>


</div>

</fieldset>  
<fieldset style="    padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;" >
  <legend style="width:fit-content;border: 0px;" dir="rtl">Work and Eduction</legend>
 

<div class="form-group">
<label class="col-md-2 col-sm-2 col-xs-12 ">Education</label>
<div class="col-md-3 col-sm-3 col-xs-12 has-feedback">
 {!!Form::select('Education',
                    array( 'High Education' => 'High Education',
                    'Low Education' => 'Low Education','No Data' => 'No Data',
                    'Medium Education' => 'Medium Education',
                    'No Education' => 'No Education')
                   ,[null => 'Select Education'],['id' => 'Education','class'=>'chosen-select TypeP form-control']);!!} 


</div>

<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
<input type="text" class="form-control has-feedback-left"  id="Name" name="Job" placeholder="enter Job" >
<span class="fa fa-briefcase form-control-feedback left" aria-hidden="true"></span>



</div>
</div>

</fieldset> 


<fieldset style="    padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;" >
  <legend style="width: fit-content;border: 0px;" dir="rtl">Other Data</legend>
 

<div class="form-group">
<label class="col-md-2 col-sm-2 col-xs-12 ">Has Facebook</label>
<div class="col-md-3 col-sm-3 col-xs-12 has-feedback">
 {!!Form::select('Has_Facebook',
                    array( 'Yes' => 'Yes',
                    'No' => 'No')
                   ,[null => 'Select Choose'],['id' => 'Has_Facebook','class'=>'chosen-select TypeP form-control']);!!}  


</div>

<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
<input type="text" class="form-control has-feedback-left"  id="Facebook_Account" name="Facebook_Account" placeholder="enter Eng Facebook_Account" >

<span class="fa fa-facebook-official form-control-feedback left" aria-hidden="true"></span>



</div>
</div>
  
<div class="form-group">
<label class="col-md-2 col-sm-2 col-xs-12 ">Has Computer</label>
<div class="col-md-3 col-sm-3 col-xs-12 has-feedback">

 {!!Form::select('Computer',
                    array( 'Yes' => 'Yes',
                    'No' => 'No')
                   ,[null => 'Select Has Computer'],['id' => 'Computer','class'=>'chosen-select TypeP form-control']);!!} 

</div>
<label class="col-md-2 col-sm-2 col-xs-12 ">Phone Type</label>
<div class="col-md-3 col-sm-3 col-xs-12 has-feedback">
 {!!Form::select('Phone_Type',
                    array( 'Old' => 'Old',
                    'New' => 'New')
                   ,[null => 'Select Phone Type'],['id' => 'Phone_Type','class'=>'chosen-select TypeP form-control']);!!} 

</div>
</div>
<div class="form-group">
<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
<input type="text" class="form-control has-feedback-left"  id="Name" name="Email" placeholder="enter  Email" >
<span class="fa fa-envelope form-control-feedback left" aria-hidden="true"></span>
</div>
</fieldset>         












  </fieldset>




 
<fieldset style="    padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;" >
  <legend style="width: fit-content;border: 0px;" dir="rtl">Review Data</legend>
 

<!-- <div class="form-group">

<label class="col-md-2 col-sm-2 col-xs-12 ">Long</label>
 <div class="col-md-3 col-sm-3 col-xs-12 has-feedback">
<input type="text" class="form-control has-feedback-left"  id="Name" name="Long"
                    value="{{ $Review['Long'] }}"  placeholder="enter Long" >

</div>

<label class="col-md-2 col-sm-2 col-xs-12 ">Lat</label>
 <div class="col-md-3 col-sm-3 col-xs-12 has-feedback">
<input type="text" class="form-control has-feedback-left"  id="Name" name="Lat" value="{{ $Review['Lat'] }}"  placeholder="enter Lat" >
</div>
</div> -->
  
<div class="form-group">

<label class="col-md-2 col-sm-2 col-xs-12 ">Project NO</label>
 <div class="col-md-3 col-sm-3 col-xs-12 has-feedback">
<input type="text" class="form-control has-feedback-left"  id="Name" name="Project_NO" value="{{ $Review['Project_NO'] }}" placeholder="enter Project_NO" >

</div>
<label class="col-md-2 col-sm-2 col-xs-12 ">Workers</label>
 <div class="col-md-3 col-sm-3 col-xs-12 has-feedback">

<input type="text" class="form-control has-feedback-left"  id="Name" name="Workers" value="{{ $Review['Workers'] }}" placeholder="enter  Workers" >
</div>

</div>

<div class="form-group">

<label class="col-md-2 col-sm-2 col-xs-12 ">Cement Consuption</label>
 <div class="col-md-3 col-sm-3 col-xs-12 has-feedback">
<input type="text" class="form-control has-feedback-left"  id="Name" name="Cement_Consuption" value="{{ $Review['Cement_Consuption'] }}" placeholder="enter Cement_Consuption" >

</div>
<label class="col-md-2 col-sm-2 col-xs-12 ">Cement Bricks</label>
 <div class="col-md-3 col-sm-3 col-xs-12 has-feedback">

 {!!Form::select('Cement_Bricks',
                    array( 'Yes' => 'Yes',
                    'No' => 'No')
                   ,$Review->Cement_Bricks,['id' => 'Cement_Bricks','class'=>'chosen-select TypeP form-control']);!!} 
</div>

</div>

<div class="form-group">

<label class="col-md-2 col-sm-2 col-xs-12 ">Steel Consumption</label>
 <div class="col-md-3 col-sm-3 col-xs-12 has-feedback">
<input type="text" class="form-control has-feedback-left"  id="Name" name="Steel_Consumption" value="{{ $Review['Steel_Consumption'] }}" placeholder="enter  Steel_Consumption" >

</div>
<label class="col-md-2 col-sm-2 col-xs-12 ">Has Wood</label>
 <div class="col-md-3 col-sm-3 col-xs-12 has-feedback">
  {!!Form::select('Has_Wood',
                    array( 'Yes' => 'Yes',
                    'No' => 'No')
                   ,$Review->Has_Wood,['id' => 'Has_Wood','class'=>'chosen-select TypeP form-control']);!!} 
</div>
</div>
<div class="form-group">
<label class="col-md-2 col-sm-2 col-xs-12 ">Wood_Meters</label>
 <div class="col-md-3 col-sm-3 col-xs-12 has-feedback">
<input type="text" class="form-control has-feedback-left"  id="Wood_Meters" name="Wood_Meters" value="{{ $Review['Wood_Meters'] }}" placeholder="enter Wood_Meters" >

</div>
  
</div>


<div class="form-group">


<label class="col-md-2 col-sm-2 col-xs-12 ">Has Mixers</label>
 <div class="col-md-3 col-sm-3 col-xs-12 has-feedback">
   {!!Form::select('Has_Mixers',
                    array( 'Yes' => 'Yes',
                    'No' => 'No')
                   ,$Review->Has_Mixers,['id' => 'Has_Mixers','class'=>'chosen-select TypeP form-control']);!!}  
</div>

</div>
<div class="form-group">

<label class="col-md-2 col-sm-2 col-xs-12 ">No Of Mixers</label>
 <div class="col-md-3 col-sm-3 col-xs-12 has-feedback">
<input type="text" class="form-control has-feedback-left"  id="No_Of_Mixers" name="No_Of_Mixers" value="{{ $Review['No_Of_Mixers'] }}" placeholder="enter No_Of_Mixers" >

</div>
<label class="col-md-2 col-sm-2 col-xs-12 ">Intership No</label>
 <div class="col-md-3 col-sm-3 col-xs-12 has-feedback">

<input type="text" class="form-control has-feedback-left"  id="Name" name="Intership_No" value="{{ $Review['Intership_No'] }}" placeholder="enter Intership_No"  >

</div>

</div>
<div class="form-group">

<label class="col-md-2 col-sm-2 col-xs-12 ">Capital</label>
 <div class="col-md-3 col-sm-3 col-xs-12 has-feedback">
<input type="number" class="form-control has-feedback-left"  id="Name" name="Capital" value="{{ $Review['Capital'] }}" placeholder="enter Capital" >


</div>
 <label class="col-md-2 col-sm-2 col-xs-12 " >Credit Debit</label>
 <div class="col-md-3 col-sm-3 col-xs-12 has-feedback" >
 {!!Form::select('Credit_Debit',
                    array( 'كاش' => 'كاش',
                    'أجل' => 'أجل',
                    'كاش واجل' => 'كاش واجل')
                   ,[null => 'Select Choose'],['id' => 'Credit_Debit','class'=>'form-control']);!!} 
</div>

</div>

<div class="form-group">

 <label class="col-md-2 col-sm-2 col-xs-12 " >Has SubContractor</label>
 <div class="col-md-3 col-sm-3 col-xs-12 has-feedback" >
 {!!Form::select('Has_Sub_Contractor',
                    array( 'Yes' => 'Yes',
                    'No' => 'No')
                   ,$Review->Has_Sub_Contractor,['id' => 'Has_Sub_Contractor','class'=>'chosen-select TypeP form-control']);!!} 
</div>
<label class="col-md-2 col-sm-2 col-xs-12 ">Sub Contractor</label>
 <div class="col-md-3 col-sm-3 col-xs-12 has-feedback">

<input type="text" class="form-control has-feedback-left"  id="Sub_Contractor1" name="Sub_Contractor1" value="{{ $Review['Sub_Contractor1'] }}" placeholder="enter Sub_Contractor1" >

</div>
</div>
<div class="form-group">
<label class="col-md-2 col-sm-2 col-xs-12 ">Sub Contractor2</label>
 <div class="col-md-3 col-sm-3 col-xs-12 has-feedback">
<input type="text" class="form-control has-feedback-left"  id="Sub_Contractor2" name="Sub_Contractor2" value="{{ $Review['Sub_Contractor2'] }}" placeholder="enter Sub_Contractor2" >

</div>
<label class="col-md-2 col-sm-2 col-xs-12 ">Class</label>
 <div class="col-md-3 col-sm-3 col-xs-12 has-feedback">
<input type="text" class="form-control has-feedback-left"  id="Name" name="Class" value="{{ $Review['Class'] }}"  placeholder="enter Class" >

</div>
</div>

<div class="form-group">
<label class="col-md-2 col-sm-2 col-xs-12 ">Seller1</label>
 <div class="col-md-3 col-sm-3 col-xs-12 has-feedback">
<input type="text" class="form-control has-feedback-left"  id="Name" name="Seller1" value="{{ $Review['Seller1'] }}"  placeholder="enter Seller1" >

</div>
<label class="col-md-2 col-sm-2 col-xs-12 ">Seller2</label>
 <div class="col-md-3 col-sm-3 col-xs-12 has-feedback">
<input type="text" class="form-control has-feedback-left"  id="Name" name="Seller2" value="{{ $Review['Seller2'] }}"  placeholder="enter Seller2" >

</div>
</div>


<div class="form-group">
<label class="col-md-2 col-sm-2 col-xs-12 ">Seller3</label>
 <div class="col-md-3 col-sm-3 col-xs-12 has-feedback">
<input type="text" class="form-control has-feedback-left"  id="Name" name="Seller3" value="{{ $Review['Seller3'] }}"  placeholder="enter Seller3" >

</div>
<label class="col-md-2 col-sm-2 col-xs-12 ">Seller4</label>
 <div class="col-md-3 col-sm-3 col-xs-12 has-feedback">
<input type="text" class="form-control has-feedback-left"  id="Name" name="Seller4" placeholder="enter Seller4" value="{{ $Review['Seller4'] }}"  >

</div>
</div>

<div class="form-group" >
 <label class="col-md-2 col-sm-2 col-xs-12 ">Status</label>
 <div class="col-md-3 col-sm-3 col-xs-12 has-feedback" >
                        {!!Form::select('Status',
  array( 'Reviewed' => 'Reviewed',
  'Not Reviewed' => 'Not Reviewed')
 ,$Review['Status'],['id' => 'Status','name' => 'Status','class'=>'chosen-select TypeP form-control']);!!}  
</div>
 <label class="col-md-2 col-sm-2 col-xs-12 ">Call Status</label>
 <div class="col-md-3 col-sm-3 col-xs-12 has-feedback">
          {!!Form::select('Call_Status',
  array('0' => 'None',
  'Updated' => 'Updated',
  'No Answer' => 'No Answer',
  'User Busy' => 'User Busy',
 'Closed Line' => 'Closed Line',
 'Not Cemex Contractor' => 'Not Cemex Contractor',
  'Stop Work' => 'Stop Work',
   'Refused' => 'Refused',
    'Unavilable' => 'Unavilable',
 'Not Contractor' => 'Not Contractor',
  'wrong Number' => 'wrong Number','Out Service' => 'Out Service')
 ,$Review['Call_Status'],['id' => 'Call_Status','name' => 'Call_Status','class'=>'chosen-select TypeP form-control']);!!}  
</div>
</div>

<!-- <div class="form-group" >
 <label class="col-md-2 col-sm-2 col-xs-12 " >Area</label>
 <div class="col-md-3 col-sm-3 col-xs-12 has-feedback">
                   {!!Form::select('Area',
  array( 'Upper' => 'Upper',
  'Lower' => 'Lower')
 ,$Review['Area'],['id' => 'Area','name' => 'Area','class'=>'chosen-select TypeP form-control']);!!} 
</div>
<label class="col-md-2 col-sm-2 col-xs-12 ">Cont Type</label>
 <div class="col-md-3 col-sm-3 col-xs-12 has-feedback">

<input type="text" class="form-control has-feedback-left" id="Name" name="Cont_Type" placeholder="enter Cont_Type" value="{{ $Review['Cont_Type'] }}"  >

<span class="fa fa-map-marker form-control-feedback left" aria-hidden="true"></span> 
</div>
</div> -->
<fieldset style="    padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;" >
  <legend style="width: fit-content;border: 0px;" dir="rtl">Items</legend>



<div class="form-group">
<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
      {!!Form::select('item[]',([null => 'Choose Product'] + $product->toArray()) ,null,['class'=>'form-control ','id' => 'prettify'])!!}
</div>
<div class="col-md-4 col-sm-4 col-xs-12 has-feedback">


<input type="text" class="form-control has-feedback-left" id="Name" type="number" name="quantity[]" placeholder="ادخل الكمية" />

</div>
<div class="col-md-2 col-sm-2 col-xs-12 has-feedback">
 <button type="button" class="btn btn-default addButton"><i class="fa fa-plus"></i></button>

</div>
</div>

<div class="form-group hide" id="emailTemplate">
<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
      {!!Form::select('item[]',([null => 'Choose Product'] + $product->toArray()) ,null,['class'=>'form-control ','id' => 'prettify'])!!}
</div>
<div class="col-md-4 col-sm-4 col-xs-12 has-feedback">


<input type="text" class="form-control has-feedback-left" id="Name" type="number" name="quantity[]" placeholder="ادخل الكمية" />

</div>
<div class="col-md-2 col-sm-2 col-xs-12 has-feedback">
 <button type="button" class="btn btn-default addButton"><i class="fa fa-plus"></i></button>

</div>
</div>


</fieldset> 

</fieldset>  
<br>
             <input type="hidden" name="Type_User" value="Outdoor Promoters"> 

                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                          <button type="button" class="btn btn-primary " href="/offlineneew/public/users">Cancel</button>
                          
                         <button class="btn btn-primary" type="reset">Reset</button>
                          <button type="submit"  name="login_form_submit" class="btn btn-success" value="Save">Submit</button>
                        </div>
                      </div>

      {!!Form::close() !!} 
                  </div>
                </div>
                 </div>
                
                 <div class="col-md-2 col-xs-12 ">
                 </div>
                </div>

@section('other_scripts')
<script type="text/javascript">

  $(document).ready(function(){
     $('#Has_Mixers').change(function(){
            if($(this).val() == "No")
              $("#No_Of_Mixers").attr("disabled","disabled");
            
            else
              $("#No_Of_Mixers").removeAttr('disabled');
    });
           
           $('#Has_Wood').change(function(){
            if($(this).val() == "No")
            
              $("#Wood_Meters").attr("disabled","disabled");
              else
              $("#Wood_Meters").removeAttr('disabled');
            
    });
            $('#Has_Sub_Contractor').change(function(){
            if($(this).val() == "No")
            {
              $("#Sub_Contractor1").attr("disabled","disabled");
              $("#Sub_Contractor2").attr("disabled","disabled");
            }
               else
               {
              $("#Sub_Contractor1").removeAttr('disabled');
               $("#Sub_Contractor2").removeAttr('disabled');
            }
    });
    $('#profileForm')
        .formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {

          'Capital':{
                validators: {            
                     greaterThan: {
                        value: 0,
                        message: 'The value must be greater than or equal to 0'
                    },
                      numeric: {
                            message: 'The value must be number'
                        }
                }
            },
                       'Intership_No':{
                validators: {            
                    greaterThan: {
                        value: 0,
                        message: 'The value must be greater than or equal to 0'
                    },
                      numeric: {
                            message: 'The value must be number'
                        }
                }
            },
                       'No_Of_Mixers':{
                validators: {            
                    greaterThan: {
                        value: 0,
                        message: 'The value must be greater than or equal to 0'
                    },
                      numeric: {
                            message: 'The value must be number'
                        }
                }
            },
                    'Wood_Consumption':{
                validators: {            
                    greaterThan: {
                        value: 0,
                        message: 'The value must be greater than or equal to 0'
                    },
                      numeric: {
                            message: 'The value must be number'
                        }
                }
            },
                    'Wood_Meters':{
                validators: {            
                    greaterThan: {
                        value: 0,
                        message: 'The value must be greater than or equal to 0'
                    },
                      numeric: {
                            message: 'The value must be number'
                        }
                }
            },
                   'Steel_Consumption':{
                validators: {            
                    greaterThan: {
                        value: 0,
                        message: 'The value must be greater than or equal to 0'
                    },
                      numeric: {
                            message: 'The value must be number'
                        }
                }
            },
                 
               
                           'Workers':{
                validators: {            
                    greaterThan: {
                        value: 0,
                        message: 'The value must be greater than or equal to 0'
                    },
                      numeric: {
                            message: 'The value must be number'
                        }
                }
            },
                          'Project_NO':{
                validators: {            
                     greaterThan: {
                        value: 0,
                        message: 'The value must be greater than or equal to 0'
                    },
                      numeric: {
                            message: 'The value must be number'
                        }
                }
            },
           
          
                'Class': {
                validators: {
                    between: {
                        min: 0,
                        max: 9,
                        message: 'The Class must be between 0 and 9'
                    },
                      numeric: {
                            message: 'The value must be number'
                        }
                }
            }
                    ,   'Sub_Contractor1': {
                  
                       validators: {
              
                    regexp: {
                        regexp: /^[a-zA-Z\u0600-\u06FF\s]+$/,
                        message: 'The Sub Contractor1 can only consist of alphabetical and spaces'
                    }
                    }

                    },
                       'Sub_Contractor2': {
                  
                       validators: {
               
                    regexp: {
                        regexp: /^[a-zA-Z\u0600-\u06FF\s]+$/,
                        message: 'The Sub Contractor2 can only consist of alphabetical and spaces'
                    }
                    }

                    },
        
                       'Seller1': {
                  
                       validators: {
             
                    regexp: {
                        regexp: /^[a-zA-Z\u0600-\u06FF\s]+$/,
                        message: 'The Seller 1 Debit can only consist of alphabetical and spaces'
                    }
                    }

                    }
                    ,   'Seller2': {
                  
                       validators: {
              
                    regexp: {
                        regexp: /^[a-zA-Z\u0600-\u06FF\s]+$/,
                        message: 'The Seller 2 can only consist of alphabetical and spaces'
                    }
                    }

                    },
                       'Seller3': {
                  
                       validators: {
               
                    regexp: {
                        regexp: /^[a-zA-Z\u0600-\u06FF\s]+$/,
                        message: 'The Seller 3 can only consist of alphabetical and spaces'
                    }
                    }

                    },
                      'Seller4': {
                  
                       validators: {
               
                    regexp: {
                        regexp: /^[a-zA-Z\u0600-\u06FF\s]+$/,
                        message: 'The Seller 4 can only consist of alphabetical and spaces'
                    }
                    }

                    },
                       'Area': {
                  
                       validators: {
               
                    regexp: {
                        regexp: /^[a-zA-Z\u0600-\u06FF\s]+$/,
                        message: 'The Area can only consist of alphabetical and spaces'
                    }
                    }

                    }, 'Call_Status': {
                  
                       validators: {
               
                    regexp: {
                        regexp: /^[a-zA-Z\u0600-\u06FF\s]+$/,
                        message: 'Please choose call status'
                    }
                    }

                    },
                      'Cont_Type': {
                  
                       validators: {
               
                    regexp: {
                        regexp: /^[a-zA-Z\u0600-\u06FF\s]+$/,
                        message: 'The Cont_Type can only consist of alphabetical and spaces'
                    }
                    }

                    }
           
              } ,submitHandler: function(form) {
        form.submit();
    }
        })
        .on('success.form.fv', function(e) {
   // e.preventDefault();
    var $form = $(e.target);

    // Enable the submit button
   
    $form.formValidation('disableSubmitButtons', false);
}).on('click', '.addButton', function() {
            var $template = $('#emailTemplate'),
                $clone    = $template
                                .clone()
                                .removeClass('hide')
                                .removeAttr('id')
                                .insertBefore($template),
                $email    = $clone.find('[name="item[]"]'),
                $Engname    = $clone.find('[name="quantity[]"]');

            // Add new field
            $('#profileForm').formValidation('addField', $email).formValidation('addField', $Engname);
        })

        // Remove button click handler
        .on('click', '.removeButton', function() {
            var $row   = $(this).closest('.form-group'),
                $email = $row.find('[name="item[]"]'),
                $Engname = $row.find('[name="quantity[]"]');
            // Remove element containing the email
            $row.remove();

            // Remove field
            $('#profileForm').formValidation('removeField', $email).formValidation('removeField', $Engname);
        });
         $(".Role").chosen({ 
                   width: '100%',
                   no_results_text: "No Results",
                   allow_single_deselect: true, 
                   search_contains:true, });
 $(".Role").trigger("chosen:updated");

    $('[data-toggle="popover"]').popover({ trigger: "hover" }); 

 $(".Government").chosen({ 
                   width: '100%',
                   no_results_text: "No Results",
                   allow_single_deselect: true, 
                   search_contains:true, });
 $(".Government").trigger("chosen:updated");

    $('[data-toggle="popover"]').popover({ trigger: "hover" }); 
 });

 $(".Government").change(function() {
       
            $.getJSON("/offlineneew/public/government/district/" + $(".Government").val(), function(data) {
 
                var $contractors = $(".District");
                   $(".District").chosen("destroy");
                $contractors.empty();
                $.each(data, function(index, value) {
                    $contractors.append('<option value="' + index +'">' + value + '</option>');
                });
                  
        
             $('.District').chosen({
                width: '100%',
                no_results_text:'No Results'
            });
                $(".District").trigger("chosen:updated");
            $(".District").trigger("change");
            });
  
    
});


</script>

@stop
@stop