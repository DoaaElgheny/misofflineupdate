@extends('master')


@section('content')


<meta name="csrf-token" content="{{ csrf_token() }}" />
<section class="panel panel-primary">
<div class="panel-body">

                
{!!Form::open(['route' =>[ 'Reviews.update',$Review->id],'method' => 'put','id'=>'profileForm'])!!}
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
 <div class="col-md-12">
                <label for="input_product" class="control-label col-md-3">Contractors</label>
                <div class="col-md-9"> 
              {!!Form::select('Contractor_Id',([null => 'please chooose Contractors'] + $Contractors->toArray()) ,$Review['Contractor_Id'],['class'=>'form-control subuser_list','id' => 'prettify'])!!}
             <!--  $Review->ReviewContractor->Name -->
           </div>
           </div>
          <div class="col-md-12">
                <label for="input_product" class="control-label col-md-3">Longitude</label>
                <div class="col-md-9"> 
                    <input type="number" step="any" class="form-control" id="Name" name="Long"
                    value="{{ $Review['Long'] }}"  placeholder="enter Long" >
                </div>
           </div>
             <div class="col-md-12">
                <label for="input_product" class="control-label col-md-3"> Latitude</label>
                <div class="col-md-9"> 
                    <input type="number" step="any" class="form-control" id="Name" name="Lat" value="{{ $Review['Lat'] }}"  placeholder="enter Lat" >
               </div>
            </div>
                 <div class="col-md-12">
                <label for="input_product" class="control-label col-md-3">Project Number</label>
                <div class="col-md-9"> 
                    <input type="number" class="form-control" id="Name" name="Project_NO" value="{{ $Review['Project_NO'] }}" placeholder="enter Project_NO" >
                </div>
           </div>
             <div class="col-md-12">
                <label for="input_product" class="control-label col-md-3">The Number Of Workers</label>
                <div class="col-md-9"> 
                    <input type="number" class="form-control" id="Name" name="Workers" value="{{ $Review['Workers'] }}" placeholder="enter  Workers" >
               </div>
            </div>
                 <div class="col-md-12">
                <label for="input_product" class="control-label col-md-3">Average Cement Consumption</label>
                <div class="col-md-9"> 
                    <input type="number"  step="any" class="form-control" id="Name" name="Cement_Consuption" value="{{ $Review['Cement_Consuption'] }}" placeholder="enter Cement_Consuption" >
                </div>
           </div>
             <div class="col-md-12">
                <label for="input_product" class="control-label col-md-3">Average Consumption Of The Cement Bricks</label>
                <div class="col-md-9"> 
                    <input type="number" step="any" class="form-control" id="Name" name="Cement_Bricks" value="{{ $Review['Cement_Bricks'] }}" placeholder="enter  Cement_Bricks" >
               </div>
            </div>

   <div class="col-md-12">
                <label for="input_product" class="control-label col-md-3">Average Iron Consumption</label>
                <div class="col-md-9"> 
                    <input type="number" step="any"  class="form-control" id="Name" name="Steel_Consumption" value="{{ $Review['Steel_Consumption'] }}" placeholder="enter  Steel_Consumption" >
                </div>
           </div>
              <div class="col-md-12">
                <label for="input_product" class="control-label col-md-3">Has Wood</label>
                <div class="col-md-9"> 
                  {!!Form::select('Has_Wood',
                    array( 'Yes' => 'Yes',
                    'No' => 'No')
                   ,$Review->Has_Wood,['id' => 'Has_Wood','class'=>'chosen-select TypeP form-control']);!!}  

                </div>
           </div>

               <div class="col-md-12">
                <label for="input_product" class="control-label col-md-3">Number Of Meters Of Wood</label>
                <div class="col-md-9"> 
                    <input type="number" step="any"  class="form-control" id="Wood_Meters" name="Wood_Meters" value="{{ $Review['Wood_Meters'] }}" placeholder="enter Wood_Meters" >
                </div>
           </div>
             <div class="col-md-12">
                <label for="input_product" class="control-label col-md-3">Average Wood Consumption</label>
                <div class="col-md-9"> 
                    <input type="number" step="any" class="form-control" id="Name" name="Wood_Consumption" value="{{ $Review['Wood_Consumption'] }}" placeholder="enter Wood_Consumption" >
               </div>
            </div>
   <div class="col-md-12">
                <label for="input_product" class="control-label col-md-3">Has Mixers</label>
                <div class="col-md-9"> 
                  {!!Form::select('Has_Mixers',
                    array( 'Yes' => 'Yes',
                    'No' => 'No')
                   ,$Review->Has_Mixers,['id' => 'Has_Mixers','class'=>'chosen-select TypeP form-control']);!!}  

                </div>
           </div>
       
             <div class="col-md-12">
                <label for="input_product" class="control-label col-md-3">Number Of Mixers</label>
                <div class="col-md-9"> 
                    <input type="number" class="form-control" id="No_Of_Mixers" name="No_Of_Mixers" value="{{ $Review['No_Of_Mixers'] }}" placeholder="enter No_Of_Mixers" >
               </div>
            </div>

         <div class="col-md-12">
                <label for="input_product" class="control-label col-md-3">Intership Number</label>
                <div class="col-md-9"> 
                    <input type="number" class="form-control" id="Name" name="Intership_No" value="{{ $Review['Intership_No'] }}" placeholder="enter Intership_No" >
                </div>
           </div>
             <div class="col-md-12">
                <label for="input_product" class="control-label col-md-3">Capital</label>
                <div class="col-md-9"> 
                    <input type="number" class="form-control" id="Name" name="Capital" value="{{ $Review['Capital'] }}" placeholder="enter Capital" >
               </div>
            </div>
  
 <div class="col-md-12">
                <label for="input_product" class="control-label col-md-3">Credit Debit</label>
                <div class="col-md-9"> 
                    {!!Form::select('Credit_Debit',
                    array( 'كاش' => 'كاش',
                    'أجل' => 'أجل')
                   ,[null => 'Select Choose'],['id' => 'Credit_Debit','class'=>'form-control']);!!} 
               </div>
            </div>
             <div class="col-md-12">
                <label for="input_product" class="control-label col-md-3">Has Sub Contractor</label>
                <div class="col-md-9"> 
                  {!!Form::select('Has_Sub_Contractor',
                    array( 'Yes' => 'Yes',
                    'No' => 'No')
                   ,$Review->Has_Sub_Contractor,['id' => 'Has_Sub_Contractor','class'=>'chosen-select TypeP form-control']);!!}  

                </div>
           </div>
         
 <div class="col-md-12">
                <label for="input_product" class="control-label col-md-3">Sub Contractor1</label>
                <div class="col-md-9"> 
                    <input type="text" class="form-control" id="Sub_Contractor1" name="Sub_Contractor1" value="{{ $Review['Sub_Contractor1'] }}" placeholder="enter Sub_Contractor1" >
               </div>
  </div>
 <div class="col-md-12">
                <label for="input_product" class="control-label col-md-3">Sub Contractor2</label>
                <div class="col-md-9"> 
                    <input type="text" class="form-control" id="Sub_Contractor2" name="Sub_Contractor2" value="{{ $Review['Sub_Contractor2'] }}" placeholder="enter Sub_Contractor2" >
               </div>
  </div>

 <div class="col-md-12">
                <label for="input_product" class="control-label col-md-3">Class</label>
                <div class="col-md-9"> 
                    <input type="number" class="form-control" id="Name" name="Class" value="{{ $Review['Class'] }}"  placeholder="enter Class" >
               </div>
  </div>
  <div class="col-md-12">
                <label for="input_product" class="control-label col-md-3">Seller1</label>
                <div class="col-md-9"> 
                    <input type="text" class="form-control" id="Name" name="Seller1" value="{{ $Review['Seller1'] }}"  placeholder="enter Seller1" >
               </div>
  </div>
    <div class="col-md-12">
                <label for="input_product" class="control-label col-md-3">Seller2</label>
                <div class="col-md-9"> 
                    <input type="text" class="form-control" id="Name" name="Seller2" value="{{ $Review['Seller2'] }}"  placeholder="enter Seller2" >
               </div>
  </div>
    <div class="col-md-12">
                <label for="input_product" class="control-label col-md-3">Seller3</label>
                <div class="col-md-9"> 
                    <input type="text" class="form-control" id="Name" name="Seller3" value="{{ $Review['Seller3'] }}"  placeholder="enter Seller3" >
               </div>
  </div>
  <div class="col-md-12">
                <label for="input_product" class="control-label col-md-3">Seller4</label>
                <div class="col-md-9"> 
                    <input type="text" class="form-control" id="Name" name="Seller4" placeholder="enter Seller4" value="{{ $Review['Seller4'] }}"  >
               </div>
  </div>

  <div class="col-md-12">
                <label for="input_product" class="control-label col-md-3">Status</label>
                <div class="col-md-9"> 
                               {!!Form::select('Status',
  array( 'Reviewed' => 'Reviewed',
  'Not Reviewed' => 'Not Reviewed')
 ,$Review['Status'],['id' => 'Status','name' => 'Status','class'=>'chosen-select TypeP form-control']);!!}                  </div>
            
                  
               </div>


  <div class="col-md-12">
                <label for="input_product" class="control-label col-md-3">Call_Status</label>
                <div class="col-md-9"> 
                  {!!Form::select('Call_Status',
  array( 'Updated' => 'Updated',
  'No Answer' => 'No Answer',
  'wrong Number' => 'wrong Number','Out Service' => 'Out Service')
 ,$Review['Call_Status'],['id' => 'Call_Status','name' => 'Call_Status','class'=>'chosen-select TypeP form-control']);!!}                  </div>
              
                   
               </div>


  <div class="col-md-12">
                <label for="input_product" class="control-label col-md-3">Area</label>
                <div class="col-md-9"> 
                           {!!Form::select('Area',
  array( 'Upper' => 'Upper',
  'Lower' => 'Lower')
 ,$Review['Area'],['id' => 'Area','name' => 'Area','class'=>'chosen-select TypeP form-control']);!!} 
               </div>
  </div>

  <div class="col-md-12">
                <label for="input_product" class="control-label col-md-3">Cont Type</label>
                <div class="col-md-9"> 
                    <input type="text" class="form-control" id="Name" name="Cont_Type" placeholder="enter Cont_Type" value="{{ $Review['Cont_Type'] }}"  >
               </div>
  </div>
         
               <!-- Message container -->
  <div class="form-group">
                            <div class="col-xs-9 col-xs-offset-3">
                                <div id="messageContainer"></div>
                            </div>
  </div>  

                        <div class="form-group">
                            <div class="col-xs-5 col-xs-offset-3">
                                <button type="submit" name="submitButton" class="btn btn-info" >Submit</button>  
                            </div>
                        </div>
                
                  </tr>  
             </table> 
  
          {!!Form::close()!!}


<script type="text/javascript">

  $(document).ready(function(){
     $('#Has_Mixers').change(function(){
            if($(this).val() == "No")
              $("#No_Of_Mixers").attr("disabled","disabled");
            
            else
              $("#No_Of_Mixers").removeAttr('disabled');
    });
           
           $('#Has_Wood').change(function(){
            if($(this).val() == "No")
            
              $("#Wood_Meters").attr("disabled","disabled");
              else
              $("#Wood_Meters").removeAttr('disabled');
            
    });
            $('#Has_Sub_Contractor').change(function(){
            if($(this).val() == "No")
            {
              $("#Sub_Contractor1").attr("disabled","disabled");
              $("#Sub_Contractor2").attr("disabled","disabled");
            }
               else
               {
              $("#Sub_Contractor1").removeAttr('disabled');
               $("#Sub_Contractor2").removeAttr('disabled');
            }
    });
    $('#profileForm')
        .formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
          'Capital':{
                validators: {            
                     greaterThan: {
                        value: 0,
                        message: 'The value must be greater than or equal to 0'
                    },
                      numeric: {
                            message: 'The value must be number'
                        }
                }
            },
                       'Intership_No':{
                validators: {            
                    greaterThan: {
                        value: 0,
                        message: 'The value must be greater than or equal to 0'
                    },
                      numeric: {
                            message: 'The value must be number'
                        }
                }
            },
                       'No_Of_Mixers':{
                validators: {            
                    greaterThan: {
                        value: 0,
                        message: 'The value must be greater than or equal to 0'
                    },
                      numeric: {
                            message: 'The value must be number'
                        }
                }
            },
                    'Wood_Consumption':{
                validators: {            
                    greaterThan: {
                        value: 0,
                        message: 'The value must be greater than or equal to 0'
                    },
                      numeric: {
                            message: 'The value must be number'
                        }
                }
            },
                    'Wood_Meters':{
                validators: {            
                    greaterThan: {
                        value: 0,
                        message: 'The value must be greater than or equal to 0'
                    },
                      numeric: {
                            message: 'The value must be number'
                        }
                }
            },
                   'Steel_Consumption':{
                validators: {            
                    greaterThan: {
                        value: 0,
                        message: 'The value must be greater than or equal to 0'
                    },
                      numeric: {
                            message: 'The value must be number'
                        }
                }
            },
                    'Cement_Bricks':{
                validators: {            
                     greaterThan: {
                        value: 0,
                        message: 'The value must be greater than or equal to 0'
                    },
                      numeric: {
                            message: 'The value must be number'
                        }
                }
            },
                   'Cement_Consuption':{
                validators: {            
                    greaterThan: {
                        value: 0,
                        message: 'The value must be greater than or equal to 0'
                    },
                      numeric: {
                            message: 'The value must be number'
                        }
                }
            },
                           'Workers':{
                validators: {            
                    greaterThan: {
                        value: 0,
                        message: 'The value must be greater than or equal to 0'
                    },
                      numeric: {
                            message: 'The value must be number'
                        }
                }
            },
                          'Project_NO':{
                validators: {            
                     greaterThan: {
                        value: 0,
                        message: 'The value must be greater than or equal to 0'
                    },
                      numeric: {
                            message: 'The value must be number'
                        }
                }
            },
                     'Lat':{
                validators: {
                   
     
                      greaterThan: {
                        value: 0,
                        message: 'The value must be greater than or equal to 0'
                    },
                      numeric: {
                            message: 'The value must be number'
                        }
                }
            },
           'Long':{
                validators: {
        
                      greaterThan: {
                        value: 0,
                        message: 'The value must be greater than or equal to 0'
                    },
                      numeric: {
                            message: 'The value must be number'
                        }
                }
            },
                'Class': {
                validators: {
                    between: {
                        min: 0,
                        max: 9,
                        message: 'The Class must be between 0 and 9'
                    },
                      numeric: {
                            message: 'The value must be number'
                        }
                }
            },
                  'Capital': {
                  
                       validators: {
               
                   numeric: {
                        message: 'The Capital can only consist of numbers'
                    }
                    }

                    }
                    ,   'Sub_Contractor1': {
                  
                       validators: {
              
                    regexp: {
                        regexp: /^[a-zA-Z\u0600-\u06FF\s]+$/,
                        message: 'The Sub Contractor1 can only consist of alphabetical and spaces'
                    }
                    }

                    },
                       'Sub_Contractor2': {
                  
                       validators: {
               
                    regexp: {
                        regexp: /^[a-zA-Z\u0600-\u06FF\s]+$/,
                        message: 'The Sub Contractor2 can only consist of alphabetical and spaces'
                    }
                    }

                    },
        
                       'Seller1': {
                  
                       validators: {
             
                    regexp: {
                        regexp: /^[a-zA-Z\u0600-\u06FF\s]+$/,
                        message: 'The Seller 1 Debit can only consist of alphabetical and spaces'
                    }
                    }

                    }
                    ,   'Seller2': {
                  
                       validators: {
              
                    regexp: {
                        regexp: /^[a-zA-Z\u0600-\u06FF\s]+$/,
                        message: 'The Seller 2 can only consist of alphabetical and spaces'
                    }
                    }

                    },
                       'Seller3': {
                  
                       validators: {
               
                    regexp: {
                        regexp: /^[a-zA-Z\u0600-\u06FF\s]+$/,
                        message: 'The Seller 3 can only consist of alphabetical and spaces'
                    }
                    }

                    },
                      'Seller4': {
                  
                       validators: {
               
                    regexp: {
                        regexp: /^[a-zA-Z\u0600-\u06FF\s]+$/,
                        message: 'The Seller 4 can only consist of alphabetical and spaces'
                    }
                    }

                    },
                       'Area': {
                  
                       validators: {
               
                    regexp: {
                        regexp: /^[a-zA-Z\u0600-\u06FF\s]+$/,
                        message: 'The Area can only consist of alphabetical and spaces'
                    }
                    }

                    },
                      'Cont_Type': {
                  
                       validators: {
               
                    regexp: {
                        regexp: /^[a-zA-Z\u0600-\u06FF\s]+$/,
                        message: 'The Cont_Type can only consist of alphabetical and spaces'
                    }
                    }

                    }
           
              } ,submitHandler: function(form) {
        form.submit();
    }
        })
        .on('success.form.fv', function(e) {
   // e.preventDefault();
    var $form = $(e.target);

    // Enable the submit button
   
    $form.formValidation('disableSubmitButtons', false);
});
         $(".Role").chosen({ 
                   width: '100%',
                   no_results_text: "No Results",
                   allow_single_deselect: true, 
                   search_contains:true, });
 $(".Role").trigger("chosen:updated");

    $('[data-toggle="popover"]').popover({ trigger: "hover" }); 

 $(".Government").chosen({ 
                   width: '100%',
                   no_results_text: "No Results",
                   allow_single_deselect: true, 
                   search_contains:true, });
 $(".Government").trigger("chosen:updated");

    $('[data-toggle="popover"]').popover({ trigger: "hover" }); 
 });

 $(".Government").change(function() {
       
            $.getJSON("/offlineneew/public/government/district/" + $(".Government").val(), function(data) {
 
                var $contractors = $(".District");
                   $(".District").chosen("destroy");
                $contractors.empty();
                $.each(data, function(index, value) {
                    $contractors.append('<option value="' + index +'">' + value + '</option>');
                });
                  
        
             $('.District').chosen({
                width: '100%',
                no_results_text:'No Results'
            });
                $(".District").trigger("chosen:updated");
            $(".District").trigger("change");
            });
  
    
});

</script>



</div>
</section>
@stop