@extends('masternew')
@section('content')
 <section class="panel-body">
<style type="text/css">
  input[type="file"] {
    display: none;
}
.custom-file-upload {
    border: 1px solid #ccc;
    display: inline-block;
    padding: 6px 12px;
    cursor: pointer;
}

</style>
<?php
 if(!empty($_COOKIE['FileError'])) {     
    echo "<div class='alert alert-block alert-danger fade in center'>";
    echo $_COOKIE['FileError'];
    echo "</div>";
  }
  ?>
 <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                     <h2 >Reviews <i class="fa fa-tasks" ></i></h2>
                        <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                     
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

  
<a type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal1" style="margin-bottom: 20px;" >Add New Reviews </a>


<!-- model -->
 
<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
     <div class="modal-content">
    
          <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                 <h4 class="modal-title span7 text-center" id="myModalLabel"><span class="title">Add New Reviews</span></h4>
          </div>
    <div class="modal-body">
        <div class="tabbable"> <!-- Only required for left/right tabs -->
        <ul class="nav nav-tabs">
  

        
        </ul>
        <div class="tab-content">
        <div class="tab-pane active" id="tab1">
              <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                 
                  <div class="x_content">
                    <br />
               

{!! Form::open(['action' => "ReviewsController@store",'method'=>'post','class'=>'form-horizontal form-label-left 
                    ','id'=>'profileForm']) !!}  
                 
             <fieldset style="    padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;" >
  <legend style="width: fit-content;border: 0px;" dir="rtl">Main Data</legend>
 

<div class="form-group">

<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
  {!!Form::select('Government',([null => 'please chooose'] + $governments->toArray()) ,null,['class'=>'form-control ','id' => 'Government','required'=>'required'])!!}
</div>

<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
{!!Form::select('district',([null => 'please chooose'] ) ,null,['class'=>'form-control ','id' => 'district'])!!}
</div>
</div>

<div class="form-group">

<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
{!!Form::select('Contractor_Id',([null => 'please chooose Contractors']) ,null,['class'=>'form-control','id' => 'Contractor_Id','required' => 'required','name'=>'Contractor_Id'])!!}
</div>


</div>

                     
</fieldset>
 
<fieldset style="    padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;" >
  <legend style="width: fit-content;border: 0px;" dir="rtl">Review Data</legend>
 

<div class="form-group">

<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
<input type="text" class="form-control has-feedback-left"  id="Name" name="Long" placeholder="enter Long">
<span class="fa fa-map-marker form-control-feedback left" aria-hidden="true"></span>

</div>

<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
<input type="text" class="form-control has-feedback-left"  id="Name" name="Lat" placeholder="enter Lat" >
<span class="fa fa-map-marker form-control-feedback left" aria-hidden="true"></span>
</div>
</div>
  
<div class="form-group">

<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
<input type="text" class="form-control has-feedback-left"  id="Name" name="Project_NO" placeholder="enter Project_NO" >

<span class="fa fa-map-marker form-control-feedback left" aria-hidden="true"></span>
</div>
<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
<input type="text" class="form-control has-feedback-left"  id="Name" name="Workers" placeholder="enter  Workers" >
<span class="fa fa-map-marker form-control-feedback left" aria-hidden="true"></span>
</div>

</div>

<div class="form-group">

<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
<input type="text" class="form-control has-feedback-left"  id="Name" name="Cement_Consuption" placeholder="enter Cement_Consuption" >

<span class="fa fa-map-marker form-control-feedback left" aria-hidden="true"></span>
</div>
<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
<input type="text" class="form-control has-feedback-left"  id="Name" name="Cement_Bricks" placeholder="enter  Cement_Bricks" >
<span class="fa fa-map-marker form-control-feedback left" aria-hidden="true"></span>
</div>

</div>

<div class="form-group" >

<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
<input type="text" class="form-control has-feedback-left"  id="Name" name="Steel_Consumption" placeholder="enter  Steel_Consumption">

<span class="fa fa-map-marker form-control-feedback left" aria-hidden="true"></span>
</div>
<label class="col-md-2 col-sm-2 col-xs-12 ">Has Wood</label>
 <div class="col-md-3 col-sm-3 col-xs-12 has-feedback">
 {!!Form::select('Has_Wood',
                    array( 'Yes' => 'Yes',
                    'No' => 'No')
                   ,[null => 'Select Company name'],['id' => 'Has_Wood','class'=>'chosen-select TypeP form-control']);!!}  
</div>
<div class="form-group" >
<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
<input type="text" class="form-control has-feedback-left"  id="Wood_Meters" name="Wood_Meters" placeholder="enter Wood_Meters" style="margin-bottom: 10px; ">

<span class="fa fa-map-marker form-control-feedback left" aria-hidden="true"></span>
</div>
  
</div>

</div>

<div class="form-group">

<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
<input type="text" class="form-control has-feedback-left"  id="Name" name="Wood_Consumption" placeholder="enter Wood_Consumption" >

<span class="fa fa-map-marker form-control-feedback left" aria-hidden="true"></span>
</div>
<label class="col-md-2 col-sm-2 col-xs-12 ">Has Mixers</label>
 <div class="col-md-3 col-sm-3 col-xs-12 has-feedback">
  {!!Form::select('Has_Mixers',
                    array( 'Yes' => 'Yes',
                    'No' => 'No')
                   ,[null => 'Select Company name'],['id' => 'Has_Mixers','class'=>'chosen-select TypeP form-control']);!!} 
</div>

</div>
<div class="form-group">

<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
<input type="text" class="form-control has-feedback-left"  iid="No_Of_Mixers" name="No_Of_Mixers" placeholder="enter No_Of_Mixers" >

<span class="fa fa-map-marker form-control-feedback left" aria-hidden="true"></span>
</div>
<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">

<input type="text" class="form-control has-feedback-left"  id="Name" name="Intership_No" placeholder="enter Intership_No" >

<span class="fa fa-map-marker form-control-feedback left" aria-hidden="true"></span>
</div>

</div>
<div class="form-group">

<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
<input type="text" class="form-control has-feedback-left"  id="Name" name="Capital" placeholder="enter Capital" >

<span class="fa fa-map-marker form-control-feedback left" aria-hidden="true"></span>

</div>
 <label class="col-md-2 col-sm-2 col-xs-12 ">Credit Debit</label>
 <div class="col-md-3 col-sm-3 col-xs-12 has-feedback">
  {!!Form::select('Credit_Debit',
                    array( 'كاش' => 'كاش',
                    'أجل' => 'أجل')
                   ,[null => 'Select Choose'],['id' => 'Credit_Debit','class'=>'form-control']);!!}  
</div>

</div>

<div class="form-group">

 <label class="col-md-2 col-sm-2 col-xs-12 ">Has SubContractor</label>
 <div class="col-md-3 col-sm-3 col-xs-12 has-feedback">
 {!!Form::select('Has_Sub_Contractor',
                    array( 'Yes' => 'Yes',
                    'No' => 'No')
                   ,[null => 'Select Company name'],['id' => 'Has_Sub_Contractor','class'=>'chosen-select TypeP form-control']);!!} 
</div>
<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">


<input type="text" class="form-control has-feedback-left"  id="Sub_Contractor1" name="Sub_Contractor1" placeholder="enter Sub_Contractor1" >

<span class="fa fa-map-marker form-control-feedback left" aria-hidden="true"></span>
</div>
</div>
<div class="form-group">
<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">

<input type="text" class="form-control has-feedback-left"  id="Sub_Contractor2" name="Sub_Contractor2" placeholder="enter Sub_Contractor2" >

<span class="fa fa-map-marker form-control-feedback left" aria-hidden="true"></span>
</div>
<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">


<input type="text" class="form-control has-feedback-left"  d="Name" name="Class" placeholder="enter Class" >

<span class="fa fa-map-marker form-control-feedback left" aria-hidden="true"></span>
</div>
</div>

<div class="form-group">
<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">

<input type="text" class="form-control has-feedback-left"  id="Name" name="Seller1" placeholder="enter Seller1" >

<span class="fa fa-map-marker form-control-feedback left" aria-hidden="true"></span>
</div>
<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">


<input type="text" class="form-control has-feedback-left"  id="Name" name="Seller2" placeholder="enter Seller2" >

<span class="fa fa-map-marker form-control-feedback left" aria-hidden="true"></span>
</div>
</div>


<div class="form-group">
<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">

<input type="text" class="form-control has-feedback-left"  id="Name" name="Seller3" placeholder="enter Seller3" >

<span class="fa fa-map-marker form-control-feedback left" aria-hidden="true"></span>
</div>
<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">


<input type="text" class="form-control has-feedback-left"  id="Name" name="Seller4" placeholder="enter Seller4" >

<span class="fa fa-map-marker form-control-feedback left" aria-hidden="true"></span>
</div>
</div>

<div class="form-group">
 <label class="col-md-2 col-sm-2 col-xs-12 ">Status</label>
 <div class="col-md-3 col-sm-3 col-xs-12 has-feedback">
                 {!!Form::select('Status',
  array( 'Reviewed' => 'Reviewed',
  'Not Reviewed' => 'Not Reviewed')
 ,[null => 'Select Company name'],['id' => 'Status','name' => 'Status','class'=>'chosen-select TypeP form-control']);!!} 
</div>
 <label class="col-md-2 col-sm-2 col-xs-12 ">Call Status</label>
 <div class="col-md-3 col-sm-3 col-xs-12 has-feedback">
  {!!Form::select('Call_Status',
  array( 'Updated' => 'Updated',
  'No Answer' => 'No Answer',
  'wrong Number' => 'wrong Number','Out Service' => 'Out Service')
 ,[null => 'Select Company name'],['id' => 'Call_Status','name' => 'Call_Status','class'=>'chosen-select TypeP form-control']);!!}   
</div>
</div>

<div class="form-group">
 <label class="col-md-2 col-sm-2 col-xs-12 ">Area</label>
 <div class="col-md-3 col-sm-3 col-xs-12 has-feedback">
        {!!Form::select('Area',
  array( 'Upper' => 'Upper',
  'Lower' => 'Lower')
 ,[null => 'Select  Area'],['id' => 'Area','name' => 'Area','class'=>'chosen-select TypeP form-control']);!!} 
</div>
<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">


<input type="text" class="form-control has-feedback-left" id="Name" name="Cont_Type" placeholder="enter Cont_Type" >

<span class="fa fa-map-marker form-control-feedback left" aria-hidden="true"></span> 
</div>
</div>


</fieldset>  

<fieldset style="    padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;" >
  <legend style="width: fit-content;border: 0px;" dir="rtl">Items</legend>



<div class="form-group">
<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
      {!!Form::select('item[]',([null => 'Choose Product'] + $product->toArray()) ,null,['class'=>'form-control ','id' => 'prettify'])!!}
</div>
<div class="col-md-4 col-sm-4 col-xs-12 has-feedback">


<input type="text" class="form-control has-feedback-left" id="Name" type="number" name="quantity[]" placeholder="ادخل الكمية" />

</div>
<div class="col-md-2 col-sm-2 col-xs-12 has-feedback">
 <button type="button" class="btn btn-default addButton"><i class="fa fa-plus"></i></button>

</div>
</div>

<div class="form-group hide" id="emailTemplate">
<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
      {!!Form::select('item[]',([null => 'Choose Product'] + $product->toArray()) ,null,['class'=>'form-control ','id' => 'prettify'])!!}
</div>
<div class="col-md-4 col-sm-4 col-xs-12 has-feedback">


<input type="text" class="form-control has-feedback-left" id="Name" type="number" name="quantity[]" placeholder="ادخل الكمية" />

</div>
<div class="col-md-2 col-sm-2 col-xs-12 has-feedback">
 <button type="button" class="btn btn-default addButton"><i class="fa fa-plus"></i></button>

</div>
</div>


</fieldset> 




                       


                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                          <button type="button" class="btn btn-primary " data-dismiss="modal">Cancel</button>
                         <button class="btn btn-primary" type="reset">Reset</button>
     <button type="submit"  name="login_form_submit" class="btn btn-success" value="save">Save</button>
                        </div>
                      </div>

      {!!Form::close() !!}                
        </div>
                </div>
                   </div>
                </div>
        </div>
        </div>
        </div>

    </div>
 

         </div>
     </div>
  </div>
<!-- ENDModal -->



                   
                    <table class="table table-hover table-bordered  dt-responsive nowrap Brands" cellspacing="0" width="100%" >
  <thead>
 <th>No</th>
               <th>Contractors</th>
               <th>Education</th>
              <th>Government</th>
               <th>District</th>
                <th>Address</th>
              <th>Project_NO</th>
              <th>Workers</th>
               <th>Cement_Consuption</th>
               <th> Cement_Bricks</th>
              <th>Steel_Consumption</th>
              <th>Has_Wood</th>
               <th>Wood_Meters</th>
              <th>Wood_Consumption</th>
              <th>Has_Mixers</th>
              <th>No_Of_Mixers</th>
               <th>Capital</th>
              <th>Credit_Debit</th>
              <th>Has_Sub_Contractor</th>
              <th>Sub_Contractor1</th>
              <th>Sub_Contractor2</th>
              <th>Class</th>
              <th>Seller1</th>
              <th>Seller2</th>
              <th>Seller3</th>
              <th>Seller4</th>
              <th>Status</th>
              <th>Call_Status</th>
              <th>Area</th>
              <th>Cont_Type</th>
             
               <th>Edit</th>
              <th>Process</th>        
</tfoot>


<tbody style="text-align:center;">
<?php $i=1;?>
 @foreach($Reviews as $Review)
    <tr>
    
     <td>{{$i++}}</td> 
     <td > 
  {{$Review->ReviewContractor->Name}}
 </td>
    <td > 
  {{$Review->ReviewContractor->Education}}
 </td>
   <td >{{$Review->ReviewContractor->Government}}</td>


      <td >{{$Review->ReviewContractor->District}}</td>
      <td >{{$Review->ReviewContractor->Address}}</td>


      <td >{{$Review->Project_NO}}</td>

       <td >{{$Review->Workers}}</td>


      <td>{{$Review->Cement_Consuption}}</td>

       <td >{{$Review->Cement_Bricks}}</td>

        <td >{{$Review->Steel_Consumption}}</td>
        <td>{{ $Review->Has_Wood }}</td>

      <td >{{$Review->Wood_Meters}}</td>

       <td >{{$Review->Wood_Consumption}}</td>

<td>{{$Review->Has_Mixers}}</td>

       <td >{{$Review->No_Of_Mixers}}</td>


      <td >{{$Review->Capital}}</td>

       <td >{{$Review->Credit_Debit}}</td>

<td> {{ $Review->Has_Sub_Contractor }}</td>
       <td >{{$Review->Sub_Contractor1}}</td>


      <td >{{$Review->Sub_Contractor2}}</td>

       <td >{{$Review->Class}}</td>

        <td >{{$Review->Seller1}}</td>

        <td >{{$Review->Seller2}}</td>

        <td > {{$Review->Seller3}}</td>

        <td >{{$Review->Seller4}}</td>

       <td >{{$Review->Status}}</td>

        <td >{{$Review->Call_Status}}</td>

         <td > {{$Review->Area}}</td>

         <td >{{$Review->Cont_Type}}</td>

 
 
   <td>
     <a href="/offlineneew/public/Reviews/{{$Review->id}}/edit"class="btn btn-default btn-sm" ><span class="glyphicon glyphicon-edit"></span>
       </a>
</td>
<!-- Promoter -->
  <td>
     <a href="/offlineneew/public/Reviews/destroy/{{$Review->id}}"class="btn btn-default btn-sm" ><span class="glyphicon glyphicon-trash"></span>
       </a>
</td>

   </tr>
     @endforeach
</tbody>
</table>


                  </div>
               



{!!Form::open(['action'=>'ContractorController@importContractors','method' => 'post','files'=>true])!!}

<label for="file-upload" class="custom-file-upload">
    <i class="fa fa-cloud-upload"></i> Select File
</label>
<input id="file-upload"  name="file" type="file"/>    <input type="submit" name="submit" value="Import File" class="btn btn-primary" />  
{!!Form::close()!!}


                </div>
              </div>
 
  @section('other_scripts')


<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>



   <script type="text/javascript">

var editor;

  $(document).ready(function(){
     $('#Government').change(function(){
       $(".subuser_list").chosen("destroy");
        $.get("{{ url('api/dropdown')}}", 
        { option: $(this).val() }, 
        function(data) {
            $('#district').empty(); 
            $.each(data, function(key, element) {
                $('#district').append("<option value='" + key +"'>" + element + "</option>");
            });
        });
    });
           $('#district').change(function(){
        $.get("{{ url('api/Contractor_Id')}}", 
        { option: $(this).val() }, 
        function(data) {
         
            $('#Contractor_Id').empty(); 
            $.each(data, function(key, element) {
                $('#Contractor_Id').append("<option value='" + key +"'>" + element + "</option>");
            });
            $('#Contractor_Id').addClass("subuser_list");
             $(".subuser_list").chosen({ 
                   width: '100%',
                   no_results_text: "No Results",
                   allow_single_deselect: true, 
                   search_contains:true, });
        });
    });
           $('#Has_Mixers').change(function(){
            if($(this).val() == "No")
              $("#No_Of_Mixers").attr("disabled","disabled");
            
            else
              $("#No_Of_Mixers").removeAttr('disabled');
    });
           
           $('#Has_Wood').change(function(){
            if($(this).val() == "No")
            
              $("#Wood_Meters").attr("disabled","disabled");
              else
              $("#Wood_Meters").removeAttr('disabled');
            
    });
            $('#Has_Sub_Contractor').change(function(){
            if($(this).val() == "No")
            {
              $("#Sub_Contractor1").attr("disabled","disabled");
              $("#Sub_Contractor2").attr("disabled","disabled");
            }
               else
               {
              $("#Sub_Contractor1").removeAttr('disabled');
               $("#Sub_Contractor2").removeAttr('disabled');
            }
    });
     $('#profileForm')
       .find('[name="Contractor_Id"]')
            .change(function(e) {

                $('#profileForm').formValidation('revalidateField', 'Contractor_Id');}).end()
        .formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                       'Capital':{
                validators: {            
                     greaterThan: {
                        value: 0,
                        message: 'The value must be greater than or equal to 0'
                    },
                      numeric: {
                            message: 'The value must be number'
                        }
                }
            },
                       'Intership_No':{
                validators: {            
                    greaterThan: {
                        value: 0,
                        message: 'The value must be greater than or equal to 0'
                    },
                      numeric: {
                            message: 'The value must be number'
                        }
                }
            },
                       'No_Of_Mixers':{
                validators: {            
                    greaterThan: {
                        value: 0,
                        message: 'The value must be greater than or equal to 0'
                    },
                      numeric: {
                            message: 'The value must be number'
                        }
                }
            },
                    'Wood_Consumption':{
                validators: {            
                    greaterThan: {
                        value: 0,
                        message: 'The value must be greater than or equal to 0'
                    },
                      numeric: {
                            message: 'The value must be number'
                        }
                }
            },
                    'Wood_Meters':{
                validators: {            
                    greaterThan: {
                        value: 0,
                        message: 'The value must be greater than or equal to 0'
                    },
                      numeric: {
                            message: 'The value must be number'
                        }
                }
            },
                   'Steel_Consumption':{
                validators: {            
                    greaterThan: {
                        value: 0,
                        message: 'The value must be greater than or equal to 0'
                    },
                      numeric: {
                            message: 'The value must be number'
                        }
                }
            },
                    'Cement_Bricks':{
                validators: {            
                     greaterThan: {
                        value: 0,
                        message: 'The value must be greater than or equal to 0'
                    },
                      numeric: {
                            message: 'The value must be number'
                        }
                }
            },
                   'Cement_Consuption':{
                validators: {            
                    greaterThan: {
                        value: 0,
                        message: 'The value must be greater than or equal to 0'
                    },
                      numeric: {
                            message: 'The value must be number'
                        }
                }
            },
                           'Workers':{
                validators: {            
                    greaterThan: {
                        value: 0,
                        message: 'The value must be greater than or equal to 0'
                    },
                      numeric: {
                            message: 'The value must be number'
                        }
                }
            },
                          'Project_NO':{
                validators: {            
                     greaterThan: {
                        value: 0,
                        message: 'The value must be greater than or equal to 0'
                    },
                      numeric: {
                            message: 'The value must be number'
                        }
                }
            },
                     'Lat':{
                validators: {
                   
     
                      greaterThan: {
                        value: 0,
                        message: 'The value must be greater than or equal to 0'
                    },
                      numeric: {
                            message: 'The value must be number'
                        }
                }
            },
           'Long':{
                validators: {
        
                      greaterThan: {
                        value: 0,
                        message: 'The value must be greater than or equal to 0'
                    },
                      numeric: {
                            message: 'The value must be number'
                        }
                }
            },
              'Class': {
                validators: {
                    between: {
                        min: 0,
                        max: 9,
                        message: 'The Class must be between 0 and 9'
                    },
                      numeric: {
                            message: 'The value must be number'
                        }
                }
            },
                 'Capital': {
                  
                       validators: {
               
                    numeric: {
                        message: 'The Capital can only consist of numbers'
                    }
                    }

                    }
                    ,   'Sub_Contractor1': {
                  
                       validators: {
              
                    regexp: {
                        regexp: /^[a-zA-Z\u0600-\u06FF\s]+$/,
                        message: 'The Sub Contractor1 can only consist of alphabetical and spaces'
                    }
                    }

                    },
                       'Sub_Contractor2': {
                  
                       validators: {
               
                    regexp: {
                        regexp: /^[a-zA-Z\u0600-\u06FF\s]+$/,
                        message: 'The Sub Contractor2 can only consist of alphabetical and spaces'
                    }
                    }

                    },
                   
                       'Seller1': {
                  
                       validators: {
             
                    regexp: {
                        regexp: /^[a-zA-Z\u0600-\u06FF\s]+$/,
                        message: 'The Seller 1 Debit can only consist of alphabetical and spaces'
                    }
                    }

                    }
                    ,   'Seller2': {
                  
                       validators: {
              
                    regexp: {
                        regexp: /^[a-zA-Z\u0600-\u06FF\s]+$/,
                        message: 'The Seller 2 can only consist of alphabetical and spaces'
                    }
                    }

                    },
                       'Seller3': {
                  
                       validators: {
               
                    regexp: {
                        regexp: /^[a-zA-Z\u0600-\u06FF\s]+$/,
                        message: 'The Seller 3 can only consist of alphabetical and spaces'
                    }
                    }

                    },
                      'Seller4': {
                  
                       validators: {
               
                    regexp: {
                        regexp: /^[a-zA-Z\u0600-\u06FF\s]+$/,
                        message: 'The Seller 4 can only consist of alphabetical and spaces'
                    }
                    }

                    },
                       'Area': {
                  
                       validators: {
               
                    regexp: {
                        regexp: /^[a-zA-Z\u0600-\u06FF\s]+$/,
                        message: 'The Area can only consist of alphabetical and spaces'
                    }
                    }

                    },
                      'Cont_Type': {
                  
                       validators: {
               
                    regexp: {
                        regexp: /^[a-zA-Z\u0600-\u06FF\s]+$/,
                        message: 'The Cont_Type can only consist of alphabetical and spaces'
                    }
                    }

                    },
                'item[]': {
                  
                    validators: {
                        callback: {
                            callback: function(value, validator, $field) {
                                var $emails          = validator.getFieldElements('item[]'),
                                    numEmails        = $emails.length,
                                    notEmptyCount    = 0,
                                    obj              = {},
                                    duplicateRemoved = [];

                                for (var i = 0; i < numEmails; i++) {
                                    var v = $emails.eq(i).val();
                                    if (v !== '') {
                                        obj[v] = 0;
                                        notEmptyCount++;
                                    }
                                }

                                for (i in obj) {
                                    duplicateRemoved.push(obj[i]);
                                }

                                if (duplicateRemoved.length === 0) {
                                    return {
                                        valid: false,
                                        message: 'You must fill at least one item'
                                    };
                                } else if (duplicateRemoved.length !== notEmptyCount) {
                                    return {
                                        valid: false,
                                        message: 'The item must be unique'
                                    };
                                }

                                validator.updateStatus('item[]', validator.STATUS_VALID, 'callback');
                                return true;
                            }
                        }
                    }
                }
                ,'quantity[]':
                 {
                    err: '#messageContainer1',
                    validators: {           
                      
                   notEmpty:{
                    message:"the quantity is required"
                  }, greaterThan: {
                        value: 0,
                        message: 'The value must be greater than or equal to 0'
                    },
                      numeric: {
                            message: 'The value must be number'
                        }
                    }
                    } ,
                    'Contractor_Id':{
                    validators: {
                            remote: {
                        url: '/offlineneew/public/checkcontractorexist',
                        message: 'The Contractor Already Exist',
                        type: 'GET'
                    },
                      notEmpty:{
                    message:"the Contractor_Id is required"
                  }
                   
                    }
                }
            } ,submitHandler: function(form) {
        form.submit();
        }
        })
        .on('success.form.fv', function(e) {
   // e.preventDefault();
    var $form = $(e.target);

    // Enable the submit button
   
    $form.formValidation('disableSubmitButtons', false);
}).on('click', '.addButton', function() {
            var $template = $('#emailTemplate'),
                $clone    = $template
                                .clone()
                                .removeClass('hide')
                                .removeAttr('id')
                                .insertBefore($template),
                $email    = $clone.find('[name="item[]"]'),
                $Engname    = $clone.find('[name="quantity[]"]');

            // Add new field
            $('#profileForm').formValidation('addField', $email).formValidation('addField', $Engname);
        })

        // Remove button click handler
        .on('click', '.removeButton', function() {
            var $row   = $(this).closest('.form-group'),
                $email = $row.find('[name="item[]"]'),
                $Engname = $row.find('[name="quantity[]"]');
            // Remove element containing the email
            $row.remove();

            // Remove field
            $('#profileForm').formValidation('removeField', $email).formValidation('removeField', $Engname);
        });
     var table= $('.Brands').DataTable({
  
    select:true,
    responsive: true,
    "order":[[0,"asc"]],
    'searchable':true,
    "scrollCollapse":true,
    "paging":true,
    "pagingType": "simple",
      dom: 'lBfrtip',
        buttons: [
           
            
            { extend: 'excel', className: 'btn btn-primary dtb' }
            
            
        ],

fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
$.fn.editable.defaults.send = "always";

$.fn.editable.defaults.mode = 'inline';

$('.testEdit').editable({

      validate: function(value) {
        name=$(this).editable().data('name');
        if(name=="Name")
        {
                if($.trim(value) == '') {
                    return 'Value is required.';
                  }
                    }
        },

    placement: 'right',
    url:'{{URL::to("/")}}/updateReview',
  
     ajaxOptions: {
     type: 'get',
    sourceCache: 'false',
     dataType: 'json'

   },

       params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');

            // console.log(params);
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'This Data Already Exist,Enter Correct Data.';
            } else {
                return response.responseText;
            }
        }

,
        success:function(response)
        {
            if(response.status==="You do not have permission.")
             {
              return 'You do not have permission.';
              }
        }

});

}
});
   $('.Brands tfoot th').each(function () {
      var title = $('.Brands thead th').eq($(this).index()).text();       
     if($(this).index()>=1 && $(this).index()<=6)
            {

           $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
}
        

    });
  table.columns().every( function () {
  var that = this;
 $(this.footer()).find('input').on('keyup change', function () {
          that.search(this.value).draw();
            if (that.search(this.value) ) {
               that.search(this.value).draw();
           }
        });
      
    });

 });
  </script>
@stop

@stop
