@extends('masternew')
@section('content')

<h1>Dash board Backcheck</h1>

<br>

{!!Form::open(['action'=>'ReviewsController@ExportDilyBackcheck','method' => 'post','files'=>true])!!}
<!--  <input type="submit" name="submit" value="Export Daily Backcheck" class="btn btn-primary" style="margin-bottom: 20px;"/>  --> 
{!!Form::close()!!}

<br>
<div class="col-md-12">
@foreach($BackCheck as $back)
  
    <div class="animated flipInY col-lg-4 col-md-6 col-sm-4 col-xs-12" >
                <div class="tile-stats panel-order" style="background-color: #7cc576">
                  <div class="icon"><i class="fa fa-sort-amount-desc"></i></div>
                        <div class="count huge" id="order"><strong style="color: whit">{{$back['Total']}}/{{$back['Updated']}} </strong></div>
                      <h3 style="color: #111;
    padding-left: 50px;
    font-size: x-large;
    font-weight: bold;">{{$back['Name']}}</h3>
                    <br>
                
                  <br>
             
         <l1 style="olor: #344e67;
    font-size: x-large;
    font-weight: 600;
    padding-left: 23px;">Closed line:{{$back['Closed']}}</l1>
     <br>
      <l1 style="olor: #344e67;
    font-size: x-large;
    font-weight: 600;
    padding-left: 23px;">User Busy:
   {{$back['Busy']}}</l1>
     <br>
      <l1 style="olor: #344e67;
    font-size: x-large;
    font-weight: 600;
    padding-left: 23px;">Stop Work:{{$back['Stop']}}</l1>
     <br>

      <l1 style="olor: #344e67;
    font-size: x-large;
    font-weight: 600;
    padding-left: 23px;">refused:{{$back['Refused']}}</l1>
              
               <br>   
  
     <l1 style="olor: #344e67;
    font-size: x-large;
    font-weight: 600;
    padding-left: 23px;">Wrong Number:{{$back['Wrong']}}</l1>

                </div>
              </div>

             

           
      @endforeach           

</div>

      <div class="col-md-12">


{!!Form::open(['action'=>'ReviewsController@ExportPerformanceBack','method' => 'post','files'=>true])!!}


                      <div class="col-md-4 col-sm-4 col-xs-12 has-feedback">
                        <input type='text' name="Start_Date"  id="Start_Date" placeholder=" Start Date" class="form-control has-feedback-left Start_Date"  >


                        <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                         {!!$errors->first('Start_Date','<small class="label label-danger">:message</small>')!!}

                      </div>
                       <div class="col-md-4 col-sm-4 col-xs-12 has-feedback">
                        <input type='text' name="End_Date" id="End_Date" placeholder=" End_Date" class="form-control has-feedback-left End_Date"   >


                        <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                         {!!$errors->first('End_Date','<small class="label label-danger">:message</small>')!!}


                      </div>

    

                       <div class="col-md-4 col-sm-4 col-xs-12 has-feedback">
                     
 <a class="btn btn-primary" onclick="ShowReport()">Show Report</a>





    

                      </div>

    <input type="submit" name="submit" value="Export" class="btn btn-primary" style="margin-bottom: 20px;"/>  
{!!Form::close()!!}

                      </div>
  <div class="col-md-12">
    <div id="chartBack" class="demo-placeholder" ></div>
  </div>

 @section('other_scripts')
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
  <script type="text/javascript">

var editor;
$(function(){
  $('#Start_Date').daterangepicker({
        singleDatePicker: true,
        singleClasses: "picker_4",
         locale: {
            format: 'YYYY-MM-DD'
        }
      }, function(start) {
        console.log(start.toISOString(), 'Hadeel');
      });
  $('#End_Date').daterangepicker({
        singleDatePicker: true,
        singleClasses: "picker_4",
         locale: {
            format: 'YYYY-MM-DD'
        }
      }, function(start) {
        console.log(start.toISOString(), 'Hadeel');
      });


 





 });






    function ShowReport()
{

     $.ajax({
      url: "/offlineneew/public/CalculateBackCheck",
        data:{End_Date:$('#End_Date').val(),Start_Date:$('#Start_Date').val()},
      type: "Get",
     success: function(data){
                 console.log(data);         
      var chart = new CanvasJS.Chart("chartBack",
              {
                  theme: "theme3",
                  exportEnabled: true,
                   animationEnabled: true,
                   zoomEnabled: true,
                   axisX:{
                   labelFontColor: "black",
                  labelMaxWidth: 100,

                   labelAutoFit: true ,
                     labelFontWeight: "bold"


                 },
    
                 toolTip: {
                      shared: true
                  },          
                        dataPointWidth: 20,
          data: [ 
                 {
                type: "column", 
                name: "performance per promoter",
                legendText: " Item",
                showInLegend: true, 
                 indexLabel: "{y}",
                   indexLabelFontSize: 16,
                indexLabelFontColor: "black",
                dataPoints:data.b


              }                 
                  ]
              
        });
      chart.render();
}
     });




}



 </script>
@stop
@stop