@extends('masternew')
@section('content')
<section class="panel-body">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2><i class="fa fa-tasks"></i>  Charts</h2> 
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
            <li><a class="close-link"><i class="fa fa-close"></i></a></li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
        
  
<div class="row" >
	 <div class="col-sm-12 col-md-10 col-md-offset-1 form-horizontal">
            <div class="form-group">
              <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                <label class="col-xs-3 control-label">Suervey:</label>
                <div class="col-xs-9">
{!!Form::select('Suervey_id',([null => 'Select  Suervey'] +$Survey->toArray())   ,null,['class'=>'form-control chosen-select Catogry', 'id' => 'Suervey_id','name'=>'Suervey_id','required'=>'required'])!!} 
                </div>
              </div>
            
            </div>
           
          
          </div> 
          
          <div class="col-sm-12 col-md-10 col-md-offset-1 form-horizontal">
            <div class="form-group">
            
            
            </div>
            <div class="ln_solid"></div>
            <div class="form-group">
              <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                <button type="button" onclick="ShowReport()" id="Search" name="login_form_submit" class="btn btn-success">Show Report</button>
                <label style="color:red" id="warn"></label> 
              </div>
            </div>
          </div>  
        </div>
   <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="dashboard_graph x_panel">
              <div class="row x_title">
                <div class="col-md-6">
                  <h3>Chart </h3>
                </div>
                <div class="col-md-6">
                </div>
              </div>
              <div class="x_content">
                <div class="demo-container" style="height:280px" >
                  <div id="chart-container">Chart will load here!</div>             
                </div>
              </div>
            </div>
          </div>
        </div>
     </div>
    </div>
  </div>
</section>
@section('other_scripts') 
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>


  <script type="text/javascript">
         $(document).ready(function(){


 $('.Brand').chosen({
                width: '100%',
                no_results_text:'No Results'
            });
                $(".Brand").trigger("chosen:updated");
            $(".Brand").trigger("change");

             $('.Catogry').chosen({
                width: '100%',
                no_results_text:'No Results'
            });
                $(".Catogry").trigger("chosen:updated");
            $(".Catogry").trigger("change");


 
            $('.Catogry').chosen({
                width: '100%',
                inherit_select_classes: true
            })
            // Revalidate the color when it is changed
           $('.ClassDate').daterangepicker({
        singleDatePicker: true,
        singleClasses: "picker_4",
         locale: {
            format: 'YYYY-MM-DD'
        }
      }, function(start) {
        console.log(start.toISOString(), 'Hadeel');
      });
  });
    function ShowReport()
{
  
  if($('#Suervey_id').val() !="" )
  {
   
  
    document.getElementById("warn").innerHTML="";

  // $.getJSON("/CostPerItem?Start2="+$('#Start2').val()+"&item_id="+$('#item_id').val(),



  //  function(result) {
   
    
     $.ajax({
      url: "/offlineneew/public/AnaylsisQuadrantsCharts",
        data:{Servey_ID:$('#Suervey_id').val()},
      type: "Get",
     success: function(data){
     	console.log(data[0].MinX);
     	  FusionCharts.ready(function(){
    var fusioncharts = new FusionCharts({
    type: 'bubble',
    renderAt: 'chart-container',
    width: '100%',
    height: '100%',
    dataFormat: 'json',
    dataSource: {
        "chart": {
            "theme": "fint",
            "caption": "CHART",
            "subcaption": "Last Quarter",
              "exportenabled": "1",
        "exportatclient": "1",
        "exporthandler": "http://export.api3.fusioncharts.com",
        "html5exporthandler": "http://export.api3.fusioncharts.com",
           "xAxisValuesStep":".5",
       
                     "plotFillAlpha": "70",
            "plotFillHoverColor": "#6baa01",
            "showPlotBorder": "0",
            "xAxisName": "IMPACT",
            "yAxisName": "BUDGET",
            "numDivlines": "2",
            "showValues": "1",
            "showTrendlineLabels": "0",
            "plotTooltext": "$zvalue : $zvalue%",
            //Drawing quadrants on chart
            "drawQuadrant": "1",
            "quadrantLabelTL": "High Budget / Low Impact",
            "quadrantLabelTR": "High Budget / High Impact",
            "quadrantLabelBL": "Low Budget / Low Impact",
            "quadrantLabelBR": "Low Budget / High Impact",
            //Customizing quadrant labels
            "quadrantLabelFontItalic": "1",
            "quadrantLabelFontBold": "1",
            "quadrantLabelTLFontColor": "#123456",
            "quadrantLabelTRFontColor": "#ABCDEF",
            "quadrantLabelBLFontColor": "#123ABC",
            "quadrantLabelBRFontColor": "#DEF456",                            
            //Setting x quadrant value to 54
            "quadrantXVal": data[0].averageX,
            //Setting y quadrant value to 12000
            "quadrantYVal": data[0].averageY,
            "quadrantLineAlpha": "50",
            "quadrantLineThickness": "2"
        },
        
        "categories": [{
            "category":data[0].Xaxis
        }],
        "dataset": [{
            "color": "#00aee4",
            "data": 
           data[0].dataset
                
        }],
        "trendlines": [{
            "line": [
           
             {
                "startValue":data[0].MinY ,
                "endValue": data[0].MaxY,
                "isTrendZone": "1",
                "color": "#aaaaaa",
                "alpha": "7"
            }]
        }]
    }
}
);
    fusioncharts.render();
});
                          
      
    
}
     });
}
else

document.getElementById("warn").innerHTML="Please select at least one Brand";


}
 

  </script>
@stop
@stop