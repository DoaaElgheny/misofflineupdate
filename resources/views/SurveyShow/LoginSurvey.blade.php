
<!DOCTYPE html>
<html>

<head>

<title>Login Survey</title>

<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="keywords" content="Existing Login Form Widget Responsive, Login Form Web Template, Flat Pricing Tables, Flat Drop-Downs, Sign-Up Web Templates, Flat Web Templates, Login Sign-up Responsive Web Template, Smartphone Compatible Web Template, Free Web Designs for Nokia, Samsung, LG, Sony Ericsson, Motorola Web Design">


 <link rel="stylesheet" type="text/css" href="/offlineneew/public/assets/css/bootstrap-horizon.css"> 

    <!-- Bootstrap -->
    <link href="/offlineneew/public/bower_components/gentelella/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="/offlineneew/public/bower_components/gentelella/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet prefetch" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>


<!-- <link href="css/popuo-box.css" rel="stylesheet" type="text/css" media="all" />
 --> <link rel="stylesheet" href="/offlineneew/public/assets/survayLayout/style.css" type="text/css" media="all">


<!-- Fonts -->
<link href="//fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet">
<!-- //Fonts -->

</head>
<!-- //Head -->

<!-- Body -->
<body>

  <style>
.btn-green {
  background-color: #1ab394;
  color: #fff;
  border-radius: 3px;
}
.btn-green:hover, .btn-green:focus {
    background-color: #18a689;
    color: #fff;
}
.panel-footer {
    padding: 0 15px;
    border:none;
    text-align: right;
    background-color: #fff;
}
h5{
      text-align: left;
  
}

.panel-footer {
    padding: 0 15px;
    border: none;
    text-align: right;
    background-color: transparent;
}

.panel-footer {
    padding: 10px 15px;
    /* background-color: #f5f5f5; */
    /* border-top: 1px solid #ddd; */
    border-bottom-right-radius: 3px;
    border-bottom-left-radius: 3px;
}

.table>tbody>tr>td {
  text-align: left;
}

}
 .wizard-card .picture:hover {
  border-color:  #841851;
  vertical-align: right;
      max-width: 30%;
    padding-left: 10px;
}
  .wizard-card .picture 
   {
         padding: 50px 0px 50px 0px;
     width: 50%;
    height: 40%;
    border-radius: 50%;
         
    }
</style>
{!!Form::open(['action'=>"SurveyController@CheckLogin",'method'=>'post','id'=>'profileForm','files'=>'true']) !!}
<input type="hidden" name="_token" id="token" value="{{csrf_token()}}">
<div class="col-md-12 col-ms-12 col-xs-12">
   <input type="hidden" name="survey_id" id="survey_id" value="{{$servey->id}}">
    <div class="col-md-4 col-ms-12 col-xs-12  wizard-card">
        <div class="picture-container" >
            <div class="picture" id="blah"  >
              <img src="{{$servey->Logo}}" style="margin-top: -40px">
            </div>  
        </div>
    </div> 
     <div class="col-md-8 col-ms-12 col-xs-12" style="padding-right: 28%;font-style:bold ">
      <h1 class="wow animated zoomIn" data-wow-offset="70" style="margin-top:40px">{{$servey->Name}}</h1>
     
    </div>

</div>
<div class="col-md-12 col-ms-12 col-xs-12"   style="padding-top:40px ;text-align: center;">
  <pre class="wow animated zoomIn" data-wow-offset="40" data-wow-delay="100ms" style="background:transparent;color: #fff">{{$servey->Description}}</pre>
</div>



<div class="col-md-12 col-ms-12 col-xs-12" style="padding-top:40px;padding-left: 10%;" slideInRight>
    <div class="col-md-9 col-sm-9 col-xs-12" style="margin-left:15% ">
      <input id="Email" class="form-control" data-validate-words="1" name="Email" placeholder="ادخل اسمك"  type="text" required="required" style="text-align: right; ">
      </div>
        <div class="col-md-9 col-sm-9 col-xs-12" style="margin-left:15% ">
      <input id="Tel" class="form-control" data-validate-words="1" name="Tel" placeholder="ادخل تليفونك"  type="text" required="required" style="text-align: right; ">
      </div>
</div>
<div class="col-md-6 col-sm-6 col-xs-6" style="margin-left:27% ;margin-top: 30px;">
  <button type="submit" style="background-color: #537b35;    margin-left: 27%;
    margin-top: 30px;width: 40%" class="btn btn-primary btn-block">ابدا الاستبيان</button>
 <!--    <button type="submit" style="background-color: #537b35;    margin-left: 27%;
    margin-top: 30px;width: 40%" class="btn btn-primary btn-block">Start Survey</button> -->
</div>
{!!Form::close()!!}
  <div class="col-md-6 col-sm-6 col-xs-6 " style="margin-left:25% ;margin-top: 90px;">
    <p> &copy; Copyright 2017  .Digital Marketing Team </p>
  </div>

<link rel="stylesheet" type="text/css" href="/offlineneew/public/assets/survayLayout/animate.css">
<!-- zoomIn -->
   <script src="/offlineneew/public/assets/survayLayout/wow.min.js"> new WOW().init();</script>

    <!-- jQuery -->
    <script src="/offlineneew/public/bower_components/gentelella/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
<script type="text/javascript" src="/offlineneew/public/assets/js/bootstrap.min.js"></script>
  
<script src="https://surveyjs.azureedge.net/0.12.36/survey.jquery.min.js"></script>
 




</body>

</html>



