@extends('masternew')
@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}" />
<section class="panel-body">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title"> 
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
          <li><a class="close-link"><i class="fa fa-close"></i></a></li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <h1><i class='fa fa-tasks'></i> Not Participated Contractor Report </h1>
        {!! Form::open(['action' => "TestController@ShowNotParticipatedReport",'method'=>'post','id'=>'profileForm']) !!}  
        <fieldset style="padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;">
          <legend style="width: fit-content;border: 0px;">Filter Attribute:</legend>
          <div class="row" >
            <div class="form-group col-md-offset-1">
              <div class="col-md-2 col-sm-2 col-xs-12 has-feedback">
                <label class="control-label">Start Date:</label>
              </div>
              <div class="col-md-4 col-sm-4 col-xs-12 has-feedback">
                <input type='text' name="Start_Date"  placeholder=" Start Date" class="form-control has-feedback-left Start_Date" id="single_ca"  >
                <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                {!!$errors->first('Start_Date','<small class="label label-danger">:message</small>')!!}
              </div>
              <div class="col-md-2 col-sm-2 col-xs-12 has-feedback">
                <label class="control-label">End Date:</label>
              </div>
              <div class="col-md-4 col-sm-4 col-xs-12 has-feedback">
                <input type='text' name="End_Date"  placeholder=" End_Date" class="form-control has-feedback-left End_Date" id="single_cal"  >
                <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                {!!$errors->first('End_Date','<small class="label label-danger">:message</small>')!!}
              </div>
            </div>
          </div>
          <div class="row" style="margin-top:10px;">
            <div class="form-group col-md-offset-1">
              <div class="col-md-2 col-sm-2 col-xs-12 has-feedback">
                <label class="control-label">Government:</label>
              </div>
              <div class="col-md-4 col-sm-4 col-xs-12 has-feedback">
                {!!Form::select('Government[]', ( ['all' => 'All Government'] +$Government->toArray()) ,null,['class'=>'form-control chosen-select Government ','id' => 'Government','multiple' => true,'required'=>'required'])!!}
              </div>
              <div class="col-md-2 col-sm-2 col-xs-12 has-feedback">
                <label class="control-label">District:</label>
              </div>
              <div class="col-md-4 col-sm-4 col-xs-12 has-feedback">
                {!!Form::select('District[]' ,$District->toArray(),null,['class'=>'form-control chosen-select District ','id' => 'District','multiple' => true,'required'=>'required'])!!}
              </div>
            </div>
          </div>
          <div class="row" style="margin-top: 20px;" align="center" >
            <input type="submit" class="btn btn-primary" id="Search" value=" Press To Show Report" title="Press To Show Report">
          </div>                   
        </fieldset>
      {!!Form::close() !!}                
    </div>
    <div class="row" style="margin-top: 20px;">
      <table class="table table-hover table-bordered  dt-responsive nowrap display tasks" cellspacing="0" width="100%" id="tasks">
        <thead>
          <th>Contractor Code</th>
          <th>Contractor Name</th>
          <th>Contractor Telephone1</th>
          <th>Contractor Telephone2</th>
          <th>Goverment</th>
          <th>District</th>      
        </thead>
        <tfoot>
          <th>Contractor Code</th>
          <th>Contractor Name</th>
          <th>Contractor Telephone1</th>
          <th>Contractor Telephone2</th>
          <th>Goverment</th>
          <th>District</th>
        </tfoot>
        <tbody style="text-align:center;">
          @foreach($reportactivites as $value)
            <tr>
              <th>{{$value->Code}}</th>
              <th>{{$value->Name}}</th>
              <th>{{$value->Tele1}}</th>
              <th>{{$value->Tele2}}</th>
              <th>{{$value->Government}}</th>
              <th>{{$value->District}}</th> 
            </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>
</section>
  

 @section('other_scripts')
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>


  <script type="text/javascript">

var editor;
$(function(){

 $(".Government").chosen({ 
                   width: '100%',
                   no_results_text: "No Results",
                   allow_single_deselect: true, 
                   search_contains:true, });
 $(".Government").trigger("chosen:updated");
    $('[data-toggle="popover"]').popover({ trigger: "hover" }); 
       $(".District").chosen({ 
                   width: '100%',
                   no_results_text: "No Results",
                   allow_single_deselect: true, 
                   search_contains:true, });
 $(".District").trigger("chosen:updated");
    $('[data-toggle="popover"]').popover({ trigger: "hover" }); 
    $(".Government").change(function() {
       
            $.getJSON("/offlineneew/public/getdistrict/" + $("#Government").val(), function(data) {
            var $product = $(".District");
                   $(".District").chosen("destroy");
                $product.empty();
                $.each(data, function(index, value) {
                    $product.append('<option value="' + index +'">' + value + '</option>');
                });
                  
        
             $('.District').chosen({
                width: '100%',
                no_results_text:'No Results'
            });
                $(".District").trigger("chosen:updated");
            $(".District").trigger("change");
   });
            });

  $('#single_cal').daterangepicker({
        singleDatePicker: true,
        singleClasses: "picker_4",
         locale: {
            format: 'YYYY-MM-DD'
        }
      }, function(start) {
        console.log(start.toISOString(), 'Hadeel');
      });
  $('#single_ca').daterangepicker({
        singleDatePicker: true,
        singleClasses: "picker_4",
         locale: {
            format: 'YYYY-MM-DD'
        }
      }, function(start) {
        console.log(start.toISOString(), 'Hadeel');
      });
var table = $('#tasks').dataTable({

select:true,
responsive: true,
"order":[[0,"asc"]],
'searchable':true,
"scrollCollapse":true,
"paging":true,
"pagingType": "simple",
dom: 'lBfrtip',
buttons: [


{ extend: 'excel', className: 'btn btn-primary dtb' }


]});

 
   $('.tasks tfoot th').each(function () {



      var title = $('.tasks thead th').eq($(this).index()).text();
               
 if($(this).index()>=0 && $(this).index()<=14)
        {

           $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
}
        

    });
  table.columns().every( function () {
  var that = this;
 $(this.footer()).find('input').on('keyup change', function () {
          that.search(this.value).draw();
            if (that.search(this.value) ) {
               that.search(this.value).draw();
           }
        });
      
 
   });
 });
  </script>
@stop
@stop