@extends('masternew')
@section('content')
  <section class="panel-body">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2><i class="fa fa-tasks"></i>Marketing Actitvity</h2> 
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
            <li><a class="close-link"><i class="fa fa-close"></i></a></li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <a type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal1" style="margin-bottom: 20px;"> Add New Marketing Actitvity </a>
          <!-- model -->
          <div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                  <h4 class="modal-title span7 text-center" id="myModalLabel"><span class="title">Add New Marketing Actitvity</span></h4>
                </div>
                <div class="modal-body">
                  <div class="tabbable"> <!-- Only required for left/right tabs -->
                    <ul class="nav nav-tabs"></ul>
                    <div class="tab-content">
                      <div class="tab-pane active" id="tab1">
                        <div class="row">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                              <div class="x_content">
                                <br />
                                {!! Form::open(['action' => "MarktingactitvityController@store",'method'=>'post','id'=>'profileForm']) !!} 
                                  <fieldset style="    padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;">
                                    <legend style="width: fit-content;border: 0px;">Main Date:</legend>
                                      <div class="form-group">
                                        <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
<input type="text" class="form-control has-feedback-left"  id="name" name="name" placeholder="Markting Actitvity Name" required>
                                          <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                                        </div>
                                      </div>                    
                                  </fieldset>
                                  <div class="ln_solid"></div>
                                  <div class="form-group">
                                    <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                                      <button type="button" class="btn btn-primary " data-dismiss="modal">Cancel</button>
                               <!--        <button class="btn btn-primary" type="reset">Reset</button> -->
                                      <button type="submit"  name="login_form_submit" class="btn btn-success" value="Save" disabled="disabled">Submit</button>
                                    </div>
                                  </div>
                                {!!Form::close() !!}                
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- ENDModal -->
          <br/>
          <a type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal2" style="margin-bottom: 20px;">Add Photo To Gallery</a>
          <!-- model --> 
          <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                  <h4 class="modal-title span7 text-center" id="myModalLabel"><span class="title">Add Photo To Gallery</span></h4>
                </div>
                <div class="modal-body">
                  <div class="tabbable"> <!-- Only required for left/right tabs -->
                  <ul class="nav nav-tabs"></ul>
                  <div class="tab-content">
                    <div class="tab-pane active" id="tab1">
                      <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <div class="x_panel">             
                            <div class="x_content">
                              <br />
                              {!! Form::open(['action' => "MarktingactitvityController@storephoto",'method'=>'post','id'=>'surveyForm','files'=>'true']) !!} 
                                <fieldset style="    padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;">
                                  <legend style="width: fit-content;border: 0px;">Main Date:</legend>
                                  <div class="form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">

                                      {!!Form::select('activity',([null => 'please chooose Marketing Activity'] + $marketingactitvities->toArray()) ,null,['class'=>'form-control subuser_list','id' => 'prettify','required'=>'required'])!!}
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                                      <label class="col-xs-2 control-label">Photos:</label>
                                      <div class="col-xs-8">
                                        <input type="file" name="img[]">  
                                      </div>
                                      <div class="col-xs-2">
                                        <button type="button" class="btn btn-default addButton"><i class="fa fa-plus"></i></button>
                                      </div>
                                    </div>
                                  </div>
                                  <!-- The template containing an email field and a Remove button -->
                                  <div class="form-group hide" id="emailTemplate">
                                    <label class="col-xs-2 control-label"></label>
                                      <div class="col-xs-8">
                                           {!! Form::file('img[]', ['class' => 'form-control','id'=>'uploadFrontFile','name'=>'img[]']) !!}
                                      </div>
                                    
                                      <div class="col-xs-2">
                                          <button type="button" class="btn btn-default removeButton"><i class="fa fa-minus"></i></button>
                                      </div>
                                  </div>                  
                                </fieldset>
                                <div class="ln_solid"></div>
                                <div class="form-group">
                                  <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                                    <button type="button" class="btn btn-primary " data-dismiss="modal">Cancel</button>
                                   <button class="btn btn-primary" type="reset">Reset</button>
                                    <button type="submit" disabled="disabled"  name="login_form_submit" class="btn btn-success"  value="Save">Submit</button>
                                  </div>
                                </div>
                              {!!Form::close() !!}                
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- ENDModal -->
        <table class="table table-hover table-bordered  dt-responsive nowrap  tasks" cellspacing="0" width="100%">
          <thead>
            <th>No</th>
            <th>Marketing Actitvity Category</th>
            <th>Show All Activities</th> 
            <th>Show Gallery</th> 
            <th>Process</th>  
          </thead>
          <tfoot>
            <th></th>
            <th>Marketing Actitvity Category</th>
            <th></th> 
            <th></th> 
            <th></th>  
          </tfoot>
          <tbody style="text-align:center;">
            <?php $i=1;?>
            @foreach($marktingactitvities as $marktingactitvity)
              <tr>
                <td>{{$i++}}</td>   
                <td><a  class="testEdit" data-type="text" data-name="Name"  data-pk="{{ $marktingactitvity->id }}">{{$marktingactitvity->Name}}</a></td> 
                <td><a href="/offlineneew/public/subacivity/{{$marktingactitvity->id}}"class="btn btn-default btn-sm" ><span class="glyphicon glyphicon-plus"></span>  </a></td>
                <td><a href="/offlineneew/public/showgallery/{{$marktingactitvity->id}}"class="btn btn-default btn-sm" ><span class="glyphicon glyphicon-plus"></span>  </a></td>
                <td><a href="/offlineneew/public/marktingactitvities/destroy/{{$marktingactitvity->id}}"class="btn btn-default btn-sm" ><span class="glyphicon glyphicon-trash"></span>  </a></td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</section>
@section('other_scripts')
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
<script type="text/javascript">

var editor;

  $(document).ready(function(){
    
        $('#surveyForm')
        .formValidation({
            framework: 'bootstrap',

            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                'img[]': {
                    err: '#messageContainer',
                    validators: {
                     
                    }
                },
                'activity':{
                   validators: {
                         notEmpty:{
                    message:"this field is required"
                  }
                     
                    }
                }
               
            } ,submitHandler: function(form) {
        form.submit();
    }
        })
        .on('success.form.fv', function(e) {
   // e.preventDefault();
    var $form = $(e.target);

    // Enable the submit button
   
    $form.formValidation('disableSubmitButtons', false);
})
        // Add button click handler
        .on('click', '.addButton', function() {
            var $template = $('#emailTemplate'),
                $clone    = $template
                                .clone()
                                .removeClass('hide')
                                .removeAttr('id')
                                .insertBefore($template),
                $email    = $clone.find('[name="img[]"]');

            // Add new field
            $('#surveyForm').formValidation('addField', $email);
        })

        // Remove button click handler
        .on('click', '.removeButton', function() {
            var $row   = $(this).closest('.form-group'),
                $email = $row.find('[name="img[]"]');
              
            // Remove element containing the email
            $row.remove();

            // Remove field
            $('#surveyForm').formValidation('removeField', $email);
        });

    $('#profileForm')
        .formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                'name': {
                    validators: {
                            remote: {
                        url: '/offlineneew/public/checkmarketactivityname',
                        message: 'The Marketing_Activity name is not available',
                        type: 'GET'
                    },
                      notEmpty:{
                    message:"the  Marketing_Activity name is required"
                  }
                    ,
                    regexp: {
                        regexp: /^[A-z\s]+$/,
                        message: 'The Marketing_Activity  name can only consist of alphabetical and spaces'
                    }
                    }
                }
              
            } ,submitHandler: function(form) {
        form.submit();
    }
        })
        .on('success.form.fv', function(e) {
   // e.preventDefault();
    var $form = $(e.target);

    // Enable the submit button
   
    $form.formValidation('disableSubmitButtons', false);
});

     var table= $('.tasks').DataTable({
  
    select:true,
    responsive: true,
    "order":[[0,"asc"]],
    'searchable':true,
    "scrollCollapse":true,
    "paging":true,
    "pagingType": "simple",
      dom: 'lBfrtip',
        buttons: [
           
            
            { extend: 'excel', className: 'btn btn-primary dtb' }
            
            
        ],

fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
$.fn.editable.defaults.send = "always";

$.fn.editable.defaults.mode = 'inline';

$('.testEdit').editable({

      validate: function(value) {
        name=$(this).editable().data('name');
        if(name=="Name")
        {
                if($.trim(value) == '') {
                    return 'Value is required.';
                  }
        }
           if(name=="Name")
        {

      var regexp = /^[a-zA-Z\u0600-\u06FF,-][\sa-zA-Z\u0600-\u06FF,-]/;            if (!regexp.test(value)) {
            return 'This field is not valid';
        }
        
        }
      
        },

    placement: 'right',
    url:'{{URL::to("/")}}/updatemarktingactitvity',
  
     ajaxOptions: {
     type: 'get',
    sourceCache: 'false',
     dataType: 'json',
   },

       params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');

            // console.log(params);
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'This Data Already Exist,Enter Correct Data.';
            }
             else {
                return response.responseText;
            }
        },
        success:function(response)
        {
            if(response.status==="You do not have permission.")
             {
              return 'You do not have permission.';
              }
        }


});

}
});

 



   $('.tasks tfoot th').each(function () {



      var title = $('.tasks thead th').eq($(this).index()).text();
               
 if($(this).index()>=1 && $(this).index()<=1)
        {

           $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
}
        

    });
  table.columns().every( function () {
  var that = this;
 $(this.footer()).find('input').on('keyup change', function () {
          that.search(this.value).draw();
            if (that.search(this.value) ) {
               that.search(this.value).draw();
           }
        });
      
    });


 });
function ActiveCheck(e,id)
{
  var act;
  if(e.value==1)
    act=0;
  else
    act=1;

  $.ajax({
      url: "/offlineneew/public/updateactive?active="+act+"&id="+id+"'",
      type: "get",
      success: function(data){
      }
    }); 
}
  </script>
@stop
@stop
