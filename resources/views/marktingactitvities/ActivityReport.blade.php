@extends('masternew')
@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}" />

<section class="panel-body">
 <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title"> 
                  <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                     
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

    <h1><i class='fa fa-tasks'></i>Participated Contractor Report </h1>
<div>
 {!! Form::open(['action' => "TestController@ShowActivityReport",'method'=>'post','id'=>'profileForm']) !!}  

         <fieldset style="    padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;">
  <legend style="width: fit-content;border: 0px;">Filter Attribute:</legend>
 

 <div class="row" >
<div class="form-group col-md-offset-1">
<div class="col-md-2 col-sm-2 col-xs-12 has-feedback">
        <label class="control-label">Start Date:</label>
      </div>
                      <div class="col-md-4 col-sm-4 col-xs-12 has-feedback">
                        <input type='text' name="Start_Date"  placeholder=" Start Date" class="form-control has-feedback-left Start_Date" id="single_ca"  >


                        <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                         {!!$errors->first('Start_Date','<small class="label label-danger">:message</small>')!!}

                      </div><div class="col-md-2 col-sm-2 col-xs-12 has-feedback">
        <label class="control-label">End Date:</label>
      </div>
                       <div class="col-md-4 col-sm-4 col-xs-12 has-feedback">
                        <input type='text' name="End_Date"  placeholder=" End_Date" class="form-control has-feedback-left End_Date" id="single_cal"  >


                        <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                         {!!$errors->first('End_Date','<small class="label label-danger">:message</small>')!!}

                      </div>
                         {!!$errors->first('End_Date','<small class="label label-danger">:message</small>')!!}
                      </div>


                      </div>
                      <div class="row" style="height: 30px;">
                      </div>
                       <div class="row" >
<div class="form-group col-md-offset-1">
<div class="col-md-2 col-sm-2 col-xs-12 has-feedback">
        <label class="control-label">Activity:</label>
      </div>
                      <div class="col-md-4 col-sm-4 col-xs-12 has-feedback">
{!!Form::select('subactivity[]', ( ['all' => 'All Activity'] +$subactivties->toArray()) ,null,['class'=>'form-control chosen-select subactivity ','id' => 'company','multiple' => true,'required'=>'required'])!!}
                      </div>
      <div class="col-md-2 col-sm-2 col-xs-12 has-feedback">
        <label class="control-label">Round:</label>
      </div>
       <div class="col-md-4 col-sm-4 col-xs-12 has-feedback">
        {!!Form::select('Activity[]' ,$activity->toArray(),null,['class'=>'form-control chosen-select Activity ','id' => 'Activity','multiple' => true,'required'=>'required'])!!}

                      </div>
                        
                      </div>


                      </div>
              <div class="row" style="margin-top: 20px;" align="center" >
                                <div  >
            <input type="submit" class="btn btn-primary" id="Search" value=" Press To Show Report" title="Press To Show Report">
              
            </div>  
            </div>       
                 
</fieldset>
      {!!Form::close() !!}                

</div>


<div class="row" style="margin-top: 20px;">



 
                       <table class="table table-hover table-bordered  dt-responsive nowrap display tasks" cellspacing="0" width="100%" id="tasks">

  <thead>
    <th>Contractor Code</th>
    <th>Contractor Name</th>
    <th>Contractor Telephone1</th>
    <th>Contractor Telephone2</th>
    <th>Goverment</th>
    <th>District</th>
    <th>Date</th> 
    <th>Activity Name</th> 
    <th>Sub Activity</th>          
</thead>
  <tfoot>
    <th>Contractor Code</th>
    <th>Contractor Name</th>
    <th>Contractor Telephone1</th>
    <th>Contractor Telephone2</th>
    <th>Goverment</th>
    <th>District</th>
    <th>Date</th> 
    <th>Activity Name</th> 
    <th>Sub Activity</th>  
</tfoot>


  <tbody style="text-align:center;">

    @foreach($reportactivites as $value)
    <tr>
   <th>{{$value->Code}}</th>
    <th>{{$value->Name}}</th>
     <th>{{$value->Tele1}}</th>
      <th>{{$value->Tele2}}</th>
    <th>{{$value->Government}}</th>
    <th>{{$value->District}}</th>
    <th>{{$value->Date}}</th>
    <th>{{$value->activityname}}</th> 
    <th>{{$value->SubActivity}}</th> 
   </tr>
    @endforeach
</tbody>
</table>
 </div>
  </div>
    </div>
  

  </div>

</section>
  

 @section('other_scripts')
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>


  <script type="text/javascript">

var editor;
$(function(){

   $(".subactivity").chosen({ 
                   width: '100%',
                   no_results_text: "No Results",
                   allow_single_deselect: true, 
                   search_contains:true, });
 $(".subactivity").trigger("chosen:updated");
    $('[data-toggle="popover"]').popover({ trigger: "hover" }); 
       $(".Activity").chosen({ 
                   width: '100%',
                   no_results_text: "No Results",
                   allow_single_deselect: true, 
                   search_contains:true, });
 $(".Activity").trigger("chosen:updated");
    $('[data-toggle="popover"]').popover({ trigger: "hover" }); 
    $(".subactivity").change(function() {
       
            $.getJSON("/offlineneew/public/getactivity/" + $(".subactivity").val()+"/" + $(".Start_Date").val()+"/" + $(".End_Date").val(), function(data) {
 
                var $product = $(".Activity");
                   $(".Activity").chosen("destroy");
                $product.empty();
                $.each(data, function(index, value) {
                    $product.append('<option value="' + index +'">' + value + '</option>');
                });
                  
        
             $('.Activity').chosen({
                width: '100%',
                no_results_text:'No Results'
            });
                $(".Activity").trigger("chosen:updated");
            $(".Activity").trigger("change");
   });
            });
  $('#single_cal').daterangepicker({
        singleDatePicker: true,
        singleClasses: "picker_4",
         locale: {
            format: 'YYYY-MM-DD'
        }
      });
  $('#single_ca').daterangepicker({
        singleDatePicker: true,
        singleClasses: "picker_4",
         locale: {
            format: 'YYYY-MM-DD'
        }
      });
var table = $('#tasks').dataTable({

select:true,
responsive: true,
"order":[[0,"asc"]],
'searchable':true,
"scrollCollapse":true,
"paging":true,
"pagingType": "simple",
dom: 'lBfrtip',
buttons: [


{ extend: 'excel', className: 'btn btn-primary dtb' }


]});

 
   $('.tasks tfoot th').each(function () {



      var title = $('.tasks thead th').eq($(this).index()).text();
               
 if($(this).index()>=0 && $(this).index()<=14)
        {

           $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
}
        

    });
  table.columns().every( function () {
  var that = this;
 $(this.footer()).find('input').on('keyup change', function () {
          that.search(this.value).draw();
            if (that.search(this.value) ) {
               that.search(this.value).draw();
           }
        });       
   });
 });
  </script>
@stop
@stop
