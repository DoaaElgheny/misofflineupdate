@extends('masternew')
@section('content')
 <section class="panel-body">
<style type="text/css">
  
</style>
 <div class="col-md-12 col-sm-12 col-xs-12">
 <div class="x_panel">
 <div class="x_title">
                     <h2><i class="fa fa-tasks"></i> Supplier</h2> 
                        <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                     
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

  
<!-- <a type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal1" style="margin-bottom: 20px;"> Add New Supplier </a> -->



     <!-- model -->
 
<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
     <div class="modal-content">
    
          <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
               <h4 class="modal-title span7 text-center" id="myModalLabel" style="    text-align: center;
    color: #15202a;
    font-size: 20px;
    font-weight: bold;"><span class="title">Add New Supplier</span></h4>
          </div>
    <div class="modal-body">
        <div class="tabbable"> <!-- Only required for left/right tabs -->
        <ul class="nav nav-tabs">
  

        
        </ul>
        <div class="tab-content">
        <div class="tab-pane active" id="tab1">
              <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                 
                  <div class="x_content">
                    <br />
{!!Form::open(['action'=>"SupplierController@store",'method'=>'post','id'=>'profileForm'])!!} 
             <fieldset style="    padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;">
  <legend style="width: fit-content;border: 0px;">Main Date:</legend>
     <div class="form-group">
         <div class="col-md-12 col-sm-12 col-xs-12 has-feedback">
             <input type="text" class="form-control has-feedback-left"  id="Name" name="Name" placeholder="Supplier Name" style=" border: 1px solid #15202a;border-radius: 4px; margin-bottom: 9px;">


                  <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
         </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 has-feedback">
                          <input type="email" class="form-control has-feedback-left" id="Email" name="Email" placeholder="Email" style=" border: 1px solid #15202a;border-radius: 4px; margin-bottom: 9px;">
                                       <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>

                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 has-feedback">
                          <input type="number" class="form-control has-feedback-left" id="Tele" name="Tele" placeholder="Tele" style=" border: 1px solid #15202a;border-radius: 4px; margin-bottom: 9px;">
                                       <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>

                        </div>
                      </div>

                   
                    
                      
                        </fieldset>
                      <div class="ln_solid"></div>
                      <div class="form-group">
                <div class="col-md-12 col-sm-12 col-xs-12 " style="text-align:center;">
                          <button type="button" class="btn btn-danger " data-dismiss="modal">Cancel</button>
                         <button class="btn btn-info" type="reset">Reset</button>
                          <button type="submit" disabled="disabled" name="login_form_submit" class="btn btn-success" value="Save">Submit</button>
                        </div>
                      </div>

      {!!Form::close() !!}                
        </div>
                </div>
                   </div>
                </div>
        </div>
        </div>
        </div>

    </div>
 

         </div>
     </div>
  </div>
<!-- ENDModal -->
 



                   
                    <table class="table table-hover table-bordered  dt-responsive nowrap  warehouse" cellspacing="0" width="100%">
   <thead>
        <th></th>

				<th>companyName</th>
				<th>Spcailzation</th>
				<th>credtional</th>
        <th>Fax</th>

        <th>landline1</th>
        <th>landline2</th>
         <th>website</th>
         <th>adress</th>

        <th>Email</th>
        <th>landline3</th>
        <th>Whatsapp</th>

        <th>ownerName</th>
        <th>Name</th>
        <th>Tele</th>
        
       

        <th>Process</th>  
  </thead>

 
 <tfoot>
        <th></th>
				  <th>companyName</th>
        <th>Spcailzation</th>
        <th>credtional</th>
        <th>Fax</th>

        <th>landline1</th>
        <th>landline2</th>
        <th>website</th>
        <th>adress</th>

        <th>Email</th>
        <th>landline3</th>
        <th>Whatsapp</th>

        <th>ownerName</th>
        <th>Name</th>
        <th>Tele</th>
        
        
      <th>Process</th>
</tfoot>


<tbody style="text-align:center;">
<?php $i=1;?>
 @foreach($Supplier as $Value)
    <tr>
     <td>{{$i++}}</td> 

  <td ><a  class="testEdit" data-type="text" data-name="companyName"  data-pk="{{ $Value->id}}">{{$Value->companyName}}</a></td>

 <td ><a  class="testEdit" data-type="text" data-name="Spcailzation"  data-pk="{{ $Value->id }}">{{$Value->Spcailzation}}</a></td>

  <td ><a  class="testEdit" data-type="text" data-name="credtional"  data-pk="{{ $Value->id}}">{{$Value->credtional}}</a></td>

   <td ><a  class="testEdit" data-type="text" data-name="Fax"  data-pk="{{ $Value->id}}">{{$Value->Fax}}</a></td>
    

    <td ><a  class="testEdit" data-type="text" data-name="landline1"  data-pk="{{ $Value->id}}">{{$Value->landline1}}</a></td>

     <td ><a  class="testEdit" data-type="text" data-name="landline2"  data-pk="{{ $Value->id}}">{{$Value->landline2}}</a></td>
      <td ><a  class="testEdit" data-type="text" data-name="website"  data-pk="{{ $Value->id }}">{{$Value->website}}</a></td>
 <td ><a  class="testEdit" data-type="text" data-name="adress"  data-pk="{{$Value->id }}">{{$Value->adress}}</a></td>
           
           <td ><a  class="testEdit" data-type="text" data-name="Email"  data-pk="{{ $Value->id }}">{{$Value->Email}}</a></td>
      <td ><a  class="testEdit" data-type="text" data-name="landline3"  data-pk="{{$Value->id}}">{{$Value->landline3}}</a></td>

       <td ><a  class="testEdit" data-type="text" data-name="Whatsapp"  data-pk="{{$Value->id}}">{{$Value->Whatsapp}}</a></td>

       <td ><a  class="testEdit" data-type="text" data-name="ownerName"  data-pk="{{ $Value->id}}">{{$Value->ownerName}}</a></td>
        <td ><a  class="testEdit" data-type="text" data-name="Name"  data-pk="{{$Value->id}}">{{$Value->Name}}</a></td>

         <td ><a  class="testEdit" data-type="text" data-name="Tele"  data-pk="{{$Value->id}}">{{$Value->Tele}}</a></td>

         
           
           
       
       
       


      <td>
       <a href="/offlineneew/public/DeleteSupplier/{{$Value->id}}" class="btn btn-default btn-sm" ><span class="glyphicon glyphicon-trash"></span>  </a>
      </td>


   </tr>
     @endforeach
</tbody>
</table>


                  </div>
                </div>
              </div>
 
{!!Form::open(['action'=>'SupplierController@importSupplier','method' => 'post','files'=>true])!!}

<input id="file-upload"  name="file" type="file"/>
    <input type="submit" name="asd" value="Import File" class="btn btn-primary" />   
{!!Form::close()!!}

@section('other_scripts')


<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>


<script>
$(document).ready(function() {
     $('.District').chosen({
                width: '100%',
                no_results_text:'No Results'
            });
                $(".District").trigger("chosen:updated");
            $(".District").trigger("change");
    $('#profileForm')
        .find('[name="Government"]')
            .chosen({
                width: '100%',
                inherit_select_classes: true
            })
            // Revalidate the color when it is changed
            .change(function(e) {

                $('#profileForm').formValidation('revalidateField', 'Government');

  $.getJSON("./government/district/" + $("#Government").val(), function(data) {
 
                var $contractors = $(".District");
                   $(".District").chosen("destroy");
                $contractors.empty();
                $.each(data, function(index, value) {
                    $contractors.append('<option value="' + index +'">' + value + '</option>');
                });
                  
        
             $('.District').chosen({
                width: '100%',
                no_results_text:'No Results'
            });
                $(".District").trigger("chosen:updated");
            $(".District").trigger("change");
            });
            })
            .end()  
        .formValidation({
            framework: 'bootstrap',
            excluded: ':disabled',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                Government: {
                    validators: {
                        callback: {
                            message: 'Please choose Government',
                            callback: function(value, validator, $field) {
                                // Get the selected options
                                var options = validator.getFieldElements('Government').val();
                              
                                return (options !== '' );
                            }
                        }
                    }
                },
                   'Name':{
                validators: {
                    notEmpty: {
                        message: 'The  name is required'
                    }
                }
            },
                'Email': {
                   validators: {
                     
                   notEmpty:{
                     message:"the Address is required"
                    }
                     }
                },
                'Tele': {
                   validators: {
                   notEmpty:{
                     message:"the EName is required"
                    }
                    
                     }
                }
              
            }
        });
});
</script>


  <script type="text/javascript">

var editor;

  $(document).ready(function(){

  



     var table= $('.warehouse').DataTable({
  
    select:true,
    responsive: true,
    "order":[[0,"asc"]],
    'searchable':true,
    "scrollCollapse":true,
    "paging":true,
    "pagingType": "simple",
      dom: 'lBfrtip',
        buttons: [
           
            
            { extend: 'excel', className: 'btn btn-primary dtb' }
            
            
        ],

fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
$.fn.editable.defaults.send = "always";

$.fn.editable.defaults.mode = 'inline';

$('.testEdit').editable({

      validate: function(value) {
        name=$(this).editable().data('name');
        if(name=="Name"||name=="address"||name=="location"||name=="EngName")
        {
                if($.trim(value) == '') {
                    return 'Value is required.';
                  }
                    }
      
      
    },
       
    placement: 'right',
    url:'{{URL::to("/")}}/updateSupplier',
  
     ajaxOptions: {
     type: 'get',
    sourceCache: 'false',
     dataType: 'json'

   },

       params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');

            // console.log(params);
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'This Data Already Exist,Enter Correct Data.';
            } else {
                return response.responseText;
            }
        }
        ,
        success:function(response)
        {
            if(response.status==="You do not have permission.")
             {
              return 'You do not have permission.';
              }
        }


});

}
});

 



   $('.warehouse tfoot th').each(function () {



      var title = $('.warehouse thead th').eq($(this).index()).text();
               
 if($(this).index()>=1 && $(this).index()<=6)
        {

           $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
}
        

    });
  table.columns().every( function () {
  var that = this;
 $(this.footer()).find('input').on('keyup change', function () {
          that.search(this.value).draw();
            if (that.search(this.value) ) {
               that.search(this.value).draw();
           }
        });
      
    });

 $(".Government").trigger("chosen:updated");

    $('[data-toggle="popover"]').popover({ trigger: "hover" }); 
 });

  </script>
@stop
@stop
