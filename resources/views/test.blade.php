
<!DOCTYPE html>
<html>

<head>

<title></title>

<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="keywords" content="Existing Login Form Widget Responsive, Login Form Web Template, Flat Pricing Tables, Flat Drop-Downs, Sign-Up Web Templates, Flat Web Templates, Login Sign-up Responsive Web Template, Smartphone Compatible Web Template, Free Web Designs for Nokia, Samsung, LG, Sony Ericsson, Motorola Web Design">

<meta name="csrf-token" content="{{ csrf_token() }}" />
 <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-horizon.css"> 

    <!-- Bootstrap -->
    <link href="/offlineneew/public/bower_components/gentelella/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="/offlineneew/public/bower_components/gentelella/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet prefetch" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>


<!-- <link href="css/popuo-box.css" rel="stylesheet" type="text/css" media="all" />
 --> <link rel="stylesheet" href="/offlineneew/public/assets/survayLayout/style.css" type="text/css" media="all">

<!-- Fonts -->
<link href="//fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet">
<!-- //Fonts -->

</head>
<!-- //Head -->

<!-- Body -->
<body>

  <style>
 

.btn-green {
  background-color: #1ab394;
  color: #fff;
  border-radius: 3px;
}
.btn-green:hover, .btn-green:focus {
    background-color: #18a689;
    color: #fff;
}
.panel-footer {
    padding: 0 15px;
    border:none;
    text-align: right;
    background-color: #fff;
}
/*h5{
      text-align: left;
  
}*/
h5{
      text-align: right;
  
}

th {
    text-align: right;
}
.panel-footer {
    padding: 0 15px;
    border: none;
    text-align: right;
    background-color: transparent;
}

.panel-footer {
    padding: 10px 15px;
    /* background-color: #f5f5f5; */
    /* border-top: 1px solid #ddd; */
    border-bottom-right-radius: 3px;
    border-bottom-left-radius: 3px;
}

/*.table>tbody>tr>td {
  text-align: left;
}*/
.table>tbody>tr>td {
    text-align: right;
}
</style>
<input type="hidden" name="survey_id" id="survey_id" value="{{$id}}">
<input type="hidden" name="user_id" id="user_id" value="{{$user_id}}">
 <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">

<h1>Survey</h1>

  <div class="w3layoutscontaineragileits">
<div id="surveyContainer" style="direction: rtl;"></div>

</div>
  <div class="w3footeragile">
    <p> &copy; 2017  All Rights Reserved | Design by Digital Marketing Team </p>
  </div>




    <!-- jQuery -->
    <script src="/offlineneew/public/bower_components/gentelella/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
<script type="text/javascript" src="/offlineneew/public/assets/js/bootstrap.min.js"></script>
  
<script src="https://surveyjs.azureedge.net/0.12.36/survey.jquery.min.js"></script>

<script type="text/javascript">
    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
</script>
  <script type="text/javascript">

     $(document).ready(function() {




  var survey_id=$('#survey_id').val();
var user_id=$('#user_id').val();
  Survey.Survey.cssType = "bootstrap";
  Survey.defaultBootstrapCss.navigationButton = "btn btn-green";

var survey_Data;

   $.ajax({
        type: "Get",
        url: '/offlineneew/public/getsurveydata',
        data: {survey_id:survey_id},
        success: function(data) {

 survey_Data=data[0];
console.log(survey_Data);

window.survey = new Survey.Model(survey_Data);


// survey.onComplete.add(function(result) {




// });

function sendDataToServer(survey) {
    console.log(JSON.stringify(survey.data));
var token=$('meta[name="csrf-token"]').attr('content');
    // var sData={};
    // var sData=JSON.stringify(survey.data);
   
  $.ajax({ 
        
        type: "POST",
        url: '/offlineneew/public/Savesurveydata',
        data: {
          _token:token,
          survey_id:survey_id,
          survey_Data:JSON.stringify(survey.data),
          user_id:user_id},
          dataType: "JSON",

        success: function(data) {



   // document.querySelector('#surveyContainer').innerHTML = "result: " +JSON.stringify(result.data);

        }});
   // alert("The results are:" +JSON.stringify(survey.data));
}


$("#surveyContainer").Survey({
    model: survey,
     onComplete: sendDataToServer

});
        }});
});
  </script>




</body>

</html>



