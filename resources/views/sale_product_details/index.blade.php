@extends('masternew')
@section('content')
<?php 
  if(!empty($_COOKIE['sale_product_detailsErr'])) {     
    echo "<div><div class='alert alert-block alert-danger fade in center'>";
    echo $_COOKIE['sale_product_detailsErr'];
    echo "</div> </div>";
  } 

?>
<style type="text/css">
/*    input[type="file"] {
    display: block;
   }
*/

.error{
  color: #a51c1c;
}

.chosen-choices {
    border: 1px solid #ccc;
    border-radius: 4px;
    min-height: 34px;
    padding: 6px 12px;
}
.chosenContainer .form-control-feedback {
    /* Adjust feedback icon position */
    right: -15px;
}
.chosenContainer .form-control {
    height: inherit; 
    padding: 0px;
}

</style>
<section class="panel-body">
 <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><i class="fa fa-lightbulb-o" aria-hidden="true"></i> Multi Product Details  </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                     
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
<div class="panel-body">
  <a type="button" class="btn btn-primary" data-toggle="modal" data-target="#activityModal" style="margin-bottom: 20px;"> Add New Multi Product Details </a>










<!-- model -->
<div class="modal fade" id="activityModal" tabindex="-1" role="dialog" aria-labelledby="activityModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
         <h4 class="modal-title span7 text-center" id="activityModalLabel">
         <span class="title">Add Multi Product Details</span></h4>

    </div>
    <div class="modal-body">

 <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                 
                  <div class="x_content">
                    <br />
  {!! Form::open(['action' => "sale_product_detailsController@store",'method'=>'post','id'=>'profileForm','files'=>'true','class'=>'form-horizontal form-label-left ']) !!}
       <!--  <input type="hidden" name="_token" value="{{ csrf_token() }}"> -->

   

   <fieldset style=" padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;">
   <legend style="width: fit-content;border: 0px;">Main Data:</legend>

        
           

      <div class="form-group">


<div class="control-label col-md-6 col-sm-6 col-xs-12">

                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Sales Product
                        </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                             {!!Form::select('sale_product',([null => 'please chooose'] + $sale_product->toArray()) ,null,['class'=>'form-control subuser_list  chosen-select sale_product','id' => 'prettify','required'=>'required'])!!}
                        </div>
                        </div>

                        <div class="control-label col-md-6 col-sm-6 col-xs-12">

                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Government
                        </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                             {!!Form::select('government',([null => 'please chooose'] + $governments->toArray()) ,null,['class'=>'form-control subuser_list  chosen-select government','id' => 'prettify','required'=>'required'])!!}
                        </div>
                        </div>

                   
                   

</div>
                    

      
                    

                     </fieldset>


   <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                          <button type="button" class="btn btn-primary " data-dismiss="modal">Cancel</button>
                         <button class="btn btn-primary" type="reset">Reset</button>
                          <button type="submit"  name="login_form_submit" class="btn btn-success" value="Save">Submit</button>
                        </div>
                      </div>

      {!!Form::close() !!}
    </div>
    </div>
     </div>
    </div>
    </div>
  </div>
  </div>
  </div>

<table class="table table-hover table-bordered  dt-responsive nowrap display Brands" cellspacing="0" width="100%">
  <thead>
              <th>No</th>
              <th>Multi Product Name</th>   
              <th>Government </th>     
              <th>Available </th>
              <th>Process</th>          
  </thead>
   <tfoot>
        
                 <th></th>
       <th>Multi Product Name</th>    
              <th>Government </th>     
              <th>Available </th>
              <th>Process</th>  
                    
                  </tfoot>
               

<tbody style="text-align:center;">
  <?php $i=1;?>
  @foreach($sale_productdetials as $values)
    <tr>
    
     <td>{{$i++}}</td> 
      <td>
 <a class="testEdit" data-type="select"  data-value="{{$Sale_products}}" data-source ="{{$Sale_products}}" data-name="SaleProduct_id"  data-pk="{{$values->id}}"> 
 {{$values->sale_product__detailsbelongtosale_product->Name}}
  
  </a>
 </td>

<td>
   <a class="testEdit" data-type="select"  data-value="{{$government}}" data-source ="{{$government}}" data-name="Government"  data-pk="{{$values->id}}"> 
 {{$values->Government}}
  
  </a>
  
 </td>
     @if($values->Available==1)
                <td><input id="check" type="checkbox" class="check" data-name="Available"  data-pk="{{ $values->id }}" onclick="ActiveCheck(this,{{ $values->id }})" value="{{$values->Available}}"  checked/></td>
              @else
                <td><input id="check" type="checkbox" class="check" data-name="Available"  data-pk="{{ $values->id }}" onclick="ActiveCheck(this,{{ $values->id }})" value="{{$values->Available}}"  /></td>
              @endif 
  <td>
     <a href="/offlineneew/public/sale_product_details/destroy/{{$values->id}}"class="btn btn-default btn-sm" ><span class="glyphicon glyphicon-trash"></span>
       </a>

</td>

   </tr>
     @endforeach
</tbody>
</table> 
</div>
 </div>
 </div>   
    </section>
  
  @section('other_scripts')


<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>


  <script type="text/javascript">

var editor;

  $(document).ready(function(){

     $('#profileForm')
.find('[name="government"]')
          
            .chosen({
                width: '100%',
                inherit_select_classes: true
            })
            
            .change(function(e) {
                $('#profileForm').formValidation('revalidateField', 'government');
            })
            .end()
        .formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },

            fields: {
                'government': {
                    validators: {
                  notEmpty:{
                    message:"the Government is required"
                  }
            }
             }
           }
            ,submitHandler: function(form) {
        form.submit();
    }
        })
        .on('success.form.fv', function(e) {
   // e.preventDefault();
    var $form = $(e.target);

    // Enable the submit button
   
    $form.formValidation('disableSubmitButtons', false);
});
     var table= $('.Brands').DataTable({
  
    select:true,
    responsive: true,
    "order":[[0,"asc"]],
    'searchable':true,
    "scrollCollapse":true,
    "paging":true,
    "pagingType": "simple",
      dom: 'lBfrtip',
        buttons: [
           
            
            { extend: 'excel', className: 'btn btn-primary dtb' }
            
            
        ],

fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
$.fn.editable.defaults.send = "always";

$.fn.editable.defaults.mode = 'inline';

$('.testEdit').editable({


    placement: 'right',
    url:'{{URL::to("/")}}/updatesaleproductdetails',
  
     ajaxOptions: {
     type: 'get',
    sourceCache: 'false',
     dataType: 'json'

   },

       params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');

            // console.log(params);
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'This Data Already Exist,Enter Correct Data.';
            } else {
                return response.responseText;
            }
        }
        ,    success:function(response)
        {
            if(response.status==="You do not have permission.")
             {
              return 'You do not have permission.';
              }
        }


});

}
});
   $('.Brands tfoot th').each(function () {
      var title = $('.Brands thead th').eq($(this).index()).text();       
     if($(this).index()>=1 && $(this).index()<=10)
            {

           $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
}
        

    });
  table.columns().every( function () {
  var that = this;
 $(this.footer()).find('input').on('keyup change', function () {
          that.search(this.value).draw();
            if (that.search(this.value) ) {
               that.search(this.value).draw();
           }
        });
      
    });

    $(".sale_product").chosen({ 
                   width: '100%',
                   no_results_text: "There is no result",
                   allow_single_deselect: true, 
                   search_contains:true, });
                        $(".sale_product").trigger("chosen:updated");



      $(".government").chosen({ 
                   width: '100%',
                   no_results_text: "There is no result",
                   allow_single_deselect: true, 
                   search_contains:true, });
                          $(".government").trigger("chosen:updated");

 });
function ActiveCheck(e,id)
{
  var act;
  if(e.value==1)
    act=0;
  else
    act=1;

  $.ajax({
      url: "/offlineneew/public/updateactivesaleproduct?active="+act+"&id="+id,
      type: "get",
      success: function(data){
      }
    }); 


}
</script>











@stop

@stop

