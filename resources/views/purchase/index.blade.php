@extends('masternew')
@section('content')

  <style type="text/css">
  #blah_Billout{
  background: url(assets/img/noimage.jpg) no-repeat;
  background-size: cover;
  height: 60px;
  width: 60px;
}
</style>
 <section class="panel-body">
  <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                     <h2 style="margin-left:85%">ايصال استلام <i class="fa fa-lightbulb-o"></i></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                     
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
  <script src="/assets/js/bootstrap-datepicker.ar.js" ></script>
<a type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal1" style="margin-bottom: 20px; margin-left:85% " > اضافة ايصال استلام</a>



                  

<!-- model -->
 
<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
     <div class="modal-content">
    
          <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                 <h4 class="modal-title span7 text-center" id="myModalLabel"><span class="title">اضافة فاتورة جديدة</span></h4>
          </div>
    <div class="modal-body">
        <div class="tabbable"> <!-- Only required for left/right tabs -->
        <ul class="nav nav-tabs">
  

        
        </ul>
        <div class="tab-content">
        <div class="tab-pane active" id="tab1">
              <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                 
                  <div class="x_content">
                    <br />
                   {!! Form::open(['action' => "PurchaseController@store",'method'=>'post','id'=>'profileForm', 'files' => 'true']) !!}  
             
             <fieldset style="    padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;" dir="rtl">
  <legend style="width: fit-content;border: 0px;" dir="rtl">بيانات اساسية</legend>
 

          <div class="form-group col-md-12">
                
                <div class="col-md-9" dir="rtl">
                    <input type="text" class="form-control inventorytexbox" id="PNumer" name="PNumer" placeholder="ادخل رقم الايصال" >
                </div>
                <label for="input_product" class="control-label col-md-3"> رقم الايصال</label>
            </div>
                <!-- <div class="form-group col-md-12">
                <div class="col-md-9  input-group" dir="rtl">

                    <input type="text" class="form-control" id="Pdate" name="Pdate" placeholder="ادخل تاريخ الايصال ">
                </div>
                <label for="input_Enname" class="control-label col-md-3"> تاريخ الايصال</label>
            </div> -->
       
<div class="form-group col-md-12" dir="rtl">
        <label class=" control-label">تاريخ الايصال</label>


<div class="col-md-9 col-sm-9 col-xs-12" dir="rtl">
                        <span class="fa fa-calendar-o form-control-feedback right  " aria-hidden="true"></span>

                          <input type="text" class="form-control has-feedback-right inventorytexbox" id="single_cal4" name="Date" placeholder="Date" required>
                        </div>

        <!-- <div class="dateContainer col-md-9" >
            <div class="input-group input-append date" id="startDatePicker">
              <span class="input-group-addon add-on">
                 <span class="glyphicon glyphicon-calendar"></span>
                </span>
             
              <input type="text" class="form-control has-feedback-left" id="startDatePicker" name="Pdate" placeholder="Date">
            </div>
        </div> -->
    </div>
                <!-- 
                    <div class="form-group col-md-12">
                <label for="input_Enname" class="control-label  col-md-3"> المستخدم</label>
                <div class="col-md-9">
                  {!!Form::select('user', ([null => 'Select User Eame'] + $user->toArray()) ,null,['class'=>'form-control','id' => 'user','name'=>'user'])!!}
                </div>
            </div>  -->
                <div class="form-group col-md-12">
                
                <div class="col-md-9 inventorytexbox" dir="rtl">
                  {!!Form::select('warehouse', $warehouse ,null,
                 ['class'=>'form-control subuser_list',
                 'id' => 'warehouseaction','name'=>'warehouse','required'=>'required'])!!} 
                </div>
                <label for="input_Enname" class="control-label  col-md-3"> المخزن</label>
            </div>
                 <div class="form-group col-md-12">
                 <div class="col-md-9" dir="rtl">
 <div id="blah_Billout"></div>
            </div>
                        <label for="BillOut-upload" class="custom-file-upload" >
    <i class="fa fa-cloud-upload"></i> Image Upload
</label>
<input id="BillOut-upload"  name="pic" type="file"/>
               
             </div>
             <div class="form-group col-md-12">
                
                <div class="col-md-9" dir="rtl">
               <input type="text" class="form-control inventorytexbox" id="Note" name="Note" placeholder="ادخل ملاحظات">
                </div>
                <label for="input_Enname" class="control-label  col-md-3"> ملاحظات </label>
            </div>
 </fieldset>
             <fieldset style="    padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;" dir="rtl">
  <legend style="width: fit-content;border: 0px;">الاصناف</legend>
                     <div class="form-group col-xs-12">
                            <div class="col-xs-2" dir="rtl">
                            <button type="button" class="btn btn-default addButton"><i class="fa fa-plus"></i></button>
                            </div>
                           
                            <div class="col-xs-4" dir="rtl">
                                <input class="form-control inventorytexbox" type="number" name="quantity[]" placeholder="ادخل الكمية"  required />
                            </div>
                            <div class="col-xs-4 inventorytexbox" dir="rtl">
                                 {!!Form::select('item[]',([null => 'اختر الصنف'] + $item->toArray()) ,null,['class'=>'form-control subuser_list','id' => 'prettify','required'=>'required'])!!}
                            </div>
                            <label class="col-xs-2 control-label">الاصناف</label>
                        </div>

                        <!-- The template containing an email field and a Remove button -->
                        <div class="form-group hide col-xs-12" id="emailTemplate">
                           <div class="col-xs-2" dir="rtl">
                                <button type="button" class="btn btn-default removeButton"><i class="fa fa-minus"></i></button>
                            </div>
                           
                            <div class="col-xs-4" dir="rtl">
                                   <input class="form-control inventorytexbox" type="number" name="quantity[]" id="quantity[]" placeholder="ادخل الكمية" />
                            </div>
                              <div class="col-xs-4 inventorytexbox" dir="rtl">
                                  {!!Form::select('item[]',([null => 'اختر الصنف'] + $item->toArray()) ,null,['class'=>'form-control subuser_list','id' => 'prettify'])!!}
                                  
                            </div>
    
</fieldset>
        
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12 " style="text-align: center;">
                          <button type="button" class="btn btn-danger " data-dismiss="modal">الغاء</button>
                         <button class="btn btn-info" type="reset">اعادة ادخال</button>
     <button type="submit"  name="login_form_submit" class="btn btn-success" value="save">حفظ</button>
                        </div>
                      </div>

      {!!Form::close() !!}                
        </div>
                </div>
                   </div>
                </div>
        </div>
        </div>
        </div>

    </div>
 

         </div>
     </div>
  </div>
<!-- ENDModal -->


                    <table class="table table-hover table-bordered  dt-responsive nowrap  purchase" cellspacing="0" width="100%" dir="rtl" dir="rtl">
  <thead>
   
        <th></th>
              <th>رقم الايصال</th>
              <th>تاريخ الايصال</th>  
              <!-- <th>نوع الايصال</th> -->
             <!--  <th>صوره الايصال</th> 
                 -->
              <th>المستخدم</th> 
              <th>ملاحظات</th>  
              <th> الاصناف/الكمية</th> 
               <th>تعديل الصوره</th>   
              <th>تفاصيل الايصال</th>        
  </thead>
  <tfoot>
     <th></th>
              <th>رقم الايصال</th>
              <th>تاريخ الايصال</th>  
              <!-- <th>نوع الايصال</th> -->
             <!--  <th>صوره الايصال</th> 
                 -->
              <th>المستخدم</th> 
              <th>ملاحظات</th>  
              <th> الاصناف/الكمية</th> 
               <th></th>   
              <th></th> 
</tfoot>


<tbody style="text-align:center;">
<?php $i=1;?>
  @foreach($purchases as $purchase)
    <tr>
    
     <td>{{$i++}}</td>
        
        
     
         <td ><a  class="testEdit" data-type="number" data-name="PurchesNo"  data-pk="{{ $purchase->id }}">{{$purchase->PurchesNo}}</a></td>

         <td ><a  class="testEdit" data-type="text" data-name="Date"  data-pk="{{ $purchase->id }}">{{$purchase->Date}}</a></td>
                  <!-- <td >{{$purchase->Type}}</td> -->
 <!--                     @if($purchase->ActivityitemsID!==null)
  
         <td ><a  class="testEdit" data-type="select" data-name="ActivityitemsID" data-source="{{$activityitems}}" data-pk="{{ $purchase->id }}">{{$purchase->purchesactivity->Code}}</a></td>
  @endif
                       @if($purchase->ActivityitemsID==null)

           <td ><a  class="testEdit" data-type="select" data-name="ActivityitemsID" data-source="{{$activityitems}}" data-pk="{{ $purchase->id }}"></a></td>
  @endif -->

<!--   <td>
          @if($purchase->RelatedDecomentLink!==null)
          <img src="/purchase/{{$purchase->id}}" style="width:60px;hight:60px;">
          @endif
        </td> -->
        <!--  <td >عمرو عبد الحفيظ</td> -->
        <td >{{$purchase->getpurchesusers->Name}}</td>
     <td ><a  class="testEdit" data-type="text" data-name="Description"  data-pk="{{ $purchase->id }}">{{$purchase->Description}}</a></td>

        
  <td >
  <ul style="text-align: right;">
    @foreach($purchase->getpurchaseitem as $item_purchase)
        <li>{{$item_purchase->Name}}. <p style="padding-left:2px ">الكمية :{{$item_purchase->pivot->quantity}}</p></li>
   @endforeach
   </ul>
 </td>
  <td>
 <a href="/MIS/EditPhotoPurchesIN/{{$purchase->id}}" class="btn btn-default btn-sm" ><span class="glyphicon glyphicon-edit"></span>  </a>
</td>
       <td>
       <a href="/MIS/showallDetails/{{$purchase->id}}"class="btn btn-default btn-sm" ><span class="glyphicon glyphicon-plus"></span>  </a>
  </td>
   </tr>
     @endforeach
</tbody>
</table>


                  </div>
                </div>
              </div>
 
  @section('other_scripts')


<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>



  <script type="text/javascript">

var editor;

  $(document).ready(function(){


    $("#BillOut-upload").on("change", function()
    {
    
        var files = !!this.files ? this.files : [];

        if (!files.length || !window.FileReader) 
        {
           $("#blah_Billout").css("background-image",'url(assets/img/noimage.jpg)');
        } // no file selected, or no FileReader support
 
        else if (/^image/.test( files[0].type)){ // only image file
            var reader = new FileReader(); // instance of the FileReader
            reader.readAsDataURL(files[0]); // read the local file
 
            reader.onloadend = function(){ // set image data as background of div
                $("#blah_Billout").css("background-image", "url("+this.result+")");
            }
        }
    });
/////////////////////

$('#startDatePicker').datepicker({
     language: 'ar',
     format: "yyyy-mm-dd"
}).on('changeDate', function(e) {
            // Revalidate the start date field
            $('#profileForm').formValidation('revalidateField', 'Pdate');
        });;

console.log($('#startDatePicker').val());
  $('#profileForm')
        .formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
               'PNumer':{
                validators: {
                       notEmpty: {
                            message: 'يجب ادخال التاريخ'
                    },
                         integer: {
                            message: 'هذا الحقل ارقام صحيحة فقط '
                    },

                     remote: {
                        url: '/MIS/checkpurscahsenumber',
                        data:function(validator, $field, value)
                        {return{PNumer:validator.getFieldElements('PNumer').val()};},
                        message: 'رقم الفاتورة مدخل من قبل ',
                        type: 'GET'
                    }
                }
            },
                   'Pdate':{
                validators: {
                         notEmpty: {
                            message: 'يجب ادخال التاريخ'
                    }
                }
            }
            
            ,
              'warehouse':{
                validators: {
                         notEmpty: {
                            message: 'يجب ادخال المخزن'
                    }
                }
            }
            ,

                'item[]': {
                  
                    validators: {
                        callback: {
                            callback: function(value, validator, $field) {
                                var $emails          = validator.getFieldElements('item[]'),
                                    numEmails        = $emails.length,
                                    notEmptyCount    = 0,
                                    obj              = {},
                                    duplicateRemoved = [];

                                for (var i = 0; i < numEmails; i++) {
                                    var v = $emails.eq(i).val();
                                    if (v !== '') {
                                        obj[v] = 0;
                                        notEmptyCount++;
                                    }
                                }

                                for (i in obj) {
                                    duplicateRemoved.push(obj[i]);
                                }

                                if (duplicateRemoved.length === 0) {
                                    return {
                                        valid: false,
                                        message: 'يجب ادخل كمية واحده علي الاقل '
                                    };
                                } else if (duplicateRemoved.length !== notEmptyCount) {
                                    return {
                                        valid: false,
                                        message: 'هذا الصنف مدخل من قبل '
                                    };
                                }

                                validator.updateStatus('item[]', validator.STATUS_VALID, 'callback');
                                return true;
                            }
                        }
                    }
                }
                ,'quantity[]':
                 {
                   
                    validators: {           
                      
                   notEmpty:{
                    message:"يجب ادخال الكمية"
                  }
                    }
                    } ,
                    'pic' :
                    {
                   validators: {           
                      
                   notEmpty:{
                    message:"يجب ادخال الصورة "
                  }
                    }
                    }

            } ,submitHandler: function(form) {
        form.submit();
    }
        })
        .on('success.form.fv', function(e) {
   // e.preventDefault();
    var $form = $(e.target);

    // Enable the submit button
   
    $form.formValidation('disableSubmitButtons', false);
})
        // Add button click handler
        .on('click', '.addButton', function() {
            var $template = $('#emailTemplate'),
                $clone    = $template
                                .clone()
                                .removeClass('hide')
                                .removeAttr('id')
                                .insertBefore($template),
                $email    = $clone.find('[name="item[]"]'),
                $Engname    = $clone.find('[name="quantity[]"]');

            // Add new field
            $('#profileForm').formValidation('addField', $email).formValidation('addField', $Engname);
        })

        // Remove button click handler
        .on('click', '.removeButton', function() {
            var $row   = $(this).closest('.form-group'),
                $email = $row.find('[name="item[]"]'),
                $Engname = $row.find('[name="quantity[]"]');
            // Remove element containing the email
            $row.remove();

            // Remove field
            $('#profileForm').formValidation('removeField', $email).formValidation('removeField', $Engname);
        });
   


     var table= $('.purchase').DataTable({
  
    select:true,
    responsive: true,
    "order":[[0,"asc"]],
    'searchable':true,
    "scrollCollapse":true,
    "paging":true,
    "pagingType": "simple",
      dom: 'lBfrtip',
        buttons: [
           
            
            { extend: 'excel', className: 'btn btn-primary dtb' }
            
            
        ],

fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
$.fn.editable.defaults.send = "always";

$.fn.editable.defaults.mode = 'inline';

$('.testEdit').editable({

      validate: function(value) {
        name=$(this).editable().data('name');
        if(name=="PurchesNo"||name=="Date")
        {
                if($.trim(value) == '') {
                    return 'Value is required.';
                  }
                    }
      
       
         if(name=="Date")
        {

      var regexp = /^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/;            
      if (!regexp.test(value)) {
            return 'This field is not valid';
        }
        
        }
    },
       
    placement: 'right',
    url:'{{URL::to("/")}}/updatePurches',
  
     ajaxOptions: {
     type: 'get',
    sourceCache: 'false',
     dataType: 'json'

   },

       params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');

            // console.log(params);
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'This Data Already Exist,Enter Correct Data.';
            } else {
                return response.responseText;
            }
        }
        ,
        success:function(response)
        {
            if(response.status==="You do not have permission.")
             {
              return 'You do not have permission.';
              }
        }


});

}
});

 



   $('.purchase tfoot th').each(function () {



      var title = $('.purchase thead th').eq($(this).index()).text();
               
 if($(this).index()>=1 && $(this).index()<=5)
        {

           $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
}
        

    });
  table.columns().every( function () {
  var that = this;
 $(this.footer()).find('input').on('keyup change', function () {
          that.search(this.value).draw();
            if (that.search(this.value) ) {
               that.search(this.value).draw();
           }
        });
      
    });


   
 });


  </script>
@stop
@stop
