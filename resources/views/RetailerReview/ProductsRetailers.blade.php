@extends('masternew')
@section('content')
  <section class="panel-body">
    <style type="text/css">
      input[type="file"] {
        display: none;
      }
      .custom-file-upload {
          border: 1px solid #ccc;
          display: inline-block;
          padding: 6px 12px;
          cursor: pointer;
      }
    </style>
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2 ><i class="fa fa-tasks" ></i>Products Retailers</h2>
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
            <li><a class="close-link"><i class="fa fa-close"></i></a></li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <a type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal1" style="margin-bottom: 20px;"> Add New Product</a>
        <!-- model -->
        <div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title span7 text-center" id="myModalLabel"><span class="title">Add New Product</span></h4>
              </div>
              <div class="modal-body">
                <div class="tabbable"> <!-- Only required for left/right tabs -->
                  <ul class="nav nav-tabs"></ul>
                  <div class="tab-content">
                    <div class="tab-pane active" id="tab1">
                      <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <div class="x_panel">
                            <div class="x_content">
                              <br />
                              {!! Form::open(['action' => "RetailerReviewController@CreateProductRetailers",'method'=>'post','id'=>'profileForm']) !!} 
                                <fieldset style="padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;">
                                  <legend style="width: fit-content;border: 0px;">Main Date:</legend>
                                  <input type="hidden" name="Retailer_Id" value="{{$Retailer_Id}}">
                                    <div class="form-group">
                                      <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                                        {!!Form::select('Product',([null => 'please chooose Product'] + $Cement_Name->toArray()) ,null,['class'=>'form-control subuser_list','required'=>'required'])!!}
                                      </div>
                                      <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                                        <input type="number" class="form-control has-feedback-left"  id="Quentity" name="Quentity" placeholder="Product Quentity" required>
                                        <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                                      </div>
                                    </div>                    
                                </fieldset>
                                <div class="ln_solid"></div>
                                <div class="form-group">
                                  <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                                    <button type="button" class="btn btn-primary " data-dismiss="modal">Cancel</button>
                                    <button class="btn btn-primary" type="reset">Reset</button>
                                    <button type="submit"  name="login_form_submit" class="btn btn-success" value="Save" disabled="disabled">Submit</button>
                                  </div>
                                </div>
                              {!!Form::close() !!}                
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- ENDModal --> 
        <table class="table table-hover table-bordered  dt-responsive nowrap Brands" cellspacing="0" width="100%" >
          <thead>
            <th>No</th>
            <th>Name</th>
            <th>Quantity</th>
            <th>Process</th>
          </thead>
          <tfoot>
            <th></th>
            <th>Name</th>
            <th>Quantity</th>
            <th></th>
          </tfoot>
          <tbody style="text-align:center;">
            <?php $i=1;?>
            @foreach($products as $Value)
              <tr>  
                <td>{{$i++}}</td> 
                <td ><a  class="testEdit" data-type="select" data-name="Product_Id" data-source="{{$Cement_Name}}"  data-pk="{{$Value->id}}">{{$Value->Cement_Name}}</a></td>
                <td ><a  class="testEdit" data-type="number" data-name="Quantity"  data-pk="{{$Value->id}}">{{$Value->Quantity}}</a></td>
                <td><a href="/offlineneew/public/DeletePro/{{$Value->id}}" class="btn btn-default btn-sm" ><span class="glyphicon glyphicon-trash"></span></a></td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</section>
@section('other_scripts')
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
<script type="text/javascript">
var editor;

  $(document).ready(function(){      
     $('#profileForm')
        .formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                          
                    'Product_Id':{
                    validators: {
            
                  notEmpty:{
                    message:"the Name is required"
                  } ,
                     regexp: {
                        regexp: /^[\u0600-\u06FFA-za-z\s]+$/,
                        message: 'The Name can only consist of Arabic alphabetical and spaces'
                     }
                    }
                  },
                      
               'Quantity':{
                    validators: {
            
                  notEmpty:{
                    message:"the Age is required"
                  } 
                    }
                },  
              
                
            } ,submitHandler: function(form) {
        form.submit();
        }
        })
        .on('success.form.fv', function(e) {
   // e.preventDefault();
    var $form = $(e.target);

    // Enable the submit button
   
    $form.formValidation('disableSubmitButtons', false);
});
     var table= $('.Brands').DataTable({
  
    select:true,
    responsive: true,
    "order":[[0,"asc"]],
    'searchable':true,
    "scrollCollapse":true,
    "paging":true,
    "pagingType": "simple",
      dom: 'lBfrtip',
        buttons: [
           
            
            { extend: 'excel', className: 'btn btn-primary dtb' }
            
            
        ],

fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
$.fn.editable.defaults.send = "always";

$.fn.editable.defaults.mode = 'inline';

$('.testEdit').editable({

      validate: function(value) {
        name=$(this).editable().data('name');
        if(name=="Product_Id")
        {
                if($.trim(value) == '') {
                    return 'Value is required.';
                  }
                    }
                     if(name=="Quantity")
        {
                if($.trim(value) == '') {
                    return 'Value is required.';
                  }
                    }
        },

    placement: 'right',
    url:'{{URL::to("/")}}/updateProductRetailers',
  
     ajaxOptions: {
     type: 'get',
    sourceCache: 'false',
     dataType: 'json'

   },

       params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');

            // console.log(params);
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'This Data Already Exist,Enter Correct Data.';
            } else {
                return response.responseText;
            }
        }

,
        success:function(response)
        {
            if(response.status==="You do not have permission.")
             {
              return 'You do not have permission.';
              }
        }

});

}
});
   $('.Brands tfoot th').each(function () {
      var title = $('.Brands thead th').eq($(this).index()).text();       
     if($(this).index()>=1 && $(this).index()<=2)
            {

           $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
}
        

    });
  table.columns().every( function () {
  var that = this;
 $(this.footer()).find('input').on('keyup change', function () {
          that.search(this.value).draw();
            if (that.search(this.value) ) {
               that.search(this.value).draw();
           }
        });
      
    });

 });
  </script>
@stop

@stop
