@extends('masternew')
@section('content')
 <section class="panel-body">
<style type="text/css">
  input[type="file"] {
    display: none;
}
.custom-file-upload {
    border: 1px solid #ccc;
    display: inline-block;
    padding: 6px 12px;
    cursor: pointer;
}

</style>

 <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                     <h2 ><i class="fa fa-tasks" ></i>Retailers Review </h2>
                        <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                     
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

  

                    <table class="table table-hover table-bordered  dt-responsive nowrap Brands" cellspacing="0" width="100%" >
  <thead>
          <th>No</th>
          <th>Retailer Code</th>
          <th>Has_Truncks</th>
          <th>Workers</th>
          <th>No_Truncks</th>
          <th>Has_Used_Computer</th>
          <th>Equipment</th>
          <th> Status </th>
          <th>Call_Status</th>
          <th>Area</th>
          <th>Cont_Type</th>
          
          <th></th>  
          <th></th>
          <th></th>
          <th></th>
        </thead>
        <tfoot>
          <th></th>
          <th>Retailer Code</th>
          <th>Has_Truncks</th>
          <th>Workers</th>
          <th>No_Truncks</th>
          <th>Has_Used_Computer</th>
          <th>Equipment</th>
          <th> Status </th>
          <th>Call_Status</th>
          <th>Area</th>
          <th>Cont_Type</th>
          
          <th></th>  
          <th></th>
          <th></th>
          <th></th>
</tfoot>


<tbody style="text-align:center;">
<?php $i=1;?>
 @foreach($RetailersReview as $Value)
    <tr>
    
     <td>{{$i++}}</td> 
         <td > 
  {{$Value->getRetailersData->Code}}
 </td>

   <td >{{$Value->Has_Truncks}}</td>

      <td >{{$Value->Workers}}</td>


      <td >{{$Value->No_Truncks}}</td>

       <td >{{$Value->Has_Used_Computer}}</td>


      <td>{{$Value->Equipment}}</td>

       <td >{{$Value->Status}}</td>

        <td >{{$Value->Call_Status}}</td>
        <td>{{ $Value->Area}}</td>

      <td >{{$Value->Cont_Type}}</td>
  
 

  <td>
     <a href="/offlineneew/public/ReviewRet/ShowKids/{{$Value->Retailer_Id}}" class="btn btn-default btn-sm" ><span class="glyphicon glyphicon-plus">Kids </span>
       </a>
</td>
 <td>
     <a href="/offlineneew/public/ReviewRet/ShowProduct/{{$Value->Retailer_Id}}" class="btn btn-default btn-sm" ><span class="glyphicon glyphicon-plus">Products </span>
       </a>
</td>

 <td>
     <a href="/offlineneew/public/ReviewRet/edit/{{$Value->id}}" class="btn btn-default btn-sm" ><span class="glyphicon glyphicon-edit"></span>
       </a>
</td>


 
<!-- Promoter -->
  <td>
     <a href="/offlineneew/public/ReviewRet/destroy/{{$Value->id}}" class="btn btn-default btn-sm" ><span class="glyphicon glyphicon-trash"></span>
       </a>
</td>

   </tr>
     @endforeach
</tbody>
</table>


                  </div>
 {!!Form::open(['action'=>'RetailerReviewController@ImportReviewRet','method' => 'post','files'=>true])!!}
    <input type="file" name="file" class="btn btn-primary"/>
    <input type="submit" name="submit" value="Import File" class="btn btn-primary" style="margin-bottom: 20px;"/>  
{!!Form::close()!!}
                </div>
              </div>
 
  @section('other_scripts')


<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
  <script type="text/javascript">

var editor;

  $(document).ready(function(){
     $('#Government').change(function(){
        $.get("{{ url('api/dropdown')}}", 
        { option: $(this).val() }, 
        function(data) {
            $('#district').empty(); 
            $.each(data, function(key, element) {
                $('#district').append("<option value='" + key +"'>" + element + "</option>");
            });
        });
    });
           $('#district').change(function(){
        $.get("{{ url('api/Contractor_Id')}}", 
        { option: $(this).val() }, 
        function(data) {
         
            $('#Contractor_Id').empty(); 
            $.each(data, function(key, element) {
                $('#Contractor_Id').append("<option value='" + key +"'>" + element + "</option>");
            });
            $('#Contractor_Id').addClass("subuser_list");
             $(".subuser_list").chosen({ 
                   width: '100%',
                   no_results_text: "No Results",
                   allow_single_deselect: true, 
                   search_contains:true, });
        });
    });
           $('#Has_Mixers').change(function(){
            if($(this).val() == "No")
              $("#No_Of_Mixers").attr("disabled","disabled");
            
            else
              $("#No_Of_Mixers").removeAttr('disabled');
    });
           
           $('#Has_Wood').change(function(){
            if($(this).val() == "No")
            
              $("#Wood_Meters").attr("disabled","disabled");
              else
              $("#Wood_Meters").removeAttr('disabled');
            
    });
            $('#Has_Sub_Contractor').change(function(){
            if($(this).val() == "No")
            {
              $("#Sub_Contractor1").attr("disabled","disabled");
              $("#Sub_Contractor2").attr("disabled","disabled");
            }
               else
               {
              $("#Sub_Contractor1").removeAttr('disabled');
               $("#Sub_Contractor2").removeAttr('disabled');
            }
    });
     $('#profileForm')
        .formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                       'Capital':{
                validators: {            
                     greaterThan: {
                        value: 0,
                        message: 'The value must be greater than or equal to 0'
                    },
                      numeric: {
                            message: 'The value must be number'
                        }
                }
            },
                       'Intership_No':{
                validators: {            
                    greaterThan: {
                        value: 0,
                        message: 'The value must be greater than or equal to 0'
                    },
                      numeric: {
                            message: 'The value must be number'
                        }
                }
            },
                       'No_Of_Mixers':{
                validators: {            
                    greaterThan: {
                        value: 0,
                        message: 'The value must be greater than or equal to 0'
                    },
                      numeric: {
                            message: 'The value must be number'
                        }
                }
            },
                    'Wood_Consumption':{
                validators: {            
                    greaterThan: {
                        value: 0,
                        message: 'The value must be greater than or equal to 0'
                    },
                      numeric: {
                            message: 'The value must be number'
                        }
                }
            },
                    'Wood_Meters':{
                validators: {            
                    greaterThan: {
                        value: 0,
                        message: 'The value must be greater than or equal to 0'
                    },
                      numeric: {
                            message: 'The value must be number'
                        }
                }
            },
                   'Steel_Consumption':{
                validators: {            
                    greaterThan: {
                        value: 0,
                        message: 'The value must be greater than or equal to 0'
                    },
                      numeric: {
                            message: 'The value must be number'
                        }
                }
            },
                    'Cement_Bricks':{
                validators: {            
                     greaterThan: {
                        value: 0,
                        message: 'The value must be greater than or equal to 0'
                    },
                      numeric: {
                            message: 'The value must be number'
                        }
                }
            },
                   'Cement_Consuption':{
                validators: {            
                    greaterThan: {
                        value: 0,
                        message: 'The value must be greater than or equal to 0'
                    },
                      numeric: {
                            message: 'The value must be number'
                        }
                }
            },
                           'Workers':{
                validators: {            
                    greaterThan: {
                        value: 0,
                        message: 'The value must be greater than or equal to 0'
                    },
                      numeric: {
                            message: 'The value must be number'
                        }
                }
            },
                          'Project_NO':{
                validators: {            
                     greaterThan: {
                        value: 0,
                        message: 'The value must be greater than or equal to 0'
                    },
                      numeric: {
                            message: 'The value must be number'
                        }
                }
            },
                     'Lat':{
                validators: {
                   
     
                      greaterThan: {
                        value: 0,
                        message: 'The value must be greater than or equal to 0'
                    },
                      numeric: {
                            message: 'The value must be number'
                        }
                }
            },
           'Long':{
                validators: {
        
                      greaterThan: {
                        value: 0,
                        message: 'The value must be greater than or equal to 0'
                    },
                      numeric: {
                            message: 'The value must be number'
                        }
                }
            },
              'Class': {
                validators: {
                    between: {
                        min: 0,
                        max: 9,
                        message: 'The Class must be between 0 and 9'
                    },
                      numeric: {
                            message: 'The value must be number'
                        }
                }
            },
                 'Capital': {
                  
                       validators: {
               
                    numeric: {
                        message: 'The Capital can only consist of numbers'
                    }
                    }

                    }
                    ,   'Sub_Contractor1': {
                  
                       validators: {
              
                    regexp: {
                        regexp: /^[a-zA-Z\u0600-\u06FF\s]+$/,
                        message: 'The Sub Contractor1 can only consist of alphabetical and spaces'
                    }
                    }

                    },
                       'Sub_Contractor2': {
                  
                       validators: {
               
                    regexp: {
                        regexp: /^[a-zA-Z\u0600-\u06FF\s]+$/,
                        message: 'The Sub Contractor2 can only consist of alphabetical and spaces'
                    }
                    }

                    },
                   
                       'Seller1': {
                  
                       validators: {
             
                    regexp: {
                        regexp: /^[a-zA-Z\u0600-\u06FF\s]+$/,
                        message: 'The Seller 1 Debit can only consist of alphabetical and spaces'
                    }
                    }

                    }
                    ,   'Seller2': {
                  
                       validators: {
              
                    regexp: {
                        regexp: /^[a-zA-Z\u0600-\u06FF\s]+$/,
                        message: 'The Seller 2 can only consist of alphabetical and spaces'
                    }
                    }

                    },
                       'Seller3': {
                  
                       validators: {
               
                    regexp: {
                        regexp: /^[a-zA-Z\u0600-\u06FF\s]+$/,
                        message: 'The Seller 3 can only consist of alphabetical and spaces'
                    }
                    }

                    },
                      'Seller4': {
                  
                       validators: {
               
                    regexp: {
                        regexp: /^[a-zA-Z\u0600-\u06FF\s]+$/,
                        message: 'The Seller 4 can only consist of alphabetical and spaces'
                    }
                    }

                    },
                      'Cont_Type': {
                  
                       validators: {
               
                    regexp: {
                        regexp: /^[a-zA-Z\u0600-\u06FF\s]+$/,
                        message: 'The Cont_Type can only consist of alphabetical and spaces'
                    }
                    }

                    },
                'item[]': {
                  
                    validators: {
                        callback: {
                            callback: function(value, validator, $field) {
                                var $emails          = validator.getFieldElements('item[]'),
                                    numEmails        = $emails.length,
                                    notEmptyCount    = 0,
                                    obj              = {},
                                    duplicateRemoved = [];

                                for (var i = 0; i < numEmails; i++) {
                                    var v = $emails.eq(i).val();
                                    if (v !== '') {
                                        obj[v] = 0;
                                        notEmptyCount++;
                                    }
                                }

                                for (i in obj) {
                                    duplicateRemoved.push(obj[i]);
                                }

                                if (duplicateRemoved.length === 0) {
                                    return {
                                        valid: false,
                                        message: 'You must fill at least one item'
                                    };
                                } else if (duplicateRemoved.length !== notEmptyCount) {
                                    return {
                                        valid: false,
                                        message: 'The item must be unique'
                                    };
                                }

                                validator.updateStatus('item[]', validator.STATUS_VALID, 'callback');
                                return true;
                            }
                        }
                    }
                }
                ,'quantity[]':
                 {
                    err: '#messageContainer1',
                    validators: {           
                      
                   notEmpty:{
                    message:"the quantity is required"
                  }, greaterThan: {
                        value: 0,
                        message: 'The value must be greater than or equal to 0'
                    },
                      numeric: {
                            message: 'The value must be number'
                        }
                    }
                    } ,
                    'Contractor_Id':{
                    validators: {
                            remote: {
                        url: '/cemexmarketring/public/checkcontractorexist',
                        message: 'The Contractor Already Exist',
                        type: 'GET'
                    },
                      notEmpty:{
                    message:"the Contractor_Id is required"
                  }
                   
                    }
                }
            } ,submitHandler: function(form) {
        form.submit();
        }
        })
        .on('success.form.fv', function(e) {
   // e.preventDefault();
    var $form = $(e.target);

    // Enable the submit button
   
    $form.formValidation('disableSubmitButtons', false);
}).on('click', '.addButton', function() {
            var $template = $('#emailTemplate'),
                $clone    = $template
                                .clone()
                                .removeClass('hide')
                                .removeAttr('id')
                                .insertBefore($template),
                $email    = $clone.find('[name="item[]"]'),
                $Engname    = $clone.find('[name="quantity[]"]');

            // Add new field
            $('#profileForm').formValidation('addField', $email).formValidation('addField', $Engname);
        })

        // Remove button click handler
        .on('click', '.removeButton', function() {
            var $row   = $(this).closest('.form-group'),
                $email = $row.find('[name="item[]"]'),
                $Engname = $row.find('[name="quantity[]"]');
            // Remove element containing the email
            $row.remove();

            // Remove field
            $('#profileForm').formValidation('removeField', $email).formValidation('removeField', $Engname);
        });
     var table= $('.Brands').DataTable({
  
    select:true,
    responsive: true,
    "order":[[0,"asc"]],
    'searchable':true,
    "scrollCollapse":true,
    "paging":true,
    "pagingType": "simple",
      dom: 'lBfrtip',
        buttons: [
           
            
            { extend: 'excel', className: 'btn btn-primary dtb' }
            
            
        ],

fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
$.fn.editable.defaults.send = "always";

$.fn.editable.defaults.mode = 'inline';

$('.testEdit').editable({

      validate: function(value) {
        name=$(this).editable().data('name');
        if(name=="Name")
        {
                if($.trim(value) == '') {
                    return 'Value is required.';
                  }
                    }
        },

    placement: 'right',
    url:'{{URL::to("/")}}/updateReview',
  
     ajaxOptions: {
     type: 'get',
    sourceCache: 'false',
     dataType: 'json'

   },

       params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');

            // console.log(params);
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'This Data Already Exist,Enter Correct Data.';
            } else {
                return response.responseText;
            }
        }

,
        success:function(response)
        {
            if(response.status==="You do not have permission.")
             {
              return 'You do not have permission.';
              }
        }

});

}
});
   $('.Brands tfoot th').each(function () {
      var title = $('.Brands thead th').eq($(this).index()).text();       
     if($(this).index()>=1 && $(this).index()<=6)
            {

           $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
}
        

    });
  table.columns().every( function () {
  var that = this;
 $(this.footer()).find('input').on('keyup change', function () {
          that.search(this.value).draw();
            if (that.search(this.value) ) {
               that.search(this.value).draw();
           }
        });
      
    });

 });
  </script>
@stop

@stop
