@extends('masternew')


@section('content')


<div class="row">

<div class="col-md-2 col-xs-12 ">
 </div>

              <div class="col-md-12 col-xs-12 ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Edit Users</h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    
{!!Form::open(['route' =>['ReviewRet.update',$RetailersReview->id],'method' => 'put','class'=>'form-horizontal form-label-left ','id'=>'profileForm'])!!}
                    
  <input type="hidden" name="_token" value="{{ csrf_token() }}">

       <input type="hidden" id="id" name="id" value="{{ $RetailersReview['id'] }}">
       <fieldset style="    padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;">
  <legend style="width: fit-content;border: 0px;">Personal Data:</legend>

                    <div class="form-group">
                   <label class="control-label col-md-2 col-sm-2 col-xs-12">Has Truncks</label>
                      <div class="col-md-4 col-sm-4 col-xs-12 has-feedback">
                          {!!Form::select('Has_Truncks',
                    array( 'Yes' => 'Yes',
                    'No' => 'No')
                   ,$RetailersReview->Has_Truncks,['id' => 'Has_Wood','class'=>'chosen-select TypeP form-control']);!!} 
                      </div>
                       <label class="control-label col-md-2 col-sm-2 col-xs-12">Workers</label> 
                        <div class="col-md-4 col-sm-4 col-xs-12">
                        <input type="text" class="form-control has-feedback-left" id="Name" name="Workers" value="{{ $RetailersReview['Workers'] }}"  placeholder="enter Workers" >
                      </div>
                      </div>

                     <div class="form-group">
                       <label class="control-label col-md-2 col-sm-2 col-xs-12">No Truncks</label> 
                        <div class="col-md-4 col-sm-4 col-xs-12">
                        <input type="text" class="form-control has-feedback-left"  id="Name" name="No_Truncks" value="{{ $RetailersReview['No_Truncks'] }}" placeholder="enter No_Truncks" >

                        <span class="fa fa-envelope form-control-feedback left" aria-hidden="true"></span>
                      </div>

                       <label class="control-label col-md-2 col-sm-2 col-xs-12">Has Computer</label>
                       <div class="col-md-4 col-sm-4 col-xs-12  has-feedback">
                      {!!Form::select('Has_Used_Computer',
                    array( 'Yes' => 'Yes',
                    'No' => 'No')
                   ,$RetailersReview->Has_Used_Computer,['id' => 'Has_Wood','class'=>'chosen-select TypeP form-control']);!!} 
                      </div>
                     
                       
                      </div>
                       <div class="form-group has-feedback">
                        <label class="control-label col-md-2 col-sm-2 col-xs-12">Equipment</label> 
                          <div class="col-md-4 col-sm-4 col-xs-12">
                        <input type="text" class="form-control has-feedback-left"  id="Equipment" name="Equipment" value="{{ $RetailersReview['Equipment'] }}" placeholder="enter Equipment" >

                        <span class="fa fa-envelope form-control-feedback left" aria-hidden="true"></span>
                      </div>

                            <label class="control-label col-md-2 col-sm-2 col-xs-12">Status</label>
                        <div class="col-md-4 col-sm-4 col-xs-12">

          {!!Form::select('Status',
  array( 'Reviewed' => 'Reviewed',
  'Not Reviewed' => 'Not Reviewed')
 ,$RetailersReview['Status'],['id' => 'Status','name' => 'Status','class'=>'chosen-select TypeP form-control']);!!}  
                                

                               

                        </div>

                      
                       </div>
 <div class="form-group">
                      
                         <label class="control-label col-md-2 col-sm-2 col-xs-12">Call Status</label>
                         <div class="col-md-4 col-sm-4 col-xs-12  has-feedback">
                               {!!Form::select('Call_Status',
  array( 'Updated' => 'Updated',
  'No Answer' => 'No Answer',
  'wrong Number' => 'wrong Number','Out Service' => 'Out Service')
 ,$RetailersReview['Call_Status'],['id' => 'Call_Status','name' => 'Call_Status','class'=>'chosen-select TypeP form-control']);!!}    
                      </div>
                      <label class="control-label col-md-2 col-sm-2 col-xs-12">Area</label> 
                        <div class="col-md-4 col-sm-4 col-xs-12">
 <input type="text" class="form-control has-feedback-left"  id="Area" name="Area" value="{{ $RetailersReview['Area'] }}" placeholder="enter Area" >
                                 

                   
                      </div>
                      </div>

<div class="form-group">
            
                         <label class="control-label col-md-2 col-sm-2 col-xs-12">Cont Type</label> 
                        <div class="col-md-4 col-sm-4 col-xs-12">

 <input type="number" class="form-control has-feedback-left"  id="Name" name="Cont_Type" value="{{ $RetailersReview['Cont_Type'] }}" placeholder="enter Cont Type" >
                             
                        
                   
                      </div>
                      </div>




                      </fieldset>
                     

             <input type="hidden" name="Type_User" value="Outdoor Promoters"> 

                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                          <a  class="btn btn-primary " href="/RetailersReview">Cancel</a>
                         <button class="btn btn-primary" type="reset">Reset</button>
                          <button type="submit"  name="login_form_submit" class="btn btn-success" value="Save">Submit</button>
                        </div>
                      </div>

      {!!Form::close() !!} 
                  </div>
                </div>
                 </div>
                
                 <div class="col-md-2 col-xs-12 ">
                 </div>
                </div>

@section('other_scripts')
<script type="text/javascript">

  $(document).ready(function(){
    
    $('#profileForm')
        .formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
          'Has_Truncks':{
                validators: {            
                    
                      
                }
            },
                
                 
           
                         
                      
                       'Area': {
                  
                       validators: {
               
                    }

                    },
                      'Cont_Type': {
                  
                       validators: {
               
                  
                    }

                    }
           
              } ,submitHandler: function(form) {
        form.submit();
    }
        })
        .on('success.form.fv', function(e) {
   // e.preventDefault();
    var $form = $(e.target);

    // Enable the submit button
   
    $form.formValidation('disableSubmitButtons', false);
});
        


  
 });



</script>

</div>
</section>
@stop
@stop