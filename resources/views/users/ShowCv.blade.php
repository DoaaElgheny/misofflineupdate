@extends('masternew')
@section('content')
<div class="row">
  <div class="col-md-2 col-xs-12 ">
  </div>
  <div class="col-md-8 col-xs-12 ">
    <div class="x_panel">
      <div class="x_title">
        <h2>Upload CV</h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        {!!Form::open(['action'=>'UserController@add','method' => 'post','files'=>true])!!}
        <input type="hidden" name="id" value="{{$id}}" />  
        
          <input   name="filefield" type="file"/>
          <input  name="submitcv" value="Import File" class="btn btn-primary" type="submit" />  



        {!!Form::close()!!}
      </div>
    </div>
  </div>               
  <div class="col-md-2 col-xs-12 ">
  </div>
</div>
@section('other_scripts')
@stop
@stop