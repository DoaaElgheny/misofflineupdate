@extends('masternew')
@section('content')
<style type="text/css">
  @import url(http://fonts.googleapis.com/css?family=Roboto+Condensed:400,700);
/* written by riliwan balogun http://www.facebook.com/riliwan.rabo*/
.board{
    width: 100%;
margin: 60px auto;
height: 500px;
background: #fff;
/*box-shadow: 10px 10px #ccc,-10px 20px #ddd;*/
}
.board .nav-tabs {
    position: relative;
    /* border-bottom: 0; */
    /* width: 80%; */
    margin: 40px auto;
    margin-bottom: 0;
    box-sizing: border-box;

}

.board > div.board-inner{
    background: #fafafa ;
    background-size: 30%;
}

p.narrow{
    width: 60%;
    margin: 10px auto;
}

.liner{
    height: 2px;
    background: #ddd;
    position: absolute;
    width: 80%;
    margin: 0 auto;
    left: 0;
    right: 90px;
    top: 50%;
    z-index: 1;
}

.nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus {
    color: #555555;
    cursor: default;
    /* background-color: #ffffff; */
    border: 0;
    border-bottom-color: transparent;
}

span.round-tabs{
    width: 70px;
    height: 70px;
    line-height: 70px;
    display: inline-block;
    border-radius: 100px;
    background: white;
    z-index: 2;
    position: absolute;
    left: 0;
    text-align: center;
    font-size: 25px;
}

span.round-tabs.one{
    color: rgb(34, 194, 34);border: 2px solid rgb(34, 194, 34);
}

li.active span.round-tabs.one{
    background: #fff !important;
    border: 2px solid #ddd;
    color: rgb(34, 194, 34);
}

span.round-tabs.two{
    color: #febe29;border: 2px solid #febe29;
}

li.active span.round-tabs.two{
    background: #fff !important;
    border: 2px solid #ddd;
    color: #febe29;
}

span.round-tabs.three{
    color: #3e5e9a;border: 2px solid #3e5e9a;
}

li.active span.round-tabs.three{
    background: #fff !important;
    border: 2px solid #ddd;
    color: #3e5e9a;
}

span.round-tabs.four{
    color: #f1685e;border: 2px solid #f1685e;
}

li.active span.round-tabs.four{
    background: #fff !important;
    border: 2px solid #ddd;
    color: #f1685e;
}

span.round-tabs.five{
    color: #999;border: 2px solid #999;
}

li.active span.round-tabs.five{
    background: #fff !important;
    border: 2px solid #ddd;
    color: #999;
}

.nav-tabs > li.active > a span.round-tabs{
    background: #fafafa;
}
.nav-tabs > li {
    width: 20%;
}
/*li.active:before {
    content: " ";
    position: absolute;
    left: 45%;
    opacity:0;
    margin: 0 auto;
    bottom: -2px;
    border: 10px solid transparent;
    border-bottom-color: #fff;
    z-index: 1;
    transition:0.2s ease-in-out;
}*/
.nav-tabs > li:after {
    content: " ";
    position: absolute;
    left: 45%;
   opacity:0;
    margin: 0 auto;
    bottom: 0px;
    border: 5px solid transparent;
    border-bottom-color: #ddd;
    transition:0.1s ease-in-out;
    
}
.nav-tabs > li.active:after {
    content: " ";
    position: absolute;
    left: 45%;
   opacity:1;
    margin: 0 auto;
    bottom: 0px;
    border: 10px solid transparent;
    border-bottom-color: #ddd;
    
}
.nav-tabs > li a{
   width: 70px;
   height: 70px;
   margin: 20px auto;
   border-radius: 100%;
   padding: 0;
}

.nav-tabs > li a:hover{
    background: transparent;
}

.tab-content{
}
.tab-pane{
   position: relative;
padding-top: 50px;
}
.tab-content .head{
    font-family: 'Roboto Condensed', sans-serif;
    font-size: 25px;
    text-transform: uppercase;
    padding-bottom: 10px;
}
.btn-outline-rounded{
    padding: 10px 40px;
    margin: 20px 0;
    border: 2px solid transparent;
    border-radius: 25px;
}

.btn.green{
    background-color:#5cb85c;
    /*border: 2px solid #5cb85c;*/
    color: #ffffff;
}



@media( max-width : 585px ){
    
    .board {
width: 90%;
height:auto !important;
}
    span.round-tabs {
        font-size:16px;
width: 50px;
height: 50px;
line-height: 50px;
    }
    .tab-content .head{
        font-size:20px;
        }
    .nav-tabs > li a {
width: 50px;
height: 50px;
line-height:50px;
}

.nav-tabs > li.active:after {
content: " ";
position: absolute;
left: 35%;
}

.btn-outline-rounded {
    padding:12px 20px;
    }
</style>
<section style="background:#efefe9;">
        <div class="container">
            <div class="row">
                <div class="board">
                    <!-- <h2>Welcome to IGHALO!<sup>™</sup></h2>-->
                    <div class="board-inner">
                    <ul class="nav nav-tabs" id="myTab">
                    <div class="liner"></div>
                     <li class="active">
                     <a href="#home" data-toggle="tab" title="Outdoor Promoters">
                      <span class="round-tabs one">
                              <i class="glyphicon glyphicon-user"></i>
                      </span> 
                  </a></li>

                  <li><a href="#profile" data-toggle="tab" title="Office Promoters">
                     <span class="round-tabs two">
                         <i class="glyphicon glyphicon-user"></i>
                     </span> 
           </a>
                 </li>
                 

                    

                     
                     
                     </ul></div>

                     <div class="tab-content">
                      <div class="tab-pane fade in active" id="home">

                  <section class="panel-body">
 <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Users </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                     
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
<?php
//File
  if(!empty($_COOKIE['FileError'])) {     
    echo "<div class='alert alert-block alert-danger fade in center'>";
    echo $_COOKIE['FileError'];
    echo "</div>";
  } 
  //Doublepromoter 
  if(!empty($_COOKIE['DubleUserErr'])) {      
    echo "<div><div class='alert alert-block alert-danger fade in center'>";
    echo $_COOKIE['DubleUserErr'];
    echo "</div> </div>";
  } 
//PromoterErr 
  if(!empty($_COOKIE['UserErr'])) {     
    echo "<div><div class='alert alert-block alert-danger fade in center'>";
    echo $_COOKIE['UserErr'];
    echo "</div> </div>";
  } 

?>
<!-- end php code -->

@if ($showuser==1)
  <h1><i class='fa fa-users'></i> Outdoor Promoters </h1>
  
  @endif
 

  @if ($showuser==1)
   <a type="button" ng-show="{{$showuser}}" class="btn btn-primary" data-toggle="modal" data-target="#myModal1" style="margin-bottom: 20px;"> Add Outdoor Users </a>
  
  
  @endif

     <!-- model -->
 
<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
     <div class="modal-content">
    
          <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                 <h4 class="modal-title span7 text-center" id="myModalLabel"><span class="title">Add New Users</span></h4>
          </div> <!-- end modal-header -->
    <div class="modal-body">
        <div class="tabbable"> <!-- Only required for left/right tabs -->
        <ul class="nav nav-tabs"></ul>
        <div class="tab-content">
        <div class="tab-pane active" id="tab1">
 <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                 
                  <div class="x_content">
                    <br />
{!! Form::open(['route'=>'users.store','method'=>'post','class'=>'form-horizontal form-label-left 
  ','id'=>'profileForm','files'=>'true']) !!}
    <fieldset style="padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;">
      <legend style="width: fit-content;border: 0px;">Personalia:</legend>
 


        <div class="form-group">

          <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
            <input type="text" class="form-control has-feedback-left"  id="Name" name="Name" placeholder="Name">


            <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
          </div>
         

            <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
              <input type="text" class="form-control has-feedback-left" id="EnName" name="EngName" placeholder="English Name">
                           <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>

            </div>
          </div>
  
           <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12  has-feedback">
                  <input type="text" class="form-control has-feedback-left" id="Email" name="Email"  placeholder="Email">
                  <span class="fa fa-envelope form-control-feedback left" aria-hidden="true"></span>
                </div>
                 <div class="col-md-6 col-sm-6 col-xs-12  has-feedback">
                  <input type="text" class="form-control has-feedback-left" id ="Telephone" name="Telephone" placeholder="Telephone">
                  <span class="fa fa-phone form-control-feedback left" aria-hidden="true"></span>
                </div>
            </div>
            <div class="form-group">
             
                  <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                    <input type="number" class="form-control has-feedback-left" id ="IDNumber" name="IDNumber" placeholder=" ID Number">
                     <span class="fa fa-credit-card form-control-feedback left" aria-hidden="true"></span>

                  </div>

                    <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                    <input type="number" class="form-control has-feedback-left" id ="CardNumber" name="CardNumber"  placeholder=" Card Number">
                     <span class="fa fa-bank form-control-feedback left" aria-hidden="true"></span>

                  </div>
            </div>
</fieldset>  <!-- end personail -->
<fieldset style="    padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;">
  <legend style="width: fit-content;border: 0px;">Locations:</legend>

               <div class="form-group">
                      <div class="control-label col-md-6 col-sm-6 col-xs-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Government</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          {!!Form::select('Government', ([null => 'Select  Government'] + $Government->toArray()) ,null,['class'=>' chosen-select','id' => 'Government'])!!}
                        </div>
                     </div>
                    <div class="control-label col-md-6 col-sm-6 col-xs-12">

                        <label class="control-label col-md-3 col-sm-3 col-xs-12">District
                        </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                           {!!Form::select('District', ([null => 'Select  District'] + $District->toArray()) ,null,['class'=>'form-control chosen-select District ','id' => 'prettify'])!!}
                        </div>
                        </div>

                </div>
                <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                          <input type="text" class="form-control has-feedback-left" id ="Latitude" name="Latitude" placeholder="Home Latitude">
                        <span class="fa fa-map-marker form-control-feedback left" aria-hidden="true"></span>
                        </div>
                     
                        <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                          <input type="text" class="form-control has-feedback-left" id ="Longitude" name="Longitude" placeholder="Home Longitude">
                        <span class="fa fa-map-marker form-control-feedback left" aria-hidden="true"></span>
                        </div>
                      </div>
              </fieldset>  <!-- end Locations -->

        <fieldset style="    padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;">
           <legend style="width: fit-content;border: 0px;">Sytem Roles:</legend>
              <div class="form-group">
                    <div class="control-label col-md-6 col-sm-6 col-xs-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Mobile Role
                        </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">  
                        {!!Form::select('MobRole',array('0' => 'Chosse Mobile Role','admin' => 'admin','user' => 'user',),null,
                           ['id'=> 'prettify','class'=>'chosen-select Role form-control']);!!}
                           </div>
                    </div>

                      
                    <div class="control-label col-md-6 col-sm-6 col-xs-12">

                        <label class="control-label col-md-3 col-sm-3 col-xs-12">System Roles
                        </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">                       
                         {!!Form::select('Role[]',($Role->toArray()) ,null,['class'=>'chosen-select Role ','id' => 'prettify','multiple' => true])!!}
                        </div>
                    </div>
                </div>
                <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12  has-feedback">
                        <input type="text" class="form-control has-feedback-left" id="Username" name="Username" placeholder="Username">
                        <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                      </div>
                      <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                        <input type="Password" class="form-control has-feedback-left" id="Password" name="Password"   placeholder="Password">
                         <span class="fa fa-lock form-control-feedback left" aria-hidden="true"></span>

                      </div>
                </div>
      </fieldset> <!-- end Sytem Roles -->

<fieldset style="padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;">
      <legend style="width: fit-content;border: 0px;">Other Data:</legend>

            <div class="form-group">
                <div class="control-label col-md-6 col-sm-6 col-xs-12">

                        <label class="control-label col-md-5 col-sm-5 col-xs-12">HE/She is Supervisior? </label>
                        <div class="col-md-1 col-sm-1 col-xs-12">
                        <input type="checkbox" class="flat" name="Position" value="1">

                        </div>
                </div>

                <div class="control-label col-md-6 col-sm-6 col-xs-12">

                         <label class="control-label col-md-3 col-sm-3 col-xs-12">Supervisior</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          {!!Form::select('User', ([null => 'Select Supervisior Name'] + $User->toArray()) ,null,['class'=>'form-control chosen-select Sup_User ','id' => 'prettify','name'=>'User'])!!}
                        </div>
                </div>
            </div>

            <div class="form-group">

                  <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                    <input type="number" class="form-control has-feedback-left" id ="Experince" name="Experince"  placeholder=" Experince">
                   <span class="fa fa-briefcase form-control-feedback left" aria-hidden="true"></span>

                  </div>
                      
                   <div class="col-md-6 col-sm-6 col-xs-12">
                    <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>

                      <input type="text" class="form-control has-feedback-left" id="single_cal4" name="Date" placeholder="Date">
                    </div>
            </div>
            <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                          <input type="text" class="form-control has-feedback-left" name="Salary" placeholder="Salary" required>
                         <span class="fa fa-money form-control-feedback left" aria-hidden="true"></span>

                        </div>
            </div>
             <input type="hidden" name="Type_User" value="Outdoor Promoters"> 
                     
</fieldset>

   <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                          <button type="button" class="btn btn-primary " data-dismiss="modal">Cancel</button>
                         <button class="btn btn-primary" type="reset">Reset</button>
                          <button type="submit"  name="login_form_submit" class="btn btn-success" value="Save">Submit</button>
                        </div>
                      </div>



      {!!Form::close() !!}  

</div><!-- end x_content-->
</div> <!-- end x_panel -->
</div> <!-- end col-md-12 col-sm-12 col-xs-12 -->
</div><!-- end row-->



</div> <!-- end tab1 -->
</div> <!-- end tab-content -->
</div>  <!--  end tabbable -->
</div> <!-- end modal-body -->
</div><!-- end modal-content -->
</div><!-- end modal-dialog -->
</div><!-- end myModal1 -->
 
                   
                    <table class="table table-hover table-bordered  dt-responsive nowrap  users" cellspacing="0" width="100%">


  <thead>
<th>No</th>
<th>Name</th>
<th>Username</th>
<th>Email</th>
<th>Government</th>
<th>District</th>
<th>Code</th>
<th>Telephone</th>
<th>Bank No</th>

<th>ID Number</th>


<th>Latitude</th>
<th>Longitude</th>
<th>Daily Wage</th>

<th>Start Date</th>
<th>Experince</th>
<th>Supervisior</th>
<th>Position</th>

<th>Mobile Role</th>
<th>Role</th>  
<th> Upload</th>
<th>Download</th>
<th>open</th>   
<th>Process</th>         
  </thead>
<tfoot>

<th></th>
<th>Name</th>
<th>Username</th>
<th>Email</th>
<th>Government</th>
<th>District</th>
<th>Code</th>
<th>Telephone</th>
<th>Bank No</th>

<th>ID Number</th>

<th>Daily Wage</th>
<th>Latitude</th>
<th>Longitude</th>
<th>Start Date</th>
<th>Experince</th>
<th>Supervisior</th>
<th>Position</th>
<th>Mobile Role</th>

<th>Role</th>
<th></th> 
<th></th> 
<th></th> 
<th></th> 
</tfoot>


<tbody style="text-align:center;">
<?php $i=1;?>
@foreach($usersOutDoor as $user)
<tr>

<td>{{$i++}}</td>
<td>{{$user->Name}}</td>
<td >{{$user->Username}}</td>
<td >{{$user->Email}}</td>
<td >{{$user->Government}}</td>
<td >{{$user->District}}</td>
<td >{{$user->Code}}</td>
<td >{{$user->Tele}}</td>
<td >{{$user->CardNumber}}</td>
<td >{{$user->IDNumber}}</td>
<td >{{$user->Lat}}</td>
<td >{{$user->Long}}</td>
<td >{{$user->Salary}}</td>
<td >{{$user->Start_Date}}</td>
<td >{{$user->Experince}}</td>
@if($user->Supervisor == NULL)
<td ></td>
@else
<td >{{$user->visor->Name}}</td>
@endif
<td >{{$user->Position}}</td>

<td >{{$user->role}}</td>


<td >
@foreach($user->roles as $rp) 

{{$rp->name}}

@endforeach

</td>
<td >

<a href="/offlineneew/public/ShowFiles/{{$user->id}}" class="btn btn-default btn-sm" ><span class="glyphicon glyphicon-open"></span></a>



</td>
<td >
<a href="/offlineneew/public/get/{{$user->id}}" class="btn btn-default btn-sm" ><span class="glyphicon glyphicon-save"></span></a>


</td>

<td>
<a href="/offlineneew/public/ViewPDFUser/{{$user->id}}" class="btn btn-default btn-sm" ><span class="glyphicon glyphicon-folder-open"></span></a>

</td>
<td >
 <small>Delete </small><a href="/offlineneew/public/users/destroy/{{$user->id}}" class="btn btn-default btn-sm" ><span class="glyphicon glyphicon-trash"></span>  </a>
 <small>Edit </small><a  href="/offlineneew/public/users/{{$user->id}}/edit" class="btn btn-default btn-sm" ><span class="glyphicon glyphicon-edit"></span>  </a>

</td>
</tr>
@endforeach
</tbody>
</table>
{!!Form::open(['action'=>'UserController@importuser','method' => 'post','files'=>true])!!}
  

<label >
    <i ></i> Custom Upload
</label>
<input id="file-upload"  name="file" type="file"/>
  <input  name="submituser" value="Import File" class="btn btn-primary" type="submit" />  


{!!Form::close()!!}
          
                  </div>
                </div>
              </div>
                      </div>
                      <div class="tab-pane fade" id="profile">
                  <section class="panel-body">
 <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Users </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                     
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
<?php
//File
  if(!empty($_COOKIE['FileError'])) {     
    echo "<div class='alert alert-block alert-danger fade in center'>";
    echo $_COOKIE['FileError'];
    echo "</div>";
  } 
  //Doublepromoter 
  if(!empty($_COOKIE['DubleUserErr'])) {      
    echo "<div><div class='alert alert-block alert-danger fade in center'>";
    echo $_COOKIE['DubleUserErr'];
    echo "</div> </div>";
  } 
//PromoterErr 
  if(!empty($_COOKIE['UserErr'])) {     
    echo "<div><div class='alert alert-block alert-danger fade in center'>";
    echo $_COOKIE['UserErr'];
    echo "</div> </div>";
  } 

?>
<!-- end php code -->


  
    <h1><i class='fa fa-users'></i> Office Promoters </h1>

 

  
  
   <a type="button" ng-show="{{$showuser}}" class="btn btn-primary" data-toggle="modal" data-target="#myModal1" style="margin-bottom: 20px;"> Add Office Users </a>
  
  

     <!-- model -->
 
<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
     <div class="modal-content">
    
          <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                 <h4 class="modal-title span7 text-center" id="myModalLabel"><span class="title">Add New Users</span></h4>
          </div> <!-- end modal-header -->
    <div class="modal-body">
        <div class="tabbable"> <!-- Only required for left/right tabs -->
        <ul class="nav nav-tabs"></ul>
        <div class="tab-content">
        <div class="tab-pane active" id="tab1">
 <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                 
                  <div class="x_content">
                    <br />
{!! Form::open(['route'=>'users.store','method'=>'post','class'=>'form-horizontal form-label-left 
  ','id'=>'profileForm','files'=>'true']) !!}
    <fieldset style="padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;">
      <legend style="width: fit-content;border: 0px;">Personalia:</legend>
 


        <div class="form-group">

          <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
            <input type="text" class="form-control has-feedback-left"  id="Name" name="Name" placeholder="Name">


            <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
          </div>
         

            <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
              <input type="text" class="form-control has-feedback-left" id="EnName" name="EngName" placeholder="English Name">
                           <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>

            </div>
          </div>
  
           <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12  has-feedback">
                  <input type="text" class="form-control has-feedback-left" id="Email" name="Email"  placeholder="Email">
                  <span class="fa fa-envelope form-control-feedback left" aria-hidden="true"></span>
                </div>
                 <div class="col-md-6 col-sm-6 col-xs-12  has-feedback">
                  <input type="text" class="form-control has-feedback-left" id ="Telephone" name="Telephone" placeholder="Telephone">
                  <span class="fa fa-phone form-control-feedback left" aria-hidden="true"></span>
                </div>
            </div>
            <div class="form-group">
             
                  <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                    <input type="number" class="form-control has-feedback-left" id ="IDNumber" name="IDNumber" placeholder=" ID Number">
                     <span class="fa fa-credit-card form-control-feedback left" aria-hidden="true"></span>

                  </div>

                    <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                    <input type="number" class="form-control has-feedback-left" id ="CardNumber" name="CardNumber"  placeholder=" Card Number">
                     <span class="fa fa-bank form-control-feedback left" aria-hidden="true"></span>

                  </div>
            </div>
</fieldset>  <!-- end personail -->
<fieldset style="    padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;">
  <legend style="width: fit-content;border: 0px;">Locations:</legend>

               <div class="form-group">
                      <div class="control-label col-md-6 col-sm-6 col-xs-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Government</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          {!!Form::select('Government', ([null => 'Select  Government'] + $Government->toArray()) ,null,['class'=>' chosen-select','id' => 'Government'])!!}
                        </div>
                     </div>
                    <div class="control-label col-md-6 col-sm-6 col-xs-12">

                        <label class="control-label col-md-3 col-sm-3 col-xs-12">District
                        </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                           {!!Form::select('District', ([null => 'Select  District'] + $District->toArray()) ,null,['class'=>'form-control chosen-select District ','id' => 'prettify'])!!}
                        </div>
                        </div>

                </div>
                <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                          <input type="text" class="form-control has-feedback-left" id ="Latitude" name="Latitude" placeholder="Home Latitude">
                        <span class="fa fa-map-marker form-control-feedback left" aria-hidden="true"></span>
                        </div>
                     
                        <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                          <input type="text" class="form-control has-feedback-left" id ="Longitude" name="Longitude" placeholder="Home Longitude">
                        <span class="fa fa-map-marker form-control-feedback left" aria-hidden="true"></span>
                        </div>
                      </div>
              </fieldset>  <!-- end Locations -->

        <fieldset style="    padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;">
           <legend style="width: fit-content;border: 0px;">Sytem Roles:</legend>
              <div class="form-group">
                    <div class="control-label col-md-6 col-sm-6 col-xs-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Mobile Role
                        </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">  
                        {!!Form::select('MobRole',array('0' => 'Chosse Mobile Role','admin' => 'admin','user' => 'user',),null,
                           ['id'=> 'prettify','class'=>'chosen-select Role form-control']);!!}
                           </div>
                    </div>

                      
                    <div class="control-label col-md-6 col-sm-6 col-xs-12">

                        <label class="control-label col-md-3 col-sm-3 col-xs-12">System Roles
                        </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">                       
                         {!!Form::select('Role[]',($Role->toArray()) ,null,['class'=>'chosen-select Role ','id' => 'prettify','multiple' => true])!!}
                        </div>
                    </div>
                </div>
                <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12  has-feedback">
                        <input type="text" class="form-control has-feedback-left" id="Username" name="Username" placeholder="Username">
                        <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                      </div>
                      <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                        <input type="Password" class="form-control has-feedback-left" id="Password" name="Password"   placeholder="Password">
                         <span class="fa fa-lock form-control-feedback left" aria-hidden="true"></span>

                      </div>
                </div>
      </fieldset> <!-- end Sytem Roles -->

<fieldset style="padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;">
      <legend style="width: fit-content;border: 0px;">Other Data:</legend>

            <div class="form-group">
                <div class="control-label col-md-6 col-sm-6 col-xs-12">

                        <label class="control-label col-md-5 col-sm-5 col-xs-12">HE/She is Supervisior? </label>
                        <div class="col-md-1 col-sm-1 col-xs-12">
                        <input type="checkbox" class="flat" name="Position" value="1">

                        </div>
                </div>

                <div class="control-label col-md-6 col-sm-6 col-xs-12">

                         <label class="control-label col-md-3 col-sm-3 col-xs-12">Supervisior</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          {!!Form::select('User', ([null => 'Select Supervisior Name'] + $User->toArray()) ,null,['class'=>'form-control chosen-select Sup_User ','id' => 'prettify','name'=>'User'])!!}
                        </div>
                </div>
            </div>

            <div class="form-group">

                  <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                    <input type="number" class="form-control has-feedback-left" id ="Experince" name="Experince"  placeholder=" Experince">
                   <span class="fa fa-briefcase form-control-feedback left" aria-hidden="true"></span>

                  </div>
                      
                   <div class="col-md-6 col-sm-6 col-xs-12">
                    <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>

                      <input type="text" class="form-control has-feedback-left" id="single_cal4" name="Date" placeholder="Date">
                    </div>
            </div>
            <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                          <input type="text" class="form-control has-feedback-left" name="Salary" placeholder="Salary" required>
                         <span class="fa fa-money form-control-feedback left" aria-hidden="true"></span>

                        </div>
            </div>
             <input type="hidden" name="Type_User" value="Office Promoters"> 
                     
</fieldset>

   <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                          <button type="button" class="btn btn-primary " data-dismiss="modal">Cancel</button>
                         <button class="btn btn-primary" type="reset">Reset</button>
                          <button type="submit"  name="login_form_submit" class="btn btn-success" value="Save">Submit</button>
                        </div>
                      </div>



      {!!Form::close() !!}  

</div><!-- end x_content-->
</div> <!-- end x_panel -->
</div> <!-- end col-md-12 col-sm-12 col-xs-12 -->
</div><!-- end row-->



</div> <!-- end tab1 -->
</div> <!-- end tab-content -->
</div>  <!--  end tabbable -->
</div> <!-- end modal-body -->
</div><!-- end modal-content -->
</div><!-- end modal-dialog -->
</div><!-- end myModal1 -->
 
                   
                    <table class="table table-hover table-bordered  dt-responsive nowrap  users" cellspacing="0" width="100%">


  <thead>
<th>No</th>
<th>Name</th>
<th>Username</th>
<th>Email</th>
<th>Government</th>
<th>District</th>
<th>Code</th>
<th>Telephone</th>
<th>Bank No</th>

<th>ID Number</th>


<th>Latitude</th>
<th>Longitude</th>
<th>Daily Wage</th>

<th>Start Date</th>
<th>Experince</th>
<th>Supervisior</th>
<th>Position</th>

<th>Mobile Role</th>
<th>Role</th>  
<th> Upload</th>
<th>Download</th>
<th>open</th>   
<th>Process</th>         
  </thead>
<tfoot>

<th></th>
<th>Name</th>
<th>Username</th>
<th>Email</th>
<th>Government</th>
<th>District</th>
<th>Code</th>
<th>Telephone</th>
<th>Bank No</th>

<th>ID Number</th>

<th>Daily Wage</th>
<th>Latitude</th>
<th>Longitude</th>
<th>Start Date</th>
<th>Experince</th>
<th>Supervisior</th>
<th>Position</th>
<th>Mobile Role</th>

<th>Role</th>
<th></th> 
<th></th> 
<th></th> 
<th></th> 
</tfoot>


<tbody style="text-align:center;">
<?php $i=1;?>
@foreach($usersOffice as $user)
<tr>

<td>{{$i++}}</td>
<td>{{$user->Name}}</td>
<td >{{$user->Username}}</td>
<td >{{$user->Email}}</td>
<td >{{$user->Government}}</td>
<td >{{$user->District}}</td>
<td >{{$user->Code}}</td>
<td >{{$user->Tele}}</td>
<td >{{$user->CardNumber}}</td>
<td >{{$user->IDNumber}}</td>
<td >{{$user->Lat}}</td>
<td >{{$user->Long}}</td>
<td >{{$user->Salary}}</td>
<td >{{$user->Start_Date}}</td>
<td >{{$user->Experince}}</td>
@if($user->Supervisor == NULL)
<td ></td>
@else
<td >{{$user->visor->Name}}</td>
@endif
<td >{{$user->Position}}</td>

<td >{{$user->role}}</td>


<td >
@foreach($user->roles as $rp) 

{{$rp->name}}

@endforeach

</td>
<td >

<a href="/offlineneew/public/ShowFiles/{{$user->id}}" class="btn btn-default btn-sm" ><span class="glyphicon glyphicon-open"></span></a>



</td>
<td >
<a href="/offlineneew/public/get/{{$user->id}}" class="btn btn-default btn-sm" ><span class="glyphicon glyphicon-save"></span></a>


</td>

<td>
<a href="/offlineneew/public/ViewPDFUser/{{$user->id}}" class="btn btn-default btn-sm" ><span class="glyphicon glyphicon-folder-open"></span></a>

</td>
<td >
 <small>Delete </small><a href="/offlineneew/public/users/destroy/{{$user->id}}" class="btn btn-default btn-sm" ><span class="glyphicon glyphicon-trash"></span>  </a>
 <small>Edit </small><a  href="/offlineneew/public/users/{{$user->id}}/edit" class="btn btn-default btn-sm" ><span class="glyphicon glyphicon-edit"></span>  </a>

</td>
</tr>
@endforeach
</tbody>
</table>
{!!Form::open(['action'=>'UserController@importuser','method' => 'post','files'=>true])!!}
  

<label >
    <i ></i> Custom Upload
</label>
<input id="file-upload"  name="file" type="file"/>
  <input  name="submituser" value="Import File" class="btn btn-primary" type="submit" />  


{!!Form::close()!!}
          
                  </div>
                </div>
              </div>
                          
                      </div>
                
           

<div class="clearfix"></div>
</div>

</div>
</div>
</div>
</section>

  @section('other_scripts')
                   <script type="text/javascript">
                     $(function(){
$('a[title]').tooltip();
});
                   </script>
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>



  <script type="text/javascript">
           $('.District').chosen({
                width: '100%',
                no_results_text:'No Results'
            });
                $(".District").trigger("chosen:updated");
            $(".District").trigger("change");



     $('.Sup_User').chosen({
                width: '100%',
                no_results_text:'No Results'
            });
                $(".Sup_User").trigger("chosen:updated");
            $(".Sup_User").trigger("change");









            
 $('#profileForm')
        .find('[name="Government"]')
            .chosen({
                width: '100%',
                inherit_select_classes: true
            })
            // Revalidate the color when it is changed
            .change(function(e) {

                $('#profileForm').formValidation('revalidateField', 'Government');

  $.getJSON("/offlineneew/public/government/district/" + $("#Government").val(), function(data) {
 
                var $contractors = $(".District");
                   $(".District").chosen("destroy");
                $contractors.empty();
                $.each(data, function(index, value) {
                    $contractors.append('<option value="' + index +'">' + value + '</option>');
                });
                  
        
             $('.District').chosen({
                width: '100%',
                no_results_text:'No Results'
            });
                $(".District").trigger("chosen:updated");
            $(".District").trigger("change");
            });
            }).end()
        .formValidation({
            framework: 'bootstrap',
            excluded: ':disabled',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                Government: {
                    validators: {
                        callback: {
                            message: 'Please choose Government',
                            callback: function(value, validator, $field) {
                                // Get the selected options
                                var options = validator.getFieldElements('Government').val();
                              
                                return (options !== '' );
                            }
                        }
                    }
                }, 'Name': {
                    validators: {
                  

             notEmpty:{
                    message:"the Name is required"
                  },
                     regexp: {
                        regexp: /^[\u0600-\u06FF\s]+$/,
                        message: 'The full name can only consist of Arabic alphabetical and spaces'
                     }
                  }
                },
                     'EngName': {
                    validators: {
                     

             notEmpty:{
                    message:"the English Name is required"
                  },
                    regexp: {
                        regexp: /^[\sa-zA-Z]+$/,
                        message: 'The full name can only consist of English alphabetical and spaces'
                    }
                       
                    }
                },
                      'Username': {
                    validators: {
                     
          remote: {
                        url: '/offlineneew/public/checkUsername',
                        // data:function(validator, $field, value)
                        // {return{name:validator.getFieldElements('name').val()};},
                        message: 'The Username name is not available',
                        type: 'GET'
                    }, 
             notEmpty:{
                    message:"the Username is required"
                  },
                    regexp: {
                        regexp: /^[\u0600-\u06FF\sa-zA-Z]+$/,
                        message: 'The full Username can only consist of  alphabetical and spaces'
                    }
                       
                    }
                },'Longitude':
                {
                    validators: {
                    regexp: {
                        regexp: /^[0-9]{2}[\.]{1}[0-9]+$/,
                        message: 'The full name can only consist of English alphabetical and spaces'
                    }
                       
                    }
                },'Latitude':
                 {
                    validators: {
                    regexp: {
                        regexp: /^[0-9]{2}[\.]{1}[0-9]+$/,
                        message: 'The full name can only consist of English alphabetical and spaces'
                    }
                       
                    }
                },

                'Email':{
                    validators: {
                     
                      remote: {
                        url: '/offlineneew/public/checkEmailuser',
                       
                        message: 'The Email name is not available',
                        type: 'GET'
                    }, 
             notEmpty:{
                    message:"the Email is required"
                  } 
                    }
                },
                 'Telephone':{
                    validators: {
                       remote: {
                        url: '/offlineneew/public/checkTelephoneUser',
                        // data:function(validator, $field, value)
                        // {return{name:validator.getFieldElements('name').val()};},
                        message: 'The Telephone  is not available',
                        type: 'GET'
                    },
                     
      stringLength: {
        min:11,
                        max: 11,
                        message: 'The Telephone must be between 6 and 11 digits'
                    },
             notEmpty:{
                    message:"the Telephone is required"
                  } 
                    }
                },
                 
                   'Password': {
                    validators: {  
             notEmpty:{
                    message:"the Password is required"
                  } 
                    }
                },
         
                    'District': {
                    validators: {
                     
        
             notEmpty:{
                    message:"the District is required"
                  } 
                    }
                },
             //           'User': {
             //        validators: {
                     
        
             // notEmpty:{
             //        message:"the Supervisior is required"
             //      } 
             //        }
             //    },
            User:{
                    validators: {
                        callback: {
                            message: 'the Supervisior is required',
                            callback: function(value, validator, $field) {
                                var framework = $('#profileForm').find('[name="Position"]:checked').val();
                                console.log(framework == '1');
                                return (framework == '1') ? null : (value !== '');
                            }
                        }
                    }
                }
         
            }
        });
var editor;

  $(document).ready(function(){

     var table= $('.users').DataTable({
  
    select:true,
    responsive: true,
    "order":[[0,"asc"]],
    'searchable':true,
    "scrollCollapse":true,
    "paging":true,
    "pagingType": "simple",
      dom: 'lBfrtip',
        buttons: [
           
            
            { extend: 'excel', className: 'btn btn-primary dtb' }
            
            
        ],

fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
$.fn.editable.defaults.send = "always";

$.fn.editable.defaults.mode = 'inline';

$('.testEdit').editable({

      validate: function(value) {
        name=$(this).editable().data('name');
        if(name=="name"||name=="username"||name=="email"||name=="role")
        {
                if($.trim(value) == '') {
                    return 'Value is required.';
                  }
                    }
        if(name=="name"||name=="username")
        {

      var regexp = /^[a-zA-Z\u0600-\u06FF,-][\sa-zA-Z\u0600-\u06FF,-]+ {1}[a-zA-Z\u0600-\u06FF,-][\sa-zA-Z\u0600-\u06FF,-]/;            if (!regexp.test(value)) {
            return 'This field is not valid';
        }
        
        }
        },

    placement: 'right',
    url:'{{URL::to("/")}}/fupdate',
  
     ajaxOptions: {
     type: 'get',
    sourceCache: 'false',
     dataType: 'json'

   },

       params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');

            // console.log(params);
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'This Data Already Exist,Enter Correct Data.';
            } else {
                return response.responseText;
            }
        }


});

}
});

 



   $('.users tfoot th').each(function () {



      var title = $('.users thead th').eq($(this).index()).text();
               
 if($(this).index()>=1 && $(this).index()<=14)
        {

           $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
}
        

    });
  table.columns().every( function () {
  var that = this;
 $(this.footer()).find('input').on('keyup change', function () {
          that.search(this.value).draw();
            if (that.search(this.value) ) {
               that.search(this.value).draw();
           }
        });
      
 
   });

  $(".Role").chosen({ 
                   width: '100%',
                   no_results_text: "No Results",
                   allow_single_deselect: true, 
                   search_contains:true, });
 $(".Role").trigger("chosen:updated");

    $('[data-toggle="popover"]').popover({ trigger: "hover" }); 

 

    $('[data-toggle="popover"]').popover({ trigger: "hover" }); 
 });
  </script>


                   @endsection
@endsection