@extends('masternew')


@section('content')


<div class="row">

<div class="col-md-2 col-xs-12 ">
 </div>

              <div class="col-md-8 col-xs-12 ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Edit Users</h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                      {!! Form::open(['route' =>[ 'users.update',$users->id],'method'=>'put','class'=>'form-horizontal form-label-left ','id'=>'profileForm','files'=>'true']) !!}

                    
  <input type="hidden" name="_token" value="{{ csrf_token() }}">

       <input type="hidden" id="id" name="id" value="{{ $users['id'] }}">
       <fieldset style="    padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;">
  <legend style="width: fit-content;border: 0px;">Personal Data:</legend>

                    <div class="form-group">

                      <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                        <input type="text" class="form-control has-feedback-left"  id="Name" value="{{ $users['Name'] }}" name="Name">


                        <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                      </div>
                      <div class="col-md-6 col-sm-6 col-xs-12  has-feedback">
                        <input type="text" class="form-control has-feedback-left" id="EnName" value="{{ $users['Enname'] }}" name="EngName" placeholder="English Name">


                        <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                      </div>
                      </div>

                     <div class="form-group">
                      <div class="col-md-6 col-sm-6 col-xs-12  has-feedback">
                        <input type="text" class="form-control has-feedback-left"  value="{{ $users['Email'] }}" name="Email"  placeholder="Email">
                        <span class="fa fa-envelope form-control-feedback left" aria-hidden="true"></span>
                      </div>
                       <div class="col-md-6 col-sm-6 col-xs-12  has-feedback">
                        <input type="number" class="form-control has-feedback-left" id ="Telephone" name ="Telephone"  value="{{ $users['Tele'] }}" placeholder="Telephone" >
                        <span class="fa fa-phone form-control-feedback left" aria-hidden="true"></span>
                      </div>
                      </div>
                       <div class="form-group ">
                        <label class="control-label col-md-1 col-sm-1 col-xs-12">ID No.</label> 
                        <div class="col-md-5 col-sm-5 col-xs-12">

         <input type="number" class="form-control has-feedback-left" id ="IDNumber" name="IDNumber" value="{{ $users['IDNumber'] }}" placeholder ="ID Number">
                                 <span class="fa fa-credit-card form-control-feedback left" aria-hidden="true"></span>

                        </div>

                     

                       <div class="form-group has-feedback">
                        <label class="control-label col-md-1 col-sm-1 col-xs-12">Bank No.</label> 
                        <div class="col-md-5 col-sm-5 col-xs-12">

         <input type="number" class="form-control has-feedback-left" value="{{ $users['CardNumber'] }}" id ="CardNumber" name="CardNumber" placeholder="Bank Number">
                                 <span class="fa fa-bank form-control-feedback left" aria-hidden="true"></span>

                               

                        </div>
                      </div>
                       </div>
                      </fieldset>
                      
    <fieldset style="    padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;">
  <legend style="width: fit-content;border: 0px;">Locations:</legend>
 <div class="form-group">
 <label class="control-label col-md-1 col-sm-1 col-xs-12">Goverment</label>
                      <div class="col-md-5 col-sm-5 col-xs-12 has-feedback">
                         {!!Form::select('Government', ([null => 'Select  Government'] + $Government->toArray()) ,$users['Government'],['class'=>' chosen-select','id' => 'Government'])!!}

                      </div>
                       <label class="control-label col-md-1 col-sm-1 col-xs-12">District</label>
                      <div class="col-md-5 col-sm-5 col-xs-12  has-feedback">
                         {!!Form::select('District', ([null => 'Select  District']+ $District->toArray()) ,$users['District'],['class'=>'form-control chosen-select District ','id' => 'District'])!!}


                      </div>
                      </div>
                       <div class="form-group">
                        <label class="control-label col-md-1 col-sm-1 col-xs-12">Lat</label>


                      <div class="col-md-5 col-sm-5 col-xs-12 has-feedback">
                        <input type="text" class="form-control has-feedback-left"  value="{{ $users['Lat'] }}" id ="Lat" name="Lat">
                      </div>

                       <label class="control-label col-md-1 col-sm-1 col-xs-12">Long</label>
                      <div class="col-md-5 col-sm-5 col-xs-12  has-feedback">
                        <input type="text" class="form-control has-feedback-left" id="Long" value="{{ $users['Long'] }}" name="Long" placeholder="Long">


                      </div>
                      </div>
                     
                      </fieldset>




 <fieldset style="    padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;">
  <legend style="width: fit-content;border: 0px;">Locations:</legend>
 <div class="form-group">
 <label class="control-label col-md-1 col-sm-1 col-xs-12">Username</label>
                      <div class="col-md-5 col-sm-5 col-xs-12 has-feedback">
                          <input type="text" class="form-control" id="Username" value="{{ $users['Username'] }}" name="Username" placeholder="Username" required>

                      </div>
                       <label class="control-label col-md-1 col-sm-1 col-xs-12">Password</label>
                      <div class="col-md-5 col-sm-5 col-xs-12  has-feedback">
                         <input type="password"  class="form-control"  id="Password"  name="Password"  placeholder="text" >


                      </div>
                      </div>
                       <div class="form-group">
                        <label class="control-label col-md-1 col-sm-1 col-xs-12">Mobile Role</label>


                      <div class="col-md-5 col-sm-5 col-xs-12 has-feedback">
         {!!Form::select('MobRole',
 array( 
 '0' => 'Chosse Role',
 'admin' => 'admin',
  'User' => 'User',
),$users['role'],['id' => 'prettify','class'=>'chosen-select Role form-control'])!!}
                      </div>

                       <label class="control-label col-md-1 col-sm-1 col-xs-12">System Role</label>
                      <div class="col-md-5 col-sm-5 col-xs-12  has-feedback">
                        {!!Form::select('Role[]',($Role->toArray()) ,null,['class'=>'form-control  chosen-select Role ','id' => 'prettify','multiple' => true])!!}


                      </div>
                      </div>
                     
                      </fieldset>




                          
 <fieldset style="    padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;">
  <legend style="width: fit-content;border: 0px;">Other Data:</legend>
 <div class="form-group">
 <label class="control-label col-md-1 col-sm-1 col-xs-12">He/She Supervisior?</label>
                      <div class="col-md-5 col-sm-5 col-xs-12 has-feedback">
           <input type="checkbox" name="Position" id="Position" class="flat" value="{{ $users['Position'] }}" >


                      </div>
                       <label class="control-label col-md-1 col-sm-1 col-xs-12">Supervisor</label>
                      <div class="col-md-5 col-sm-5 col-xs-12  has-feedback">
                          {!!Form::select('User', ([null => 'Select  Name'] + $User->toArray()) ,$users['Supervisor'],['class'=>'form-control chosen-select User ','id' => 'prettify','name'=>'User'])!!}


                      </div>
                      </div>
                       <div class="form-group">
                        <label class="control-label col-md-1 col-sm-1 col-xs-12">Experince</label>


                      <div class="col-md-5 col-sm-5 col-xs-12 has-feedback">
          <input type="number" placeholder="Experince"  class="form-control"  value="{{ $users['Experince'] }}" id ="Experince" name="Experince" placeholder="Experince " >
                      </div>

                       <label class="control-label col-md-1 col-sm-1 col-xs-12">Salary</label>
                      <div class="col-md-5 col-sm-5 col-xs-12  has-feedback">
                        <input type="number" placeholder="Salary"  class="form-control" value="{{ $users['Salary'] }}" name="Salary" placeholder="Salary " >



                      </div>
                      </div>

                          <div class="form-group">
                        <label class="control-label col-md-1 col-sm-1 col-xs-12">Start Date</label>


                      <div class="col-md-10 col-sm-10 col-xs-12 has-feedback">
                      <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>

           <input type="text" class="form-control has-feedback-left" id="single_cal4" value="{{$usersdatef}}" name="Date" placeholder="Start Date">
                      </div>

                  
                      </div>
                     
                      </fieldset>                     

             <input type="hidden" name="Type_User" value="Outdoor Promoters"> 

                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
          <a  class="btn btn-primary " href="/offlineneew/public/users">Cancel</a>
                         <button class="btn btn-primary" type="reset">Reset</button>
                          <button type="submit"  name="login_form_submit" class="btn btn-success"  disabled>Save</button>
                        </div>
                      </div>

      {!!Form::close() !!} 
                  </div>
                </div>
                 </div>
                
                 <div class="col-md-2 col-xs-12 ">
                 </div>
                </div>

@section('other_scripts')
<script type="text/javascript">

 $(document).ready(function() {
   $('.District').chosen({
                width: '100%',
                no_results_text:'No Results'
            });
                $(".District").trigger("chosen:updated");
            $(".District").trigger("change");
    $('#profileForm')
        .find('[name="Government"]')
            .chosen({
                width: '100%',
                inherit_select_classes: true
            })
            // Revalidate the color when it is changed
            .change(function(e) {

                $('#profileForm').formValidation('revalidateField', 'Government');

  $.getJSON("/users/government/district2/" +$("#Government").val(), function(data) {
                  console.log(data);
                var $contractors = $(".District");
                   $(".District").chosen("destroy");
                $contractors.empty();
                $.each(data, function(index, value) {
                    $contractors.append('<option value="' + index +'">' + value + '</option>');
                });
                  
        
             $('.District').chosen({
                width: '100%',
                no_results_text:'No Results'
            });
                $(".District").trigger("chosen:updated");
            $(".District").trigger("change");
            });
            }).end()
        .formValidation({
            framework: 'bootstrap',
            excluded: ':disabled',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                Government: {
                    validators: {
                        callback: {
                            message: 'Please choose Government',
                            callback: function(value, validator, $field) {
                                // Get the selected options
                                var options = validator.getFieldElements('Government').val();
                              
                                return (options !== '' );
                            }
                        }
                    }
                }, 'Name': {
                    validators: {
                  

             notEmpty:{
                    message:"the Name is required"
                  },
                     regexp: {
                        regexp: /^[\u0600-\u06FF\s]+$/,
                        message: 'The full name can only consist of Arabic alphabetical and spaces'
                     }
                  }
                },
                     'EngName': {
                    validators: {
                     

             notEmpty:{
                    message:"the English Name is required"
                  },
                    regexp: {
                        regexp: /^[a-zA-Z\s]+$/,
                        message: 'The full name can only consist of English alphabetical and spaces'
                    }
                       
                    }
                },'Longitude':
                {
                    validators: {
                    regexp: {
                        regexp: /^[0-9]{2}[\.]{1}[0-9]+$/,
                        message: 'The full name can only consist of English alphabetical and spaces'
                    }
                       
                    }
                },'Latitude':
                 {
                    validators: {
                    regexp: {
                        regexp: /^[0-9]{2}[\.]{1}[0-9]+$/,
                        message: 'The full name can only consist of English alphabetical and spaces'
                    }
                       
                    }
                },
                      'Username': {
                    validators: {
                     
          remote: {
                        url: '/offlineneew/public/checkUsername',
                        data:function(validator, $field, value)
                        {return{id:validator.getFieldElements('id').val()};},
                        message: 'The Username name is not available',
                        type: 'GET'
                    }, 
             notEmpty:{
                    message:"the Username is required"
                  },
                    regexp: {
                        regexp: /^[\u0600-\u06FF\sa-zA-Z]+$/,
                        message: 'The full Username can only consist of  alphabetical and spaces'
                    }
                       
                    }
                },

                'Email':{
                    validators: {
                     
                      remote: {
                        url: '/offlineneew/public/checkEmailuser',
                        data:function(validator, $field, value)
                        {return{id:validator.getFieldElements('id').val()};},
                        message: 'The Email name is not available',
                        type: 'GET'
                    }, 
             notEmpty:{
                    message:"the Email is required"
                  } 
                    }
                },
                 
            
                 'Telephone':{
                    validators: {
                       remote: {
                        url: '/offlineneew/public/checkTelephoneUser',
                        data:function(validator, $field, value)
                        {return{id:validator.getFieldElements('id').val()};},
                        message: 'The Telephone  is not available',
                        type: 'GET'
                    },
                     
      stringLength: {
        min:10,
                        max: 10,
                        message: 'The Telephone must be between 6 and 11 digits'
                    },
             notEmpty:{
                    message:"the Telephone is required"
                  } 
                    }
                },
                 
                   'Password': {
                    validators: {  
             notEmpty:{
                    message:"the Password is required"
                  } 
                    }
                },
       
                    'District': {
                    validators: {
                     
        
             notEmpty:{
                    message:"the District is required"
                  } 
                    }
                }
            
         
            }
        });


 
         $(".Role").chosen({ 
                   width: '100%',
                   no_results_text: "No Results",
                   allow_single_deselect: true, 
                   search_contains:true, });
 $(".Role").trigger("chosen:updated");

    $('[data-toggle="popover"]').popover({ trigger: "hover" }); 

    $('[data-toggle="popover"]').popover({ trigger: "hover" }); 
 });

</script>



</div>
</section>
@stop
@stop