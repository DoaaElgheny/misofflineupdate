@extends('masternew')
@section('content')
<!DOCTYPE html>
<meta name="csrf-token" content="{{ csrf_token() }}" />

<section class="panel-body">
 <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                   <h2><i class="fa fa-group"></i> Gps Promoter Report</h2> 
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                     
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

    <h1><i class='fa fa-tasks'></i> Report </h1>




<div>
 {!! Form::open(['action' => "AdminGpsController@reportgpsdata",'method'=>'post','id'=>'profileForm']) !!}  

         <fieldset style="    padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;">
  <legend style="width: fit-content;border: 0px;">Enter Date:</legend>
 

 <div class="row" >
                    <div class="form-group">

                      <div class="col-md-4 col-sm-4 col-xs-12 has-feedback">
                        <input type='text' name="Start_Date"  placeholder=" Start Date" class="form-control has-feedback-left Start_Date" id="single_ca"  >


                        <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                         {!!$errors->first('Start_Date','<small class="label label-danger">:message</small>')!!}

                      </div>
                       <div class="col-md-4 col-sm-4 col-xs-12 has-feedback">
                        <input type='text' name="End_Date"  placeholder=" End_Date" class="form-control has-feedback-left End_Date" id="single_cal"  >


                        <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                         {!!$errors->first('End_Date','<small class="label label-danger">:message</small>')!!}

                      </div>

                        <div class="col-md-4 col-sm-4 col-xs-12 has-feedback">
                        {!!Form::select('users[]', (['all' => 'all'] + $users->toArray()) ,null,['class'=>'form-control Users chosen-select ','id' => 'prettify','multiple' => true])!!} 


                        
                         {!!$errors->first('End_Date','<small class="label label-danger">:message</small>')!!}

                      </div>

                      </div>


                      </div>
              <div class="row" style="margin-top: 20px;" align="center" >
                                <div  >
            <input type="submit" class="btn btn-primary" id="Search" value=" Press To Show Report" title="Press To Show Report">
              
            </div>  
            </div>       
                 
</fieldset>
      {!!Form::close() !!}                

</div>


<div class="row" style="margin-top: 20px;">



 
                       <table class="table table-hover table-bordered  dt-responsive nowrap display users" cellspacing="0" width="100%" id="tasks">

  <thead>

     <th>Promoter Name</th>
                     <th> Promoter Goverment</th>
                        <th>  Goverment</th>


          <th>Date</th>
          <th>Time</th>
          <th>Lat</th> 
          <th>Long</th> 
         
          <th>Contractor Code</th> 
             <th>Contractor</th> 
             <th>Contractor Phone</th> 
       
      <!--    <th>Backcheck</th>
          <th>Status</th>  -->
          <th>Type</th>
          <th>Product</th>
                  <th>Sales Product</th>
                   <th>Amount</th>
</thead>

  <tfoot>

     <th>Promoter Name</th>
                     <th> Promoter Goverment</th>
                        <th>  Goverment</th>


          <th>Date</th>
          <th>Time</th>
          <th>Lat</th> 
          <th>Long</th> 
         
          <th>Contractor Code</th> 
             <th>Contractor</th> 
             <th>Contractor Phone</th> 
       
      <!--    <th>Backcheck</th>
          <th>Status</th>  -->
          <th>Type</th>
          <th>Product</th>
                  <th>Sales Product</th>
                   <th>Amount</th>
</tfoot>

  <tbody style="text-align:center;">

    @foreach($reportgps as $value)
    <tr>
   <td> {{ $value->PName}} </td>
   <td> {{ $value->Pgov}} </td>
  <td> {{ $value->Cgov}} </td>
   <td> {{ $value->NDate}} </td>
   <td> {{ $value->NTime}} </td>
   <td> {{ $value->Lat}} </td>
   <td> {{ $value->Long}} </td>
   <td> {{ $value->Contractor_Code}} </td>
   <td> {{ $value->Name}} </td>
   <td> {{ $value->Tele1}} </td>
   <td> {{ $value->GpsType}} </td>
   <td> {{ $value->Cement_Name}} </td>
   <td> {{ $value->Sname}} </td>
   <td> {{ $value->Amount}} </td>
   </tr>
    @endforeach
</tbody>
</table>
 </div>
  </div>
    </div>
  

  </div>

</section>
  

 @section('other_scripts')
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>


  <script type="text/javascript">

var editor;
$(function(){

   $(".Users").chosen({ 
                   width: '100%',
                   no_results_text: "لا توجد نتيجه",
                   allow_single_deselect: true, 
                   search_contains:true, });
 $(".Users").trigger("chosen:updated");
  $('#single_cal').daterangepicker({
        singleDatePicker: true,
        singleClasses: "picker_4",
         locale: {
            format: 'YYYY-MM-DD'
        }
      }, function(start) {
        console.log(start.toISOString(), 'Hadeel');
      });
  $('#single_ca').daterangepicker({
        singleDatePicker: true,
        singleClasses: "picker_4",
         locale: {
            format: 'YYYY-MM-DD'
        }
      }, function(start) {
        console.log(start.toISOString(), 'Hadeel');
      });
var table = $('#tasks').dataTable({

select:true,
responsive: true,
"order":[[0,"asc"]],
'searchable':true,
"scrollCollapse":true,
"paging":true,
"pagingType": "simple",
dom: 'lBfrtip',
buttons: [


{ extend: 'excel', className: 'btn btn-primary dtb' }


]});

 



$('#Search').on('click',function () {
  $.getJSON("/offlineneew/public/reportgpsdata?End_Date="+$('.End_Date').val()+"&Start_Date="+$('.Start_Date').val()+"&Users="+$('.Users').val(),
       function(data) {




table.fnClearTable();

$.each(data, function(index,value) { 
  console.log(value);
table.fnAddData([ value.PName,value.Pgov,value.NDate,value.NTime,value.Lat,value.Long,value.Contractor_Code,value.HR,value.cphone,value.GpsType,value.Cement_Name,value.Name,value.Amount]);  


});
 });
});




   $('.users tfoot th').each(function () {



      var title = $('.users thead th').eq($(this).index()).text();
               
 if($(this).index()>=1 && $(this).index()<=14)
        {

           $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
}
        

    });
  table.columns().every( function () {
  var that = this;
 $(this.footer()).find('input').on('keyup change', function () {
          that.search(this.value).draw();
            if (that.search(this.value) ) {
               that.search(this.value).draw();
           }
        });
      
 
   });
 });
  </script>
@stop
@stop
