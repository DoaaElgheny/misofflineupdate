@extends('masternew')
@section('content')
  <section class="panel-body">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2><i class="fa fa-coffee"></i> Cafes</h2> 
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>             
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
          </ul>
          <div class="clearfix"></div>
        </div>
      <div class="x_content">
        <a type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal1" style="margin-bottom: 20px;"> Add New Cafes </a>
        <!-- model -->
          <div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                  <h4 class="modal-title span7 text-center" id="myModalLabel"><span class="title">Add New Activity</span></h4>
                </div>
                <div class="modal-body">
                  <div class="tabbable"> <!-- Only required for left/right tabs -->
                    <ul class="nav nav-tabs"></ul>
                    <div class="tab-content">
                      <div class="tab-pane active" id="tab1">
                        <div class="row">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                              <div class="x_content">
                                <br />
                                {!! Form::open(['action' => "CafeController@store",'method'=>'post','id'=>'profileForm','class'=>'form-horizontal']) !!} 
                                  <fieldset style="    padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;">
                                    <legend style="width: fit-content;border: 0px;">Main Date:</legend>
                                      <div class="form-group">
                                        <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                                          <input type="text" class="form-control has-feedback-left"  id="Address" name="Address" placeholder="Enter Item Address">
                                          <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                                          <input type="number" class="form-control has-feedback-left"  id="Code" name="Code" placeholder="Enter Code">
                                          <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                                        </div>
                                      </div>
                                      <div class="form-group">
                                        <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                                          <label class="col-xs-3 control-label">Government:</label>
                                          <div class="col-xs-9">
                                            {!!Form::select('Government', ([null => 'Select  Name'] + $Government->toArray()) ,null,['class'=>'form-control chosen-select','id' => 'government'])!!}
                                          </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                                          <label class="col-xs-3 control-label">District:</label>
                                          <div class="col-xs-9">
                                            {!!Form::select('District', ([null => 'Select  Name'] + $District->toArray()) ,null,['class'=>'form-control chosen-select District ','id' => 'prettify'])!!}
                                          </div>
                                        </div>
                                      </div>        
                                  </fieldset>
                                  <div class="ln_solid"></div>
                                  <div class="form-group">
                                    <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                                      <button type="button" class="btn btn-primary " data-dismiss="modal">Cancel</button>
                                      <button class="btn btn-primary" type="reset">Reset</button>
                                      <button type="submit"  name="login_form_submit" class="btn btn-success" value="Save">Submit</button>
                                    </div>
                                  </div>
                                {!!Form::close() !!}                
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <table class="table table-hover table-bordered  dt-responsive cafes" cellspacing="0" width="100%">
            <thead>
              <th>NO</th>
              <th>Code</th>
              <th>Address</th>
              <th>Government</th>
              <th>District</th>
              <th>Longitude</th>
              <th>Latitude </th>
              <th>Process</th>
            </thead>
            <tfoot>
              <th></th>
              <th>Code</th>
              <th>Address</th>
              <th>Government</th>
              <th>District</th>
              <th>Longitude</th>
              <th>Latitude </th>
              
              <th></th>
            </tfoot>
            <tbody style="text-align:center;">
              <?php $i=1;?>
              @foreach($cafe as $cafes)
                <tr>
                  <td>{{$i++}}</td>
                 <td>{{$cafes->Code}}</td>
                  <td><a  class="testEdit" data-type="text" data-name="Adress" data-pk="{{ $cafes->id }}">{{$cafes->Adress}}</a></td>
                  <td><a  class="testEdit"  data-type="select"  data-value="{{$Government}}" data-source ="{{$Government}}"  data-name="Government" data-pk="{{ $cafes->id }}">{{$cafes->Government}}</a></td>
                  <td><a  class="testEdit" data-type="select"  data-value="{{$District}}" data-source ="{{$District}}" data-name="District" data-pk="{{ $cafes->id }}">{{$cafes->District}}</a></td>
                  <td>{{$cafes->Long}}</td>
                  <td>{{$cafes->Lat}}</td>
                  <td><a href="/offlineneew/public/cafe/destroy/{{$cafes->id}}"class="btn btn-default btn-sm" ><span class="glyphicon glyphicon-trash"></span>  </a></td>
                </tr>
              @endforeach  
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </section>
  @section('other_scripts')


<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
  <script>
$(document).ready(function() {
    $('#profileForm')
        .find('[name="Government"]')
            .chosen({
                width: '100%',
                inherit_select_classes: true
            })
            // Revalidate the color when it is changed
            .change(function(e) {

                $('#profileForm').formValidation('revalidateField', 'Government');

  $.getJSON("/government/district/" + $("#government").val(), function(data) {
 
                var $contractors = $(".District");
                   $(".District").chosen("destroy");
                $contractors.empty();
                $.each(data, function(index, value) {
                    $contractors.append('<option value="' + index +'">' + value + '</option>');
                });
                  
        
             $('.District').chosen({
                width: '100%',
                no_results_text:'No Results'
            });
                $(".District").trigger("chosen:updated");
            $(".District").trigger("change");
            });
            }).end()
        .formValidation({
            framework: 'bootstrap',
            excluded: ':disabled',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                Government: {
                    validators: {
                        callback: {
                            message: 'Please choose Government',
                            callback: function(value, validator, $field) {
                                // Get the selected options
                                var options = validator.getFieldElements('Government').val();
                              
                                return (options !== '' );
                            }
                        }
                    }
                }, 'Address': {
                    validators: {
                  

             notEmpty:{
                    message:"the Address is required"
                  }
                    
                  }
                },
                     
                    'Code': {
                    validators: {
                         remote: {
                        url: '/checkCafeCode',
    
                        message:'Code Already Exist',
                        type: 'GET'
                    } 
                    }
                }
               ,

                
                 
          
               'District': {
                    validators: {
                     
        
             notEmpty:{
                    message:"the District is required"
                  } 
                    }
                }
            
         
            }
        });
 $('#promoterform')
        .find('[name="Government"]')
            .chosen({
                width: '100%',
                inherit_select_classes: true
            })
            // Revalidate the color when it is changed
            .change(function(e) {

                $('#promoterform').formValidation('revalidateField', 'Government');

  $.getJSON("../government/district/" + $("#government").val(), function(data) {
 
                var $contractors = $(".district");
                   $(".district").chosen("destroy");
                $contractors.empty();
                $.each(data, function(index, value) {
                    $contractors.append('<option value="' + index +'">' + value + '</option>');
                });
                  
        
             $('.district').chosen({
                width: '100%',
                no_results_text:'No Results'
            });
                $(".district").trigger("chosen:updated");
            $(".district").trigger("change");
            });
            })
            .end()
        .formValidation({
            framework: 'bootstrap',
            excluded: ':disabled',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                Government: {
                    validators: {
                        callback: {
                            message: 'Please choose Government',
                            callback: function(value, validator, $field) {
                                // Get the selected options
                                var options = validator.getFieldElements('Government').val();
                              
                                return (options !== '' );
                            }
                        }
                    }
                },
             
                
                    

     
                 
            
          
                    'District': {
                    validators: {
                     
        
             notEmpty:{
                    message:"the District is required"
                  } 
                    }
                }
              
                  
           
            }
        });
});
</script>

  <script type="text/javascript">

var editor;

  $(document).ready(function(){

     var table= $('.cafes').DataTable({
  
    select:true,
    responsive: true,
    "order":[[0,"asc"]],
    'searchable':true,
    "scrollCollapse":true,
    "paging":true,
    "pagingType": "simple",
      dom: 'lBfrtip',
        buttons: [
           
            
            { extend: 'excel', className: 'btn btn-primary dtb' }
            
            
        ],

fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
$.fn.editable.defaults.send = "always";

$.fn.editable.defaults.mode = 'inline';

$('.testEdit').editable({

      validate: function(value) {
        name=$(this).editable().data('name');
        if(name=="name"||name=="username"||name=="email"||name=="role")
        {
                if($.trim(value) == '') {
                    return 'Value is required.';
                  }
                    }
        if(name=="name"||name=="username")
        {

      var regexp = /^[a-zA-Z\u0600-\u06FF,-][\sa-zA-Z\u0600-\u06FF,-]+ {1}[a-zA-Z\u0600-\u06FF,-][\sa-zA-Z\u0600-\u06FF,-]/;            if (!regexp.test(value)) {
            return 'This field is not valid';
        }
        
        }
        },

    placement: 'right',
    url:'{{URL::to("/")}}/fupdatecafe',
  
     ajaxOptions: {
     type: 'get',
    sourceCache: 'false',
     dataType: 'json'

   },

       params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');

            // console.log(params);
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'This Data Already Exist,Enter Correct Data.';
            } else {
                return response.responseText;
            }
        }
             ,    success:function(response)
        {
            if(response.status==="You do not have permission.")
             {
              return 'You do not have permission.';
              }
              else
              {
                window.location = "{{ url('/cafes') }}";
               
              }
        },



});

}
});

 



   $('.cafes tfoot th').each(function () {



      var title = $('.cafes thead th').eq($(this).index()).text();
               
 if($(this).index()>=1 && $(this).index()<=7)
        {

           $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
}
        

    });
  table.columns().every( function () {
  var that = this;
 $(this.footer()).find('input').on('keyup change', function () {
          that.search(this.value).draw();
            if (that.search(this.value) ) {
               that.search(this.value).draw();
           }
        });
      
 
   });



   
 });
  </script>
@stop
@stop
