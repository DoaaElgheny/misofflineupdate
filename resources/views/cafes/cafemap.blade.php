@extends('masternew')

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCVlcsFShbXwxnPdklmXmHZiqhwL6JBaac&libraries=drawing"></script>

<script type="text/javascript">  
  window.onload = function() {
    ajaxCall();
  };
</script>

@section('content')
<section class="panel-body">
 <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Signs & Coffeshops Map </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                     
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

<div class="panel-body">

<section class="panel panel-primary">
<div class="panel-heading">Coffee Shops</div>

<div class="panel-body">
<div id="map" style=" width: 100%; height: 70%;"></div>
</div>
</section>
<section class="panel panel-primary">
  <div class="panel-heading">Signs</div>

<div class="panel-body">
<div id="Signsmap" style=" width: 100%; height: 70%;"></div>
</div>
</div>
</div>
</div>
</div>
  @section('other_scripts')

<!-- map -->
<script type="text/javascript"> 
var timer;
var map = new google.maps.Map(document.getElementById('map'), {
              zoom: 6,
              center: new google.maps.LatLng(28.000, 32.444),
              mapTypeId: google.maps.MapTypeId.ROADMAP

          });
var markerArr = new Array();
function clearOverlays() {
    if (markerArr) {
      for (i in markerArr) {
        markerArr[i].setVisible(false)
        
      }
    // console.log("clear");
    }
}

// function RefreshMap() {
//   // console.log('RefreshMap');
//   map = new google.maps.Map(document.getElementById('map'), {
//               zoom: 6,
//               center: new google.maps.LatLng(28.000, 32.444),
//               mapTypeId: google.maps.MapTypeId.ROADMAP

//           });
// ajaxCall();
// }
// ajaxCall();
function ajaxCall() {
  var marker,i;
   $.ajax({
        type: "GET",
        url : '{{ url("/getcafemap") }}',
        success: function(response) {
          var jArray = JSON.parse(response);
           console.log(jArray);
           
          var infowindow = new google.maps.InfoWindow();
          console.log(jArray.cafes.length);
                  for (i = 0; i < jArray.cafes.length; i++) {
            
                  marker = new google.maps.Marker({
                      position: new google.maps.LatLng(jArray.cafes[i].Lat, jArray.cafes[i].Long),
                      map: map
                  });
            
           
       var geocoder = new google.maps.Geocoder();
        var latitude = jArray.cafes[i].Lat;
        var longitude = jArray.cafes[i].Long;
        var address_arr = new Array();
        var latLng = new google.maps.LatLng(latitude,longitude);
        geocoder.geocode({       
            latLng: latLng     
          }, 
        function(responses) 
            {     
              if (responses && responses.length > 0) {    
                  address_arr.push(responses[0].formatted_address); 
              } 
              else{     
                  address_arr[i] = 'Not getting address for given latitude and longitude';  
              }   
            }
        );
    google.maps.event.addListener(marker, 'click', (function(marker, i) {
          return function() {           
            infowindow.setContent('Name '+jArray.cafes[i].Name+' <br/> Adress '+ jArray.cafes[i].Adress);
            infowindow.open(map, marker);
          }
        })(marker, i));


    markerArr[i]=marker;
    }
  }});
}





var Signtimer;
var Signsmap = new google.maps.Map(document.getElementById('Signsmap'), {
              zoom: 6,
              center: new google.maps.LatLng(28.000, 32.444),
              mapTypeId: google.maps.MapTypeId.ROADMAP

          });
var markerArr = new Array();
function clearSignOverlays() {
    if (markerArr) {
      for (i in markerArr) {
        markerArr[i].setVisible(false)
        
      }
    // console.log("clear");
    }
}

// function RefreshSignMap() {
//   // console.log('RefreshMap');
//   Signsmap = new google.maps.Map(document.getElementById('Signsmap'), {
//               zoom: 6,
//               center: new google.maps.LatLng(28.000, 32.444),
//               mapTypeId: google.maps.MapTypeId.ROADMAP

//           });

//  ajaxSignCall();
// }
ajaxSignCall();
function ajaxSignCall() {

  var Signmarker,i;
   $.ajax({
        type: "GET",
        url : '{{ url("/allSignsMap") }}',
        success: function(response) {
          var jArray = JSON.parse(response);
         
          
          var infowindow = new google.maps.InfoWindow();
        
                  for (i = 0; i < jArray.Signs.length; i++) {
                  if(jArray.Signs[i].subactivity_id==46)
                  {
                  marker = new google.maps.Marker({
                 position: new google.maps.LatLng(jArray.Signs[i].Latitude,jArray.Signs[i].Longitude),
                      map: Signsmap,
                       icon: '/assets/img/if_MapMarker_Marker_Outside_Azure_88051.png'
                  
                      });
                }
                else if(jArray.Signs[i].subactivity_id==47)
                     {
                  marker = new google.maps.Marker({
                 position: new google.maps.LatLng(jArray.Signs[i].Latitude,jArray.Signs[i].Longitude),
                      map: Signsmap,
                       icon: '/assets/img/if_map-marker_285659 (1).png'
                  
                      });
                }
                else
                     {
                  marker = new google.maps.Marker({
                 position: new google.maps.LatLng(jArray.Signs[i].Latitude,jArray.Signs[i].Longitude),
                      map: Signsmap,
                       icon: '/assets/img/if_Untitled-2-03_536303.png'
                  
                      });
                }
            
           
       var geocoder = new google.maps.Geocoder();
        var longitude = jArray.Signs[i].Longitude;
        var latitude = jArray.Signs[i].Latitude;
        var address_arr = new Array();
        var latLng = new google.maps.LatLng(latitude,longitude);
        geocoder.geocode({       
            latLng: latLng     
          }, 
        function(responses) 
            {     
              if (responses && responses.length > 0) {    
                  address_arr.push(responses[0].formatted_address); 
              } 
              else{     
                  address_arr[i] = 'Not getting address for given latitude and longitude';  
              }   
            }
        );
    google.maps.event.addListener(marker, 'click', (function(marker, i) {
          return function() {           
            infowindow.setContent('Government :'+jArray.Signs[i].Government+'<br/> District :'+jArray.Signs[i].District +' <br/> Status : '+ jArray.Signs[i].Status);
            infowindow.open(Signsmap, marker);
          }
        })(marker, i));


    markerArr[i]=marker;
    }
             for (i = 0; i < jArray.Project.length; i++) {
      
                  marker = new google.maps.Marker({
                 position: new google.maps.LatLng(jArray.Project[i].Latitude,jArray.Project[i].Longitude),
                      map: Signsmap,
                       icon: '/assets/img/if_MapMarker_Marker_Inside_Azure_88048.png'
                  
                  });
            
           
       var geocoder = new google.maps.Geocoder();
        var longitude = jArray.Project[i].Longitude;
        var latitude = jArray.Project[i].Latitude;
        var address_arr = new Array();
        var latLng = new google.maps.LatLng(latitude,longitude);
        geocoder.geocode({       
            latLng: latLng     
          }, 
        function(responses) 
            {     
              if (responses && responses.length > 0) {    
                  address_arr.push(responses[0].formatted_address); 
              } 
              else{     
                  address_arr[i] = 'Not getting address for given latitude and longitude';  
              }   
            }
        );
    google.maps.event.addListener(marker, 'click', (function(marker, i) {
          return function() {           
            infowindow.setContent('Government :'+jArray.Project[i].Government+'<br/> District :'+jArray.Project[i].District +' <br/> Status : '+ jArray.Project[i].Status+' <br/> Trader Name  : '+ jArray.Project[i].Trader_Name +' <br/> Contractor Name : '+ jArray.Project[i].Contractor_Name+' <br/> Project Type  : '+ jArray.Project[i].Project_Type );
            infowindow.open(Signsmap, marker);
          }
        })(marker, i));


    markerArr[i]=marker;
    }
  }});
}


</script>
<!-- 
<style type="text/css">
  .chosen-container .chosen-drop {
    border-bottom: 0;
    border-top: 1px solid #aaa;
    top: auto;
    bottom: 40px;
}

</style> -->





@stop
@stop