<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <title>MIS </title>
    <link rel="stylesheet" href="/tabs/css/reset.css"> <!-- CSS reset -->
  <link rel="stylesheet" href="/tabs/css/style.css"> <!-- Resource style -->
  <link rel="stylesheet" href="/tabs/css/demo.css">

<!-- style -->
 <link href="/assets/css/jquerysctipttop.css" rel="stylesheet" type="text/css"> 

<!-- formvalidation -->
 <link href="/assets/css/jquerysctipttop.css" rel="stylesheet" type="text/css"> 
<!-- query-builder -->

    <link rel="stylesheet" type="text/css" href="/assets/css/query-builder.default.min.css">

<!-- Ionicons -->
  <link rel="stylesheet" href="/assets/css/ionicons.min.css">
<!-- Chosen -->
  <link rel="stylesheet" href="/assets/css/chosen.min.css" />

 <!-- datepicker --> 
<link rel="stylesheet" href="/assets/css/datepicker.min.css" />
<link rel="stylesheet" href="/assets/css/datepicker3.min.css"/>
<!-- buttons.jqueryui --> 
<link rel="stylesheet" type="text/css" href="/assets/css/buttons.jqueryui.min.css">
<!-- bootstrap-horizon --> 

 <link rel="stylesheet" type="text/css" href="/assets/css/bootstrap-horizon.css"> 

    <!-- Bootstrap -->
    <link href="/bower_components/gentelella/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="/bower_components/gentelella/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="/bower_components/gentelella/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="/bower_components/gentelella/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
  
    <!-- bootstrap-progressbar -->
    <link href="/bower_components/gentelella/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- JQVMap -->
    {{--  <link href="/bower_components/gentelella/vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>  --}}
    <!-- bootstrap-daterangepicker -->
    <link href="/bower_components/gentelella/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

<!-- Datatables -->
   <link href="/assets/css/bootstrap-editable.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="/assets/css/datatables.min.css"/>
<!-- <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"/> -->
<link rel="stylesheet" type="text/css" href="/assets/css/dataTables.bootstrap.min.css"/>
<link rel="stylesheet" type="text/css" href="/assets/css/jquery.dataTables.min.css"/>


<link rel="stylesheet prefetch" href="/assets/css/dataTables.responsive.css">
<link rel="stylesheet" type="text/css" href="/assets/css/responsive.bootstrap.min.css"/>
<link rel="stylesheet" href="/assets/formvalidation/css/formValidation.min.css">
    <link href="/bower_components/gentelella/build/css/custom.min.css" rel="stylesheet">

   <link rel="stylesheet" href="/assets/plugins/fullcalendar/fullcalendar.min.css">
  <link rel="stylesheet" href="/assets/plugins/fullcalendar/fullcalendar.print.css" media="print">
<!-- survey -->
    <!-- Custom Theme Style -->
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet prefetch" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <style type="text/css">  

/*     input[type="file"] {
    display: none;
   }*/
.custom-file-upload {
    border: 1px solid #ccc;
    display: inline-block;
    padding: 6px 12px;
    cursor: pointer;
}

#blah{
  
  background: url('{{Auth::user()->Img}}') no-repeat;
  background-size: cover;
  height: 60px;
  width: 60px;

}
 .picture input[type="file"] {
  cursor: pointer;
  display: block;
  height: 100%;
  left: 0;
  opacity: 0 !important;
  position: absolute;
  top: 0;
  width: 100%;
}

img:full-screen 
{
  width: 100%;
}
div:-webkit-full-screen img  { width: 100% }
div:-moz-full-screen img  { width: 100% }
div:-ms-full-screen img   { width: 100% }
div:-o-full-screen img  { width: 100% }
div:full-screen   img   { width: 100% }

</style>

     <!-- validator -->
   
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
      
@include('Notifications')
            <!-- sidebar menu -->
           @include('sidebar')

            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
          @include('menu_footer')
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
    @include('header')

        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main" >
  @yield('content')
        </div>
        <!-- /page content -->

        <!-- footer content -->
    @include('footer')
        <!-- /footer content -->

      </div>
    </div>

    <!-- jQuery -->
    <script src="/bower_components/gentelella/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
<script type="text/javascript" src="/assets/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="/bower_components/gentelella/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="/bower_components/gentelella/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <!-- gauge.js -->
    <script src="/bower_components/gentelella/vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="/bower_components/gentelella/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="/bower_components/gentelella/vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="/bower_components/gentelella/vendors/skycons/skycons.js"></script>
    {{--  <!-- Flot -->
    <script src="/bower_components/gentelella/vendors/Flot/jquery.flot.js"></script>
    <script src="/bower_components/gentelella/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="/bower_components/gentelella/vendors/Flot/jquery.flot.time.js"></script>
    <script src="/bower_components/gentelella/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="/bower_components/gentelella/vendors/Flot/jquery.flot.resize.js"></script>  --}}
    <!-- Flot plugins -->
    {{--  <script src="/bower_components/gentelella/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="/bower_components/gentelella/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="/bower_components/gentelella/vendors/flot.curvedlines/curvedLines.js"></script>  --}}
    <!-- DateJS -->
    <script src="/bower_components/gentelella/vendors/DateJS/build/date.js"></script>
    <!-- JQVMap -->
    <script src="/bower_components/gentelella/vendors/jqvmap/dist/jquery.vmap.js"></script>
    <script src="/bower_components/gentelella/vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
    <script src="/bower_components/gentelella/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="/bower_components/gentelella/vendors/moment/min/moment.min.js"></script>
    <script src="/bower_components/gentelella/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- Datatables -->


<script type="text/javascript" src="/assets/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/assets/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="/assets/js/dataTables.responsive.min.js"></script>
<script type="text/javascript" src="/assets/js/responsive.bootstrap.min.js"></script>
<script type="text/javascript" src="/assets/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="/assets/js/buttons.print.min.js"></script>
<script type="text/javascript" src="/assets/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="/assets/js/buttons.jqueryui.min.js"></script>
<script type="text/javascript" src="/assets/js/jszip.min.js"></script>
<script type="text/javascript" src="/assets/js/pdfmake.min.js"></script> 
  <script type="text/javascript" src="/bower_components/js/fnReloadAjax.js"></script>
<script src="/assets/js/bootstrap-editable.min.js"></script>
    {{--  <script type="text/javascript" src="/bower_components/gentelella/vendors/datatables.net/js/buttons.jqueryui.min.js"></script>  --}}
<script src="/assets/formvalidation/js/formValidation.min.js"></script>
<script src="/assets/formvalidation/js/framework/bootstrap.min.js"></script>
 
    <!-- Custom Theme Scripts -->
    <script src="/bower_components/gentelella/build/js/custom.js"></script>
 
<!-- canvasjs -->

     <script type="text/javascript" src="/assets/js/canvasjs.min.js"></script>

   <!-- datepicker -->

     <script src="/assets/js/bootstrap-datepicker.min.js"></script>
   <!-- extendext -->

     <script src="/assets/js/jQuery.extendext.js"></script>
        <!-- doT -->

<script src="/assets/js/doT.min.js"></script>
   <!-- query-builder -->

<script src="/assets/js/query-builder.min.js"></script>
   <!-- javascript links -->

<script type="text/javascript" src="/assets/js/jquery.validate.js"></script>
<script type="text/javascript" src="/assets/js/additional-methods.js"></script>
<script type="text/javascript" src="/assets/js/fnReloadAjax.js"></script>
<script type="text/javascript" src="/assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="/assets/js/additional-methods.min.js"></script>
<!-- chosen -->

      <script src="/assets/js/chosen.jquery.min.js"></script>
   <!-- formvalidation -->

<script src="/assets/formvalidation/js/formValidation.min.js"></script>
<script src="/assets/formvalidation/js/framework/bootstrap.min.js"></script>
   <!-- tabletoexcel -->
  <!--  Validator -->
   <script src="/bower_components/gentelella/vendors/validator/validator.js"></script>
 <script src="/assets/js/jquery.table2excel.js"></script>
 <!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Slimscroll -->
<script src="/assets/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="/assets/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="/assets/dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="/assets/dist/js/demo.js"></script>
<!-- fullCalendar 2.2.5 -->

<script src="/assets/plugins/fullcalendar/fullcalendar.min.js"></script>
<!-- survey -->
<!-- formvalidation -->
<script type="text/javascript" src="http://static.fusioncharts.com/code/latest/fusioncharts.js"></script>
<script type="text/javascript" src="http://static.fusioncharts.com/code/latest/themes/fusioncharts.theme.fint.js?cacheBust=56"></script>

<script type="text/javascript">
  $(document).ready(function(){
$("#file-uploaduser").on("change", function()
    {


        var files = !!this.files ? this.files : [];

        if (!files.length || !window.FileReader) 
        {
           $("#blah").css("background-image",'url(/assets/dist/img/noimage.jpg) no-repeat');
        } // no file selected, or no FileReader support
 
        else if (/^image/.test( files[0].type)){ // only image file
            var reader = new FileReader(); // instance of the FileReader
            reader.readAsDataURL(files[0]); // read the local file
 
            reader.onloadend = function(){ // set image data as background of div

                $("#blah").css("background-image", "url("+this.result+")");
             $("#profileFormimage").trigger('submit');
            }
        }
    });
})
</script>
<script src="/tabs/js/main.js"></script>
        @yield('other_scripts')
  </body>
</html>
