@extends('masternew')
@section('content')
<style type="text/css">
  input[type="file"] {
    display: none;
}
.custom-file-upload {
    border: 1px solid #ccc;
    display: inline-block;
    padding: 6px 12px;
    cursor: pointer;
}

</style>
  <section class="panel-body">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2><i class="fa fa-tasks"></i>KPI Users</h2> 
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
            <li><a class="close-link"><i class="fa fa-close"></i></a></li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <div class="row">
            <h5>To see salary for this month if you dont import kpi file  you cant see it</h5>
            {!!Form::open(['action'=>'KpiUserController@importKPI','method' => 'post','files'=>true])!!}
              <label for="file-upload" class="custom-file-upload">
                <i class="fa fa-cloud-upload"></i> Custom Upload
              </label>
              <input id="file-upload"  name="file" type="file"/>
              <input  name="submit" value="Import File" class="btn btn-primary" type="submit" />  
            {!!Form::close()!!}  
          </div>
          <div class="row" >
            <div class="col-sm-12 col-md-10 col-md-offset-1 form-horizontal" >
              <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                  <label class="col-xs-3 control-label">Start Date:</label>
                  <div class="col-xs-9">
                    <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                    <input type="text"  placeholder="Date"  class="form-control has-feedback-left ClassDate" name="fromdate" id="fromdate" placeholder="enter Start Date " required>     
                  </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                  <label class="col-xs-3 control-label">End Date:</label>
                  <div class="col-xs-9">
                    <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                    <input type="text" placeholder="Date"   class="form-control has-feedback-left ClassDate" name="todate" id="todate" placeholder="enter End Date " required>
                  </div>
                </div>
              </div> 
              <div class="ln_solid"></div>
              <div class="form-group">
                <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                  <button type="button" onclick="ShowReport()" id="Search" name="login_form_submit" class="btn btn-success">Show Report</button>
                  <label style="color:red" id="warn"></label> 
                </div>
              </div>
            </div>  
          </div>      
          @if(Session::has('error'))
            <div class="alert alert-danger" style="text-align:center;" >
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <strong>Whoops!</strong> {{ Session::get('error') }}<br><br>
            </div>
          @endif
        <table class="table table-hover table-bordered  dt-responsive nowrap  tasks" cellspacing="0" width="100%">
          <thead>
            <th>User Name</th>
            <th>Salary</th>
            <th>Show Details</th>    
          </thead>
          <tfoot>
            <th>User Name</th>
            <th>Salary</th>
            <th>Show Details</th>   
          </tfoot>
          <tbody style="text-align:center;" id="Mytable">
          </tbody>
        </table>
      </div>
    </div>
  </div>
</section>
@section('other_scripts')
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
<script type="text/javascript">
var editor;

  $(document).ready(function(){
    $('.ClassDate').daterangepicker({
        singleDatePicker: true,
        singleClasses: "picker_4",
         locale: {
            format: 'YYYY-MM-DD'
        }
      }, function(start) {
        console.log(start.toISOString(), 'Hadeel');
      });
       $('#profileForm')
        .formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },

            fields: {
               'From_Date': {
                   
                validators: {
                        

             notEmpty:{
                    message:"the name is required"
                  }
                }
                },
                     'End_Date': {
                  
                    validators: {
                        

             notEmpty:{
                    message:"the name is required"
                  }
                        
                    }
                }
            
            },submitHandler: function(form) {
        form.submit();
    }
        }).on('success.form.fv', function(e) {
   // e.preventDefault();
    var $form = $(e.target);

    // Enable the submit button
   
    $form.formValidation('disableSubmitButtons', false);
});
     var table= $('.tasks').DataTable({
  
    select:true,
    responsive: true,
    "order":[[0,"asc"]],
    'searchable':true,
    "scrollCollapse":true,
    "paging":true,
    "pagingType": "simple",
      dom: 'lBfrtip',
        buttons: [
            { extend: 'excel', className: 'btn btn-primary dtb' }
        ],

fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
$.fn.editable.defaults.send = "always";

$.fn.editable.defaults.mode ='inline';

$('.testEdit').editable({

      validate: function(value) {
        name=$(this).editable().data('name');
        if(name=="Name" || name=="Engname")
        {
                if($.trim(value) == '') {
                    return 'Value is required.';
                  }
                    }
      
      
        },

    placement: 'right',
    url:'{{URL::to("/")}}/updateKPI',
  
     ajaxOptions: {
     type: 'get',
    sourceCache: 'false',
     dataType: 'json'

   },

       params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');

            // console.log(params);
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'This Data Already Exist,Enter Correct Data.';
            } else {
                return response.responseText;
            }
        }


});

}
});
 
 $(".Users").chosen({ 
                   width: '100%',
                   no_results_text: "No Results",
                   allow_single_deselect: true, 
                   search_contains:true, });
 $(".Users").trigger("chosen:updated");


    $('[data-toggle="popover"]').popover({ trigger: "hover" }); 
   $('.tasks tfoot th').each(function () {
      var title = $('.item thead th').eq($(this).index()).text();
               
 if($(this).index()>=1 && $(this).index()<=6)
        {

           $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
        }
        

    });
  table.columns().every( function () {
  var that = this;
 $(this.footer()).find('input').on('keyup change', function () {
          that.search(this.value).draw();
            if (that.search(this.value) ) {
               that.search(this.value).draw();
           }
        });
      
    });

 });


function ShowReport()
{


  if($('#fromdate').val() <= $('#todate').val())
  {
   if($('#fromdate').val()!="" && $('#todate').val()!="" )
   {
document.getElementById("warn").innerHTML="";

 $.ajax({
      url: "/offlineneew/public/ShowReportKPI",
        data:{todate:$('#todate').val(),fromdate:$('#fromdate').val()},
      type: "Get",
     success: function(data){
     

       $("#Mytable").empty(); 
    var Newstring="";

   for (var ke in data) {
       if (data.hasOwnProperty(ke)) {

        Newstring+="<tr>";
       
         Newstring+='<td >'+data[ke].Name+'</td>';
        Newstring+='<td>'+data[ke].FinalSalary+'</td>';
         Newstring+='<td><a href="/offlineneew/public/ShowDetailsKpi/'+data[ke].id+'/'+$('#fromdate').val()+'/'+$('#todate').val()+'"class="btn btn-default btn-sm" ><span class="glyphicon glyphicon-plus"></span>  </a></td>';
}
}
   $("#Mytable").append(Newstring);      
         

      }

    }); 
  
}
else

document.getElementById("warn").innerHTML="Please enter Dates";
}
else
document.getElementById("warn").innerHTML="Please enter end date after start date";
 }
 

  </script>
@stop
@stop


