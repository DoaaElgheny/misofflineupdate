@extends('masternew')
@section('content')
  <section class="panel-body">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2><i class="fa fa-tasks"></i>Report Details For Kpi</h2> 
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
            <li><a class="close-link"><i class="fa fa-close"></i></a></li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
        <table class="table table-hover table-bordered  dt-responsive nowrap  display tasks" cellspacing="0" width="100%">
          <thead>
            <th>No</th>
            <th>Name</th>
            <th>Kpi Name</th>
            <th>Weight</th>
            <th>Achieved</th> 
            <th>Target</th> 
            <th>Performance</th>      
          </thead>
          <tfoot>
            <th></th>
            <th>Name</th>
            <th>Kpi Name</th>
            <th>Weight</th>
            <th>Achieved</th> 
            <th>Target</th> 
            <th>Performance</th>      
          </tfoot>
          <tbody style="text-align:center;">
            <?php $i=1;?>
            @foreach($userkpis as $Kpi)
              <tr>
                <td>{{$i++}}</td>
                <td>{{$Kpi->Name}}</td>
                <td>{{$Kpi->kpiname}}</td>
                <td>{{$Kpi->Weight}}</td>    
                <td>{{$Kpi->Achieved}}</td>
                <td>{{$Kpi->Target}}</td>    
                <td>{{$Kpi->Performance}}%</td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</section>
@section('other_scripts')
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
  <script type="text/javascript">
var editor;

  $(document).ready(function(){
      
     var table= $('.tasks').DataTable({
  
    select:true,
    responsive: true,
    "order":[[0,"asc"]],
    'searchable':true,
    "scrollCollapse":true,
    "paging":true,
    "pagingType": "simple",
      dom: 'lBfrtip',
        buttons: [
            { extend: 'excel', className: 'btn btn-primary dtb' }
        ],

fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
$.fn.editable.defaults.send = "always";

$.fn.editable.defaults.mode = 'inline';

$('.testEdit').editable({

      validate: function(value) {
        name=$(this).editable().data('name');
        if(name=="Name" || name=="EngName")
        {
          if($.trim(value) == '') {
          return 'Value is required.';
          }
          }
      
      
        },

    placement: 'right',
    url:'{{URL::to("/")}}/updateKPI',
  
     ajaxOptions: {
     type: 'get',
    sourceCache: 'false',
     dataType: 'json'

   },

       params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');

            // console.log(params);
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'This Data Already Exist,Enter Correct Data.';
            } else {
                return response.responseText;
            }
        }


});

}
});

   $('.tasks tfoot th').each(function () {
      var title = $('.item thead th').eq($(this).index()).text();
               
 if($(this).index()>=1 && $(this).index()<=6)
        {

           $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
        }
        

    });
  table.columns().every( function () {
  var that = this;
 $(this.footer()).find('input').on('keyup change', function () {
          that.search(this.value).draw();
            if (that.search(this.value) ) {
               that.search(this.value).draw();
           }
        });
      
    });

 });

  </script>
@stop
@stop

