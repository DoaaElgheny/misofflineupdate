@extends('masternew')
@section('content')
<style type="text/css">
  input[type="file"] {
    display: none;
}
.custom-file-upload {
    border: 1px solid #ccc;
    display: inline-block;
    padding: 6px 12px;
    cursor: pointer;
}

</style>
  <section class="panel-body">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2><i class="fa fa-tasks"></i>KPI</h2> 
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
            <li><a class="close-link"><i class="fa fa-close"></i></a></li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          {!! Form::open(['action' => "KpiUserController@store",'method'=>'post','id'=>'profileForm','class'=>'form-horizontal']) !!} 
            <div class="form-group">
              <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                <label class="col-xs-3 control-label">Start Date:</label>
                <div class="col-xs-9">
                  <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                  <input type="text"  placeholder="Date"  class="form-control has-feedback-left ClassDate" name="From_Date" id="Start_Date" placeholder="enter Start Date " oninvalid="this.setCustomValidity('Please Enter From Date')" oninput="setCustomValidity('')" required>     
                </div>
              </div>
              <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                <label class="col-xs-3 control-label">End Date:</label>
                <div class="col-xs-9">
                  <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                  <input type="text" placeholder="Date"   class="form-control has-feedback-left ClassDate" name="End_Date" id="End_Date" placeholder="enter End Date " oninvalid="this.setCustomValidity('Please Enter To Date')" oninput="setCustomValidity('')" required>
                </div>
              </div>
            </div>
            <!-- <div class="form-group col-md-12">
              <label for="input_Enname" class="control-label  col-md-3"> KPI Type</label>
              <div class="col-md-9">
                  {!!Form::select('Type_User',
                  array( 'Office Promoters' => 'Office Promoters',
                  'Outdoor Promoters' => 'Outdoor Promoters')
                  ,[null => 'Select Type User name'],['id' => 'Status','name' => 'Type_User','class'=>'chosen-select TypeP form-control']);!!} 
              </div>
            </div> -->
            <div class="form-group">
              <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                <label class="col-xs-3 control-label">Users:</label>
                <div class="col-xs-9">
                  {!!Form::select('Users',($Users->toArray()) ,null,['class'=>'form-control  chosen-select Users ','id' => 'Users'])!!}
                </div>
              </div>
            </div>
            <!-- 
            <div class="form-group col-md-12">
                 <label for="input_point" class="control-label col-md-3"> KPI</label>
                  <div class="col-md-9">
                    {!!Form::select('Kpi[]',($Kpis->toArray()) ,null,['class'=>'form-control  chosen-select Kpi ','id' => 'prettify','multiple' => true])!!}
                        </div>
                    </div> -->
     
            <hr/>
            <table class="table table-bordered table-hover">
              <thead>
                <th> No</th>
                <th>KPI</th>
                <th>Weight</th>
                <th>Target</th>
                <th>Action</th>
              </thead>
              <tbody class="body">
                <!--   <td>{!!Form::select('Kpi[]', ($Kpis->toArray()) ,null,['class'=>'form-control','id' => 'prettify'])!!}</td>

                 -->
                <?php $i=1;?>
                @foreach($Kpis as $Kpi)
                  <tr>
                    <td class="no">{{$i++}}</td>
                    <td>{{$Kpi->Name}} <input type="hidden" name="Kpi[]" value="{{$Kpi->id}}" ></td>
                    <td><input type="number" name="Weight[]" class="form-control" id="W{{$Kpi->id}}" placeholder="Enter Weight" required></td>
                    <td><input type="number" name="Target[]" class="form-control" id="T{{$Kpi->id}}" placeholder="Enter Target" required></td>  
                    <td><a href="#" class="btn btn-danger delete">Delete</a></td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
              <button type="button" class="btn btn-primary " data-dismiss="modal">Cancel</button>
              <button class="btn btn-primary" type="reset">Reset</button>
              <button type="submit"  name="login_form_submit" class="btn btn-success" value="Save">Submit</button>
            </div>
          </div>
        {!!Form::close() !!}
        {!!Form::open(['action'=>'KpiUserController@importkpipromoter','method' => 'post','files'=>true])!!}  
          <label for="file-upload" class="custom-file-upload">
            <i class="fa fa-cloud-upload"></i> Custom Upload
          </label>
          <input id="file-upload"  name="file" type="file"/>
          <input type="submit" name="submit" value="Import File" class="btn btn-primary"/>  
        {!!Form::close()!!}
      </div>
    </div>
  </div>
</section>
@section('other_scripts')
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
     <script type="text/javascript">
$(function(){
          $('.ClassDate').daterangepicker({
        singleDatePicker: true,
        singleClasses: "picker_4",
         locale: {
            format: 'YYYY-MM-DD'
        }
      }, function(start) {
        console.log(start.toISOString(), 'Hadeel');
      });
   $("#Users").on("change",function(e){

    $("input[type=number]").each(function(e) {
          this.value="";
        });
     $.get("{{ url('getuserkpi')}}", 
        { user_id: $(this).val() }, 
        function(data) {
  
          $.each(data, function(k, v) {
            $("#T"+v.kpi_id).val(v.Target);
            $("#W"+v.kpi_id).val(v.Weight);
             
});
            });
        });

 $(".Users").chosen({ 
                   width: '100%',
                   no_results_text: "No Results",
                   allow_single_deselect: true, 
                   search_contains:true, });

 $(".Users").trigger("chosen:updated");

//  $('#profileForm').formValidation({
//             framework: 'bootstrap',
//             icon: {
//                 valid: 'glyphicon glyphicon-ok',
//                 invalid: 'glyphicon glyphicon-remove',
//                 validating: 'glyphicon glyphicon-refresh'
//             },

//             fields: {
//                'From_Date': {
//                     validators: {
                 
//                 notEmpty:{
//                     message:"the Start Date is required"
//                   }
                        
//                     }
//                 },
//                      'End_Date': {
                  
//                     validators: {
                  

//              notEmpty:{
//                     message:"the End Date is required"
//                   }
                        
//                     }
//                 }
//             },submitHandler: function(form) {
//         form.submit();
//     }
//         }).on('success.form.fv', function(e) {
//    // e.preventDefault();
//     var $form = $(e.target);

//     // Enable the submit button
   
//     $form.formValidation('disableSubmitButtons', false);
// });
   




 $('.body').delegate('.delete','click',function(){
  $(this).parent().parent().remove();

 });

});
</script>
@stop
@stop
