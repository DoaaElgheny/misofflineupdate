@extends('masternew')
@section('content')
  <section class="panel-body">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2><i class="fa fa-tasks"></i>KPI</h2> 
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
            <li><a class="close-link"><i class="fa fa-close"></i></a></li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <a type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal1" style="margin-bottom: 20px;"> Add KPI </a>
          <!-- model -->
          <div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                  <h4 class="modal-title span7 text-center" id="myModalLabel"><span class="title">Add KPI</span></h4>
                </div>
                <div class="modal-body">
                  <div class="tabbable"> <!-- Only required for left/right tabs -->
                    <ul class="nav nav-tabs"></ul>
                    <div class="tab-content">
                      <div class="tab-pane active" id="tab1">
                        <div class="row">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                              <div class="x_content">
                                <br />
                                {!! Form::open(['action' => "KpiController@store",'method'=>'post','id'=>'profileForm','class'=>'form-horizontal']) !!} 
                                  <fieldset style="    padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;">
                                    <legend style="width: fit-content;border: 0px;">Main Date:</legend>
                                      <div class="form-group">
                                        <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                                          <input type="text" class="form-control has-feedback-left" id="Name" name="Name" placeholder="enter Name" required>
                                          <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                                          <input type="text" class="form-control has-feedback-left" id="EngName" name="EngName" placeholder="enter EngName" required>
                                          <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                                        </div>
                                      </div>
                                      <div class="form-group">
                                        <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                                          <input type="number" class="form-control has-feedback-left" id="Weight" name="Weight" placeholder="enter  Weight" required>
                                          <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                                          <input type="number"  step="any" class="form-control has-feedback-left" value="0" id="Points" name="Target" placeholder="Target" required>
                                          <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                                        </div>
                                      </div> 
                                      <div class="form-group">
                                        <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                                          <input type="text"  step="any" class="form-control has-feedback-left" id="Code" name="Code" placeholder="Code" required>
                                          <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                                        </div>
                                      </div>        
                                  </fieldset>
                                  <div class="ln_solid"></div>
                                  <div class="form-group">
                                    <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                                      <button type="button" class="btn btn-primary " data-dismiss="modal">Cancel</button>
                                      <button class="btn btn-primary" type="reset">Reset</button>
                                      <button type="submit"  name="login_form_submit" class="btn btn-success" value="Save">Submit</button>
                                    </div>
                                  </div>
                                {!!Form::close() !!}                
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          @if(Session::has('error'))         
            <div class="alert alert-danger" style="text-align:center;" >
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <strong>Whoops!</strong> {{ Session::get('error') }}<br><br>
            </div>
          @endif
        <table class="table table-hover table-bordered  dt-responsive nowrap  tasks" cellspacing="0" width="100%">
          <thead>
            <th>No</th>
            <th>Name</th>
        <!--     <th>EngName</th> -->
            <th>Active</th>
          
            <th>Code</th>
            <th>Process</th> 
          </thead>
          <tfoot>
            <th></th>
            <th>Name</th>
  
            <th>Active</th>

            <th>Code</th>
            <th></th>  
          </tfoot>
          <tbody style="text-align:center;">
            <?php $i=1;?>
            @foreach($Kpis as $Kpi)
              <tr>
                <td>{{$i++}}</td>
                <td ><a  class="testEdit" data-type="text"  data-value="{{$Kpi->Name}}" data-name="Name"  data-pk="{{ $Kpi->id }}">{{$Kpi->Name}}</a></td>
             <!--    <td ><a  class="testEdit" data-type="text"  data-value="{{$Kpi->EngName}}" data-name="EngName"  data-pk="{{ $Kpi->id }}">{{$Kpi->EngName}}</a></td> -->
                <!-- <td ><a  class="testEdit" data-type="number"  data-value="{{$Kpi->Active}}" data-name="Active"  data-pk="{{ $Kpi->id }}">{{$Kpi->Active}}</a></td>  --> 



                                   @if($Kpi->Active == 0)
                                    <td valign="middle"> <span class="label bg-success"><a  href="Active/{{$Kpi->id}}">Active</a></span></td>
                                    @elseif($Kpi->Active == 1)
                                 <td valign="middle"><span class="label bg-danger"><a  href="Active/{{$Kpi->id}}">Not Active</a></span></td>
                                    @endif     
               
                <td ><a  class="testEdit" data-type="text"  data-value="{{$Kpi->Code}}" data-name="Code"  data-pk="{{ $Kpi->id }}">{{$Kpi->Code}}</a></td> 
                <td>

                           <span  data-toggle="tooltip"  title="Delete" data-placement="top" >
                                <a href="/KPI/destroy/{{$Kpi->id}}"  class="btn btn-default btn-sm" 
                                   
                                   data-original-title="Delete"><i class="fa fa-trash-o"></i></a>
                                </span>
</td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</section>
@section('other_scripts')
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
  <script type="text/javascript">
var editor;

  $(document).ready(function(){
          $("[data-toggle=tooltip]").tooltip();

       $('#profileForm')
        .formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },

            fields: {
               'Name': {
                    validators: {
                      remote: {
                        url: '/offlineneew/public/checkNameKPI',
                      
                        message: 'The Name is Already exist',
                        type: 'GET'
                    }, 
                notEmpty:{
                    message:"the name is required"
                  },
                    regexp: {
                        regexp: /^[a-zA-Z\u0600-\u06FF,-][\sa-zA-Z\u0600-\u06FF,-]/,
                        message: 'The full name can only consist of alphabetical and spaces'
                  }
                        
                    }
                },
                     'EngName': {
                  
                    validators: {
                            remote: {
                        url: '/offlineneew/public/checkEngNameKPI',
                      
                        message: 'The English Name is Already exist',
                        type: 'GET'
                    }, 

             notEmpty:{
                    message:"the name is required"
                  },
                    regexp: {
                        regexp: /^[a-zA-Z\u0600-\u06FF,-][\sa-zA-Z\u0600-\u06FF,-]/,
                        message: 'The full name can only consist of alphabetical and spaces'
                    }
                        
                    }
                }
                ,
                     'Code': {
                  
                    validators: {
                            remote: {
                        url: '/offlineneew/public/checkCodeKPI',
                      
                        message: 'The Code is Already exist',
                        type: 'GET'
                    }, 

             notEmpty:{
                    message:"Code is required"
                  }
                        
                    }
                }
            
            },submitHandler: function(form) {
        form.submit();
    }
        }).on('success.form.fv', function(e) {
   // e.preventDefault();
    var $form = $(e.target);

    // Enable the submit button
   
    $form.formValidation('disableSubmitButtons', false);
});
     var table= $('.tasks').DataTable({
  
    select:true,
    responsive: true,
    "order":[[0,"asc"]],
    'searchable':true,
    "scrollCollapse":true,
    "paging":true,
    "pagingType": "simple",
      dom: 'lBfrtip',
        buttons: [
            { extend: 'excel', className: 'btn btn-primary dtb' }
        ],

fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
$.fn.editable.defaults.send = "always";

$.fn.editable.defaults.mode = 'inline';

$('.testEdit').editable({

      validate: function(value) {
        name=$(this).editable().data('name');
        if(name=="Name" || name=="EngName")
        {
          if($.trim(value) == '') {
          return 'Value is required.';
          }
          }
      
      
        },

    placement: 'right',
    url:'{{URL::to("/")}}/updateKPI',
  
     ajaxOptions: {
     type: 'get',
    sourceCache: 'false',
     dataType: 'json'

   },

       params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');

            // console.log(params);
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'This Data Already Exist,Enter Correct Data.';
            } else {
                return response.responseText;
            }
        }


});

}
});

   $('.tasks tfoot th').each(function () {
      var title = $('.item thead th').eq($(this).index()).text();
               
 if($(this).index()>=1 && $(this).index()<=5)
        {

           $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
        }
        

    });
  table.columns().every( function () {
  var that = this;
 $(this.footer()).find('input').on('keyup change', function () {
          that.search(this.value).draw();
            if (that.search(this.value) ) {
               that.search(this.value).draw();
           }
        });
      
    });

 });

  </script>
@stop
@stop

