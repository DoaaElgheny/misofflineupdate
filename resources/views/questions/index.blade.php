@extends('masternew')
@section('content')
<!DOCTYPE html>
<section class="panel-body">
 <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Questions </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                     
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

 


   <a type="button"  class="btn btn-primary" data-toggle="modal" data-target="#myModal1" style="margin-bottom: 20px;"> Add Question </a>
  
     <!-- model -->
 
<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
     <div class="modal-content">
    
          <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                 <h4 class="modal-title span7 text-center" id="myModalLabel"><span class="title">Add New Question</span></h4>
          </div>
    <div class="modal-body">
        <div class="tabbable"> <!-- Only required for left/right tabs -->
        <ul class="nav nav-tabs">
  

        
        </ul>
        <div class="tab-content">
        <div class="tab-pane active" id="tab1">
              <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                 
                  <div class="x_content">
                    <br />

        {!! Form::open(['action' => "QuestionController@store",'method'=>'post','id'=>'profileForm','class'=>'form-horizontal form-label-left 
                    ']) !!}  
         <fieldset style=" padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;">
   <legend style="width: fit-content;border: 0px;">Main Data:</legend>

        
           

      <div class="form-group">


<div class="control-label col-md-6 col-sm-6 col-xs-12">

           <label class="control-label col-md-3 col-sm-3 col-xs-12">Question 
                        </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                              <input type="text" class="form-control" id="Question" name="Question" placeholder="enter  Question" required>
                        </div>
                        </div>

                        <div class="control-label col-md-6 col-sm-6 col-xs-12">

                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Date
                        </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                             <input type="text" class="form-control has-feedback-left" id="Date" name="Date" >
                  <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                        </div>
                        </div>

                   
                   

</div>

<!--     <div class="form-group">


<div class="control-label col-md-6 col-sm-6 col-xs-12">

           <label class="control-label col-md-3 col-sm-3 col-xs-12">Type 
                        </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                      {!!Form::select('Type',array('0' => 'Chosse Type Of Answer','Multi' => 'Multi','Text' => 'Text'),null,
                           ['id'=> 'prettify','class'=>'chosen-select  form-control']);!!}      
                        </div>
                        </div>
                    </div> -->
                         <div class="form-group">

                          <div class="form-group col-xs-12">
                            <div class="col-xs-2" >
                                <button type="button" class="btn btn-default addButton"><i class="fa fa-plus"></i></button>
                            </div>
                           
                            
    
                            <div class="col-xs-4" >
                             <input type="text" class="form-control" id="Answer[0]" name="Question.Answers[0]" placeholder="enter  Answer" required>  
                            </div>
                            
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Right Answer </label>
                  
		                        <div class="col-md-1 col-sm-1 col-xs-4 checkbox" >
		                        	<label>
		                        		<input type="hidden" name="Question.Right[0]" id="first" value="off">
		                        <input type="checkbox"  name="Question.Right[0]" id="Right[0]" class="first" >
		                        </label>
		                        </div>


                            </div>                      
                        
 </div> 
                        <!-- The template containing an email field and a Remove button -->
                        <div class="form-group  hide col-xs-12" id="emailTemplate">
                           <div class="col-xs-2">
               <button type="button" class="btn btn-default removeButton"><i class="fa fa-minus"></i></button>
                            </div>
                           
                           
                              <div class="col-xs-4" >
                            <input type="text" class="form-control" id="Answer" name="Answers" placeholder="enter  Answer" required> 
                                  
                            </div>
                      <label class="control-label col-md-3 col-sm-3 col-xs-12">Right Answer </label>
                        <div class="col-md-1 col-sm-1 col-xs-4 checkbox" >
                        	<label>
                        		<input type="hidden" name="Right" id="first" value="off">
                        <input type="checkbox" class="sec" name="Right" id="Right" class="first" >
                        </label>
</div>
                          </div>

                     

                 
                 

                    

      
                    

                     </fieldset>


   <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                          <button type="button" class="btn btn-primary " data-dismiss="modal">Cancel</button>
                         <button class="btn btn-primary" type="reset">Reset</button>
                          <button type="submit"  name="login_form_submit" class="btn btn-success" value="Save">Submit</button>
                        </div>
                      </div>
  
          {!!Form::close()!!}               
        </div>
                </div>
                   </div>
                </div>
        </div>
        </div>
        </div>

    </div>
 

         </div>
     </div>
  </div>
<!-- ENDModal -->
 
<!-- ENDModal -->



<div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel">
                <div class="x_title">
                  <h2>Questions </h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                   
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">

                	@foreach($Questions as $Question)
                  <div class="dashboard-widget-content">

                    <ul class="list-unstyled timeline widget">
                      <li>
                        <div class="block">
                          <div class="block_content">
                            <h2 class="title">

                                              <a>{{$Question->Question}}</a>
                                          </h2>
                         
                            	@foreach($Question->getUsers as $QuestionName)

                               <li>
                            <div class="byline">
                             <span>{{$QuestionName->pivot->Date}}</span>
                             
                            </div>
                            <div class="message_wrapper">
                             <blockquote class="message">{{$QuestionName->Name}}</blockquote>
                            <h2> {{$QuestionName ->pivot->UserAnswer}}</h2>
                             
                            </div>
                          </li>
                              @endforeach

                          </div>
                        </div>
                      </li>
                    
                     
                  
                    </ul>
                  </div>

                  @endforeach
                </div>
              </div>
            </div>


  </div>
                </div>
              </div>  
    </section>
@section('other_scripts')
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>


  <script type="text/javascript">

var editor;
 bookIndex = 0;


  $(document).ready(function(){


  	 $('.first').iCheck({
  	 	tap:true,
                checkboxClass: 'icheckbox_flat-green',
                radioClass: 'iradio_flat-green'
            });

// For oncheck callback
$('.first').on('ifChecked', function () { $('#first').val(0); })

// For onUncheck callback
$('.first').on('ifUnchecked', function () { $('#first').val(1);})
   

  	$('#Date').daterangepicker({
        singleDatePicker: true,
        singleClasses: "picker_4",
         locale: {
            format: 'YYYY-MM-DD'
        }
      }, function(start) {
        console.log(start.toISOString(), 'Hadeel');
      });

    var AnswerValidators = {
            row: '.col-xs-4',   // The title is placed inside a <div class="col-xs-4"> element
            validators: {
                notEmpty: {
                    message: 'The Answer is required'
                }
            }
        },

     

        bookIndex = 0;

    $('#profileForm')
        .formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                'Question.Answers[0]': AnswerValidators
            }
        })

        // Add button click handler
        .on('click', '.addButton', function() {
            bookIndex++;
            var $template = $('#emailTemplate'),
                $clone    = $template
                                .clone()
                                .removeClass('hide')
                                .removeAttr('id')
                                .attr('data-book-index', bookIndex)
                                .insertBefore($template);

            // Update the name attributes
            $clone
                .find('[name="Answers"]').attr('name', 'Question.Answers[' + bookIndex + ']') .end()
                .find('[name="Right"]')
                   .attr('name', 'Question.Right[' + bookIndex + ']').attr('id', 'Question.Right[' + bookIndex + ']').iCheck({
                    	tap:true,
                   	  checkboxClass: 'icheckbox_flat-green',
                       radioClass: 'iradio_flat-green'
                   }).on('ifChecked', function(event){
   $(this).val("on");
    }).on('ifUnchecked', function(event){
      // $(this).val("off");
    }).end();

            // Add new fields
            // Note that we also pass the validator rules for new field as the third parameter
            $('#profileForm')
                .formValidation('addField', 'Question.Answers[' + bookIndex + ']', AnswerValidators)
                
        })

        // Remove button click handler
        .on('click', '.removeButton', function() {
            var $row  = $(this).parents('.form-group'),
                index = $row.attr('data-book-index');

            // Remove fields

                                $Right = $row.find('[name="Question.Right[' + index + ']"]');



            // Remove element containing the Name
            $row.remove();
            $('#profileForm')
                .formValidation('removeField', $row.find('[name="Question.Answers[' + index + ']"]'),$Right)
              ;

            // Remove element containing the fields
            $row.remove();
        });



     var table= $('.Brands').DataTable({
  
    select:true,
    responsive: true,
    "order":[[0,"asc"]],
    'searchable':true,
    "scrollCollapse":true,
    "paging":true,
    "pagingType": "simple",
      dom: 'lBfrtip',
        buttons: [
           
            
            { extend: 'excel', className: 'btn btn-primary dtb' }
            
            
        ],

fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
$.fn.editable.defaults.send = "always";

$.fn.editable.defaults.mode = 'inline';

$('.testEdit').editable({

      validate: function(value) {
        name=$(this).editable().data('name');
        if(name=="name")
        {
                if($.trim(value) == '') {
                    return 'Value is required.';
                  }
                    }
                        if(name=="name")
        {

           var regexp = /^[\u0600-\u06FFA-Za-z\s]+$/;        

           if (!regexp.test(value)) 
           {
                return 'This field is not valid';
           }
            
         }
      
        },

    placement: 'right',
    url:'{{URL::to("/")}}/updateEnergysources',
  
     ajaxOptions: {
     type: 'get',
    sourceCache: 'false',
     dataType: 'json'

   },

       params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');

            // console.log(params);
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'This Data Already Exist,Enter Correct Data.';
            } else {
                return response.responseText;
            }
        }
        ,
        success:function(response)
        {
            if(response.status==="You do not have permission.")
             {
              return 'You do not have permission.';
              }
        }


});

}
});
   $('.Brands tfoot th').each(function () {
      var title = $('.Brands thead th').eq($(this).index()).text();       
     if($(this).index()>=1 && $(this).index()<=6)
            {

           $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
}
        

    });
  table.columns().every( function () {
  var that = this;
 $(this.footer()).find('input').on('keyup change', function () {
          that.search(this.value).draw();
            if (that.search(this.value) ) {
               that.search(this.value).draw();
           }
        });
      
    });
  $.ajaxPrefilter(function( options, original_Options, jqXHR ) {
    options.async = false;
});



   
 });




  </script>




@stop
@stop