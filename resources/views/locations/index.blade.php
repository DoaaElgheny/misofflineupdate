@extends('masternew')
@section('content')
<!DOCTYPE html>
<meta name="csrf-token" content="{{ csrf_token() }}" />
<section class="panel panel-primary">
  <div class="panel-body">
    <h1><i class='fa fa-tasks'></i>Location</h1>
    <a type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal1" style="margin-bottom: 20px;"> Add New Location</a>
    <!-- model -->
    <div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <h4 class="modal-title span7 text-center" id="myModalLabel"><span class="title">Add New Location</span></h4>
          </div>
          <div class="modal-body">
            {!! Form::open(['action' => "LocationController@storeLocation",'method'=>'post','id'=>'profileForm']) !!}  
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <div class="form-group col-md-12"> 
                <label  class="control-label col-md-3">Location:</label>        
                <div class="col-md-9"> 
                  <input class="form-control" type="text" id="Location" name="Location" placeholder="Location" required/>
                </div>
              </div>
              <div class="form-group col-md-12">
                <label for="input_cement_name" class="control-label col-md-3">Latuitde:</label>
                <div class="col-md-9">
                  <input class="form-control" type="number" id="Lat" step="any" name="Lat" placeholder="Latuitde" />
                </div>
              </div>
              <div class="form-group col-md-12">
                <label for="input_cement_name" class="control-label col-md-3">Langitude:</label>
                <div class="col-md-9">
                  <input class="form-control" type="number" id="Lan" step="any" name="Lan" placeholder="Langitude" />
                </div>
              </div>
              <hr/>
              <button type="submit" class="btn btn-primary "style="float:right" name="login_form_submit" value="Save">Save</button>
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            {!!Form::close()!!}
          </div>
        </div>
      </div>
    </div>
    <!-- ENDModal -->
    <table class="table table-hover table-bordered  dt-responsive nowrap display tasks" cellspacing="0" width="100%">
      <thead>
        <th>No</th>
        <th>Retailer Name</th> 
        <th>Location</th>
        <th>Longitude</th>
        <th>Latitude</th> 
        <th>Process</th>          
      </thead>
      <tfoot>
        <th></th>
        <th>Retailer Name</th> 
        <th>Location</th>
        <th>Longitude</th>
        <th>Latitude</th> 
        <th></th>        
      </tfoot>               
      <tbody style="text-align:center;">
        <?php $i=1;?>
        @foreach($Locations as $Location)
          <tr>
            <td>{{$i++}}</td>   
            <td>{{$Location->HasRetailer->Name}}</td>
            <td><a  class="testEdit" data-type="text" data-name="Location"  data-pk="{{ $Location->id }}">{{$Location->Location }}</a></td> 
            <td><a  class="testEdit" data-type="text" data-name="Long"  data-pk="{{ $Location->id }}">{{$Location->Long}}</a></td> 
            <td><a  class="testEdit" data-type="text" data-name="Lat"  data-pk="{{ $Location->id }}">{{$Location->Lat}}</a></td>
            <td><a href="/cemexmarketingsystem/public/locations/destroy/{{$Location->id}}"class="btn btn-default btn-sm" ><span class="glyphicon glyphicon-trash"></span></a></td>
          </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</section>  
<script type="text/javascript">
  $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });
</script>
<script type="text/javascript">
  var editor;
  $(document).ready(function()
    {   
        $('#profileForm')
        .formValidation({
        framework: 'bootstrap',
        icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
              },
              fields: { 
                        'Long': 
                        {
                          validators: 
                          {
                            numeric:
                            {
                              message: 'The value is not a number'
                            }
                          }
                        },     
                        'Lat': 
                        {
                          validators: 
                          {
                            numeric:
                            {
                              message: 'The value is not a number'
                            }
                          }
                        },
                        'Location':
                        {
                          validators: 
                          {
                            notEmpty:
                            {
                              message:"It is required"
                            },
                            regexp:
                            {
                              regexp: /^[\u0600-\u06FFA-Za-z0-9\.\,\s]+$/,
                              message: 'The Name Licence can only consist of Alphabit'
                            }
                          }
                        }      
              } ,submitHandler: function(form) {
              form.submit();
            }
          })
          .on('success.form.fv', function(e)
          {
            // e.preventDefault();
            var $form = $(e.target);
            // Enable the submit button 
            $form.formValidation('disableSubmitButtons', false);
          });
        var table= $('.tasks').DataTable
        ({
          select:true,
          responsive: true,
          "order":[[0,"asc"]],
          'searchable':true,
          "scrollCollapse":true,
          "paging":true,
          "pagingType": "simple",
          dom: 'lBfrtip',
          buttons: [  
            { extend: 'excel', className: 'btn btn-primary dtb' }      
          ],
          fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull)
          {
            $.fn.editable.defaults.send = "always";
            $.fn.editable.defaults.mode = 'inline';
            $('.testEdit').editable
            ({
              validate: function(value)
              {
                name=$(this).editable().data('name');
                if(name=="Location" || name=="Long" || name== "Lat" )
                {
                  if($.trim(value) == '') 
                  {
                    return 'Value is required.';
                  }
                }
                if(name=="Location")
                {
                  var regexp =/^[\u0600-\u06FFA-Za-z0-9\.\,\s]+$/;    
                  if (!regexp.test(value))
                  {
                    return 'This field is not valid';
                  }               
                }
                if(name=="Long" || name=="Lat")
                {
                  var regexp =/^[0-9\.]+$/;    
                  if (!regexp.test(value))
                  {
                    return 'This field is not valid';
                  }               
                }
              },
              placement: 'right',
              url:'{{URL::to("/")}}/updatelocation',      
              ajaxOptions:
              {
                type: 'get',
                sourceCache: 'false',
                dataType: 'json',
              },
              params: function(params)
              {
                // add additional params from data-attributes of trigger element
                params.name = $(this).editable().data('name');
                // console.log(params);
                return params;
              },
              error: function(response, newValue)
              {
                if(response.status === 500) 
                {
                  return 'This Data Already Exist,Enter Correct Data.';
                }
                else 
                {
                  return response.responseText;
                }
              },
              success:function(response)
              {
                if(response.status==="You do not have permission.")
                {
                  return 'You do not have permission.';
                }
              }
            });
          }
        });
        $('.tasks tfoot th').each(function ()
          {
            var title = $('.tasks thead th').eq($(this).index()).text();             
            if($(this).index()>=1 && $(this).index()<=6)
            {
              $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
            }
          });
        table.columns().every( function ()
        {
          var that = this;
          $(this.footer()).find('input').on('keyup change', function ()
          {
            that.search(this.value).draw();
            if (that.search(this.value) )
            {
              that.search(this.value).draw();
            }
          }); 
        });
    });
</script> 
@stop
