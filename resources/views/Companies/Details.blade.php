@extends('masternew')
@section('content')
<!DOCTYPE html>
<meta name="csrf-token" content="{{ csrf_token() }}" />
<style type="text/css">
.flip {
  -webkit-perspective: 800;
perspective: 800;

    position: relative;
 text-align: center;
}
.flip .card.flipped {
  -webkit-transform: rotatey(-180deg);
    transform: rotatey(-180deg);
}
.flip .card {

  height: 100%;
  -webkit-transform-style: preserve-3d;
  -webkit-transition: 0.5s;
    transform-style: preserve-3d;
    transition: 0.5s;
}
.flip .card .face {

  -webkit-backface-visibility: hidden ;
    backface-visibility: hidden ;
  z-index: 2;
   
}
.flip .card .front {
  position: absolute;
   width: 100%;
  z-index: 1;

}
.flip .card .back {
  -webkit-transform: rotatey(-180deg);
    transform: rotatey(-180deg);
}
  .inner{margin:0px !important;}
</style>

 <section class="panel-body">

 <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                     <h2><i class="fa fa-tasks"></i> Company Details</h2> 
                        <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                     
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

  


<section class="panel panel-primary">
  <div class="">
  <br/>
   <br/>
   <div class="row">
    @foreach($Products as $value)
    <div class="flip" style=" display: inline-block;">
        <div class="card"> 
          <div class="face front" style=" width:300px;height:350px; padding-left:50px "> 
            <div class="well well-sm inner" style="height:50px;padding-bottom:10px  ">{{ $value->Cement_Name }}</div>
              <img src="{{$value->Frontimg}}" style="width:100%;height:310px;">
          </div> 
          <div class="face back"> 
            <div class="well well-sm inner" style=" width:300px;height:350px;background-color:white;">
                  <img src="{{$value->Backimg}}" style="width:100%;height:350px;">
            </div>
          </div>
        </div>   
      </div>
    @endforeach  
    </div>
    <div class="row">
      <br>
        <center>
        <h1 style="color:#3c8dbc;">{{$Companyname->Name}}</h1>
        </center>
        <br>
        <br>
        <div class="col-lg-4">
          <h3 style="color:#3c8dbc;">Front Office:</h3>
          <p>
            @foreach($sellerofices as $value)
                <ul>
                  <b>{{$value->Location}}</b>
                  <li>Telephone:{{$value->Telephone}}</li>
                  <li>Government:{{$value->Government}}</li>
                  <li>District:{{$value->District}}</li>
                  <li>Address:{{$value->Address}}</li>
                </ul>
            @endforeach
          </p>
        </div><!-- col -->
        <div class="col-lg-8">
          <h3 style="color:#3c8dbc; ">Company Activity:</h3>
              @foreach($Activity as $value)
              <div class="row">
                <div class="col-lg-4">
                    <img src="{{$value->Img}}"  width="100%" hieght="100%"/>
                </div>
                <div class="col-lg-6">
                  Government:{{$value->Government}}
                  <br/>
                  District:{{$value->District}}
                  <br/>
                  Duration:{{$value->Duration}}
                  <br/>
                  Start Date:{{$value->Start_date}}
                  <br/>
                  Activity Type:{{$value->Activity_type}}
                  <br/>
                  Description:{{$value->Description}}
                </div>
              </div>
              <br/>
              <br/>
            @endforeach
        </div><!-- col -->

      </div><!-- row -->
    
  </div>
</section>


                  </div>
                </div>
              </div>
 
  @section('other_scripts')


<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>


  <script type="text/javascript">
    $('.flip').click(function(){
        $(this).find('.card').toggleClass('flipped');

    });
  </script>
@stop
@stop
