

  
<a type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal1" style="margin-bottom: 20px;"> Add New Company </a>



     <!-- model -->
 
<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
     <div class="modal-content">
    
          <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                 <h4 class="modal-title span7 text-center" id="myModalLabel"><span class="title">Add New Company</span></h4>
          </div>
    <div class="modal-body">
        <div class="tabbable"> <!-- Only required for left/right tabs -->
        <ul class="nav nav-tabs">
  

        
        </ul>
        <div class="tab-content">
        <div class="tab-pane active" id="tab1">
              <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                 
                  <div class="x_content">
                    <br />
      {!! Form::open(['action' => "CompaniesController@store",'method'=>'post','id'=>'profileForm','class'=>'form-horizontal form-label-left ']) !!}   
             <fieldset style="padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;">
  <legend style="width: fit-content;border: 0px;">Main Date:</legend>
 

 
                    <div class="form-group">

                      <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                        <input type="text" class="form-control has-feedback-left"  id="Name" name="Name" placeholder="Company Name">


                        <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                      </div>
                     

                        <div class="col-md-6 col-sm-6 col-xs-12 has-feedback" >
                          <input type="text" class="form-control has-feedback-left" id="EnName" name="EnName" placeholder="English Name">
                        <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>

                        </div>
                      </div>

                        <div class="form-group" >

                    <div class="col-md-6 col-sm-6 col-xs-12 has-feedback"  >
                        <input type="number" class="form-control has-feedback-left"  id="Code" name="Code" placeholder="Company Code" >


                        <span class="fa fa-tasks form-control-feedback left" aria-hidden="true"></span>
                      </div>
                     

                       
                      </div>
    
                   
                    
                      
</fieldset>
        <fieldset style="    padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;">
  <legend style="width: fit-content;border: 0px;">Locations:</legend>


       

                      
                          <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                          <input type="text" class="form-control has-feedback-left" id ="Address" name="Address" placeholder="Company Address">
                        <span class="fa fa-map-marker form-control-feedback left" aria-hidden="true"></span>
                        </div>
                     
                        <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                          <input type="number" class="form-control has-feedback-left" id ="ProductionLines" name="ProductionLines" placeholder=" Production Lines">
                        <span class="fa fa-map-marker form-control-feedback left" aria-hidden="true"></span>
                        </div>
                      </div>


   <div class="form-group">
                      <div class="control-label col-md-6 col-sm-6 col-xs-12" >
                       
                        
                          <input type="number" class="form-control has-feedback-left" id ="MonthlyCapacity" name="MonthlyCapacity" placeholder="Monthly Capacity">
                          <span class="fa fa-tasks form-control-feedback left" aria-hidden="true"></span>
                       
                     </div>
                       <div class="control-label col-md-6 col-sm-6 col-xs-12"  >

                       
                       
                     {!!Form::select('HeadOfficeGov',([null => 'please chooose HeadOfficeGov'] + $Government->toArray()) ,null,['class'=>'form-control subuser_list chosen-select HeadOfficeGov','id' => 'HeadOfficeGov'])!!} 
                      
                        </div>

                      </div>

 <div class="form-group">
                      <div class="control-label col-md-6 col-sm-6 col-xs-12" >
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Government</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                        {!!Form::select('Government', ([null => 'Select  Government'] + $Government->toArray()) ,null,['class'=>' chosen-select','id' => 'Government'])!!}
                        </div>
                     </div>
                       <div class="control-label col-md-6 col-sm-6 col-xs-12" >

                        <label class="control-label col-md-3 col-sm-3 col-xs-12">District
                        </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                       {!!Form::select('District', ([null => 'Select  District'] + $District->toArray()) ,null,['class'=>'form-control chosen-select District ','id' => 'prettify'])!!}
                        </div>
                        </div>

                      </div>




  
           
                      </fieldset>

           


                       


                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                          <button type="button" class="btn btn-primary " data-dismiss="modal">Cancel</button>
                         <button class="btn btn-primary" type="reset">Reset</button>
                          <button type="submit"  name="login_form_submit" class="btn btn-success" value="submit">Submit</button>
                        </div>
                      </div>

      {!!Form::close() !!}                
        </div>
                </div>
                   </div>
                </div>
        </div>
        </div>
        </div>

    </div>
 

         </div>
     </div>
  </div>
<!-- ENDModal -->
<table class="table table-hover table-bordered  dt-responsive nowrap  Company" cellspacing="0" width="100%">
  <thead>
  <th></th>
             
               <th>Parent Company</th>
               <th>Name</th>
               <th>Code</th>
              <th>Government</th>
              <th>District</th>
              <th>Address</th>
               <th>Head Office Gov</th>
              <th>Head Office Addre</th>
              <th>Production Lines</th>
              <th>Monthly Capacity</th>
               <th>Show Details</th> 
              <th>Process</th>  

</thead>


<tbody style="text-align:center;">
<?php $i=1;?>
@foreach($Companies as $Companies)
    <tr>
    
     <td>{{$i++}}</td>
         <td ><a  class="testEdit" data-type="text"  data-value="{{$Companies->EnName}}" data-name="EnName"  data-pk="{{ $Companies->id }}">{{$Companies->EnName}}</a></td>
          
      <td ><a  class="testEdit" data-type="text"  data-value="{{$Companies->Name}}" data-name="Name"  data-pk="{{ $Companies->id }}">{{$Companies->Name}}</a></td>

   <td >{{$Companies->Code}}</td>

    <td>   <a class="testEdit" data-type="select"  data-value="{{$Companies->Government}}" data-source ="{{$Government}}" data-name="Government"  data-pk="{{$Companies->id}}"> 
 {{$Companies->Government}}
  
  </a></td>

   <td>   <a class="testEdit" data-type="select"  data-value="{{$District}}" data-source ="{{$District}}" data-name="District"  data-pk="{{$Companies->id}}"> 
 {{$Companies->District}}
  
  </a></td>
  <td ><a  class="testEdit" data-type="text"  data-value="{{$Companies->Address}}" data-name="Address"  data-pk="{{ $Companies->id }}">{{$Companies->Address}}</a></td>


  <td ><a  class="testEdit" data-type="select" data-source ="{{$Government}}"  data-value="{{$Companies->HeadOfficeGov}}" data-name="HeadOfficeGov"  data-pk="{{ $Companies->id }}">{{$Companies->HeadOfficeGov}}</a></td>

 <td ><a  class="testEdit" data-type="text"  data-value="{{$Companies->HeadOfficeAddre}}" data-name="HeadOfficeAddre"  data-pk="{{ $Companies->id }}">{{$Companies->HeadOfficeAddre}}</a></td>


<td ><a  class="testEdit" data-type="number"  data-value="{{$Companies->ProductionLines}}" data-name="ProductionLines"  data-pk="{{ $Companies->id }}">{{$Companies->ProductionLines}}</a></td>

 <td ><a  class="testEdit" data-type="text"  data-value="{{$Companies->MonthlyCapacity}}" data-name="MonthlyCapacity"  data-pk="{{ $Companies->id }}">{{$Companies->MonthlyCapacity}}</a></td> 
 <td><a href="/offlineneew/public/ShowCompanyDeteils/{{$Companies->id}}"class="btn btn-default btn-sm" ><span class="glyphicon glyphicon-plus"></span>
       </a></td>
  <td>
     
     <a href="/Companies/destroy/{{$Companies->id}}"class="btn btn-default btn-sm" ><span class="glyphicon glyphicon-trash"></span>
       </a>
</td>


   </tr>
     @endforeach
</tbody>

<tfoot>
  <th></th>
             
               <th>Parent Company</th>
               <th>Name</th>
               <th>Code</th>
              <th>Government</th>
              <th>District</th>
              <th>Address</th>
               <th>Head Office Gov</th>
              <th>Head Office Addre</th>
              <th>Production Lines</th>
              <th>Monthly Capacity</th>
              <th></th>
                            <th></th>

              <!--  <th>Company</th> -->
             
</tfoot>
</table>


             
 
  @section('other_scripts')


<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    
</script>


  <script type="text/javascript">
console.log('KL');
var editor;

  $(document).ready(function(){

    
    $('.HeadOfficeGov').chosen({
                width: '100%',
                no_results_text:'No Results'
            });
                $(".HeadOfficeGov").trigger("chosen:updated");
            $(".HeadOfficeGov").trigger("change");
     $('.District').chosen({
                width: '100%',
                no_results_text:'No Results'
            });
                $(".District").trigger("chosen:updated");
            $(".District").trigger("change");
   
     $('#profileForm')

     .find('[name="Government"]')
            .chosen({
                width: '100%',
                inherit_select_classes: true
            })
            // Revalidate the color when it is changed
            .change(function(e) {

                $('#profileForm').formValidation('revalidateField', 'Government');

  $.getJSON("api/newdropdown?option=" + $("#Government").val(), function(data) {
 
                var $contractors = $(".District");
                   $(".District").chosen("destroy");
                $contractors.empty();
                $.each(data, function(index, value) {
                    $contractors.append('<option value="' + index +'">' + value + '</option>');
                });
                  
        
             $('.District').chosen({
                width: '100%',
                no_results_text:'No Results'
            });
                $(".District").trigger("chosen:updated");
            $(".District").trigger("change");
            });
            }).end()
        .formValidation({
            framework: 'bootstrap',
            excluded: ':disabled',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                 Government: {
                    validators: {
                        callback: {
                            message: 'Please choose Government',
                            callback: function(value, validator, $field) {
                                // Get the selected options
                                var options = validator.getFieldElements('Government').val();
                              
                                return (options !== '' );
                            }
                        }
                    }
                },
                'Name': {
                    validators: {
                        remote: {
                        url: '/offlineneew/public/checkCompanyName',
                        message: 'The company name is not available',
                        type: 'GET'
                    },
                  notEmpty:{
                    message:"the name is required"
                  },
                    regexp: {
                        regexp: /^[\u0600-\u06FF\s]+$/,
                        message: 'The full name can only consist of alphabetical and spaces'
                    }
                    }
                },
                    'EnName': {
                    validators: {
                        remote: {
                        url: '/offlineneew/public/checkCompanyEngName',
                        message: 'The company EnName is not available',
                        type: 'GET'
                    },
                  notEmpty:{
                    message:"the name is required"
                  },
                    regexp: {
                        regexp: /^[A-Za-z\s]+$/,
                        message: 'The full name can only consist of alphabetical and spaces'
                    }
                    }
                },  'Code': {
                    validators: {
                         remote: {
                        url: '/offlineneew/public/checkCompanyCode',
    
                        message:'Company Already Exist',
                        type: 'GET'
                    } 
                    }
                }
               

                ,
                    'Address': {
                    validators: {
               
                  notEmpty:{
                    message:"the Address is required"
                  }
                    }
                }
                ,
                'District': {
                    validators: {
               
                  notEmpty:{
                    message:"the District is required"
                  }
               }
                }
                ,
                    'HeadOfficeGov': {
                    validators: {
               
                   
                    }
                }
         
            } ,submitHandler: function(form) {
        form.submit();
    }
        })
        .on('success.form.fv', function(e) {
   e.preventDefault();
    var $form = $(e.target);

    // Enable the submit button
   
    $form.formValidation('disableSubmitButtons', false);
})
        // Add button click handler
        .on('click', '.addButton', function() {
            var $template = $('#emailTemplate'),
                $clone    = $template
                                .clone()
                                .removeClass('hide')
                                .removeAttr('id')
                                .insertBefore($template),
                $Name    = $clone.find('[name="name[]"]');

            // Add new field
            $('#profileForm').formValidation('addField', $Name);
        })
        $('#profileForm').formValidation('validate');

        // Remove button click handler
       
     var table= $('.Company').DataTable({
  
    select:true,
    responsive: true,
    "order":[[0,"asc"]],
    'searchable':true,
    "scrollCollapse":true,
    "paging":true,
    "pagingType": "simple",
      dom: 'lBfrtip',
        buttons: [
           
            
            { extend: 'excel', className: 'btn btn-primary dtb' }
            
            
        ],

fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
$.fn.editable.defaults.send = "always";

$.fn.editable.defaults.mode = 'inline';

$('.testEdit').editable({

      validate: function(value) {
        name=$(this).editable().data('name');
        if(name=="Address"||name=="Name"||name=="EnName"||name=="Government"||name=="District")
        {
                if($.trim(value) == '') {
                    return 'Value is required.';
                  }
                    }
        if(name=="Name")
        {

           var regexp = /^[\u0600-\u06FF\s]+$/;         

           if (!regexp.test(value)) 
           {
                return 'This field is not valid';
           }
            
         } 
         if(name=="EnName")
        {

           var regexp = /^[A-Za-z\s]+$/;        

           if (!regexp.test(value)) 
           {
                return 'This field is not valid';
           }
            
         }
      
        },

    placement: 'right',
    url:'{{URL::to("/")}}/updateCompanies',
  
     ajaxOptions: {
     type: 'get',
    sourceCache: 'false',
     dataType: 'json'

   },

       params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');

            // console.log(params);
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'This Data Already Exist,Enter Correct Data.';
            } else {
                return response.responseText;
            }
        }
        ,    success:function(response)
        {
            if(response.status==="You do not have permission.")
             {
              return 'You do not have permission.';
              }
        }


});

}
});
     $('.Company tfoot th').each(function () {
      var title = $('.Company thead th').eq($(this).index()).text();
          $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
          
  });

  table.columns().every( function () {
      var that = this;
    $(this.footer()).find('input').on('keyup change', function () {
            that.search(this.value).draw();
          if (that.search(this.value) ) {
              that.search(this.value).draw();
          }
      });     
      });

 });
  </script>

@stop

