@extends('masternew')
@section('content')
  <section class="panel-body">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2><i class="fa fa-tasks"></i>Update ActivityItem</h2> 
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
            <li><a class="close-link"><i class="fa fa-close"></i></a></li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
         {!! Form::open(['action' => "ActivityItemController@updateactivityitemattributes",'method'=>'post','id'=>'profileForm','files'=>'true','class'=>'form-horizontal']) !!}  
            <input id="invisible_id" name="id" type="hidden" value="{{$item[0]->Code}}">
            <fieldset style="    padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;">
              <legend style="width: fit-content;border: 0px;">Main Date:</legend>
              @foreach($item as  $value)
                <div class="form-group">
                  <label class="col-xs-2 control-label">
                    {{$value->Name}}
                  </label>
                  @if($value->Type=="select")
                    @if(strpos($value->Name,"government") !== false)
                      <div class="col-md-10 col-sm-10 col-xs-12">
                        {!!Form::select($value->id,([null => 'please chooose governments'] + $Government->toArray()) ,$value->Value,['class'=>'form-control subuser_list','id' => 'Government','required'=>'required'])!!}
                      </div>
                    @elseif($value->Name=="district")
                      <div class="col-md-10 col-sm-10 col-xs-12">
                        {!!Form::select($value->id,([null => 'please chooose districts'] + $District->toArray()) ,$value->Value,['class'=>'form-control subuser_list','id' => 'District','required'=>'required'])!!}
                      </div>
                    @endif
                  @elseif($value->Type=="image")
                    <div class="col-md-10 col-sm-10 col-xs-12">
                      <label for="file-upload" class="custom-file-upload">
                        <i class="fa fa-cloud-upload"></i> Custom Upload
                      </label>
                      <input id="file-upload"  name="img" type="file"/>
                    </div>
                  @elseif($value->Type=="date")
                    <div class="col-md-10 col-sm-10 col-xs-12">
                      <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                      <input type="text" onchange="myFunction(this)" value="{{$value->Value}}"  name="{{$value->Type}}" id="{{$value->id}}" class="form-control has-feedback-left Dateclass"  required />
                    </div>
                    <input id="hidden_{{$value->id}}" value="{{$value->Value}}" name="{{$value->id}}" type="hidden" >
                  @else
                    <div class="col-md-10 col-sm-10 col-xs-12">
                      <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                      <input type="{{$value->Type}}" onchange="myFunction(this)" value="{{$value->Value}}"  name="{{$value->Type}}" id="{{$value->id}}" class="form-control has-feedback-left"  required />
                    </div>
                    <input id="hidden_{{$value->id}}" value="{{$value->Value}}" name="{{$value->id}}" type="hidden" >
                  @endif
                </div>
              @endforeach
              <div class="form-group">  
                <button type="submit" class="btn btn-primary "style="float:right" name="login_form_submit" value="Save">Save</button>         
              </div>              
            </fieldset>
          {!!Form::close()!!}
        </div>
      </div>
    </div>
  </section>
  @section('other_scripts')
    <script type="text/javascript">
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });
    </script>
    <script>
      function myFunction(e)
      {
        $('#hidden_'+e.id).val(e.value);
      }
      $(document).ready(function(){
        $('.Dateclass').daterangepicker({
          singleDatePicker: true,
          singleClasses: "picker_4",
           locale: {
              format: 'YYYY-MM-DD'
          }
        });

        $('#Government').change(function(){
          $.get("{{ url('api/newdropdown')}}", 
          { option: $(this).val() }, 
          function(data) {
              $('#District').empty(); 
              $.each(data, function(key, element) {
                  $('#District').append("<option value='" + key +"'>" + element + "</option>");
              });
          });
        });

        $('#profileForm')
          .formValidation(
          {
            framework: 'bootstrap',
            icon: 
              {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
              },
            fields: 
              {
                'text': 
                  {
                  
                    validators: 
                      {
                  
                        notEmpty:
                        {
                          message:"the name is required"
                        },
                        regexp: 
                        {
                          regexp: /^[a-zA-Z\u0600-\u06FF,-][\sa-zA-Z\u0600-\u06FF,-]/,
                          message: 'The full name can only consist of alphabetical and spaces'
                        }
                      }
                  },
                'number': 
                  {
                  
                    validators: 
                      {
                        notEmpty:
                          {
                            message:"the number is required"
                          }
                    }
                  },
                'date': 
                  {
                  
                    validators: 
                      {                   
                        notEmpty:
                          {
                            message:"the date is required"
                          }
                      }
                  },
            //  'img': 
            //    {
            //      validators: 
            //        {
            //          notEmpty: 
            //            {
            //              message: 'Please select an image'
            //            }
            //        }
            //    }
              } 
            ,submitHandler: function(form) 
              {
                form.submit();
              }
          })
          .on('success.form.fv', function(e) 
            {
              // e.preventDefault();
              var $form = $(e.target);

              // Enable the submit button
         
              $form.formValidation('disableSubmitButtons', false);
            });
        $(".attribute").chosen({ 
                         width: '100%',
                         no_results_text: "There is no result",
                         allow_single_deselect: true, 
                         search_contains:true, });
        $(".attribute").trigger("chosen:updated");
      });
    </script>
  @stop
@stop

