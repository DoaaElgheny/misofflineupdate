@extends('masternew')
@section('content')
  <section class="panel-body">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2><i class="fa fa-tasks"></i>Sings</h2> 
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
            <li><a class="close-link"><i class="fa fa-close"></i></a></li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <a type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal1" style="margin-bottom: 20px;"> Add New Sings </a>
          <!-- model -->
          <div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                  <h4 class="modal-title span7 text-center" id="myModalLabel"><span class="title">Add New Sings</span></h4>
                </div>
                <div class="modal-body">
                  <div class="tabbable"> <!-- Only required for left/right tabs -->
                    <ul class="nav nav-tabs"></ul>
                    <div class="tab-content">
                      <div class="tab-pane active" id="tab1">
                        <div class="row">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                              <div class="x_content">
                                <br />
                                {!! Form::open(['action' => "SignController@store",'method'=>'post','id'=>'profileForm','class'=>'form-horizontal','files'=>'true']) !!} 
                                  <fieldset style="padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;">
                                    <legend style="width: fit-content;border: 0px;">Main Date:</legend>
                                      <div class="form-group">
                                        <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                                          <input type="number"  step="any" class="form-control has-feedback-left" id="Longitude" name="Longitude" placeholder="Enter Longitude">
                                          <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                                          <input type="number"  step="any" class="form-control has-feedback-left" id="Latitude" name="Latitude" placeholder="Enter Latitude">
                                          <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                                        </div>
                                      </div> 
                                      <div class="form-group">
                                        <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                                          <input type="text" class="form-control has-feedback-left" id="Code" name="Code" placeholder="Enter Code">
                                          <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                                          <input type="text"  class="form-control has-feedback-left" id="Address" name="Address" placeholder="Enter Address">
                                          <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                                        </div>
                                      </div> 
                                      <div class="form-group">
                                        <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                                          <input type="text" class="form-control has-feedback-left" id="Trader_Name" name="Trader_Name" placeholder="Enter Trader Name">
                                          <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                                          <input type="text" class="form-control has-feedback-left" id="Contractor_Name" name="Contractor_Name" placeholder="Enter Contractor Name">
                                          <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                                        </div>
                                      </div> 
                                      <div class="form-group">
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                          <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                          <input type="text" class="form-control has-feedback-left" id="single_cal4" name="Date" placeholder="Date" required>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                                          <input type="text" class="form-control has-feedback-left" id="Project_Type" name="Project_Type" placeholder="Enter Project Type">
                                          <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                                        </div>
                                      </div> 
                                      <div class="form-group">
                                        <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                                          <label class="col-xs-3 control-label">Government:</label>
                                          <div class="col-xs-9">
                                            {!!Form::select('Government', ([null => 'Select  Government'] + $Government->toArray()) ,null,['class'=>'form-control','id' => 'Government'])!!}
                                          </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                                          <label class="col-xs-3 control-label">District:</label>
                                          <div class="col-xs-9">
                                            {!!Form::select('District', ([null => 'Select  District'] ) ,null,['class'=>'form-control','id' => 'District'])!!}
                                          </div>
                                        </div>
                                      </div>
                                      <div class="form-group">
                                        <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                                          <label class="col-xs-3 control-label">Status:</label>
                                          <div class="col-xs-9">
                                            {!!Form::select('Status',array('New' => 'New','Old' => 'Old'),null,['id' => 'prettify','class'=>'form-control']);!!}
                                          </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                                          <label class="col-xs-3 control-label">Photo:</label>
                                          <div class="col-xs-9"> 
                                            <label for="file-upload" class="custom-file-upload">
                                            <i class="fa fa-cloud-upload"></i> Custom Upload
                                            </label>
                                            <input id="file-upload"  name="Img" type="file"/> 
                                          </div>
                                        </div>
                                      </div>        
                                  </fieldset>
                                  <div class="ln_solid"></div>
                                  <div class="form-group">
                                    <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                                      <button type="button" class="btn btn-primary " data-dismiss="modal">Cancel</button>
                                      <button class="btn btn-primary" type="reset">Reset</button>
                                      <button type="submit"  name="login_form_submit" class="btn btn-success" value="Save">Submit</button>
                                    </div>
                                  </div>
                                {!!Form::close() !!}                
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        <table class="table table-hover table-bordered  dt-responsive nowrap  users" cellspacing="0" width="100%">
          <thead>
            <th>No</th>
            <th>Code</th>
            <th>Government</th>
            <th>District</th>
            <th>Img</th>
            <th>Longitude</th>
            <th>Latitude</th>
            <th>Status</th>    
            <th>Date</th>
        
              <th>Trader_Name</th>
              <th>Contractor_Name</th>
              <th>Project_Type</th>
       
            <th>Address</th>
            <th>show Image</th>
            <th> Edit</th>
            <th>Process</th>
          </thead>
          <tfoot>
            <th></th>
            <th>Code</th>
            <th>Government</th>
            <th>District</th>
            <th>Img</th>
            <th>Longitude</th>
            <th>Latitude</th>
            <th>Status</th>    
            <th>Date</th>
        
              <th>Trader_Name</th>
              <th>Contractor_Name</th>
              <th>Project_Type</th>
        
            <th>Address</th>
            <th></th>
            <th></th>
            <th></th>
          </tfoot>
          <tbody style="text-align:center;">
            <?php $i=1;?>
            @foreach($sign as $value)
              <tr>
                <td>{{$i++}}</td>
                <td>{{$value->Code}}</td>
                <td>{{$value->Government}}</td>
                <td>{{$value->District}}</td>
                <td><img src="{{$value->Img}}" height="60" width="60"></td>
                <td>{{$value->Longitude}}</td>
                <td>{{$value->Latitude}}</td>
                <td>{{$value->Status}}</td>
                <td>{{date('d-m-Y', strtotime($value->Date))}}</td>
                  <td>{{$value->Trader_Name}}</td>
                  <td>{{$value->Contractor_Name}}</td>
                  <td>{{$value->Project_Type}}</td>
        
                <td>{{$value->Address}}</td>
                <td>
                  <a href="/offlineneew/public/showallactivityitemsimage/{{$value->id}}"class="btn btn-default btn-sm" ><span class="glyphicon glyphicon-plus"></span></a>
                </td>
                <td>
                  <a href="/offlineneew/public/showallactivityitems/edit/{{$value->id}}"class="btn btn-default btn-sm" ><span class="glyphicon glyphicon-plus"></span></a>
                </td>
                <td>
                   <a href="/offlineneew/public/showallactivityitems/delete/{{$value->id}}"class="btn btn-default btn-sm" ><span class="glyphicon glyphicon-trash"></span></a>
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
        {!!Form::open(['action'=>'SignController@importsign','method' => 'post','files'=>true])!!}
          <input type="file" name="file" class="btn btn-primary"/>
          <input type="submit" name="submit" value="Import File" class="btn btn-primary" style="margin-bottom: 20px;"/>  
        {!!Form::close()!!}
      </div>
    </div>
  </div>
</section>
@section('other_scripts')
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
 <script>
$(document).ready(function() {
         $('#Government').change(function(){
        $.get("{{ url('api/newdropdown')}}", 
        { option: $(this).val() }, 
        function(data) {
            $('#District').empty(); 
            $.each(data, function(key, element) {
                $('#District').append("<option value='" + key +"'>" + element + "</option>");
            });
        });
    });
        $('#profileForm')
        .formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                     'Code': {
                  
                    validators: {
                    remote: {
                        url: '/offlineneew/public/checkCodeSign',
                        data:function(validator, $field, value)
                        {return{id:validator.getFieldElements('id').val()};},
                        message: 'The Code is not available',
                        type: 'GET'
                    },
                  
                  notEmpty:{
                    message:"the Code is required"
                  }
                    }
                },
                     'Government': {
                  
                    validators: {                 
                  notEmpty:{
                    message:"the Government is required"
                  }
                    }
                },
                     'District': {
                  
                    validators: {
                     notEmpty:{
                    message:"the District is required"
                  }
                    }
                },
                     'Longitude': {
                  
                    validators: {
                 
                  notEmpty:{
                    message:"the Longitude  is required"
                  },
                    numric: {
                        message: 'The Longitude must be number'
                    }
                    }
                },
                     'Latitude': {
                  
                    validators: {
                 
                  notEmpty:{
                    message:"the Latitude  is required"
                  },
                    numric: {
                        message: 'The Latitude must be number'
                    }
                    }
                // },
                //      'Address': {
                  
                //     validators: {
                
                //     regexp: {
                //         regexp:/^[\u0600-\u06FFa-zA-Z\s]+$/,
                //         message: 'The Address can only consist of alphabetical and spaces'
                //     }
                //     }
                },
                  
                //      'Contractor_Name': {
                  
                //     validators: {
                 
               
                //     regexp: {
                //         regexp:/^[\u0600-\u06FFa-zA-Z\s]+$/,
                //         message: 'The Contractor Name  can only consist of alphabetical and spaces'
                //     }

                //     }
                // }
                // ,
                //      'Project_Type': {
                  
                //     validators: {
                 
              
                //     regexp: {
                //         regexp: /^[\u0600-\u06FFa-zA-Z\s]+$/,
                //         message: 'The Project Type  can only consist of alphabetical and spaces'
                //     }

                //     }
                // }
                // ,
                     'Img': {
                  
                    validators: {
                 
                  notEmpty:{
                    message:"the Img is required"
                  }
                    }
                }
             
                
            }  ,submitHandler: function(form) {
        form.submit();
    }
        })
        .on('success.form.fv', function(e) {
   // e.preventDefault();
    var $form = $(e.target);

    // Enable the submit button
   
    $form.formValidation('disableSubmitButtons', false);
});
});
</script>

  <script type="text/javascript">

var editor;

  $(document).ready(function(){

     var table= $('.users').DataTable({
  
    select:true,
    responsive: true,
    "order":[[0,"asc"]],
    'searchable':true,
    "scrollCollapse":true,
    "paging":true,
    "pagingType": "simple",
      dom: 'lBfrtip',
        buttons: [
           
            
            { extend: 'excel', className: 'btn btn-primary dtb' }
            
            
        ],

fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
$.fn.editable.defaults.send = "always";

$.fn.editable.defaults.mode = 'inline';

$('.testEdit').editable({

      validate: function(value) {
        name=$(this).editable().data('name');
        },

    placement: 'right',
    url:'{{URL::to("/")}}/fupdate',
  
     ajaxOptions: {
     type: 'get',
    sourceCache: 'false',
     dataType: 'json'

   },

       params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');

            // console.log(params);
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'This Data Already Exist,Enter Correct Data.';
            } else {
                return response.responseText;
            }
        }


});

}
});

 



   $('.users tfoot th').each(function () {



      var title = $('.users thead th').eq($(this).index()).text();
               
 if($(this).index()>=1 && $(this).index()<=4)
        {

           $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
}
        

    });
  table.columns().every( function () {
  var that = this;
 $(this.footer()).find('input').on('keyup change', function () {
          that.search(this.value).draw();
            if (that.search(this.value) ) {
               that.search(this.value).draw();
           }
        });
      
 
   });
    $('[data-toggle="popover"]').popover({ trigger: "hover" }); 

 

    $('[data-toggle="popover"]').popover({ trigger: "hover" }); 
 });
  </script>
@stop
@stop
