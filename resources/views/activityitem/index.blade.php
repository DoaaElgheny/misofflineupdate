@extends('masternew')
@section('content')
  <section class="panel-body">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2><i class="fa fa-tasks"></i>ActivityItem</h2> 
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
            <li><a class="close-link"><i class="fa fa-close"></i></a></li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <input id="subactivity" name="id" type="hidden" value="{{$id}}">
          <a onClick= "Create(this)" class="btn btn-primary" style="margin-bottom: 20px;"> Add New ActivityItem </a>
        <table class="table table-hover table-bordered  dt-responsive nowrap  Brands" cellspacing="0" width="100%">
          <thead>
            <th>No</th>
            <th>Marketing Actitvity Category</th> 
            <th>Actitvity</th>     
            <th>Contractor Count</th>
            <th>Item Code</th> 
            @for($i = 0 ; $i < 1 ; $i++)
              @if(count($fdata)!=0)
                @foreach($fdata[$i] as $key => $value)
                  @if($key!='code' && $key!='itemid'&& $key!='Contractor_Count')
                    <th>{{$key}}</th>
                  @endif 
                @endforeach
              @endif 
            @endfor
            <th>Show All Subscribers</th>
            <th>Show Winner Contractors</th>
            <th>Show Winner Retailers</th>
            <th>Process</th> 
          </thead>
          <tfoot>
            <th></th>
            <th>Marketing Actitvity Category</th> 
            <th>Actitvity</th>
            <th>Contractor Count</th>
            <th>Item Code</th>
            @for($i = 0 ; $i < 1 ; $i++)
              @if(count($fdata)!=0)
                @foreach($fdata[$i] as $key => $value)
                  @if($key!='code' && $key!='itemid'&& $key!='Contractor_Count')
                    <th>{{$key}}</th>
                  @endif 
                @endforeach
              @endif 
            @endfor
            <th></th>
            <th></th>
            <th></th>
            <th></th> 
          </tfoot>
          <tbody style="text-align:center;">
            <?php $i=1;?>
            @foreach($fdata as $item)
              <tr>
                <th>{{$i++}}</th>
                <th>{{$activity_sub->MarktingactitvityName}}</th>
                <th>{{$activity_sub->EngName}}</th>
                @foreach($item as $key => $value)
                  @if( $key=='Image')
                  <th> <img  src="http://10.43.144.19/offlineneew/public{{$value}}" width="50" height="50"/></th>
                    
                  @elseif($key!='itemid')
                   <th>{{$value}}</th>
                  @endif 
                @endforeach 
                <th><a href="/offlineneew/public/showallcontractor/{{$item['itemid']}}"class="btn btn-default btn-sm" ><span class="glyphicon glyphicon-plus"></span></a></th>
                <th><a href="/offlineneew/public/showallwinnercontractor/{{$item['itemid']}}"class="btn btn-default btn-sm" ><span class="glyphicon glyphicon-plus"></span></a></th>
                <th><a href="/offlineneew/public/showallwinnerretailer/{{$item['itemid']}}"class="btn btn-default btn-sm" ><span class="glyphicon glyphicon-plus"></span></a></th>
                <th><a href="/offlineneew/public/activityitem/destroy/{{$item['itemid']}}"class="btn btn-default btn-sm" ><span class="glyphicon glyphicon-trash"></span></a><a href="/offlineneew/public/UpdateItem/{{$item['itemid']}}"class="btn btn-default btn-sm" ><span class="glyphicon glyphicon-pencil"></span></a></th>
              </tr>
            @endforeach  
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@section('other_scripts')
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
<script type="text/javascript">

var editor;

  $(document).ready(function(){

     var table= $('.Brands').DataTable({
  
    select:true,
    responsive: true,
    "order":[[0,"asc"]],
    'searchable':true,
    "scrollCollapse":true,
    "paging":true,
    "pagingType": "simple",
      dom: 'lBfrtip',
        buttons: [
           
            
            { extend: 'excel', className: 'btn btn-primary dtb' }
            
            
        ],

fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
$.fn.editable.defaults.send = "always";

$.fn.editable.defaults.mode = 'inline';

$('.testEdit').editable({

      validate: function(value) {
        name=$(this).editable().data('name');
        if(name=="Name")
        {
                if($.trim(value) == '') {
                    return 'Value is required.';
                  }
                    }
      
        },

    placement: 'right',
    url:'{{URL::to("/")}}/updateSubactivity',
  
     ajaxOptions: {
     type: 'get',
    sourceCache: 'false',
     dataType: 'json'

   },

       params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');

            // console.log(params);
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'This Data Already Exist,Enter Correct Data.';
            } else {
                return response.responseText;
            }
        }


});

}
});
   $('.Brands tfoot th').each(function () {
      var title = $('.Brands thead th').eq($(this).index()).text();       
     if($(this).index()>=1 && $(this).index()<=6)
            {

           $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
}
        

    });
  table.columns().every( function () {
  var that = this;
 $(this.footer()).find('input').on('keyup change', function () {
          that.search(this.value).draw();
            if (that.search(this.value) ) {
               that.search(this.value).draw();
           }
        });
      
    });

 });


function Create(e)
{
 
var id=$('#subactivity').val();
 //alert(id);

    e.href="/offlineneew/public/actattruibute?id="+id;
}

</script>
@stop
@stop
