@extends('masternew')
@section('content')
  <style type="text/css">
    input[type=file] {
      display: block;}
      .card {
      /* Add shadows to create the "card" effect */
      box-shadow: -3px 3px 20px 14px rgba(117, 114, 114);
      transition: 0.3s;
    }
    /* On mouse-over, add a deeper shadow */
    .card:hover {
        box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
    }
  </style>
  <section class="panel-body">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2><i class="fa fa-tasks"></i>Add ActivityItem</h2> 
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
            <li><a class="close-link"><i class="fa fa-close"></i></a></li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <div class="form-group">
            <label for="input_frontimg" class="control-label col-md-3"></label>
            <div class="col-md-12">
              <div style="padding-top: 25px "></div>                     
              <div  class="card" style="padding: 10px;width: 500px;">
                <img  src="/offlineneew/public{{$sign->Img}}"  style="width: 100%;height:500px;" />       
              </div>          
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
@stop
