@extends('masternew')
@section('content')
  <section class="panel-body">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2><i class="fa fa-tasks"></i>Winner Retailers</h2> 
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
            <li><a class="close-link"><i class="fa fa-close"></i></a></li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">       
          <table class="table table-hover table-bordered  dt-responsive nowrap  display tasks" cellspacing="0" width="100%">
            <thead>
              <th>No</th>
              <th>retailer  Code</th>
              <th>Prize</th>
              <th>Quantity</th>   
            </thead>
            <tfoot>
              <th></th>
              <th>retailer  Code</th>
              <th>Prize</th>
              <th>Quantity</th>   
            </tfoot>
            <tbody style="text-align:center;">
              <?php $i=1;?>
              @foreach($retailers as $retailer)
                <tr>
                  <td>{{$i++}}</td>
                  <td>{{$retailer->Code}}</td> 
                  <td>{{$retailer->pivot->Item_Name}}</td>
                  <td>{{$retailer->pivot->Quantity}}</td> 
                </tr>
              @endforeach
            </tbody>
          </table>
          {!!Form::open(['action'=>'ActivityItemController@importwinnercontractor','method' => 'post','files'=>true])!!}
            <label for="file-upload" class="custom-file-upload">
                          <i class="fa fa-cloud-upload"></i> Custom Upload
                          </label>
                          <input id="file-upload"  name="file" type="file"/>
            <input type="submit" name="submit" value="Import File" class="btn btn-primary"/>  
          {!!Form::close()!!}
      </div>
    </div>
  </div>
</section>
@section('other_scripts')
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
  <script type="text/javascript">

var editor;

  $(document).ready(function(){
   
     var table= $('.tasks').DataTable({
  
    select:true,
    responsive: true,
    "order":[[0,"asc"]],
    'searchable':true,
    "scrollCollapse":true,
    "paging":true,
    "pagingType": "simple",
      dom: 'lBfrtip',
        buttons: [
           
            
            { extend: 'excel', className: 'btn btn-primary dtb' }
            
            
        ],

fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
$.fn.editable.defaults.send = "always";

$.fn.editable.defaults.mode = 'inline';



}
});

 



   $('.tasks tfoot th').each(function () {



      var title = $('.tasks thead th').eq($(this).index()).text();
               
 if($(this).index()>=1 && $(this).index()<=3)
        {

           $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
}
        

    });
  table.columns().every( function () {
  var that = this;
 $(this.footer()).find('input').on('keyup change', function () {
          that.search(this.value).draw();
            if (that.search(this.value) ) {
               that.search(this.value).draw();
           }
        });
      
    });
  // table.DataTable()
  //  .columns.adjust()
  //  .responsive.recalc();
    var table1= $('.contractors').DataTable({
    select:true,
    responsive: true,
    "order":[[0,"asc"]],
    'searchable':true,
    "scrollCollapse":true,
    "paging":true,
    "pagingType": "simple",
      dom: 'lBfrtip',
             buttons: [
           
            
            { extend: 'excel', className: 'btn btn-primary dtb' }
            
            
        ],

fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
$.fn.editable.defaults.send = "always";
$.fn.editable.defaults.mode = 'inline';
}
});

   $('.contractors tfoot th').each(function () {



      var title = $('.contractors thead th').eq($(this).index()).text();
               
 if($(this).index()>=1 && $(this).index()<=5)
        {

           $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
}
        

    });
  table1.columns().every( function () {
  var that = this;
 $(this.footer()).find('input').on('keyup change', function () {
          that.search(this.value).draw();
            if (that.search(this.value) ) {
               that.search(this.value).draw();
           }
        });
      
    });
// table1.on( 'responsive-display', function ( e, datatable, row, showHide, update ) {
//     console.log( 'Details for row '+row.index()+' '+(showHide ? 'shown' : 'hidden') );
// } );

 });

  </script>
@stop
@stop
