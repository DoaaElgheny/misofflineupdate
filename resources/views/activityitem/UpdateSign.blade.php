@extends('masternew')
@section('content')
  <section class="panel-body">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2><i class="fa fa-tasks"></i>Update Sings</h2> 
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
            <li><a class="close-link"><i class="fa fa-close"></i></a></li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
            {!! Form::open(['action' => "SignController@Update",'method'=>'post','id'=>'profileForm','class'=>'form-horizontal','files'=>'true']) !!} 
            <input type="hidden" id="id" name="id" value="{{ $sign['id'] }}">
            <fieldset style="padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;">
                <legend style="width: fit-content;border: 0px;">Main Date:</legend>
                  <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                      <input type="number"  step="any" class="form-control has-feedback-left" value="{{ $sign['Longitude'] }}" id="Longitude" name="Longitude" placeholder="Enter Longitude">
                      <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                      <input type="number"  step="any" class="form-control has-feedback-left" value="{{ $sign['Latitude'] }}" id="Latitude" name="Latitude" placeholder="Enter Latitude">
                      <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                    </div>
                  </div> 
                  <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                      <input type="text" class="form-control has-feedback-left" value="{{ $sign['Code'] }}"  id="Code" name="Code" placeholder="Enter Code">
                      <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                      <input type="text"  class="form-control has-feedback-left" id="Address" value="{{ $sign['Address'] }}"  name="Address" placeholder="Enter Address">
                      <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                    </div>
                  </div> 
                  <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                      <input type="text" class="form-control has-feedback-left" id="Trader_Name" value="{{ $sign['Trader_Name'] }}" name="Trader_Name" placeholder="Enter Trader Name">
                      <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                      <input type="text" class="form-control has-feedback-left" id="Contractor_Name" value="{{ $sign['Contractor_Name'] }}" name="Contractor_Name" placeholder="Enter Contractor Name">
                      <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                    </div>
                  </div> 
                  <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                      <input type="text" class="form-control has-feedback-left" id="single_cal4" value="{{ date('dd-mm-yyy',strtotime($sign['Date']))}}" name="Date" placeholder="Date" required>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                      <input type="text" class="form-control has-feedback-left" id="Project_Type" value="{{ $sign['Project_Type'] }}" name="Project_Type" placeholder="Enter Project Type">
                      <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                    </div>
                  </div> 
                  <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                      <label class="col-xs-3 control-label">Government:</label>
                      <div class="col-xs-9">
                        {!!Form::select('Government', ([null => 'Select  Government'] + $Government->toArray()) ,$sign['Government'],['class'=>'form-control','id' => 'Government'])!!}
                      </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                      <label class="col-xs-3 control-label">District:</label>
                      <div class="col-xs-9">
                        {!!Form::select('District', ([null => 'Select  District']+ $District->toArray() ) ,$sign['District'],['class'=>'form-control','id' => 'District'])!!}
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                      <label class="col-xs-3 control-label">Status:</label>
                      <div class="col-xs-9">
                        {!!Form::select('Status',array('New' => 'New','Old' => 'Old'),$sign['Status'],['id' => 'prettify','class'=>'form-control']);!!}
                      </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                      <label class="col-xs-3 control-label">Photo:</label>
                      <div class="col-xs-9">
                        <label for="file-upload" class="custom-file-upload">
                          <i class="fa fa-cloud-upload"></i> Custom Upload
                          </label>
                          <input id="file-upload"  name="Img" type="file"/> 
                      </div>
                    </div>
                  </div>        
              </fieldset>
              <div class="ln_solid"></div>
              <div class="form-group">
                <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                  <button type="submit"  name="login_form_submit" class="btn btn-success" value="Save">Submit</button>
                </div>
              </div>
            {!!Form::close() !!}                
                             
      </div>
    </div>
  </div>
</section>
@section('other_scripts')
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
<script type="text/javascript">

  $(document).ready(function(){
    if ( $('#Pdate')[0].type != 'date' ){
      $('#Pdate').datepicker()
    }
     $('#profileForm')
        .formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                     'Code': {
                  
                    validators: {
                    remote: {
                        url: '/offlineneew/public/checkCodeSign',
                        data:function(validator, $field, value)
                        {return{id:validator.getFieldElements('id').val()};},
                        message: 'The Code is not available',
                        type: 'GET'
                    },
                  
                  notEmpty:{
                    message:"the Code is required"
                  }
                    }
                },
                     'Government': {
                  
                    validators: {                 
                  notEmpty:{
                    message:"the Government is required"
                  }
                    }
                },
                     'District': {
                  
                    validators: {
                     notEmpty:{
                    message:"the District is required"
                  }
                    }
                },
                     'Longitude': {
                  
                    validators: {
                 
                  notEmpty:{
                    message:"the Longitude  is required"
                  },
                    numric: {
                        message: 'The Longitude must be number'
                    }
                    }
                },
                     'Latitude': {
                  
                    validators: {
                 
                  notEmpty:{
                    message:"the Latitude  is required"
                  },
                    numric: {
                        message: 'The Latitude must be number'
                    }
                    }
                }
                  
                //      'Contractor_Name': {
                  
                //     validators: {
                 
               
                //     regexp: {
                //         regexp:/^[\u0600-\u06FFa-zA-Z\s]+$/,
                //         message: 'The Contractor Name  can only consist of alphabetical and spaces'
                //     }

                //     }
                // }
                // ,
                //      'Project_Type': {
                  
                //     validators: {
                 
              
                //     regexp: {
                //         regexp: /^[\u0600-\u06FFa-zA-Z\s]+$/,
                //         message: 'The Project Type  can only consist of alphabetical and spaces'
                //     }

                //     }
                // }
             
             
                
            }  ,submitHandler: function(form) {
        form.submit();
    }
        })
        .on('success.form.fv', function(e) {
   // e.preventDefault();
    var $form = $(e.target);

    // Enable the submit button
   
    $form.formValidation('disableSubmitButtons', false);
});
            $('#Government').change(function(){
        $.get("{{ url('api/newdropdown')}}", 
        { option: $(this).val() }, 
        function(data) {
            $('#District').empty(); 
            $.each(data, function(key, element) {
                $('#District').append("<option value='" + key +"'>" + element + "</option>");
            });
        });
    });
    
});

</script>
@stop
@stop
