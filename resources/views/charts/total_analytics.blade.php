@extends('master')
@section('title') analytics:: @parent @stop
@section('content')



<div class="row">
    <div class="page-header" style="text-align: center;">
        <h3><?php echo $Title; ?></h3>
    </div>

</div>



<section class="panel panel-primary">
<div class="panel-body">

<table class="table table-hover table-bordered  dt-responsive nowrap display gps" cellspacing="0" width="100%">
  	<thead>
  		<tr>
		<th>No</th>
			<th>Pormoter Name</th> 
			<th>Total points</th>
						
			<th>Contractor Name</th>
			<th>Contractor Code</th>		
		</tr>
	</thead>

	<tbody id="tbody" style="text-align:center;">
		<?php $i=1; ?>
		@foreach($name as  $key => $user_gps)
			<tr> 
				<td>{{$i++}}</td>
			    <td>{{$user_gps->getusers->name}}</td>
			   
	
		     				
		 
				<td>{{$key}}</td>	    
			    <td>{{$user_gps->Contractor_Name}}</td>
			    <td>{{$user_gps->Contractor_Code}}</td>
			</tr>    
		@endforeach	    

	</tbody>

	<tfoot>
 		<th>No</th>
			<th>Pormoter Name</th> 
			<th>Total Points</th>
					
			<th>Contractor Name</th>
			<th>Contractor Code</th>
	</tfoot>

</table>



<script type="text/javascript">

  $(document).ready(function(){
    var table= $('.gps').DataTable({ 
     select:true,
    responsive: true,
    "order":[[0,"asc"]],
    'searchable':true,
   	"scrollCollapse":true,
   	"paging":true,
   	"pagingType": "simple",
      dom: 'lBfrtip',
        buttons: [
           
            
            { extend: 'excel', className: 'btn btn-primary dtb' }
            
            
        ],

});


$('.gps tfoot th').each(function () {
    var title = $('.gps thead th').eq($(this).index()).text();
	$(this).html( '<input type="text" placeholder="Search '+title+'" />' );
});

table.columns().every( function () {
  	var that = this;
	$(this.footer()).find('input').on('keyup change', function () {
		that.search(this.value).draw();
		    if (that.search(this.value) ) {
		        that.search(this.value).draw();
		    }
		});     
    });
});
</script>


</section>

@endsection
