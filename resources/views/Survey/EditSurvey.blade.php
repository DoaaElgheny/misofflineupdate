@extends('masternew')
@section('content')
  <section class="panel-body">
  	<style type="text/css">
  		.the-legend 
        {
          border-style: none;
          border-width: 0;
          font-size: 14px;
          color: black;
          line-height: 20px;
          margin-bottom: 0;
          width: auto;
          padding:12px;
          background-color: #73879c
          border: 1px solid #e0e0e0;
          font-weight: bold;
        }
      .the-fieldset 
        {
          border: 1px solid #e0e0e0;
          padding: 10px;
        }
      #blahasd
        {
          background: url('{{$Survey->Logo}}') no-repeat;
          background-size: contain;
        }
      .wizard-card .picture:hover 
        {
          border-color:  #841851;
        }
      .wizard-card .picture 
      {
        width: 200px;
        height: 180px;
        border-radius: 50% ;
      }
      .picture input[type="file"] 
        {
          cursor: pointer;
          display: block;
          height: 50%;
          left: 0;
          opacity: 0 !important;
          position: absolute;
          top: 0;
          width: 50%;
        }
    </style>
    <center><h1> Edit New Survey </h1></center>    
    {!!Form::open(['route' =>['createsurvey.update',$Survey->id],'method' => 'put','id'=>'profileForm','files'=>'true']) !!}
      <fieldset class="the-fieldset"  style="margin-bottom: 15px;">
        <legend class="the-legend">Main Data</legend>     
        <input type="hidden" name="_token" id="token" value="{{csrf_token()}}">
        <div class="col-md-12 ">
       	  <div class="col-md-9 col-sm-12 col-xs-12">
       		  <div class="item form-group col-md-12 col-ms-12 col-xs-12">
        			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="Name">
                Survey Name
                <span class="required">*</span>
        			</label>
        			<div class="col-md-9 col-sm-9 col-xs-12">
        			  <input id="Name" class="form-control" data-validate-words="1" value="{{$Survey->Name}}" name="Name" placeholder="Enter Survey Name" required="required" type="text">
        			</div>
            </div>
            <div class="item form-group col-md-12 col-ms-12 col-xs-12">
      			  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Name">
                Survey Description
                <span class="required"></span>
      			  </label>
        			<div class="col-md-9 col-sm-9 col-xs-12">
        			  <textarea   class="form-control" name="Desc" rows="5">
                  {{$Survey->Description}}
                </textarea>
        			</div>
            </div>
          </div>
          <div class="col-md-3 ">
            <div class="col-sm-12  wizard-card">
            	<div class="picture-container" >
            		<div class="picture" id="blahasd" style="margin-bottom: 10px;" >
    				      <input  type="file" id="file-uploadEdit"  name="pic" type="file"/>
    				    </div>  
    		      </div>
        		</div>  
         	</div>
        </div>
      </fieldset>
      <fieldset class="the-fieldset"  style="margin-bottom: 15px;">
        <legend class="the-legend">Survey Date</legend>
        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
        <div class="col-md-12 col-sm-12 col-xs-12">
     	    <div class="col-md-6 col-sm-12 col-xs-12 has-feedback">
      			<label class="control-label col-md-2 col-sm-12 col-xs-12" for="Name">
              Start Date
              <span class="required">*</span>
      			</label>
      			<div class="col-md-6 col-sm-12 col-xs-12 ">
        			<input type='text' name="Start_Date" value="{{$Survey->StartDate}}"  id="Start_Date" placeholder=" Start Date" class="form-control has-feedback-left Start_Date"  >
        			<span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
        			{!!$errors->first('Start_Date','<small class="label label-danger">:message</small>')!!}
      			</div>            
          </div>
          <div class="col-md-6 col-sm-12 col-xs-12 ">
    			  <label class="control-label col-md-2 col-sm-12 col-xs-12" for="Name">
              End Date
              <span class="required">*</span>
    			  </label>
      			<div class="col-md-6 col-sm-12 col-xs-12 has-feedback">
        			<input type='text' name="End_Date" value="{{$Survey->EndDate}}" id="End_Date" placeholder=" End Date" class="form-control has-feedback-left End_Date"  >
        			<span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
        			{!!$errors->first('End_Date','<small class="label label-danger">:message</small>')!!}
      			</div>         
          </div>                           
        </div>
      </fieldset>
      <div class="col-md-6 col-sm-6 col-xs-6" style="margin-left:25% ;margin-top: 30px;">
      	<button type="submit" style="background-color: #1abb9c" class="btn btn-primary btn-block">
          Edit Survey
        </button>
      </div>
    {!!Form::close()!!}
  </section>
  @section('other_scripts')
    <script>
      var editor;
      $(document).ready(function(){
        $('#Start_Date').daterangepicker({
            singleDatePicker: true,
            singleClasses: "picker_4",
             locale: {
                format: 'YYYY-MM-DD'
            }
          });
        $('#End_Date').daterangepicker({
            singleDatePicker: true,
            singleClasses: "picker_4",
             locale: {
                format: 'YYYY-MM-DD'
            }
          });
      	//change imgage    
        $("#file-uploadEdit").on("change", function()
        {
          var files = !!this.files ? this.files : [];
          if (!files.length || !window.FileReader) 
            {
               $("#blah").css("background-image",'url(assets/img/noimage.jpg)');
            } // no file selected, or no FileReader support
          else if (/^image/.test( files[0].type))
            { // only image file
              var reader = new FileReader(); // instance of the FileReader
              reader.readAsDataURL(files[0]); // read the local file
              reader.onloadend = function()
                { // set image data as background of div
                  $("#blahasd").css("background-image", "url("+this.result+")");
                }
            }
        });
        /////////////////////
        $('#profileForm')
        .formValidation({
            framework: 'bootstrap',
            excluded: ':disabled',
            icon: 
              {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
              },
            fields: 
              {
                'Name': 
                  {
                 
                    validators: 
                      {
                        notEmpty:
                          {
                            message:"Please Enter Name"
                          }
                      }
                  }
              }  
        });
    });
  </script>        <!-- /page content -->
@stop
@stop