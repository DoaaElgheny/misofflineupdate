@extends('masternew')
@section('content')
  <section class="panel-body">
    <style type="text/css">
      .the-legend {
    border-style: none;
    border-width: 0;
    font-size: 14px;
    color: black;
    line-height: 20px;
    margin-bottom: 0;
    width: auto;
   padding:12px;
   background-color: #73879c
    border: 1px solid #e0e0e0;
font-weight: bold;
}
.the-fieldset {
    border: 1px solid #e0e0e0;
    padding: 10px;
}
    #blah{
  background: url('assets/img/noimage.jpg') no-repeat;
  background-size: cover;

}
 .wizard-card .picture:hover {
  border-color:  #841851;
}
  .wizard-card .picture 
   {
         
        width: 200px;
    height: 180px;
         border-radius: 50%;
         
    }
    .picture input[type="file"] {
  cursor: pointer;
  display: block;
  height: 100%;
  left: 0;
  opacity: 0 !important;
  position: absolute;
  top: 0;
  width: 100%;
}
</style>
<center><h1> Assign your Survey </h1></center>
    

 <fieldset class="the-fieldset"  style="margin-bottom: 15px;">
<legend class="the-legend">People Emails</legend>


 
 {!!Form::open(['action'=>"SurveyController@AddUsertoServey",'method'=>'post','id'=>'profileForm','files'=>'true']) !!}
  <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
   <input type="hidden" name="id" id="id" value="{{$id}}">
  
 <div class="col-md-12 ">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="item form-group col-md-12 col-ms-12 col-xs-12">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Name">Email<span class="required">*</span>
      </label>
       <div class="form-group col-xs-12">
                    <div class="col-xs-4" >
                                <input class="form-control inventorytexbox" type="email" name="Email[]" id="Email[]" placeholder="Enter Email"   />
                            </div>
                            <div class="col-xs-4" >
                                <input class="form-control inventorytexbox" type="number" name="Tel[]" id="Tel[]" placeholder="Enter Tel"   />
                            </div>
                            <div class="col-xs-2" >
                            <button type="button" class="btn btn-default addButton"><i class="fa fa-plus"></i></button>
                            </div>
                           
                          
                           
                        </div>

                        <!-- The template containing an email field and a Remove button -->
                        <div class="form-group hide col-xs-12" id="emailTemplate">
                          <div class="col-xs-4" >
                                   <input class="form-control inventorytexbox" type="email" name="Email[]" id="Email[]" placeholder="Enter Email" />

                            </div>
                            <div class="col-xs-4" >
                                   <input class="form-control inventorytexbox" type="number" name="Tel[]" id="Tel[]" placeholder="Enter Telephone" />
                                   
                            </div>
                           <div class="col-xs-2" >
                                <button type="button" class="btn btn-default removeButton"><i class="fa fa-minus"></i></button>
                            </div>
                           
                            
                            
        </div>
       
  </div>
  
 </div>
 </div>



<div class="col-md-6 col-sm-6 col-xs-6" style="margin-left:25% ;margin-top: 30px;">
  <button type="submit" style="background-color: #1abb9c" class="btn btn-primary btn-block">
  Assgin Survey</button>

</div>

{!!Form::close()!!}
</fieldset>

  @section('other_scripts')
    <script>
    var editor;

  $(document).ready(function(){

 $('#profileForm').on('click', '.addButton', function() {
            var $template = $('#emailTemplate'),
                $clone    = $template
                                .clone()
                                .removeClass('hide')
                                .removeAttr('id')
                                .insertBefore($template),
                $email    = $clone.find('[name="Email[]"]'),
                $Tel    = $clone.find('[name="Tel[]"]');

            // Add new field
            $('#profileForm').formValidation('addField', $email).formValidation('addField', $Tel);
        })

        // Remove button click handler
        .on('click', '.removeButton', function() {
            var $row   = $(this).closest('.form-group'),
                $email = $row.find('[name="Email[]"]'),
                 $Tel    = $row.find('[name="Tel[]"]');
            // Remove element containing the email
            $row.remove();

            // Remove field
            $('#profileForm').formValidation('removeField', $email).formValidation('removeField', $Tel);
        });


    
 });
    </script>
        <!-- /page content -->
@stop

@stop