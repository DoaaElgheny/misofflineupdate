@extends('masternew')
@section('content')
 <section class="panel-body">


 <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <center> <h1 >All your Users </h1></center>
                    
                        <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class=""></i></a>
                      </li>
                     
                      <li><a class="close-link"><i class=""></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
<!-- <h2> <a href="/ShowDashboard" class="btn btn-link">Dashboard</a>/All User</h2> 
 <a href="/createsurvey" data-toggle="tooltip" title="Add New Survey"><i class="fa fa-plus-square" aria-hidden="true" style="font-size: 3em;padding-bottom:20px; " ></i></a> 
 -->





                   
                    <table class="table table-hover table-bordered  dt-responsive nowrap Brands" cellspacing="0" width="100%" >
  <thead>
   <th> </th>
          <th> Email </th>
          <th>Telephone</th>
         <!--  <th>Process</th> -->
         
          </thead>
          <tfoot>
             <th> </th>
          <th> Email </th>
          <th>Telephone</th>
          <!-- 
          <th>Process</th>  -->
</tfoot>


<tbody style="text-align:center;">
<?php $i=1;?>
 @foreach($AllUserAnswer as $Value)

 @if( $Value['Aswer']  == 1)
    <tr>
    <td>{{$i++}}</td> 
     
      <td > {{$Value['Email']}}</td>

       <td >
        {{$Value['Tele']}}  

       </td>
<!-- 
     <td>

<a  href="/offlineneew/public/DeleteSurvey/{{$Value['id']}}" class="btn btn-danger"><i class="fa fa-trash-o" aria-hidden="true"></i> </a>

     </td> -->


   </tr>
@else
    <tr style="background-color: #FFCCCB">
    <td>{{$i++}}</td> 
     
      <td > {{$Value['Email']}}</td>

       <td >
        {{$Value['Tele']}}  

       </td>
  


     <!-- <td>

<a  href="/offlineneew/public/DeleteSurvey/{{$Value['id']}}" class="btn btn-danger"><i class="fa fa-trash-o" aria-hidden="true"></i> </a>

     </td>
 -->

 

   </tr>
@endif 
     
  
     @endforeach
</tbody>
</table>


                  </div>
                </div>
              </div>
 
  @section('other_scripts')


<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>



 <script type="text/javascript">

var editor;

  $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip(); 
     $('#profileForm')
        .formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
           fields: {
                'Name': {
                 
                    validators: {
                            remote: {
                        url: '/offlineneew/public/checknamebrand',
                        
                        message: 'هذه الماركة مدخله من قبل',
                        type: 'GET'
                    },

             notEmpty:{
                    message:"يجب ادخال الاسم"
                  },
                    regexp: {
                        regexp: /^[\u0600-\u06FF\s0-9\_]+$/,
                        message: 'يجب ادخال حروف عربي وارقام فقط'
                    }
                  }
                },
                     'EngName': {
                  
                    validators: {
                            remote: {
                        url: '/offlineneew/public/checkEngNamebrand',
                      
                        message: 'هذه الماركه مدخله من قبل ',
                        type: 'GET'
                    }, 

             // notEmpty:{
             //        message:"يجب ادخال الاسم"
             //      },
                    regexp: {
                        regexp: /^[a-zA-Z\s0-9\_]+$/,
                        message: 'يجب ادخال حروف انجليزيه وارقام فقط '
                    }
                        
                    }
                }
            }  
});
       
     var table= $('.Brands').DataTable({
  
    select:true,
    responsive: true,
    "order":[[0,"asc"]],
    'searchable':true,
    "scrollCollapse":true,
    "paging":true,
    "pagingType": "simple",
      dom: 'lBfrtip',
        buttons: [
           
            
            { extend: 'excel', className: 'btn btn-primary dtb' }
            
            
        ],

fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
$.fn.editable.defaults.send = "always";

$.fn.editable.defaults.mode = 'inline';

$('.testEdit').editable({

      validate: function(value) {
        name=$(this).editable().data('name');
        if(name=="Name" )
        {
                if($.trim(value) == '') {
                    return 'يجب ادخال القيمة.';
                  }
                    }
                              if(name=="Name")
        {

           var regexp = /^[\u0600-\u06FF\s0-9\_]+$/;          

           if (!regexp.test(value)) 
           {
                return 'هذا الادخال غير صحيح';
           }
            
         } 
            if(name=="EngName")
        {

           var regexp = /^[a-zA-Z\s0-9\_]+$/;          

           if (!regexp.test(value)) 
           {
                return 'هذا الادخال غير صحيح .';
           }
            
         }
      
        },

    placement: 'right',
    url:'{{URL::to("/")}}/updateBrands',
  
     ajaxOptions: {
     type: 'get',
    sourceCache: 'false',
     dataType: 'json'

   },

       params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');

            // console.log(params);
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'This Data Already Exist,Enter Correct Data.';
            } else {
                return response.responseText;
            }
        }
        ,    success:function(response)
        {
            if(response.status==="You do not have permission.")
             {
              return 'You do not have permission.';
              }
        }


});

}
});
   $('.Brands tfoot th').each(function () {
      var title = $('.Brands thead th').eq($(this).index()).text();       
     if($(this).index()>=1 && $(this).index()<=2)
            {

           $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
}
        

    });
  table.columns().every( function () {
  var that = this;
 $(this.footer()).find('input').on('keyup change', function () {
          that.search(this.value).draw();
            if (that.search(this.value) ) {
               that.search(this.value).draw();
           }
        });
      
    });

 });



  </script>
@stop

@stop
