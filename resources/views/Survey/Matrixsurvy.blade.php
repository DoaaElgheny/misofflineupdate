@extends('masternew')
@section('content')

  <style type="text/css">
  body {
  padding: 0;
  margin: 10px 0;
}

.save-all-wrap{text-align:center;padding:20px;}
  	
  		.the-legend {
    border-style: none;
    border-width: 0;
    font-size: 14px;
    color: black;
    line-height: 20px;
    margin-bottom: 0;
    width: auto;
   padding:12px;
   background-color: #73879c
    border: 1px solid #e0e0e0;
font-weight: bold;
}
.the-fieldset {
    border: 1px solid #e0e0e0;
    padding: 10px;
}
    #blah{
  background: url('assets/img/noimage.jpg') no-repeat;
  background-size: cover;

}
 .wizard-card .picture:hover {
  border-color:  #841851;
}
  .wizard-card .picture 
   {
         
        width: 200px;
    height: 180px;
         border-radius: 50%;
         
    }
    .picture input[type="file"] {
  cursor: pointer;
  display: block;
  height: 100%;
  left: 0;
  opacity: 0 !important;
  position: absolute;
  top: 0;
  width: 100%;
}
.form-horizontal .control-label {
    text-align: left;
}

</style>
 <section class="panel-body">
 <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2> </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                     
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
{!!Form::open(['action'=>"QuestionController@storequestion",'method'=>'post','id'=>'profileForm','class'=>'form-horizontal form-label-left','files'=>'true','novalidate']) !!}
 <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
  <input type="hidden" name="Survey_id" id="Survey_id" value="{{$id}}">

<div id="form-builder-pages">

       
  <ul id="tabs" style="background: transparent;border: 0">

    <li><a href="#page-1">Page 1</a></li>
    <li id="add-page-tab"><a href="#new-page">+ Page</a></li>
  </ul>

  <div id="page-1" class="fb-editor">
 <fieldset class="the-fieldset"  style="  padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;">
         <legend class="the-legend" style="width: fit-content;border: 0px;">General</legend>

					  <div class="form-group col-xs-12">

					              <div class="col-md-12 col-sm-12 col-xs-12 has-feedback">
					                   <input type="text" class="form-control has-feedback-left"  id="Name" name="Name" placeholder="Name">
					             </div>
					      </div>
					<div class="form-group col-xs-12">

					          <div class="col-md-12 col-sm-12 col-xs-12 has-feedback">
					              <textarea class="form-control has-feedback-left"  id="Title" name="Title" placeholder="Title"></textarea>
					         </div>
					</div>
           <div class="col-md-12 col-sm-12 col-xs-12 has-feedback">
                             <input type="text" class="form-control has-feedback-left"  id="Weight" name="Weight" placeholder="Weight">
           </div>
                    <div class="form-group">
                       <div class="control-label col-md-12 col-sm-12 col-xs-12">

                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Is Required? </label>
                         <div class="col-md-1 col-sm-1 col-xs-12">
                              <input type="checkbox" class="checkbox" name="IsRequired" value="1" >
                               <input type="hidden" name="IsRequired" id="IsRequired" value="0" />
                        </div>
                        </div>
                    </div>
</fieldset>

<fieldset class="the-fieldset"  style="margin-bottom: 15px;" id="RowsForm">
<legend class="the-legend">Rows</legend>

<div class="col-md-12 col-sm-12 col-xs-12">
 		<div class="item form-group col-md-12 col-ms-12 col-xs-12">
			<label class="control-label col-md-5 col-sm-5 col-xs-12" for="Name" style="padding-left: 30px;"> Text<span class="required"></span>
			</label>
				<label class="control-label col-md-7 col-sm-7 col-xs-12" for="Name">Value<span class="required"></span>
			</label>
			<hr class="col-md-12"/>
			 <div class="form-group col-xs-12">
			 	            <div class="col-xs-5" >
                                <input class="form-control " type="text" name="Text[]" placeholder="Enter Text"  required />
                            </div>
                            <div class="col-xs-5" >
                                <input class="form-control " type="text" id="Valu[0]" name="Value[]" placeholder="Enter Value"  required />
                            </div>
                            <div class="col-xs-2" >
                            <button type="button" class="btn btn-default addButton"><i class="fa fa-plus"></i></button>
                            </div>
                           
                          
                           
                        </div>

                        <!-- The template containing an email field and a Remove button -->
                        <div class="form-group hide col-xs-12" id="RowsTemplate">
                        	   <div class="col-xs-5" >
                                <input class="form-control " type="text" name="Text[]" placeholder="Enter Text"  required />
                            </div>
                            <div class="col-xs-5" >
                                <input class="form-control " type="text" id="Valu[]" name="Value[]" placeholder="Enter Value"  required />
                            </div>
                            <div class="col-xs-2" >
                            <button type="button" class="btn btn-default removeButton"><i class="fa fa-minus"></i></button>
                            </div>
                           
                            
                            
        </div>
      <label style="color:red" id="warn"></label> 
 	</div>



</div>
</fieldset>

<fieldset class="the-fieldset"  style="margin-bottom: 15px;" id="ColumnsForm">
<legend class="the-legend">Columns</legend>
<div class="col-md-12 col-sm-12 col-xs-12">
 		<div class="item form-group col-md-12 col-ms-12 col-xs-12">
			<label class="control-label col-md-5 col-sm-5 col-xs-12" for="Name" style="padding-left: 30px;"> Text<span class="required"></span>
			</label>
				<label class="control-label col-md-7 col-sm-7 col-xs-12" for="Name">Value<span class="required"></span>
			</label>
			<hr class="col-md-12" />
			 <div class="form-group col-xs-12">
			 	            <div class="col-xs-5" >
                                <input class="form-control inventorytexbox" type="text" name="TextCol[]" placeholder="Enter Text"  required />
                            </div>
                            <div class="col-xs-5" >
                                <input class="form-control inventorytexbox" type="text" name="ValueCol[]" placeholder="Enter Value"  required />
                            </div>
                            <div class="col-xs-2" >
                            <button type="button" class="btn btn-default addButton"><i class="fa fa-plus"></i></button>
                            </div>
                           
                          
                           
                        </div>

                        <!-- The template containing an email field and a Remove button -->
                        <div class="form-group hide col-xs-12" id="ColumnsTemplate">
                        	   <div class="col-xs-5" >
                                <input class="form-control inventorytexbox" type="text" name="TextCol[]" placeholder="Enter Text"  required />
                            </div>
                            <div class="col-xs-5" >
                                <input class="form-control inventorytexbox" type="text" name="ValueCol[]" placeholder="Enter Value"  required />
                            </div>
                            <div class="col-xs-2" >
                            <button type="button" class="btn btn-default removeButton"><i class="fa fa-minus"></i></button>
                            </div>
                           
                            
                            
        </div>
       
 	</div>



</div>





</fieldset>





  </div>
  <div id="new-page"></div>
   <div class="form-group" align="center">
   <button type="submit"  name="login_form_submit" class="btn btn-success" value="Save">Submit</button>
   </div>
</div>     
{!!Form::close()!!}
              </div>
                </div>
              </div>
 </section>
@section('other_scripts')
<!--   <script src="assets/formBuilder/js/vendor.js"></script>
 --> 
<!--   <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
 -->  



<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>



<script type="text/javascript">
 $(document).ready(function() {



var asd =0;
 bookIndex = 0;
$('.checkbox').on('click', function () {
   $('#'+this.name).val($(this).val());
});














	var $fbPages = $(document.getElementById("form-builder-pages"));
  var addPageTab = document.getElementById("add-page-tab");
	  $fbPages.tabs({
    beforeActivate: function(event, ui) {
      if (ui.newPanel.selector === "#new-page") {
        return false;
      }
    }
  });

	  addPageTab.onclick = function() {
    var tabCount = document.getElementById("tabs").children.length,
      tabId = "page-" + tabCount.toString(),
      $newPageTemplate = $(document.getElementById("new-page")),
      $newPage = $newPageTemplate
        .clone()
        .attr("id", tabId)
        .addClass("fb-editor"),
      $newTab = $(this).clone().removeAttr("id"),
      $tabLink = $("a", $newTab)
        .attr("href", "#" + tabId)
        .text("Page " + tabCount); 

    $newPage.insertBefore($newPageTemplate);
    $newTab.insertBefore(this);
    $fbPages.tabs("refresh");
    $fbPages.tabs("option", "active", tabCount - 1);
var Textcolname=`[name="TextCol${tabCount.toString()}[]"]`;
var Valuecolname=`[name="ValueCol${tabCount.toString()}[]"]`;
var Textname=`[name="Text${tabCount.toString()}[]"]`;
var Valuename=`[name="Value${tabCount.toString()}[]"]`;

var endsrp='script>';

var contanthtml=`<fieldset class="the-fieldset"  style="  padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;"><legend class="the-legend" style="width: fit-content;border: 0px;">General</legend><div class="form-group col-xs-12"><div class="col-md-12 col-sm-12 col-xs-12 has-feedback"><input type="text" class="form-control has-feedback-left"  id="Name${tabCount.toString()}" name="Name${tabCount.toString()}" placeholder="Name"></div></div><div class="form-group col-xs-12"><div class="col-md-12 col-sm-12 col-xs-12 has-feedback"><textarea class="form-control has-feedback-left"  id="Title${tabCount.toString()}" name="Title${tabCount.toString()}" placeholder="Title"></textarea> <div class="col-md-12 col-sm-12 col-xs-12 has-feedback"><input type="text" class="form-control has-feedback-left" style="margin-top:10px" id="Weight${tabCount.toString()}" name="Weight${tabCount.toString()}" placeholder="Weight">
           </div></div></div><div class="form-group"><div class="control-label col-md-12 col-sm-12 col-xs-12"><label class="control-label col-md-3 col-sm-3 col-xs-12">Is Required? </label><div class="col-md-1 col-sm-1 col-xs-12"><input type="checkbox" class="checkbox" name="IsRequired${tabCount.toString()}" value="1"> <input type="hidden" name="IsRequired${tabCount.toString()}" id="IsRequired${tabCount.toString()}" value="0" /></div></div></div></fieldset><fieldset class="the-fieldset"  style="margin-bottom: 15px;" id="RowsForm${tabCount.toString()}"><legend class="the-legend">Rows</legend><div class="col-md-12 col-sm-12 col-xs-12"> <label style="color:red" id="warn${tabCount.toString()}"></label><div class="item form-group col-md-12 col-ms-12 col-xs-12"><label class="control-label col-md-5 col-sm-5 col-xs-12" for="Name" style="padding-left: 30px;"> Text<span class="required"></span></label><label class="control-label col-md-7 col-sm-7 col-xs-12" for="Name">Value<span class="required"></span></label><hr class="col-md-12"/><div class="form-group col-xs-12">
<div class="col-xs-5" ><input class="form-control " type="text" name="Text${tabCount.toString()}[]" placeholder="Enter Text"  required /></div><div class="col-xs-5" ><input class="form-control " type="text" id="Valudd${tabCount.toString()}[0]" name="Value${tabCount.toString()}[]" placeholder="Enter Value"  required /></div><div class="col-xs-2" ><button type="button" class="btn btn-default addButton${tabCount.toString()}"><i class="fa fa-plus"></i></button></div></div><div class="form-group hide col-xs-12" id="RowsTemplate${tabCount.toString()}"><div class="col-xs-5" ><input class="form-control" type="text" name="Text${tabCount.toString()}[]" placeholder="Enter Text"  required /></div><div class="col-xs-5" ><input class="form-control" type="text" id="Valudd${tabCount.toString()}[]" name="Value${tabCount.toString()}[]" placeholder="Enter Value"  required /></div><div class="col-xs-2" ><button type="button" class="btn btn-default  removeButton${tabCount.toString()}"><i class="fa fa-minus"></i></button></div></div></div></div></fieldset><fieldset class="the-fieldset"  style="margin-bottom: 15px;"  id="ColumnsForm${tabCount.toString()}"><legend class="the-legend">Columns</legend><div class="col-md-12 col-sm-12 col-xs-12"><div class="item form-group col-md-12 col-ms-12 col-xs-12"> <label class="control-label col-md-5 col-sm-5 col-xs-12" for="Name" style="padding-left: 30px;"> Text<span class="required"></span></label><label class="control-label col-md-7  col-sm-7 col-xs-12" for="Name">Value<span class="required"></span></label><hr class="col-md-12" /><div class="form-group col-xs-12"><div class="col-xs-5" ><input class="form-control  inventorytexbox" type="text" name="TextCol${tabCount.toString()}[]" placeholder="Enter Text"  required /></div><div class="col-xs-5" ><input class="form-control inventorytexbox"  type="text" name="ValueCol${tabCount.toString()}[]" placeholder="Enter Value"  required /></div><div class="col-xs-2" ><button type="button" class="btn btn-default addButton${tabCount.toString()}"><i class="fa fa-plus"></i></button></div></div><div class="form-group hide col-xs-12" id="ColumnsTemplate${tabCount.toString()}"><div class="col-xs-5" ><input class="form-control inventorytexbox" type="text" name="TextCol${tabCount.toString()}[]" placeholder="Enter Text"  required /></div><div class="col-xs-5" ><input class="form-control inventorytexbox" type="text" name="ValueCol${tabCount.toString()}[]"  placeholder="Enter Value"  required /></div><div class="col-xs-2" ><button type="button" class="btn btn-default removeButton${tabCount.toString()}"><i class="fa fa-minus"></i></button> </div>  </div></div></fieldset><script>
 $(document).ready(function() {
  var book=0;var asdu=0;
  $("#RowsForm${tabCount.toString()}").on("click",".addButton${tabCount.toString()}", function() {var valuext=0; asdu=0;

  for(i = 0; i < book+1; i++) 
    {

       valuext=document.getElementById('Valudd${tabCount.toString()}['+i+']').value;
     
      asdu=asdu+parseInt(valuext);
     
    }
  document.getElementById("warn").innerHTML="";

  if(0 < asdu && asdu < 100){
   
    book=book+1;
     var $template = $("#RowsTemplate${tabCount.toString()}"),
    $clone = $template.clone() .removeClass("hide").removeAttr("id").insertBefore($template)
    ,$Text =  $clone.find(${Textname}),$Value=
  $clone.find('[name="Value${tabCount.toString()}[]"]').attr('name','Value${tabCount.toString()}[' + book + ']').attr('id','Valudd${tabCount.toString()}[' + book + ']');
   $("#RowsForm${tabCount.toString()}").formValidation("addField", $Text).formValidation("addField", $Value);
  }
   else
  {
  
    document.getElementById("warn${tabCount.toString()}").innerHTML="Please Must be equal 100";
       if( asdu > 100)
  {

       document.getElementById('Valudd${tabCount.toString()}[' + book + ']').value=0;
  }
  }

 
 })
   .on("click",".removeButton${tabCount.toString()}",  function() {
     document.getElementById("warn${tabCount.toString()}").innerHTML="";
           var valuetext = document.getElementById('Valudd${tabCount.toString()}[' + book + ']').value;
   
              asdu=asdu-parseInt(valuetext);

              book=book-1;

    var $row   = $(this).closest(".form-group"),
    $Text = $row.find(${Textname}),
    $Value = $row.find('[name="Value${tabCount.toString()}[]"]').attr('name','Value${tabCount.toString()}[' + book + ']').attr('id','Valudd${tabCount.toString()}[' + book + ']');
    $row.remove();

     $("#RowsForm${tabCount.toString()}").formValidation("removeField",  $Text).formValidation("removeField",  $Value); });$("#ColumnsForm${tabCount.toString()}").on("click",".addButton${tabCount.toString()}", function() { var $template = $("#ColumnsTemplate${tabCount.toString()}"),$clone    =  $template.clone().removeClass("hide").removeAttr("id").insertBefore($template),$Text    = $clone.find(${Textcolname}),$Value    = $clone.find(${Valuecolname});$("#ColumnsForm${tabCount.toString()}").formValidation("addField",$Text).formValidation("addField", $Value);}).on("click",".removeButton${tabCount.toString()}", function() {var $row   = $(this).closest(".form-group"),$Text = $row.find(${Textcolname}), $Value    = $row.find(${Valuecolname});$row.remove();$("#ColumnsForm${tabCount.toString()}").formValidation("removeField", $Text).formValidation("removeField", $Value);});});</${endsrp}`;


  $("#"+tabId).html(contanthtml);

  };
  
   $('#RowsForm').on('click', '.addButton', function() {
      asd=0;
      var valuetext=0;
    for (i = 0; i < bookIndex+1; i++) { 

     valuetext = document.getElementById('Valu[' + i + ']').value;
     if(valuetext=="")
     {
      valuetext=0;
     } 
        
     asd=asd+parseInt(valuetext);
}

  document.getElementById("warn").innerHTML="";  
  if(0 < asd && asd < 100)
  {
  bookIndex ++;
     var $template = $('#RowsTemplate'),
                $clone    = $template
                                .clone()
                                .removeClass('hide')
                                .removeAttr('id')
                                .insertBefore($template),
                $Text  = $clone.find('[name="Text[]"]'),
                   $Value = $clone.find('[name="Value[]"]').attr('name','Value[' + bookIndex + ']').attr('id','Valu[' + bookIndex + ']');
               
            // Add new field
            $('#RowsForm').formValidation('addField', $Text).formValidation('addField', $Value);
      ;
       
  }
  else
  {
   
    document.getElementById("warn").innerHTML="Please Must be equal 100";
    // alert(bookIndex);
     if( asd > 100)
  {

       document.getElementById('Valu[' + bookIndex + ']').value=0;
  }

  }
        })

        // Remove button click handler
        .on('click', '.removeButton', function() {
           document.getElementById("warn").innerHTML="";
           var valuetext = document.getElementById('Valu[' + bookIndex + ']').value;
   
              asd=asd-parseInt(valuetext);

              bookIndex=bookIndex-1;
            var $row   = $(this).closest('.form-group'),
                $Text = $row.find('[name="Text[]"]'),
                 $Value = $row.find('[name="Value[]"]').attr('name','Value[' + bookIndex + ']').attr('id','Valu[' + bookIndex + ']');
            // Remove element containing the email
            $row.remove();
 
            // Remove field
            $('#RowsForm').formValidation('removeField', $Text).formValidation('removeField', $Value);
        });

           $('#ColumnsForm').on('click', '.addButton', function() {
            var $template = $('#ColumnsTemplate'),
                $clone    = $template
                                .clone()
                                .removeClass('hide')
                                .removeAttr('id')
                                .insertBefore($template),
                $Text    = $clone.find('[name="TextCol[]"]'),
                $Value    = $clone.find('[name="ValueCol[]"]');

            // Add new field
            $('#ColumnsForm').formValidation('addField', $Text).formValidation('addField', $Value);
        })

        // Remove button click handler
        .on('click', '.removeButton', function() {
            var $row   = $(this).closest('.form-group'),
                $Text = $row.find('[name="TextCol[]"]'),
                 $Value    = $row.find('[name="ValueCol[]"]');
            // Remove element containing the email
            $row.remove();

            // Remove field
            $('#ColumnsForm').formValidation('removeField', $Text).formValidation('removeField', $Value);
        });
});
</script>


@endsection
@endsection