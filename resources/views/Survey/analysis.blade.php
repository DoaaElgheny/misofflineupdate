@extends('masternew')
@section('content')
 <section class="panel-body">
 <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2> </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                     
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                  	<div class="row" >
                        <fieldset style="    padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;">
  <legend style="width: fit-content;border: 0px;">Option:</legend>

	 <div class="col-sm-12 col-md-12  form-horizontal">
            <div class="col-md-12 form-group">
              <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                <label class="col-xs-3 control-label">servey:</label>
                <div class="col-xs-9">
{!!Form::select('servey',([null => 'Select  servey'] +$servey->toArray())   ,null,['class'=>'form-control chosen-select servey', 'id' => 'servey','name'=>'servey','required'=>'required'])!!} 
                </div>
              </div>
              <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                <label class="col-md-3 col-sm-12 col-xs-12  control-label">Sub servey:</label>
                <div class="col-xs-6">
                

{!!Form::select('Question[]', ( ['all' => 'Select Question'] +$Question->toArray()) ,null,['class'=>'form-control chosen-select Question ','id' => 'Question','name'=>'Question','required'=>'required'])!!}


                </div>
              </div>
            </div>
           
          
          </div> 
          
          <div class="col-sm-12 col-md-12  form-horizontal">
         
            <div class="form-group">
              <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                <button type="button" onclick="ShowReport()" id="Search" name="login_form_submit" class="btn btn-success" style="float: left;">Show Report</button>
                <label style="color:red" id="warn"></label> 
              </div>
            </div>
          </div>  
        </div>
        </fieldset>
<div id="chartContainer" style="height: 370px; width: 100%;"></div>
<br/>
<br/>
<br/>
<br/>
<br/>
    <table id="jsontable" class="display table table-bordered" cellspacing="0" width="100%">
    <thead>
    <tr>
    <th>Activity Name</th>
    <th>Top</th>
    <th>Middle</th>
    <th>Bottom</th>
    <th>Na</th>

    </tr>
    </thead>

    </table>
                    </div>  
                              
</div>
</div>
</div>
</section>
@section('other_scripts') 
<script type="text/javascript" src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
<script>

 
var oTable = $('#jsontable').dataTable({

select:true,
responsive: true,
"order":[[0,"asc"]],
'searchable':true,
"scrollCollapse":true,
"paging":true,
"pagingType": "simple",
dom: 'lBfrtip',
buttons: [


{ extend: 'excel', className: 'btn btn-primary dtb' }


]});


       
 $(document).ready(function(){

 $('.Question').chosen({
                width: '100%',
                no_results_text:'No Results'
            });
         $(".Question").trigger("chosen:updated");
            $(".Question").trigger("change");
             $('.servey').chosen({
                width: '100%',
                no_results_text:'No Results'
            });
            $(".servey").trigger("chosen:updated");
            $(".servey").trigger("change");


 
            $('.servey').chosen({
                width: '100%',
                inherit_select_classes: true
            })
            // Revalidate the color when it is changed
            .change(function(e) {



  $.getJSON("/offlineneew/public/servey/Question/" + $("#servey").val(), function(data) {
 
                var $Cat = $(".Question");
                   $(".Question").chosen("destroy");
                $Cat.empty();
                $.each(data, function(index, value) {
                    $Cat.append('<option value="' + index +'">' + value + '</option>');
                });
                  
        
             $('.Question').chosen({
                width: '100%',
                no_results_text:'No Results'
            });
                $(".Question").trigger("chosen:updated");
            $(".Question").trigger("change");
            });
            });
  });

    function ShowReport()
{
	var id=$("#Question").val();
	console.log(id);
   $.ajax({
      url: "/offlineneew/public/analysissurvey",
        data:{id:id},
       type: "Get",

       success: function(data){

var canvas_data =[];
oTable.fnClearTable();

$.each(data[0].data, function(index,value) { 
console.log(index,value);
oTable.fnAddData([ value.Name,value.Top,value.Middle,value.Bottom,value.Na]);  


});

jQuery.each(data[0].normal[0], function( i, val ) {
	console.log(i,val);
	canvas_data.push({
                       type: "stackedBar100", 
                       name: i,
                       toolTipContent: "{label}<br><b>{name}:</b> {y} (#percent%)",
                       showInLegend: true, 
            
                      dataPoints:val
          });

});

console.log(data[0].avg);
canvas_data.push({
	  axisYType: "secondary", 
	   name: 'Average',
	   	linkedDataSeriesIndex: 0,
	   indexLabelFontColor: "#fff",
	   toolTipContent:"<b>Avg:{y[1]}</b>",
   indexLabel: "{y[1]}",
   showInLegend: true, 
		type: "error",
		stemColor: "#990000",
		stemThickness: 10,
		dataPoints:data[0].avg
	});


           
     var chart = new CanvasJS.Chart("chartContainer", {
	animationEnabled: true,
	theme: "light2", //"light1", "dark1", "dark2"
  exportEnabled: true,
animationEnabled: true,
zoomEnabled: true,
	title:{
		text: "Activity Analysis (Count User)"             
	},dataPointMaxWidth: 30,
	axisY:{
		interval: 10,
		suffix: "%",
		

	},
	// axisX:{	},
 	axisY2:{
 		interval: 1,
			title: "Axis Average for Data",
			lineColor: "#014D65",
			maximum: 10
			
		},
	toolTip:{
		shared: true
	},
	data:canvas_data,
  legend:{
fontSize: 15,
cursor:"pointer",
itemclick: function(e){
if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
e.dataSeries.visible = false;
}
else {
e.dataSeries.visible = true;
}
chart.render();
}
}
});
chart.render();



  }         
    

});



}

         

        

</script>

@endsection
@endsection