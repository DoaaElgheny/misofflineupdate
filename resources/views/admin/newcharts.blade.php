@extends('master')
@section('content')  
 <div class="panel panel-default">
                <div class="panel-heading">UPE Visits</div>
                <div class="panel-body">
                 <div id="canvasUPE" style="height: 360px; width:100%;">

                </div>
            </div>
        </div>
         

<div class="panel panel-default">
                <div class="panel-heading">LOE Visits</div>
                <div class="panel-body">
                 <div id="canvasLOE" style="height: 360px; width:100%;">

                </div>
            </div>
        </div>
      


           <script type="text/javascript">
    $(function(){


   var updateChart = function () {
        $.getJSON("VisitsGovUPE", function(result) {
    
             
      var chart = new CanvasJS.Chart("canvasUPE",
              {
                zoomEnabled: true,
                  theme: "theme3",
                  exportEnabled: true,
                              animationEnabled: true,
               dataPointWidth: 20,
                  toolTip: {
                      shared: true
                  }, 
        axisY:{
    
   
     },
            data: [ 

               

                      {
                      type: "column", 
                      name: "Marketing",
                      indexLabelFontSize: 16,
                      indexLabelFontColor: "black",
                      legendText: "Marketing",
                      showInLegend: true, 
                      dataPoints:result.type
                  },
                   {
                      type: "column", 
                      name: "Sales",
                      indexLabelFontSize: 16,
                      indexLabelFontColor: "black",
                      legendText: "Sales",
                      showInLegend: true, 
                      dataPoints:result.salle
                  }
 
                  ],
                  legend:{
                    cursor:"pointer",
                    itemclick: function(e){
                      if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
                        e.dataSeries.visible = false;
                      }
                      else {
                        e.dataSeries.visible = true;
                       }
                      chart.render();
                    }
                  },
                });
      chart.render();
       });
         $.getJSON("VisitsGovLOE", function(result) {
    
             
      var chart = new CanvasJS.Chart("canvasLOE",
              {
                zoomEnabled: true,
                  theme: "theme3",
                  exportEnabled: true,
                              animationEnabled: true,
               dataPointWidth: 20,
                  toolTip: {
                      shared: true
                  }, 
        axisY:{
    
   
     },
            data: [ 

               

                      {
                      type: "column", 
                      name: "Marketing",
                      indexLabelFontSize: 16,
                      indexLabelFontColor: "black",
                      legendText: "Marketing",
                      showInLegend: true, 
                      dataPoints:result.type
                  },
                   {
                      type: "column", 
                      name: "Sales",
                      indexLabelFontSize: 16,
                      indexLabelFontColor: "black",
                      legendText: "Sales",
                      showInLegend: true, 
                      dataPoints:result.salle
                  }
 
                  ],
                  legend:{
                    cursor:"pointer",
                    itemclick: function(e){
                      if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
                        e.dataSeries.visible = false;
                      }
                      else {
                        e.dataSeries.visible = true;
                       }
                      chart.render();
                    }
                  },
                });
      chart.render();
       });
           
};
updateChart();
    setInterval(function(){updateChart()}, 20000);
    


});


       </script>
         @stop