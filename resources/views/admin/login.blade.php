<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>MIS OFFLINE SYSTEM! </title>

    <!-- Bootstrap -->
    <link href="/offlineneew/public/bower_components/gentelella/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="/offlineneew/public/bower_components/gentelella/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="/offlineneew/public/bower_components/gentelella/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- Animate.css -->
    <link href="/offlineneew/public/bower_components/gentelella/vendors/animate.css/animate.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="/offlineneew/public/bower_components/gentelella/build/css/custom.min.css" rel="stylesheet">
  </head>

  <body class="login">
    <div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>

      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">


   {!! Form::open(['action' => "AdminController@login",'method'=>'post','id'=>'profileForm','class'=>'form-horizontal form-label-left','novalidate'=>'novalidate']) !!}  
     
              <h1>Login Form</h1>
           
              <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />

               


                  <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="username">Username *
                </label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <input id="username" class="form-control col-md-9 col-xs-12"  name="username" placeholder="Enter Username" required="required" type="text">


                </div>


              </div>


             
                  <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="password">Password* 
                </label>
                <div class="col-md-9 col-sm-9 col-xs-12">
                  <input id="password" class="form-control col-md-8 col-xs-12" data-validate-words="1" name="password" placeholder="Enter password" required="required" type="password">
                </div>
              </div>
              <div>
            <a> <input type="submit" value="Login" class="btn btn-default" /> </a>
               
              </div>

              <div class="clearfix"></div>

              <div class="separator">
               
   @if(Session::has('error'))
            
          <div class="alert alert-danger" style="text-align:center;" >
                  <strong>Whoops!</strong> There is something wrong<br><br>

    
    </div>
    @endif
                <div class="clearfix"></div>
                <br />

                <div>
                  <h1><i class="fa fa-paw"></i> MIS SYSTEM</h1>
                  <p>©2017 All Rights Reserved. Digital Marketing Team . Privacy and Terms</p>
                </div>
              </div>
           {!!Form::close()!!}
          </section>
        </div>
         <!-- jQuery -->
    <script src="/offlineneew/public/bower_components/gentelella/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
<script type="text/javascript" src="/offlineneew/public/assets/js/bootstrap.min.js"></script>
    <script src="/offlineneew/public/bower_components/gentelella/build/js/custom.js"></script>

       <script src="/offlineneew/public/bower_components/gentelella/vendors/validator/validator.js"></script>
<script type="text/javascript">
  
$(function(){

      $('form').submit(function(e) {
        e.preventDefault();
        var submit = true;

        // evaluate the form using generic validaing
        if (!validator.checkAll($(this))) {
          submit = false;
        }

        if (submit)
           this.submit();

        return false;
    });

});

</script>
      </div>
    </div>
  </body>
</html>
