@extends('masternew')

<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDB908G39VM37Q53FpkqJM6JwDz-mmewsQ&libraries=geometry&dummy=.js"></script>



<script type="text/javascript">  
  window.onload = function() {
    ajaxCall();
  };
</script>
<style type="text/css">
  .chosen-container .chosen-drop {
    border-bottom: 0;
    border-top: 1px solid #aaa;
    top: auto;
    bottom: 40px;
}

</style>
@section('content')
<section class="panel-body">
 <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                   <h2><i class="fa fa-map-marker"></i> Promoters Map</h2> 
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                     
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
<div id="map" style=" width: 100%; height: 70%;"></div>
<form  class="form-horizontal">
 <fieldset style="    padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;">
  <legend style="width: fit-content;border: 0px;">Enter Data:</legend>
  <div class="control-group" style="margin-top: 10px">
   <div class="controls form-inline">
      <label>From Date</label>
      <div class='input-group date col-md-2'  id='datepicker'> 
          <input type='text'name="date"  class="input-small form-control" id="date" placeholder="Enter Date"/>
          <span class="input-group-addon" id="date">
            <span class="glyphicon glyphicon-calendar"></span>
          </span>
      </div>

      <label>To Date</label>
      <div class='input-group date col-md-2'  id='datepicker2'> 
          <input type='text' name="date2"  class="input-small form-control" id="date2" placeholder="Enter Date"/>
          <span class="input-group-addon" id="date2">
            <span class="glyphicon glyphicon-calendar"></span>
          </span>
      </div>
      
      <div class="input-group col-md-2" style="margin-left: 3%;text-align: center;">
       
              {!!Form::select('username[]', (['empty' => 'Please Select'] + $users->toArray()) ,null,['class'=>'form-control  input-small  Users chosen-select ','id' => 'username'])!!} 
         
      </div>

       <div class="input-group  col-md-2" style="text-align: center;" >
          <a href="javascript:FilterValue()" class="btn btn-labeled  ">
          <span class="btn-label col-md-2"><i class="glyphicon glyphicon-filter"></i></span> Filter</a>
     </div>

      <div  class="input-group  col-md-2">
          <a href="javascript:RefreshMap()" class="btn  btn-labeled ">
          <span class="btn-label col-md-2"><i class="glyphicon glyphicon-refresh"></i></span> Full Map</a>
      </div>

      <div class="label btn-primary col-md-2 col-md-offset-2" id="total_visits" >
          Total Visits 
      </div>
      <div class="label btn-primary col-md-2 col-md-offset-2" id="total_distance">
          Total Distances 
      </div>
      <div class="label btn-primary col-md-2 col-md-offset-2" id="distance_home">
         Distances from home
      </div>

    </div> 
  </div> 
  </fieldset>
</form>

</div>

</div>
</div>
</div>
  @section('other_scripts')

<script type="text/javascript"> 
var timer;
var map = new google.maps.Map(document.getElementById('map'), {
              zoom: 6,
              center: new google.maps.LatLng(28.000, 32.444),
              mapTypeId: google.maps.MapTypeId.ROADMAP

          });
var markerArr = new Array();
function clearOverlays() {
    if (markerArr) {
      for (i in markerArr) {
        markerArr[i].setVisible(false)
        
      }
    // console.log("clear");
    }
}

function RefreshMap() {
  // console.log('RefreshMap');
  map = new google.maps.Map(document.getElementById('map'), {
              zoom: 6,
              center: new google.maps.LatLng(28.000, 32.444),
              mapTypeId: google.maps.MapTypeId.ROADMAP

          });
ajaxCall();
}

function ajaxCall() {
  var marker,i;
   $.ajax({
        type: "GET",
        url : '{{ url("/map") }}',
        success: function(response) {
          var jArray = JSON.parse(response);
           console.log(jArray);
           
          var infowindow = new google.maps.InfoWindow();
          console.log(jArray.locations.length);
                  for (i = 0; i < jArray.locations.length; i++) {
                 marker = new google.maps.Marker({
                         position: new google.maps.LatLng(jArray.locations[i].Lat, jArray.locations[i].Long),
                        map: map,
                       icon: 'http://217.139.86.100/multiproductsystem/assets/img/Google-location-icon-color_icons_green_home.png'
                    });
        var geocoder = new google.maps.Geocoder();
        var latitude = jArray.locations[i].Lat;
        var longitude = jArray.locations[i].Long;
        var address_arr = new Array();
        var latLng = new google.maps.LatLng(latitude,longitude);
        geocoder.geocode({       
            latLng: latLng     
          }, 
        function(responses) 
            {     
              if (responses && responses.length > 0) {    
                  address_arr.push(responses[0].formatted_address); 
              } 
              else{     
                  address_arr[i] = 'Not getting address for given latitude and longitude';  
              }   
            }
        );
    google.maps.event.addListener(marker, 'click', (function(marker, i) {
          return function() {           
            infowindow.setContent('Name '+jArray.locations[i].Name);
            infowindow.open(map, marker);
          }
        })(marker, i));

    markerArr[i]=marker;
    }
  }}).always(function() {
      setTimeout(clearOverlays, 9999); // 9.99 seconds
      timer=setTimeout(ajaxCall, 10000); // 10 seconds
   });
}

var name = "empty";
var date = "empty";

function FilterValue() {
  map = new google.maps.Map(document.getElementById('map'), {
              zoom: 6,
              center: new google.maps.LatLng(28.000, 32.444),
              mapTypeId: google.maps.MapTypeId.ROADMAP

          });
  window.clearTimeout(timer);
  var selectedIndex = document.getElementById('username').selectedIndex;
  //name
  if (document.getElementById('username')[selectedIndex].value) {
      name = document.getElementById('username')[selectedIndex].value;
  }
  //date
  if (document.getElementById('date').value) {
      date = document.getElementById('date').value;
      var date=date.split('/');
      var date =date[0];
  }
  if (document.getElementById('date').value ==="") {
      date = "empty";
  } 
  //date2
  if (document.getElementById('date2').value) {
      date2 = document.getElementById('date2').value;
      var date2=date2.split('/');
      var date2 =date2[0];
  }
  if (document.getElementById('date2').value ==="") {
      date2 = "empty";
  }  
  var marker,i;
  $.ajax({
        type: "GET",
        url : '../gps/filter/name/'+name+'/date/'+date+'/to/'+date2,
        success: function(response) {
          console.log("shaza"+name);
          var jArray = JSON.parse(response);
          // console.log(response);
        document.getElementById('total_visits').innerHTML='Total Visits: '+jArray.locations.length;
          var map = new google.maps.Map(document.getElementById('map'), {
              zoom: 6,
              center: new google.maps.LatLng(28.000, 32.444),
              mapTypeId: google.maps.MapTypeId.ROADMAP

          });
          var infowindow = new google.maps.InfoWindow();
          var path = [];
          var area=0;
          var polylineLength = 0;
          var polylineLength1 = 0;
           console.log("hh"+jArray.locations);
          for (i = 0; i < jArray.locations.length; i++) {

              if (jArray.locations[i].Status == 0 &&jArray.locations[i].user_id != "المعمل المتجول") {
                  marker = new google.maps.Marker({
                      position: new google.maps.LatLng(jArray.locations[i].Lat, jArray.locations[i].Long),
                      map: map
                  });
              }
              else if (jArray.locations[i].Status == 1 &&jArray.locations[i].user_id != "المعمل المتجول"){
                 marker = new google.maps.Marker({
                         position: new google.maps.LatLng(jArray.locations[i].Lat, jArray.locations[i].Long),
                        map: map,
                       icon: 'http://maps.google.com/mapfiles/ms/icons/green-dot.png'
                    });
}
             else if (jArray.locations[i].user_id === "المعمل المتجول"){
                 marker = new google.maps.Marker({
                         position: new google.maps.LatLng(jArray.locations[i].Lat, jArray.locations[i].Long),
                        map: map,
                       icon: '/public/assets/img/Map.png'
                    });

              }
            var geocoder = new google.maps.Geocoder();
            var latitude = jArray.locations[i].Lat;
            var longitude = jArray.locations[i].Long;
            var address_arr = new Array();
            var latLng = new google.maps.LatLng(latitude,longitude);
            var latLng1 = new google.maps.LatLng(jArray.locations[0].Lat,jArray.locations[0].Long);
            path.push(latLng);

if (i > 0) polylineLength += google.maps.geometry.spherical.computeDistanceBetween(path[i], path[i-1]);
else
polylineLength1 = google.maps.geometry.spherical.computeDistanceBetween(latLng1, path[i]);
            geocoder.geocode({       
                latLng: latLng     
              }, 
            function(responses) 
                {     
                  if (responses && responses.length > 0) {    
                      address_arr.push(responses[0].formatted_address); 
                  } 
                  else{     
                      address_arr[i] = 'Not getting address for given latitude and longitude';  
                  }   
                }
            );
       google.maps.event.addListener(marker, 'click', (function(marker, i) {
          return function() {           
            infowindow.setContent('Name '+jArray.locations[i].user_id+'<br/> Address '+address_arr[i] +' <br/> Contractor Name '+ jArray.locations[i].Contractor_Name+'<br/>Code '+jArray.locations[i].Contractor_Code+'<br/>Time : '+jArray.locations[i].NTime+'<br/>Type : '+jArray.locations[i].Type);
            infowindow.open(map, marker);
          }
        })(marker, i));

          }
          for (i = 0; i < jArray.locations.length; i++) {
                 marker = new google.maps.Marker({
                         position: new google.maps.LatLng(jArray.locations[i].Lat, jArray.locations[i].Long),
                        map: map,
                       icon: 'http://217.139.86.100/multiproductsystem/assets/img/Google-location-icon-color_icons_green_home.png'
                    });
            var geocoder = new google.maps.Geocoder();
            var latitude = jArray.locations[i].Lat;
            var longitude = jArray.locations[i].Long;
            var address_arr = new Array();
            var latLng = new google.maps.LatLng(latitude,longitude);
            geocoder.geocode({       
                latLng: latLng     
              }, 
            function(responses) 
                {     
                  if (responses && responses.length > 0) {    
                      address_arr.push(responses[0].formatted_address); 
                  } 
                  else{     
                      address_arr[i] = 'Not getting address for given latitude and longitude';  
                  }   
                }
            );
       google.maps.event.addListener(marker, 'click', (function(marker, i) {
          return function() {           
            infowindow.setContent('Name '+jArray.locations[i].user_id+'<br/> Address '+address_arr[i] +' <br/> User Name '+ jArray.locations[i].Name+'<br/>Code '+jArray.locations[i].Code);

            infowindow.open(map, marker);
          }
        })(marker, i));

          }
          if(name !="empty")
          {
                                polyline = new google.maps.Polyline({
  path: path,
  strokeColor: "#FF0000",
  strokeOpacity: 1.0,
  strokeWeight: 2
});



polyline.setMap(map);



document.getElementById('total_distance').innerHTML='Total Distances: '+(polylineLength/1000).toFixed(1);
document.getElementById('distance_home').innerHTML='Distances: '+(polylineLength1/1000).toFixed(1);

        }   } });
  }
</script>






<script type="text/javascript">
    $(function(){
 $('#date2').daterangepicker({
        singleDatePicker: true,
        singleClasses: "picker_4",
         locale: {
            format: 'YYYY-MM-DD'
        }
      }, function(start) {
        console.log(start.toISOString(), 'Hadeel');
      });
  $('#date').daterangepicker({
        singleDatePicker: true,
        singleClasses: "picker_4",
         locale: {
            format: 'YYYY-MM-DD'
        }
      }, function(start) {
        console.log(start.toISOString(), 'Hadeel');
     });
         
      
      $(".Users").chosen({ 
                       width: '100%',
                       no_results_text: "لا توجد نتيجه",
                       allow_single_deselect: true, 
                       search_contains:true, });
      $(".Users").trigger("chosen:updated");

     

 
          
      
    });
</script>


</section>
@stop
@stop
