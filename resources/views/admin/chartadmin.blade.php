 @extends('masternew')
@section('content') 

    <!-- page content -->
          <div class="">
            <div class="row top_tiles">
              <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12 ">
                <div class="tile-stats panel-pormoter">
                  <div class="icon"><i class="fa fa-caret-square-o-right"></i></div>
              
                  <div class="count huge" id="pormoter"><strong>0 </strong></div>
                      <h3>currently working</h3>
                  <p id="target_pr" style="display: inline;"> Target  : <strong> </strong>
                  </p>
                  <p  id="achived" style="display: inline;">Achived :<strong> </strong>%
                  </p>

                </div>
              </div>




              <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats panel-contractor">
                  <div class="icon"><i class="fa fa-users"></i></div>
                    <div class="count huge" id="contractor"><strong>0 </strong></div>
                      <h3>Daily Contractor</h3>
                  <p id="target_cont" style="display: inline;"> Target  : <strong> </strong>
                  </p>
                  <p  id="achived_cont" style="display: inline;">Achived :<strong> </strong>%
                  </p>
                </div>
              </div>
              <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats panel-order">
                  <div class="icon"><i class="fa fa-sort-amount-desc"></i></div>
                        <div class="count huge" id="order"><strong>0 </strong></div>
                      <h3>Daily Order</h3>
                  <p id="target_ord" style="display: inline;"> Target  : <strong> </strong>
                  </p>
                  <p  id="achived_ord" style="display: inline;">Achived :<strong> </strong>%
                  </p>
                </div>
              </div>
              <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats panel-visit">
                  <div class="icon"><i class="fa fa-check-square-o"></i></div>
                           <div class="count huge" id="visit"><strong>0 </strong></div>
                      <h3>Daily Visit</h3>
                      <div>
                  <p  style="display: inline;"  id="target_vis"> Target  : <strong> </strong>
                  </p>
                  <p  id="achived_vis" style="display: inline;">Achived :<strong> </strong>%
                  </p>
                  </div>
                </div>
              </div>
            </div>

 

        <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="dashboard_graph x_panel">
                  <div class="row x_title">
                    <div class="col-md-6">
                      <h3>Total Visits And Total Contractor  <small>By Promoter Upper</small></h3>
                    </div>
                    <div class="col-md-6">
                      <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">
                        <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                        <span>December 30, 2014 - January 28, 2015</span> <b class="caret"></b>
                      </div>
                    </div>
                  </div>
                  <div class="x_content">
                    <div class="demo-container" style="height:280px" >
                    
                    <div id="canvaspromoterUpper" class="demo-placeholder" ></div>   
                                  
                    </div>
                  </div>
                </div>
              </div>
            </div>

        <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="dashboard_graph x_panel">
                  <div class="row x_title">
                    <div class="col-md-6">
                      <h3>Total Visits And Total Contractor  <small>By Promoter Lower</small></h3>
                    </div>
                    <div class="col-md-6">
                      <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">
                        <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                        <span>December 30, 2014 - January 28, 2015</span> <b class="caret"></b>
                      </div>
                    </div>
                  </div>
                  <div class="x_content">
                    <div class="demo-container" style="height:280px" >
                    
                    <div id="canvaspromoter" class="demo-placeholder" ></div>   
                                  
                    </div>
                  </div>
                </div>
              </div>
            </div>


            <div class="row">
              <div class="col-md-6">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Activities & Sales <small>Opportunities</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <div class="demo-container" style="height:280px" >
                    
                    <div id="canvastype" class="demo-placeholder" ></div>   
                                  
                    </div>
                  </div>
                </div>
              </div>

              <div class="col-md-6">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Contractors Program  Products <small>(Ton)</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                   
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                     <div class="demo-container" style="height:280px" >   
                    <div id="canvassales" class="demo-placeholder" ></div>   
                                  
                    </div>
                  </div>
                </div>
              </div>

              </div>


               <div class="row">
              <div class="col-md-6">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Activities & Sales <small>Opportunities</small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                   <div class="table-responsive demo-placeholder" >

                    <table id="jsontable" class="display table table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                    <th>Gov</th>
                    <th>Name</th>
                    <th>Time</th>

                    </tr>
                    </thead>

                    </table>
                    </div>  
                                  
                    </div>
                  </div>
                </div>
             

              <div class="col-md-6">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Sales Order  <small></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                   
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                     <div class="demo-container" style="height:280px" >   
                    <div id="canvas" class="demo-placeholder" ></div>   
                                  
                    </div>
                  </div>
                </div>
              </div>

              </div>
          </div>
        @endsection
  @section('other_scripts')
<script type="text/javascript">




$(function(){

var oTable = $('#jsontable').dataTable({

select:true,
responsive: true,
"order":[[0,"asc"]],
'searchable':true,
"scrollCollapse":true,
"paging":true,
"pagingType": "simple",
dom: 'lBfrtip',
buttons: [


{ extend: 'excel', className: 'btn btn-primary dtb' }


]});
var updateChart = function () {

$.getJSON("http://10.43.144.19/offlineneew/public/saleschart", function(result) {



var chart = new CanvasJS.Chart("canvassales",
{
theme: "theme3",
exportEnabled: true,
animationEnabled: true,
zoomEnabled: true,
axisX:{
labelFontColor: "black",
labelMaxWidth: 100,

labelAutoFit: true ,
labelFontWeight: "bold"


},
legend:{
fontSize: 15
},

toolTip: {
shared: true
},          
dataPointWidth: 20,
data: [ 
{
indexLabelFontSize: 16,
indexLabelFontColor: "black",
type: "column", 
indexLabel: "{y} Ton " ,
name: "Total Product Quantity",
legendText: "Total Product Quantity",
showInLegend: true, 

dataPoints:result.products
}

]
});
chart.render();
});








$.getJSON("http://10.43.144.19/offlineneew/public/contchartt" , function (result) {



var chart = new CanvasJS.Chart("canvas",

{
zoomEnabled: true,
zoomType: "xy",
exportEnabled: true,
theme: "theme3",
animationEnabled: true,
dataPointWidth: 20,
axisY: {
title: "No. Of Orders",

},
axisX:{
labelFontColor: "black",
labelMaxWidth: 100,

labelAutoFit: true ,
gridThickness: 2,
labelFontWeight: "bold"


},

toolTip: {
content: "Date: {label} </br> <strong>detals:</strong> </br>{y} "
}, 



axisY2:{ 
title: "Total Amount",
titleFontColor : "steelBlue",
lineThickness:4
},
data: [ 
{
type: "column", 
indexLabel: "{y}",
name: "Total Products",
legendText: "Total Products",
indexLabelFontSize: 16,
indexLabelFontColor: "black",
showInLegend: true, 
dataPoints:result.products


},
{
type: "column", 
axisYType: "secondary",
indexLabel: "{y}",
name: "Total Amounts",
legendText: "Total Amount",
showInLegend: true, 
indexLabelFontSize: 16,
indexLabelFontColor: "black",
dataPoints:result.Amount


},
{
type: "column", 
indexLabel: "{y}",
name: "Total Approves",
legendText: "Total Approves",
showInLegend: true, 
dataPoints:result.approves


}

],
legend:{
fontSize: 15,
cursor:"pointer",
itemclick: function(e){
if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
e.dataSeries.visible = false;
}
else {
e.dataSeries.visible = true;
}
chart.render();
}
},
});

chart.render();

});




$.getJSON("http://10.43.144.19/offlineneew/public/typechart", function(result) {


var chart = new CanvasJS.Chart("canvastype",
{
zoomEnabled: true,
theme: "theme3",
exportEnabled: true,
animationEnabled: true,
dataPointWidth: 20,
toolTip: {
shared: true
}, 
axisY:{


},
data: [ 



{
type: "column", 
name: "Contractors Program",
indexLabel: "{y}",
indexLabelFontSize: 16,
indexLabelFontColor: "black",
legendText: "Contractors Program",
showInLegend: true, 
dataPoints:result.type
},
{
type: "column", 
name:"Regular Visits",
indexLabel: "{y}",
indexLabelFontSize: 16,
indexLabelFontColor: "black",
legendText: "Regular Visits",
showInLegend: true, 
dataPoints:result.salle
}

],
legend:{
fontSize: 15,
cursor:"pointer",
itemclick: function(e){
if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
e.dataSeries.visible = false;
}
else {
e.dataSeries.visible = true;
}
chart.render();
}
},
});
chart.render();
});

$.getJSON("http://10.43.144.19/offlineneew/public/chartadminpanel", function(result) {



var chart = new CanvasJS.Chart("canvaspromoter",
{
zoomEnabled: true,
theme: "theme3",
dataPointWidth: 20,
exportEnabled: true,
animationEnabled: true,

toolTip: {
shared: true
},
axisY:{

},      

data: [ 
{
type: "column", 
name: "Total Visits",
legendText: "Total Visits",
indexLabel: "{y}",
showInLegend: true,
indexLabelFontSize: 16,
indexLabelFontColor: "black", 
dataPoints:result.totalvisit


},{
type: "column", 
name: "Total Contractor",
legendText: "Total Contractor",
showInLegend: true, 
indexLabel: "{y}",
indexLabelFontSize: 16,
indexLabelFontColor: "black",
dataPoints:result.totalcontractor


}

],
legend:{
fontSize: 15,
cursor:"pointer",
itemclick: function(e){
if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
e.dataSeries.visible = false;
}
else {
e.dataSeries.visible = true;
}
chart.render();
}
},
});

chart.render();

});
$.getJSON("http://10.43.144.19/offlineneew/public/chartadminpanelUpper", function(result) {



var chart = new CanvasJS.Chart("canvaspromoterUpper",
{
zoomEnabled: true,
theme: "theme3",
dataPointWidth: 20,
exportEnabled: true,
animationEnabled: true,

toolTip: {
shared: true
},
axisY:{

},      

data: [ 
{
type: "column", 
name: "Total Visits",
legendText: "Total Visits",
indexLabel: "{y}",
showInLegend: true,
indexLabelFontSize: 16,
indexLabelFontColor: "black", 
dataPoints:result.totalvisit


},{
type: "column", 
name: "Total Contractor",
legendText: "Total Contractor",
showInLegend: true, 
indexLabel: "{y}",
indexLabelFontSize: 16,
indexLabelFontColor: "black",
dataPoints:result.totalcontractor


}

],
legend:{
fontSize: 15,
cursor:"pointer",
itemclick: function(e){
if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
e.dataSeries.visible = false;
}
else {
e.dataSeries.visible = true;
}
chart.render();
}
},
});

chart.render();
});
};
updateChart();
setInterval(function(){updateChart()}, 30000);




setInterval( function()
{

$.getJSON("http://10.43.144.19/offlineneew/public/charttotal", function(data) {




oTable.fnClearTable();

$.each(data.login, function(index,value) { 

oTable.fnAddData([ value.Government,value.Name,value.NTime]);  


});




// console.log(data.visits.visits);


$totalvisit=data.visits;
$totalcontractor=data.contractors.code;
$totalpormoter=data.totalpormoter.Name;
$totalorder=data.totalorder.type;
$target_pr=40;
$target_co=220;
$target_or=40;
$target_vi=230;

$achieve_wo=(($totalpormoter/$target_pr)*100).toFixed(1);     
$achieve_con= (($totalcontractor/$target_co)*100).toFixed(1);    
$achieve_or=(($totalorder/$target_or)*100).toFixed(1);     
$achieve_visits=(($totalvisit/$target_vi)*100).toFixed(1);  




$('#pormoter strong').text(function( ) {

if($totalpormoter < ($target_pr/2)){

$(this).text('  '+ $totalpormoter).css('color','#F7C0BA') ; 
$('#target_pr strong').text(''+$target_pr);
$('#achived strong').text(' '+ $achieve_wo).css('color','#F7C0BA') ; 
$("#achived ").removeClass("up").addClass("down");
if( $achieve_wo <=30){

$('.panel-pormoter').css({'background-color':'#C80404','color':'white'});


}
else
{
$('.panel-pormoter').css({'background-color':'gold','color':'white'});

$(this).css('color','red') ; 
$('#achived strong').text(' '+ $achieve_wo).css('color','red') ; 




}


}
else
{
$('.panel-pormoter').css({'background-color':'#00a63f','color':'white'});
$(this).text('  '+ $totalpormoter).css('color','white') ; 
$('#target_pr strong').text(''+$target_pr);
$('#achived strong').text(' '+ $achieve_wo).css('color','white') ; 
$("#achived ").removeClass("down").addClass("up");   
$("#achived li").removeClass("fa-caret-down").addClass("fa-caret-up"); 
}

});

$('#contractor strong').text(function( ) {

if($totalcontractor < ($target_co/2))
{

$(this).text('  '+ $totalcontractor).css('color','#F7C0BA') ; 
$('#target_cont strong').text(''+$target_co);
$('#achived_cont strong').text(' '+ $achieve_con).css('color','#F7C0BA') ; 
$("#achived_cont ").removeClass("up").addClass("down");
if( $achieve_con <=30){

$('.panel-contractor').css({'background-color':'#C80404','color':'white'});

}
else
{
$('.panel-contractor').css({'background-color':'gold','color':'white'});
$(this).css('color','red') ; 

$('#achived_cont strong').text(' '+ $achieve_con).css('color','red') ; 
}


}
else
{
$('.panel-contractor').css({'background-color':'#00a63f','color':'white'});
$(this).text('  '+ $totalcontractor).css('color','white') ; 
$('#target_cont strong').text(''+$target_co);
$('#achived_cont strong').text(' '+ $achieve_con).css('color','white') ; 
$("#achived_cont ").removeClass("down").addClass("up");   
$("#achived_cont li").removeClass("fa-caret-down").addClass("fa-caret-up"); 

}

});

$('#order strong').text(function( ) {

if($totalorder < ($target_or/2))
{

$(this).text('  '+ $totalorder).css('color','#F7C0BA') ; 
$('#target_ord strong').text(''+$target_or);
$('#achived_ord strong').text(' '+ $achieve_or).css('color','#F7C0BA') ; 
$("#achived_ord ").removeClass("up").addClass("down");

if($achieve_or < 30 ){

$('.panel-order').css({'background-color':'#C80404','color':'white'});

}
else
{
$('.panel-order').css({'background-color':'gold','color':'white'});
$(this).css('color','red') ; 

$('#achived_ord strong').text(' '+ $achieve_or).css('color','red') ; 
}


}
else
{
$('.panel-order').css({'background-color':'#00a63f','color':'white'});

$(this).text('  '+ $totalorder).css('color','white') ; 
$('#target_ord strong').text(''+$target_or);
$('#achived_ord strong').text(' '+ $achieve_or).css('color','white') ; 
$("#achived_ord  ").removeClass("down").addClass("up");   
$("#achived_ord  li").removeClass("fa-caret-down").addClass("fa-caret-up"); 




}

});

$('#visit strong').text(function( ) {

if($totalvisit < ($target_vi/2))
{

$(this).text('  '+ $totalvisit).css('color','#F7C0BA') ; 
$('#target_vis strong').text(''+$target_vi);
$('#achived_vis strong').text(' '+ $achieve_visits).css('color','#F7C0BA') ; 
$("#achived_vis ").removeClass("up").addClass("down");

if($achieve_visits < 30 ){
$('.panel-visit').css({'background-color':'#C80404','color':'white'});

}
else
{
$('.panel-visit').css({'background-color':'gold','color':'white'});

$(this).css('color','red') ; 

$('#achived_vis strong').text(' '+ $achieve_visits).css('color','red') ; 
}


}
else
{
$('.panel-visit').css({'background-color':'#00a63f','color':'white'});

$(this).text('  '+ $totalvisit).css('color','white') ; 
$('#target_vis strong').text(''+$target_vi);
$('#achived_vis strong').text(' '+ $achieve_visits).css('color','white') ; 
$("#achived_vis  ").removeClass("down").addClass("up");   
$("#achived_vis  li").removeClass("fa-caret-down").addClass("fa-caret-up"); 


}

});

});
} ,10000);

});


</script>
@endsection