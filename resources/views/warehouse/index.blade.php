@extends('masternew')
@section('content')
 <section class="panel-body">
<style type="text/css">
  
</style>
 <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                     <h2><i class="fa fa-tasks"></i> Warehouses</h2> 
                        <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                     
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

  
<a type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal1" style="margin-bottom: 20px;"> Add New Warehouse </a>



     <!-- model -->
 
<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
     <div class="modal-content">
    
          <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                 <h4 class="modal-title span7 text-center" id="myModalLabel" style="    text-align: center;
    color: #15202a;
    font-size: 20px;
    font-weight: bold;"><span class="title">Add New Warehouse</span></h4>
          </div>
    <div class="modal-body">
        <div class="tabbable"> <!-- Only required for left/right tabs -->
        <ul class="nav nav-tabs">
  

        
        </ul>
        <div class="tab-content">
        <div class="tab-pane active" id="tab1">
              <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                 
                  <div class="x_content">
                    <br />
                   {!! Form::open(['action' => "WarehouseController@store",'method'=>'post','id'=>'profileForm']) !!} 
             <fieldset style="    padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;">
  <legend style="width: fit-content;border: 0px;">Main Date:</legend>
 

 
                    <div class="form-group">

                      <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                        <input type="text" class="form-control has-feedback-left"  id="Name" name="Name" placeholder="Warehouse Name" style=" border: 1px solid #15202a;border-radius: 4px; margin-bottom: 9px;">


                        <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                      </div>
                     

                        <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                          <input type="text" class="form-control has-feedback-left" id="EName" name="EName" placeholder="English Name" style=" border: 1px solid #15202a;border-radius: 4px; margin-bottom: 9px;">
                                       <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>

                        </div>
                      </div>

                   
                    
                      
</fieldset>
        <fieldset style="    padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;">
  <legend style="width: fit-content;border: 0px;">Locations:</legend>


       

                      
                          <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                          <input type="text" class="form-control has-feedback-left" id ="Address" name="Address" placeholder="Warehouse Address" style=" border: 1px solid #15202a;border-radius: 4px; margin-bottom: 9px;">
                        <span class="fa fa-map-marker form-control-feedback left" aria-hidden="true"></span>
                        </div>
                     
                        <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                          <input type="text" class="form-control has-feedback-left" id ="Location" name="Location" placeholder="Warehouse Location" style=" border: 1px solid #15202a;border-radius: 4px; margin-bottom: 9px;">
                        <span class="fa fa-map-marker form-control-feedback left" aria-hidden="true"></span>
                        </div>
                      </div>

 <div class="form-group">
                      <div class="control-label col-md-6 col-sm-6 col-xs-12" style="padding-top: 10px">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Government</label>
                        <div class="col-md-9 col-sm-9 col-xs-12" style=" border: 1px solid #15202a;border-radius: 4px; margin-bottom: 9px;">
                          {!!Form::select('Government', ([null => 'Select  Name'] + $Government->toArray()) ,null,['class'=>' chosen-select','id' => 'Government'])!!}
                        </div>
                     </div>
                       <div class="control-label col-md-6 col-sm-6 col-xs-12" style="padding-top: 10px">

                        <label class="control-label col-md-3 col-sm-3 col-xs-12">District
                        </label>
                        <div class="col-md-9 col-sm-9 col-xs-12" style=" border: 1px solid #15202a;border-radius: 4px; margin-bottom: 9px;">
                      {!!Form::select('District', ([null => 'Select  Name'] + $District->toArray()) ,null,['class'=>'form-control chosen-select District ','id' => 'prettify','name'=>'District','required'=>'required'])!!}
                        </div>
                        </div>

                      </div>
  

                      </fieldset>

           


                       


                      <div class="ln_solid"></div>
                      <div class="form-group">
                <div class="col-md-12 col-sm-12 col-xs-12 " style="text-align:center;">
                          <button type="button" class="btn btn-danger " data-dismiss="modal">Cancel</button>
                         <button class="btn btn-info" type="reset">Reset</button>
                          <button type="submit" disabled="disabled" name="login_form_submit" class="btn btn-success" value="Save">Submit</button>
                        </div>
                      </div>

      {!!Form::close() !!}                
        </div>
                </div>
                   </div>
                </div>
        </div>
        </div>
        </div>

    </div>
 

         </div>
     </div>
  </div>
<!-- ENDModal -->
 



                   
                    <table class="table table-hover table-bordered  dt-responsive nowrap  warehouse" cellspacing="0" width="100%">
  <thead>
  <th></th>
                <th>Name</th>
              <th>EnglishName</th>
                  <th>Government</th>
                    <th>District</th>
                <th>Address</th>
                 <th>Location</th>  

                   <th>Process</th>  
                 </thead>
                 <tfoot>
                    <th></th>
                <th>Name</th>
              <th>EnglishName</th>
                  <th>Government</th>
                    <th>District</th>
                <th>Address</th>
                 <th>Location</th>  

                   <th>Process</th>
</tfoot>


<tbody style="text-align:center;">
<?php $i=1;?>
 @foreach($warehouse as $warehouse)
    <tr>
    
     <td>{{$i++}}</td>
        
        
     
         <td ><a  class="testEdit" data-type="text" data-name="Name"  data-pk="{{ $warehouse->id }}">{{$warehouse->Name}}</a></td>
         <td ><a  class="testEdit" data-type="text" data-name="EngName"  data-pk="{{ $warehouse->id }}">{{$warehouse->EngName}}</a></td>
    <td>   <a class="testEdit" data-type="select"  data-value="{{$Government}}" data-source ="{{$Government}}" data-name="Government"  data-pk="{{$warehouse->id}}"> 
 {{$warehouse->Government}}
  
  </a></td>

   <td>   <a class="testEdit" data-type="select"  data-value="{{$District}}" data-source ="{{$District}}" data-name="District"  data-pk="{{$warehouse->id}}"> 
 {{$warehouse->District}}
  
  </a></td>
         <td ><a  class="testEdit" data-type="text" data-name="address"  data-pk="{{ $warehouse->id }}">{{$warehouse->address}}</a></td>
         <td ><a  class="testEdit" data-type="text" data-name="location"  data-pk="{{ $warehouse->id }}">{{$warehouse->Location}}</a></td>

       <td>
   
       <a href="/MIS/warehouse/destroy/{{$warehouse->id}}"class="btn btn-default btn-sm" ><span class="glyphicon glyphicon-trash"></span>  </a>


  </td>

    
 
   </tr>
     @endforeach
</tbody>
</table>


                  </div>
                </div>
              </div>
 
  @section('other_scripts')


<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>


<script>
$(document).ready(function() {
     $('.District').chosen({
                width: '100%',
                no_results_text:'No Results'
            });
                $(".District").trigger("chosen:updated");
            $(".District").trigger("change");
    $('#profileForm')
        .find('[name="Government"]')
            .chosen({
                width: '100%',
                inherit_select_classes: true
            })
            // Revalidate the color when it is changed
            .change(function(e) {

                $('#profileForm').formValidation('revalidateField', 'Government');

  $.getJSON("./government/district/" + $("#Government").val(), function(data) {
 
                var $contractors = $(".District");
                   $(".District").chosen("destroy");
                $contractors.empty();
                $.each(data, function(index, value) {
                    $contractors.append('<option value="' + index +'">' + value + '</option>');
                });
                  
        
             $('.District').chosen({
                width: '100%',
                no_results_text:'No Results'
            });
                $(".District").trigger("chosen:updated");
            $(".District").trigger("change");
            });
            })
            .end()  
        .formValidation({
            framework: 'bootstrap',
            excluded: ':disabled',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                Government: {
                    validators: {
                        callback: {
                            message: 'Please choose Government',
                            callback: function(value, validator, $field) {
                                // Get the selected options
                                var options = validator.getFieldElements('Government').val();
                              
                                return (options !== '' );
                            }
                        }
                    }
                },
                   'Name':{
                validators: {
                    notEmpty: {
                        message: 'The  name is required'
                    },
                     regexp: {
                        regexp: /^[\u0600-\u06FF\s]+$/,
                        message: 'The full name can only consist of Arabic alphabeticals and spaces '
                     },
                      remote: {
                        url: '/MIS/checkArabicnameWarehouse',
                        
                        message: 'The Name  is not available',
                        type: 'GET'
                    }
                }
            },
                'Address': {
                   validators: {
                        regexp: {
                        regexp: /^[a-zA-Z\u0600-\u06FF,-][\sa-zA-Z\u0600-\u06FF,-]/,
                        message: 'This field  can only consist of  alphabeticals and spaces'
                    },
                   notEmpty:{
                     message:"the Address is required"
                    }
                     }
                },
                'EName': {
                   validators: {
                   notEmpty:{
                     message:"the EName is required"
                    },
                    regexp: {
                        regexp: /^[a-zA-Z\s]+$/,
                        message: 'This field  can only consist of  alphabeticals and spaces'
                    },
                      remote: {
                        url: '/MIS/checkEnglisnameWarehouse',
                        
                        message: 'The English name is not available',
                        type: 'GET'
                    }
                     }
                },
               'Location': {
                   validators: {
                     regexp: {
                        regexp: /^[a-zA-Z\u0600-\u06FF,-][\sa-zA-Z\u0600-\u06FF,-]/,
                        message: 'This field  can only consist of  alphabeticals and spaces'
                    },
                   notEmpty:{
                     message:"the Location is required"
                    }
                     }
                },
                'District': {
                   validators: {

                   notEmpty:{
                     message:"the District is required"
                    }
                     }
                }
            }
        });
});
</script>


  <script type="text/javascript">

var editor;

  $(document).ready(function(){

  



     var table= $('.warehouse').DataTable({
  
    select:true,
    responsive: true,
    "order":[[0,"asc"]],
    'searchable':true,
    "scrollCollapse":true,
    "paging":true,
    "pagingType": "simple",
      dom: 'lBfrtip',
        buttons: [
           
            
            { extend: 'excel', className: 'btn btn-primary dtb' }
            
            
        ],

fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
$.fn.editable.defaults.send = "always";

$.fn.editable.defaults.mode = 'inline';

$('.testEdit').editable({

      validate: function(value) {
        name=$(this).editable().data('name');
        if(name=="Name"||name=="address"||name=="location"||name=="EngName")
        {
                if($.trim(value) == '') {
                    return 'Value is required.';
                  }
                    }
      
       
         if(name=="EngName")
        {

      var regexp = /^[a-zA-Z\s]+$/;            
      if (!regexp.test(value)) {
            return 'This field is not valid';
        }
        
        }
           if(name=="Name")
        {

      var regexp = /^[\u0600-\u06FF\s]+$/;            
      if (!regexp.test(value)) {
            return 'This field is not valid';
        }
        
        }
           if(name=="address"||name=="location")
        {

      var regexp = /^[a-zA-Z\u0600-\u06FF,-][\sa-zA-Z\u0600-\u06FF,-]/;            
      if (!regexp.test(value)) {
            return 'This field is not valid';
        }
        
        }
    },
       
    placement: 'right',
    url:'{{URL::to("/")}}/updatewarehouse',
  
     ajaxOptions: {
     type: 'get',
    sourceCache: 'false',
     dataType: 'json'

   },

       params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');

            // console.log(params);
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'This Data Already Exist,Enter Correct Data.';
            } else {
                return response.responseText;
            }
        }
        ,
        success:function(response)
        {
            if(response.status==="You do not have permission.")
             {
              return 'You do not have permission.';
              }
        }


});

}
});

 



   $('.warehouse tfoot th').each(function () {



      var title = $('.warehouse thead th').eq($(this).index()).text();
               
 if($(this).index()>=1 && $(this).index()<=6)
        {

           $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
}
        

    });
  table.columns().every( function () {
  var that = this;
 $(this.footer()).find('input').on('keyup change', function () {
          that.search(this.value).draw();
            if (that.search(this.value) ) {
               that.search(this.value).draw();
           }
        });
      
    });

 $(".Government").trigger("chosen:updated");

    $('[data-toggle="popover"]').popover({ trigger: "hover" }); 
 });

  </script>
@stop
@stop
