@extends('master')
@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}" />
<style type="text/css">

.error{
  color: #a51c1c;
}
</style>

<section class="panel panel-primary">

<div class="panel-body">

  <h1><i class="fa fa-lightbulb-o" aria-hidden="true"></i> Products </h1>


<table class="table table-hover table-bordered  dt-responsive nowrap display products" cellspacing="0" width="100%">
  <thead>
              <th>No</th>
                <th>Product Name</th>
                     <th>available</th>
                      <th>Government</th>

                         <th>Action</th>            
  </thead>
    <tfoot>
        
                  <th></th> 
                    <th>Product Name</th>
                     <th>available</th>
                    <th>Government</th>

                            <th></th>
            
                  </tfoot>
               

<tbody style="text-align:center;">
  <?php $i=1;?>
  @foreach($sales as $product)
    <tr>
    
     <td>{{$i++}}</td>
         <td>
          <a  >{{$product->Name}}</a></td>
         

         <td ><a  class="testEdit" data-type="select"  data-value="{{$product->available}}"data-source ="[{'value':'available', 'text': 'available'}, {'value':'not available', 'text': 'not available'}]" data-name="available"  data-pk="{{ $product->id }}">{{$product->available}}</a></td>
    
  <td ><a>{{$product->Government}}</a></td>     
           <td>
   
            <a href="/offlineneew/public/products/destroy/{{$product->id}}"class="btn btn-default btn-sm" ><span class="glyphicon glyphicon-trash"></span>  </a>


             </td>
   </tr>
     @endforeach
</tbody>
</table>

</div>
    
    </section>
  


<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>


  <script type="text/javascript">

var editor;

  $(document).ready(function(){
     var table= $('.products').DataTable({
  
   
    responsive: true,
    "order":[[0,"asc"]],
    'searchable':true,
    "scrollCollapse":true,
    "paging":true,
    "pagingType": "simple",
      dom: 'lBfrtip',
        buttons: [
           
            
            { extend: 'excel', className: 'btn btn-primary dtb' }
            
            
        ],


   

fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
$.fn.editable.defaults.send = "always";

$.fn.editable.defaults.mode = 'inline';

$('.testEdit').editable({

      validate: function(value) {
        name=$(this).editable().data('name');
        if(name=="Name"||name=="type")
        {
                if($.trim(value) == '') {

                    return 'Value is required.';
                  }
         }
        if(name=="Name")
        {

              var regexp = /^[a-zA-Z\u0600-\u06FF,-][\sa-zA-Z\u0600-\u06FF,-]/;           

                  if (!regexp.test(value)) {
                        return 'This field is not valid';
                    }
        
         }

        },

    url:'{{URL::to("/")}}/Drfupdate',
  
     ajaxOptions: {
     type: 'get',
    sourceCache: 'false',
     dataType: 'json'

   },

       params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');

            // console.log(params);
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'This Data Already Exist,Enter Correct Data.';
            } else {
                return response.responseText;
            }
        }


});

}
});

 



   $('.products tfoot th').each(function () {



      var title = $('.products thead th').eq($(this).index()).text();
               
 if($(this).index()>=1 && $(this).index()<=3)
        {

           $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
}
        

    });
  table.columns().every( function () {
  var that = this;
 $(this.footer()).find('input').on('keyup change', function () {
          that.search(this.value).draw();
            if (that.search(this.value) ) {
               that.search(this.value).draw();
           }
        });
      
    });



$.validator.addMethod("alpha", function(value, element) {
    return this.optional(element) || /^[a-zA-Z\u0600-\u06FF,-][\sa-zA-Z\u0600-\u06FF,-]/i.test(value);
 });

$.validator.addMethod("English", function(value, element) {

    return this.optional(element) || /^[\w&_\-'\s"\\,.\/]*$/.test(value);
 });


 $("#form").validate({
        rules: {

            Name: {
                        
                        required: true,
               
                          alpha:true,
                          remote: {
                                      url: "/offlineneew/public/Name",
                                     type: "get",
                                      data: {
                                      Name: function() {
                                           return $( "#name" ).val();
                                           }
                                          },
                                      beforeSend: function (request) {
                                      return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                                         }
                                      }
                            },
             Enname: {
                        
                        required: true,
               
                          English:true,
                          remote: {
                                      url: "/offlineneew/public/ENGName",
                                      type: "get",
                                      data: {
                                      Name: function() {
                                           return $( "#Enname" ).val();
                                           }
                                          },
                                      beforeSend: function (request) {
                                      return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
                                         }
                                      }
                            }              
        },

        messages: {
            Name: {
                required: "Please enter Product Name",
                alpha: "Product Name must be at least 3 characters",
                remote:jQuery.validator.format("This Product Name {0} is Already Existed")

            },
              Enname: {
                required: "Please enter Product English Name",
                English: "Product Name must be English characters",
                remote:jQuery.validator.format("This Product Name {0} is Already Existed")

            }  

        }
    });















 });
  </script>











@stop

