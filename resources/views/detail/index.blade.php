@extends('masternew')
@section('content')
<!DOCTYPE html>
<section class="panel-body">
 <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Details</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                     
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

 


   <a type="button"  class="btn btn-primary" data-toggle="modal" data-target="#myModal1" style="margin-bottom: 20px;"> Add New Details</a>
  
     <!-- model -->
 
<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
     <div class="modal-content">
    
          <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                 <h4 class="modal-title span7 text-center" id="myModalLabel"><span class="title">Add New Details</span></h4>
          </div>
    <div class="modal-body">
        <div class="tabbable"> <!-- Only required for left/right tabs -->
        <ul class="nav nav-tabs">
  

        
        </ul>
        <div class="tab-content">
        <div class="tab-pane active" id="tab1">
              <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                 
                  <div class="x_content">
                    <br />
{!! Form::open(['action' => "DetailsController@store",'method'=>'post','id'=>'profileForm', 'class'=>'form-horizontal form-label-left  ']) !!} 



<fieldset style="    padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;">
  <legend style="width: fit-content;border: 0px;">Main Data:</legend>
    <div class="form-group">


      <div class="control-label col-md-6 col-sm-6 col-xs-12">

                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Product Name
                        </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                             {!!Form::select('product',([null => 'please chooose'] + $product->toArray()) ,null,['class'=>'form-control subuser_list chosen-select product','id' => 'product','required'=>'required'])!!}
                        </div>
                        </div>
                      <div class="control-label col-md-6 col-sm-6 col-xs-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"> Points</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                                    
 <input type="number" name="Points" placeholder="Enter your Points" class="form-control itemname_list" required />

 
</select>
                        </div>
                     </div>

   
                     

                      </div>
                      
<div class="form-group">
        <div class="control-label col-md-6 col-sm-6 col-xs-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"> Government</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                                    
   {!!Form::select('Government',([null => 'please chooose'] + $Government->toArray()) ,null,['class'=>'form-control subuser_list Government chosen-select','id' => 'Government','required'=>'required'])!!}
                        </div>
                     </div>


                     <div class="control-label col-md-6 col-sm-6 col-xs-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"> District</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                                    
  {!!Form::select('District[]',([null => 'please chooose'] ) ,null,['class'=>'form-control District chosen-select','id' => 'District','required'=>'required','multiple' => true])!!}


                        </div>
                     </div>
</div>
 

 

                 
</fieldset>
        


        

                    

                        
                       


                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                          <button type="button" class="btn btn-primary " data-dismiss="modal">Cancel</button>
                         <button class="btn btn-primary" type="reset">Reset</button>
                          <button type="submit"  name="login_form_submit" class="btn btn-success" value="Save">Submit</button>
                        </div>
                      </div>

                   
        {!!Form::close() !!}                
        </div>
                </div>
                   </div>
                </div>
        </div>
        </div>
        </div>

    </div>
 

         </div>
     </div>
  </div>
<!-- ENDModal -->
 
<!-- ENDModal -->

<table class="table table-hover table-bordered  dt-responsive nowrap display Brands" cellspacing="0" width="100%">
  <thead>
              <th>No</th>
              <th>Product Name</th>   
              <th>Government</th>     
              <th>District</th> 
              <th>Points</th> 
              <th>Process</th>          
  </thead>
    <tfoot>
        
              <th></th>
              <th>Product Name</th>   
              <th>Government</th>     
              <th>District</th> 
              <th>Points</th>
              <th>Process</th> 
                    
   </tfoot>
               

<tbody style="text-align:center;">
  <?php $i=1;?>
  @foreach($details as $detail)
    <tr>
    
     <td>{{$i++}}</td> 
      <td>
 <a class="testEdit" data-type="select"  data-value="{{$products}}" data-source ="{{$products}}" data-name="Product_id"  data-pk="{{$detail->id}}"> 
 {{$detail->get_deproduct->Enname}}
  
  </a>
 </td>
      <td>
{{$detail->Government}}
 </td>
     <td>

 {{$detail->District }}
  

 </td>
 <td><a  class="testEdit" data-type="number" data-name="Points"  data-pk="{{ $detail->id }}">{{$detail->Points}}</a></td>
  <td>
     <a href="/offlineneew/public/detail/destroy/{{$detail->id}}"class="btn btn-default btn-sm" ><span class="glyphicon glyphicon-trash"></span>
       </a>

</td>

   </tr>
     @endforeach
</tbody>
</table>
  </div>
                </div>
              </div>  
    </section>
@section('other_scripts')
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>


  <script type="text/javascript">

var editor;

  $(document).ready(function(){

     $('#District').chosen({
                width: '100%',
                no_results_text:'No Results'
            });
                $("#District").trigger("chosen:updated");
            $("#District").trigger("change");

     $('#product').chosen({
                width: '100%',
                no_results_text:'No Results'
            });
                $("#product").trigger("chosen:updated");
            $("#product").trigger("change");


    $('#Government').change(function(){
        $.get("{{ url('api/dropdown')}}", 
        { option: $(this).val() }, 
        function(data) {
            $("#District").chosen("destroy");
            $.each(data, function(key, element) {
                $('#District').append("<option value='" + key +"'>" + element + "</option>");
            });
             $("#District").chosen({ 
                   width: '100%',
                   no_results_text: "There is no result",
                   allow_single_deselect: true, 
                   search_contains:true, });
 $("#District").trigger("chosen:updated");
        });
    });

$('#profileForm')
        .find('[name="Government"]')
            .chosen({
                width: '100%',
                inherit_select_classes: true
            })
            // Revalidate the color when it is changed
            .change(function(e) {

                $('#profileForm').formValidation('revalidateField', 'Government');

  $.getJSON("/offlineneew/public/government/district/" + $("#Government").val(), function(data) {
 
                var $contractors = $(".District");
                   $(".District").chosen("destroy");
                $contractors.empty();
                $.each(data, function(index, value) {
                    $contractors.append('<option value="' + index +'">' + value + '</option>');
                });
                  
        
             $('.District').chosen({
                width: '100%',
                no_results_text:'No Results'
            });
                $(".District").trigger("chosen:updated");
            $(".District").trigger("change");
            });
            }).end()
        .formValidation({
            framework: 'bootstrap',
            excluded: ':disabled',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                Government: {
                    validators: {
                        callback: {
                            message: 'Please choose Government',
                            callback: function(value, validator, $field) {
                                // Get the selected options
                                var options = validator.getFieldElements('Government').val();
                              
                                return (options !== '' );
                            }
                        }
                    }
                },
                'Points':
                 {
                    validators: {
                    notEmpty:{
                    message:"the Points is required"
                  },
                    numric: {
                        message: 'The Points must be  number'
                    } 
                    }
                },

          
                        
                    'District[]': {
                    validators: {
                     
        
             notEmpty:{
                    message:"the District is required"
                  } 
                    }
                },

            
         
            }
        });

     var table= $('.Brands').DataTable({
  
    select:true,
    responsive: true,
    "order":[[0,"asc"]],
    'searchable':true,
    "scrollCollapse":true,
    "paging":true,
    "pagingType": "simple",
      dom: 'lBfrtip',
        buttons: [
           
            
            { extend: 'excel', className: 'btn btn-primary dtb' }
            
            
        ],

fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
$.fn.editable.defaults.send = "always";

$.fn.editable.defaults.mode = 'inline';

$('.testEdit').editable({

      validate: function(value) {
        name=$(this).editable().data('name');
        if(name=="Points")
        {
                if($.trim(value) == '') {
                    return 'Value is required.';
                  }
                    }

      
        },

    placement: 'right',
    url:'{{URL::to("/")}}/Drfupdate',
  
     ajaxOptions: {
     type: 'get',
    sourceCache: 'false',
     dataType: 'json'

   },

       params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');

            // console.log(params);
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'This Data Already Exist,Enter Correct Data.';
            } else {
                return response.responseText;
            }
        }
        ,    success:function(response)
        {
            if(response.status==="You do not have permission.")
             {
              return 'You do not have permission.';
              }
        }


});

}
});
   $('.Brands tfoot th').each(function () {
      var title = $('.Brands thead th').eq($(this).index()).text();       
     if($(this).index()>=1 && $(this).index()<=6)
            {

           $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
}
        

    });
  table.columns().every( function () {
  var that = this;
 $(this.footer()).find('input').on('keyup change', function () {
          that.search(this.value).draw();
            if (that.search(this.value) ) {
               that.search(this.value).draw();
           }
        });
      
    });

 });

</script>




@stop
@stop