@extends('masternew')
@section('content')
<?php
//File
  if(!empty($_COOKIE['FileError'])) {     
    echo "<div class='alert alert-block alert-danger fade in center'>";
    echo $_COOKIE['FileError'];
    echo "</div>";
  } 
//ActivityErr 
  if(!empty($_COOKIE['ActivityErr'])) {      
    echo "<div><div class='alert alert-block alert-danger fade in center'>";
    echo $_COOKIE['ActivityErr'];
    echo "</div> </div>";
  } 
//EmptyActivityErr 
  if(!empty($_COOKIE['EmptyActivityErr'])) {     
    echo "<div><div class='alert alert-block alert-danger fade in center'>";
    echo $_COOKIE['EmptyActivityErr'];
    echo "</div> </div>";
  } 

?>
<style type="text/css">
/*    input[type="file"] {
    display: block;
   }
*/

.error{
  color: #a51c1c;
}
</style>
<section class="panel-body">
 <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><i class="fa fa-lightbulb-o" aria-hidden="true"></i> Multi Product </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                     
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
<div class="panel-body">
  <a type="button" class="btn btn-primary" data-toggle="modal" data-target="#activityModal" style="margin-bottom: 20px;"> Add New Multi Product </a>










<!-- model -->
<div class="modal fade" id="activityModal" tabindex="-1" role="dialog" aria-labelledby="activityModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
         <h4 class="modal-title span7 text-center" id="activityModalLabel">
         <span class="title">Add Multi Product</span></h4>

    </div>
    <div class="modal-body">

 <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                 
                  <div class="x_content">
                    <br />
        {!! Form::open(['action' => "sale_productController@store",'method'=>'post','id'=>'profileForm','files'=>'true','class'=>'form-horizontal form-label-left ']) !!}
       <!--  <input type="hidden" name="_token" value="{{ csrf_token() }}"> -->

   

   <fieldset style=" padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;">
   <legend style="width: fit-content;border: 0px;">Main Data:</legend>

        
           

      <div class="form-group">

   <div class="control-label col-md-6 col-sm-6 col-xs-12">
                        <div>
                  
                         <input type="text" class="form-control" name="name" placeholder="Enter Sales Product Name"/ required>
                        </div>
                          
                     </div>
                   
 <div class="col-md-6 col-sm-6 col-xs-12 ">
                        <input class="form-control" type="text" name="Engname" placeholder="Enter Sales Product English Name" />
                           
                           </div>

                    
                   
                    </div>

                        <div class="form-group">
  <div class="col-md-6 col-sm-6 col-xs-12">
                       

                   <input type="number" class="form-control " id="Address" name="Code" placeholder="Enter Code" required>
                    
                        </div>


                          
                      
                        </div>

      
                    

                     </fieldset>


   <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                          <button type="button" class="btn btn-primary " data-dismiss="modal">Cancel</button>
                         <button class="btn btn-primary" type="reset">Reset</button>
                          <button type="submit"  name="login_form_submit" class="btn btn-success" value="Save">Submit</button>
                        </div>
                      </div>

      {!!Form::close() !!}
    </div>
    </div>
     </div>
    </div>
    </div>
  </div>
  </div>
  </div>

<table class="table table-hover table-bordered  dt-responsive nowrap display tasks" cellspacing="0" width="100%">
      <thead>
          <th>No</th>
          <th>Name</th>
           <th>Code</th>
         <!--  <th>English Name</th> -->
          
          <th>Show Details</th> 
          <th>Process</th>          
      </thead>
      <tfoot>
        <th></th>
        <th>Name</th>
         <th>Code</th>
       <!--  <th>English Name</th> -->
      
          <th>Show Details</th> 
        <th>Process</th>        
      </tfoot>               

      <tbody style="text-align:center;">
        <?php $i=1;?>
        @foreach($sale_products as $sale_product)
          <tr>
            <td>{{$i++}}</td>   
            <td><a  class="testEdit" data-type="text" data-name="Name"  data-pk="{{ $sale_product->id }}">{{$sale_product->Name}}</a></td>
             <td>{{$sale_product->Code}}</td>
            <!-- <td><a  class="testEdit" data-type="text" data-name="EngName"  data-pk="{{ $sale_product->id }}">{{$sale_product->EngName}}</a></td> -->
        
            <td><a href="/offlineneew/public/saleproductdetails/{{$sale_product->id}}"class="btn btn-default btn-sm" ><span class="glyphicon glyphicon-plus"></span>  </a></td>
            <td><a href="/offlineneew/public/sale_products/destroy/{{$sale_product->id}}"class="btn btn-default btn-sm" ><span class="glyphicon glyphicon-trash"></span>  </a></td>
          </tr>
        @endforeach
      </tbody>
    </table> 
</div>
 </div>
 </div>   
    </section>
  
  @section('other_scripts')


<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>


  <script type="text/javascript">

var editor;

  $(document).ready(function(){
    $('#profileForm')
        .formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                'name': {
                    validators: {
                            remote: {
                        url: '/offlineneew/public/checknamesaleproduct',
                        message: 'The Sale_Product name is not available',
                        type: 'GET'
                    },
                      notEmpty:{
                    message:"the  Sale_Product name is required"
                  }
                    ,
                    regexp: {
                        regexp: /^[\u0600-\u06FF0-9\s\\.]+$/,
                        message: 'The Sale_Product name can only consist of alphabetical and spaces'
                    }
                    }
                },'Code': {
                    validators: {
                         remote: {
                        url: '/offlineneew/public/checksaleProductCode',
    
                        message:'Sale_Product Already Exist',
                        type: 'GET'
                    } 
                    }
                }
                ,'Engname':
                 {
                    validators: {
                            remote: {
                       url: '/offlineneew/public/checkenglishnamesaleproduct',
                       message: 'The sale_product English name is not available',
                         type: 'GET'
                     },
                       notEmpty:{
                    message:"the  sale_product English name is required"
                  },
                    regexp: {
                        regexp: /^[A-Za-z\s0-9\\.]+$/,
                        message: 'The English name  can only consist of alphabetical and spaces'
                    }

                    }
                } 
            } ,submitHandler: function(form) {
        form.submit();
    }
        })
        .on('success.form.fv', function(e) {
   // e.preventDefault();
    var $form = $(e.target);

    // Enable the submit button
   
    $form.formValidation('disableSubmitButtons', false);
});

     var table= $('.tasks').DataTable({
  
    select:true,
    responsive: true,
    "order":[[0,"asc"]],
    'searchable':true,
    "scrollCollapse":true,
    "paging":true,
    "pagingType": "simple",
      dom: 'lBfrtip',
        buttons: [
           
            
            { extend: 'excel', className: 'btn btn-primary dtb' }
            
            
        ],

fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
$.fn.editable.defaults.send = "always";

$.fn.editable.defaults.mode = 'inline';

$('.testEdit').editable({
  validate: function(value) {
        name=$(this).editable().data('name');
        if(name=="Name"||name=="EngName")
        {
                if($.trim(value) == '') {
                    return 'Value is required.';
                  }
        }
        if(name=="Name")
        {

      var regexp = /^[\u0600-\u06FF\s]+$/;            if (!regexp.test(value)) {
            return 'This field is not valid';
        }
        
        }
         if(name=="EngName")
        {

      var regexp = /^[a-zA-Z\s]+$/;  
                if (!regexp.test(value)) {
            return 'This field is not valid';
        }
        
        }
      
        },

    placement: 'right',
    url:'{{URL::to("/")}}/updatesaleproduct',
  
     ajaxOptions: {
     type: 'get',
    sourceCache: 'false',
     dataType: 'json'

   },

       params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');

            // console.log(params);
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'This Data Already Exist,Enter Correct Data.';
            } else {
                return response.responseText;
            }
        }
        ,    success:function(response)
        {
            if(response.status==="You do not have permission.")
             {
              return 'You do not have permission.';
              }
        }


});

}
});

 



   $('.tasks tfoot th').each(function () {



      var title = $('.tasks thead th').eq($(this).index()).text();
               
 if($(this).index()>=1 && $(this).index()<=6)
        {

           $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
}
        

    });
  table.columns().every( function () {
  var that = this;
 $(this.footer()).find('input').on('keyup change', function () {
          that.search(this.value).draw();
            if (that.search(this.value) ) {
               that.search(this.value).draw();
           }
        });
      
    });


 });

  </script>











@stop

@stop

