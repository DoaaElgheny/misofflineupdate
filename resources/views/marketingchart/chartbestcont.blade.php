@extends('masternew')
@section('content')
  <section class="panel-body">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2><i class="fa fa-tasks"></i>Charts</h2> 
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
            <li><a class="close-link"><i class="fa fa-close"></i></a></li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <div class="row" >
            <div class="col-sm-12 col-md-10 col-md-offset-1 form-horizontal"  > 
              <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                  <label class="col-xs-3 control-label">Start Date:</label>
                  <div class="col-xs-9">
                    <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                    <input type="text"  placeholder="Date"  class="form-control has-feedback-left ClassDate" name="StartDate" id="StartDate" placeholder="enter Start Date " required>     
                  </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                  <label class="col-xs-3 control-label">End Date:</label>
                  <div class="col-xs-9">
                    <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                    <input type="text" placeholder="Date"   class="form-control has-feedback-left ClassDate" name="EndDate" id="EndDate" placeholder="enter End Date " required>
                  </div>
                </div>
              </div> 
              <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                  <label class="col-xs-3 control-label">Government:</label>
                  <div class="col-xs-9">
                    {!!Form::select('Government', ([null => 'Select  Government'] + $Government->toArray()) ,null,['class'=>'form-control chosen-select ','id' => 'Government'])!!}
                  </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                  <label class="col-xs-3 control-label">District:</label>
                  <div class="col-xs-9">
                    {!!Form::select('district[]',  ([null => 'Select  District'] )  ,null,['class'=>'form-control chosen-select product ','id' => 'district','name'=>'district','multiple' => true,'required'=>'required'])!!}
                  </div>
                </div>
              </div>     
              <div class="ln_solid"></div>
              <div class="form-group">
                <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                  <button type="button" onclick="ShowReport()" id="Search" name="login_form_submit" class="btn btn-success">Show Report</button>
                  <label style="color:red" id="warn"></label> 
                </div>
              </div>
            </div>  
          </div>
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="dashboard_graph x_panel">
                <div class="row x_title">
                  <div class="col-md-6">
                    <h3>Best contractor</h3>
                  </div>
                  <div class="col-md-6">
                  </div>
                </div>
                <div class="x_content">
                  <div class="demo-container" style="height:280px" >
                    <div id="canvasActivity" class="demo-placeholder" ></div>               
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row" >
            <div class="col-sm-12 col-md-10 col-md-offset-1 form-horizontal" >
              <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                  <label class="col-xs-3 control-label">Start Date:</label>
                  <div class="col-xs-9">
                    <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                    <input type="text"  placeholder="Date"  class="form-control has-feedback-left ClassDate" name="StartDate1" id="StartDate1" placeholder="enter Start Date " required>     
                  </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                  <label class="col-xs-3 control-label">End Date:</label>
                  <div class="col-xs-9">
                    <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                    <input type="text" placeholder="Date"   class="form-control has-feedback-left ClassDate" name="EndDate1" id="EndDate1" placeholder="enter End Date " required>
                  </div>
                </div>
              </div> 
              <div class="ln_solid"></div>
              <div class="form-group">
                <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                  <button type="button" onclick="ShowReport1()" id="Search" name="login_form_submit" class="btn btn-success">Show Report</button>
                  <label style="color:red" id="warn1"></label> 
                </div>
              </div>
            </div>  
          </div>
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="dashboard_graph x_panel">
                <div class="row x_title">
                  <div class="col-md-6">
                    <h3>Best Retailer</h3>
                  </div>
                  <div class="col-md-6">
                  </div>
                </div>
                <div class="x_content">
                  <div class="demo-container" style="height:280px" >
                    <div id="canvasActivity1" class="demo-placeholder" ></div>               
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>
  </div>
</section>
  @section('other_scripts') 
       <script type="text/javascript">
         $(document).ready(function(){
                $('.ClassDate').daterangepicker({
        singleDatePicker: true,
        singleClasses: "picker_4",
         locale: {
            format: 'YYYY-MM-DD'
        }
      }, function(start) {
        console.log(start.toISOString(), 'Hadeel');
      });
  });
          $('#Government').change(function(){
        $.get("{{ url('api/newdropdown')}}", 
        { option: $(this).val() }, 
        function(data) {
                   var $product = $("#district");
                   $("#district").chosen("destroy");
                $product.empty();
                $('#district').append("<option value='all'>Choose All</option>");
               $.each(data, function(key, element) {
                $('#district').append("<option value='" + key +"'>" + element + "</option>");
            });
                  
        
             $('#district').chosen({
                width: '100%',
                no_results_text:'No Results'
            });
                $("#district").trigger("chosen:updated");
            $("#district").trigger("change");
   });
        
    });
           $("#district").chosen({ 
                   width: '100%',
                   no_results_text: "No Results",
                   allow_single_deselect: true, 
                   search_contains:true, });
 $("#district").trigger("chosen:updated");
    function ShowReport()
{
   if($('#StartDate').val() <=$('#EndDate').val())
  {
   
   if($('#StartDate').val()!="" && $('#EndDate').val()!=""&& $('#Government').val()!="" &&$('#district').val()!="" )
   {
    document.getElementById("warn").innerHTML="";
   $.getJSON("/offlineneew/public/BestContractorwithdate?StartDate="+$('#StartDate').val()+"&EndDate="+$('#EndDate').val()+"&Government="+$('#Government').val()+"&district="+$('#district').val(), function(result) {   
      var chart = new CanvasJS.Chart("canvasActivity",
              {
                  theme: "theme3",
                  exportEnabled: true,
                   animationEnabled: true,
                   zoomEnabled: true,
                   axisX:{
                   labelFontColor: "black",
                  labelMaxWidth: 100,

                   labelAutoFit: true ,
                     labelFontWeight: "bold"


                 },
    
                 toolTip: {
                      shared: true
                  },          
                        dataPointWidth: 20,
          data: [ 
              {
                indexLabelFontSize: 16,
                  indexLabelFontColor: "black",
                type: "column", 
                 indexLabel: "{y} " ,
                name: "Total Activity ",
                legendText: "Total Activity ",
                showInLegend: true, 
            
                              dataPoints:result.totalactivity
                      },
                      {
                type: "column", 
                name: "Total Prize",
                legendText: "Total Prize",
                showInLegend: true, 
                 indexLabel: "{y}",
                   indexLabelFontSize: 16,
                indexLabelFontColor: "black",
                dataPoints:result.totalprize


              }
                  
                  ]
              });
      chart.render();
        });
}
else

document.getElementById("warn").innerHTML="Please enter Dates Or Government";
}
else
document.getElementById("warn").innerHTML="Please enter end date after start date"
           
}
  function ShowReport1()
{
   if($('#StartDate1').val() <=$('#EndDate1').val())
  {
   
   if($('#StartDate1').val()!="" && $('#EndDate1').val()!="")
   {
    document.getElementById("warn").innerHTML="";
   $.getJSON("/offlineneew/public/Bestretailerwithdate?StartDate="+$('#StartDate1').val()+"&EndDate="+$('#EndDate1').val(), function(result) {   
      var chart = new CanvasJS.Chart("canvasActivity1",
              {
                  theme: "theme3",
                  exportEnabled: true,
                   animationEnabled: true,
                   zoomEnabled: true,
                   axisX:{
                   labelFontColor: "black",
                  labelMaxWidth: 100,

                   labelAutoFit: true ,
                     labelFontWeight: "bold"


                 },
    
                 toolTip: {
                      shared: true
                  },          
                        dataPointWidth: 20,
          data: [ 
              {
                indexLabelFontSize: 16,
                  indexLabelFontColor: "black",
                type: "column", 
                 indexLabel: "{y} " ,
                name: "Total Activity ",
                legendText: "Total Activity ",
                showInLegend: true, 
            
                              dataPoints:result.totalactivity
                      },
                      {
                type: "column", 
                name: "Total Prize",
                legendText: "Total Prize",
                showInLegend: true, 
                 indexLabel: "{y}",
                   indexLabelFontSize: 16,
                indexLabelFontColor: "black",
                dataPoints:result.totalprize


              }
                  
                  ]
              });
      chart.render();
        });
}
else

document.getElementById("warn1").innerHTML="Please enter Dates";
}
else
document.getElementById("warn1").innerHTML="Please enter end date after start date"
           
}
  
  </script>
@stop
@stop