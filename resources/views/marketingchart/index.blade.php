@extends('masternew')
@section('content')
<section class="panel-body">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2><i class="fa fa-tasks"></i>Charts</h2> 
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
            <li><a class="close-link"><i class="fa fa-close"></i></a></li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
        <div class="row" >
          <div class="col-sm-12 col-md-10 col-md-offset-1 form-horizontal"  >
            <div class="form-group">
              <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                <label class="col-xs-3 control-label">Start Date:</label>
                <div class="col-xs-9">
                  <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                  <input type="text"  placeholder="Date"  class="form-control has-feedback-left ClassDate" name="StartDate" id="StartDate" placeholder="enter Start Date " required>     
                </div>
              </div>
              <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                <label class="col-xs-3 control-label">End Date:</label>
                <div class="col-xs-9">
                  <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                  <input type="text" placeholder="Date"   class="form-control has-feedback-left ClassDate" name="EndDate" id="EndDate" placeholder="enter End Date " required>
                </div>
              </div>
            </div>
            <div class="ln_solid"></div>
            <div class="form-group">
              <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                <button type="button" onclick="ShowReport()" id="Search" name="login_form_submit" class="btn btn-success">Show Report</button>
                <label style="color:red" id="warn"></label> 
              </div>
            </div>
          </div>  
        </div>
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="dashboard_graph x_panel">
              <div class="row x_title">
                <div class="col-md-6">
                  <h3>Total Contractor and Total Retailer By Activity</h3>
                </div>
                <div class="col-md-6">
                </div>
              </div>
              <div class="x_content">
                <div class="demo-container" style="height:280px" >
                  <div id="chartContainer" class="demo-placeholder" ></div>               
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="row" >
          <div class="col-sm-12 col-md-10 col-md-offset-1 form-horizontal"  >
            <div class="form-group">
              <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                <label class="col-xs-3 control-label">Choose Year:</label>
                <div class="col-xs-9">
                  <select  placeholder="Choose Year"  class="form-control" name="Start" id="Start">
                    <option>2018</option>
                    <option>2017</option>
                    <option>2016</option>
                    <option>2015</option>
                    <option>2014</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="ln_solid"></div>
            <div class="form-group">
              <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                <button type="button" onclick="Showchart()" id="Search" name="login_form_submit" class="btn btn-success">Show Report</button>
                <label style="color:red" id="warn2"></label> 
              </div>
            </div>
          </div>  
        </div> 
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="dashboard_graph x_panel">
              <div class="row x_title">
                <div class="col-md-6">
                  <h3>Total Contractor, Activity and Retailer per Month</h3>
                </div>
                <div class="col-md-6">
                </div>
              </div>
              <div class="x_content">
                <div class="demo-container" style="height:280px" >
                  <div id="chartContainer1" class="demo-placeholder" ></div>               
                </div>
              </div>
            </div>
          </div>
        </div>
     </div>
    </div>
  </div>
</section>
@section('other_scripts') 
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>


  <script type="text/javascript">
         $(document).ready(function(){
           $('.ClassDate').daterangepicker({
        singleDatePicker: true,
        singleClasses: "picker_4",
         locale: {
            format: 'YYYY-MM-DD'
        }
      }, function(start) {
        console.log(start.toISOString(), 'Hadeel');
      });
  });
    function ShowReport()
{
  if($('#StartDate').val() <=$('#EndDate').val())
  {
   
   if($('#StartDate').val()!="" &&$('#EndDate').val()!="" )
   {
    document.getElementById("warn").innerHTML="";
  $.getJSON("/offlineneew/public/getchartdata?StartDate="+$('#StartDate').val()+"&EndDate="+$('#EndDate').val(), function(result) {
   
    
                          
            
      var chart = new CanvasJS.Chart("chartContainer",
              {
                  theme: "theme3",
                  exportEnabled: true,
                   animationEnabled: true,
                   zoomEnabled: true,
                   axisX:{
                   labelFontColor: "black",
                  labelMaxWidth: 100,

                   labelAutoFit: true ,
                     labelFontWeight: "bold"


                 },
    
                 toolTip: {
                      shared: true
                  },          
                        dataPointWidth: 20,
          data: [ 
                 {
                type: "column", 
                name: "Total Contractor",
                legendText: "Total Contractor",
                showInLegend: true, 
                 indexLabel: "{y}",
                   indexLabelFontSize: 16,
                indexLabelFontColor: "black",
                dataPoints:result.totalcontractor


              },
                     {
                type: "column", 
                name: "Total Retailer",
                legendText: "Total Retailer",
                showInLegend: true, 
                 indexLabel: "{y}",
                   indexLabelFontSize: 16,
                indexLabelFontColor: "black",
                dataPoints:result.totalretailer


              }                  
                  ]
              });
      chart.render();
});
}
else

document.getElementById("warn").innerHTML="Please enter Dates";
}
else
document.getElementById("warn").innerHTML="Please enter end date after start date"
}
    function Showchart()
{
  
   
   if($('#Start').val()!="")
   {
    document.getElementById("warn2").innerHTML="";
    $.getJSON("/offlineneew/public/BestContractor?Start="+$('#Start').val(), function(result) {
     
                          
            
      var chart = new CanvasJS.Chart("chartContainer1",
              {
                  theme: "theme3",
                  exportEnabled: true,
                   animationEnabled: true,
                   zoomEnabled: true,
                   axisX:{
                   labelFontColor: "black",
                  labelMaxWidth: 100,

                   labelAutoFit: true ,
                     labelFontWeight: "bold"


                 },
    
                 toolTip: {
                      shared: true
                  },          
                        dataPointWidth: 20,
          data: [ 
              {
                indexLabelFontSize: 16,
                  indexLabelFontColor: "black",
                type: "column", 
                 indexLabel: "{y} " ,
                name: "Total activity",
                legendText: "Total activity",
                showInLegend: true, 
                dataPoints:result.totalactivity
                      },
                      {
                type: "column", 
                name: "Total Contractor",
                legendText: "Total Contractor",
                showInLegend: true, 
                 indexLabel: "{y}",
                   indexLabelFontSize: 16,
                indexLabelFontColor: "black",
                dataPoints:result.totalcontractor


              }
              ,
                {
                type: "column", 
                name: "Total Retailer",
                legendText: "Total Retailer",
                showInLegend: true, 
                 indexLabel: "{y}",
                   indexLabelFontSize: 16,
                indexLabelFontColor: "black",
                dataPoints:result.totalretailer


              }
                  
                  ]
              });
      chart.render();
});
}
else
document.getElementById("warn").innerHTML="Please enter Year";
}

  </script>
@stop
@stop