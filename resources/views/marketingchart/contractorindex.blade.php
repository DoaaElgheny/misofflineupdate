@extends('masternew')
@section('content')
 <section class="panel-body">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2><i class="fa fa-tasks"></i>Charts</h2> 
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
            <li><a class="close-link"><i class="fa fa-close"></i></a></li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="dashboard_graph x_panel">
                <div class="row x_title">
                  <div class="col-md-6">
                    <h3>Total Contractor By Government</h3>
                  </div>
                  <div class="col-md-6">
                  </div>
                </div>
                <div class="x_content">
                  <div class="demo-container" style="height:280px" >
                    <div id="chartContainer" class="demo-placeholder" ></div>               
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="dashboard_graph x_panel">
                <div class="row x_title">
                  <div class="col-md-6">
                    <h3>Total Contractor By Upper Or Lower</h3>
                  </div>
                  <div class="col-md-6">
                  </div>
                </div>
                <div class="x_content">
                  <div class="demo-container" style="height:280px" >
                    <div id="chartContainer1" class="demo-placeholder" ></div>               
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="dashboard_graph x_panel">
                <div class="row x_title">
                  <div class="col-md-6">
                    <h3>Total Contractor By Status</h3>
                  </div>
                  <div class="col-md-6">
                  </div>
                </div>
                <div class="x_content">
                  <div class="demo-container" style="height:280px" >
                    <div id="chartContainer2" class="demo-placeholder" ></div>               
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="dashboard_graph x_panel">
                <div class="row x_title">
                  <div class="col-md-6">
                    <h3>Details About Contractors</h3>
                  </div>
                  <div class="col-md-6">
                  </div>
                </div>
                <div class="x_content">
                  <div class="demo-container" style="height:280px" >
                    <div id="chartContainer3" class="demo-placeholder" ></div>               
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="dashboard_graph x_panel">
                <div class="row x_title">
                  <div class="col-md-6">
                    <h3>Total Contractor Use Products</h3>
                  </div>
                  <div class="col-md-6">
                  </div>
                </div>
                <div class="x_content">
                  <div class="demo-container" style="height:280px" >
                    <div id="chartContainer4" class="demo-placeholder" ></div>               
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>
  </div>
</section>
  @section('other_scripts') 
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
  <script type="text/javascript">


 $.getJSON("http://10.43.144.19/offlineneew/public/govermentcontractor", function(result) {
    
                          
            
      var chart = new CanvasJS.Chart("chartContainer",
              {
                  theme: "theme3",
               
                  exportEnabled: true,
                   animationEnabled: true,
                   zoomEnabled: true,
                   axisX:{
                   labelFontColor: "black",
                  labelMaxWidth: 100,

                   labelAutoFit: true ,
                     labelFontWeight: "bold"


                 },
    
                 toolTip: {
                      shared: true
                  },          
                        dataPointWidth: 20,
          data: [ 
              {
                indexLabelFontSize: 16,
                  indexLabelFontColor: "black",
                type: "column", 
                 indexLabel: "{y} " ,
                name: "Total Contractor number",
                legendText: "Total Contractor number",
                showInLegend: true, 
            
                              dataPoints:result
                      }
                  
                  ]
              });
      chart.render();
});
 $.getJSON("http://10.43.144.19/offlineneew/public/upperlowercontractor", function(result) {
    
                          
            
      var chart = new CanvasJS.Chart("chartContainer1",
              {
title:{
   
    },
                animationEnabled: true,
    legend:{
      verticalAlign: "bottom",
      horizontalAlign: "center"
    },
    data: [
    {        
      indexLabelFontSize: 20,
      indexLabelFontFamily: "Monospace",       
      indexLabelFontColor: "darkgrey", 
      indexLabelLineColor: "darkgrey",        
      indexLabelPlacement: "outside",
      type: "pie",       
      showInLegend: true,
      toolTipContent: "{y} - <strong>#percent%</strong>",
      dataPoints: result    }
    ]
  });
      chart.render();
});
  $.getJSON("http://10.43.144.19/offlineneew/public/productcontractor", function(result) {      
    var chart = new CanvasJS.Chart("chartContainer4",
      {

               title:{
         
          },
                      animationEnabled: true,
          legend:{
            verticalAlign: "bottom",
            horizontalAlign: "center"
          },
          data: [
          {        
            indexLabelFontSize: 20,
            indexLabelFontFamily: "Monospace",       
            indexLabelFontColor: "darkgrey", 
            indexLabelLineColor: "darkgrey",        
            indexLabelPlacement: "outside",
            type: "pie",       
            showInLegend: true,
            toolTipContent: "{y} - <strong>#percent%</strong>",
            dataPoints: result}
          ]
        });
            chart.render();


      });
 $.getJSON("http://10.43.144.19/offlineneew/public/reviewedcontractor", function(result) {
    
                          
            
      var chart = new CanvasJS.Chart("chartContainer2",
              {
                  theme: "theme3",
                
                  exportEnabled: true,
                   animationEnabled: true,
                   zoomEnabled: true,
                   axisX:{
                   labelFontColor: "black",
                  labelMaxWidth: 100,

                   labelAutoFit: true ,
                     labelFontWeight: "bold"


                 },
    
                 toolTip: {
                      shared: true
                  },          
                        dataPointWidth: 20,
          data: [ 
              {
                indexLabelFontSize: 16,
                  indexLabelFontColor: "black",
                type: "column", 
                 indexLabel: "{y} " ,
                name: "Total Contractor number",
                legendText: "Total Contractor number",
                showInLegend: true, 
            
                              dataPoints:result
                      }
                  
                  ]
              });
      chart.render();
});
   $.getJSON("http://10.43.144.19/offlineneew/public/analysisContractor" , function (result) {
       
  

   var chart = new CanvasJS.Chart("chartContainer3",

            {
           zoomEnabled: true,
          zoomType: "xy",
          exportEnabled: true,
          theme: "theme3",
            
          animationEnabled: true,
         dataPointWidth: 20,
             axisY: {
                          title: "No. Of Contractor",
                         
                    },
                      axisX:{
                   labelFontColor: "black",
                  labelMaxWidth: 100,

                   labelAutoFit: true ,
                   gridThickness: 2,
                labelFontWeight: "bold"


                 },
             
              toolTip: {
                content: "Date: {label} </br> <strong>detals:</strong> </br>{y} "
              },      
             
      
      axisY2:{ 
                 title: "Total Amount",
                  titleFontColor : "steelBlue",
                  lineThickness:4
             },
              data: [ 
              {
                type: "column", 
                 indexLabel: "{y}",
                name: "Total Has Facebook",
                legendText: "Total Has Facebook",
                  indexLabelFontSize: 16,
               indexLabelFontColor: "black",
                showInLegend: true, 
                dataPoints:result.Has_Facebook
 

              },
              {
                type: "column", 
                 axisYType: "secondary",
                 indexLabel: "{y}",
                name: "Total Phone Type",
                legendText: "Total Phone Type",
                showInLegend: true, 
                  indexLabelFontSize: 16,
    indexLabelFontColor: "black",
                dataPoints:result.Phone_Type


              },
              {
                type: "column", 
                 indexLabel: "{y}",
                name: "Total Has Email",
                legendText: "Total Has Email",
                showInLegend: true, 
                dataPoints:result.Has_Email


              }
              ,
              {
                type: "column", 
                 indexLabel: "{y}",
                name: "Total Job",
                legendText: "Total Job",
                showInLegend: true, 
                dataPoints:result.Education


              }
              
              ],
                  legend:{
                    cursor:"pointer",
                    itemclick: function(e){
                      if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
                        e.dataSeries.visible = false;
                      }
                      else {
                        e.dataSeries.visible = true;
                      }
                      chart.render();
                    }
                  },
                }); 

        chart.render();

            });
  </script>
@stop
@stop
