@extends('masternew')
@section('content')
 <section class="panel-body">
<style type="text/css">
  #success_message{ display: none;}
</style>
 

 <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                     <h2 style="margin-left:85%">الاقسام<i class="fa fa-tasks"></i></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                     
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    
<a type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal1" style="margin-bottom: 20px; margin-left:85%" > قسم جديد</a>


<!-- model -->
 
<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
     <div class="modal-content">
    
          <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                 <h4 class="modal-title span7 text-center" id="myModalLabel" style="    text-align: center;
    color: #15202a;
    font-size: 20px;
    font-weight: bold;"><span class="title">اضافة قسم جديد</span></h4>
          </div>
    <div class="modal-body">
        <div class="tabbable"> <!-- Only required for left/right tabs -->
        <ul class="nav nav-tabs">
  

        
        </ul>
        <div class="tab-content">
        <div class="tab-pane active" id="tab1">
              <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                 
                  <div class="x_content">
                    <br />
                  {!! Form::open(['action' => "CategoryController@store",'method'=>'post','id'=>'profileForm']) !!}  
             <fieldset style="    padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;" dir="rtl">
  <legend style="width: fit-content;border: 0px;" dir="rtl">بيانات اساسية</legend>
 

 
                    <div class="form-group">
                     <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                          <input type="text" class="form-control has-feedback-left" id="EngName" name="EngName" placeholder="الاسم الانجليزي" required style=" border: 1px solid #15202a;border-radius: 4px; margin-bottom: 9px;">
                                       <span class="fa fa-tasks form-control-feedback right" aria-hidden="true"></span>

                        </div>

                      <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                        <input type="text" class="form-control has-feedback-left"  id="name" name="name" placeholder="اسم القسم" required style=" border: 1px solid #15202a;border-radius: 4px; margin-bottom: 9px;">


                        <span class="fa fa-tasks form-control-feedback right" aria-hidden="true"></span>
                      </div>
                     

                       
                      </div>

                   
                    
                      
</fieldset>
        
           


                       


                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12 " style="text-align: center;">
                          <button type="button" class="btn btn-danger " data-dismiss="modal">الغاء</button>
                         <button class="btn btn-info" type="reset">اعادة ادخال</button>
     <button type="submit"  name="login_form_submit"  class="btn btn-success" value="save">حفظ</button>
                        </div>
                      </div>

      {!!Form::close() !!}                
        </div>
                </div>
                   </div>
                </div>
        </div>
        </div>
        </div>

    </div>
 

         </div>
     </div>
  </div>
<!-- ENDModal -->



                    <table class="table table-hover table-bordered  dt-responsive nowrap display users" cellspacing="0" width="100%" dir="rtl">
  <thead>
    <th>العدد </th>
          <th> الاسم </th>
          <th> الاسم بالانجليزي</th>
          <th>يعمل</th>
          <th>الحذف</th>
        </thead>
        <tfoot>
           <th> </th>
          <th> الاسم </th>
          <th> الاسم بالانجليزي</th>
          <th>يعمل</th>
          <th>الحذف</th>
        </tfoot>



<tbody style="text-align:center;">
<?php $i=1;?>
  @foreach($categories as $category)
          <tr>
            <td>{{$i++}} </td> 
            <td><a  class="testEdit" data-type="text" data-name="Name"  data-pk="{{ $category->id }}">{{$category->Name}}</a></td>
           <td><a  class="testEdit" data-type="text" data-name="EngName"  data-pk="{{ $category->id }}">{{$category->EngName}}</a></td>
  
            
           
              @if($category->active==1)
                <td><input id="check" type="checkbox" class="check" data-name="Active"  data-pk="{{ $category->id }}" onclick="ActiveCheck(this,{{ $category->id }})" value="{{$category->active}}"  checked/></td>
              @else
                <td><input id="check" type="checkbox" class="check" data-name="Active"  data-pk="{{ $category->id }}" onclick="ActiveCheck(this,{{ $category->id }})" value="{{$category->active}}"  /></td>
              @endif  
          
            <td><a href="./categories/destroy/{{$category->id}}"class="btn btn-default btn-sm" ><span class="glyphicon glyphicon-trash"></span>  </a></td>
          </tr>
        @endforeach
</tbody>
</table>


                  </div>
                </div>
              </div>
 
  @section('other_scripts')


<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>



  <script type="text/javascript">

var editor;

  $(document).ready(function(){
     $('#profileForm')
        .formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
           fields: {
                'Name': {
                 
                    validators: {
                            remote: {
                        url: '/MIS/checknamecategory',
                        
                        message: 'هذه الماركة مدخله من قبل',
                        type: 'GET'
                    },

             notEmpty:{
                    message:"يجب ادخال الاسم"
                  },
                    regexp: {
                        regexp: /^[\u0600-\u06FF\s0-9\_]+$/,
                        message: 'يجب ادخال حروف عربي وارقام فقط'
                    }
                  }
                },
                     'EngName': {
                  
                    validators: {
                            remote: {
                        url: '/MIS/checkEnglishnamecategory',
                      
                        message: 'هذه الماركه مدخله من قبل ',
                        type: 'GET'
                    }, 

             // notEmpty:{
             //        message:"يجب ادخال الاسم"
             //      },
                    regexp: {
                        regexp: /^[a-zA-Z\s0-9\_]+$/,
                        message: 'يجب ادخال حروف انجليزيه وارقام فقط '
                    }
                        
                    }
                }
            }  
});

     var table= $('.users').DataTable({
  
    select:true,
    responsive: true,
    "order":[[0,"asc"]],
    'searchable':true,
    "scrollCollapse":true,
    "paging":true,
    "pagingType": "simple",
      dom: 'lBfrtip',
        buttons: [
           
            
            { extend: 'excel', className: 'btn btn-primary dtb' }
            
            
        ],

fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
$.fn.editable.defaults.send = "always";

$.fn.editable.defaults.mode = 'inline';

$('.testEdit').editable({

      validate: function(value) {
        name=$(this).editable().data('name');
      
       
        },

    placement: 'right',
    url:'{{URL::to("/")}}/updatecategory',
  
     ajaxOptions: {
     type: 'get',
    sourceCache: 'false',
     dataType: 'json'

   },

       params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');

            // console.log(params);
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'This Data Already Exist,Enter Correct Data.';
            } else {
                return response.responseText;
            }
        }


});

}
});

 



   $('.users tfoot th').each(function () {



      var title = $('.users thead th').eq($(this).index()).text();
               
 if($(this).index()>=1 && $(this).index()<=14)
        {

           $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
}
        

    });
  table.columns().every( function () {
  var that = this;
 $(this.footer()).find('input').on('keyup change', function () {
          that.search(this.value).draw();
            if (that.search(this.value) ) {
               that.search(this.value).draw();
           }
        });
      
 
   });

 

    $('[data-toggle="popover"]').popover({ trigger: "hover" }); 

 

    $('[data-toggle="popover"]').popover({ trigger: "hover" }); 
 });
  function ActiveCheck(e,id)
{
  var act;
  if(e.value==1)
    act=0;
  else
    act=1;

  $.ajax({
      url: "/updateactive?active="+act+"&id="+id+"'",
      type: "get",
      success: function(data){
      }
    }); 
}
  </script>
@stop
@stop
