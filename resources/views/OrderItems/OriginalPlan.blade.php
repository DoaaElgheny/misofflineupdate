@extends('masternew')
@section('content')
 <section class="panel-body">


 <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
            <h2 style="margin-left:85%">الخطة المتوقعه للصنف<i class="fa fa-tasks"></i></h2>
                        <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                     
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">


                 <table class="table table-hover table-bordered  dt-responsive nowrap tasks" cellspacing="0" width="100%" dir="rtl">
  <thead>
  <th></th>
  <th> اسم الصنف</th>
         <th>الكمية</th>
         
          <th>سبب الصنف </th>
          
</tfoot>
<tbody style="text-align:center;">
<?php $i=1;?>
   @foreach($items_plans as $Value)
          <tr>
            <td>{{$i++}}</td> 
             
            <td>{{$Value->Name}}</td>
            <td>{{$Value->Quantity}}</td> 
            <td>{{$Value->ActivityName}}</td>
           
     
           
            
          </tr>
          
        @endforeach
</tbody>
</table>


                  </div>
                </div>
              </div>
 
  @section('other_scripts')


<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>



 <script type="text/javascript">

var editor;
  $(document).ready(function(){
 

     var table= $('.tasks').DataTable({
  
    select:true,
    responsive: true,
    "order":[[0,"asc"]],
    'searchable':true,
    "scrollCollapse":true,
    "paging":true,
    "pagingType": "simple",
      dom: 'lBfrtip',
        buttons: [
           
            
            { extend: 'excel', className: 'btn btn-primary dtb' }
            
            
        ],

fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
$.fn.editable.defaults.send = "always";

$.fn.editable.defaults.mode = 'inline';

$('.testEdit').editable({

      validate: function(value) {
        name=$(this).editable().data('name');
        if(name=="Name"||name=="quantity")
        {
                if($.trim(value) == '') {
                    return 'Value is required.';
                  }
        }
      
        },

    placement: 'right',
    url:'{{URL::to("/")}}/updatePurchesDetailsIn',
  
     ajaxOptions: {
     type: 'get',
    sourceCache: 'false',
     dataType: 'json'

   },

       params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');
            params.Purches= $('#hidden').val();


            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'This Data Already Exist,Enter Correct Data.';
            } else {
                return response.responseText;
            }
        }
           ,
        success:function(response)
        {
            if(response.status==="You do not have permission.")
             {
              return 'ليس لديك صلاحيه لهذه العملية';
              }
              if(response.status==="total quantity not Valid.")
             {
              return 'هذه الكمية غير متاحه';
              }
        }


});

}
});

 



   $('.tasks tfoot th').each(function () {



      var title = $('.tasks thead th').eq($(this).index()).text();
               
 if($(this).index()>=1 && $(this).index()<=2)
        {

           $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
}
        

    });
  table.columns().every( function () {
  var that = this;
 $(this.footer()).find('input').on('keyup change', function () {
          that.search(this.value).draw();
            if (that.search(this.value) ) {
               that.search(this.value).draw();
           }
        });
      
    });


 });

  </script>
@stop
@stop
