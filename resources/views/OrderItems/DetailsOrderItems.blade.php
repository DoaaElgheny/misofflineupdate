@extends('masternew')
@section('content')
 <section class="panel-body">


 <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
            <h2 style="margin-left:85%">اصناف الطلبية<i class="fa fa-tasks"></i></h2>
                        <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                     
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
<a type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal1" style="margin-bottom: 20px; margin-left:85% " >اضافة صنف جديد</a>


<!-- model -->
 
<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
     <div class="modal-content">
    
          <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                 <h4 class="modal-title span7 text-center" id="myModalLabel"><span class="title">اضافة اصناف جديدة</span></h4>
          </div>
    <div class="modal-body">
        <div class="tabbable"> <!-- Only required for left/right tabs -->
        <ul class="nav nav-tabs">
  

        
        </ul>
        <div class="tab-content">
        <div class="tab-pane active" id="tab1">
              <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                 
                  <div class="x_content">
                    <br />
    


    {!! Form::open(['action' => "OrderItemsController@storeDetailsOrder",'method'=>'post','id'=>'profileForm', 'files' => 'true']) !!}   
             <fieldset style="    padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;" dir="rtl">
  <legend style="width: fit-content;border: 0px;">الاصناف</legend>
                     <div class="form-group col-xs-12">
                     <input type="hidden"  name="id" value="{{$id}}"/>
                            <div class="col-xs-2" dir="rtl">
                            <button type="button" class="btn btn-default addButton"><i class="fa fa-plus"></i></button>
                            </div>
                            <div class="col-xs-2" dir="rtl">
                                <input class="form-control" type="number" min="1"  step ="0.1" name="Cost[]" placeholder=" التكلفة"   />
                            </div>
                            <div class="col-xs-2" dir="rtl">
                                <input class="form-control" type="number" name="quantity[]" placeholder=" الكمية"  required />
                            </div>
                             <div class="col-xs-3" dir="rtl">
                                <input class="form-control" type="text" name="Activity[]" placeholder="Activity"  required />
                            </div>
                            <div class="col-xs-3" dir="rtl">
                                 {!!Form::select('item[]',([null => 'اختر الصنف'] + $item->toArray()) ,null,['class'=>'form-control subuser_list','id' => 'prettify','required'=>'required'])!!}
                            </div>
                            
                        </div>

                        <!-- The template containing an email field and a Remove button -->
                        <div class="form-group hide col-xs-12" id="emailTemplate">
                           <div class="col-xs-2" dir="rtl">
                                <button type="button" class="btn btn-default removeButton"><i class="fa fa-minus"></i></button>
                            </div>
                              <div class="col-xs-2" dir="rtl">
                                <input class="form-control" type="number" min="1"  step ="0.1" name="Cost[]" placeholder=" التكلفة"   />
                            </div>

                            <div class="col-xs-2" dir="rtl">
                                   <input class="form-control" type="number" name="quantity[]" id="quantity[]" placeholder=" الكمية" required="required" />
                            </div>
                           <div class="col-xs-3" dir="rtl">
                                <input class="form-control" type="text" name="Activity[]" placeholder="Activity"  required />
                            </div>
                            
                              <div class="col-xs-3" dir="rtl">
                                  {!!Form::select('item[]',([null => 'اختر الصنف'] + $item->toArray()) ,null,['class'=>'form-control subuser_list','id' => 'prettify','required'=>'required'])!!}
                                  
                            </div>
    
</fieldset>
        
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12 " style="text-align: center">
                          <button type="button" class="btn btn-danger " data-dismiss="modal">الغاء</button>
                         <button class="btn btn-info" type="reset">اعادة ادخال</button>
     <button type="submit"  name="login_form_submit" class="btn btn-success" value="save">حفظ</button>
                        </div>
                      </div>

      {!!Form::close() !!}                
        </div>
                </div>
                   </div>
                </div>
        </div>
        </div>
        </div>

    </div>
 

         </div>
     </div>
  </div>
<!-- ENDModal -->



                    <table class="table table-hover table-bordered  dt-responsive nowrap tasks" cellspacing="0" width="100%" dir="rtl">
  <thead>
  <th></th>
         <th>الاسم </th>
          <th>الكمية </th>
          <th>الشاط </th>
           <th>التكلفة </th>
          <th> </th>
</tfoot>


<tbody style="text-align:center;">
<?php $i=1;?>
   @foreach($ItemPlans as $Value)
          <tr>
            <td>{{$i++}}</td>   
            <td>{{$Value->Name}}</td>
            <td>{{$Value->ActivityName}}</td>
            <td>{{$Value->Quantity}}</td>
            <td>{{$Value->Cost}}</td>
             <td>
       <a href="/offlineneew/public/OrderItemDetails/Destroy/{{$Value->id}}" class="btn btn-default btn-sm" ><span class="glyphicon glyphicon-trash"></span>  </a>
  </td>
           
            
          </tr>
          
        @endforeach
</tbody>
</table>


                  </div>
                </div>
              </div>
 
  @section('other_scripts')


<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>



 <script type="text/javascript">

var editor;
  $(document).ready(function(){
  $('#profileForm')
        .formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
            
                  
                'item[]': {
                  
                    validators: {
                       
                    }
                }
                ,'quantity[]':
                 {
                   
                    validators: {           
                      
                   notEmpty:{
                    message:"يجب ادخال الكمية"
                  }
                    }
                    } ,


                    'Activity[]' :
                    {
                   validators: {           
                      
                   notEmpty:{
                    message:"يجب ادخال سبب الطلبية "
                  }
                    }
                    }

            } ,submitHandler: function(form) {
        form.submit();
    }
        })

        .on('success.form.fv', function(e) {
   // e.preventDefault();
   //Doaa Elgheny


    var $form = $(e.target);

    // Enable the submit button
   
    $form.formValidation('disableSubmitButtons', false);
})   .on('click', '.addButton', function() {
            var $template = $('#emailTemplate'),
                $clone    = $template
                                .clone()
                                .removeClass('hide')
                                .removeAttr('id')
                                .insertBefore($template),
                $email    = $clone.find('[name="item[]"]'),
                $Engname    = $clone.find('[name="quantity[]"]');
                $Activity=$clone.find('[name="Activity[]"]');
                 $Cost=$clone.find('[name="Cost[]"]');

            // Add new field
            $('#profileForm').formValidation('addField', $email).formValidation('addField', $Activity).formValidation('addField', $Engname).formValidation('addField', $Cost);
        })

        // Remove button click handler
        .on('click', '.removeButton', function() {
            var $row   = $(this).closest('.form-group'),
                $email = $row.find('[name="item[]"]'),
                $Engname = $row.find('[name="quantity[]"]');
                $Activity = $row.find('[name="Activity[]"]');
                $Cost = $row.find('[name="Cost[]"]');
            // Remove element containing the email
            $row.remove();

            // Remove field
            $('#profileForm').formValidation('removeField', $email).formValidation('removeField', $Engname).formValidation('removeField', $Engname).formValidation('removeField', $Cost);
        });
   

     var table= $('.tasks').DataTable({
  
    select:true,
    responsive: true,
    "order":[[0,"asc"]],
    'searchable':true,
    "scrollCollapse":true,
    "paging":true,
    "pagingType": "simple",
      dom: 'lBfrtip',
        buttons: [
           
            
            { extend: 'excel', className: 'btn btn-primary dtb' }
            
            
        ],

fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
$.fn.editable.defaults.send = "always";

$.fn.editable.defaults.mode = 'inline';

$('.testEdit').editable({

      validate: function(value) {
        name=$(this).editable().data('name');
        if(name=="Name"||name=="quantity")
        {
                if($.trim(value) == '') {
                    return 'Value is required.';
                  }
        }
      
        },

    placement: 'right',
    url:'{{URL::to("/")}}/updatePurchesDetailsIn',
  
     ajaxOptions: {
     type: 'get',
    sourceCache: 'false',
     dataType: 'json'

   },

       params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');
            params.Purches= $('#hidden').val();


            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'This Data Already Exist,Enter Correct Data.';
            } else {
                return response.responseText;
            }
        }
           ,
        success:function(response)
        {
            if(response.status==="You do not have permission.")
             {
              return 'ليس لديك صلاحيه لهذه العملية';
              }
              if(response.status==="total quantity not Valid.")
             {
              return 'هذه الكمية غير متاحه';
              }
        }


});

}
});

 



   $('.tasks tfoot th').each(function () {



      var title = $('.tasks thead th').eq($(this).index()).text();
               
 if($(this).index()>=1 && $(this).index()<=2)
        {

           $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
}
        

    });
  table.columns().every( function () {
  var that = this;
 $(this.footer()).find('input').on('keyup change', function () {
          that.search(this.value).draw();
            if (that.search(this.value) ) {
               that.search(this.value).draw();
           }
        });
      
    });


 });

  </script>
@stop
@stop
