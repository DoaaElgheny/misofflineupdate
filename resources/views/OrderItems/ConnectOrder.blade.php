@extends('masternew')
@section('content')
 <section class="panel-body">
<style>
  #blahorder{
  background: url('{{$OrderItems->Pos_Image}}') no-repeat;
  background-size: cover;
  height: 250px;
  width: 250px;
}
.wizard-card .picture {
    width: 250px;
    height: 250px;
    background-color: #999999;
    border: 4px solid #CCCCCC;
    color: #FFFFFF;
   border-radius: 0;
  
    overflow: hidden;
    transition: all 0.2s;
    -webkit-transition: all 0.2s;
}
input[type="file"] {
    display: block;
   }
</style>

 <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
            <div class="x_title">
                <h1 ><i class="fa fa-tasks"></i>Po Details</h1>
                    <ul class="nav navbar-right panel_toolbox">
                       <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                       </li>
                       <li><a class="close-link"><i class="fa fa-close"></i></a>
                       </li>
                    </ul>
                    <div class="clearfix">
                    	
                    </div>
            </div>
            <div class="x_content">
       {!! Form::open(['action' => "OrderItemsController@UpdatePos",'method'=>'post','id'=>'profileForm','class'=>'form-horizontal form-label-left 
  ', 'files' => 'true']) !!}
       <input type="hidden" value="{{ csrf_token() }}" id="_token" name="_token" />
       <input type="hidden" value="{{$OrderItems->id}}" id="id" name="id" />
<div class="col-md-12">
	<div class="col-md-8">
		<div class="col-md-3" >Po No</div>
		<div class="col-md-9" >

           <input type="number" class="form-control inventorytexbox" id="PosNo" name="PosNo" value="{{$OrderItems->PosNo}}"  placeholder="Pos No" required="required">
        </div>
      

        <div class="col-md-3 col-ms-12 col-xs-12" >Reson</div>
        <div class="col-md-9 col-ms-12 col-xs-12" style="margin-top: 10px">
           <input type="text" class="form-control inventorytexbox"  id="Res_Pos" name="Res_Pos" placeholder="Reson" value="{{$OrderItems->Res_Pos}}" required="required">
        </div>
         <div class="col-md-3 col-ms-12 col-xs-12">Date</div>
        <div class="col-md-9 col-ms-12 col-xs-12" style="margin-top: 10px">
            <input type="text" class="form-control has-feedback-right col-xs-12 inventorytexbox" id="single_cal4" value="{{$OrderItems->Pos_Date}}" name="Pos_Date" placeholder="Date" required>
        </div>
        <div class="col-md-3 col-ms-12 col-xs-12" >Expense</div>
        <div class="col-md-9 col-ms-12 col-xs-12" style="margin-top: 10px">
            <input type="number" class="form-control inventorytexbox" id="Pos_Expense" name="Pos_Expense"  value="{{$OrderItems->Pos_Expense}}" placeholder="Pos_Expense" required="required">
        </div>

 <div class="col-md-3 col-ms-12 col-xs-12" >Supplier Name</div>
        <div class="col-md-8 col-ms-12 col-xs-12" style="margin-top: 10px">
            {!!Form::select('Supplier_ID',([null => 'Choose suppliers'] + $suppliers->toArray()) ,$OrderItems['Supplier_ID'],['class'=>'form-control subuser_list','id' => 'Supplier_ID','required'=>'required'])!!}


        </div>
        <div class=" btn btn-default btn-sm" style="margin-top: 10px"><a href="/offlineneew/public/Supplier"><span class="glyphicon glyphicon-plus"></span></a></div>



        <div class="col-md-3 col-ms-12 col-xs-12" >Other Notes</div>
        <div class="col-md-9 col-ms-12 col-xs-12" style="margin-top: 10px">
            <textarea style="margin-top: 10px;"  id="Pos_OtherNotes" name="Pos_OtherNotes"  required="required" rows="7" cols="9"></textarea>
        </div>
 

	</div>
	<div class="col-md-4 col-ms-12 col-xs-12">
		<div class="col-sm-12 col-ms-12 col-xs-12 wizard-card">
<div class="picture-container" >
<div class="picture" id="blahorder">
<input id="file-upload"  name="pic" type="file"/>

</div> 
</div>
</div>
	</div>
</div>
      
<div class="col-md-12 col-ms-12 col-xs-12">
	<button class="btn btn-primary" style="margin-left: 80%">Save</button>
</div>
 {!!Form::close() !!}  
            </div>
    </div>
</div>
 
  @section('other_scripts')


<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>



 <script type="text/javascript">

var editor;
  $(document).ready(function(){
 document.getElementById("Pos_OtherNotes").value ="{{$OrderItems->Pos_OtherNotes}}";
         //change imgage    
$("#file-upload").on("change", function()
    {
    
        var files = !!this.files ? this.files : [];

        if (!files.length || !window.FileReader) 
        {
           $("#blahorder").css("background-image",'url(assets/img/noimage.jpg)');
        } // no file selected, or no FileReader support
 
        else if (/^image/.test( files[0].type)){ // only image file
            var reader = new FileReader(); // instance of the FileReader
            reader.readAsDataURL(files[0]); // read the local file
 
            reader.onloadend = function(){ // set image data as background of div
                $("#blahorder").css("background-image", "url("+this.result+")");
            }
        }
    });
/////////////////////




 });

  </script>
@stop
@stop
