@extends('masternew')
@section('content')
<section class="panel-body">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h3><i class="fa fa-tasks"></i> budget Charts</h3> 
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
            <li><a class="close-link"><i class="fa fa-close"></i></a></li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="dashboard_graph x_panel">
              <div class="row x_title">
                <div class="col-md-6">
                  <h3> Total Budget Per Activity</h3>
                </div>
                <div class="col-md-6">
                </div>
              </div>
              <div class="x_content">
                <div class="demo-container" style="height:280px" >
                  <div id="chartactivityCost" class="demo-placeholder" ></div>               
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="row" >
          <div class="col-sm-12 col-md-10 col-md-offset-1 form-horizontal"  >
            <div class="form-group">
              <div class="col-md-12 col-sm-12 col-xs-12 has-feedback">
                <label class=" col-md-2 col-xs-4 control-label">Choose Year:</label>
                <div class="col-md-4 col-xs-9">
                  <select  placeholder="Choose Year"  class="form-control" name="Start" id="Start">
                    <option>2017</option>
                    <option>2018</option>
                    <option>2019</option>
                   
                  </select>
                </div>
              </div>
            </div>
            <div class="ln_solid"></div>
            <div class="form-group">
              <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                <button type="button" onclick="Showchart()" id="Search" name="login_form_submit" class="btn btn-success">Show Report</button>
                <label style="color:red" id="warn2"></label> 
              </div>
            </div>
          </div>  
        </div> 
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="dashboard_graph x_panel">
              <div class="row x_title">
                <div class="col-md-6">
                  <h3> Total Budget Per Month</h3>
                </div>
                <div class="col-md-6">
                </div>
              </div>
              <div class="x_content">
                <div class="demo-container" style="height:280px" >
                  <div id="chartContainer1" class="demo-placeholder" ></div>               
                </div>
              </div>
            </div>
          </div>
        </div>
<div class="row" >
          <div class="col-sm-12 col-md-10 col-md-offset-1 form-horizontal">
            <div class="form-group">
              <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                <label class="col-xs-3 control-label">Items:</label>
                <div class="col-xs-9">
 
  <!--    {!!Form::select('item_id', ([null => 'Please Choose  Items'] +  $Items->toArray()) ,null,['class'=>'form-control chosen-select item_id ','id' => 'item_id','required'=>'required'])!!} -->

     {!!Form::select('item_id[]',($Items->toArray()) ,null,['class'=>'form-control  chosen-select item_id ','id' => 'item_id','multiple' => true,'required'=>'required'])!!}
     
                </div>
              </div>
              <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                <label class="col-xs-3 control-label">year:</label>
                <div class="col-xs-9">
                 <select  placeholder="Choose Year"  class="form-control" name="Start2" id="Start2">
                    <option>2017</option>
                    <option>2018</option>
                    <option>2019</option>
                   
                  </select>

                </div>
              </div>
            </div>
            <div class="ln_solid"></div>
            <div class="form-group">
              <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                <button type="button" onclick="ShowReport()" id="Search" name="login_form_submit" class="btn btn-success">Show Report</button>
                <label style="color:red" id="warn"></label> 
              </div>
            </div>
          </div>  
        </div>
   <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="dashboard_graph x_panel">
              <div class="row x_title">
                <div class="col-md-6">
                  <h3>Budget Per Items</h3>
                </div>
                <div class="col-md-6">
                </div>
              </div>
              <div class="x_content">
                <div class="demo-container" style="height:280px" >
                  <div id="chartContainer" class="demo-placeholder" ></div>               
                </div>
              </div>
            </div>
          </div>
        </div>
     </div>
    </div>
  </div>
</section>
@section('other_scripts') 
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>


  <script type="text/javascript">
         $(document).ready(function(){

          $('.item_id').chosen({
                width: '100%',
                no_results_text:'No Results'
            });
                $(".item_id").trigger("chosen:updated");
            $(".item_id").trigger("change");


           $('.ClassDate').daterangepicker({
        singleDatePicker: true,
        singleClasses: "picker_4",
         locale: {
            format: 'YYYY-MM-DD'
        }
      }, function(start) {
        console.log(start.toISOString(), 'Hadeel');
      });


updateChart();

           //////
  });

var updateChart = function () {

$.getJSON("/offlineneew/public/ActivityCost", function(result) {



var chart = new CanvasJS.Chart("chartactivityCost",
{
theme: "theme3",
exportEnabled: true,
animationEnabled: true,
zoomEnabled: true,
axisX:{
labelFontColor: "black",
labelMaxWidth: 100,

labelAutoFit: true ,
labelFontWeight: "bold"


},
legend:{
fontSize: 15
},

toolTip: {
shared: true
},          
dataPointWidth: 20,
data: [ 
{
indexLabelFontSize: 16,
indexLabelFontColor: "black",
type: "column", 
indexLabel: "{y} L.E " ,
name: "Activity Name",
legendText: "Activity Name",
showInLegend: true, 

dataPoints:result.activityBudget
}

]
});
chart.render();
});



}



    function ShowReport()
{
  if($('#item_id').val() !="")
  {
   
  
    document.getElementById("warn").innerHTML="";

  // $.getJSON("/CostPerItem?Start2="+$('#Start2').val()+"&item_id="+$('#item_id').val(),



  //  function(result) {
   
    
     $.ajax({
      url: "/offlineneew/public/CostPerItem",
        data:{Start2:$('#Start2').val(),item_id:$('#item_id').val()},
      type: "Get",
     success: function(data){
                          
      var chart = new CanvasJS.Chart("chartContainer",
              {
                  theme: "theme3",
                  exportEnabled: true,
                   animationEnabled: true,
                   zoomEnabled: true,
                   axisX:{
                   labelFontColor: "black",
                  labelMaxWidth: 100,

                   labelAutoFit: true ,
                     labelFontWeight: "bold"


                 },
    
                 toolTip: {
                      shared: true
                  },          
                        dataPointWidth: 20,
          data: [ 
                 {
                type: "column", 
                name: "Budget Item",
                legendText: "Budget Item",
                showInLegend: true, 
                 indexLabel: "{y}",
                   indexLabelFontSize: 16,
                indexLabelFontColor: "black",
                dataPoints:data.totalBudgetItems


              }                 
                  ]
              
        });
      chart.render();
}
     });
}
else

document.getElementById("warn").innerHTML="Please select at least one item";


}
    function Showchart()
{
  
   
   if($('#Start').val()!="")
   {
    document.getElementById("warn2").innerHTML="";
    $.getJSON("/offlineneew/public/TotalitemCost?Start="+$('#Start').val(), function(result) {
     
                          
            
      var chart = new CanvasJS.Chart("chartContainer1",
              {
                  theme: "theme3",
                  exportEnabled: true,
                   animationEnabled: true,
                   zoomEnabled: true,
                   axisX:{
                   labelFontColor: "black",
                  labelMaxWidth: 100,

                   labelAutoFit: true ,
                     labelFontWeight: "bold"


                 },
    
                 toolTip: {
                      shared: true
                  },          
                        dataPointWidth: 20,
          data: [ 
              {
                indexLabelFontSize: 16,
                  indexLabelFontColor: "black",
                type: "column", 
                 indexLabel: "{y} " ,
                name: "Total Budget",
                legendText: "Total Budget",
                showInLegend: true, 
                dataPoints:result.totalBudget
                      }
             
                  ]
              });
      chart.render();
});
}
else
document.getElementById("warn").innerHTML="Please enter Year";
}

  </script>
@stop
@stop