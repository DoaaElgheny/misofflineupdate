@extends('masternew')
@section('content')
<!DOCTYPE html>
<meta name="csrf-token" content="{{ csrf_token() }}" />

<section class="panel-body">
 <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                   <h2><i class="fa fa-lightbulb-o"></i> Min Limit Items </h2> 
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                     
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

    <h1><i class='fa fa-tasks'></i>Min Limit Items</h1>






         <fieldset style="    padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;">
  <legend style="width: fit-content;border: 0px;">Enter Date:</legend>
 

 
                    <div class="form-group">



 <div class="col-md-4 selectContainer">
    <div class="input-group">
        <span class="input-group-addon" style="background-color:#3c8dbc; "><i style="color:white; " class="glyphicon glyphicon-list"></i></span>
 {!!Form::select('item_id',([null => 'please chooose Items'] + $Items->toArray()) ,null,['class'=>'form-control subuser_list','id' => 'item_id'])!!}  
  </div>
</div>


    <div class="col-md-4 selectContainer">
    <div class="input-group">
        <span class="input-group-addon" style="background-color:#3c8dbc; "><i style="color:white;" class="glyphicon glyphicon-calendar"></i></span>
  <input type="number" class="form-control" id="MinLimit" name="MinLimit" placeholder="enter Item MinLimit">
  </div>
</div>



                      </div>
  
                    
       <div >
        <label style="color:red" id="warn"></label> 
     </div>
 <div class="row" style="margin-top: 20px;" align="center" >
                                <div  >
            <input type="button" class="btn btn-primary" onclick="ShowReport();" id="Search" value=" Press To Show Report" title="Press To Show Report">
              
            </div>  
            </div> 
</fieldset>
 

                      <button class="btn btn-success" style="margin-top: 20px;id="Export">Export</button>
<table class="table table-hover table-bordered  dt-responsive nowrap display tasks" id="tasks" cellspacing="0" width="100%">
 <thead>

          <th>Item Name</th>  
          <th>Quantity</th>  
          <th>Warehouses Name</th>        
      </thead>
                
      <tbody style='text-align:center;' id="Mytable">
      <?php $i=1;?>
  @foreach($AllItemLimit as $itemLimit)
    <tr>
    

   <td >{{$itemLimit->Name}}</td>
    <td >{{$itemLimit->totalmstock}}</td>
     <td >{{$itemLimit->NameStock}}</td>
   </tr>
   @endforeach
      </tbody>
</table>
  
  </div>
    </div>

  </div>

</section>
  

 @section('other_scripts')
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>

<script type="text/javascript">

function ShowReport()
{
  
 if ($('#MinLimit').val()!="")
 {
 $.ajax({
      url: "/offlineneew/public/MinLimitQuatityItems",
        data:{MinLimit:$('#MinLimit').val(),item_id:$('#item_id').val()},
      type: "Get",
     success: function(data){
      console.log(data);
       $("#Mytable").empty(); 
    var Newstring="";
   for (var ke in data) {
       if (data.hasOwnProperty(ke)) {
        Newstring+="<tr>";
        Newstring+='<td >'+data[ke].item_Name+'</td>';
        Newstring+='<td>'+data[ke].quantity+'</td>';
        Newstring+='<td >'+data[ke].Name+'</td>';
}
}
   $("#Mytable").append(Newstring);      
      }

    }); 
  }
  else
  {
document.getElementById("warn").innerHTML="Please Enter MinLimit";
  }
}


</script>
<script>
  
      $(function() {
        $("#Export").click(function(){
        $("#tasks").table2excel({
          exclude: ".noExl",
          name: "Excel Document Name",
          filename: "Min_Limit_Items",
          fileext: ".xls",
          exclude_img: true,
          exclude_links: true,
          exclude_inputs: true
        });
         });
      });
    </script>

  @stop

@stop
