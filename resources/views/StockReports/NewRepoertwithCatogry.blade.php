@extends('masternew')
@section('content')
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />

<style type="text/css">
	.boxclass{
	background-color: white;
    height: 150px;
    box-shadow: 2px 2px 2px 2px darkgrey;
    margin: 7px 7px 7px 7px;

	}
	.textclass{
		    text-align: center;
    margin-top: 40px;
    color: #344e67;
    font-size: 25px;
    font-weight: 500;
	}
	.background_white {
    background: #ffffff;
}
.box_shadow {
    box-shadow: 1px 0 5px rgba(0, 0, 0, 0.1);
}
.cursor {
    cursor: pointer;
}
.border-radius-5 {
    border-radius: 5px;
}
.padding-10 {
    padding: 10px;
}
.margin-bottom-15 {
    margin-bottom: 15px;
}
.border-radius-5 {
    border-radius: 5px;
}
.sizeicon-font {
    font-size: 45px;
}
.fa {
    display: inline-block;
    font: normal normal normal 14px/1 FontAwesome;
    font-size: inherit;
    text-rendering: auto;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
}
.border-radius-icon {
    border: 1px solid;
    border-radius: 50%;
    padding: 20%;
}
.color_blu {
    color: #8d1b3d;
}
</style>
	 <div id="EditModal"  class="modal fade col-md-12" style="margin-left:2%; position:fixed; background-color: white;">
  <div class="modal-header" style="background: white">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title text-center form-title">your Item</h4>
   </div>
  <div class="col-md-12" style="overflow: auto;background: white;max-height:600px" id="asd">
        </div>
   </div>
<div class="col-md-12" style="height: 100%">
@foreach($allCatogry as $Item)
	<!-- <div class="col-md-2 boxclass"    
		<i class="fas fa-sort-amount-up color_blu border-radius-icon"></i>
		<p class="textclass">{{$Item->Name}}</p>
		
	</div> -->
<div class="co-lg-3 col-md-3 col-sm-4 col-xs-12" onclick="Showitem({{$Item->id}})">
                <div class="background_white box_shadow border-radius-5 padding-10 margin-bottom-15 cursor" >
                    <div class="text-center sizeicon-font" style="margin-top: 40px;">
                        <i class="{{$Item->Image}} color_blu border-radius-icon" aria-hidden="true"></i>
                       <!--  {{($Item->getBrandItem->first())?$Item->getBrandItem->first()->ImageItem:''}} -->
                    </div>
                    <div class="text-center color_blu" style="margin-top: 40px;">
                        <h4>{{$Item->Name}} </h4>
                    </div>
                </div>
            </div>
@endforeach
</div>




 @section('other_scripts')
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
<script>
	function Showitem($id)
	{
		
		
 var $asd = $("#asd");
 $asd.empty();
 $.getJSON("/offlineneew/public/ShowReportIventoryStockNew/" + $id, function(data) {
 console.log(data);
 
 $.each(data, function(index, value) {

//var image=encodeURIComponent(value.ImageItem).replace(/%20/,'+');



  $asd.append('<div class="col-md-3">'+' <img src='+value.Image+' id="My_Image" style="width:200px;height:200px;padding-top: 10px;padding-bottom: 10px;margin:5px 5px 5px 5px">'+'<br><br>'+'<label>'+value.item_Name+'<br>'+'العدد'+value.quantity+'</label><br/>'+'</div>');
                });


});
        
  $('#EditModal').modal('show'); 
	}
</script>
@stop
@stop