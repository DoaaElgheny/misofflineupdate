@extends('masternew')
@section('content')
<!DOCTYPE html>
<meta name="csrf-token" content="{{ csrf_token() }}" />

<section class="panel-body">
 <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                  <h2><i class="fa fa-lightbulb-o"></i> Inventory Stock</h2>
                 
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                     
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

   






         <fieldset style="    padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;">
  <legend style="width: fit-content;border: 0px;">Enter Date:</legend>
 

 
                    <div class="form-group">



 <div class="col-md-4 selectContainer">
    <div class="input-group">
        <span class="input-group-addon" style="background-color:#3c8dbc; "><i style="color:white; "class="glyphicon glyphicon-list"></i></span>
 
  {!!Form::select('Warehouse_iD',([null => 'please chooose Warehouse'] + $Warehouses->toArray()) ,null,['class'=>'form-control subuser_list','id' => 'Warehouse_iD'])!!}
  </div>
</div>






                      </div>
  
               <!--   <input name="agree" id="agree"  type="checkbox" > 
                  <input name="agree" id="agree"  type="hidden" value="0" > -->


       <div >
        <label style="color:red" id="warn"></label> 
     </div>
 <div class="row" style="margin-top: 20px;" align="center" >
                                <div  >
            <input type="button" class="btn btn-primary" onclick="ShowReport();" id="Search" value=" Press To Show Report" title="Press To Show Report">
              
            </div>  
            </div> 
</fieldset>
 

                      <button class="btn btn-success" id="Export" style="margin-top: 20px;">Export</button>
<table class="table table-hover table-bordered  dt-responsive nowrap display tasks" id="tasks" cellspacing="0" width="100%">
<!--  <thead>
         
          <th>Invoice No</th>
          <th>Date</th>
          <th>Warehouses Name</th> 
           <th>Item English Name</th> 
           <th>Item Name</th>   
          <th>quantity</th>        
      </thead> -->
                     
      <tbody style='text-align:center;' id="Mytable">
      </tbody>
</table>
  
  </div>
    </div>

  </div>

</section>
  

 @section('other_scripts')
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>


  <script type="text/javascript">
 $(document).ready(function(){ 

      $(".item_id").chosen({ 
                   width: '100%',
                   no_results_text: "No Results",
                   allow_single_deselect: true, 
                   search_contains:true, });
 $(".item_id").trigger("chosen:updated");


  });


$('#check').on('click', function () {
    $(this).val(this.checked ? 1 : 0);
});




function ShowReport()
{
 
 $.ajax({
      url: "/MIS/ShowTotalIventoryStock",
        data:{Warehouse_iD:$('#Warehouse_iD').val(),item_id:$('#item_id').val(),agree:$('#check').val()},
      type: "Get",
     success: function(data){
      console.log(data);
         $("#Mytable").empty(); 
         Nstring="<tr><th>اسم الصنف</th><th>Quntity In</th><th>Quntity Out</th> <th>Total Quantity</th> </tr>";
   $("#Mytable").append(Nstring);
    var Newstring="";
   for (var ke in data) {
       if (data.hasOwnProperty(ke)) {
         if(data[ke].quantity ===0 )
          {
            if(data[ke].show ==1)
            {
               Newstring+="<tr style='background-color:#FFCCCB;'>";
        // Newstring+='<td >'+data[ke].item_NameArabic+'</td>';
        Newstring+='<td >'+data[ke].item_Name+'</td>';
         Newstring+='<td>'+data[ke].quantityIN+'</td>';
          Newstring+='<td>'+data[ke].quantityOut+'</td>';
          Newstring+='<td>'+data[ke].quantity+'</td>';
            }
       
       

          }
         else if (data[ke].quantity <=10  && data[ke].quantity !=0 )
          {
        Newstring+="<tr style='background-color:#FFFF66'>";
        // Newstring+='<td >'+data[ke].item_NameArabic+'</td>';
        Newstring+='<td >'+data[ke].item_Name+'</td>';
         Newstring+='<td>'+data[ke].quantityIN+'</td>';
          Newstring+='<td>'+data[ke].quantityOut+'</td>';
          Newstring+='<td>'+data[ke].quantity+'</td>';
       

          }
          else {
             Newstring+="<tr >";
        // Newstring+='<td >'+data[ke].item_NameArabic+'</td>';
        Newstring+='<td >'+data[ke].item_Name+'</td>';
         Newstring+='<td>'+data[ke].quantityIN+'</td>';
          Newstring+='<td>'+data[ke].quantityOut+'</td>';
          Newstring+='<td>'+data[ke].quantity+'</td>';
          }
        
      
        

}

}
   $("#Mytable").append(Newstring);      
         

      }

    }); 
  
}
       

</script>
<script>
  
      $(function() {
       
        $("#Export").click(function(){

        $("#Mytable").table2excel({
          exclude: ".noExl",
          name: "Excel Document Name",
          filename: "Inventory stock",
          fileext: ".xls",
          exclude_img: true,
          exclude_links: true,
          exclude_inputs: true
        });
         });
      });
    </script>
  @stop

@stop
