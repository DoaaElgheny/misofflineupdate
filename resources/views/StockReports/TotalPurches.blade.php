 
@extends('master')
@section('content')

<div class="container">
  <div class="row" >
      <h1><i class='fa fa-tasks'></i> Show Stock Out </h1>
      

      <div class="col-sm-12 col-md-10 col-md-offset-1"  >
        <div class="col-sm-12">
        <div  class="col-sm-3">
        <label> 
             From Date
            </label>
        </div>
        <div class="col-sm-3">
           <div class="form-group input-group">
            
             <div class='input-group '> 
             <input type='date' name="Start_Date" id="Start_Date" class="form-control" placeholder="Enter Start Date"/>
                
              </div>
              {!!$errors->first('Start_Date','<small class="label label-danger">:message</small>')!!}
          </div>
        </div>


        <div class="col-sm-3">
        <label> 
              TO Date
            </label>
        </div>
        <div class="col-sm-3"> 
           <div class="form-group input-group">
            
             <div class='input-group ' > 
         <input type='date' name="End_Date"   class="form-control" id="End_Date" placeholder="Enter Start Date"/>
               
                
            </div>
      {!!$errors->first('End_Date','<small class="label label-danger">:message</small>')!!}
        </div>
       </div>

     </div>
       <div class="col-sm-12">

       <div class="col-sm-3">
       <label> 
              warehouse
            </label>
       </div>

       <div class="col-sm-3">
               <!-- warehouse -->
<div   class="form-group input-group">

              {!!Form::select('Warehouse_iD',([null => 'please chooose Warehouse'] + $Warehouses->toArray()) ,null,['class'=>'form-control subuser_list','id' => 'Warehouse_iD'])!!}
        
</div>
       </div>

<Div class="col-sm-3">
  <div class="form-group text-center">
    <button type="button" onclick="ShowReport()" id="Search" class="btn btn-primary">Show Report</button>
   
  </div>
</div>
  <div class="col-sm-3">
        <label style="color:red" id="warn"></label> 
     </div>
       </div>
  
     

</div>  
      
  </div>
</div>
<!---We Want Dsplay Report-->
<button class="btn btn-success" id="Export">Export</button>
<table class="table table-hover table-bordered  dt-responsive nowrap display tasks" id="tasks" cellspacing="0" width="100%">
 <thead>
         
          <th>Invoice No</th>
          <th>Date</th>
          <th>Activity Name</th>
          <th>Warehouses Name</th> 
          <th>Item English Name</th>  
          <th>Item Name</th>  
          <th>Quantity</th> 
                 
      </thead>
                  
      <tbody style='text-align:center;' id="Mytable">
      </tbody>
</table>

<script type="text/javascript">
$(function(){

      $( "#datepicker" ).datepicker(
        );
       

      $("#datepicker2" ).datepicker(
        );

  $(".Users").chosen({ 
                   width: '100%',
                   no_results_text: "لا توجد نتيجه",
                   allow_single_deselect: true, 
                   search_contains:true, });
 $(".Users").trigger("chosen:updated");


        $("#datepicker").on("dp.change", function (e) {
       
            $('#datepicker').data("DateTimePicker").minDate(e.date);


        });
      

   $("#datepicker2").on("dp.change", function (e) {

            $('#datepicker2').data("DateTimePicker").minDate(e.date);

        });

  if ( $('#End_Date')[0].type != 'date' ) $('#End_Date').datepicker();
if ( $('#Start_Date')[0].type != 'date' ) $('#Start_Date').datepicker();
  
});
function ShowReport()
{
  // if($('#Start_Date').val() <=$('#End_Date').val())
  // {
   
  //  if($('#Start_Date').val()!="" &&$('#End_Date').val()!="" )
  //  {
 $.ajax({
      url: "/offlineneew/public/ShowReport",
        data:{End_Date:$('#End_Date').val(),Start_Date:$('#Start_Date').val(),Warehouse_iD:$('#Warehouse_iD').val()},
      type: "Get",
     success: function(data){
       $("#Mytable").empty(); 
   //       Nstring="<tr><td>PurchesNo</td><td>Date</td><td>ActivityName</td><td>datawarehousesName</td><td>Item Name</td><td>quantity</td></tr>";
   // $("#Mytable").append(Nstring);

    var Newstring="";
   for (var ke in data) {
       if (data.hasOwnProperty(ke)) {

        Newstring+="<tr>";
        Newstring+='<td rowspan='+data[ke].rowspan+'>'+data[ke].PurchesNo+'</td>';
        Newstring+='<td rowspan='+data[ke].rowspan+'>'+data[ke].Date+'</td>';
        Newstring+='<td rowspan='+data[ke].rowspan+'>'+data[ke].sub_activitiesName+'</td>';
        Newstring+='<td rowspan='+data[ke].rowspan+'>'+data[ke].datawarehousesName+'</td>';
       
        var n=0;
        if(data[ke].items.length>0)
        {
          for(var k in data[ke].items)
             {

              if(n==0)
              {
                 Newstring+='<td>'+data[ke].items[k].item_Name+'</td>';
                 Newstring+='<td>'+data[ke].items[k].itemName+'</td>';
                  Newstring+='<td>'+data[ke].items[k].quantity+'</td>';
                 Newstring+="</tr>";

              }
               else
               {   Newstring+="<tr>";
                    Newstring+='<td>'+data[ke].items[k].item_Name+'</td>';
                    Newstring+='<td>'+data[ke].items[k].itemName+'</td>';
                    Newstring+='<td>'+data[ke].items[k].quantity+'</td>';
                    Newstring+="</tr>";

               }  
               n++;
              }
        
        }
        
       else
       {
                    Newstring+='<td>ـــــــــــــــــ</td>';
                    Newstring+='<td>ـــــــــــــــــــ</td>';
                    Newstring+="</tr>";

       }

}

}
   $("#Mytable").append(Newstring);      
         

      }

    }); 
  
 }
// else

// document.getElementById("warn").innerHTML="Please enter Dates";
// }
// else
// document.getElementById("warn").innerHTML="Please enter end date after start date";
//  }
 



</script>


<script>
  
      $(function() {
        $("#Export").click(function(){
        $("#tasks").table2excel({
          exclude: ".noExl",
          name: "Excel Document Name",
          filename: "Stock_Out",
          fileext: ".xls",
          exclude_img: true,
          exclude_links: true,
          exclude_inputs: true
        });
         });
      });
    </script>

  @stop










