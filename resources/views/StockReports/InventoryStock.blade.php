@extends('masternew')
@section('content')
<!DOCTYPE html>
<meta name="csrf-token" content="{{ csrf_token() }}" />

<section class="panel-body">
 <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                  <h2><i class="fa fa-lightbulb-o"></i> Inventory Stock</h2>
                 
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                     
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

   






         <fieldset style="    padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;">
  <legend style="width: fit-content;border: 0px;">Enter Date:</legend>
 

 
                    <div class="form-group">



 <div class="col-md-4 selectContainer">
    <div class="input-group">
        <span class="input-group-addon" style="background-color:#3c8dbc; "><i style="color:white; " class="glyphicon glyphicon-list"></i></span>
 
  {!!Form::select('Warehouse_iD',([null => 'please chooose Warehouse'] + $Warehouses->toArray()) ,null,['class'=>'form-control subuser_list','id' => 'Warehouse_iD'])!!}
  </div>
</div>

 <div class="col-sm-12 col-md-12  form-horizontal">
            <div class="col-md-12 form-group">
              <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                <label class="col-xs-3 control-label">Catogry:</label>
                <div class="col-xs-9">
{!!Form::select('Brand',(['0'=> 'all Cateogry'] +$Brand->toArray())   ,null,['class'=>'form-control chosen-select Brand', 'id' => 'Brand','name'=>'Brand','required'=>'required'])!!} 
                </div>
              </div>
    <!--           <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                <label class="col-md-3 col-sm-12 col-xs-12  control-label">Sub Catogry:</label>
                <div class="col-xs-6">
                

{!!Form::select('Brand[]', ( $Brand->toArray()) ,null,['class'=>'form-control chosen-select Brand ','id' => 'Brand','name'=>'Brand','multiple' => true,'required'=>'required'])!!}


                </div>
              </div> -->
            </div>
           
          
          </div> 
    <div class="col-md-4 selectContainer">
 
             
    <div class="input-group">
        <span class="input-group-addon" style="background-color:#3c8dbc; "><i style="color:white; " class="glyphicon glyphicon-list"></i></span>
 
     <!-- {!!Form::select('item_id', ([null => 'Please Choose  Items'] +  $Items->toArray()) ,null,['class'=>'form-control chosen-select item_id ','id' => 'item_id','required'=>'required'])!!} -->

     {!!Form::select('item_id[]',($Items->toArray()) ,null,['class'=>'form-control  chosen-select item_id ','id' => 'item_id','multiple' => true,'required'=>'required'])!!}
  </div>
</div>




                      </div>
  
               <!--   <input name="agree" id="agree"  type="checkbox" > 
                  <input name="agree" id="agree"  type="hidden" value="0" > -->

<input type="checkbox" id= "check" class="check"  />اظهار اصناف تساوي صفر

       <div >
        <label style="color:red" id="warn"></label> 
     </div>
 <div class="row" style="margin-top: 20px;" align="center" >
                                <div  >
            <input type="button" class="btn btn-primary" onclick="ShowReport();" id="Search" value=" Press To Show Report" title="Press To Show Report">
              
            </div>  
            </div> 
</fieldset>
<center>
 <div id="EditModal" class="modal fade" style="width:500px;height:500;margin-left: 40%;    margin-top: 10%">
  <div class="modal-header" style="background: white">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title text-center form-title">Your Image</h4>
          </div>
  <div style="background: white">
             
                        
                        <img src="/offlineneew/public/assets/dist/img/Items/23718224_1467149190072151_623515177_n.jpg" id="My_Image" style="width:400px;height:400px;padding-top: 10px;
    padding-bottom: 10px;">
                      
</div>
</div>
</center>
 <button class="btn btn-success" id="Export" style="margin-top: 20px;">Export</button>
  <button id="Exportwithimage" class="btn btn-success"  style="margin-top: 20px;">Export with image</button>

<table class="table table-hover table-bordered  dt-responsive nowrap display tasks" id="Mytable" cellspacing="0" width="100%">

</table>
  
  </div>
    </div>

  </div>

</section>
  

 @section('other_scripts')
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>


  <script type="text/javascript">
 $(document).ready(function(){ 

  
$('.Brand').chosen({
                width: '100%',
                no_results_text:'No Results'
            });
                $(".Brand").trigger("chosen:updated");
            $(".Brand").trigger("change");

             $('.Catogry').chosen({
                width: '100%',
                no_results_text:'No Results'
            });
                $(".Catogry").trigger("chosen:updated");
            $(".Catogry").trigger("change");


 
            $('.Catogry').chosen({
                width: '100%',
                inherit_select_classes: true
            })
            // Revalidate the color when it is changed
            .change(function(e) {

 $('.item_id').chosen({
                width: '100%',
                no_results_text:'No Results'
            });
                $(".item_id").trigger("chosen:updated");
            $(".item_id").trigger("change");


  $.getJSON("/offlineneew/public/Catogry/Brand/" + $("#Catogry").val(), function(data) {
 
                var $Cat = $(".Brand");
                   $(".Brand").chosen("destroy");
                $Cat.empty();
                $.each(data, function(index, value) {
                    $Cat.append('<option value="' + index +'">' + value + '</option>');
                });
                  
        
             $('.Brand').chosen({
                width: '100%',
                no_results_text:'No Results'
            });
                $(".Brand").trigger("chosen:updated");
            $(".Brand").trigger("change");
            });
            });


 $(".Brand").change(function() {
       
            $.getJSON("/offlineneew/public/Brand/item/" + $(".Brand").val(), function(data) {

                var $asd = $(".item_id");
                   $(".item_id").chosen("destroy");
                $asd.empty();
                $.each(data, function(index, value) {
                    $asd.append('<option value="' + index +'">' + value + '</option>');
                });
                  
        
     
             $('.item_id').chosen({
                width: '100%',
                no_results_text:'No Results'
            });
                $(".item_id").trigger("chosen:updated");
            $(".item_id").trigger("change");
   });
            });

  });


$('#check').on('click', function () {
    $(this).val(this.checked ? 1 : 0);
});
//Doaa Elgheny 2018-01-16
$('#Exportwithimage').on('click', function () {
   $.redirect("/offlineneew/public/Exportwithimage",{
        Warehouse_iD:$('#Warehouse_iD').val(),
        item_id:$('#item_id').val(),
        agree:$('#check').val(),Catogry:$('#Catogry').val(),
        Brand:$('#Brand').val()},
       "get",null,null,true);

   
});
//End My Function



function ShowReport()
{
 
 $.ajax({
      url: "/offlineneew/public/ShowReportIventoryStock",
        data:{Warehouse_iD:$('#Warehouse_iD').val(),item_id:$('#item_id').val(),agree:$('#check').val(),Catogry:$('#Catogry').val(),Brand:$('#Brand').val()},
      type: "Get",
     success: function(data){
      console.log(data);
         $("#Mytable").empty(); 
         Nstring="<tr><th>Sub Catogry </th><th>Item Name </th><th>Notes</th><th>Quntity In</th><th>Quntity Out</th> <th>Total Quantity</th>  <th>Image</th><th>Original Plan</th><th>Actual Plan</th></tr>";
   $("#Mytable").append(Nstring);
    var Newstring="";
   for (var ke in data) {
       if (data.hasOwnProperty(ke)) {
         if(data[ke].quantity ===0 )
          {
            if(data[ke].show ==1)
            {
               Newstring+="<tr style='background-color:#FFCCCB;'>";
        // Newstring+='<td >'+data[ke].item_NameArabic+'</td>';
       
          Newstring+='<td >'+data[ke].bandName+'</td>';
          
        Newstring+='<td >'+data[ke].item_Name+'</td>';
       
        Newstring+='<td >'+data[ke].Notes+'</td>';
        
         Newstring+='<td>'+data[ke].quantityIN+'</td>';
          Newstring+='<td>'+data[ke].quantityOut+'</td>';
          Newstring+='<td>'+data[ke].quantity+'</td>';
         // Newstring+='<td>'+data[ke].asd+'</td>';
           Newstring+='<td  onClick="ShowImageLarge(\'' + data[ke].Image + '\')"><img src="'+data[ke].Image+'" style="height:100px;width:100px;"/></a></td>';

           Newstring+= '<td> <a href="/offlineneew/public/OriginalPlan/'+data[ke].item_id+' " class="btn btn-default btn-sm" ><span class="glyphicon glyphicon-plus"></span>  </a></td>';

         Newstring+= '<td> <a href="/offlineneew/public/ActualPlan/'+data[ke].item_id+' " class="btn btn-default btn-sm" ><span class="glyphicon glyphicon-plus"></span>  </a></td></tr>';
            }
       
       

          }
         else if (data[ke].quantity <=10  && data[ke].quantity !=0 )
          {
        Newstring+="<tr style='background-color:#FFFF66'>";
        // Newstring+='<td >'+data[ke].item_NameArabic+'</td>';
        
          Newstring+='<td >'+data[ke].bandName+'</td>';

        Newstring+='<td >'+data[ke].item_Name+'</td>';
      Newstring+='<td >'+data[ke].Notes+'</td>';

         Newstring+='<td>'+data[ke].quantityIN+'</td>';
          Newstring+='<td>'+data[ke].quantityOut+'</td>';
          Newstring+='<td>'+data[ke].quantity+'</td>';
         // Newstring+='<td>'+data[ke].asd+'</td>';
           Newstring+='<td onClick="ShowImageLarge(\'' + data[ke].Image + '\')"><img src="'+data[ke].Image+'" style="height:100px;width:100px;"/></a></td>';
          
           Newstring+= '<td> <a href="/offlineneew/public/OriginalPlan/'+data[ke].item_id+' " class="btn btn-default btn-sm" ><span class="glyphicon glyphicon-plus"></span>  </a></td>';

         Newstring+= '<td> <a href="/offlineneew/public/ActualPlan/'+data[ke].item_id+' " class="btn btn-default btn-sm" ><span class="glyphicon glyphicon-plus"></span>  </a></td></tr>';

          }
          else {
             Newstring+="<tr >";
        // Newstring+='<td >'+data[ke].item_NameArabic+'</td>';
       
          Newstring+='<td >'+data[ke].bandName+'</td>';
        Newstring+='<td >'+data[ke].item_Name+'</td>';
 Newstring+='<td >'+data[ke].Notes+'</td>';
         Newstring+='<td>'+data[ke].quantityIN+'</td>';
          Newstring+='<td>'+data[ke].quantityOut+'</td>';
          Newstring+='<td>'+data[ke].quantity+'</td>';
         // Newstring+='<td>'+data[ke].asd+'</td>';
          Newstring+='<td onClick="ShowImageLarge(\'' + data[ke].Image + '\')"><img src="'+data[ke].Image+'" style="height:100px;width:100px;"/></a></td>';
           Newstring+= '<td> <a href="/offlineneew/public/OriginalPlan/'+data[ke].item_id+' " class="btn btn-default btn-sm" ><span class="glyphicon glyphicon-plus"></span>  </a></td>';
         Newstring+= '<td> <a href="/offlineneew/public/ActualPlan/'+data[ke].item_id+' " class="btn btn-default btn-sm" ><span class="glyphicon glyphicon-plus"></span>  </a></td></tr>';
       }

}

}
   $("#Mytable").append(Newstring);      
         

      }

    }); 
  
}
       

</script>
<script>
function ShowImageLarge($id)
{
  console.log("Dodo",$id);

 $("#My_Image").attr("src",$id);
 
  $('#EditModal').modal('show');

}
  
      $(function() {
       
        $("#Export").click(function(){

        $("#Mytable").table2excel({
          exclude: ".noExl",
          name: "Excel Document Name",
          filename: "Inventory stock",
          fileext: ".xls",
          exclude_img: true,
          exclude_links: true,
          exclude_inputs: true
        });
         });
      });

    </script>
  @stop

@stop
