@extends('masternew')
@section('content')
<section class="panel-body">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2><i class="fa fa-tasks"></i> Items Stock</h2> 
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
            <li><a class="close-link"><i class="fa fa-close"></i></a></li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
        
  
<div class="row" >
	 <div class="col-sm-12 col-md-12  form-horizontal">
            <div class="col-md-12 form-group">
              <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                <label class="col-xs-3 control-label">Catogry:</label>
                <div class="col-xs-9">
{!!Form::select('Catogry',([null => 'Select  Catogry'] +$catogry->toArray())   ,null,['class'=>'form-control chosen-select Catogry', 'id' => 'Catogry','name'=>'Catogry','required'=>'required'])!!} 
                </div>
              </div>
              <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                <label class="col-md-3 col-sm-12 col-xs-12  control-label">Sub Catogry:</label>
                <div class="col-xs-6">
                

{!!Form::select('Brand[]', ( ['all' => 'All Brand'] +$Brand->toArray()) ,null,['class'=>'form-control chosen-select Brand ','id' => 'Brand','name'=>'Brand','multiple' => true,'required'=>'required'])!!}


                </div>
              </div>
            </div>
           
          
          </div> 
          
          <div class="col-sm-12 col-md-12  form-horizontal">
            <div class="form-group">
              <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                <label class="col-xs-3 control-label">Inventory:</label>

                <div class="col-xs-9">

      <!--   <span class="input-group-addon" style="background-color:#3c8dbc; "><i style="color:white; " class="glyphicon glyphicon-list"></i></span> -->
 
  {!!Form::select('Warehouse_iD',([null => 'please chooose Warehouse'] + $Warehouses->toArray()) ,null,['class'=>'form-control subuser_list','id' => 'Warehouse_iD'])!!}
 


<!-- {!!Form::select('item_id[]', ( ['all' => 'All Items'] ),null,['class'=>'form-control chosen-select item_id ','id' => 'item_id','name'=>'item_id','multiple' => true,'required'=>'required'])!!} -->



     
                </div>
              </div>
            
            </div>
            <div class="ln_solid"></div>
            <div class="form-group">
              <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                <button type="button" onclick="ShowReport()" id="Search" name="login_form_submit" class="btn btn-success">Show Report</button>
                <label style="color:red" id="warn"></label> 
              </div>
            </div>
          </div>  
        </div>
   <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="dashboard_graph x_panel">
              <div class="row x_title">
                <div class="col-md-6">
                  <h3>Stock Items</h3>
                </div>
                <div class="col-md-6">
                </div>
              </div>
              <div class="x_content">
                <div class="demo-container" style="height:280px" >
                  <div id="chartContainer" class="demo-placeholder" ></div>               
                </div>
              </div>
            </div>
          </div>
        </div>
     </div>
    </div>
  </div>
</section>
@section('other_scripts') 
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>


  <script type="text/javascript">
         $(document).ready(function(){


 $('.Brand').chosen({
                width: '100%',
                no_results_text:'No Results'
            });
                $(".Brand").trigger("chosen:updated");
            $(".Brand").trigger("change");

             $('.Catogry').chosen({
                width: '100%',
                no_results_text:'No Results'
            });
                $(".Catogry").trigger("chosen:updated");
            $(".Catogry").trigger("change");


 
            $('.Catogry').chosen({
                width: '100%',
                inherit_select_classes: true
            })
            // Revalidate the color when it is changed
            .change(function(e) {

 $('.item_id').chosen({
                width: '100%',
                no_results_text:'No Results'
            });
                $(".item_id").trigger("chosen:updated");
            $(".item_id").trigger("change");


  $.getJSON("/offlineneew/public/Catogry/Brand/" + $("#Catogry").val(), function(data) {
 
                var $Cat = $(".Brand");
                   $(".Brand").chosen("destroy");
                $Cat.empty();
                $.each(data, function(index, value) {
                    $Cat.append('<option value="' + index +'">' + value + '</option>');
                });
                  
        
             $('.Brand').chosen({
                width: '100%',
                no_results_text:'No Results'
            });
                $(".Brand").trigger("chosen:updated");
            $(".Brand").trigger("change");
            });
            });


 $(".Brand").change(function() {
       
            $.getJSON("/offlineneew/public/Brand/item/" + $(".Brand").val(), function(data) {

                var $asd = $(".item_id");
                   $(".item_id").chosen("destroy");
                $asd.empty();
                $.each(data, function(index, value) {
                    $asd.append('<option value="' + index +'">' + value + '</option>');
                });
                  
        
     
             $('.item_id').chosen({
                width: '100%',
                no_results_text:'No Results'
            });
                $(".item_id").trigger("chosen:updated");
            $(".item_id").trigger("change");
   });
            });


         

           $('.ClassDate').daterangepicker({
        singleDatePicker: true,
        singleClasses: "picker_4",
         locale: {
            format: 'YYYY-MM-DD'
        }
      }, function(start) {
        console.log(start.toISOString(), 'Hadeel');
      });
  });
    function ShowReport()
{
  
  if($('#Brand').val() !="" && $('#Brand').val() !=null)
  {
   
  if($('#Warehouse_iD').val() !="")
  {
    document.getElementById("warn").innerHTML="";
 

  // $.getJSON("/CostPerItem?Start2="+$('#Start2').val()+"&item_id="+$('#item_id').val(),



  //  function(result) {
   
    
     $.ajax({
      url: "/offlineneew/public/ChartItem",
        data:{Brand:$('#Brand').val(),Catogry:$('#Catogry').val(),Warehouse_iD:$('#Warehouse_iD').val()},
      type: "Get",
     success: function(data){
                          
      var chart = new CanvasJS.Chart("chartContainer",
              {
                  theme: "theme3",
                  exportEnabled: true,
                   animationEnabled: true,
                   zoomEnabled: true,
                   axisX:{
                   labelFontColor: "black",
                  labelMaxWidth: 100,

                   labelAutoFit: true ,
                     labelFontWeight: "bold"


                 },
    
                 toolTip: {
                      shared: true
                  },          
                        dataPointWidth: 20,
          data: [ 
                 {
                type: "column", 
                name: " Item",
                legendText: " Item",
                showInLegend: true, 
                 indexLabel: "{y}",
                   indexLabelFontSize: 16,
                indexLabelFontColor: "black",
                dataPoints:data.Items


              }                 
                  ]
              
        });
      chart.render();
}
     });
}
else
{
document.getElementById("warn").innerHTML="Please select at least one Brand";
}

}
else
{
document.getElementById("warn").innerHTML="Please select Warehouses";
}




}
    function Showchart()
{
  
   
   if($('#Start').val()!="")
   {
    document.getElementById("warn2").innerHTML="";
    $.getJSON("/TotalitemCost?Start="+$('#Start').val(), function(result) {
     
                          
            
      var chart = new CanvasJS.Chart("chartContainer1",
              {
                  theme: "theme3",
                  exportEnabled: true,
                   animationEnabled: true,
                   zoomEnabled: true,
                   axisX:{
                   labelFontColor: "black",
                  labelMaxWidth: 100,

                   labelAutoFit: true ,
                     labelFontWeight: "bold"


                 },
    
                 toolTip: {
                      shared: true
                  },          
                        dataPointWidth: 20,
          data: [ 
              {
                indexLabelFontSize: 16,
                  indexLabelFontColor: "black",
                type: "column", 
                 indexLabel: "{y} " ,
                name: "Total Budget",
                legendText: "Total Budget",
                showInLegend: true, 
                dataPoints:result.totalBudget
                      }
             
                  ]
              });
      chart.render();
});
}
else
document.getElementById("warn").innerHTML="Please enter Year";
}

  </script>
@stop
@stop