@extends('masternew')
@section('content')
<!DOCTYPE html>
<meta name="csrf-token" content="{{ csrf_token() }}" />

<section class="panel-body">
 <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
               <h2><i class="fa fa-lightbulb-o"></i> Stock IN Report</h2> 

                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                     
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">







         <fieldset style="    padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;">
  <legend style="width: fit-content;border: 0px;">Enter Date:</legend>
 

 
                    <div class="form-group">

                      <div class="col-md-4 col-sm-4 col-xs-12 has-feedback">
                        <input type='text' name="Start_Date"  id="Start_Date" placeholder=" Start Date" class="form-control has-feedback-left Start_Date"  >


                        <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                         {!!$errors->first('Start_Date','<small class="label label-danger">:message</small>')!!}

                      </div>
                       <div class="col-md-4 col-sm-4 col-xs-12 has-feedback">
                        <input type='text' name="End_Date" id="End_Date" placeholder=" End_Date" class="form-control has-feedback-left End_Date"   >


                        <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                         {!!$errors->first('End_Date','<small class="label label-danger">:message</small>')!!}


                      </div>

    <div class="col-md-4 selectContainer">
    <div class="input-group">
        <span class="input-group-addon" style="background-color:#3c8dbc; "><i style="color:white; "class="glyphicon glyphicon-list"></i></span>
 
     {!!Form::select('Warehouse_iD',([null => 'please chooose Warehouse'] + $Warehouses->toArray()) ,null,['class'=>'form-control selectpicker','id' => 'Warehouse_iD'])!!}
  </div>
</div>



                      </div>
  
                    
       <div >
        <label style="color:red" id="warn"></label> 
     </div>
 <div class="row" style="margin-top: 20px;" align="center" >
                                <div  >
            <input type="button" class="btn btn-primary" onclick="ShowReport();" id="Search" value=" Press To Show Report" title="Press To Show Report">
              
            </div>  
            </div> 
</fieldset>
 


   
                      <button class="btn btn-success" style="margin-top: 20px; "id="Export">Export</button>
<table class="table table-hover table-bordered  dt-responsive nowrap display tasks" id="tasks" cellspacing="0" width="100%">
<!--  <thead>
         
          <th>Invoice No</th>
          <th>Date</th>
          <th>Warehouses Name</th> 
           <th>Item English Name</th> 
           <th>Item Name</th>   
          <th>quantity</th>        
      </thead> -->
                     
      <tbody style='text-align:center;' id="Mytable">
      </tbody>
</table>
  
  </div>
    </div>

  </div>

</section>
  

 @section('other_scripts')
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>


  <script type="text/javascript">

var editor;
$(function(){
  $('#Start_Date').daterangepicker({
        singleDatePicker: true,
        singleClasses: "picker_4",
         locale: {
            format: 'YYYY-MM-DD'
        }
      }, function(start) {
        console.log(start.toISOString(), 'Hadeel');
      });
  $('#End_Date').daterangepicker({
        singleDatePicker: true,
        singleClasses: "picker_4",
         locale: {
            format: 'YYYY-MM-DD'
        }
      }, function(start) {
        console.log(start.toISOString(), 'Hadeel');
      });


 





 });
function ShowReport()
{
  if($('#Start_Date').val() <= $('#End_Date').val())
  {
   
   if($('#Start_Date').val()!="" && $('#End_Date').val()!="" )
   {
document.getElementById("warn").innerHTML="";
 $.ajax({
      url: "/offlineneew/public/ShowReportPurchase",
        data:{End_Date:$('#End_Date').val(),Start_Date:$('#Start_Date').val(),Warehouse_iD:$('#Warehouse_iD').val()},
      type: "Get",
     success: function(data){
       $("#Mytable").empty(); 
   //       Nstring="<tr><td>PurchesNo</td><td>Date</td><td>ActivityName</td><td>datawarehousesName</td><td>Item Name</td><td>quantity</td></tr>";
   // $("#Mytable").append(Nstring);

    var Newstring="";
     Nstring="<tr><th>Invoice No</th><th>Date</th><th>Warehouses Name</th>  <th>Item Name</th><th>quantity</th></tr>"
 $("#Mytable").append(Nstring);
   for (var ke in data) {
       if (data.hasOwnProperty(ke)) {

        Newstring+="<tr>";
        Newstring+='<td rowspan='+data[ke].rowspan+'>'+data[ke].PurchesNo+'</td>';
        Newstring+='<td rowspan='+data[ke].rowspan+'>'+data[ke].Date+'</td>';
        Newstring+='<td rowspan='+data[ke].rowspan+'>'+data[ke].datawarehousesName+'</td>';
       
        var n=0;
        if(data[ke].items.length>0)
        {
          for(var k in data[ke].items)
             {

              if(n==0)
              {
                 // Newstring+='<td>'+data[ke].items[k].item_Name+'</td>';
                 Newstring+='<td>'+data[ke].items[k].itemName+'</td>';
                  Newstring+='<td>'+data[ke].items[k].quantity+'</td>';
                 Newstring+="</tr>";

              }
               else
               {   Newstring+="<tr>";
                    // Newstring+='<td>'+data[ke].items[k].item_Name+'</td>';
                     Newstring+='<td>'+data[ke].items[k].itemName+'</td>';
                    Newstring+='<td>'+data[ke].items[k].quantity+'</td>';
                    Newstring+="</tr>";

               }  
               n++;
              }
        
        }
        
       else
       {
                    Newstring+='<td>ـــــــــــــــــ</td>';
                    Newstring+='<td>ـــــــــــــــــــ</td>';
                    Newstring+="</tr>";

       }

}

}
   $("#Mytable").append(Newstring);    
         

      }

    }); 
  
}
else

document.getElementById("warn").innerHTML="Please enter Dates";
}
else
document.getElementById("warn").innerHTML="Please enter end date after start date";
 }
  </script>
  <script>
  
      $(function() {
        $("#Export").click(function(){
        $("#tasks").table2excel({
          exclude: ".noExl",
          name: "Excel Document Name",
          filename: "Stock_In",
          fileext: ".xls",
          exclude_img: true,
          exclude_links: true,
          exclude_inputs: true
        });
         });
      });
    </script>
@stop
@stop
