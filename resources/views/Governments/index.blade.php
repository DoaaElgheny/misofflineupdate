@extends('masternew')
@section('content')
  <section class="panel-body">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2><i class="fa fa-tasks"></i>Governments</h2> 
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
            <li><a class="close-link"><i class="fa fa-close"></i></a></li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <a type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal1" style="margin-bottom: 20px;"> Add New Government </a>
          <!-- model -->
          <div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                  <h4 class="modal-title span7 text-center" id="myModalLabel"><span class="title">Add New Government</span></h4>
                </div>
                <div class="modal-body">
                  <div class="tabbable"> <!-- Only required for left/right tabs -->
                    <ul class="nav nav-tabs"></ul>
                    <div class="tab-content">
                      <div class="tab-pane active" id="tab1">
                        <div class="row">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                              <div class="x_content">
                                <br />
                                {!! Form::open(['action' => "GovernmentController@store",'method'=>'post','id'=>'profileForm','class'=>'form-horizontal']) !!} 
                                  <fieldset style="    padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;">
                                    <legend style="width: fit-content;border: 0px;">Main Date:</legend>
                                      <div class="form-group">
                                        <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                                          <input type="text" class="form-control has-feedback-left"  id="Name" name="Name" placeholder="Enter Government Name" required>
                                          <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                                          <input type="text" class="form-control has-feedback-left"  id="EngName" name="EngName" placeholder="Enter Government English Name" required>
                                          <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                                        </div>
                                      </div>
                                      <div class="form-group">
                                        <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                                          <label class="col-xs-3 control-label">Type:</label>
                                          <div class="col-xs-9">
                                            {!!Form::select('GType', array('Upper' => 'Upper','Lower' => 'Lower'),[null => 'Select Type Government'],['id' => 'GType','name' => 'GType','class'=>'chosen-select TypeP form-control','required'=>'required']);!!}                  
                                          </div>
                                        </div>
                                      </div>        
                                  </fieldset>
                                  <div class="ln_solid"></div>
                                  <div class="form-group">
                                    <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                                      <button type="button" class="btn btn-primary " data-dismiss="modal">Cancel</button>
                                      <button class="btn btn-primary" type="reset">Reset</button>
                                      <button type="submit"  name="login_form_submit" class="btn btn-success" value="Save" disabled>Submit</button>
                                    </div>
                                  </div>
                                {!!Form::close() !!}                
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        <table class="table table-hover table-bordered  dt-responsive nowrap  tasks" cellspacing="0" width="100%">
          <thead>
            <th>No</th>
            <th>Name</th>
            <th>EName</th>
            <th>Type</th>
            <th>Show All districts</th> 
            <th>Process</th> 
          </thead>
          <tfoot>
            <th></th>
            <th>Name</th>
            <th>EName</th>
            <th>Type</th>
            <th></th> 
            <th></th>  
          </tfoot>
          <tbody style="text-align:center;">
            <?php $i=1;?>
            @foreach($governments as $government)
              <tr>
                <td>{{$i++}}</td>   
                <td><a  class="testEdit" data-type="text" data-name="Name" data-value="{{$government->Name}}" data-pk="{{ $government->id }}">{{$government->Name}}</a></td>       
                <td><a  class="testEdit" data-type="text" data-name="EngName" data-value="{{$government->EngName}}" data-pk="{{ $government->id }}">{{$government->EngName}}</a></td>
                <td><a class="testEdit" data-type="select"  data-value="{{$government->GType}}" data-source ="[{'value':'Upper', 'text': 'Upper'}, {'value':'Lower', 'text': 'Lower'}]" data-name="GType"  data-pk="{{$government->id}}"> {{$government->GType}}</a></td>
                <td><a href="/offlineneew/public/showallDistriGovernment/{{$government->id}}"class="btn btn-default btn-sm" ><span class="glyphicon glyphicon-plus"></span>  </a></td>
                <td><a href="/offlineneew/public/Governments/destroy/{{$government->id}}"class="btn btn-default btn-sm" ><span class="glyphicon glyphicon-trash"></span>  </a></td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</section>
@section('other_scripts')
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>

  <script type="text/javascript">

var editor;

  $(document).ready(function(){
    $('#profileForm')
        .formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
           fields: {
                'Name': {
                    validators: {

                            remote: {
                        url: '/offlineneew/public/checknamegovernment',
                        
                        message: 'The Brand name is Already exist',
                        type: 'GET'
                    },

             notEmpty:{
                    message:"the name is required"
                  },
                  regexp: {
                        regexp: /^[\u0600-\u06FF\s]+$/,
                        message: 'The Name can only consist of alphabetical and spaces'
                    }
                  }
                },
                     'EngName': {
                    validators: {
                            remote: {
                        url: '/offlineneew/public/checkEngNamegovernment',
                      
                        message: 'The Brands name is Already exist',
                        type: 'GET'
                    }, 

             notEmpty:{
                    message:"the name is required"
                  },
                    regexp: {
                        regexp: /^[a-zA-Z\s]+$/,
                        message: 'The full name can only consist of alphabetical and spaces'
                    }
                        
                    }
                }
            }  ,submitHandler: function(form) {
        form.submit();
    }
        })
        .on('success.form.fv', function(e) {
   // e.preventDefault();
    var $form = $(e.target);

    // Enable the submit button
   
    $form.formValidation('disableSubmitButtons', false);
})
        // Add button click handler
        .on('click', '.addButton', function() {
            var $template = $('#emailTemplate'),
                $clone    = $template
                                .clone()
                                .removeClass('hide')
                                .removeAttr('id')
                                .insertBefore($template),
                $email    = $clone.find('[name="email[]"]').appendTo('s');

            // Add new field
            $('#profileForm').formValidation('addField', $email);
        })

        // Remove button click handler
        .on('click', '.removeButton', function() {
            var $row   = $(this).closest('.form-group'),
                $email = $row.find('[name="email[]"]');

            // Remove element containing the email
            $row.remove();

            // Remove field
            $('#profileForm').formValidation('removeField', $email);
        });
//Doaa Elgheny

//End Doaa Elgheny
     var table= $('.tasks').DataTable({
  
    select:true,
    responsive: true,
    "order":[[0,"asc"]],
    'searchable':true,
    "scrollCollapse":true,
    "paging":true,
    "pagingType": "simple",
      dom: 'lBfrtip',
        buttons: [
           
            
            { extend: 'excel', className: 'btn btn-primary dtb' }
            
            
        ],

fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
$.fn.editable.defaults.send = "always";

$.fn.editable.defaults.mode = 'inline';

$('.testEdit').editable({

      validate: function(value) {
        name=$(this).editable().data('name');
        if(name=="Name"||name=="EngName")
        {
                if($.trim(value) == '') {
                    return 'Value is required.';
                  }
        }
          if(name=="Name")
        {

           var regexp = /^[\u0600-\u06FF\s]+$/;          

           if (!regexp.test(value)) 
           {
                return 'This field is not valid';
           }
            
         } 
            if(name=="EngName")
        {

           var regexp = /^[a-zA-Z\s]+$/;          

           if (!regexp.test(value)) 
           {
                return 'This field is not valid';
           }
            
         } 
        },

    placement: 'right',
    url:'{{URL::to("/")}}/updateGovernments',
  
     ajaxOptions: {
     type: 'get',
    sourceCache: 'false',
     dataType: 'json'

   },

       params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');

            // console.log(params);
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'This Data Already Exist,Enter Correct Data.';
            } else {
                return response.responseText;
            }
        }
        ,
        success:function(response)
        {
            if(response.status==="You do not have permission.")
             {
              return 'You do not have permission.';
              }
        }



});

}
});

 



   $('.tasks tfoot th').each(function () {



      var title = $('.tasks thead th').eq($(this).index()).text();
               
 if($(this).index()>=1 && $(this).index()<=6)
        {

           $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
}
        

    });
  table.columns().every( function () {
  var that = this;
 $(this.footer()).find('input').on('keyup change', function () {
          that.search(this.value).draw();
            if (that.search(this.value) ) {
               that.search(this.value).draw();
           }
        });
      
    });


 });
function ActiveCheck(e,id)
{
  var act;
  if(e.value==1)
    act=0;
  else
    act=1;

  $.ajax({
      url: "/updateactive?active="+act+"&id="+id+"'",
      type: "get",
      success: function(data){
      }
    }); 
}
  </script>
@stop
@stop
