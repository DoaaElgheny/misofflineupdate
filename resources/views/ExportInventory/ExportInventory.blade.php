@extends('masternew')
@section('content')
<meta name="csrf-token" content="{{ csrf_token()}}" />
<section class="panel panel-primary">
<div class="panel-body">


 @if(Session::has('message'))

      <p class="alert alert-danger">{{ Session::get('message') }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
@endif


{!!Form::open(['action'=>'ExportInventoryController@exportItems','method' => 'post','files'=>true])!!}
    <input type="submit" name="submit" value="Export Items" class="btn btn-primary" style="margin-bottom: 20px;"/>  
{!!Form::close()!!}

{!!Form::open(['action'=>'ExportInventoryController@exportDataware_Items','method' => 'post','files'=>true])!!}
    <input type="submit" name="submit" value="Export Items DataWarehouse" class="btn btn-primary" style="margin-bottom: 20px;"/>  
{!!Form::close()!!}

{!!Form::open(['action'=>'ExportInventoryController@exportPurches','method' => 'post','files'=>true])!!}
    <input type="submit" name="submit" value="Export Purches" class="btn btn-primary" style="margin-bottom: 20px;"/>  
{!!Form::close()!!}

{!!Form::open(['action'=>'ExportInventoryController@exportitemPurches','method' => 'post','files'=>true])!!}
    <input type="submit" name="submit" value="Export Purches Item" class="btn btn-primary" style="margin-bottom: 20px;"/>  
{!!Form::close()!!}









</div>
</section>
@stop
