@extends('masternew')
@section('content')
<section class="panel-body">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2><i class="fa fa-tasks"></i>Show Winner Contractors</h2> 
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
            <li><a class="close-link"><i class="fa fa-close"></i></a></li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
        <div class="row" >
          <div class="col-sm-12 col-md-10 col-md-offset-1 form-horizontal"  >
            <div class="form-group">
              <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                <label class="col-xs-3 control-label">Start Date:</label>
                <div class="col-xs-9">
                  <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                  <input type="text"  placeholder="Date"  class="form-control has-feedback-left ClassDate" name="startdate" id="startdate" placeholder="enter Start Date " required>     
                </div>
              </div>
              <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                <label class="col-xs-3 control-label">End Date:</label>
                <div class="col-xs-9">
                  <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                  <input type="text" placeholder="Date"   class="form-control has-feedback-left ClassDate" name="enddate" id="enddate" placeholder="enter End Date " required>
                </div>
              </div>
            </div>
            <div class="ln_solid"></div>
            <div class="form-group">
              <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                <button type="button" onclick="Showwinnercontractoer()" id="Search" name="login_form_submit" class="btn btn-success">Show Report</button>
                <label style="color:red" id="warn"></label> 
              </div>
            </div>
          </div>  
        </div>
        <table id="jsontable" class="display table table-bordered" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th>Name</th>
              <th>Government</th>
              <th>District</th>
              <th>Points</th> 
              <th>Add prize</th>
            </tr>
          </thead>
        </table>
<!-- <button class="btn btn-success" id="Export">Export</button>
<table class="table table-hover table-bordered  dt-responsive nowrap display tasks" id="tasks" cellspacing="0" width="100%">
      <thead>
          <th>No</th>
          <th>Name</th>
          <th>Government</th>
          <th>District</th>
          <th>Points</th> 
          <th>Add prize</th>          
      </thead>
               
      <tbody style='text-align:center;' id="mytable">
      </tbody>
    </table> -->
    
</div>
    </div>
  </div>
</section>
@section('other_scripts')     

<script>
 $(document).ready(function(){
         $('.ClassDate').daterangepicker({
        singleDatePicker: true,
        singleClasses: "picker_4",
         locale: {
            format: 'YYYY-MM-DD'
        }
      }, function(start) {
        console.log(start.toISOString(), 'Hadeel');
      });
    });
   
var oTable = $('#jsontable').dataTable({

select:true,
responsive: true,
"order":[[0,"asc"]],
'searchable':true,
"scrollCollapse":true,
"paging":true,
"pagingType": "simple",
dom: 'lBfrtip',
buttons: [


{ extend: 'excel', className: 'btn btn-primary dtb' }


]});   
 function Showwinnercontractoer(e)
 {
  if($('#startdate').val() <= $('#enddate').val()){
   
   if($('#startdate').val()!="" && $('#enddate').val()!="")
   {
      $.ajax({
        url: "/offlineneew/public/getwinner/"+$('#startdate').val()+"/"+$('#enddate').val(),
        type: "get",
        success: function(data){
          if(data!="")
          {
        
            //sort
            for(var i=0;i<data.length;i++)
            {        
               data[i].sort( predicatBy("Amount") );
            }
            oTable.fnClearTable();
            $.each(data, function(index,valuee) { 
             $.each(valuee, function(index,value) { 
              oTable.fnAddData([ value.Name,value.Government,value.District,value.Amount,"<td><a href='/addprize/"+value.id+"/"+value.activityitem_id+"'class='btn btn-default btn-sm' ><span class='glyphicon glyphicon-plus'></span>  </a></td>"]);  
              });
            });
          }
      }
    }); 
}
else
alert("Please enter Dates");
}
else
alert("Please enter end date after start date");
 }

function predicatBy(prop){
   return function(a,b){
      if( a[prop] < b[prop]){
          return 1;
      }else if( a[prop] > b[prop] ){
          return -1;
      }
      return 0;
   }
}
</script>  
@stop
@stop
