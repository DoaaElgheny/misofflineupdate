@extends('masternew')
@section('content')
<section class="panel-body">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2><i class="fa fa-tasks"></i>Add Prizes</h2> 
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
            <li><a class="close-link"><i class="fa fa-close"></i></a></li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          {!! Form::open(['action' => "TestController@store",'method'=>'post','id'=>'profileForm','class'=>'form-horizontal']) !!}  
            <fieldset style="padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;">
              <legend style="width: fit-content;border: 0px;">Main Date:</legend>
              <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                  <label class="col-xs-3 control-label">Warehouse:</label>
                  <div class="col-xs-9">
                    {!!Form::select('warehouse', $warehouse ,null,['class'=>'form-control subuser_list','id' => 'warehouseaction','name'=>'warehouse'])!!} 
                  </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                </div>
              </div> 
              <input id="invisible_id" name="id" type="hidden" value="{{$id}}">
              <input id="invisible_id" name="activityitem_id" type="hidden" value="{{$activityitem_id}}">
              <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                  <label class="col-xs-3 control-label">Prizes:</label>
                  <div class="col-xs-9">
                    {!!Form::select('item[]',([null => 'please chooose'] + $items->toArray()) ,null,['class'=>'form-control subuser_list','id' => 'item_id0'])!!}
                  </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                  <div class="col-xs-10">
                    <input class="form-control has-feedback-left" type="number" name="quantity[]" id="quantity_id" placeholder="Enter Your  Quantity" />
                  </div>
                  <div class="col-xs-2">
                    <button type="button" class="btn btn-default addButton"><i class="fa fa-plus"></i></button>
                  </div>
                </div>
              </div>
              <div class="form-group hide" id="emailTemplate">
                <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                  <label class="col-xs-3 control-label">Prizes:</label>
                  <div class="col-xs-9">
                    {!!Form::select('item[]',([null => 'please chooose'] + $items->toArray()) ,null,['class'=>'form-control subuser_list','id' => 'item_id0'])!!}
                  </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                  <div class="col-xs-10">
                    <input class="form-control has-feedback-left" type="number" name="quantity[]" id="quantity_id" placeholder="Enter Your  Quantity" />
                  </div>
                  <div class="col-xs-2">
                    <button type="button" class="btn btn-default removeButton"><i class="fa fa-minus"></i></button>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="col-xs-9 col-xs-offset-3">
                    <div id="messageContainer"></div>
                    <div id="messageContainer1"></div>
                </div>
              </div> 
            </fieldset>
            <div class="ln_solid"></div>
            <div class="form-group">
              <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                <button type="button" class="btn btn-primary " data-dismiss="modal">Cancel</button>
                <button class="btn btn-primary" type="reset">Reset</button>
                <button type="submit" disabled="disabled" name="login_form_submit" class="btn btn-success" value="Save">Submit</button>
              </div>
            </div>
          {!!Form::close()!!}

      </div>
    </div>
  </div>
</section>
@section('other_scripts')
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>


  <script type="text/javascript">

var editor;

  $(document).ready(function(){
      var $i=0;

     $('#item_id0').on('change',function(e){
      $('#quantity_id').val(0);
     });
    $('#profileForm')
        .formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
           'item[]': {
                   
                    validators: {

                        callback: {
                            callback: function(value, validator, $field) {
                                var $emails          = validator.getFieldElements('item[]'),
                                    numEmails        = $emails.length,
                                    notEmptyCount    = 0,
                                    obj              = {},
                                    duplicateRemoved = [];

                                for (var i = 0; i < numEmails; i++) {
                                    var v = $emails.eq(i).val();
                                    if (v !== '') {
                                        obj[v] = 0;
                                        notEmptyCount++;
                                    }
                                }

                                for (i in obj) {
                                    duplicateRemoved.push(obj[i]);
                                }

                                if (duplicateRemoved.length === 0) {
                                    return {
                                        valid: false,
                                        message: 'يجب ادخال صنف علي الاقل'
                                    };
                                } else if (duplicateRemoved.length !== notEmptyCount) {
                                    return {
                                        valid: false,
                                        message: 'االصنف مدخل من قبل '
                                    };
                                }

                                validator.updateStatus('item[]', validator.STATUS_VALID, 'callback');
                                return true;
                            }
                        }
                    }
                }
                ,'quantity[]':
                 {
                   
                    validators: {           
                      remote: {
                        url: '/checkquantityBillOut',
                      data:function(validator, $field, value)
                        {return{itemid:$('#item_id'+$i).val(),Warehouse:validator.getFieldElements('warehouse').val(),type:validator.getFieldElements('Type').val(),quantity:validator.getFieldElements('quantity[]').val()};},
                        message: 'الكمية اكبر من الموجود في المخزن',
                        type: 'GET'
                    }, 
                   notEmpty:{
                    message:"يجب ادخال الكمية"
                  }
                    }
                    }  
            } ,submitHandler: function(form) {
        form.submit();
    }
        })
        .on('success.form.fv', function(e) {
   // e.preventDefault();
    var $form = $(e.target);

    // Enable the submit button
   
    $form.formValidation('disableSubmitButtons', false);
})
        // Add button click handler
       .on('click', '.addButton', function() {
          $i++;
            var $template = $('#emailTemplate'),
                $clone    = $template
                                .clone()
                                .removeClass('hide')
                                .removeAttr('id')
                                .insertBefore($template),
                $email    = $clone.find('[name="item[]"]').attr("id",'item_id'+$i),
                $Engname    = $clone.find('[name="quantity[]"]').attr("id",'quantity_id'+$i);
           $('#item_id'+$i).on('change',function(e){
      $('#quantity_id'+$i).val(0);
     });
            // Add new field
            $('#profileForm').formValidation('addField', $email).formValidation('addField', $Engname);
        })

        // Remove button click handler
        .on('click', '.removeButton', function() {
            var $row   = $(this).closest('.form-group'),
                $email = $row.find('[name="item[]"]'),
                $Engname = $row.find('[name="quantity[]"]');
            // Remove element containing the email
            $row.remove();

            // Remove field
            $('#profileForm').formValidation('removeField', $email).formValidation('removeField', $Engname);
        });
 });

  </script>
@stop
@stop
