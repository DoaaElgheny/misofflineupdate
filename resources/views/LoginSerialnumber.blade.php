  <link rel="stylesheet" href="/offlineneew/public/assets/css/bootstrap.min.css">

<script type="text/javascript" src="/offlineneew/public/assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/offlineneew/public/assets/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="/offlineneew/public/assets/js/responsive.bootstrap.min.js"></script>
<style type="text/css">
   @import "//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css";
 body {
    padding-top: 15px;
    font-size: 12px
  }
  .main {
    max-width: 320px;
    margin: 0 auto;
  }
  .login-or {
    position: relative;
    font-size: 18px;
    color: #aaa;
    margin-top: 10px;
    margin-bottom: 10px;
    padding-top: 10px;
    padding-bottom: 10px;
  }
  .span-or {
    display: block;
    position: absolute;
    left: 50%;
    top: -2px;
    margin-left: -25px;
    background-color: #fff;
    width: 50px;
    text-align: center;
  }
  .hr-or {
    background-color: #cdcdcd;
    height: 1px;
    margin-top: 0px !important;
    margin-bottom: 0px !important;
  }
  h3 {
    text-align: center;
    line-height: 300%;
  }
  .btn-label {position: relative;left: -12px;display: inline-block;padding: 6px 12px;background: rgba(0,0,0,0.15);border-radius: 3px 0 0 3px;text-align:left !important;}
.btn-labeled {padding-top: 0;padding-bottom: 0;}
.btn { margin-bottom:10px; }
</style>
<div class="container">
  <div class="row">

    <div class="main">

      <h3>Enter Code,Please</h3>
        
   

  {!!Form::open(['action'=>'AdminController@TestSerialNumber','method' => 'post','files'=>true])!!}
        <div class="form-group">
          <label for="inputUsernameEmail">Code</label>
          <input id="serialNum" name="SerialNumber"  type="text" class="form-control" required>
        </div>
      
     
        <button type="submit" class="btn btn btn-primary">
        Submit
        </button>
      {!!Form::close() !!}
    
    </div>
    
  </div>
</div>