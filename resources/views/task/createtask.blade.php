@extends('master')
@section('content')
    {!! csrf_field() !!}
   <div class="table-responsive">  
 
           <div class="container">  
                <br />  
                <br />  
                <h2 align="center">Add Your Task</h2>  

     {!! Form::open(['action' => "TaskController@storetask",'method'=>'post','id'=>'profileForm']) !!}  
             <table class="table table-bordered" id="dynamic_field">  
                  <tr>  
                    <td>
                        <div class="form-group col-xs-12">
                            <div class="col-xs-3">
                                <input type="text" class="form-control" name="name[]" placeholder="Enter Task name"/>
                            </div>
                            <div class="col-xs-3">
                                <input class="form-control" type="text" name="descraption[]" placeholder="Enter Task  descraption" />
                            </div>
                            <div class="col-xs-3">
                                <input class="form-control" type="number" name="target[]" placeholder="Enter Task  target" />
                            </div>
                            <div class="col-xs-2">
                                <button type="button" class="btn btn-default addButton"><i class="fa fa-plus"></i></button>
                            </div>
                        </div>

                        <!-- The template containing an email field and a Remove button -->
                        <div class="form-group hide col-xs-12" id="emailTemplate">
                            <div class="col-xs-3">
                                <input type="text" class="form-control" name="name[]" placeholder="Enter Task name"/>
                            </div>
                            <div class="col-xs-3">
                                <input class="form-control" type="text" name="descraption[]" placeholder="Enter Task  descraption" />
                            </div>
                            <div class="col-xs-3">
                                <input class="form-control" type="number" name="target[]" placeholder="Enter Task  target" />
                            </div>
                            <div class="col-xs-2">
                                <button type="button" class="btn btn-default removeButton"><i class="fa fa-minus"></i></button>
                            </div>
                        </div>

                        <!-- Message container -->
                        <div class="form-group">
                            <div class="col-xs-9 col-xs-offset-3">
                                <div id="messageContainer"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-xs-5 col-xs-offset-3">
                                <button type="submit" name="submitButton" class="btn btn-info" >Submit</button>  
                            </div>
                        </div>
                    </td> 
                  </tr>  
             </table>  
          {!!Form::close()!!}
               </div>  
           </div>  
  
 <script>  
 $(document).ready(function(){  
      var i=1;  
    $('#profileForm')
        .formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                'name[]': {
                    err: '#messageContainer',
                    validators: {
                            remote: {
                        url: '/offlineneew/public/checkname',
                     
                        message: 'The category name is not available',
                        type: 'GET'
                    },
                        callback: {
                            callback: function(value, validator, $field) {
                                var $emails          = validator.getFieldElements('name[]'),
                                    numEmails        = $emails.length,
                                    notEmptyCount    = 0,
                                    obj              = {},
                                    duplicateRemoved = [];

                                for (var i = 0; i < numEmails; i++) {
                                    var v = $emails.eq(i).val();
                                    if (v !== '') {
                                        obj[v] = 0;
                                        notEmptyCount++;
                                    }
                                }

                                for (i in obj) {
                                    duplicateRemoved.push(obj[i]);
                                }

                                if (duplicateRemoved.length === 0) {
                                    return {
                                        valid: false,
                                        message: 'You must fill at least one Name'
                                    };
                                } else if (duplicateRemoved.length !== notEmptyCount) {
                                    return {
                                        valid: false,
                                        message: 'The Name must be unique'
                                    };
                                }

                                validator.updateStatus('name[]', validator.STATUS_VALID, 'callback');
                                return true;
                            }
                        }
                        ,
                  notEmpty:{
                    message:"the name is required"
                  },
                    regexp: {
                        regexp: /^[\u0600-\u06FFA-Za-z\s]+$/,
                        message: 'The full name can only consist of alphabetical and spaces'
                    }
                    }
                }
                ,'descraption[]':
                 {
                    err: '#messageContainer',
                    validators: {
                      notEmpty:{
                    message:"the descraption is required"
                  },
                    regexp: {
                        regexp: /^[\u0600-\u06FFA-Za-z\s]+$/,
                        message: 'The descraption can only consist of alphabetical and spaces'
                    }
          
                    }
                }
                ,'target[]':
                 {
                    err: '#messageContainer',
                    validators: {
                      notEmpty:{
                    message:"the target is required"
                  }
          
                    }
                }
            } ,submitHandler: function(form) {
        form.submit();
    }
        })
        .on('success.form.fv', function(e) {
   // e.preventDefault();
    var $form = $(e.target);

    // Enable the submit button
   
    $form.formValidation('disableSubmitButtons', false);
})
        // Add button click handler
        .on('click', '.addButton', function() {
            var $template = $('#emailTemplate'),
                $clone    = $template
                                .clone()
                                .removeClass('hide')
                                .removeAttr('id')
                                .insertBefore($template),
                $email    = $clone.find('[name="name[]"]'),
                $Engname    = $clone.find('[name="descraption[]"]'),
                $target    = $clone.find('[name="target[]"]');

            // Add new field
            $('#profileForm').formValidation('addField', $email).formValidation('addField', $Engname).formValidation('addField', $target);
        })

        // Remove button click handler
        .on('click', '.removeButton', function() {
            var $row   = $(this).closest('.form-group'),
                $email = $row.find('[name="name[]"]'),
                $Engname = $row.find('[name="descraption[]"]'),
                $target = $row.find('[name="target[]"]');
            // Remove element containing the email
            $row.remove();

            // Remove field
            $('#profileForm').formValidation('removeField', $email).formValidation('removeField', $Engname).formValidation('removeField', $target);
        }); 
   
       // $("#addname").validate({
       //      rules: {
       //               name: {
       //            remote: {
       //              url: "/checkname",
       //              type: "post",
       //              data: {
       //                username: function() {
       //                  return $("input[name=name]").val();
       //                }
       //              }
       //            }
       //          }
       //        }
       //    submitHandler: function(form) {
       //      form.submit();
       //    }
       //  });
 });  
 </script>  
@stop