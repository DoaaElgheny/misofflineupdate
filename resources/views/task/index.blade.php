@extends('master')
@section('content')
<!DOCTYPE html>
<meta name="csrf-token" content="{{ csrf_token() }}" />

<section class="panel panel-primary">
<div class="panel-body">

  <h1><i class='fa fa-tasks'></i> Tasks </h1>

  <a href="/offlineneew/public/createtask" class="btn btn-primary" style="margin-bottom: 20px;"> Add New Task </a>
<br/>
  <a href="/offlineneew/public/task/create" class="btn btn-primary" style="margin-bottom: 20px;"> Assign Task </a>


<table class="table table-hover table-bordered  dt-responsive nowrap display tasks" cellspacing="0" width="100%">
  <thead>
              <th>No</th>
              <th>Name</th>
              <th>Description</th>  
                <th>Task Target</th>

              <th>Process</th>          
  </thead>
    <tfoot>
        
                  <th></th>
                <th>Name</th>
            
                <th>Description</th>
                <th>Task Target</th>

                   <th>Process</th>      
                  </tfoot>
               

<tbody style="text-align:center;">
  <?php $i=1;?>
  @foreach($tasks as $tasks)
    <tr>
    
     <td>{{$i++}}</td>
        
        
         <td><a  class="testEdit" data-type="text" data-name="Task_Name"  data-pk="{{ $tasks->id }}">{{$tasks->Task_Name}}</a></td>
     
         <td ><a  class="testEdit" data-type="text" data-name="Descraption"  data-pk="{{ $tasks->id }}">{{$tasks->Descraption}}</a></td>

         <td ><a  class="testEdit" data-type="number" data-name="Target"  data-pk="{{ $tasks->id }}">{{$tasks->Target}}</a></td>

       <td>
   
       <a href="/offlineneew/public/task/destroy/{{$tasks->id}}"class="btn btn-default btn-sm" ><span class="glyphicon glyphicon-trash"></span>  </a>


  </td>

    
 
   </tr>
     @endforeach
</tbody>
</table>
</div>
    
    </section>
  


<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>


  <script type="text/javascript">

var editor;

  $(document).ready(function(){
     var table= $('.tasks').DataTable({
  
    select:true,
    responsive: true,
    "order":[[0,"asc"]],
    'searchable':true,
    "scrollCollapse":true,
    "paging":true,
    "pagingType": "simple",
      dom: 'lBfrtip',
        buttons: [
           
            
            { extend: 'excel', className: 'btn btn-primary dtb' }
            
            
        ],

fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
$.fn.editable.defaults.send = "always";

$.fn.editable.defaults.mode = 'inline';

$('.testEdit').editable({

      validate: function(value) {
        name=$(this).editable().data('name');
        if(name=="Task_Name"||name=="Descraption"||name=="Target")
        {
                if($.trim(value) == '') {
                    return 'Value is required.';
                  }
                    }
        if(name=="Task_Name"||name=="Descraption")
        {

      var regexp = /^[\u0600-\u06FFA-Za-z\s]+$/;            
      if (!regexp.test(value)) {
            return 'This field is not valid';
        }
        
        }
      
        },

    placement: 'right',
    url:'{{URL::to("/")}}/updatetask',
  
     ajaxOptions: {
     type: 'get',
    sourceCache: 'false',
     dataType: 'json'

   },

       params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');

            // console.log(params);
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'This Data Already Exist,Enter Correct Data.';
            } else {
                return response.responseText;
            }
        }
        ,    success:function(response)
        {
            if(response.status==="You do not have permission.")
             {
              return 'You do not have permission.';
              }
        }


});

}
});

 



   $('.tasks tfoot th').each(function () {



      var title = $('.tasks thead th').eq($(this).index()).text();
               
 if($(this).index()>=1 && $(this).index()<=6)
        {

           $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
}
        

    });
  table.columns().every( function () {
  var that = this;
 $(this.footer()).find('input').on('keyup change', function () {
          that.search(this.value).draw();
            if (that.search(this.value) ) {
               that.search(this.value).draw();
           }
        });
      
    });

 });
  </script>
@stop
