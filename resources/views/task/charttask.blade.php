@extends('master')
@section('content')


<meta name="csrf-token" content="{{ csrf_token() }}" />

  <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
 <script src="/offlineneew/public/assets/js/datepicker-ar.js"></script>
<script src="http://demo.itsolutionstuff.com/demoTheme/js/Chart.bundle.js"></script>



  <div class="col-sm-3">
                     <!-- product -->
            <div   class="form-group input-group">
               <input type="date" placeholder="Date"  class="form-control" name="StartDate" id="StartDate" placeholder="enter End Date " required>
            
                    
            </div>
             </div>

          <div class="col-sm-3">
                     <!-- product -->
            <div   class="form-group input-group">
               <input type="date" placeholder="Date"  class="form-control" name="EndDate" id="EndDate" placeholder="enter End Date " required>
            
                    
            </div>
             </div>


              <div class="col-sm-12">

        <div class="col-sm-3">
             <div class="form-group text-center">
                <button type="button" onclick="ShowReport()" id="Search" class="btn btn-primary">Show Report</button>
            </div>
        </div>
    </div>
         <div class="container"> 
 <div class='row'>
    <div class="col-md-11 col-sm-11" Style="height:440px;">
                        <div class="panel panel-default" >
                         <div class="panel-heading"> Database Review</div>
                         <div class="panel-body">
                         <div id="canvastask" style="height: 300px; width:100%;">
                        </div>
                   </div>
              </div>
           </div> 
           </div>  
       



 <div class='row'>
    <div class="col-md-11 col-sm-11" Style="height:440px;">
                        <div class="panel panel-default" >
                         <div class="panel-heading"> Promoter Backcheck</div>
                         <div class="panel-body">
                         <div id="canvaspromoterbackcheck" style="height: 300px; width:100%;">
                        </div>
                   </div>
              </div>
           </div> 
           </div>  
          
                    <div class='row'>
    <div class="col-md-11 col-sm-11" Style="height:440px;">
                        <div class="panel panel-default" >
                         <div class="panel-heading"> Achieved</div>
                         <div class="panel-body">
                         <div id="achieved" style="height: 300px; width:100%;">
                        </div>
                   </div>
              </div>
           </div> 
           </div>  
        
               <div class="col-md-11 col-sm-11" Style="height:440px;">
                        <div class="panel panel-default" >
                         <div class="panel-heading"> Loe Sales backcheck</div>
                         <div class="panel-body">
                         <div id="loe" style="height: 300px; width:100%;">
                        </div>
                   </div>
              </div>
           </div> 
           </div>  
       
            <div class='row'>
    <div class="col-md-11 col-sm-11" Style="height:440px;">
                        <div class="panel panel-default" >
                         <div class="panel-heading"> videomakers</div>
                         <div class="panel-body">
                         <div id="videomakers" style="height: 300px; width:100%;">
                        </div>
                   </div>
              </div>
           </div> 
           </div>  
        </div>

  <script type="text/javascript">
 



    function ShowReport()
{



  
     $.ajax({
      url: "/offlineneew/public/officetask",
        data:{StartDate:$('#StartDate').val(),EndDate:$('#EndDate').val()},
       type: "Get",
       success: function(result){
    
      var chart = new CanvasJS.Chart("canvastask",
        {
                  theme: "theme3",
                  exportEnabled: true,
                   animationEnabled: true,
                   zoomEnabled: true,
                   axisX:{
                   labelFontColor: "black",
                  labelMaxWidth: 100,

                   labelAutoFit: true ,
   labelFontWeight: "bold"


                 },
                   legend:{
                             fontSize: 15
                          },
    
                 toolTip: {
                      shared: true
                  }, 
                   axisY:{
  

      valueFormatString: "#",

 
       interval: 20,
    
    stripLines:[
            {
                        showOnTop: true,

              value:result.tasktarget[0].Target,              
                color:"red",
                label : "Target Limit",
                labelFontColor: "#a8a8a8"
            }
            ]
 },         
                        dataPointWidth: 20,
          data: [ 
              {
                indexLabelFontSize: 16,
    indexLabelFontColor: "black",
                type: "column", 
                 indexLabel: "{y}  " ,
                name: "Total Task",
                legendText: "Total Task",
                showInLegend: true, 
            
                              dataPoints:result.task
                      }
                  
                  ]
              });
      chart.render();

} //End Sucess

    });

 $.ajax({
      url: "/offlineneew/public/promoterbackcheck",
        data:{StartDate:$('#StartDate').val(),EndDate:$('#EndDate').val()},
       type: "Get",
       success: function(result){

    console.log(result);
         
      var chart = new CanvasJS.Chart("canvaspromoterbackcheck",
                    {
                  theme: "theme3",
                  exportEnabled: true,
                   animationEnabled: true,
                   zoomEnabled: true,
                   axisX:{
                   labelFontColor: "black",
                  labelMaxWidth: 100,

                   labelAutoFit: true ,
   labelFontWeight: "bold"


                 },
                   legend:{
                             fontSize: 15
                          },
    
                 toolTip: {
                      shared: true
                  }, 
                   axisY:{
  

      valueFormatString: "#",
  interval: 20,

    stripLines:[
            {
                        showOnTop: true,

              value:result.promotersbackchecktasktarget[0].Target,              
                color:"red",
                label : "Target Limit",
                labelFontColor: "#a8a8a8"
            }
            ]
 },         
                        dataPointWidth: 20,
          data: [ 
              {
                indexLabelFontSize: 16,
    indexLabelFontColor: "black",
                type: "column", 
                 indexLabel: "{y}  " ,
                name: "Total Task",
                legendText: "Total Task",
                showInLegend: true, 
            
                              dataPoints:result.promotersbackcheck
                      }
                  
                  ]
              });
      chart.render();
    }

    });
 $.ajax({
      url: "/offlineneew/public/videomakers",
        data:{StartDate:$('#StartDate').val(),EndDate:$('#EndDate').val()},
       type: "Get",
       success: function(result){
         

    
                       // console.log(result.tasktarget[0].Task_Target) ;
            
      var chart = new CanvasJS.Chart("videomakers",
              {
                  theme: "theme3",
                  exportEnabled: true,
                   animationEnabled: true,
                   zoomEnabled: true,
                   axisX:{
                   labelFontColor: "black",
                  labelMaxWidth: 100,

                   labelAutoFit: true ,
   labelFontWeight: "bold"


                 },
                   legend:{
                             fontSize: 15
                          },
    
                 toolTip: {
                      shared: true
                  }, 
                   axisY:{
  
  interval: 5,
      valueFormatString: "#",


    stripLines:[
            {
                        showOnTop: true,

              value:result.videomakerstarget[0].Target,              
                color:"red",
                label : "Target Limit",
                labelFontColor: "#a8a8a8"
            }
            ]
 },         
                        dataPointWidth: 20,
          data: [ 
              {
                indexLabelFontSize: 16,
    indexLabelFontColor: "black",
                type: "column", 
                 indexLabel: "{y}  " ,
                name: "Total Task",
                legendText: "Total Task",
                showInLegend: true, 
            
                              dataPoints:result.videomakers
                      }
                  
                  ]
              });
      chart.render();
         }
          });
  $.ajax({
      url: "/offlineneew/public/loe",
        data:{StartDate:$('#StartDate').val(),EndDate:$('#EndDate').val()},
       type: "Get",
       success: function(result){

    
                       // console.log(result.tasktarget[0].Task_Target) ;
            
      var chart = new CanvasJS.Chart("loe",
              {
                  theme: "theme3",
                  exportEnabled: true,
                   animationEnabled: true,
                   zoomEnabled: true,
                   axisX:{
                   labelFontColor: "black",
                  labelMaxWidth: 100,

                   labelAutoFit: true ,
   labelFontWeight: "bold"


                 },
                   legend:{
                             fontSize: 15
                          },
    
                 toolTip: {
                      shared: true
                  }, 
                   axisY:{
  
  interval: 20,
      valueFormatString: "#",


    stripLines:[
            {
                        showOnTop: true,

              value:result.loetarget[0].Target,              
                color:"red",
                label : "Target Limit",
                labelFontColor: "#a8a8a8"
            }
            ]
 },         
                        dataPointWidth: 20,
          data: [ 
              {
                indexLabelFontSize: 16,
    indexLabelFontColor: "black",
                type: "column", 
                 indexLabel: "{y}  " ,
                name: "Total Task",
                legendText: "Total Task",
                showInLegend: true, 
            
                              dataPoints:result.loe
                      }
                  
                  ]
              });
      chart.render();
    }
          });
   $.ajax({
      url: "/offlineneew/public/achieved",
        data:{StartDate:$('#StartDate').val(),EndDate:$('#EndDate').val()},
       type: "Get",
       success: function(result){
 
    
                       // console.log(result.tasktarget[0].Task_Target) ;
            
      var chart = new CanvasJS.Chart("achieved",
              {
                  theme: "theme3",
                  exportEnabled: true,
                   animationEnabled: true,
                   zoomEnabled: true,
                   axisX:{
                   labelFontColor: "black",
                  labelMaxWidth: 100,

                   labelAutoFit: true ,
               labelFontWeight: "bold"


                 },
                   legend:{
                             fontSize: 15
                          },
    
                 toolTip: {
                      shared: true
                  }, 
                   axisY:{
  
  interval: 20,
      valueFormatString: "#"
 },         
                        dataPointWidth: 20,
          data: [ 
              {
                indexLabelFontSize: 16,
    indexLabelFontColor: "black",
                type: "column", 
                 indexLabel: "{y}%  " ,
                name: "Promoters Backcheck",
                legendText: "Promoters Backcheck",
                showInLegend: true, 
            
                              dataPoints:result.tasksBcheck
                      },
                            {
                indexLabelFontSize: 16,
    indexLabelFontColor: "black",
                type: "column", 
                 indexLabel: "{y}% " ,
                name: "Database Review",
                legendText: "Database Review",
                showInLegend: true, 
            
                  dataPoints:result.tasksDR
                      },
                            {
                indexLabelFontSize: 16,
    indexLabelFontColor: "black",
                type: "column", 
                 indexLabel: "{y}%  " ,
                name: "Contractors Program Points",
                legendText: "Contractors Program Points",
                showInLegend: true, 
            
                              dataPoints:result.tasksPonits
                      }
                  
                  ],
                        legend:{
                    cursor:"pointer",
                    itemclick: function(e){
                      if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
                        e.dataSeries.visible = false;
                      }
                      else {
                        e.dataSeries.visible = true;
                      }
                      chart.render();
                    }
                  },            });
      chart.render();
         } }); 

    
       

}
            
</script>

</section>
@stop