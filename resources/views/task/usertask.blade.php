@extends('master')
@section('content')
<!DOCTYPE html>
<meta name="csrf-token" content="{{ csrf_token() }}" />

<section class="panel panel-primary">
<div class="panel-body">

  <h1><i class='fa fa-tasks'></i> Tasks </h1>


<table class="table table-hover table-bordered  dt-responsive nowrap display tasks" cellspacing="0" width="100%">
  <thead>
              <th>No</th>
                <th>Name</th>
                     <th>Task</th>
                     <th>Description</th>
                            <th>Task Date</th>
                  <th>Task Time</th>
                         
                            <th>Target</th>
                                                        <th>Achieved</th>

                                  
         
               
          
           
              
             
             
               
  </thead>
    <tfoot>
        
                  <th></th>
                <th>Name</th>
                     <th>Task</th>
                     <th>Description</th>
                            <th>Task Date</th>
                  <th>Task Time</th>
                           
                                                       <th>Achieved</th>

                            <th>Target</th>
                                                       

                        
            
                  </tfoot>
               

<tbody style="text-align:center;">
  <?php $i=1;?>
  @foreach($tasks as $tasks)
    <tr>
    
     <td>{{$i++}}</td>
         <td>{{$tasks->username}}</td>
         <td >{{$tasks->Task_Name}}</td>
         <td >{{$tasks->Descraption}}</td>
              <td >{{$tasks->Date}}</td>
               <td >{{$tasks->Time}}</td>
         <td >{{$tasks->Target}}</td>
         <td ><a class="testEdit" data-type="text"  data-value="{{$tasks->Achieved}}" data-name="Achieved"  data-pk="{{ $tasks->utid }}">{{$tasks->Achieved}}</td>




    
  
   </tr>
     @endforeach
</tbody>
</table>
</div>
    
    </section>
  


<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>


  <script type="text/javascript">

var editor;

  $(document).ready(function(){
     var table= $('.tasks').DataTable({
  
    select:true,
    responsive: true,
    "order":[[0,"asc"]],
    'searchable':true,
    "scrollCollapse":true,
    "paging":true,
    "pagingType": "simple",
      dom: 'lBfrtip',
        buttons: [
           
            
            { extend: 'excel', className: 'btn btn-primary dtb' }
            
            
        ],

fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
$.fn.editable.defaults.send = "always";

$.fn.editable.defaults.mode = 'inline';

$('.testEdit').editable({

      validate: function(value) {
        name=$(this).editable().data('name');
        if(name=="Achieved")
        {
                if($.trim(value) == '') {
                    return 'Value is required.';
                  }
                    }
     
        },

    placement: 'right',
    url:'{{URL::to("/")}}/updateusertask',
  
     ajaxOptions: {
     type: 'get',
    sourceCache: 'false',
     dataType: 'json'

   },

       params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');

            // console.log(params);
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'This Data Already Exist,Enter Correct Data.';
            } else {
                return response.responseText;
            }
        }


});

}
});

 



   $('.tasks tfoot th').each(function () {



      var title = $('.tasks thead th').eq($(this).index()).text();
               
 if($(this).index()>=1 && $(this).index()<=6)
        {

           $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
}
        

    });
  table.columns().every( function () {
  var that = this;
 $(this.footer()).find('input').on('keyup change', function () {
          that.search(this.value).draw();
            if (that.search(this.value) ) {
               that.search(this.value).draw();
           }
        });
      
    });

 });
  </script>
@stop

