@extends('masternew')
@section('content')
<!DOCTYPE html>
<section class="panel-body">
 <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><i class="fa fa-tasks" ></i> Front Office </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                     
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

 


   <a type="button"  class="btn btn-primary" data-toggle="modal" data-target="#myModal1" style="margin-bottom: 20px;"> Add New Front Office </a>
  
     <!-- model -->
 
<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
     <div class="modal-content">
    
          <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                 <h4 class="modal-title span7 text-center" id="myModalLabel"><span class="title">Add New Front Office</span></h4>
          </div>
    <div class="modal-body">
        <div class="tabbable"> <!-- Only required for left/right tabs -->
        <ul class="nav nav-tabs">
  

        
        </ul>
        <div class="tab-content">
        <div class="tab-pane active" id="tab1">
              <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                 
                  <div class="x_content">
                    <br />
{!! Form::open(['action' => "SellerOfficeController@store",'method'=>'post','id'=>'profileForm', 'class'=>'form-horizontal form-label-left  ']) !!} 



<fieldset style="    padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;">
  <legend style="width: fit-content;border: 0px;">Main Data:</legend>
    <div class="form-group">


      <div class="control-label col-md-6 col-sm-6 col-xs-12">

                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Company
                        </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                             {!!Form::select('company_id',([null => 'please chooose Company'] + $companies->toArray()) ,null,['class'=>'form-control','id' => 'company_id','required'=>'required'])!!}
                        </div>
                        </div>
                      <div class="control-label col-md-6 col-sm-6 col-xs-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"> Products</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                                    
<select name="attribute[]" class="form-control attribute chosen-select " id="Productsid" multiple required>

 
</select>
                        </div>
                     </div>

   
                     

                      </div>
                      
<div class="form-group">
        <div class="control-label col-md-6 col-sm-6 col-xs-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12"> Telephone</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                                    
 <input type="number" class="form-control" minlength="6" maxlength="11" id="Telephone" name="Telephone" placeholder="enter Telephone" required>
                        </div>
                     </div>
</div>
 

 

                 
</fieldset>
        


             <fieldset style="    padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;">
  <legend style="width: fit-content;border: 0px;">Location:</legend>
    <div class="form-group">
                      <div class="control-label col-md-6 col-sm-6 col-xs-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Government</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                           {!!Form::select('Government',([null => 'please chooose'] + $governments->toArray()) ,null,['class'=>'form-control subuser_list','id' => 'Government','required'=>'required'])!!}
                        </div>
                     </div>
                       <div class="control-label col-md-6 col-sm-6 col-xs-12">

                        <label class="control-label col-md-3 col-sm-3 col-xs-12">District
                        </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            {!!Form::select('district',([null => 'please chooose'] ) ,null,['class'=>' form-control chosen-select District  subuser_list','id' => 'district','required'=>'required'])!!}
                        </div>
                        </div>

                      </div>
                       <div class="form-group">
                      <div class="control-label col-md-6 col-sm-6 col-xs-12">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Location</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <input type="text" class="form-control" id="Name" name="Location" placeholder="enter Location" required>
                        </div>
                     </div>
                       <div class="control-label col-md-6 col-sm-6 col-xs-12">

                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Address
                        </label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                        <input type="text" class="form-control" id="Name" name="Address" placeholder="enter  Address" required>
                        </div>
                        </div>

                      </div>



 

                 
</fieldset>
        

                    

                        
                       


                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                          <button type="button" class="btn btn-primary " data-dismiss="modal">Cancel</button>
                         <button class="btn btn-primary" type="reset">Reset</button>
                          <button type="submit"  name="login_form_submit" class="btn btn-success" value="Save">Submit</button>
                        </div>
                      </div>

                   
        {!!Form::close() !!}                
        </div>
                </div>
                   </div>
                </div>
        </div>
        </div>
        </div>

    </div>
 

         </div>
     </div>
  </div>
<!-- ENDModal -->
 
<!-- ENDModal -->


  <table id="selleroffice" style="width:100%" class="table table-hover table-bordered  dt-responsive nowrap display   selleroffice "   cellspacing="0" width="100%">
<thead>
      <tr>
      <th>No</th>
   <th>Location</th>
      <th>Telephone</th>
       <th>Government</th>
      <th>District</th>
       <th>Adress</th>
      
      <th>Company</th>
            <th>Process</th>
          
    </tr>
  </thead>
 
  <tbody id="tbody">
    <?php $i=1;  ?>

    @foreach($selleroffice as $selleroffice)

      <tr> 

        <td>{{$i++}}</td>
           <td><a class="testEdit"   data-type="text" data-name="Location" data-pk="{{ $selleroffice->id }}">{{$selleroffice->Location}}</a></td>

              <td><a class="testEdit"   data-type="number" data-name="telephone" data-pk="{{ $selleroffice->id }}">{{$selleroffice->Telephone}}</a></td>

<td>   <a class="testEdit" data-type="select"  data-value="{{$governments}}" data-source ="{{$governments}}" data-name="Government"  data-pk="{{$selleroffice->id}}"> 
 {{$selleroffice->Government}}
  
  </a></td>

   <td>   <a class="testEdit" data-type="select"  data-value="{{$districts}}" data-source ="{{$districts}}" data-name="District"  data-pk="{{$selleroffice->id}}"> 
 {{$selleroffice->District}}
  
  </a></td>


       <td><a class="testEdit"   data-type="text" data-name="Address" data-pk="{{ $selleroffice->id }}">{{$selleroffice->Address}}</a></td>

       
  <td > <a class="testEdit" data-type="select"  data-value="{{$selleroffice->getcompany->Name}}" data-source ="{{$companies}}" data-name="company_id"  data-pk="{{$selleroffice->id}}"> 
  {{$selleroffice->getcompany->Name}}
  </a>
 </td>

       <td>
       <a href="/offlineneew/public/selleroffice/destroy/{{$selleroffice->id}}"class="btn btn-default btn-sm" ><span class="glyphicon glyphicon-trash"></span>  </a>
       </td>       
             
      </tr>    
    @endforeach 

    

 </tbody>
  <tfoot>
      <th></th>
 <th>Location</th>
      <th>Telephone</th>
       <th>Government</th>
      <th>District</th>
       <th>Adress</th>
      
      <th>Company</th>
            <th>Process</th>

  </tfoot>
</table> 
  </div>
                </div>
              </div>  
    </section>
@section('other_scripts')
<script type="text/javascript">
 $(".attribute").chosen({ 
                   width: '100%',
                   no_results_text: "There is no result",
                   allow_single_deselect: true, 
                   search_contains:true, });
                    $(".attribute").trigger("chosen:updated");
                    
                    $("#Government").trigger("chosen:updated");
                          $("#Government").chosen({ 
                   width: '100%',
                   no_results_text: "There is no result",
                   allow_single_deselect: true, 
                   search_contains:true, });
                    $("#Government").trigger("chosen:updated");
                        

                   
  $(document).ready(function(){
        $('#Government').change(function(){
        $.get("{{ url('api/dropdown')}}", 
        { option: $(this).val() }, 
        function(data) {

            $('#district').empty(); 
            $.each(data, function(key, element) {
                $('#district').append("<option value='" + key +"'>" + element + "</option>");
            });
             $('#district').chosen({
                width: '100%',
                no_results_text:'No Results'
            });
                $("#district").trigger("chosen:updated");
            $("#district").trigger("change");
            });
       
    });
      

    var table= $('.selleroffice').DataTable({ 
        responsive: true,
    "order":[[0,"asc"]],
    'searchable':true,
    "scrollCollapse":true,
    "paging":true,
    "pagingType": "simple",
      dom: 'lBfrtip',
        buttons: [
            
            { extend: 'excel', className: 'btn btn-primary dtb' }
            
            
        ],
        
  fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
$.fn.editable.defaults.send = "always";

$.fn.editable.defaults.mode = 'inline';

$('.testEdit').editable({

      validate: function(value) {
        name=$(this).editable().data('name');

          if(name=="telephone"||name=="Government"||name=="District"||name=="company_id")
        {
                if($.trim(value) == '') {
                    return 'Value is required.';
                  }
                    }
             
        },

    placement: 'right',
    url:'{{URL::to("/")}}/updateSellOffier',
  
     ajaxOptions: {
     type: 'get',
    sourceCache: 'false',
     dataType: 'json'

   },

       params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');

            // console.log(params);
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'This Data Already Exist,Enter Correct Data.';
            } else {
                return response.responseText;
            }
        }
        ,
        success:function(response)
        {
            if(response.status==="You do not have permission.")
             {
              return 'You do not have permission.';
              }
        }


});



 

}


});
   



   $('.selleroffice tfoot th').each(function () {



      var title = $('.selleroffice thead th').eq($(this).index()).text();
               
 if($(this).index()>=1 && $(this).index()<=7)
        {

           $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
}
        

    });

table.columns().every( function () {
    var that = this;
  $(this.footer()).find('input').on('keyup change', function () {
    that.search(this.value).draw();
        if (that.search(this.value) ) {
            that.search(this.value).draw();
        }
    });     
    });
 $.validator.addMethod("checktelephone", 
        function(value, element) {
            var result = false;
            $.ajax({
                  data: {telephone:telephone},
    url: '/offlineneew/public/telephone',
    type: 'get',

    beforeSend: function (request) {
        return request.setRequestHeader('X-CSRF-Token', $("meta[name='csrf-token']").attr('content'));
        },

                success: function(data) {
                    result = (data == true) ? true : false;
                }
            });
            // return true if username is exist in database
            return result; 
        }, 
        "This username is already taken! Try another."
    );
   $.validator.addMethod("alpha", function(value, element) {
    return this.optional(element) || /^[a-zA-Z\u0600-\u06FF,-][\sa-zA-Z\u0600-\u06FF,-]/i.test(value);
 });

 //for all select
// $.validator.setDefaults({ ignore: ":hidden:not(.cement)" });

    
  $('#profileForm')
    .find('[name="company_id"]')
            .chosen({
                width: '100%',
                inherit_select_classes: true
            }).change(function(){
         
        $.get("{{ url('Product/dropdownlist')}}",
        { 
          option: $(this).val() 
        }, 
        function(data){
           console.log(data);
           $("#Productsid").empty(); 
            $.each(data, function(key, element) {
              $("#Productsid").append("<option value='" + key +"'>" + element + "</option>");
 
            });


             $(".attribute").chosen({ 
                   width: '100%',
                   no_results_text: "There is no result",
                   allow_single_deselect: true, 
                   search_contains:true, });
                    $(".attribute").trigger("chosen:updated");
          }
        );
    }).end()
        .formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                 company_id: {
                    validators: {
                        callback: {
                            message: 'Please choose company',
                            callback: function(value, validator, $field) {
                                // Get the selected options
                                var options = validator.getFieldElements('company_id').val();
                              
                                return (options !== '' );
                            }
                        }
                    }
                },
            'Location': {
                    validators: {
                      notEmpty:{
                    message:"the Location is required"
                  },
             
                    }
                },
                'Government': {
                    validators: {
                      notEmpty:{
                    message:"the Government is required"
                  },
             
                    }
                },
              
                'District': {
                    validators: {
                      notEmpty:{
                    message:"the District is required"
                  },
             
                    }
                },
                    'Telephone': {
                    validators: {
                  notEmpty:{
                    message:"the Telephone is required"
                  },
                    
                      remote: {
                        url: '/offlineneew/public/telephone',
                      
                        message: 'The Telephone  is not available',
                        type: 'GET'
                    },
                    }
                }
            } ,submitHandler: function(form) {
        form.submit();
    }
        })
        .on('success.form.fv', function(e) {
   // e.preventDefault();
    var $form = $(e.target);

    // Enable the submit button
   
    $form.formValidation('disableSubmitButtons', false);
});

});

</script>




@stop
@stop