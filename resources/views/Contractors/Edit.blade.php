@extends('masternew')


@section('content')


<div class="row">

<div class="col-md-2 col-xs-12 ">
 </div>

              <div class="col-md-12 col-xs-12 ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Edit Contractors</h2>
                    
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
     {!!Form::open(['route' =>[ 'Contractors.update',$Contractors->id],'method' => 'put','class'=>'form-horizontal form-label-left 
                    ','id'=>'profileForm'])!!}

                    
  <input type="hidden" name="_token" value="{{ csrf_token() }}">

       <input type="hidden" id="id" name="id" value="{{ $Contractors['id'] }}">

<fieldset style="    padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;" >
  <legend style="width: fit-content;border: 0px;" dir="rtl">Main Data</legend>
 

<div class="form-group">

<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
<input type="text" class="form-control has-feedback-left"  id="Name" name="Name" value="{{$Contractors->Name}}" placeholder="enter Name" required>
<span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
</div>

<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
<input type="text" class="form-control has-feedback-left" id="Name" name="EngName" value="{{$Contractors->EngName}}" placeholder="enter Eng Name" >
<span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
</div>
</div>

<div class="form-group">

<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
<input type="text" class="form-control has-feedback-left"  id="Name" name="Email" value="{{$Contractors->Email}}" placeholder="enter  Email" >
<span class="fa fa-envelope form-control-feedback left" aria-hidden="true"></span>
</div>

<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">

 <input type="text" class="form-control has-feedback-left" id="startdate" name="Birthday" placeholder="enter Birthday" value="{{$Contractors->Birthday}}">
<span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
</div>
</div>

<div class="form-group">

<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
<input type="text" class="form-control has-feedback-left"  id="Name" name="Nickname" value="{{$Contractors->Nickname}}" placeholder="enter Nickname" >
<span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
</div>

<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">

 <input type="text" class="form-control has-feedback-left" id="Name" name="Religion" value="{{$Contractors->Religion}}" placeholder="enter Religion" >
<span class="fa fa-bars form-control-feedback left" aria-hidden="true"></span>
</div>
</div>
<div class="form-group">

<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
<input type="text" class="form-control has-feedback-left"  id="Name" name="Fame"  value="{{$Contractors->Fame}}" placeholder="enter Fame" >
<span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
</div>

<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">

 <input type="number" class="form-control has-feedback-left"  minlength="7" maxlength="11" id="Name" name="Home_Phone" value="{{$Contractors->Home_Phone}}" placeholder="enter Home_Phone" >
<span class="fa fa-phone form-control-feedback left" aria-hidden="true"></span>
</div>
</div>


<div class="form-group">

<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
<input type="number" class="form-control has-feedback-left"  id="Name" name="Tele1" value="{{$Contractors->Tele1}}" placeholder="enter Tele1">
<span class="fa fa-phone form-control-feedback left" aria-hidden="true"></span>
</div>

<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
<input type="number" class="form-control has-feedback-left" id="Name" name="Tele2" value="{{$Contractors->Tele2}}" placeholder="enter Tele2" >
<span class="fa fa-phone form-control-feedback left" aria-hidden="true"></span>
</div>
</div>
                     
</fieldset>

<fieldset style="    padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;" >
  <legend style="width: fit-content;border: 0px;" dir="rtl">Address</legend>
 

<div class="form-group">

<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
 {!!Form::select('Government', ([null => 'Select  Government'] + $government->toArray()) ,$Contractors->Government,['class'=>' chosen-select','id' => 'Government','required'=>'required'])!!}
</div>

<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
{!!Form::select('district', ([null => 'Select  District'] + $districts->toArray()) ,$Contractors->District,['class'=>'form-control chosen-select District ','id' => 'district','required'=>'required'])!!}


</div>
</div>
  
<div class="form-group">

<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
<input type="text" class="form-control has-feedback-left"  id="Name" name="Address" value="{{$Contractors->Address}}" placeholder="enter Address" >
<span class="fa fa-map-marker form-control-feedback left" aria-hidden="true"></span>
</div>


</div>

</fieldset>  
<fieldset style="    padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;" >
  <legend style="width:fit-content;border: 0px;" dir="rtl">Work and Eduction</legend>
 

<div class="form-group">
<label class="col-md-2 col-sm-2 col-xs-12 ">Education</label>
<div class="col-md-3 col-sm-3 col-xs-12 has-feedback">
 {!!Form::select('Education',
                    array( 'High Education' => 'High Education',
                    'Low Education' => 'Low Education','No Data' => 'No Data',
                    'Medium Education' => 'Medium Education',
                    'No Education' => 'No Education')
                   ,$Contractors->Education,['id' => 'Education','class'=>'chosen-select TypeP form-control']);!!}


</div>

<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
<input type="text" class="form-control has-feedback-left"  id="Name" name="Job" value="{{$Contractors->Job}}" placeholder="enter Job" >
<span class="fa fa-briefcase form-control-feedback left" aria-hidden="true"></span>



</div>
</div>

</fieldset> 


<fieldset style="    padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;" >
  <legend style="width: fit-content;border: 0px;" dir="rtl">Other Data</legend>
 

<div class="form-group">
<label class="col-md-2 col-sm-2 col-xs-12 ">Has Facebook</label>
<div class="col-md-3 col-sm-3 col-xs-12 has-feedback">
{!!Form::select('Has_Facebook',
                    array( 'Yes' => 'Yes',
                    'No' => 'No')
                   ,$Contractors->Has_Facebook,['id' => 'Has_Facebook','class'=>'chosen-select TypeP form-control']);!!} 


</div>

<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
<input type="text" class="form-control has-feedback-left"  id="Facebook_Account" name="Facebook_Account" value="{{$Contractors->Facebook_Account}}" placeholder="enter  Facebook Account" >

<span class="fa fa-facebook-official form-control-feedback left" aria-hidden="true"></span>



</div>
</div>
  
<div class="form-group">
<label class="col-md-2 col-sm-2 col-xs-12 ">Has Computer</label>
<div class="col-md-3 col-sm-3 col-xs-12 has-feedback">
{!!Form::select('Computer',
                    array( 'Yes' => 'Yes',
                    'No' => 'No')
                   ,$Contractors->Computer,['id' => 'Computer','class'=>'chosen-select TypeP form-control']);!!} 

</div>
<label class="col-md-2 col-sm-2 col-xs-12 ">Phone Type</label>
<div class="col-md-3 col-sm-3 col-xs-12 has-feedback">
 {!!Form::select('Phone_Type',
                    array( 'Old' => 'Old',
                    'New' => 'New')
                   ,$Contractors->Phone_Type,['id' => 'Phone_Type','class'=>'chosen-select TypeP form-control']);!!}

</div>
</div>
<div class="form-group">
<div class="col-md-5 col-sm-5 col-xs-12 has-feedback">
<input type="number" class="form-control has-feedback-left"  id="Name" name="Intership_No" value="{{$Contractors->Intership_No}}" placeholder="enter Intership_No" >
  <span class="fa fa-bars form-control-feedback left" aria-hidden="true"></span>
</div>
</fieldset>         






       
    
 




                          
 

                  
               
                    


             <input type="hidden" name="Type_User" value="Outdoor Promoters"> 

                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                          <button type="button" class="btn btn-primary " href="/offlineneew/public/users">Cancel</button>
                          
                         <button class="btn btn-primary" type="reset">Reset</button>
                          <button type="submit"  name="login_form_submit" class="btn btn-success" value="Save">Submit</button>
                        </div>
                      </div>

      {!!Form::close() !!} 
                  </div>
                </div>
                 </div>
                
                 <div class="col-md-2 col-xs-12 ">
                 </div>
                </div>

@section('other_scripts')
<script type="text/javascript">

  $(document).ready(function(){

     $('#startdate').datepicker({
     language: 'ar',
     format: "yyyy-mm-dd"
});
     

          $('#Government').change(function(){
        $.get("{{ url('api/newdropdown')}}", 
        { option: $(this).val() }, 
        function(data) {
          
            $('#district').empty(); 
            $.each(data, function(key, element) {
                $('#district').append("<option value='" + key +"'>" + element + "</option>");
            });
        });
    });
$('.Government').chosen({
                width: '100%',
                no_results_text:'No Results'
            });
                $(".Government").trigger("chosen:updated");
            $(".Government").trigger("change");
 });
        $('#Has_Facebook').change(function(){
            if($(this).val() == "No")
            {
              $("#Facebook_Account").attr("disabled","disabled");
            }
    });
         $('#profileForm')
         .find('[name="Government"]')
        
            .chosen({
                width: '100%',
                inherit_select_classes: true
            })
          .end()
        .formValidation({

            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                'Name': {
                         validators: {
              
                  notEmpty:{
                    message:"the contractor name is required"
                  },
                    regexp: {
                        regexp: /^[\u0600-\u06FF\s]+$/,
                        message: 'Name must be arabic'
                    }
                    }
               
                  },
                   'EngName': {
                  
                       validators: {
                
                    regexp: {
                        regexp: /^[a-zA-Z\s]+$/,
                        message: 'The English name can only consist of alphabetical and spaces'
                    }
                    }

                    }
                 
                ,'Government':{
                       validators: {
                  notEmpty:{
                    message:"the Governmentis required"
                  }
                    }

                    }
                    ,"Address": {
                  
                       validators: {
                    regexp: {
                        regexp: /^[\\.\/a-zA-Z\u0600-\u06FF0-9\s]+$/,
                        message: 'The Address can only consist of alphabetical and spaces'
                    }
                    }

                    },'Education': {
                  
                       validators: {
                    regexp: {
                        regexp: /^[a-zA-Z\u0600-\u06FF\s]+$/,
                        message: 'The Education can only consist of alphabetical and spaces'
                    }
                    }

                    }
                    ,'Email':{
                  
                       validators: {
                                        remote: {
                        url: '/offlineneew/public/checkContractorEmail',
                         data:function(validator, $field, value)
                        {return{id:validator.getFieldElements('id').val()};},
                        message: 'The email is not available',
                        type: 'GET'
                    },
                  emailAddress:{
                    message:"this Email is not valid"
                  }
                    }

                    }
                    ,'Tele1':{
                  
                       validators: {
                                              remote: {
                        url: '/offlineneew/public/checkContractorTel1',
                         data:function(validator, $field, value)
                        {return{id:validator.getFieldElements('id').val()};},
                        message: 'The  Telephone already Exsits',
                        type: 'GET'
                    },
                       regexp: {
                        regexp: /^\d{11}$/,
                        message: 'The Telephone can only consist of numbers and length 11'
                    },
                  notEmpty:{
                    message:"the Telephone is required"
                  }
                    }

                    }
                    ,'Tele2':{
                  
                       validators: {
                                              remote: {
                        url: '/offlineneew/public/checkContractorTel2',
                         data:function(validator, $field, value)
                        {return{id:validator.getFieldElements('id').val()};},
                        message: 'The  Telephone is not available',
                        type: 'GET'
                    },   regexp: {
                        regexp: /^\d{11}$/,
                        message: 'The Telephone can only consist of numbers and length 11'
                    },
                                   }

                    }
                        ,'Job':{
                  
                       validators: {
                 regexp: {
                        regexp: /^[a-zA-Z\u0600-\u06FF\s]+$/,
                        message: 'The Job can only consist of alphabetical and spaces'
                    }
                    }

                    },'Nickname':{
                  
                       validators: {
                 regexp: {
                        regexp:/^[a-zA-Z\u0600-\u06FF\s]+$/,
                        message: 'The Nickname can only consist of alphabetical and spaces'
                    }
                    }

                    },'Religion':{
                  
                       validators: {
                 regexp: {
                        regexp: /^[a-zA-Z\u0600-\u06FF\s]+$/,
                        message: 'The Education can only consist of alphabetical and spaces'
                    }
                    }

                    },'Fame':{
                  
                       validators: {
                 regexp: {
                        regexp: /^[a-zA-Z\u0600-\u06FF\s]+$/,
                        message: 'The Fame can only consist of alphabetical and spaces'
                    }
                    }

                    }
                    ,'Home_Phone':{
                  
                       validators: {
                       numeric: {
                        message: 'The Home Phone can only consist of numbers'
                    }
                    }

                    }

            } ,submitHandler: function(form) {
        form.submit();
    }
        })
        .on('success.form.fv', function(e) {
   // e.preventDefault();
    var $form = $(e.target);

    // Enable the submit button
   
    $form.formValidation('disableSubmitButtons', false);
});

</script>


</div>
</section>
@stop
@stop