@extends('masternew')
@section('content')
 <section class="panel-body">

<?php
 if(!empty($_COOKIE['FileError'])) {     
    echo "<div class='alert alert-block alert-danger fade in center'>";
    echo $_COOKIE['FileError'];
    echo "</div>";
  }
  ?>


{!! Form::open(['action' => "ContractorController@getNewcontractor",'method'=>'post','class'=>'form-horizontal form-label-left 
                    ','id'=>'profileForm']) !!}  


   <fieldset style="    padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;">
  <legend style="width: fit-content;border: 0px;">Enter Date:</legend>
 

 
                    <div class="form-group">

                      <div class="col-md-4 col-sm-4 col-xs-12 has-feedback">
                        <input type='text' name="Start_Date"  id="Start_Date" placeholder=" Start Date" class="form-control has-feedback-left Start_Date"  >


                        <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                         {!!$errors->first('Start_Date','<small class="label label-danger">:message</small>')!!}

                      </div>
                       <div class="col-md-4 col-sm-4 col-xs-12 has-feedback">
                        <input type='text' name="End_Date" id="End_Date" placeholder=" End_Date" class="form-control has-feedback-left End_Date"   >


                        <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                         {!!$errors->first('End_Date','<small class="label label-danger">:message</small>')!!}


                      </div>

    <div class="col-md-4 selectContainer">
  
</div>



                      </div>
  
                    
       <div >
        <label style="color:red" id="warn"></label> 
     </div>
<div class="row" style="margin-top: 20px;" align="center" >
                                <div  >
            <input type="submit" class="btn btn-primary"  id="Search" value=" Press To Show Report" title="Press To Show Report">
              
            </div>  
            </div>
</fieldset>
 
       {!!Form::close() !!}  
                      <button class="btn btn-success" style="margin-top: 20px;" id="Export">Export</button>
<table class="table table-hover table-bordered  dt-responsive nowrap display tasks" id="tasks" cellspacing="0" width="100%">




 <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                     <h2 ><i class="fa fa-tasks" ></i>Contractors </h2>
                        <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                     
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

  




                   
                    <table class="table table-hover table-bordered  dt-responsive nowrap Brands" cellspacing="0" width="100%" >
  <thead>
 <th>No</th>
              <th>Name</th>
            
              </thead>
              <tfoot>
                <th></th>
              <th>Name</th>
                
</tfoot>


<tbody style="text-align:center;">
<?php $i=1;?>

 @foreach($Contractor as $Con)
 <tr>
    
     <td>{{$i++}}</td>
       <td>{{$Con->Name}}</td>
     </tr>
          @endforeach
</tbody>
 
</table>


                  </div>
               





                </div>
              </div>
 
  @section('other_scripts')


<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>



  <script type="text/javascript" >

var editor;

  $(document).ready(function( ){
$(function(){
  $('#Start_Date').daterangepicker({
        singleDatePicker: true,
        singleClasses: "picker_4",
         locale: {
            format: 'YYYY-MM-DD'
        }
      }, function(start) {
        console.log(start.toISOString(), 'Hadeel');
      });
  $('#End_Date').daterangepicker({
        singleDatePicker: true,
        singleClasses: "picker_4",
         locale: {
            format: 'YYYY-MM-DD'
        }
      }, function(start) {
        console.log(start.toISOString(), 'Hadeel');
      });


 





 });
    $('.District').chosen({
                width: '100%',
                no_results_text:'No Results'
            });
                $(".District").trigger("chosen:updated");
            $(".District").trigger("change");

        $('#Has_Facebook').change(function(){
            if($(this).val() == "No")
          
              $("#Facebook_Account").attr("disabled","disabled");
                  else
              $("#Facebook_Account").removeAttr('disabled');
    });

 $('#profileForm')
        .find('[name="Government"]')
            .chosen({
                width: '100%',
                inherit_select_classes: true
            })
            // Revalidate the color when it is changed
            .change(function(e) {

                $('#profileForm').formValidation('revalidateField', 'Government');

  $.getJSON("/offlineneew/public/government/district/" + $("#Government").val(), function(data) {
 
                var $contractors = $(".District");
                   $(".District").chosen("destroy");
                $contractors.empty();
                $.each(data, function(index, value) {
                    $contractors.append('<option value="' + index +'">' + value + '</option>');
                });
                  
        
             $('.District').chosen({
                width: '100%',
                no_results_text:'No Results'
            });
                $(".District").trigger("chosen:updated");
            $(".District").trigger("change");
            });
            }).end()


     
        .formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                'Name': {
                         validators: {
              
                  notEmpty:{
                    message:"the contractor name is required"
                  },
                    regexp: {
                        regexp: /^[\u0600-\u06FF\s]+$/,
                        message: 'Name must be arabic'
                    }
                    }
               
                  },
                   'EngName': {
                  
                       validators: {
                    regexp: {
                        regexp: /^[a-zA-Z\s]+$/,
                        message: 'The English name can only consist of alphabetical and spaces'
                    }
                    }

                    }  
                    ,'Birthday':
                   {
                      validators: {
                     date: {
                        message: 'The date is not valid',
                        format: 'YYYY-MM-DD',
                        // min and max options can be strings or Date objects
                        min: '1950-01-01',
                        max: '2001-12-30'
                    }
                    }
                }
                ,'Government':{
                       validators: {
                  notEmpty:{
                    message:"the Governmentis required"
                  }
                    }

                    },
                    'district':{
                       validators: {
                  notEmpty:{
                    message:"the District required"
                  }
                    }

                    }
                    ,"Address": {
                  
                       validators: {
                    regexp: {
                        regexp: /^[\\.\/a-zA-Z\u0600-\u06FF0-9\s]+$/,
                        message: 'The Address can only consist of alphabetical and spaces'
                    }
                    }

                    },'Education': {
                  
                       validators: {
                    regexp: {
                        regexp: /^[a-zA-Z\u0600-\u06FF\s]+$/,
                        message: 'The Education can only consist of alphabetical and spaces'
                    }
                    }

                    }
                    ,'Email':{
                  
                       validators: {
                                        remote: {
                        url: '/offlineneew/public/checkContractorEmail',
                        message: 'The email is not available',
                        type: 'GET'
                    },
                  emailAddress:{
                    message:"the Email Not Valid"
                  }
                    }

                    }
                    ,'Tele1':{
                  
                       validators: {
                                              remote: {
                        url: '/offlineneew/public/checkContractorTel1',
                        message: 'The  Telephone already Exsits',
                        type: 'GET'
                    },
                      regexp: {
                        regexp: /^\d{11}$/,
                        message: 'The Telephone can only consist of numbers and length 11'
                    },
                      notEmpty:{
                        message:"the Telephone is required"
                      }
                    }

                    }
                    ,'Tele2':{
                  
                       validators: {
                           stringLength: {
        min:7,
                        max: 11,
                        message: 'The Telephone must be between 6 and 11 digits'
                    },
                                              remote: {
                        url: '/offlineneew/public/checkContractorTel2',
                        message: 'The  Telephone is not available',
                        type: 'GET'
                    },
                                   }

                    }
                    ,'Job':{
                  
                       validators: {
                 regexp: {
                        regexp: /^[a-zA-Z\u0600-\u06FF\s]+$/,
                        message: 'The Job can only consist of alphabetical and spaces'
                    }
                    }

                    },'Nickname':{
                  
                       validators: {
                 regexp: {
                        regexp: /^[a-zA-Z\u0600-\u06FF\s]+$/,
                        message: 'The Nickname can only consist of alphabetical and spaces'
                    }
                    }

                    },'Religion':{
                  
                       validators: {
                 regexp: {
                        regexp: /^[a-zA-Z\u0600-\u06FF\s]+$/,
                        message: 'The Education can only consist of alphabetical and spaces'
                    }
                    }

                    },'Fame':{
                  
                       validators: {
                 regexp: {
                        regexp: /^[a-zA-Z\u0600-\u06FF\s]+$/,
                        message: 'The Fame can only consist of alphabetical and spaces'
                    }
                    }

                    },'Home_Phone':{
                  
                       validators: {
                 numeric: {
                        message: 'The Home Phone can only consist of numbers'
                    }
                    }

                    }

            } ,submitHandler: function(form) {
        form.submit();
    }
        })
        .on('success.form.fv', function(e) {
   // e.preventDefault();
    var $form = $(e.target);

    // Enable the submit button
   
    $form.formValidation('disableSubmitButtons', false);
});
     var table= $('.Brands').DataTable({
       
    select:true,
    responsive: true,
    "order":[[0,"asc"]],
    'searchable':true,
    "scrollCollapse":true,
    "paging":true,
    "pagingType": "simple",
      dom: 'lBfrtip',
        buttons: [
           
            
            { extend: 'excel', className: 'btn btn-primary dtb' }
            
            
        ],

fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
$.fn.editable.defaults.send = "always";

$.fn.editable.defaults.mode = 'inline';

$('.testEdit').editable({

      validate: function(value) {
        name=$(this).editable().data('name');
        if(name=="Name")
        {
                if($.trim(value) == '') {
                    return 'Value is required.';
                  }
                    }
      
        },

    placement: 'right',
    url:'{{URL::to("/")}}/updateContractors',
  
     ajaxOptions: {
     type: 'get',
    sourceCache: 'false',
     dataType: 'json'

   },

       params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');

            // console.log(params);
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'This Data Already Exist,Enter Correct Data.';
            } else {
                return response.responseText;
            }
        }


});

}
});
   $('.Brands tfoot th').each(function () {
      var title = $('.Brands thead th').eq($(this).index()).text();       
     if($(this).index()>=1 && $(this).index()<=21)
            {

           $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
}
        

    });
  table.columns().every( function () {
  var that = this;
 $(this.footer()).find('input').on('keyup change', function () {
          that.search(this.value).draw();
            if (that.search(this.value) ) {
               that.search(this.value).draw();
           }
        });
      
    });

 });



  </script>
@stop

@stop
