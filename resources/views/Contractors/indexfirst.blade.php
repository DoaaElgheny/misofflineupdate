@extends('masternew')
@section('content')
 <section class="panel-body">

<?php
 if(!empty($_COOKIE['FileError'])) {     
    echo "<div class='alert alert-block alert-danger fade in center'>";
    echo $_COOKIE['FileError'];
    echo "</div>";
  }
  ?>
 <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                     <h2 ><i class="fa fa-tasks" ></i>Contractors </h2>
                        <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                     
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

  
<a type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal1" style="margin-bottom: 20px;" >Add New Contractor </a>


<!-- model -->
 
<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
     <div class="modal-content">
    
          <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                 <h4 class="modal-title span7 text-center" id="myModalLabel"><span class="title">Add New Contractor</span></h4>
          </div>
    <div class="modal-body">
        <div class="tabbable"> <!-- Only required for left/right tabs -->
        <ul class="nav nav-tabs">
  

        
        </ul>
        <div class="tab-content">
        <div class="tab-pane active" id="tab1">
              <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                 
                  <div class="x_content">
                    <br />
                   {!! Form::open(['action' => "ContractorController@store",'method'=>'post','class'=>'form-horizontal form-label-left 
                    ','id'=>'profileForm']) !!} 


                 
             <fieldset style="    padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;" >
  <legend style="width: fit-content;border: 0px;" dir="rtl">Main Data</legend>
 

<div class="form-group">

<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
<input type="text" class="form-control has-feedback-left"  id="Name" name="Name" placeholder="Name">
<span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
</div>

<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
<input type="text" class="form-control has-feedback-left" id="EngName" name="EngName" placeholder="English Name">
<span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
</div>
</div>

<div class="form-group">

<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
<input type="text" class="form-control has-feedback-left"  id="Name" name="Email" placeholder="enter  Email" >
<span class="fa fa-envelope form-control-feedback left" aria-hidden="true"></span>
</div>

<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">

 <input type="text" class="form-control has-feedback-left" id="single_cal4" name="Birthday" placeholder="Enter Birthday">



<span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
</div>



</div>

<div class="form-group">

<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
<input type="text" class="form-control has-feedback-left"  id="Name" name="Nickname" placeholder="Enter Nickname" >
<span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
</div>

<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">

 <input type="text" class="form-control has-feedback-left" id="Name" name="Religion" placeholder="Enter Religion" >
<span class="fa fa-bars form-control-feedback left" aria-hidden="true"></span>
</div>
</div>
<div class="form-group">

<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
<input type="text" class="form-control has-feedback-left"  iid="Name" name="Fame" placeholder="Enter Family Name" >
<span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
</div>

<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">

 <input type="text" class="form-control has-feedback-left" id="Name" minlength="7" maxlength="11" name="Home_Phone" placeholder="Enter Home Phone"> 
<span class="fa fa-phone form-control-feedback left" aria-hidden="true"></span>
</div>
</div>


<div class="form-group">

<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
<input type="text" class="form-control has-feedback-left"  iid="Name" name="Tele1" placeholder="enter Tele1" required>
<span class="fa fa-phone form-control-feedback left" aria-hidden="true"></span>
</div>

<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
<input type="number" class="form-control has-feedback-left" id="Name" name="Tele2" placeholder="enter Tele2" >
<span class="fa fa-phone form-control-feedback left" aria-hidden="true"></span>
</div>
</div>
                     
</fieldset>
 
<fieldset style="    padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;" >
  <legend style="width: fit-content;border: 0px;" dir="rtl">Address</legend>
 

<div class="form-group">

<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
 {!!Form::select('Government', ([null => 'Select  Government'] + $governments->toArray()) ,null,['class'=>' chosen-select','id' => 'Government','required'=>'required'])!!}
</div>

<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
{!!Form::select('district', ([null => 'Select  District'] + $districts->toArray()) ,null,['class'=>'form-control chosen-select District ','id' => 'district','required'=>'required'])!!}
</div>
</div>
  
<div class="form-group">

<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
<input type="text" class="form-control has-feedback-left"  id="Name" name="Address" placeholder="enter Address">
<span class="fa fa-map-marker form-control-feedback left" aria-hidden="true"></span>
</div>


</div>

</fieldset>  
<fieldset style="    padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;" >
  <legend style="width:fit-content;border: 0px;" dir="rtl">Work and Eduction</legend>
 

<div class="form-group">
<label class="col-md-2 col-sm-2 col-xs-12 ">Education</label>
<div class="col-md-3 col-sm-3 col-xs-12 has-feedback">
 {!!Form::select('Education',
                    array( 'High Education' => 'High Education',
                    'Low Education' => 'Low Education','No Data' => 'No Data',
                    'Medium Education' => 'Medium Education',
                    'No Education' => 'No Education')
                   ,[null => 'Select Education'],['id' => 'Education','class'=>'chosen-select TypeP form-control']);!!} 


</div>

<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
<input type="text" class="form-control has-feedback-left"  id="Name" name="Job" placeholder="enter Job" >
<span class="fa fa-briefcase form-control-feedback left" aria-hidden="true"></span>



</div>
</div>

</fieldset> 


<fieldset style="    padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;" >
  <legend style="width: fit-content;border: 0px;" dir="rtl">Other Data</legend>
 

<div class="form-group">
<label class="col-md-2 col-sm-2 col-xs-12 ">Has Facebook</label>
<div class="col-md-3 col-sm-3 col-xs-12 has-feedback">
 {!!Form::select('Has_Facebook',
                    array( 'Yes' => 'Yes',
                    'No' => 'No')
                   ,[null => 'Select Choose'],['id' => 'Has_Facebook','class'=>'chosen-select TypeP form-control']);!!}  


</div>

<div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
<input type="text" class="form-control has-feedback-left"  id="Facebook_Account" name="Facebook_Account" placeholder="enter Eng Facebook_Account" >

<span class="fa fa-facebook-official form-control-feedback left" aria-hidden="true"></span>



</div>
</div>
  
<div class="form-group">
<label class="col-md-2 col-sm-2 col-xs-12 ">Has Computer</label>
<div class="col-md-3 col-sm-3 col-xs-12 has-feedback">

 {!!Form::select('Computer',
                    array( 'Yes' => 'Yes',
                    'No' => 'No')
                   ,[null => 'Select Has Computer'],['id' => 'Computer','class'=>'chosen-select TypeP form-control']);!!} 

</div>
<label class="col-md-2 col-sm-2 col-xs-12 ">Phone Type</label>
<div class="col-md-3 col-sm-3 col-xs-12 has-feedback">
 {!!Form::select('Phone_Type',
                    array( 'Old' => 'Old',
                    'New' => 'New')
                   ,[null => 'Select Phone Type'],['id' => 'Phone_Type','class'=>'chosen-select TypeP form-control']);!!} 

</div>
</div>
<div class="form-group">
<div class="col-md-5 col-sm-5 col-xs-12 has-feedback">
<input type="number" class="form-control has-feedback-left"  id="Name" name="Intership_No" placeholder="enter Intership_No">
  <span class="fa fa-bars form-control-feedback left" aria-hidden="true"></span>
</div>
</fieldset>         






                       


                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                          <button type="button" class="btn btn-primary " data-dismiss="modal">Cancel</button>
                         <button class="btn btn-primary" type="reset">Reset</button>
     <button type="submit"  name="login_form_submit" class="btn btn-success" value="save">Save</button>
                        </div>
                      </div>

      {!!Form::close() !!}                
        </div>
                </div>
                   </div>
                </div>
        </div>
        </div>
        </div>

    </div>
 

         </div>
     </div>
  </div>
<!-- ENDModal -->



                   
                    <table class="table table-hover table-bordered  dt-responsive nowrap Brands" cellspacing="0" width="100%" >
  <thead>
 <th>No</th>
              <th>Name</th>
               <!-- <th> Eng Name</th> -->
              <th>Government</th>
              <th>District</th>
              <th>Tele1</th>
              <th>Code</th>
               <th>Address</th>
               <th> Education</th>
              <th>Has_Facebook</th>
              <th>Facebook_Account</th>
               <th>Computer</th>
              <th>Email</th>
              <th>Birthday</th>
               <th>Tele2</th>
              <th>Job</th>
              <th>Intership_No</th>
              <th>Phone_Type</th>
              <th>Nickname</th>
              <th>Religion</th>
              <th>Home_Phone</th>
              <th>Fame</th>
              <th>Status</th>
              <th>Edit</th>
              <th>Process</th> 
              </thead>
              <tfoot>
                <th></th>
              <th>Name</th>
               <!-- <th> Eng Name</th> -->
              <th>Government</th>
              <th>District</th>
              <th>Tele1</th>
              <th>Code</th>
               <th>Address</th>
               <th> Education</th>
              <th>Has_Facebook</th>
              <th>Facebook_Account</th>
               <th>Computer</th>
              <th>Email</th>
              <th>Birthday</th>
               <th>Tele2</th>
              <th>Job</th>
              <th>Intership_No</th>
              <th>Phone_Type</th>
              <th>Nickname</th>
              <th>Religion</th>
              <th>Home_Phone</th>
              <th>Fame</th>
              <th>Status</th>
              <th>Edit</th>
              <th>Process</th>      
</tfoot>


<tbody style="text-align:center;">
<?php $i=1;?>


 @foreach($Contractors as $contractor)

    <tr>
    
      <td>{{$i++}}</td> 
      <td>{{$contractor->Name}}</td>
      <!-- <td>{{$contractor->EngName}}</td> -->
      <td>{{$contractor->Government}}</td>
      <td>{{$contractor->District}}</td>
      <td>{{$contractor->Tele1}}</td>
      <td>{{$contractor->Code}}</td>
      <td>{{$contractor->Address}}</td>
      <td>{{$contractor->Education}}</td>
      <td>{{$contractor->Has_Facebook}}</td>
      <td>{{$contractor-> Facebook_Account}}</td>
      <td>{{$contractor->Computer}}</td>
      <td>{{$contractor->Email}}</td>
      <td>{{$contractor->Birthday}}</td>
      <td>{{$contractor->Tele2}}</td>
      <td>{{$contractor->Job}}</td>
      <td>{{$contractor->Intership_No}}</td>
      <td>{{$contractor->Phone_Type}}</td>
      <td>{{$contractor->Nickname}}</td>
      <td>{{$contractor->Religion}}</td>
      <td>{{$contractor->Home_Phone}}</td>
      <td>{{$contractor->Fame}}</td>
      <td>{{$contractor->Status}}</td>

<!-- Promoter -->
  <td>
     <a href="/offlineneew/public/Contractors/{{$contractor->id}}/edit"class="btn btn-default btn-sm" ><span class="glyphicon glyphicon-edit"></span>
       </a>
</td>

  <td>
     <a href="/offlineneew/public/Contractors/destroy/{{$contractor->id}}"class="btn btn-default btn-sm" ><span class="glyphicon glyphicon-trash"></span>
       </a>
</td>


   </tr>
     @endforeach
     
</tbody>
 {!! $Contractors->links() !!}
</table>


                  </div>
               



{!!Form::open(['action'=>'ContractorController@importContractors','method' => 'post','files'=>true])!!}

<label for="file-upload" class="custom-file-upload">
    <i class="fa fa-cloud-upload"></i> Select File
</label>
<input id="file-upload"  name="file" type="file"/>    <input type="submit" name="submit" value="Import File" class="btn btn-primary" />  
{!!Form::close()!!}


                </div>
              </div>
 
  @section('other_scripts')


<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>



  <script type="text/javascript" >

var editor;

  $(document).ready(function( ){

    //  $('#Brands').DataTable( {
    //     "pagingType": "full_numbers"
    // } );

    

     $('#startdate').datepicker({
     language: 'ar',
     format: "yyyy-mm-dd"
});
    $('.District').chosen({
                width: '100%',
                no_results_text:'No Results'
            });
                $(".District").trigger("chosen:updated");
            $(".District").trigger("change");

        $('#Has_Facebook').change(function(){
            if($(this).val() == "No")
          
              $("#Facebook_Account").attr("disabled","disabled");
                  else
              $("#Facebook_Account").removeAttr('disabled');
    });

 $('#profileForm')
        .find('[name="Government"]')
            .chosen({
                width: '100%',
                inherit_select_classes: true
            })
            // Revalidate the color when it is changed
            .change(function(e) {

                $('#profileForm').formValidation('revalidateField', 'Government');

  $.getJSON("/offlineneew/public/government/district/" + $("#Government").val(), function(data) {
 
                var $contractors = $(".District");
                   $(".District").chosen("destroy");
                $contractors.empty();
                $.each(data, function(index, value) {
                    $contractors.append('<option value="' + index +'">' + value + '</option>');
                });
                  
        
             $('.District').chosen({
                width: '100%',
                no_results_text:'No Results'
            });
                $(".District").trigger("chosen:updated");
            $(".District").trigger("change");
            });
            }).end()


     
        .formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                'Name': {
                         validators: {
              
                  notEmpty:{
                    message:"the contractor name is required"
                  },
                    regexp: {
                        regexp: /^[\u0600-\u06FF\s]+$/,
                        message: 'Name must be arabic'
                    }
                    }
               
                  },
                   'EngName': {
                  
                       validators: {
                    regexp: {
                        regexp: /^[a-zA-Z\s]+$/,
                        message: 'The English name can only consist of alphabetical and spaces'
                    }
                    }

                    }  
                    ,'Birthday':
                   {
                      validators: {
                     date: {
                        message: 'The date is not valid',
                        format: 'YYYY-MM-DD',
                        // min and max options can be strings or Date objects
                        min: '1950-01-01',
                        max: '2001-12-30'
                    }
                    }
                }
                ,'Government':{
                       validators: {
                  notEmpty:{
                    message:"the Governmentis required"
                  }
                    }

                    },
                    'district':{
                       validators: {
                  notEmpty:{
                    message:"the District required"
                  }
                    }

                    }
                    ,"Address": {
                  
                       validators: {
                    regexp: {
                        regexp: /^[\\.\/a-zA-Z\u0600-\u06FF0-9\s]+$/,
                        message: 'The Address can only consist of alphabetical and spaces'
                    }
                    }

                    },'Education': {
                  
                       validators: {
                    regexp: {
                        regexp: /^[a-zA-Z\u0600-\u06FF\s]+$/,
                        message: 'The Education can only consist of alphabetical and spaces'
                    }
                    }

                    }
                    ,'Email':{
                  
                       validators: {
                                        remote: {
                        url: '/offlineneew/public/checkContractorEmail',
                        message: 'The email is not available',
                        type: 'GET'
                    },
                  emailAddress:{
                    message:"the Email Not Valid"
                  }
                    }

                    }
                    ,'Tele1':{
                  
                       validators: {
                                              remote: {
                        url: '/offlineneew/public/checkContractorTel1',
                        message: 'The  Telephone already Exsits',
                        type: 'GET'
                    },
                      regexp: {
                        regexp: /^\d{11}$/,
                        message: 'The Telephone can only consist of numbers and length 11'
                    },
                      notEmpty:{
                        message:"the Telephone is required"
                      }
                    }

                    }
                    ,'Tele2':{
                  
                       validators: {
                           stringLength: {
        min:7,
                        max: 11,
                        message: 'The Telephone must be between 6 and 11 digits'
                    },
                                              remote: {
                        url: '/offlineneew/public/checkContractorTel2',
                        message: 'The  Telephone is not available',
                        type: 'GET'
                    },
                                   }

                    }
                    ,'Job':{
                  
                       validators: {
                 regexp: {
                        regexp: /^[a-zA-Z\u0600-\u06FF\s]+$/,
                        message: 'The Job can only consist of alphabetical and spaces'
                    }
                    }

                    },'Nickname':{
                  
                       validators: {
                 regexp: {
                        regexp: /^[a-zA-Z\u0600-\u06FF\s]+$/,
                        message: 'The Nickname can only consist of alphabetical and spaces'
                    }
                    }

                    },'Religion':{
                  
                       validators: {
                 regexp: {
                        regexp: /^[a-zA-Z\u0600-\u06FF\s]+$/,
                        message: 'The Education can only consist of alphabetical and spaces'
                    }
                    }

                    },'Fame':{
                  
                       validators: {
                 regexp: {
                        regexp: /^[a-zA-Z\u0600-\u06FF\s]+$/,
                        message: 'The Fame can only consist of alphabetical and spaces'
                    }
                    }

                    },'Home_Phone':{
                  
                       validators: {
                 numeric: {
                        message: 'The Home Phone can only consist of numbers'
                    }
                    }

                    }

            } ,submitHandler: function(form) {
        form.submit();
    }
        })
        .on('success.form.fv', function(e) {
   // e.preventDefault();
    var $form = $(e.target);

    // Enable the submit button
   
    $form.formValidation('disableSubmitButtons', false);
});
     var table= $('.Brands').DataTable({
       
    select:true,
    responsive: true,
    "order":[[0,"asc"]],
    'searchable':true,
    "scrollCollapse":true,
    "paging":true,
    "pagingType": "simple",
      dom: 'lBfrtip',
        buttons: [
           
            
            { extend: 'excel', className: 'btn btn-primary dtb' }
            
            
        ],

fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
$.fn.editable.defaults.send = "always";

$.fn.editable.defaults.mode = 'inline';

$('.testEdit').editable({

      validate: function(value) {
        name=$(this).editable().data('name');
        if(name=="Name")
        {
                if($.trim(value) == '') {
                    return 'Value is required.';
                  }
                    }
      
        },

    placement: 'right',
    url:'{{URL::to("/")}}/updateContractors',
  
     ajaxOptions: {
     type: 'get',
    sourceCache: 'false',
     dataType: 'json'

   },

       params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');

            // console.log(params);
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'This Data Already Exist,Enter Correct Data.';
            } else {
                return response.responseText;
            }
        }


});

}
});
   $('.Brands tfoot th').each(function () {
      var title = $('.Brands thead th').eq($(this).index()).text();       
     if($(this).index()>=1 && $(this).index()<=21)
            {

           $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
}
        

    });
  table.columns().every( function () {
  var that = this;
 $(this.footer()).find('input').on('keyup change', function () {
          that.search(this.value).draw();
            if (that.search(this.value) ) {
               that.search(this.value).draw();
           }
        });
      
    });

 });



  </script>
@stop

@stop
