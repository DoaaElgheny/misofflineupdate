@extends('masternew')


@section('content')
 <section class="panel-body">
 <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
   <h2 style="margin-left:85%">صورة الايصال <i class="fa fa-tasks" ></i></h2>                     
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                     
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">


<style type="text/css">
  input[type=file] {
    display: block;}
    .card {
    /* Add shadows to create the "card" effect */
    box-shadow: -3px 3px 20px 14px rgba(117, 114, 114);
    transition: 0.3s;
}

/* On mouse-over, add a deeper shadow */
.card:hover {
    box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
}
</style>
<meta name="csrf-token" content="{{ csrf_token() }}" />
<section class="panel panel-primary">
<div class="panel-body" style="height: 850px;">
<a href="/MIS/BillOut"  ><span class="glyphicon glyphicon-circle-arrow-left" title="رجوع" style="font-size: 2em;
"></span></a>
                
 {!! Form::open(['action' => "BillOutController@UpdateImageOut",'method'=>'post','id'=>'profileForm','files'=>'true']) !!}   
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
       <input type="hidden" id="id" name="id" value="{{ $Purchase['id'] }}">
               <div class="form-group">
                        <label for="input_frontimg" class="control-label col-md-3">صوره االايصال</label>
               <div class="col-md-12" style="height: 450px;">

                             {!! Form::file('RelatedDecomentLink', ['class' => 'form-control','id'=>'uploadFrontFile','name'=>'RelatedDecomentLink','value'=>'$Purchase["RelatedDecomentLink"]']) !!}
                          <div style="padding-top: 25px "></div>                     
          <div style="padding:10px 10px 10px 10px;width: 500px;border:1px; " class="card" >
             <img src="{{$Purchase['RelatedDecomentLink']}}"  style="width: 100%;height: 500px;" />
           
          </div>          
           

                    </div>
                    </div>
                    </div>
                  <div style="padding-top: 25px ;padding-bottom:50px">
              <button type="submit" class="btn btn-primary "style="float:right" name="login_form_submit" value="Save">Save</button>
        </div>
  

      {!!Form::close() !!}

</div></div></div>
</section>
@section('other_scripts')
@stop
@stop