@extends('masternew')
@section('content')
 <section class="panel-body">


 <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                     <h2>تفاصيل ايصال الصرف</h2>
                        <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                     
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

  <h1><i class="fa fa-tasks"></i> تفاصيل ايصال الصرف</h1> 
<a type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal1" style="margin-bottom: 20px;">اضافة صنف جديد </a>

<div class="flash-message">
   
      @if(Session::has('message'))

      <p class="alert alert-warning">{{ Session::get('message') }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
      @endif

  </div>

<!-- model -->
 
<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
     <div class="modal-content">
    
          <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                 <h4 class="modal-title span7 text-center" id="myModalLabel"><span class="title">اضافة صنف جديد</span></h4>
          </div>
    <div class="modal-body">
        <div class="tabbable"> <!-- Only required for left/right tabs -->
        <ul class="nav nav-tabs">
  

        
        </ul>
        <div class="tab-content">
        <div class="tab-pane active" id="tab1">
              <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                 
                  <div class="x_content">
                    <br />
                     {!! Form::open(['action' => "BillOutController@storeDetails",'method'=>'post','id'=>'profileForm', 'files' => 'true']) !!} 
             <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden"  name="id" value="{{$id}}"/>
             <table class="table table-bordered" id="dynamic_field">  
                  <tr>  
                    <td>
                        <div class="form-group">
                             <div class="col-xs-2" dir="rtl">
                                <button type="button" class="btn btn-default addButton"><i class="fa fa-plus"></i></button>
                            </div>
                            
                            <div class="col-xs-4" dir="rtl">
                                <input type="text" name="descraption[]" placeholder="ادخل الكمية" class="form-control descraption_list" required/>
                            </div>
                           <div class="col-xs-4" dir="rtl">
                             {!!Form::select('item[]', $item ,null,['class'=>'form-control subuser_list','id' => 'prettify'])!!}
                            </div>
                            <label class="col-xs-2 control-label" >الاصناف </label>
                        </div>

                        <!-- The template containing an email field and a Remove button -->
                        <div class="form-group hide" id="emailTemplate">
                          <div class="col-xs-2" dir="rtl">
                                <button type="button" class="btn btn-default removeButton"><i class="fa fa-minus"></i></button>
                            </div>

                            
                            <div class="col-xs-4" dir="rtl">
                             <input type="text" name="descraption[]" placeholder="ادخل الكمية" class="form-control descraption_list" required/>
                            </div>
                            <div class="col-xs-4" dir="rtl">
                                  {!!Form::select('item[]', $item ,null,['class'=>'form-control subuser_list','id' => 'prettify'])!!}
                            </div>
                             <label class="col-xs-2 control-label"></label>
                        </div>

                        <!-- Message container -->
                        <div class="form-group">
                            <div class="col-xs-9 col-xs-offset-3">
                                <div id="messageContainer"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-xs-5 col-xs-offset-3">
                                <button type="submit" name="submitButton" class="btn btn-info" >ارسال </button>  
                            </div>
                        </div>
                    </td> 
                  </tr>  
             </table> 

      {!!Form::close() !!}                
        </div>
                </div>
                   </div>
                </div>
        </div>
        </div>
        </div>

    </div>
 

         </div>
     </div>
  </div>
<!-- ENDModal -->



                   
                    <table class="table table-hover table-bordered  dt-responsive nowrap testEdit" cellspacing="0" width="100%" dir="rtl">
  <thead>
 
        <th>العدد</th>
          <th>اسم الصنف </th>
          <th>الكمية </th>
          <th>حذف </th>
</tfoot>


<tbody style="text-align:center;">
<?php $i=1;?>
   @foreach($purchaseDetails as $purchaseDetails)
          <tr>
            <td>{{$i++}}</td>   
            <td><a   data-type="text" data-name="Name" data-value="{{$purchaseDetails->Name}}" data-pk="{{ $purchaseDetails->item_id }}">{{$purchaseDetails->Name}}</a></td>
           
            <td><a  class="testEdit" data-type="number" data-name="quantity"
             data-value="{{$purchaseDetails->quantity}}"
             data-pk="{{$purchaseDetails->item_id}}">{{$purchaseDetails->quantity}}</a></td>

           <td><a href="/MIS/BillOutDelete/destroy/{{$purchaseDetails->purches_id}}/{{$purchaseDetails->item_id}}"  class="btn btn-default btn-sm" ><span class="glyphicon glyphicon-trash" ></span>  </a></td>

            <input type="hidden" id="hidden" value="{{$purchaseDetails->purches_id}}"/>
          
            
          </tr>
          
        @endforeach
</tbody>
</table>


                  </div>
                </div>
              </div>
 
  @section('other_scripts')


<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>



 <script type="text/javascript">

var editor;
  $(document).ready(function(){
  $('#profileForm')
        .formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
              
           //Begin Doaa Elgheny
                   'descraption[]':{
                validators: {  

                         notEmpty: {
                            message: 'The quantity is Not Valid'
                    }
                }

            },    
                'item[]': {
                  
                    validators: {
                        callback: {
                            callback: function(value, validator, $field) {
                                var $emails          = validator.getFieldElements('item[]'),
                                    numEmails        = $emails.length,
                                    notEmptyCount    = 0,
                                    obj              = {},
                                    duplicateRemoved = [];

                                for (var i = 0; i < numEmails; i++) {
                                    var v = $emails.eq(i).val();
                                    if (v !== '') {
                                        obj[v] = 0;
                                        notEmptyCount++;
                                    }
                                }

                                for (i in obj) {
                                    duplicateRemoved.push(obj[i]);
                                }

                                if (duplicateRemoved.length === 0) {
                                    return {
                                        valid: false,
                                        message: 'You must fill at least one item'
                                    };
                                } else if (duplicateRemoved.length !== notEmptyCount) {
                                    return {
                                        valid: false,
                                        message: 'The item must be unique'
                                    };
                                }

                                validator.updateStatus('item[]', validator.STATUS_VALID, 'callback');
                                return true;
                            }
                        }
                    }
                }

            //End 
            } ,submitHandler: function(form) {


        form.submit();

    }
        })

        .on('success.form.fv', function(e) {
   // e.preventDefault();
   //Doaa Elgheny


    var $form = $(e.target);

    // Enable the submit button
   
    $form.formValidation('disableSubmitButtons', false);
})  .on('click', '.addButton', function() {
            var $template = $('#emailTemplate'),
                $clone    = $template
                                .clone()
                                .removeClass('hide')
                                .removeAttr('id')
                                .insertBefore($template);

            // Add new fields
            // Note that we DO NOT need to pass the set of validators
            // because the new field has the same name with the original one
            // which its validators are already set
            $('#profileForm')
                .formValidation('addField', $clone.find('[name="item[]"]'))
                .formValidation('addField', $clone.find('[name="descraption[]"]'))
        })

        // Remove button click handler
        .on('click', '.removeButton', function() {
            var $row = $(this).closest('.form-group');

            // Remove fields
            $('#profileForm')
                .formValidation('removeField', $row.find('[name="item[]"]'))
                .formValidation('removeField', $row.find('[name="descraption[]"]'));

            // Remove element containing the fields
            $row.remove();
        });

     var table= $('.tasks').DataTable({
  
    select:true,
    responsive: true,
    "order":[[0,"asc"]],
    'searchable':true,
    "scrollCollapse":true,
    "paging":true,
    "pagingType": "simple",
      dom: 'lBfrtip',
        buttons: [
           
            
            { extend: 'excel', className: 'btn btn-primary dtb' }
            
            
        ],

fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
$.fn.editable.defaults.send = "always";

$.fn.editable.defaults.mode = 'inline';

$('.testEdit').editable({

      validate: function(value) {
        name=$(this).editable().data('name');
        if(name=="Name"||name=="quantity")
        {
                if($.trim(value) == '') {
                    return 'Value is required.';
                  }
        }
      
        },

    placement: 'right',
    url:'{{URL::to("/")}}/updatePurchesDetails',
  
     ajaxOptions: {
     type: 'get',
    sourceCache: 'false',
     dataType: 'json'

   },

       params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');
            params.Purches= $('#hidden').val();


            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'This Data Already Exist,Enter Correct Data.';
            } else {
                return response.responseText;
            }
        }
           ,
        success:function(response)
        {
            if(response.status==="You do not have permission.")
             {
              return 'ليس لديك صلاحيه لهذه العملية';
              }
              if(response.status==="total quantity not Valid.")
             {
              return 'هذه الكمية غير متاحه';
              }
        }


});

}
});

 



   $('.tasks tfoot th').each(function () {



      var title = $('.tasks thead th').eq($(this).index()).text();
               
 if($(this).index()>=1 && $(this).index()<=2)
        {

           $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
}
        

    });
  table.columns().every( function () {
  var that = this;
 $(this.footer()).find('input').on('keyup change', function () {
          that.search(this.value).draw();
            if (that.search(this.value) ) {
               that.search(this.value).draw();
           }
        });
      
    });


 });

  </script>
@stop
@stop
