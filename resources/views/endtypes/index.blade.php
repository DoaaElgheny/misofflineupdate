@extends('master')
@section('content')
<!DOCTYPE html>
<meta name="csrf-token" content="{{ csrf_token() }}" />
<section class="panel panel-primary">
  <div class="panel-body">
    <h1><i class='fa fa-tasks'></i>Final Type</h1>
    <a type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal1" style="margin-bottom: 20px;"> Add New Final Type</a>
    <!-- model -->
    <div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <h4 class="modal-title span7 text-center" id="myModalLabel"><span class="title">Add New Final Type</span></h4>
          </div>
          <div class="modal-body">
            {!! Form::open(['action' => "EndTypeController@store",'method'=>'post','id'=>'profileForm']) !!}  
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <div class="form-group col-md-12"> 
                <label  class="control-label col-md-3">Sub Type Name:</label> 
                <div class="col-md-9"> 
                  {!!Form::select('subType_id',([null => 'please chooose'] + $SubTypese->toArray()) ,null,['class'=>'form-control subuser_list','id' => 'prettify','required'=>'required'])!!}
                </div>
              </div>
              <div class="form-group col-md-12">
                <label for="input_cement_name" class="control-label col-md-3">segmentation3 Name</label>
                <div class="col-md-9">
                  <input type="text" class="form-control" name="name" placeholder="Enter Your name"/>
                </div>
              </div>
              <div class="form-group col-md-12">
                <label for="input_cement_name" class="control-label col-md-3">segmentation3 English Name</label>
                <div class="col-md-9">
                  <input type="text" class="form-control" name="engname" placeholder="Enter Your english name"/>
                </div>
              </div>
              <hr/>
              <button type="submit" class="btn btn-primary "style="float:right" name="login_form_submit" value="Save">Save</button>
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            {!!Form::close()!!}
          </div>
        </div>
      </div>
    </div>
    <!-- ENDModal -->
    <table class="table table-hover table-bordered  dt-responsive nowrap display tasks" cellspacing="0" width="100%">
      <thead>
        <th>No</th>
        <th>segmentation3 Name</th>
        <th>segmentation3 English Name</th>
        <th>segmentation2 Name</th>  
        <th>Process</th>          
      </thead>
      <tfoot>
        <th></th>
         <th>segmentation3 Name</th>
        <th>segmentation3 English Name</th>
        <th>segmentation2 Name</th>   
        <th></th>        
      </tfoot>               
      <tbody style="text-align:center;">
        <?php $i=1;?>
        @foreach($EndTypes as $EndType)
          <tr>
            <td>{{$i++}}</td>   
            <td><a  class="testEdit" data-type="text" data-name="Name"  data-pk="{{ $EndType->id }}">{{$EndType->Name}}</a></td> 
            <td><a  class="testEdit" data-type="text" data-name="EngName"  data-pk="{{ $EndType->id }}">{{$EndType->EngName}}</a></td>
            <td>
              <a class="testEdit" data-type="select"  data-value="{{$SubTypese}}" data-source ="{{$SubTypese}}" data-name="SubType_Id"  data-pk="{{$EndType->id}}"> 
                {{$EndType->HasSubType->EngName}}
              </a>
            </td> 
            <td><a href="/cemexmarketingsystem/public/endtypes/destroy/{{$EndType->id}}"class="btn btn-default btn-sm" ><span class="glyphicon glyphicon-trash"></span></a></td>
          </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</section>  
<script type="text/javascript">
  $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });
</script>
<script type="text/javascript">
  var editor;
  $(document).ready(function()
    {   
      $('#profileForm')
        .formValidation
        ({
            framework: 'bootstrap',
            icon: 
            {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                      'name': 
                      {
                        validators: 
                        {
                          remote: 
                          {
                            url: '/cemexmarketingsystem/public/checkendtypeengname',
                            message: 'The Final Type name is Already Exist',
                            type: 'GET'
                          },
                          notEmpty:
                          {
                            message:"the Final Type name is required"
                          },
                          regexp:
                          {
                            regexp: /^[\u0600-\u06FF\s]+$/,
                            message: 'The Final Type name can only consist of alphabetical and spaces'
                          }
                        }
                      },
                      'engname': 
                      {
                        validators: 
                        {
                          remote: 
                          {
                            url: '/cemexmarketingsystem/public/checkendtypename',
                            message: 'The Final Type name is Already Exist',
                            type: 'GET'
                          },
                          notEmpty:
                          {
                            message:"the Final Type English name is required"
                          },
                          regexp:
                          {
                            regexp: /^[a-zA-Z\s]+$/,
                            message: 'The Final Type English name can only consist of alphabetical and spaces'
                          }
                        }
                      }
                    } 
            ,submitHandler: function(form)
              {
                form.submit();
              }
        })
        .on('success.form.fv', function(e)
        {
          // e.preventDefault();
          var $form = $(e.target);
          // Enable the submit button  
          $form.formValidation('disableSubmitButtons', false);
        });

        var table= $('.tasks').DataTable
        ({
          select:true,
          responsive: true,
          "order":[[0,"asc"]],
          'searchable':true,
          "scrollCollapse":true,
          "paging":true,
          "pagingType": "simple",
          dom: 'lBfrtip',
          buttons: [  
            { extend: 'excel', className: 'btn btn-primary dtb' }      
          ],
          fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull)
          {
            $.fn.editable.defaults.send = "always";
            $.fn.editable.defaults.mode = 'inline';
            $('.testEdit').editable
            ({
              validate: function(value)
              {
                name=$(this).editable().data('name');
                if(name=="Name" || name=="EngName" || name=="SubType_Id" )
                {
                  if($.trim(value) == '') 
                  {
                    return 'Value is required.';
                  }
                }
                if(name=="Name")
                {
                  var regexp =/^[\u0600-\u06FF\s]+$/;    
                  if (!regexp.test(value))
                  {
                    return 'This field is not valid';
                  }               
                }
                if(name=="EngName")
                {
                  var regexp =/^[a-zA-Z\s]+$/;    
                  if (!regexp.test(value))
                  {
                    return 'This field is not valid';
                  }               
                }
              },
              placement: 'right',
              url:'{{URL::to("/")}}/updateEndType',      
              ajaxOptions:
              {
                type: 'get',
                sourceCache: 'false',
                dataType: 'json',
              },
              params: function(params)
              {
                // add additional params from data-attributes of trigger element
                params.name = $(this).editable().data('name');
                // console.log(params);
                return params;
              },
              error: function(response, newValue)
              {
                if(response.status === 500) 
                {
                  return 'This Data Already Exist,Enter Correct Data.';
                }
                else 
                {
                  return response.responseText;
                }
              },
              success:function(response)
              {
                if(response.status==="You do not have permission.")
                {
                  return 'You do not have permission.';
                }
              }
            });
          }
        });
        $('.tasks tfoot th').each(function ()
          {
            var title = $('.tasks thead th').eq($(this).index()).text();             
            if($(this).index()>=1 && $(this).index()<=6)
            {
              $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
            }
          });
        table.columns().every( function ()
        {
          var that = this;
          $(this.footer()).find('input').on('keyup change', function ()
          {
            that.search(this.value).draw();
            if (that.search(this.value) )
            {
              that.search(this.value).draw();
            }
          }); 
        });
    });
</script>
@stop
