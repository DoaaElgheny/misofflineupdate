@extends('masternew')
@section('content')
 <section class="panel-body">

 <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title" >
                     <h2 style="margin-left:85%">الاصناف <i class="fa fa-tasks" ></i></h2>
                        <ul class="nav navbar-right panel_toolbox">
                      <li ><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
<a type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal1" style="margin-bottom: 20px; margin-left:85%"> اضافة صنف جديد </a>
<!-- model -->
<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
     <div class="modal-content">
    
          <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                 <h4 class="modal-title span7 text-center headerInventory" id="myModalLabel"><span class="title">اضافة صنف جديدة</span></h4>
          </div>
    <div class="modal-body">
        <div class="tabbable"> <!-- Only required for left/right tabs -->
        <ul class="nav nav-tabs">
  

        
        </ul>
        <div class="tab-content">
        <div class="tab-pane active" id="tab1">
              <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                 
                  <div class="x_content">
                    <br />
                  {!! Form::open(['route'=>'Items.store','method'=>'post','id'=>'profileForm','files'=>'true']) !!}
             
             <fieldset style="    padding: .35em .625em .75em; margin: 0 2px; border: 1px solid silver;" dir="rtl">
  <legend style="width: fit-content;border: 0px;" dir="rtl">بيانات اساسية</legend>
 

 
                    <div class="form-group">
                     <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                          <input type="text" class="form-control has-feedback-left inventorytexbox" id="EngName" name="EngName" placeholder="الاسم الانجليزي" required>
                                       

                        </div>

                      <div class="col-md-6 col-sm-6 col-xs-12 has-feedback">
                        <input type="text" class="form-control has-feedback-left inventorytexbox"  id="Name" name="Name" placeholder="اسم الصنف" required>


                        
                      </div>
                     

                       
                      </div>

     <div class="form-group">


      <div class="col-md-6 col-sm-6 col-xs-12 has-feedback" style="padding-top:10px ">
                         {!! Form::file('pic', ['class' => 'form-control','name'=>'pic']) !!}


                        
                      </div>
                     <div class="col-md-6 col-sm-6 col-xs-12 has-feedback" style="padding-top:10px ">
                          <input type="number"  class="form-control has-feedback-left inventorytexbox" id="MinLimit" name="MinLimit" placeholder="الحد الادني" required>
                                       

                        </div>

                     
                     

                       
                      </div>

 <div class="form-group">


      <div class="col-md-6 col-sm-6 col-xs-12 has-feedback" style="padding-top:10px ">
                       <button type="button" class="btn btn-default"   onclick="RandomBarcode()">باركود عشوائي </button>
                       
                      </div>
                     <div class="col-md-6 col-sm-6 col-xs-12 has-feedback" style="padding-top:10px ">
                          <input type="number" class="form-control has-feedback-left inventorytexbox" id="Code" name="Code" placeholder="الكود" required>
                        </div>
                      </div>
                  <div class="form-group">
                 <div class="control-label col-md-6 col-sm-6 col-xs-12" style="padding-top: 10px">
                        <div class="col-md-9 col-sm-9 col-xs-12 inventorytexbox">
 {!!Form::select('category', ([null => 'اختر القسم'] + $category->toArray()) ,null,['class'=>'form-control','id' => 'prettify','name'=>'category','required'=>'required'])!!}
                        </div>
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">اسم القسم</label>
                     </div>
                <div class="control-label col-md-6 col-sm-6 col-xs-12" style="padding-top: 10px">

                       
                        <div class="col-md-9 col-sm-9 col-xs-12 inventorytexbox">
                      {!!Form::select('brand', ([null => 'اختر الماركة'] + $brand->toArray()) ,null,['class'=>'form-control','id' => 'prettify','name'=>'brand','required'=>'required'])!!}
                        </div>
                         <label class="control-label col-md-3 col-sm-3 col-xs-12">اسم الماركة
                        </label>
                        </div>

                      </div>
  

                   
                    
                      
</fieldset>
        
           


                       


                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12 " style="text-align: center;">
                          <button type="button" class="btn btn-danger " data-dismiss="modal">الغاء</button>
                         <button class="btn btn-info" type="reset">اعادة ادخال</button>
     <button type="submit"  name="login_form_submit" class="btn btn-success" value="save">حفظ</button>
                        </div>
                      </div>

      {!!Form::close() !!}                
        </div>
                </div>
                   </div>
                </div>
        </div>
        </div>
        </div>

    </div>
 

         </div>
     </div>
  </div>
<!-- ENDModal -->



                   
                    <table class="table table-hover table-bordered  dt-responsive nowrap tasks" cellspacing="0" width="100%" dir="rtl">
  <thead>
       <th></th>
               
              <th>الاسم </th>
              <th>الاسم بالانجليزي</th>
              
            <!--   <th>الكمية</th> -->
              <th>اسم القسم</th>
              <th>اسم الماركة</th>
          
              <th>الكود</th>
              <th>صورة  الصنف</th>
                <th>الحد الادني</th>
              <th>حذف</th>  
            </thead>
            <tfoot>
               <th></th>
               
              <th>الاسم </th>
              <th>الاسم بالانجليزي</th>
              
            <!--   <th>الكمية</th> -->
              <th>اسم القسم</th>
              <th>اسم الماركة</th>
           
              <th>الكود</th>
              <th>صورة  الصنف</th>
               <th>الحد الادني</th>
              <th></th>  
</tfoot>


<tbody style="text-align:center;">
<?php $i=1;?>
 @foreach($items as $item)
    <tr>
     <td>{{$i++}}</td>
         <td ><a  class="testEdit" data-type="text"  data-value="{{$item->Name}}" data-name="Name"  data-pk="{{ $item->id }}">{{$item->Name}}</a></td>
         
          <td ><a  class="testEdit" data-type="text"  data-value="{{$item->EngName}}" data-name="EngName"  data-pk="{{ $item->id }}">{{$item->EngName}}</a></td>

         <!-- <td ><a  class="testEdit" data-type="number"  data-value="{{$item->price}}" data-name="price"  data-pk="{{ $item->id }}">{{$item->price}}</a></td> -->
         
<!-- <td >{{$item->totalmstock}}</td> -->

 <td > <a class="testEdit" data-type="select"  data-value="{{$item->getitemCategory->Name}}" data-source ="{{$category}}" data-name="category_id"  data-pk="{{$item->id}}"> 
  {{$item->getitemCategory->Name}}
  </a>
 </td>
 <td > <a class="testEdit" data-type="select"  data-value="{{$item->getitemBrand->Name}}" data-source ="{{$brand}}" data-name="brand_id"  data-pk="{{$item->id}}"> 
  {{$item->getitemBrand->Name}}
  </a>
 </td>





         <td >{{$item->Code}}</td> 
          <td>
          @if($item->ImageItem!==null)
          <img src="{{$item->ImageItem}}" style="width:60px;hight:60px;">
          @endif
        </td>
          <td ><a  class="testEdit" data-type="number"  data-value="{{$item->price}}" data-name="MinLimit"  data-pk="{{ $item->id }}">{{$item->MinLimit}}</a></td>
  <td>


     <a href="/MIS/Items/destroy/{{$item->id}}" class="btn btn-default btn-sm" ><span class="glyphicon glyphicon-trash"></span>
       </a>
        <a href="/MIS/EditPhotoItem/{{$item->id}}" class="btn btn-default btn-sm" ><span class="glyphicon glyphicon-Edit"></span>
       </a>

</td>
   </tr>
     @endforeach
</tbody>
</table>


                  </div>
                </div>
              </div>
 
  @section('other_scripts')


<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>



   <script type="text/javascript">
var editor;

  $(document).ready(function(){
   
       $('#profileForm')
        .formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },

            fields: {
               'Name': {
                    validators: {
                                   remote: {
                        url: '/MIS/checknameitem',
                      
                        message: 'هذا الصنف مدخل من قبل .',
                        type: 'GET'
                    },
                notEmpty:{
                    message:"يجب ادخال الاسم ."
                  },
                 
                        
                    }
                },

                     'EngName': {
                  
                    validators: {
                            remote: {
                        url: '/MIS/checkEngNameitem',
                      
                        message: 'هذا الصنف مدخل من قبل .',
                        type: 'GET'
                    }, 

             // notEmpty:{
             //        message:"يجب ادخال الاسم ."
             //      },
              
                        
                    }
                },
                   'MinLimit': {
                  
                    validators: {
                     

             notEmpty:{
                    message:"يجب ادخال الحد الادني ."
                  }
                    
                        
                    }
                },
                   'Code': {
                  
                    validators: {
                     

             notEmpty:{
                    message:"يجب ادخال الكود ."
                  }
                    
                        
                    }
                }

            
            },submitHandler: function(form) {
        form.submit();
    }
        }).on('success.form.fv', function(e) {
   // e.preventDefault();
    var $form = $(e.target);

    // Enable the submit button
   
    $form.formValidation('disableSubmitButtons', false);
});
     var table= $('.tasks').DataTable({
  
    select:true,
    responsive: true,
    "order":[[0,"asc"]],
    'searchable':true,
    "scrollCollapse":true,
    "paging":true,
    "pagingType": "simple",
      dom: 'lBfrtip',
        buttons: [
            { extend: 'excel', className: 'btn btn-primary dtb' }
        ],

fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
$.fn.editable.defaults.send = "always";

$.fn.editable.defaults.mode = 'inline';

$('.testEdit').editable({

      validate: function(value) {
        name=$(this).editable().data('name');
        if(name=="Name" || name=="totalmstock" || name=="MinLimit")
        {
        if($.trim(value) == '') {
                    return 'يجب ادخال القيمة .';
                  }
                    }
//         if(name=="Name")
//         {

//        var regexp = /^[\u0600-\u06FF\s]+$/;          
// // var regexp = /^[\u0600-\u06FF\s]+$/;  
//            if (!regexp.test(value)) 
//            {
//                 return 'هذا الادخال غير صحيح.';
//            }
            
//          } 
      
      
        },

    placement: 'right',
    url:'{{URL::to("/")}}/updateItems',
  
     ajaxOptions: {
     type: 'get',
    sourceCache: 'false',
     dataType: 'json'

   },

       params: function(params) {
            // add additional params from data-attributes of trigger element
            params.name = $(this).editable().data('name');

            // console.log(params);
            return params;
        },
        error: function(response, newValue) {
            if(response.status === 500) {
                return 'This Data Already Exist,Enter Correct Data.';
            } else {
                return response.responseText;
            }
        }
        ,    success:function(response)
        {
            if(response.status==="You do not have permission.")
             {
              return 'You do not have permission.';
              }
        }


});

}
});

   $('.tasks tfoot th').each(function () {
      var title = $('.item thead th').eq($(this).index()).text();
               
 if($(this).index()>=1 && $(this).index()<=7)
        {

           $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
        }
        

    });
  table.columns().every( function () {
  var that = this;
 $(this.footer()).find('input').on('keyup change', function () {
          that.search(this.value).draw();
            if (that.search(this.value) ) {
               that.search(this.value).draw();
           }
        });
      
    });

 }); 

  </script>


 <script>
function RandomBarcode(e)
{
  var s=Math.floor(Math.random() * 1000000000);
  $('#Code').val(s);
}
  </script>
@stop

@stop
