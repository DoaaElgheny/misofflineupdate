<?php

return [

    /**
     * The SMS service to use. twilio or plivo
     */
    'driver' => env('SMS_DRIVER','twilio'),

    /**
     * Plivo settings
     */
    'plivo' => [
        'token' => env('PLIVO_AUTH_TOKEN'),
        'user'  => env('PLIVO_AUTH_ID'),
        'from'  => env('PLIVO_FROM',null), //Default from phone number
    ], 

    /**
     * Twilio settings
     */
    'twilio' => [
         'token' => '4b531a4f6c27dc65fc18065de6edfa3e',
        'user'  => 'ACf1ca5c9327cdfbc4c75920705947e202',
        'from'  => env('TWILIO_FROM',null), //Default from phone number
    ], 
];
