<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserOption extends Model
{
     public $table = "useroption";
    public function Questions()
    {
        return $this->belongsTo('App\Question','Question_Id');
    }
     public function UsersServy()
    {
        return $this->belongsTo('App\UserSurvey','UserServy_Id');
    }
   
       
}
