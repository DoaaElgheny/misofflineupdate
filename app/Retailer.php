<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Retailer extends Model
{
    public $table="retailers";
    // protected $connection = 'mysql';

    public function HasContractors()
    {
         return $this->belongsToMany('App\Contractor','retailers_contractors','Retailer_id','contractor_id')
        ->withTimestamps();
    }
    public function HasActivityitem()
    {
        return $this->belongsToMany('App\ActivityItem','activity_contractors_retailers','Retailer_id','activityitem_id')
        ->withPivot('JoinDate')
        ->withTimestamps();
    }
    public function HasContractorsActivity()
    {
        return $this->belongsToMany('App\Contractor','activity_contractors_retailers','Retailer_id','contractor_id')
        ->withPivot('JoinDate')
        ->withTimestamps();
    }
    public function activitable()
    {
    return $this->morphToMany('App\ActivityItem', 'activitable')->withPivot('Item_Name','Quantity')
        ->withTimestamps();
    }
    public function HasType()
    {
        return $this->belongsTo('App\Type','Type_Id');
    }
    public function HasSubType()
    {
        return $this->belongsTo('App\SubType','SubType_Id');
    }
    public function HasEndType()
    {
        return $this->belongsTo('App\EndType','EndType_Id');
    }
    public function HasManyLoaction()
    {
        return $this->hasMany('App\Location','id');
    }
    public function HasManyLicense()
    {
        return $this->hasMany('App\License','id');
    }
    public function HasManyUser()
    {
        return $this->belongsToMany('App\User','retailers_users','Retailer_Id','User_Id')
                    ->withPivot('RDate')->withTrashed()
                    ->withTimestamps();
    }
     public function KidsRet()
    {
    return $this->hasMany('App\Kids','id');
    }
      public function ProductRet()
    {
    return $this->belongsToMany('App\Product','products_retailers','Retailer_Id','Product_Id')
                    ->withPivot('Quantity')
                    ->withTimestamps();
    }

}
