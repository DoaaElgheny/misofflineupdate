<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Government extends Model
{
	// protected $connection = 'mysql';
	public function governmentdistrict ()
    {
    	return $this->hasMany('App\District','id');
    }
    public function HasLicense()
    {
    	return $this->hasMany('App\License','id');
    }
}
