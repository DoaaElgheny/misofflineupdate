<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class salary_user extends Model
{
	public $table="salary_user";
	// protected $connection = 'mysql';
    public function salaryuser()
    {
        return $this->belongsTo('App\User','users_id')->withTrashed();
    }
}
