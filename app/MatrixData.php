<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MatrixData extends Model
{
    public $table = "matrixdata";
    public function question()
    {
        return $this->belongsTo('App\Question','Question_Id');
    }
}
