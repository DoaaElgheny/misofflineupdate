<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
 public $table="districts";
 // protected $connection = 'mysql';
    
    public function districtgovernment()
    {
        return $this->belongsTo('App\Government','government_id');
    }
     public function HasLicense()
    {
    	return $this->hasMany('App\License','id');
    }
}
