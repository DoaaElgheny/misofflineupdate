<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
	public $table="items";
	
   // protected $connection = 'mysql';
   public function getitemBrand()
    {
        return $this->belongsTo('App\Brand','brand_id');
    }
    
   public function getitemCategory()
    {
        return $this->belongsTo('App\Category','category_id');

    }

   public function getitempurches()
    {
        return $this->belongsToMany('App\Purchase','item_purches','item_id','purches_id')
        ->withPivot('quantity')
        ->withPivot('ActivityName')
       
        ->withTimestamps();
    }
public function getDatawerehouseitem()
    {
        return $this->belongsToMany('App\Warehouse','datawarehouse_items','item_id',
          'datawarehouse_id')
        ->withPivot('quantityinstock')
        ->withTimestamps();
    }
     public function getitemOrders()
    {
       
        return $this->belongsToMany('App\OrderItems','items_plans','Item_Id','OrderItem_Id')
        ->withPivot('Quantity')
        ->withPivot('ActivityName')
         ->withPivot('Cost')
        ->withTimestamps();
    }
}
