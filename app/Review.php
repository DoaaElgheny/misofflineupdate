<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    public $table="reviews";
	// protected $connection = 'mysql';
   
   public function ReviewContractor()
    {
        return $this->belongsTo('App\Contractor','Contractor_Id');
    }
      public function getcontractproduct()
    {
        return $this->belongsToMany('App\Product','products_reviews','review_id','product_id')
        ->withPivot('Value')
        ->withTimestamps();
    }
   
}
