<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contractor extends Model
{
	public $table="contractors";
  //protected $connection = 'mysql';
     public function HasRetailers()
    {
         return $this->belongsToMany('App\Retailer','retailers_contractors','Contractor_id','Retailer_id')
         ->withTimestamps();
    }
    public function HasRetailersActivity()
    {
        return $this->belongsToMany('App\Retailer','activity_contractors_retailers','contractor_id','Retailer_id')
        ->withPivot('activityitem_id')
        ->withPivot('JoinDate')
        ->withTimestamps();
    }
      public function HasActivityitem()
    {
        return $this->belongsToMany('App\ActivityItem','activity_contractors_retailers','contractor_id','activityitem_id')
        ->withPivot('Retailer_id')
        ->withPivot('JoinDate')
        ->withTimestamps();
    }
     public function activits()
    {
    return $this->morphToMany('App\ActivityItem', 'activitable')->withPivot('Item_Name','Quantity')
        ->withTimestamps();
    }
   
}
