<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gps extends Model
{
    // public $table ="gpss";



    public $table = "gps";
 // protected $connection = 'mysql2';
    protected $fillable = array(
        'gps_id','product_id'
    );
    public function getusers()
    {
        return $this->belongsTo('App\User','user_id')->withTrashed();
    }
     public function getcontractors()
    {
        return $this->belongsTo('App\Contractor','contractor_id');
    }

    public function gpshasorders()
    {
        return $this->hasMany('App\Order','id');
    }
    public function getproduct()
    {
        return $this->belongsToMany('App\Product','gps_product','gps_id','product_id')
        ->withPivot('Amount')
        ->withTimestamps();

    }
}