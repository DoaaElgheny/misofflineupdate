<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
   protected $table = 'events';
   // protected $connection = 'mysql2';
    protected $fillable = ['start','end','allDay','color','title'];
    protected $hidden = ['id'];
}
