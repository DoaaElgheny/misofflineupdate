<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserCustome extends Model
{
     public $table = "usercustomer";

      public function GetUser()
   {
       return $this->belongsToMany('App\Question','useroption','Question_Id','User_Id')
       ->withPivot('data')
       ->withPivot('Row')
       ->withPivot('answerdate')
       ->withTimestamps();       
   }

}
