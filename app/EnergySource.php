<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EnergySource extends Model
{
	public $table="energy_sources";
	// protected $connection = 'mysql2';
     public function getCompanyEnergySource()
    {
        return $this->belongsToMany('App\EnergySource','company_energysources','company_id','energysources_id')
        ->withPivot('Value')
        ->withTimestamps();
    }
}
