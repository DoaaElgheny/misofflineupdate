<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EndType extends Model
{
	public $table="end_typese";
    public function HasManyRetailer()
    {
        return $this->hasMany('App\Retailer','id');
    }
    public function HasSubType()
    {
    	return $this->belongsTo('App\SubType','SubType_Id');
    }
}
