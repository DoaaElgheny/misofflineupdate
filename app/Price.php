<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Price extends Model
{
    public $table="prices";
	
   // protected $connection = 'mysql2';
   public function pricesUser()
    {
return $this->belongsTo('App\user','User_id');
    }
     public function getpricesProduct()
    {
        return $this->belongsTo('App\Product','Product_id');
    }
}
