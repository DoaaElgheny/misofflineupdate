<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    public $table = "questions";
     public function getUsers()
    {
        return $this->belongsToMany('App\User','useroptions','Question_Id','User_Id')
        ->withPivot('UserAnswer')
        ->withPivot('Date')
        ->withTimestamps();
    }
    public function Questionoptions()
    {

        return $this->hasMany('App\QuestionOption');
    }
}
