<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Warehouse extends Model
{
        public $table = "datawarehouses";
// protected $connection = 'mysql';
        public function getwarehousepurschase()
    {
        return $this->hasMany('App\Purchase','id');
    }
//Doaa Elgheny
         public function getwarehouseItem()
    {

        return $this->belongsToMany('App\Item','datawarehouse_items','Warehouse_iD','item_id')
        ->withPivot('quantityinstock')
        ->withTimestamps();

}
  
}
