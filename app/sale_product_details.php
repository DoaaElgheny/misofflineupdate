<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class sale_product_details extends Model
{
	 public $table="sale_product__details";
    // protected $connection = 'mysql2';
	 
      public function sale_product__detailsbelongtosale_product()
    {
        return $this->belongsTo('App\sale_product','SaleProduct_id');
    }
}
