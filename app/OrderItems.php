<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderItems extends Model
{
   public $table="orderitems";
     public function getOrdersitem()
    {
        return $this->belongsToMany('App\Item','items_plans','OrderItem_Id','Item_Id')
        ->withPivot('Quantity')
        ->withPivot('ActivityName')
         ->withPivot('Cost')
        ->withTimestamps();

    }
}
