<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class License extends Model
{
	public $table="retailers_license";
    public function HasRetailer()
    {
    	return $this->belongsTo('App\Retailer','Retailer_Id');
    }
    public function HasGovernment()
    {
    	return $this->belongsTo('App\Government','Government_Id');
    }
    public function HasDistrict()
    {
    	return $this->belongsTo('App\District','District_Id');
    }
}
