<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubType extends Model
{
	public $table="sub_typese";
    public function HasManyRetailer()
    {
        return $this->hasMany('App\Retailer','id');
    }
    public function HasManyEndType()
    {
        return $this->hasMany('App\EndType','id');
    }
    public function HasType()
    {
    	return $this->belongsTo('App\Type','Type_Id');
    }
}
