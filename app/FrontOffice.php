<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FrontOffice extends Model
{
	  public $table = "front_offices";
      // protected $connection = 'mysql2';
    public function getproductsforntoffice()
    {
    	return $this->belongsToMany('App\Product','frontoffice_products','frontoffice_id','product_id');
    }

     public static function boot()
    {
        parent::boot();
    }
    protected $softDelete = true;
    
   protected $fillable = array(
        'selleroffice_id','product_id'
    );
 
    public function getcompany()
    {   
        return $this->belongsTo('App\Company','company_id');
    }
    public function getsellerofficeproduct()
    {
        return $this->belongsToMany('App\Product','product_selleroffice','selleroffice_id','product_id');
    }
}
