<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    public $table = "galleries";
    // protected $connection = 'mysql';

    public function getmarketingactivity()
    {
        return $this->belongsTo('App\MarketingActivity','marketingactivity_id');
    }
}
