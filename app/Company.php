<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    public $table="companies";
    //protected $connection = 'mysql2';
    public function CompaniesWerehouse ()
    {
    	return $this->hasMany('App\CompanyWarehouses','id');
    }
    public function companyget()
    {   
        return $this->hasMany('App\FrontOffice','id');
    }
      public function getEnergyCompany()
    {
        return $this->belongsToMany('App\EnergySource','company_energysources','company_id','energysources_id')
        ->withPivot('Value')
        ->withTimestamps();
    }
}
