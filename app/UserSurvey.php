<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserSurvey extends Model
{
    public $table = "userservies";
    public function UserOption()
    {
        return $this->hasMany('App\UserOption','id');
    }
    public function Users()
    {
        return $this->belongsTo('App\User','User_Id');
    }
    public function Surveies()
    {
        return $this->belongsTo('App\Survey','Survey_Id');
    }
}
