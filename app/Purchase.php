<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
public $table = "purches";
// protected $connection = 'mysql';
     public function getpurchesusers()
    {
        return $this->belongsTo('App\User','users_id');
    }
   public function getpurchasewarehouse()
    {
        return $this->belongsTo('App\Warehouse','Warehouse_iD')
     // return $this->belongsTo('App\datawarehouses','datawarehouse_id')
        ->withTimestamps();
    }
     public function getpurchaseitem()
    {
        return $this->belongsToMany('App\Item','item_purches','purches_id','item_id')
        ->withPivot('quantity')
        ->withTimestamps();
    }
   public function purchesactivity()
    {
        return $this->belongsTo('App\ActivityItem','ActivityitemsID');
    }
}
