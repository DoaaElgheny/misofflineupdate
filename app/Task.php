<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
	// protected $connection = 'mysql';
     public function getusertask()
    {
        return $this->belongsToMany('App\User','users_tasks','task_id','users_id')
        ->withPivot('Target')
        ->withPivot('Achieved')
        ->withPivot('Date')
        ->withPivot('Time')
        ->withTimestamps();
    }
}
