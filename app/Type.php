<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
	public $table="typese";
	public function HasManyRetailer()
    {
        return $this->hasMany('App\Retailer','id');
    }
    public function HasManySubType()
    {
        return $this->hasMany('App\SubType','id');
    }
}
