<?php

namespace App;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
  //  use SoftDeletes;
    
    public $table = "products";
    // protected $connection = 'mysql2';
    public function getcompanyproduct()
    {   
        return $this->belongsTo('App\Company','Company_id');
    }
    public function getprice()
    {
        return $this->hasMany('App\Price','id');
    }
        public function getforntofficeproducts()
    {
    	return $this->belongsToMany('App\FrontOffice','frontoffice_products','product_id','frontoffice_id');
      
    }
     public function getproductcontract()
    {

        return $this->belongsToMany('App\Review','products_reviews','product_id','review_id')
        ->withPivot('Value')
        ->withTimestamps();
    }
       public function HasProductRet()
    {
        
         return $this->belongsToMany('App\Retailer','products_retailers','Product_Id','Retailer_Id')
                    ->withPivot('Quantity')
                    ->withTimestamps();
    }
   
}
