<?php

namespace App;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
		use SoftDeletes;

  // protected $connection = 'mysql2';
 public static function boot()
    {
        parent::boot();
    }


    protected $softDelete = true;
    public $table="activities";

     public function getcompany()
    {   
        return $this->belongsTo('App\Company','company_id');
    }
      public function getUser()
    {   
        return $this->belongsTo('App\User','User_id')->withTrashed();
    }
}
