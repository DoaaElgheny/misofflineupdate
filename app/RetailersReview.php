<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RetailersReview extends Model
{
	public $table = "reviews_retailers";
   public function getRetailersData()
    {
        return $this->belongsTo('App\Retailer','Retailer_Id');
        
    }
}
