<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubActivity extends Model
{
    public $table="sub_activities";
    // protected $connection = 'mysql';
    public function subactivitiesactitvityitem()
    {
    	return $this->hasMany('App\ActivityItem','id');
    }
   
    public function subactivitiesMarktingactitvity()
    {
        return $this->belongsTo('App\MarketingActivity','marketingactivity_id');
    }
}
