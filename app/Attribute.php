<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attribute extends Model
{
    public $table="attributes";
    //protected $connection = 'mysql';
      public function getattributeactivityitems()
    {
        return $this->belongsToMany('App\ActivityItem','activityitem_attributes','attrbuti_id','activityitem_id')
        ->withPivot('Value')
        ->withTimestamps();
    }
}
