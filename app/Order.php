<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    public $table="orders";
    // protected $connection = 'mysql2';
    public function orderbelongtosaleproduct()
    {
        return $this->belongsTo('App\sale_product','SaleProduct_id');
    } 
     public function orderbelongtoproduct()
    {
        return $this->belongsTo('App\Product','Product_id')->withTrashed();
    } 
     public function orderbelongtogps()
    {
        return $this->belongsTo('App\Gps','gps_id');
    }    
}
