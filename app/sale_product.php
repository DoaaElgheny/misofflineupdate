<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class sale_product extends Model
{
    public $table="sale_products";
    // protected $connection = 'mysql2';
     public function sale_productshasDetials()
    {
    	return $this->hasMany('App\sale_product','id');
    }
      public function sale_productshasorders()
    {
    	return $this->hasMany('App\Order','id');
    }
}
