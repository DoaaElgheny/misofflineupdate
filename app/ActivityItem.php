<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActivityItem extends Model
{	public $table="activity_items";
   // protected $connection = 'mysql';

     public function actitvityitemsubactivities()
    {
        return $this->belongsTo('App\Subactitvity','subactivity_id');
    }
      public function getactivityitemsattribute()
    {
        return $this->belongsToMany('App\Attribute','activityitem_attributes','activityitem_id','attrbuti_id')
        ->withPivot('Value')
        ->withTimestamps();
    }
    public function contractors()
    {
        return $this->morphedByMany('App\Contractor', 'activitable')->withPivot('Item_Name','Quantity')
        ->withTimestamps();
    }
     public function retailers()
    {
        return $this->morphedByMany('App\Retailer', 'activitable')->withPivot('Item_Name','Quantity')
        ->withTimestamps();
    }

    public function HasContractors()
    {
        return $this->belongsToMany('App\Contractor','activity_contractors_retailers','activityitem_id','contractor_id')
        ->withPivot('Retailer_id')
        ->withPivot('JoinDate')
        ->withTimestamps();
    }
    public function HasRetailers()
    {
        return $this->belongsToMany('App\Retailer','activity_contractors_retailers','activityitem_id','Retailer_id')
        ->withPivot('contractor_id')
        ->withPivot('JoinDate')
        ->withTimestamps();
    }
}
