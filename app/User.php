<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth;
use DB;
class User extends Authenticatable
{
    // protected $connection = 'mysql';

use EntrustUserTrait { restore as private restoreA; }
// use SoftDeletes { restore as private restoreB; }

public function restore()
{
    $this->restoreA();
    $this->restoreB();
}
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $primaryKey='id';
     public static function boot()
    {
        parent::boot();
    }


    protected $softDelete = true;
protected $fillable = ['Name','Username', 'Email', 'password'];


    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token','Email'
    ];


      public function UserRoles()
    {
        return $this->belongsToMany('App\Role','role_user','user_id','role_id');
    }
     public function gps()
    {
        return $this->hasMany('App\Gps','id');
    }
    public function total()
{
  $totalOut=Expense::where('user_ID',Auth::user()->id)
                  ->select(DB::raw('sum(Cost)  as total'))
                 ->first();
$totalIN=ExpensesIN::where('user_ID',Auth::user()->id)
                  ->select(DB::raw('sum(Cost)  as total'))
                 ->first();
                 // if($totalOut->total !=null)
                 // {
                  $total=$totalIN->total -$totalOut->total;
                 return $total;
                 // }
                 // else
                 // {
                 //  return 0;
                 // }
                 
}
public function safetransaction()
{
$safeBalnce=safetransaction::select(DB::raw('sum(Balance)  as total'))
                 ->first();
$totalIN=ExpensesIN::select(DB::raw('sum(Cost)  as total'))
                 ->first();
                
            
                  $safetransaction=$safeBalnce->total - $totalIN->total;
                 return $safetransaction;
                
                 
}

public function gettask()
    {
        return $this->belongsToMany('App\Task','users_tasks','users_id','task_id')
        ->withPivot('Target')
        ->withPivot('Achieved')
        ->withPivot('Date')
        ->withPivot('Time')
        ->withTimestamps();
    }
 public function getUserKPI()
    {
        return $this->belongsToMany('App\Kpi','kpi_users','user_id',
          'kpi_id')
          ->withPivot('From_Date')
          ->withPivot('To_Date')
        ->withPivot('Performance')
        ->withTimestamps();
    }
       public function usersipervisor()
{
    return $this->hasMany('App\User', 'id','Supervisor');
}
 
   public function visor()
{
    return $this->belongsTo('App\User', 'Supervisor','id');
}


      public function getCafeSigns()
    {
        return $this->hasMany('App\CafeSigns','id');
    }
   public function Activity()
    {
        return $this->hasMany('App\Activity','id');
    }
     public function Price()
    {
        return $this->hasMany('App\Price','id');
    }

      
         
 
      public function getpurchesuser_user()
    {

        return $this->hasMany('App\Purchase','id');
    }

   public function getQuestion()
    {
        return $this->belongsToMany('App\Question','useroptions','User_Id','Question_Id')
        ->withPivot('UserAnswer')
        ->withPivot('Date')
        ->withTimestamps();
    }


















   
}
