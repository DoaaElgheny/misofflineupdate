<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kids extends Model
{
    //
        public $table="kids_retailers";
         public function getRetailersKids()
    {
        return $this->belongsTo('App\Retailer','Retailer_Id');
        
    }

}
