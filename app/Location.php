<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
	public $table="retailers_location";
    public function HasRetailer()
    {
    	return $this->belongsTo('App\Retailer','Retailer_Id');
    }
}
