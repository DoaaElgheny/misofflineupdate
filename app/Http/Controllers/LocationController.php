<?php

namespace App\Http\Controllers;

use Auth;
use App\Http\Requests;
use App\User;
use DB;
use App\Http\Controllers\Controller;
use Request;
use Excel;
use File;
use Illuminate\Support\Facades\Response;
use Validator;
use Illuminate\Support\Facades\Redirect;
use Input;
use Session;
use Geocode;
use Cache;
use URL;
use View;
use Khill\Lavacharts\Laravel\LavachartsFacade as Lava;
use Khill\Lavacharts\Lavacharts;
use TableView;
use App\Dategps;
use PHPExcel; 
use PHPExcel_IOFactory; 
use App\Government;
use App\District;
use App\Retailer;
use App\Type;
use App\EndType;
use App\SubType;
use App\Location;

class LocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
   {
       $this->middleware('auth');
   }
    public function index($id)
    {
        try
        {
            if (Auth::check())
            {
                // if(Auth::user()->can('show-Marketing_Activity'))
                // {
                        Session::put('LoRetailer_id',$id);
                        $Locations=Location::where('Retailer_Id',$id)->get();
                        return view('locations.index',compact('Locations'));
                // }
                // else 
                // {
                //     return view('errors.403');
                // }

            }//endAuth
            else
            {
                return redirect('/login');
            }

        }//endtry
        catch(Exception $e) 
        {
            return redirect('/login');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeLocation()
    {
        try
        {
            if (Auth::check())
            {
                // if(Auth::user()->can('show-Marketing_Activity'))
                // {
                        $LRetailer_id=Session::get('LoRetailer_id');      
                        $retailers_location=New Location;
                        $retailers_location->Location=input::get('Location');
                        $retailers_location->Retailer_Id=$LRetailer_id;
                        $retailers_location->Long=input::get('Lan');
                        $retailers_location->Lat=input::get('Lat');
                        $retailers_location->save();
                        return redirect('/locations/'.$LRetailer_id);
                // }
                // else 
                // {
                //     return view('errors.403');
                // }

            }//endAuth
            else
            {
                return redirect('/login');
            }

        }//endtry
        catch(Exception $e) 
        {
            return redirect('/login');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updatelocation()
    {
        try
        {
            if (Auth::check())
            {
                // if(Auth::user()->can('show-Marketing_Activity'))
                // {
                        $Locationid = Input::get('pk');  
                        $column_name = Input::get('name');
                        $column_value = Input::get('value');   
                        $Location = Location::whereId($Locationid)->first();
                        $Location-> $column_name=$column_value;
                        if($Location->save())
                            return \Response::json(array('status'=>1));
                        else 
                            return \Response::json(array('status'=>0));

                // }
                // else 
                // {
                //     return \Response::json(array('status'=>'You do not have permission.'));
                // }

            }//endAuth
            else
            {
                return redirect('/login');
            }

        }//endtry
        catch(Exception $e) 
        {
            return redirect('/login');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   
    public function destroy($id)
    {
        try
        {
            if (Auth::check())
            {
                // if(Auth::user()->can('show-Marketing_Activity'))
                // {
                        $Location=Location::find($id);
                        $Location->delete();
                        $LRetailer_id=Session::get('LoRetailer_id');      
                        return redirect('/locations/'.$LRetailer_id);
                // }
                // else 
                // {
                //     return view('errors.403');
                // }

            }//endAuth
            else
            {
                return redirect('/login');
            }

        }//endtry
        catch(Exception $e) 
        {
            return redirect('/login');
        }
    }
}
