<?php
namespace App\Http\Controllers;
use Auth;
use App\Http\Requests;
use App\User;
use App\Gps;
use App\Contractor;
use App\Task;
use DB;
use App\Http\Controllers\Controller;
use Request;
use Excel;
use File;
use Illuminate\Support\Facades\Response;
use Validator;
use Illuminate\Support\Facades\Redirect;
use Input;
use Session;
use Geocode;
use Cache;
use URL;
use App\Sign;
use View;
use Khill\Lavacharts\Laravel\LavachartsFacade as Lava;
use Khill\Lavacharts\Lavacharts;
use TableView;
use App\Dategps;
use PHPExcel; 
use PHPExcel_IOFactory; 
use App\MarketingActivity;
use App\SubActivity;
use App\Attribute;
use App\ActivityItem;
use App\District;
use App\Government;
use App\Event;
use App\Retailer;


class ActivityItemController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
  */
  public function __construct()
    {
      $this->middleware('auth');
    }
  // show all activity item in spesific sub activity
  public function showallactivityitems($id)
    {   
      try 
        {

        if (Auth::check())
          {

            if(Auth::user()->can('show-activity_items'))
              {

                Session::put('subactivity_id', $id);
                
                // to get the name of sub activity and marketing acvtivity
                $activity_sub = SubActivity::join('marketing_activities','marketing_activities.id','=','sub_activities.marketingactivity_id')
                 ->where('sub_activities.id','=',$id)
                 ->select('sub_activities.EngName',DB::raw('marketing_activities.Name as MarktingactitvityName'))->first();
                 
                // check if acitvity type sign
                if($activity_sub["MarktingactitvityName"]=="Signs")
                {          
                  $sign=Sign::Where('subactivity_id','=',$id)->get();
                  $Government=Government::lists('name', 'name');
                  $Type=$activity_sub["EngName"];
                  return view('activityitem.Sign',compact('sign','Government','Type')); 
                }
                else
                {

                  // to get all attribute for this sub acivity        
                  $attributname = DB::table('activityitem_attributes')
                                  ->join('activity_items','activity_items.id','=','activityitem_attributes.activityitem_id')
                                  ->join('attributes','attributes.id','=','activityitem_attributes.attrbuti_id')
                                  ->where('activity_items.subactivity_id','=',$id)
                                  ->where('activityitem_attributes.Value','!=','')
                                  ->groupBy('attributes.Name')
                                  ->select('attributes.Name')->get();

                  // return all activity for specific sub activity
                  $items =ActivityItem::where('activity_items.subactivity_id','=',$id)
                          ->select('activity_items.Code','activity_items.id')->get();
                  
                  $data=array(); 
                  $fdata=[];     

                  foreach ($items as  $value) 
                    {
                      
                      $item= DB::table('activityitem_attributes')
                              ->join('activity_items','activity_items.id','=','activityitem_attributes.activityitem_id')
                              ->join('attributes','attributes.id','=','activityitem_attributes.attrbuti_id')
                              ->where('activity_items.Code','=',$value->Code)
                              ->where('activityitem_attributes.Value','!=','')
                              ->select('activityitem_attributes.id','activity_items.Code','activity_items.id','activityitem_attributes.Value','attributes.Name')->get();
              
                      if($item!=[])
                        {
                          $i=0;
                          $itemactivity=ActivityItem::find($value->id);
                          $data['Contractor_Count']= $itemactivity->HasContractors->count();
                          $data['code']= $value->Code;
                          $data['itemid']= $value->id;

                          foreach ($attributname  as $att) 
                            {
                           
                              $i=0;
                              foreach ($item as  $key) 
                                {
                                  if($att->Name == $key->Name)
                                  {
                                    $i=1;
                                    $data[$key->Name]= $key->Value;
                                  }
                                }//end foreach
                              if($i== 0)
                                {
                                  $data[$att->Name]= "";
                                }
                            }//end foreach
                          array_push($fdata,$data);
                        }//end if
                    } //end foreach  
                  return view('activityitem.index',compact('id','fdata','attributname','activity_sub')); 
                }//end else

              }//end if permission
            else 
              {
                return view('errors.403');
              }
          }//end if Auth 
        else
          {
            return redirect('/login');
          }

      }//endtry
    catch(Exception $e) 
      {
        return redirect('/login');
      }    
  }

  //add winner contractor
  public function storeactivitycontractor()
    {
      try  
        {
          if (Auth::check())
            {
              if(Auth::user()->can('create-activityitem_contractors'))
                {

                  $itemid=Input::get('activity'); 
                  $data = Input::get('ID');
                  $quantity = Input::get('Quantity');
                  $b=[];
                  foreach ($quantity as  $key) 
                    {
                      if($key!=="")
                        {
                          array_push($b,$key);
                        }
                    }//end foreach
                  $item=ActivityItem::find($itemid);
                  for($i=0;$i<count($data);$i++) 
                    {
                      $item->contractors()->attach($data[$i],["Quantity"=>$b[$i],"Item_Name"=>" "]);
                    }  
                  $activityitemid=Session::get('activityitem_id');
                  return redirect('/showallcontractor/'.$activityitemid); 
                }//end if perrmission
              else 
                {
                  return view('errors.403');
                }
            }//endAuth
          else
            {
              return redirect('/login');
            }
        }//endtry
      catch(Exception $e) 
        {
          return redirect('/login');
        }      
    }
  
  // import winner contractor
  public function importwinnercontractor()
    {

      ini_set('xdebug.max_nesting_level', 120);
      ini_set('max_execution_time', 300);
      $temp= Request::get('submit'); 
      $rout='/showallwinnerretailer/';
      if(isset($temp))
        { 
          $filename = Input::file('file')->getClientOriginalName();
          $Dpath = base_path();
          $upload_success =Input::file('file')->move( $Dpath, $filename);
          $activityitemid=Session::get('activityitem_id');
          Excel::load($upload_success, function($reader)
          {   
            $activityitemid=Session::get('activityitem_id');
            $results = $reader->get()->all();
            $item=ActivityItem::find($activityitemid);
            foreach ($results as $data)
              {
                if($data['type']=="App\Retailer")
                  {
                    
                    $Retailer=Retailer::where('Code','=',$data['code'])->first();
                    $item->retailers()->attach($Retailer->id,["Quantity"=>$data['quantity'],"Item_Name"=>$data['prize']]);
                  }//end if
                else
                  {
                    $contractor=Contractor::where('Code','=',$data['code'])->first();
                    if($contractor!=[])
                      {               
                        $item->contractors()->attach($contractor->id,["Quantity"=>$data['quantity'],"Item_Name"=>$data['prize']]);
                      }
                  }//end else    
              }//end foreach
          });// end excel
        }//end if
      return redirect($rout.$activityitemid);             
    }

  //delete partipate contractor  
  public function deleteactivitycontractor($id)
    {
      try 
        {
          if (Auth::check())
            {
              if(Auth::user()->can('delete-activityitem_contractors'))
                {
                  $activityitemid=Session::get('activityitem_id');
                  $item=ActivityItem::find($activityitemid);
                  $item->HasContractors()->detach($id);
                  return redirect('/showallcontractor/'.$activityitemid); 
                }
              else 
                {
                  return view('errors.403');
                }//end else
            }//endAuth
          else
            {
              return redirect('/login');
            }
        }//endtry
      catch(Exception $e) 
        {
          return redirect('/login');
        }      
    }
  
  //show all participate contractors
  public function showallcontractor($code)
    {
      try 
        {
          if (Auth::check())
            {
              if(Auth::user()->can('show-activityitem_contractors'))
                {

                  Session::put('activityitem_id', $code);
                  $activity= ActivityItem::find($code); 
                  $retailers=$activity->HasRetailers;
                  $contractors=$activity->HasContractors;
                  return view('activityitem.showallcontractor',compact('contractors','code','retailers'));
                }
              else 
                {
                  return view('errors.403');
                }
            }//endAuth
          else
            {
              return redirect('/login');
            }             
        }//endtry
      catch(Exception $e) 
        {
          return redirect('/login');
        } 
    }

  // show all winner contractor
  public function showallwinnercontractor($code)
    {
      try 
        {
          if (Auth::check())
            {
              if(Auth::user()->can('show-activityitem_contractors'))
                {
                  Session::put('activityitem_id', $code);
                  $activityitem=ActivityItem::find($code); 
                  $contractors=$activityitem->contractors;
                  return view('activityitem.showwinner',compact('contractors','code','activity'));
                }
              else 
                {
                  return view('errors.403');
                }
            }//endAuth
          else
            {
              return redirect('/login');
            }
        }//endtry
      catch(Exception $e) 
        {
          return redirect('/login');
        } 
    }

  // show all winner retailer
  public function showallwinnerretailer($code)
    {
      try 
        {
          if (Auth::check())
            {
              if(Auth::user()->can('show-activityitem_contractors'))
                {
                  Session::put('activityitem_id', $code);
                  $activityitem=ActivityItem::find($code); 
                  $retailers=$activityitem->retailers;
                  return view('activityitem.showwinnerretailer',compact('retailers','code','activity'));
                }
              else 
                {
                  return view('errors.403');
                }
            }//endAuth
          else
            {
              return redirect('/login');
            }            
        }//endtry
      catch(Exception $e) 
          {
            return redirect('/login');
          } 
    }
  
  //delete activity item
  public function destroy($code)
    {
      try 
        {

          if (Auth::check())
            {
              if(Auth::user()->can('delete-activity_items'))
                {

                  $item=ActivityItem::find($code);
                  $item->delete();
                  $subactivityid=Session::get('subactivity_id');
                   return redirect('/showallactivityitems/'.$subactivityid); 
                }
              else 
                {
                  return view('errors.403');
                }
            }//endAuth
          else
            {
              return redirect('/login');
            }
        }//endtry
      catch(Exception $e) 
        {
          return redirect('/login');
        }         
    }
  
  // to show page of update activity item
  public function Updateitem($code)
    {  
      try
        {
          if (Auth::check())
            {
              if(Auth::user()->can('edit-activity_items'))
                {
                  $District=District::lists("Name","Name");
                  $Government=Government::lists("Name","Name");

                  $item=DB::table('activityitem_attributes')
                          ->join('activity_items','activity_items.id','=','activityitem_attributes.activityitem_id')
                          ->join('attributes','attributes.id','=','activityitem_attributes.attrbuti_id')
                          ->where('activity_items.id','=',$code)
                          ->where('activityitem_attributes.Value','!=','')
                          ->select('attributes.id','activity_items.Code','activityitem_attributes.Value','attributes.Name','attributes.Type')->get(); 
                  return view('activityitem.updateitem',compact('item','District','Government'));
                }// end if permission
              else 
                {
                  return view('errors.403');
                }
            }//endAuth
          else
            {
              return redirect('/login');
            }
        }// end try
      catch(Exception $e)
        {
          return redirect('/login');
        }
    }

  // to show page of create activity item
  public function actattruibute()
    {
      try 
        {
          if (Auth::check())
            {
              if(Auth::user()->can('create-activity_items'))
                {
                  $id=Input::get('id'); 
                  $subactivityid=Session::get('subactivity_id');
                  $District=District::lists("Name","Name");
                  $Government=Government::lists("Name","Name");
                  $activityitemid=ActivityItem::where('subactivity_id','=',$id)
                                    ->select('id')->pluck('id');
                  $attributeitems=DB::table('activityitem_attributes')->join('attributes', 'activityitem_attributes.attrbuti_id', '=', 'attributes.id')
                    ->where('activityitem_id','=',$activityitemid[0])
                    ->select('attrbuti_id','attributes.Name','attributes.Type')
                    ->groupBy('attrbuti_id','attributes.Name','attributes.Type')
                    ->get();
                  
                  return view('activityitem.fcreate',compact('attributeitems','id','subactivityid','District','Government'));
                }//end if permission
              else 
                {
                  return view('errors.403');
                }
            }//end if Auth
          else
            {
            return redirect('/login');
            }
        }//endtry
      catch(Exception $e) 
        {
          return redirect('/login');
        }    
    }

  // actual create activity item and its table related
  public function storeactivityitemattributes()
    {
      try 
        {
          if (Auth::check())
            {  
              //get all input 
              $values=Input::all();
              $id=Input::get('id');

              //create activity item code 
              // code contain sub activity name plus start date plus end date
              $subactivity=SubActivity::whereId($id)->select('Name')->first();
              $attrbutie=Attribute::where('Name','=','Start Date')->first();
              $attrbutieEndDate=Attribute::where('Name','=','End Date')->first();

              //check if the code not exist
              if(array_key_exists($attrbutieEndDate->id, $attrbutieEndDate))
                $ExistActivity =ActivityItem::where('Code',$subactivity->Name.$values[$attrbutie->id].$values[$attrbutieEndDate->id])->first();
              else
                $ExistActivity =ActivityItem::where('Code',$subactivity->Name.$values[$attrbutie->id])->first();

              //the new activity
              $activity = New ActivityItem();
              
              // when the cativity code is not found
              if($ExistActivity == null || $ExistActivity == [])
                {
                  if(array_key_exists($attrbutieEndDate->id, $attrbutieEndDate))
                    $activity->Code=$subactivity->Name.$values[$attrbutie->id].$values[$attrbutieEndDate->id];
                  else   
                    $activity->Code=$subactivity->Name.$values[$attrbutie->id]; 
                  $activity->subactivity_id=$id;
                  $activity->save();
                }
              //when the cativity code is found
              else
                {
                  $random_code = str_random(2);
                  if(array_key_exists($attrbutieEndDate->id, $attrbutieEndDate))
                    $activity->Code=$subactivity->Name.$values[$attrbutie->id].$values[$attrbutieEndDate->id].$random_code;
                  else   
                    $activity->Code=$subactivity->Name.$values[$attrbutie->id].$random_code;     
                  $activity->subactivity_id=$id;
                  $activity->save();
                }
              
              $activityitem= ActivityItem::where('id','=',$activity->id)->first(); 
              $file_frontimg = Request::file('img');
              if($file_frontimg!=null)
                {
                  $imageName = $file_frontimg->getClientOriginalName();
                  $path=public_path("/assets/dist/img").$imageName;
                  $attrbutie=Attribute::where('Name','=','Image')->first();

                  if (!file_exists($path)) {
                    $file_frontimg->move(public_path("/assets/dist/img"),$imageName);
                    $activityitem->getactivityitemsattribute()->attach($attrbutie->id,['Value'=>"/assets/dist/img/".$imageName]);

                  }
                  else
                  {
                     $random_string = md5(microtime());
                     $file_frontimg->move(public_path("/assets/dist/img"),$random_string.".jpg");
                     $activityitem->getactivityitemsattribute()->attach($attrbutie->id,['Value'=>"/assets/dist/img/".$random_string.".jpg"]);
                  }
                }
    
              $startevent=null;
              $endevent=null;
              $startdate=Attribute::where('Name','=','Start Date')->first();
              $enddate=Attribute::where('Name','=','End Date')->first();
              foreach ($values as $key => $value) 
                {
                  if( is_numeric($key))
                    {
                      if($key==$startdate->id)
                        $startevent=$value;
                      else if($key==$enddate->id)
                        $endevent=$value;

                      $activityitem->getactivityitemsattribute()->attach($key,['Value'=>$value]);
                    }
                }

              //insert in calander activity
              if($startevent!=null)
              {
                
                $event=new Event;
                $event->start=$startevent." 00:00:00";
                if($endevent!=null)
                  $event->end=$endevent." 00:00:00";
                else
                  $event->end=$startevent." 00:00:00";
                $event->title=$activityitem->Code;
                $event->Type="MarketingActivity";
                $event->save();
              }

         
              return redirect('/showallactivityitems/'.$id);  
            }//endAuth

          else
            {
              return redirect('/login');
            }
        }//endtry
      catch(Exception $e) 
        {
          return redirect('/login');
        }
    }

  // actual update activity item and its table related
  public function updateactivityitemattributes()
    {
      try 
        {
          if (Auth::check())
            { 
              $id=Input::get('id');
              $values=Input::all();
                  
              $activityitem= ActivityItem::where('Code','=',$id)->first();
              $file_frontimg = Request::file('img');
              if($file_frontimg!=null)
                {
                  $imageName = $file_frontimg->getClientOriginalName();
                  $path=public_path("/assets/dist/img").$imageName;
                  $attrbutie=Attribute::where('Name','=','Image')->first();

                  if (!file_exists($path)) 
                    {
                      $file_frontimg->move(public_path("/assets/dist/img"),$imageName);
                      $activityitem->getactivityitemsattribute()->attach($attrbutie->id,['Value'=>"/assets/dist/img/".$imageName]);     
                    }
                  else
                    {
                      $random_string = md5(microtime());
                      $file_frontimg->move(public_path("/assets/dist/img"),$random_string.".jpg");
                      $activityitem->getactivityitemsattribute()->attach($attrbutie->id,['Value'=>"/assets/dist/img/".$random_string.".jpg"]);
                    }
                }//end if

              foreach ($values as $key => $value) 
                {
                  if( is_numeric($key))
                    { 
                      $activityitem->getactivityitemsattribute()->detach($key); 
                      $activityitem->getactivityitemsattribute()->attach($key,['Value'=>$value]);
                    }//end if
                }//end foreach
              $subactivityid=Session::get('subactivity_id');
              return redirect('/showallactivityitems/'.$subactivityid);  
            }//endAuth
          else
            {
              return redirect('/login');
            }
        }//endtry
      catch(Exception $e) 
        {
          return redirect('/login');
        }     
    }

  // import particaite contractor and retailer
  public function importActivityItem()
    {
      ini_set('xdebug.max_nesting_level', 120);
      ini_set('max_execution_time', 300);
      $temp= Request::get('submit'); 
      if(isset($temp))
        { 
          $filename = Input::file('file')->getClientOriginalName();
          $Dpath = base_path();
          $upload_success =Input::file('file')->move( $Dpath, $filename);
          
          Excel::load($upload_success, function($reader)
          {   
            $results = $reader->get()->all();
              
            $activityitemid=Session::get('activityitem_id');
            $item=ActivityItem::find($activityitemid);
            foreach ($results as $data)
              {
                $lastcont=Contractor::orderBy('Code', 'DESC')->first();
                $lastcode=(int)$lastcont->Code+1;
                $Contractor= Contractor::Where('Tele1',$data['telephone1'])->orWhere('Tele2',$data['telephone1'])->first();
              
                $retailer=Retailer::where('Code','=',$data['coderet'])->first();
                if($Contractor!=null && $retailer!=null)
                  {               
                    $item->HasContractors()->attach($Contractor->id,["Retailer_id"=>$retailer->id,"JoinDate"=>$data['date']]);
                  }
                elseif ($Contractor != null && $retailer ==null) 
                  {
                    $item->HasContractors()->attach($Contractor->id,["Retailer_id"=>null,"JoinDate"=>$data['date']]);
                  }
                elseif ($Contractor == null && $retailer !=null) 
                  {
                    $item->HasRetailers()->attach($retailer->id,["contractor_id"=>null,"JoinDate"=>$data['date']]);
                  }
                elseif ($Contractor == null && $retailer ==null) 
                  {
                    $Contractors =New Contractor;      
                    $Contractors->Name =$data['name'];
                    $Contractors->Government =$data['government'];
                    $Contractors->District =$data['district'];
                    $Contractors->Tele1 =$data['telephone1'];
                    $Contractors->Tele2 =$data['telephone2'];
                    $Contractors->Pormoter_Id =null;
                    $Contractors->Code=$lastcode;
                    $Contractors->Status="Old";
                    $Contractors->save();         
                    $item->HasContractors()->attach($Contractors->id,["Retailer_id"=>null,"JoinDate"=>$data['date']]);
                  }// end else if       
              }//end foreach    
          });//end excel
        }//end if attamp
      $activityitemid=Session::get('activityitem_id');
      return redirect('/showallcontractor/'.$activityitemid);        
    }
}
