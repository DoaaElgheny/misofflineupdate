<?php

namespace App\Http\Controllers;
use Auth;
use App\Http\Requests;
use DB;
use App\Http\Controllers\Controller;
use Request;
use Excel;
use File;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Input;
use Validator;
use Illuminate\Support\Facades\Redirect;
use App\User;
use App\Gps;
use App\Product;
use App\Company;
use App\sale_product;
use App\sale_product_details;
use App\Contractor;
use App\Price;
use Session;
use Geocode;
use App\Order;
use App\Activity;
use Image;
use PHPExcel; 
use App\Government;
use App\District;
use App\Cafe;
use App\CafeSigns;
use App\Sign;


class ExportsController extends Controller
{

  public function __construct()
   {
       $this->middleware('auth');
   }
  public function exportgpsvisits()
   {  
    $exportbtn= input::get('submit'); 

    if(isset($exportbtn))
    { 
  
      Excel::create('Visitsfile', function($excel)
       { 

        $excel->sheet('GpsVisitssheetname',function($sheet)
        {        

          $sheet->appendRow(1, array(
               'Date','Time','Long','Lat','user_id','Contractor_code','Contractor_Name','Status','Backcheck'
));
          $data=[];

 $gps=Gps::all();


    foreach ($gps as $gps) {
if($gps->Read==0)
{ 

    array_push($data,array(

     
      $gps->NDate,
      $gps->NTime,
      $gps->Long,
      $gps->Lat,
      $gps->getusers->Code,
      $gps->Contractor_Code,
      $gps->Contractor_Name,
      $gps->Status,
      $gps->Type
      

    
 
      

      ));
    $updateread= DB::table('gps')->where('id',$gps->id)->update(array('Read' => 1));
    
    } 
}

  $sheet->fromArray($data, null, 'A2', false, false);
});
})->download('xls');
                           
        }

                          
                            } 

 public function exportsingscafes()
   {  
    $exportbtn= input::get('submit'); 

    if(isset($exportbtn))
    { 
  
      Excel::create('singscafes', function($excel)
       { 

        $excel->sheet('singscafes',function($sheet)
        {        

          $sheet->appendRow(1, array(
               'Date','Time','Adress','Long','Lat','user_id','Img','Government','District','Status','Type','Target'
));
          $data=[];

 $CafeSigns=CafeSigns::all();


    foreach ($CafeSigns as $CafeSigns) {
if($CafeSigns->Read==0)
{ 

    array_push($data,array(

     
      $CafeSigns->NDate,
      $CafeSigns->NTime,
       $CafeSigns->Adress,
      $CafeSigns->Long,
      $CafeSigns->Lat,
      $CafeSigns->getcafe_users->Code,
      $CafeSigns->Img,
      $CafeSigns->Government,
      $CafeSigns->District,
      $CafeSigns->Status,
      $CafeSigns->Type,
      $CafeSigns->Target

      

    
 
      

      ));
    $updateread= DB::table('CafeSigns')->where('id',$CafeSigns->id)->update(array('Read' => 1));
    
    } 
}

  $sheet->fromArray($data, null, 'A2', false, false);
});
})->download('xls');
                           
        }
        
                          
                            } 




 public function exportlocalcompany()
   {  
    $exportbtn= input::get('company'); 

    if(isset($exportbtn))
    { 
  
      Excel::create('localcompany', function($excel)
       { 

        $excel->sheet('localcompany',function($sheet)
        {        

          $sheet->appendRow(1, array(
               'Name','EnName','Government','District','Address','Head Office Gov','Head Office Addre','Production Lines','Monthly Capacity','Code'
));
          $data=[];

 $Company=Company::all();


    foreach ($Company as $Company) {
if($Company->Read==0)
{ 

    array_push($data,array(

     
      $Company->Name,
      $Company->EnName,
       $Company->Government,
      $Company->District,
      $Company->Address,
      $Company->HeadOfficeGov,
      $Company->HeadOfficeAddre,
      $Company->ProductionLines,
      $Company->MonthlyCapacity,
      $Company->Code

      

    
 
      

      ));
    $updateread= DB::table('companies')->where('id',$Company->id)->update(array('Read' => 1));
    
    } 
}

  $sheet->fromArray($data, null, 'A2', false, false);
});
})->download('xls');
                           
        }
        
                          
                            } 
                            
                            public function charts()
                            {
                              return view('/charts/analytics');
                            }
                             public function usersvue()
                            {
                              $users =User::all();
                            return response()->json( $users);
                            }

                             }
