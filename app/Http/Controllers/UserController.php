<?php

namespace App\Http\Controllers;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use Request;
use Excel;
use Input;
use File;
use Validator;
use DB;
use Auth;
use Symfony\Component\Debug\Exception\FatalThrowableError;
use Datatables;
use Illuminate\View\Middleware\ErrorBinder;
use Exception;
use App\Gps;
use App\Role;
use App\Permission;
use App\Government;
use Session;
use App\District;



class UserController extends Controller
{
  public function __construct()
   {
       $this->middleware('auth');
   }
public function get($id){

$entry=User::find($id);
// dd(storage_path().'/app'.$entry->FileName);
 $path = storage_path().'/app'.'/'.$entry->FileName;
 
if($entry->FileName !="")
{
  return response()->download($path);
}
else
{
return view('users.ShowCv',compact('id'));

}

 
   
 }

public function ViewPDF($id)

{
  $orginalName=User::where('id',$id)->first();
   $file = $file = 'file:///var/www/html/cemexmarketingsystem/storage/app/'.$orginalName->FileName;



                if (file_exists($file)){
                  

                   $ext =File::extension($file);
                 
                    if($ext=='pdf'){
                        $content_types='application/pdf';
                        return response(file_get_contents($file),200)
                           ->header('Content-Type',$content_types);
                       }
                     
                   
                    
                                                             
                }else
                {
                 return redirect('/users');
                }
}

public function showCV($id)
{
 return view('users.ShowCv',compact('id'));
}
public function importuser()
  {
    // try
    // {

  $temp= Request::get('submituser'); 


   if(isset($temp))

 { 


   $filename = Input::file('file')->getClientOriginalName();
     

     $Dpath = base_path();


     $upload_success =Input::file('file')->move( $Dpath, $filename);
    

       Excel::load($upload_success, function($reader)
       {   
     
         


   $results = $reader->get()->all();

       foreach ($results as $data)
        {
         app('App\Http\Controllers\UserController')->ValidatePromoter($data);
   
        }
 
      });


  }

   return redirect('/users'); 
   
 // }
 // catch(Exception $e)
 // {
 //   return redirect('/users'); 
 // }

}
  public function ValidatePromoter($data)
  {
          
    if(!isset($GLOBALS['user'])) { $GLOBALS['user']= array(); } 
        if(!isset($GLOBALS['Doubleuser'])) { $GLOBALS['Doubleuser']= array(); } 

        if(!isset($UserErr)) { $UserErr = 'البيانات غير صحيحة للمندوب: '; }      
        if(!isset($DubleUserErr)) { $DubleUserErr = 'البيانات موجودة بالفعل للمندوب: '; }  

    $data['start_date'] = strtotime($data['start_date']);
    $data['start_date'] = date('Y-m-d',$data['start_date']);
   
          
           
        

           
    
         
    $user =new User();
    $user->Name= (isset($data['name']) ? $data['name'] : '');

    if($data['enname']!=null)
    $user->Enname= (isset($data['enname']) ? $data['enname'] : '');
    else
    $user->Enname= (isset($data['name']) ? $data['name'] : '');

    $user->Tele=(isset($data['telephone']) ? $data['telephone'] : '');

    if($data['username']!=null)
    $user->Username =(isset($data['username']) ? $data['username'] : '');
    else
    $user->Username= (isset($data['name']) ? $data['name'] : '');
    $hashedpassword=(isset($data['password']) ? $data['password'] : '');
    $user->password =\Hash::make( $hashedpassword);
    $user->role=(isset($data['role']) ? $data['role'] : '');
    $user->CardNumber=(isset($data['card_number ']) ? $data['card_number'] : '');
    $user->Email =(isset($data['email']) ? $data['email'] : '');
    $user->IDNumber=(isset($data['id_number']) ? $data['id_number'] : '');
    $gov = substr($user->Government, 0, 6);
    $last_id=User::orderBy('id', 'desc')->first();
    $user->Code=$gov.($last_id->id+1);
    $user->Experince=(isset($data['experince']) ? $data['experince'] : '');
    $user->Government =(isset($data['government']) ? $data['government'] : '');
    $user->District =(isset($data['district']) ? $data['district'] : '');
    $user->Position =(isset($data['position']) ? $data['position'] : '');
    $user->Start_Date =(isset($data['start_date']) ? $data['start_date'] : '');
    $user->Salary =(isset($data['salary']) ? $data['salary'] : '');
  
    $name_regex = preg_match('/^(?:[\p{L}\p{Mn}\p{Pd}\'\x{2019}]+(?:$|\s+)){2,}/u' , $data['name']);
        //name  check
     // if($data['email']=="mohamed01286840038@yahoo.com")
     //            {
     //                     dd($data);
     //            }
        
if (isset($data['password'])) {   

    if (isset($data['name'])) {
     
            
        if ($name_regex == 1) { // true pormoter_name 

               
            $gov_regex = preg_match('/^[\pL\s]+$/u' , $data['government']);  
            if ($gov_regex == 1) { // true goverment

              $city_regex = preg_match('/^[\pL\s]+$/u' , $data['district']);
                if ($city_regex == 1 ) { // true city
              
              
                      $Exp_regex = preg_match('/^(.*[^0-9]|)(1000|[1-9]\d{0,2})([^0-9].*|)$/' , $data['experince']);
               
                      if ($Exp_regex == 1 || $data['experince']==null) { // true experince  
                                      
                          $Telephone_regex = preg_match('/^[0-9]{10,11}$/', $data['telephone']);
                                if ($Telephone_regex == 1 && isset($data['telephone'])) { // telephonno 
                                             
                                  $email_regex = preg_match('/(.+)@(.+){2,}\.(.+){2,}/' , $data['email']);
                                  if ($email_regex == 1 ) { // mail  
                                       
                                      $usename_regex = preg_match('/^(?:[\p{L}\p{Mn}\p{Pd}\'\x{2019}]+(?:$|\s+)){2,}/u' , $data['username']);
                                      //dd($usename_regex,$data['username'] );
                                    if ($usename_regex == 1 || $data['username']==null) { // user_name  
                                     //dd($user);
                                      $Salary_regex = preg_match('/^[0-9]+$/' , $data['salary']);
                                      if ($Salary_regex == 1 || $data['salary']==null) { // Salary 
                                               
                                        $Sdate= explode (' ',$data['start_date']);
                                                $Sdate_regex = preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/' , $Sdate[0]);
                                        if ($Sdate_regex == 1 || $data['start_date']==null) { //date 

                                            try {
                                                   
                                                   $user->save(); 
                                                   //dd($user);

                                                } 
                                                catch (Exception $e)
                                                 {
                                                    
                                                //dd($e);

                                                      //if promoter exists
                                               
                                                      $phone_string= "Duplicate entry '".ltrim($data['telephone'], '0')."' for key 'Tele'";
                                                      $mail_string= "Duplicate entry '".$data['email']."' for key 'users_email_unique'";
                                                      if($data['username']!=null)
                                                      $username_string= "Duplicate entry '".$data['username']."' for key 'users_username_unique'";
                                                      else
                                                      $username_string= "Duplicate entry '".$data['name']."' for key 'users_username_unique'";
                                                      
                                                          if (isset($e->errorInfo)) {
                                                          if ($phone_string == $e->errorInfo[2]) { 
                                                              array_push($GLOBALS['Doubleuser'],$data['name']);
                                                        }
                                                        if ($mail_string == $e->errorInfo[2] ) {  
                                                              array_push($GLOBALS['Doubleuser'],$data['name']);
                                                        }
                                                        if ($username_string == $e->errorInfo[2]) {  
                                                             $Pormoter_Id= User::where('Username',$data['name'])->pluck('id')->first();
                                                        }
                                                     
                                                      }
                                                        if (isset($Pormoter_Id)){  //update not unique values
                                                                 $userid= User::where('Email',$data['email'])->pluck('id')->first();
                                                                  $user_id= User::where('Tele',$data['tele'])->pluck('id')->first();
                                                             
                                                           if(($Pormoter_Id!=$user_id && $Pormoter_Id!=$userid)||($Pormoter_Id==$user_id && $Pormoter_Id==$userid))
                                                           {//dd($Pormoter_Id,$user_id,$userid);
                                                              $update_promoter = User::find($Pormoter_Id);
                                                              $user->Name= (isset($data['name']) ? $data['name'] : '');

                                                              if($data['enname']!=null)
                                                              $update_promoter->Enname= (isset($data['enname']) ? $data['enname'] : '');
                                                              else
                                                              $update_promoter->Enname= (isset($data['name']) ? $data['name'] : '');

                                                              $update_promoter->Tele=(isset($data['telephone']) ? $data['telephone'] : '');

                                                              if($data['username']!=null)
                                                              $update_promoter->Username =(isset($data['username']) ? $data['username'] : '');
                                                              else
                                                              $update_promoter->Username= (isset($data['name']) ? $data['name'] : '');

                                                              $hashedpassword=(isset($data['password']) ? $data['password'] : '');
                                                             
                                                              $update_promoter->password =\Hash::make( $hashedpassword);
                                                              $update_promoter->role=(isset($data['role']) ? $data['role'] : '');
                                                              $update_promoter->CardNumber=(isset($data['card_number ']) ? $data['card_number'] : '');
                                                              $update_promoter->Email =(isset($data['email']) ? $data['email'] : '');
                                                              $update_promoter->IDNumber=(isset($data['id_number']) ? $data['id_number'] : '');
                                                              $update_promoter->Experince=(isset($data['experince']) ? $data['experince'] : '');
                                                              $update_promoter->Government =(isset($data['government']) ? $data['government'] : '');
                                                              $update_promoter->District =(isset($data['district']) ? $data['district'] : '');
                                                              $update_promoter->Position =(isset($data['position']) ? $data['position'] : '');
                                                              $update_promoter->Start_Date =(isset($data['start_date']) ? $data['start_date'] : '');
                                                              $update_promoter->Salary =(isset($data['salary']) ? $data['salary'] : '');
                                                            
                                                              $update_promoter->save();
                                                           }
                                                           else
                                                           {

                                                             array_push($GLOBALS['Doubleuser'],$data['name']);
                                                            // dd($GLOBALS['Doubleuser']);
                                                           }
                                                        }
                                                        //update unique values
                                                    }   //end catch                    
                              
                                }
                                else 
                                  array_push($GLOBALS['user'],$data['name']); 
                                                        
                            
                              }
                              else 
                                array_push($GLOBALS['user'],$data['name']);                              
                          
                            }
                            else 
                              array_push($GLOBALS['user'],$data['name']); 
                                                            
                          
                          }
                          else
                            array_push($GLOBALS['user'],$data['name']);                                                
                        }
                        else 
                          array_push($GLOBALS['user'],$data['name']);                              
                      }
                      else 
                        array_push($GLOBALS['user'],$data['name']);
                      
                    }
                    else 
                      array_push($GLOBALS['user'],$data['name']); 
                    
                  }
             
            }
      
          
                   
     }
      else 
        array_push($GLOBALS['user'],(isset($data['telephone']) ? $data['telephone'] : $data['name'])); 
      
    }
    else
            array_push($GLOBALS['user'],$data['name']); 
             

        if ( !empty ($GLOBALS['user'] )) {
            $GLOBALS['user'] = array_unique($GLOBALS['user']);
            $UserErr = $UserErr.implode(" \n ",$GLOBALS['user']);
            $UserErr = nl2br($UserErr);  
            $cookie_name = 'UserErr';
            $cookie_value = $UserErr;
            setcookie($cookie_name, $cookie_value, time() + (60), "/");
        }

        if ( !empty ($GLOBALS['Doubleuser'] )) {
            $GLOBALS['Doubleuser'] = array_unique($GLOBALS['Doubleuser']);
            $DubleUserErr= $DubleUserErr.implode(" \n ",$GLOBALS['Doubleuser']);
            $DubleUserErr= nl2br($DubleUserErr);  
            $cookie_name = 'DubleUserErr';
            $cookie_value = $DubleUserErr;
            setcookie($cookie_name, $cookie_value, time() + (60), "/");
        }
   
  } //function end

 	public function index(Request $request)
	{

   $checkTime = strtotime('09:00:0');


// $loginTime = strtotime('10:30:00');
// $diff = $checkTime - $loginTime;
// echo ($diff < 0)? 'Late!' : 'Right time!'; echo '<br>';
// dd('Time diff in sec: '.abs($diff)/60) ;

     //      if (Auth::check()){


        //      if(\Auth::user()->role == 'admin')
          // { 
                 //     $users = User::where('role','user')->get();

                 //     return view('users/index',compact('users'));
                 // }
                 // else
                 // {
     // $updateWebsite=DB::table('users')->where('Type_User','Outdoor Promoters')
           
     //        ->update(['Supervisor' =>1680]);
       $showuser=1;
                      Session::put('UserIndex', 'index');
                       $usersOutDoor = User::has('visor')->with('visor')->where('Type_User','Outdoor Promoters')->get();
                         $usersOffice = User::has('visor')->with('visor')->where('Type_User','Office Promoters')->get();
                       $Role=Role::lists('name', 'id');
              $Government=Government::lists('name', 'id');

              $District=District::lists('name', 'id');

              $User=User::where('Position','=','Supervisoer')->lists('name', 'id');
          

                  return view('users/index',compact('usersOutDoor','usersOffice','Role','Government','District','User','showuser'));

                 // }

      // }
      
      // else {
      //  return redirect('/login');
      // }
  // }
  // catch(Exception $e)  
  // {
  //  return redirect('/login');
  // }
}
 
  public function OfficeUser(Request $request)
  {
     //  try {
           
     //      if (Auth::check()){


        //      if(\Auth::user()->role == 'admin')
          // { 
                 //     $users = User::where('role','user')->get();

                 //     return view('users/index',compact('users'));
                 // }
                 // else
                 // {
   
                      Session::put('UserIndex', 'OfficeUser');
              $showuser=0;
               $users = User::has('visor')->with('visor')->where('Type_User','Office Promoters')->get();
                      
                       $Role=Role::lists('name', 'id');
              $Government=Government::lists('name', 'id');

              $District=District::lists('name', 'id');

              $User=User::where('Position','=','Supervisoer')->lists('name', 'id');
          

                  return view('users/index',compact('users','Role','Government','District','User','showuser'));

                 // }

      // }
      
      // else {
      //  return redirect('/login');
      // }
  // }
  // catch(Exception $e)  
  // {
  //  return redirect('/login');
  // }
}
public function destroy($id)
  {

    try
    { 
      if (Auth::check()){
        $users=User::find($id);
    
        if(\Auth::user()->Username == $users->Username)
        { 
          
          return redirect('/login');
        }
          $users->delete();
        $back=Session::get('UserIndex');
        if($back==='index')
        return redirect('/users');
      else
        return redirect('/OfficeUser');
        }
        else {
        return redirect('/login');
      }

      }
     catch(Exception $e)
    {
      return redirect('/login');

    }
  }




    public function create()
  { 
    
  }


  public function store(Request $request)
  {  
     //dd(Input::all());

     
   $user = new User;
   $user->Name =Request::get('Name');
      $user->Enname =Request::get('EngName');
$user->Type_User=Request::get('Type_User');
   $user->Username =Request::get('Username');
   $user->Email =Request::get('Email');
   $hashedpassword=Request::get ('Password');
   $user->password=\Hash::make( $hashedpassword);
   $Government=Request::get('Government');
   $District=Request::get('District');
 $Districtname=District::where('id','=',$District)->first();
 $Governmentname=Government::where('id','=',$Government)->first();
$user->Government=$Governmentname->Name;
$user->District=$Districtname->Name;
$user->Experince=Request::get('Experince');
$user->Start_Date=Request::get('Date');
$user->Tele=Request::get('Telephone');
$user->Salary=Request::get('Salary');

 $last_id =User::find(DB::table('users')->max('id'));


$userCode=$last_id->id;


$user->Code=($userCode+2);

$user->CardNumber=Input::get('CardNumber');
$user->IDNumber=Input::get('IDNumber');
if(Input::get('MobRole')!=null)
{
$user->role=Input::get('MobRole');
}
 if(Request::get('Position')==1)

    {

  $user->Position="Supervisoer";


    }
    else
    {
      $user->Position="Null";
    }
    $gov = substr($Governmentname->Name, 0, 6);
  


$user->save();

         $roles = Request::get('Role');
         //dd($user,Input::get('IDNumber'));
          if($roles!=null)
          {
             foreach ($roles as $role_id) 
              {

                  $user->attachRole($role_id);
              }
          }

          if(Input::get('User')!=null)
{

 $user->Supervisor=Input::get('User');
}
else
{ 
  $user->Supervisor=$user->id;

}

$user->save();
      $back=Session::get('UserIndex');
        if($back==='index')
        return redirect('/users');
      else
        return redirect('/OfficeUser');

  

}

public function checkUsername( )
 { 
   $id=Input::get('id');
      if($id==null)
      {
      $username=Input::get('Username');
      // dd($username);
    $res = User::select('Username')
            ->where('Username','=',$username)
         
            ->get(); 

   if (count($res)<= 0)

        $isAvailable = true;
        else
         $isAvailable = false;


}
        else
       {
         $username=Input::get('Username');
    $res = User::select('Username')
            ->where('Username','=',$username)
             ->where('id','!=',$id)->get();
               if (count($res)<= 0)

        $isAvailable = true;
        else
         $isAvailable = false;

         }

    return \Response::json(array('valid' =>$isAvailable));
   }

   public function checkEmailuser( )
 { 
  $id=Input::get('id');
      if($id==null)
      {
          $Email=Input::get('Email');
    $res = user::select('Email')
            ->where('Email','=',$Email)
            ->get(); 

    if (count($res)<= 0)

        $isAvailable = true;
        else
         $isAvailable = false;
}
else
{
    $Email=Input::get('Email');
    $res = User::select('Email')
            ->where('Email','=',$Email)
            ->where('id','!=',$id)->get();

             

    if (count($res)<= 0)

        $isAvailable = true;
        else
         $isAvailable = false;

}
    return \Response::json(array('valid' =>$isAvailable));
   }



   public function checkTelephoneUser( )
 { 
    $id=Input::get('id');

      if($id==null)
      {
          $Telephone=Input::get('Telephone');
      // dd($username);
    $res = User::select('Tele')
            ->where('Tele','=',$Telephone)
         
            ->get(); 
      

  if (count($res)<= 0)

        $isAvailable = true;
        else
         $isAvailable = false;
}
else
{
  $Telephone=Input::get('Telephone');
      // dd($username);
    $res = user::select('Tele')
            ->where('Tele','=',$Telephone)
                        ->where('id','!=',$id)->get();

   
  if (count($res)<= 0)

        $isAvailable = true;
        else
            $isAvailable = false;
}
    return \Response::json(array('valid' =>$isAvailable));
   }
      public function checkCardNumberUser( )
 { 
    $id=Input::get('id');
      if($id==null)
      {
          $CardNumber=Input::get('CardNumber');
    $res = user::select('CardNumber')
            ->where('CardNumber','=',$CardNumber)
         
            ->get(); 

  if (count($res)<= 0)

        $isAvailable = true;
        else
         $isAvailable = false;
}
else
{
  $CardNumber=Input::get('CardNumber');
      // dd($username);
    $res = User::select('CardNumber')
            ->where('CardNumber','=',$CardNumber)
                        ->where('id','!=',$id)->get();

   
  if (count($res)<= 0)

        $isAvailable = true;
        else
            $isAvailable = false;
}
    return \Response::json(array('valid' =>$isAvailable));
   }
     public function checkIDNumberUser( )
 { 
    $id=Input::get('id');
      if($id==null)
      {
          $IDNumber=Input::get('IDNumber');
    $res = User::select('IDNumber')
            ->where('IDNumber','=',$IDNumber)
         
            ->get(); 

  if (count($res)<= 0)

        $isAvailable = true;
        else
         $isAvailable = false;
}
else
{
   $IDNumber=Input::get('IDNumber');
      // dd($username);
    $res = user::select('IDNumber')
            ->where('IDNumber','=',$IDNumber)
            ->where('id','!=',$id)->get();

   
  if (count($res)<= 0)

        $isAvailable = true;
        else
            $isAvailable = false;
}
    return \Response::json(array('valid' =>$isAvailable));
   }

// public function fupdate( )
//  { 



//    $users = Input::get('pk');  
//         $column_name = Input::get('name');
//         $column_value = Input::get('value');
    
//         $userData = User::whereId($users)->first();
//         $userData-> $column_name=$column_value;

//         if($userData->save())

//         return \Response::json(array('status'=>1));
//     else 
//         return \Response::json(array('status'=>0));
 
  


// }
  public function edit($id)

  {   
   $Role=Role::lists('name', 'id');
    $Government=Government::lists('Name','Name');
    $District=District::lists('Name','Name');
    $User=User::where('Position','=','Supervisoer')->lists('name', 'id');
    $users=User::find($id);
        $users->Start_Date;

 $usersdate=$users->Start_Date;
   $usersdatef =\Carbon\Carbon::parse($users->Start_Date)->format('m/d/Y');
   

    return view('users.edit',compact('users','Role','Government','User','District','usersdatef'));
 }
  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function update($id)

  {

      $user=User::find($id);
      $roles = Request::get('Role');
      if ($roles!=null)
      {
        $user->UserRoles()->detach();
        foreach ($roles as $role_id) 
        {
          $user->attachRole($role_id);
        }
      }
        if(Request::get ('Password')!="")
   {
     $hashedpassword=Request::get ('Password');
     $user->password=\Hash::make( $hashedpassword);
   }
      $user->Name =Request::get('Name');
      $user->Enname =Request::get('EngName');
      $user->Username =Request::get('Username');
      $user->Email =Request::get('Email');
      $Government=Request::get('Government');
      $District=Request::get('District');
      $user->Government=$Government;
      $user->District=$District;
      $user->Experince=Request::get('Experince') != ""? Request::get('Experince') : null;
      $user->Start_Date=Request::get('Date') != ""? Request::get('Date') : null;
      $user->Tele=Request::get('Telephone');
      $user->Salary=Request::get('Salary') != ""? Request::get('Salary') : null;
      $user->Supervisor=Request::get('User') != ""? Request::get('User') : null;
      $user->CardNumber=Request::get('CardNumber') != ""? Request::get('CardNumber') : null;
      $user->IDNumber=Input::get('IDNumber');
      $user->role=Input::get('MobRole');
        if(Input::get('User')!=null)
{

 $user->Supervisor=Input::get('User');
}
else
{ 
  $user->Supervisor=$user->id;

}

    // $gov = substr($Government, 0, 3);
    // dd(  $Governmentname,$gov);
// $last_id=User::orderBy('id', 'desc')->first();

// $user->Code=$gov.($last_id->id+1);

    $user->save();

       
               



       $back=Session::get('UserIndex');
        if($back==='index')
        return redirect('/users');
      else
        return redirect('/OfficeUser');


  }
public function GovernmentDropDownData($id)
    {
       
        $District=District::where('government_id','=',$id) ->get();

        $options = array();
      foreach ($District as $District) {
      $options += array($District->id => $District->Name);
        }
     
        return \Response::json($options);
    }
public function GovernmentDropDownData2($id)
    {
       $gov=Government::where('Name','=',$id) ->first();
        $District=District::where('government_id','=',$gov->id) ->get();

        $options = array();
      foreach ($District as $District) {
      $options += array($District->Name => $District->Name);
        }
     
        return \Response::json($options);
    }
     public function add() {

   $file = Request::file('filefield');
   $extension = $file->getClientOriginalExtension();
   \Storage::disk('local')->put($file->getClientOriginalName(),  File::get($file));
  
   $entry=User::find(Input::get('id'));
   $entry->mime = $file->getClientMimeType();   
   $entry->originalfilename = $file->getClientOriginalName();
   $entry->FileName= $file->getClientOriginalName();
   $entry->save();
   return redirect('/users');
   
 }
     public  function updateimageg()
    {
      $userid=auth::user()->id;
      $user = User::find($userid);
      $file_img=Input::file('pic');




     // $destinatonPath = '/assets/dist/img/User/';
  //return json_encode($_POST);
  // $random_string = md5(microtime());
  // $newImageName=$random_string.'.png';

   //// $file = Input::file('pic')->move(public_path($destinationPath),$newImageName);
    //Rename and move the file to the destination folder 

 if($file_img!==null)
        {

            
                $imageName = $file_img->getClientOriginalName();


                $path=public_path("assets/dist/img/User/").$imageName;
              // notation octale : valeur du mode correcte
                if (!file_exists($path)) {
                  $file_img->move(public_path("assets/dist/img/User/"),$imageName);   
                  $user->Img="/offlineneew/public/assets/dist/img/User/".$imageName;           
                }
                else
                {
                  $random_string = md5(microtime());
                  $file_img->move(public_path("assets/dist/img/User/"),$random_string.".jpg");
                  $user->Img="/offlineneew/public/assets/dist/img/User/".$random_string.".jpg";                
                }
              }
          $user->save();
          return redirect('/chartadmin');
    

}
}
