-- phpMyAdmin SQL Dump
-- version 4.6.5.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Aug 07, 2018 at 12:47 AM
-- Server version: 5.5.54
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `newonlinedatabase`
--

-- --------------------------------------------------------

--
-- Table structure for table `expensecatogry`
--

CREATE TABLE `expensecatogry` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `expensecatogry`
--

INSERT INTO `expensecatogry` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'فنادق', NULL, NULL),
(2, 'وجبات', NULL, NULL),
(3, 'زيارات المعمل', NULL, NULL),
(4, 'اخري', NULL, NULL),
(5, 'بروتوكلات', NULL, NULL),
(6, 'مدارس', NULL, NULL),
(7, 'مواصلات', NULL, NULL),
(8, 'تصليح عربيات', NULL, NULL),
(9, 'بنزين', NULL, NULL),
(10, 'اختبارات اسمنت', NULL, NULL),
(11, 'مدارس مقاولين', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `expensecatogry`
--
ALTER TABLE `expensecatogry`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `expensecatogry`
--
ALTER TABLE `expensecatogry`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
