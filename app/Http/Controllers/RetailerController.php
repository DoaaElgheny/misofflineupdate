<?php

namespace App\Http\Controllers;

use Auth;
use App\Http\Requests;
use App\User;
use DB;
use App\Http\Controllers\Controller;
use Request;
use Excel;
use File;
use Illuminate\Support\Facades\Response;
use Validator;
use Illuminate\Support\Facades\Redirect;
use Input;
use Session;
use Geocode;
use Cache;
use URL;
use Carbon;
use View;
use Khill\Lavacharts\Laravel\LavachartsFacade as Lava;
use Khill\Lavacharts\Lavacharts;
use TableView;
use App\Dategps;
use PHPExcel; 
use PHPExcel_IOFactory; 
use App\Government;
use App\District;
use App\Retailer;
use App\Type;
use App\EndType;
use App\SubType;
use App\License;
use App\Location;
class RetailerController extends Controller
{

    public function __construct()
   {
       $this->middleware('auth');
   }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
                    try {

 if (Auth::check())
  {
       $Retailers=Retailer::all();
      return view('retailers.index',compact('Retailers'));
  }
      else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function importretailers()
    {
             try {

 if (Auth::check())
  {


    if(Auth::user()->can('import Contractors Data'))
      {
       
        try
        {
          $temp= Request::get('submit'); 
           if(isset($temp))

         { 
           $filename = Input::file('file')->getClientOriginalName();
             $Dpath = base_path();


             $upload_success =Input::file('file')->move( $Dpath, $filename);

               Excel::load($upload_success, function($reader)
               {   
            $results = $reader->get()->all();

               $i=1;
               foreach ($results as $data)
                {
                
                $Retailer=Retailer::where('Telephone',$data['tel1'])
                ->orwhere('Telephone1',$data['tel1'])
                ->first();

                if($Retailer == null || $Retailer == [])
                {
                    $Type=Type::where('EngName','Distributors')->first();
                    $SubType=SubType::where('EngName',$data['sub_type'])->first();
                    $EndType=EndType::where('EngName',$data['end_type'])->first();
                    

                    $Retailer = New Retailer;
                    $Retailer->Name =$data['name'];
                    $Retailer->EngName=$data['id_name'];
                    $Retailer->Code =$i;
                    $Retailer->Email =$data['email'];
                    $Retailer->BrithDate =$data['birthdate'];
                    $Retailer->Address =$data['address'];
                    $Retailer->Telephone =$data['tel1'];
                    $Retailer->Telephone1 =$data['tel2'];
                    $Retailer->Type_Id  =$Type->id;
                    $Retailer->SubType_Id  =$SubType->id;
                    $Retailer->EndType_Id  =$EndType->id;
                    $Retailer->save();
                }

                $Government=Government::where('Name',$data['gov'])->first();
                $District=District::where('Name',$data['dis'])->first();
            
                $License= New License();
                $License->Name =$data['id_name'];
                $License->License_Number =$data['customer_id'];
                $License->Creation_Date=$data['creation_date'];
                $License->Retailer_Id=$Retailer->id;
                $License->Government_Id =$Government->id;
                $License->District_Id =$District->id;
                $License->save();
                if($data['location'] != "" ||$data['long'] != "" ||$data['lat'] != "")
                {
                $retailers_location=New Location;
                 $retailers_location->Location=$data['location'];
                 $retailers_location->Retailer_Id=$Retailer->id;
                 $retailers_location->Long=$data['long'];
                 $retailers_location->Lat=$data['lat'];
                 $retailers_location->save();
             }
             $i++;
                }
           
            });


            }

             return redirect('/retailers'); 
           
         }
         catch(Exception $e)
         {
             return redirect('/Contractors'); 
         }
         }
    else 
      {
    return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
}
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    
           public function checkRetailerName()
    {
        $id=Input::get('id');
        if($id==null)
        {
            $Name=Input::get('Name');
            $Retailer=Retailer::where('Name','=',$Name)->where('id','!=',$id)->first();
            if($Retailer==[])     
                $isAvailable = true;
            else
                $isAvailable = false;
        }
        else
        {
            $Name=Input::get('Name');
            $Retailer=Retailer::where('Name','=',$Name)->first();
            if($Retailer==[])     
                $isAvailable = true;
            else
                $isAvailable = false;
        }
        return \Response::json(array('valid' =>$isAvailable,));
    }
   public function  editRetailer($id)
   {
        $Retailers=Retailer::find($id);
         
       $types=Type::select('EngName','id')->lists('EngName','id');
       $EndTypese=EndType::select('EngName','id')->lists('EngName','id');
       $SubTypese=SubType::select('EngName','id')->lists('EngName','id');
       return view('retailers.Edit',compact('Retailers','types','EndTypese','SubTypese'));

       

        
   }

          public function checkLicenseNumber()
    {
        $id=Input::get('id');
        if($id==null)
        {
            $License=Input::get('LicenseNumber');
            $Retailer=License::where('License_Number','=',$License)->where('id','!=',$id)->first();
            if($Retailer==[])     
                $isAvailable = true;
            else
                $isAvailable = false;
        }
        else
        {
            $License=Input::get('LicenseNumber');
            $Retailer=License::where('License_Number','=',$License)->first();
            if($Retailer==[])     
                $isAvailable = true;
            else
                $isAvailable = false;
        }
        return \Response::json(array('valid' =>$isAvailable,));
    }
        public function checkRetailerEmail()
    {
        $id=Input::get('id');
        if($id==null)
        {
            $Email=Input::get('Email');
            $Retailer=Retailer::where('Email','=',$Email)->first();
            if($Retailer==[])     
                $isAvailable = true;
            else
                $isAvailable = false;
        }
        else
        {
            $Email=Input::get('Email');
            $Retailer=Retailer::where('Email','=',$Email)->where('id','!=',$id)->first();
            if($Retailer==[])     
                $isAvailable = true;
            else
                $isAvailable = false; 
        }
        return \Response::json(array('valid' =>$isAvailable,));
    }
       public function checkRetailerTele()
    {
        $id=Input::get('id');
        if($id==null)
        {
            $Telephone=Input::get('Telephone');
            $Retailer=Retailer::where('Telephone','=',$Telephone)->first();
            if($Retailer==[])     
                $isAvailable = true;
            else
                $isAvailable = false;
        }
        else
        {
            $Telephone=Input::get('Telephone');
            $Retailer=Retailer::where('Telephone','=',$Telephone)->where('id','!=',$id)->first();
            if($Retailer==[])     
                $isAvailable = true;
            else
                $isAvailable = false;
        }
        return \Response::json(array('valid' =>$isAvailable,));
    }

     public function checCodeRetailers()
    {

        $Code=Input::get('Code');
        $RetailersCode=Retailer::where('Code','=',$Code)->first();
        if($RetailersCode==[])     
            $isAvailable = true;
        else
            $isAvailable = false;
        return \Response::json(array('valid' =>$isAvailable,));
    }
    public function store(Request $request)
    {
       
      
        //MainData 
        $Retailers=New Retailer;
        $Retailers->Name=input::get('Name');
        $Retailers->EngName=input::get('EngName');
        $Retailers->Code=input::get('Code');
        $Retailers->Telephone=input::get('Telephone');
        $Retailers->Type_Id =input::get('Type_Id'); 
        $Retailers->SubType_Id=input::get('SubType_Id');
        $Retailers->EndType_Id =input::get('EndType_Id'); 
        $Retailers->Address=input::get('Address');
        $Retailers->Email=input::get('Email');
        $Retailers->save();
         //User Retailers
        $dt = Carbon::now();
        $Retailers->HasManyUser()->attach(input::get('Promoter'), ['RDate'=>$dt->toDateString()]); 
        //End User Retailers
        // Licence Data 
        $n=count(input::get('LicenseNumber'));
     
        for($i=0;$i<$n-1;$i++)
        {
           
         $retailers_license=New License;
         $retailers_license->Retailer_Id=$Retailers->id;
         $retailers_license->Name=input::get('NameLicence')[$i];
         $retailers_license->License_Number=input::get('LicenseNumber')[$i];
         $retailers_license->Creation_Date=input::get('Creation_Date')[$i];
         $retailers_license->Government_Id=input::get('Government')[$i];
         $retailers_license->District_Id=input::get('District')[$i];
         $retailers_license->save();
        }
       //End Licence Data

        //Retailers Location 
        $Location=count(input::get('Location'));
        for($m=0;$m<$Location-1;$m++)
        {
         $retailers_location=New Location;
         $retailers_location->Location=input::get('Location')[$m];
         $retailers_location->Retailer_Id=$Retailers->id;
         $retailers_location->Long=input::get('Lan')[$m];
         $retailers_location->Lat=input::get('Lat')[$m];
         $retailers_location->save();
        }
        //End Retailers Location
        return redirect('/retailers');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
public function ShowSaveRetailers()
{
     $governments=Government::select('Name','id')->lists('Name','id');
       $District=District::select('Name','id')->lists('Name','id');
       $types=Type::select('EngName','id')->lists('EngName','id');
       $EndTypese=EndType::select('EngName','id')->lists('EngName','id');
       $SubTypese=SubType::select('EngName','id')->lists('EngName','id');
       $promoter=User::where('Type_User','Outdoor Promoters')->select('Name','id')->lists('Name','id');
       return view('retailers.Save',compact('Retailers','governments','promoter','types','EndTypese','SubTypese','District'));
}
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateretailers()
    {
        $id=input::get('id');
        $Retailers=Retailer::find($id);
        $Retailers->Name=input::get('Name');
        $Retailers->EngName=input::get('EngName');
        
        $Retailers->Telephone=input::get('Telephone');
        $Retailers->Type_Id =input::get('Type_Id'); 
        $Retailers->SubType_Id=input::get('SubType_Id');
        $Retailers->EndType_Id =input::get('EndType_Id'); 
        $Retailers->Address=input::get('Address');
        $Retailers->Email=input::get('Email');
        $Retailers->save();
        return redirect('/retailers');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {


 if (Auth::check())
  {
    // if(Auth::user()->can('Delete Retailers'))
    //   {

        $Retailers=Retailer::find($id);
        $Retailers->delete();
        return redirect('/retailers');
 
  // }
  //   else 
  //     {
  //   return view('errors.403');
  //     }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
    }
    
}
