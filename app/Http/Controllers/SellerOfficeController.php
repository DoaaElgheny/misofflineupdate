<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\FrontOffice;
use App\Company;
use App\Product;
use Auth;
use Input;
use Validator;
use DB;
use App\Government;
use App\District;


class SellerOfficeController extends Controller
{ public function __construct()
   {
       $this->middleware('auth');
   }
        public function index(Request $request)
    {
        try {

 if (Auth::check())
  {


    if(Auth::user()->can('show-front_offices'))
      {

     $selleroffice = FrontOffice::all();
     $company =company::lists('name', 'id');
     $product=Product::all()->lists('Cement_Name','id');
       $governments=Government::select('Name','id')->lists('Name','id');
      $districts=District::select('Name','id')->lists('Name','id');
       $companies=Company::select('Name','id')->lists('Name','id');
        return view('selleroffice/index',compact('selleroffice','product','company','cement','governments','districts','companies'));
   

      }
    else 
      {
    return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
 }

public function telephone()
{
 
   
  $Telephone=Input::get('Telephone');
    $res = FrontOffice::select('Telephone')->where('Telephone','=',$Telephone)->get();

   $n=count($res);
  if ($n <= 0)
{
          $isAvailable = true;

}
else
        {
            $isAvailable = false;
        }

          return \Response::json(array('valid' =>$isAvailable));
}




    public function destroy($id)
    {
       try {

 if (Auth::check())
  {


    if(Auth::user()->can('delete-front_offices'))
      {

        $selleroffice = FrontOffice::find($id);
         $selleroffice->delete();
        return redirect('/selleroffice');
 

      }
    else 
      {
    return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
       
        
    }
 public function create()
    {
        //return view('selleroffice.create');
    }

public function store()
 {     

  try {

 if (Auth::check())
  {


    if(Auth::user()->can('create-front_offices'))
      {

         // dd(Input::get('attribute')) ;   
              $selleroffice= new FrontOffice;
       // $cement=Input::get('cement');
       
        $selleroffice->Location =Input::get('Location');
        $selleroffice->Telephone =Input::get('Telephone');
      
         //to get Name of Government
       $Government = Government::where('id', '=',Input::get('Government'))
                    ->select('Name')->first();
        //to get Name of districts               
       $District = District::where('id', '=',Input::get('district'))
                    ->select('Name')->first();
  $selleroffice->Government = $Government->Name;
        
        $selleroffice->District =$District->Name;
        $selleroffice->Address =Input::get('Address');
        $selleroffice->company_id =Input::get('company_id');
        //dd($selleroffice);
        $selleroffice->save();
        $products=Input::get('attribute');
       
        if($products!=null)
        {
          foreach ($products as $value) {
             $selleroffice->getproductsforntoffice()->attach($value);
          }
        }
       // $selleroffice->getsellerofficeproduct()->attach($cement);
     return redirect('/selleroffice'); 

      }
    else 
      {
    return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }

}

   public  function updateSellOffier()
    {
          try {

 if (Auth::check())
  {


    if(Auth::user()->can('edit-front_offices'))
      {

      $selleroffice = Input::get('pk');   
        $column_name = Input::get('name');
        $column_value = Input::get('value'); 
     

    if (Input::get('name')=='District')
        {
          
        $District = District::where('id', '=',Input::get('value'))
                    ->select('Name')->first();
                    $column_value = $District->Name;

         }
    if (Input::get('name')=='Government')
        {
           
            $Government =Government::where('id', '=',Input::get('value'))
                    ->select('Name')->first();
                     $column_value = $Government->Name;

        }
        $userData = FrontOffice::whereId($selleroffice)->first();
        $userData-> $column_name=$column_value;
         if($userData->save())
            return \Response::json(array('status'=>1));
        else 
            return \Response::json(array('status'=>0));
 

      }
    else 
      {
    return \Response::json(array('status'=>'You do not have permission.'));
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
       
    }
    



}