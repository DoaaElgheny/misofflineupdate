<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Input;
use App\Task;
use App\User;

use Excel;
use File;
use Validator;
use DB;
use Auth;
use Symfony\Component\Debug\Exception\FatalThrowableError;
use Datatables;
use Illuminate\View\Middleware\ErrorBinder;
use Exception;
use App\Gps;


class TaskController extends Controller
{ public function __construct()
   {
       $this->middleware('auth');
   }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {  
    try {

 if (Auth::check())
  {


    if(Auth::user()->can('show-task'))
      {

        $tasks = Task::all();          
        return view('/task.index',compact('tasks'));

      }
    else 
      {
    return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
    }
      public function promoterbackcheck(){
    $taskid=Input::get('taskid');
 $data=array();
      $StartDate=Input::get('StartDate');
      $EndDate=Input::get('EndDate');
      $range=array( $StartDate,$EndDate);

      
 $tasks =DB::table('users_tasks')
 ->join('tasks','tasks.id','=','users_tasks.task_id')
->whereBetween('users_tasks.Date',$range) 
->where('tasks.Task_Name','=','Promoters Backcheck') 

->select('users_tasks.task_id')
->groupBy('users_tasks.task_id')
->get();

  $task=[];
for($i=0;$i<count($tasks);$i++) {

 $task[$i] =DB::table('users_tasks')
 ->join('users','users.id','=','users_tasks.users_id')
 ->join('tasks','tasks.id','=','users_tasks.task_id')
->where('users_tasks.task_id','=',$tasks[$i]->task_id) 
->select('users.Username','users_tasks.Achieved','tasks.Task_Name','tasks.Target')->get();

  }   
  

  for($i=0;$i<count($task);$i++) 
  {                
                     $b=[];
                   foreach ($task[$i] as  $value) {
                             
                                    array_push($b,array(
                                      'label' => $value->Username,
                                      'y' => $value->Achieved,
                                      'taskname'=>$value->Task_Name,
                                      'Target'=>$value->Target
                                      ));
     }
$tasktarget =Task::where('tasks.Task_Name','=','Promoters Backcheck') 
->select('tasks.Target')
->groupBy('id')
->get();
// dd($tasktarget);
  $data['promotersbackcheck']=$b;
  $data['promotersbackchecktasktarget']=$tasktarget;

}
     return \Response::json($data,  200, [], JSON_NUMERIC_CHECK);
         

  }      
    public function admintask()
    { 
        try {

 if (Auth::check())
  {


    if(Auth::user()->can('show-assgintask'))
      { 
         if(Auth::user()->can('show-alltask'))
          {

            $tasks =DB::table('users_tasks')
            ->join('users','users.id','=','users_tasks.users_id')
            ->join('tasks','tasks.id','=','users_tasks.task_id')
            ->select('users_tasks.*','tasks.Task_Name','users.Username','tasks.Descraption')->get();
            return view('/task/admintask',compact('tasks'));
          }
          else
          {
            $tasks =DB::table('users_tasks')
            ->join('users','users.id','=','users_tasks.users_id')
            ->join('tasks','tasks.id','=','users_tasks.task_id')
            ->where('users.id','=',Auth::user()->id)
            ->select('users_tasks.*','tasks.Task_Name','users.Username','tasks.Descraption')->get();

            return view('/task/admintask',compact('tasks'));
          }
      }
    else 
      {
    return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }

    }
               public function usertask()
            { 
$userlog=Auth::user()->id;
       
     $tasks =DB::table('users_tasks')
     ->join('users','users.id','=','users_tasks.users_id')
     ->join('tasks','tasks.id','=','users_tasks.task_id')
     ->where('users_tasks.users_id','=',$userlog)
     ->select('users_tasks.*','tasks.Task_Name','tasks.Descraption','users.Username')->get();
    return view('/task/usertask',compact('tasks'));
  
            }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {

 if (Auth::check())
  {


    if(Auth::user()->can('create-assgintask'))
      {
         $user = User::find(Auth::user()->id);
         $role=$user->UserRoles()->first();
         if($role['name']=="superadmin" || $role['name']=="Admin"  )
         {  
          $subuser= User::where('Type_User','=','Office Promoters')->where('id','!=',Auth::user()->id)->where('Supervisor','!=',null)->lists('Username','id');
          $tasks= Task::lists('Task_Name','id');
          return view('task.create',compact('subuser','tasks'));
      }
      else
      {
          $subuser= User::where('Type_User','=','Office Promoters')->where('Supervisor','=',Auth::user()->id)->lists('Username','id');
          $tasks= Task::lists('Task_Name','id');
          return view('task.create',compact('subuser','tasks'));
      }

      }
    else 
      {
    return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }

    }


    public function createtask()
    {
        try {

 if (Auth::check())
  {


    if(Auth::user()->can('create-task'))
      {

       return view('task.createtask');

      }
    else 
      {
    return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }

    }

    public function updatetask()
    {
        try {

 if (Auth::check())
  {


    if(Auth::user()->can('edit-task'))
      {

              $tasks = Input::get('pk');  
        $column_name = Input::get('name');
        $column_value = Input::get('value');
    
        $userData = Task::whereId($tasks)->first();
        $userData-> $column_name=$column_value;

        if($userData->save())

        return \Response::json(array('status'=>1));
    else 
        return \Response::json(array('status'=>0));

      }
    else 
      {
   return \Response::json(array('status'=>'You do not have permission.'));
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
       
    }
    public function updateadmintask()
    {
          // try {

 if (Auth::check())
  {


    if(Auth::user()->can('edit-assgintask'))
      {

          $tasks = Input::get('pk');  
        $column_name = Input::get('name');
        $column_value = Input::get('value');
        $taskst =DB::table('users_tasks')->whereId($tasks)->first();

         $taskd=Task::find($taskst->task_id);

         if($column_name=='Target')
         {   if(Auth::user()->id==$taskst->users_id)

{
   return \Response::json(array('status'=>'You do not have permission.'));
}
else{
  $taskd->getusertask()->detach($taskst->users_id);
            $taskd->getusertask()->attach($taskst->users_id, ["Target" => $column_value,"Status" => $taskst->Status,"Achieved" => $taskst->Achieved,"Date"=>$taskst->Date,"Time"=>$taskst->Time]);
}
            

         }

         if($column_name=='Status')
         {
           if(Auth::user()->id==$taskst->users_id)

{
   return \Response::json(array('status'=>'You do not have permission.'));
}
else{
 $taskd->getusertask()->detach($taskst->users_id);
            $taskd->getusertask()->attach($taskst->users_id, ["Status" => $column_value,"Target" => $taskst->Target,"Achieved" => $taskst->Achieved,"Date"=>$taskst->Date,"Time"=>$taskst->Time]);
}
            

         }
          if($column_name=='Achieved')
         {
           
            $taskd->getusertask()->detach($taskst->users_id);
            $taskd->getusertask()->attach($taskst->users_id, ["Achieved" => $column_value,"Target" => $taskst->Target,"Status" => $taskst->Status,"Date"=>$taskst->Date,"Time"=>$taskst->Time]);

         }


        return \Response::json(array('status'=>1));



      }
    else 
      {
     return \Response::json(array('status'=>'You do not have permission.'));
      }

   }//endAuth

else
{
return redirect('/login');
}

// }//endtry
// catch(Exception $e) 
//     {
//     return redirect('/login');
//     }
    }
      public function updateusertask()
    {
       $tasks = Input::get('pk');  
        $column_name = Input::get('name');
        $column_value = Input::get('value');
        // dd( $tasks, $column_name, $column_value);
        $taskst = DB::table('users_tasks')->where('utid','=',$tasks)->first();

 $taskd=Task::find($taskst->task_id);
  if($column_name=='Achieved')
 {
   
    $taskd->getusertask()->detach();
     $taskd->getusertask()->attach($taskst->users_id, ["Achieved" => $column_value,"Target" => $taskst->Target,"Status" => $taskst->Status,"Date"=>$taskst->Date,"Time"=>$taskst->Time]);

 }

return \Response::json(array('status'=>1));

       
    }
    public function loe(){
    $taskid=Input::get('taskid');
 $data=array();
 $StartDate=Input::get('StartDate');
      $EndDate=Input::get('EndDate');
      $range=array( $StartDate,$EndDate);

  
 $tasks =DB::table('users_tasks')
 ->join('tasks','tasks.id','=','users_tasks.task_id')
->whereBetween('users_tasks.Date',$range) 
->select('users_tasks.task_id')
->where('tasks.Task_Name','=','Loe Sales backcheck') 

->groupBy('users_tasks.task_id')
->get();
 // dd($tasks);

  $task=[];
for($i=0;$i<count($tasks);$i++) {

 $task[$i] =DB::table('users_tasks')
 ->join('users','users.id','=','users_tasks.users_id')
 ->join('tasks','tasks.id','=','users_tasks.task_id')
->where('users_tasks.task_id','=',$tasks[$i]->task_id) 
->select('users.Username','users_tasks.Achieved','tasks.Task_Name','tasks.Target')->get();

  }   
  

  for($i=0;$i<count($task);$i++) 
  {                
                     $b=[];
                   foreach ($task[$i] as  $value) {
                             
                                    array_push($b,array(
                                      'label' => $value->Username,
                                      'y' => $value->Achieved,
                                     
                                      ));
     }
$tasktarget =Task::
where('tasks.Task_Name','=','Loe Sales backcheck') 
->select('tasks.Target')
->groupBy('id')
->get();
// dd($tasktarget);
  $data['loe']=$b;
  $data['loetarget']=$tasktarget;

}
     return \Response::json($data,  200, [], JSON_NUMERIC_CHECK);
         

  }

     public function Name()
    {          $Name[0][0]=Input::get('name');


         $res = Task::where('Task_Name','=',$Name[0][0])
            ->first(); 
 
  if($res==null)     
        $isAvailable = true;
        else
         $isAvailable = false;

    return \Response::json(array('valid' =>$isAvailable,));
}


    public function storetask()
    {

        $taskname=Input::get('name');
        $descrapion=Input::get('descraption'); 
        $target=Input::get('target'); 
         
        for($i=0; $i<count($taskname)-1;$i++)
        {
   
            $tasks = New Task;
            $tasks->Task_Name=$taskname[$i]; 
            $tasks->Descraption=$descrapion[$i]; 
            $tasks->Target=$target[$i]; 
            $tasks->save();

        }
        return redirect('/task');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { 

$name=Input::get('name');
$descraption=Input::get('descraption');
$target=Input::get('target');
 
for($i=0; $i<count($name)-1;$i++)
  {
$today = \Carbon::now()->setTimezone('Africa/Cairo');
$date = $today->toDateString();
  $time=$today->toTimeString() ;
$task = Task::find($descraption[$i]);
$task->getusertask()->attach($name[$i],["Date"=>$date,"Time"=>$time,"Target"=>$target[$i]]);

  

  }

     
          return redirect('/tasks/admintask');

    }

  

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {

 if (Auth::check())
  {


    if(Auth::user()->can('delete-task'))
      {

         $tasks=Task::find($id);
        $tasks->delete();
        return redirect('/task');

      }
    else 
      {
    return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }   
             
    }
    public function charttask()
    {
       try {

 if (Auth::check())
  {


    if(Auth::user()->can('show-charttask'))
      {
          $tasks= Task::lists('Task_Name','id');
          return view('/task/charttask',compact('tasks'));
      }
    else 
      {
  return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
  return redirect('/login');
    }

    }
      public function officetask(){
    $taskid=Input::get('taskid');
 $data=array();
 // $date = date('Y-m-d');
// dd(Input::all());
      $StartDate=Input::get('StartDate');
      $EndDate=Input::get('EndDate');
      $range=array( $StartDate,$EndDate);
 $tasks =DB::table('users_tasks')
 ->join('tasks','tasks.id','=','users_tasks.task_id')
->whereBetween('users_tasks.Date',$range) 
->where('tasks.Task_Name','=','Database Review') 
->select('users_tasks.task_id')
->groupBy('users_tasks.task_id')
->get();
   // dd($tasks);

  $task=[];
for($i=0;$i<count($tasks);$i++) {

 $task[$i] =DB::table('users_tasks')
 ->join('users','users.id','=','users_tasks.users_id')
 ->join('tasks','tasks.id','=','users_tasks.task_id')
->where('users_tasks.task_id','=',$tasks[$i]->task_id) 
->select('users.Username','users_tasks.Achieved','tasks.Task_Name','tasks.Target')->get();

  }   
  

  for($i=0;$i<count($task);$i++) 
  {                
                     $b=[];
                   foreach ($task[$i] as  $value) {
                             
                                    array_push($b,array(
                                      'label' => $value->Username,
                                      'y' => $value->Achieved,
                                      'taskname'=>$value->Task_Name,
                                      'Target'=>$value->Target
                                      ));
     }
$tasktarget =Task::
where('tasks.Task_Name','=','Database Review') 
->select('tasks.Target')

->get();
// dd($tasktarget);
  $data['task']=$b;
  $data['tasktarget']=$tasktarget;
 // dd($data);
}
     return \Response::json($data,  200, [], JSON_NUMERIC_CHECK);
         

  }
  public function achieved(){
  
 $data=array();
 $StartDate=Input::get('StartDate');
      $EndDate=Input::get('EndDate');
      $range=array( $StartDate,$EndDate);

      
 $tasksBcheck =DB::table('users_tasks')
 ->join('tasks','tasks.id','=','users_tasks.task_id')
 ->join('users','users.id','=','users_tasks.users_id')
->whereBetween('users_tasks.Date',$range) 
->where('tasks.Task_Name','=','Promoters Backcheck') 
->select('users_tasks.*','tasks.*','users.*')
->groupBy('users_tasks.users_id')
->get();
  //dd($tasksBcheck);



                   $b=[];
                   foreach ($tasksBcheck as  $value) {
                            $presntage= number_format(($value->Achieved/$value->Target*100),0);
                            // dd($presntage);
                                    array_push($b,array(
                                      'label' => $value->Username,
                                      'y' => $presntage
                                     
                                      ));
                                }



$tasksDR =DB::table('users_tasks')
 ->join('tasks','tasks.id','=','users_tasks.task_id')
 ->join('users','users.id','=','users_tasks.users_id')
->whereBetween('users_tasks.Date',$range) 
->where('tasks.Task_Name','=','Database Review') 
->select('users_tasks.*','tasks.*','users.*')
->groupBy('users_tasks.users_id')
->get();
 // dd($tasks);



                   $c=[];
                   foreach ($tasksDR as  $value) {
                          $presntage=  number_format(($value->Achieved/$value->Target*100),0);
                             // dd($presntage);
                                    array_push($c,array(
                                      'label' => $value->Username,
                                      'y' => $presntage
                                     
                                      ));
                                }








$tasksPonits =DB::table('users_tasks')
 ->join('tasks','tasks.id','=','users_tasks.task_id')
 ->join('users','users.id','=','users_tasks.users_id')
->whereBetween('users_tasks.Date',$range) 
->where('tasks.Task_Name','=','Contractors Program Points') 
->select('users_tasks.*','tasks.*','users.*')
->groupBy('users_tasks.users_id')
->get();
 // dd($tasks);



                   $d=[];
                   foreach ($tasksPonits as  $value) {
                           $presntage= number_format(($value->Achieved/$value->Target*100),0);
                             // dd($presntage);
                                    array_push($d,array(
                                      'label' => $value->Username,
                                      'y' => $presntage
                                     
                                      ));
                                }




    
  $data['tasksBcheck']=$b;
  $data['tasksDR']=$c;
 $data['tasksPonits']=$d;
// dd( $data);
     return \Response::json($data,  200, [], JSON_NUMERIC_CHECK);
         

  }
    public function videomakers(){
    $taskid=Input::get('taskid');
 $data=array();
$StartDate=Input::get('StartDate');
      $EndDate=Input::get('EndDate');
      $range=array( $StartDate,$EndDate);


      
 $tasks =DB::table('users_tasks')
 ->join('tasks','tasks.id','=','users_tasks.task_id')
->whereBetween('users_tasks.Date',$range) 
->select('users_tasks.task_id')
->where('tasks.Task_Name','=','Video Makers') 

->groupBy('users_tasks.task_id')
->get();


  $task=[];
for($i=0;$i<count($tasks);$i++) {

 $task[$i] =DB::table('users_tasks')
 ->join('users','users.id','=','users_tasks.users_id')
 ->join('tasks','tasks.id','=','users_tasks.task_id')
->where('users_tasks.task_id','=',$tasks[$i]->task_id) 
->select('users.Username','users_tasks.Achieved','tasks.Task_Name','tasks.Target')->get();

  }   
  

  for($i=0;$i<count($task);$i++) 
  {                
                     $b=[];
                   foreach ($task[$i] as  $value) {
                             
                                    array_push($b,array(
                                      'label' => $value->Username,
                                      'y' => $value->Achieved,
                                     
                                      ));
     }
$tasktarget =Task::where('tasks.Task_Name','=','Video Makers') 
->select('tasks.Target')
->groupBy('id')
->get();
  $data['videomakers']=$b;
  $data['videomakerstarget']=$tasktarget;

}
     return \Response::json($data,  200, [], JSON_NUMERIC_CHECK);
         

  }

}
