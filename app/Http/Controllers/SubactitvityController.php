<?php

namespace App\Http\Controllers;

use Auth;
use App\Http\Requests;
use DB;
use App\Http\Controllers\Controller;
use Request;
use Excel;
use File;
use Illuminate\Support\Facades\Response;
use Validator;
use Illuminate\Support\Facades\Redirect;
use Input;
use Session;
use Geocode;
use Cache;
use URL;
use View;
use Khill\Lavacharts\Laravel\LavachartsFacade as Lava;
use Khill\Lavacharts\Lavacharts;
use TableView;
use App\Dategps;
use PHPExcel; 
use PHPExcel_IOFactory; 
use App\MarketingActivity;
use App\SubActivity;
use App\Attribute;
use App\ActivityItem;
class SubactitvityController extends Controller
{
  public function __construct()
   {
       $this->middleware('auth');
   }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function index()
    // {
    //     $subactitvities = Subactitvity::all();
    //     $MarktingActivity = MarketingActivity::all()->lists('Name','id');
                
    //     return view('subactitvities.index',compact('subactitvities','MarktingActivity'));   
    // }
 public function checkshaza()
 {
              try {

 if (Auth::check())
  {
      $subactname=Input::get('subactivityname');
      $subactivity = SubActivity::where('Name','=',$subactname)->first();
     if($subactivity==[])     
        $isAvailable = true;
        else
         $isAvailable = false;

    return \Response::json(array('valid' =>$isAvailable,));
       }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
 }
 
  public function checkenglishnamesubactivity()
 {
              try {

 if (Auth::check())
  {
      $subactname=Input::get('subactivityenglishname');
      $subactivity = SubActivity::where('EngName','=',$subactname)->first();
     if($subactivity==[])     
        $isAvailable = true;
        else
         $isAvailable = false;

    return \Response::json(array('valid' =>$isAvailable,));
       }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
 }
    public function showsubactivityofmarket($id)
    {
          try {

 if (Auth::check())
  {


    if(Auth::user()->can('show-sub_activities'))
      {


      Session::put('marketingactivity_id', $id);
        $activity = MarketingActivity::whereId($id)->lists('Name','id');
        $attribute= Attribute::where('Name','!=','Start Date')->where('Name','!=','End Date')->lists('Name','id');
        $subactitvities = SubActivity::where('marketingactivity_id','=',$id)->get();
        $MarktingActivity = MarketingActivity::all()->lists('Name','id');        
        return view('subactitvities.index',compact('subactitvities','MarktingActivity','activity','attribute'));   
   

      }
    else 
      {
    return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
        
         }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

  try {

 if (Auth::check())
  {


    if(Auth::user()->can('create-sub_activities'))
      {

        $subactivity = New SubActivity();
        $subactivity->Name=Input::get('subactivityname');
        $subactivity->EngName=Input::get('subactivityenglishname');
        $subactivity->marketingactivity_id=Input::get('activity');
        $subactivity->save();
    
        $activity = New ActivityItem();
        $activity->Code=substr($subactivity->Name,0,4).str_random(6);
        $activity->subactivity_id=$subactivity->id;
        $activity->save();
        $startend= Attribute::where('Name','=','Start Date')->orWhere('Name','=','End Date')->lists('Name','id');
        foreach ($startend as $key => $value) {
           $activity->getactivityitemsattribute()->attach($key);
        }
        $attribute=Input::get('attribute');
         
        for($i=0; $i<count($attribute);$i++)
        {
            $activity->getactivityitemsattribute()->attach($attribute[$i]);
        }
        $marketid=Session::get('marketingactivity_id');
        return redirect('/subacivity/'.$marketid);

      }
    else 
      {
    return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
      
      
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }
    public function updateSubactivity()
    {

  try {

 if (Auth::check())
  {


    if(Auth::user()->can('edit-sub_activities'))
      {

      
        $Subactivity_id= Input::get('pk');  
        $column_name = Input::get('name');
        $column_value = Input::get('value');
    
        $gpsData = SubActivity::whereId($Subactivity_id)->first();
        $gpsData-> $column_name=$column_value;

        if($gpsData->save())

            return \Response::json(array('status'=>1));
        else 
            return \Response::json(array('status'=>0));

      }
    else 
      {
   return \Response::json(array('status'=>'You do not have permission.'));
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }


    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
          try {

 if (Auth::check())
  {


    if(Auth::user()->can('delete-sub_activities'))
      {

       $marketid=Session::get('marketingactivity_id');
        $tasks=SubActivity::find($id);
        $tasks->delete();    
        return redirect('/subacivity/'.$marketid);

      }
    else 
      {
    return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
      
    }
}
