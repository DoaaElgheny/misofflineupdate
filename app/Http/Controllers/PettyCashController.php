<?php
namespace App\Http\Controllers;
use Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

use App\Http\Requests;
use Session;
use App\Company;
use App\Activity;
use Image;
use Excel;
use DB;
use PHPExcel; 
use PHPExcel_IOFactory;
use PHPExcel_Worksheet_Drawing;
use PHPExcel_Worksheet_MemoryDrawing;
use App\User;
use App\Expense;
use App\ExpensesIN;
use App\ExpenseCatogry;
use App\dashboardcheck;
use App\safetransaction;
use Auth;

class PettyCashController extends Controller
{
	 //  * Display a listing of the resource.
  //   *
   // @return \Illuminate\Http\Response
   //  */
public function Pettycashdelete($id)
{
    $Expense=Expense::find($id);
             $Expense->delete();
             return redirect('/PettyCash');

}
public function safetransaction()
{

   $safetransaction=safetransaction::all();

  return view("PettyCash.safetransaction",compact('safetransaction'));
}
public function storeDatsafetrans(Request $request)
  {
    $expensedata=new safetransaction();
    

  $expensedata->Datetrans=Input::get('Date');     
      
       $expensedata->Notes=Input::get('Note');
   $expensedata->Balance=Input::get('PNumer');

       //End Image 
       $expensedata->save();


return redirect('/safetransaction');

    
  }
  public function Deletesafe($id)
{

  $Expense=safetransaction::find($id);
           $Expense->delete();
           return redirect('/safetransaction');
    
  
}
public function Pettycashsidebar()
{

  return view("PettyCash.MenusidebarPettyCash");
}
public function PettycashdeleteIN($id)
{
    $Expense=ExpensesIN::find($id);
             $Expense->delete();
             return redirect('/Delivery');

}
public function Delivery()
{

  $users =User::where('Type_User','Office Promoters')
        ->lists("Name","id");
        $ExpensesIN=ExpensesIN::join('users','users.id','=','expensesin.user_ID')->select('*','users.Name','expensesin.id as Expid')
        ->get();
  return view("PettyCash.Delivery",compact('users','ExpensesIN'));

}
	public function ShowPetty()

	{


$Start=\Carbon::now()->toDateString();
$mon=explode("-", $Start);
$number = cal_days_in_month(CAL_GREGORIAN,$mon[1] ,$mon[0]);



$dtst =$mon[0]."-".$mon[1]."-01";
$dten =$mon[0]."-".$mon[1]."-".$number;


 $range=array($dtst,$dten);
	$ExpenseCat=ExpenseCatogry::lists('Name','id');

 if(Auth::user()->can('permissionPettycash'))
        {
          $totaladmin=Expense::select(DB::raw('sum(Cost)  as total'))
                        ->first();
                    
          $Expense=Expense::all();

        }
        else
        {
           $totaladmin=Expense::where('user_ID',Auth::user()->id)
                      // ->whereBetween('ExpDate',$range)
                       ->select(DB::raw('sum(Cost)  as total'))
                        ->first();
                         
$Expense=Expense::where('user_ID',Auth::user()->id)->get();
        }

        $total=Expense::where('user_ID',Auth::user()->id)
                       ->whereBetween('ExpDate',$range)
                       ->select(DB::raw('sum(Cost)  as total'))
                        ->first();
	
		return view("PettyCash.index",compact('ExpenseCat','Expense','total','totaladmin'));
}
public function ChartPettyCash()
{
  $Start_Date=input::get('Start_Date');$End_Date=input::get('End_Date');
  $range=array($Start_Date,$End_Date);
  if(input::get('Cat_iD')==null)
  {
 $total=Expense::join('expensecatogry','expensecatogry.id','=','expenses.ExpenseCatID')
                 ->whereBetween('ExpDate',$range)
                  ->select(DB::raw('sum(Cost)  as total'),DB::raw('expensecatogry.Name as ExpCat'))
                  ->groupby('expenses.ExpenseCatID')
                 ->get();

  }
  else
  {
 $total=Expense::join('expensecatogry','expensecatogry.id','=','expenses.ExpenseCatID')
                 ->whereBetween('ExpDate',$range)
                 ->where('expenses.ExpenseCatID',input::get('Cat_iD'))
                 ->select(DB::raw('sum(Cost)  as total'),DB::raw('expensecatogry.Name as ExpCat'))
                  ->groupby('expenses.ExpenseCatID')
                 ->get();
  }
 $c=[];
            
           foreach ($total as  $value) {

          
            array_push($c,array('label' =>$value->ExpCat,'y' => $value->total));
          
           }
              $data = array();
$data['Items']=$c;

           return Response::json($data,  200, [], JSON_NUMERIC_CHECK);



}
//Betty Cash 


public function totalPetty()
{
 


 
  $arrayUser=[];
  $Allusers=ExpensesIN::select('user_ID')->distinct()
                 ->get();


  foreach ($Allusers as  $value) {

$totalOut=Expense::where('user_ID',$value->user_ID)
                  ->select(DB::raw('sum(Cost)  as total'))
                 ->first();
$totalIN=ExpensesIN::where('user_ID',$value->user_ID)
                  ->select(DB::raw('sum(Cost)  as total'))
                 ->first();

                  $total=$totalOut->total-$totalIN->total;
$userName=User::where('id',$value->user_ID)->select('*')->first();
if($total !=0){
   array_push($arrayUser,array('Name' =>$userName->Name,'total' => $total,'Userid'=>$userName->id));


    }
   }
 
  return view("PettyCash.Final",compact('arrayUser'));
//}
}

public function CaculateFinal($id,$total)
{
//increase Safe 
$expensedata=new safetransaction();
$expensedata->Datetrans=\Carbon::now()->toDateString();;     
    $userName=User::where('id',$id)->select('*')->first();
     $expensedata->Notes="تسوية من ".$userName->Name;
 // $expensedata->Balance=-($total);
 //     $expensedata->save();
     //New need git 
     $totalIN=ExpensesIN::where('user_ID',$id)                
              ->first();
              $Finaltotal=$totalIN->Cost+$total;



$updatetotal=DB::table('expensesin')->where('user_ID',$id)
->where('ExpDate',$totalIN->ExpDate)
->update(['Cost'=>$Finaltotal]);


}

public function ChartPettyCashUser()
{
  $Start_Date=input::get('Start_Date2');$End_Date=input::get('End_Date2');
  $range=array($Start_Date,$End_Date);

  if(input::get('User_iD')==null)
  {

 $total=Expense::join('users','users.id','=','expenses.user_ID')
                 ->whereBetween('ExpDate',$range)
                  ->select(DB::raw('sum(Cost)  as total'),DB::raw('users.Name as Name'))
                  ->groupby('expenses.user_ID')
                 ->get();
             

  }
  else
  {

    
 $total=Expense::join('expensecatogry','expensecatogry.id','=','expenses.ExpenseCatID')
                 ->whereBetween('ExpDate',$range)
               ->where('expenses.user_ID','=',input::get('User_iD'))
                
                 ->select(DB::raw('sum(Cost)  as total'),DB::raw('expensecatogry.Name as Name'))
                  ->groupby('expenses.ExpenseCatID')
                 ->get();
  }
 $c=[];
            
           foreach ($total as  $value) {

          
            array_push($c,array('label' =>$value->Name,'y' => $value->total));
          
           }
              $data = array();
$data['Items']=$c;

           return Response::json($data,  200, [], JSON_NUMERIC_CHECK);
}
public function showChartPetty()

{
$ExpenseCat=ExpenseCatogry::lists('Name','id');
 $Users=User::where('Type_User','Office Promoters')
        ->lists("Name","id");
    return view("PettyCash.ReportPettycashChart",compact('ExpenseCat','Users'));
  
}
public function ShowReportPetty()
{
  $User=User::where('Type_User','Office Promoters')
        ->lists("Name","id");
    return view("PettyCash.Report",compact('User'));
}
public function ReportPettyCash(request $request)
{

if(Input::get('User_ID') !=null)
{
$Data=Expense::join('users','users.id','=','expenses.user_ID')->where('user_ID',Input::get('User_ID'))
->select('*','users.Name as UserName')
->get();
}
else
{
$Data=Expense::join('users','users.id','=','expenses.user_ID')
->select('*','users.Name as UserName')
->get();
}

return \Response::json($Data);

}
public function importBackcheck()
  {

    $temp= Request::get('submit'); 
    if(isset($temp))

      { 

         $filename = Input::file('file')->getClientOriginalName();
         $Dpath = base_path();
         $upload_success =Input::file('file')->move( $Dpath, $filename);
     Excel::load($upload_success, function($reader)
     {   
 $results = $reader->get()->all();

     foreach ($results as $data)
      { 
        $Datetime=\Carbon::now()->toDateString();

          $selectdash=dashboardcheck::where('Name',$data["name"])
                      ->where('Date',$Datetime)->first();

        if($selectdash==null){
            $dashboardcheck=new dashboardcheck();
        }
        else{
            $dashboardcheck=dashboardcheck::find($selectdash->id);
        }
        $dashboardcheck->Name=$data["name"];
        $dashboardcheck->updated=$data["updated"];
        $dashboardcheck->Date=$Datetime;
        $dashboardcheck->Closed=$data["closed"];
        $dashboardcheck->Wrong=$data["wrong"];
        $dashboardcheck->refused=$data["refused"];
        $dashboardcheck->Stop=$data["stop"];
        $dashboardcheck->Total=$data["total"];
        $dashboardcheck->Performance=$data["performance"];
        $dashboardcheck->save();   

      }

    });


}
     return redirect('/showaadminpanel');  
    

  }
  public function storeDataIN(Request $request)
  {
    $expensedata=new ExpensesIN();
    


 $expensedata->user_ID=Input::get('users'); 
  $expensedata->ExpDate=Input::get('Date');     
      
       $expensedata->Notes=Input::get('Note');
   $expensedata->Cost=Input::get('PNumer');
   
   if(Input::file('pic') !=null)
   {
      $file_img = Input::file('pic');
       



     ////////////Save Image//////////////////
           // $file_img = Request::file('pic');
       
            $imageName = $file_img->getClientOriginalName();


                $path=public_path("assets/dist/img/Purchase").$imageName;
              // notation octale : valeur du mode correcte
                  

                if (!file_exists($path)) {
                  $file_img->move(public_path("assets/dist/img/Purchase/"),$imageName);
                  $expensedata->ImgRecipt  ="/MIS/assets/dist/img/Purchase/".$imageName; 
                }
                else
                {
                   $random_string = md5(microtime());
                   $file_img->move(public_path("assets/dist/img/Purchase/"),$random_string.".jpg");
                   
                  $expensedata->ImgRecipt ="/MIS/assets/dist/img/Purchase/".$random_string.".jpg";
                
                }
   }
     


   
       //End Image 
       $expensedata->save();


return redirect('/Delivery');

    
  }
	  public function storeData(Request $request)
    {
        
        $expensedata=new Expense();
    


 $expensedata->user_ID=Auth::user()->id;     
       $expensedata->ExpenseCatID=Input::get('ExpenseCat');
       $Expcat=ExpenseCatogry::where('id',Input::get('ExpenseCat'))->first();
      
          $expensedata->ExpDate=Input::get('Date');
          if(Input::get('Note')==null)
            {
$expensedata->Notes=$Expcat->name;
            }
       $expensedata->Notes=Input::get('Note');
   $expensedata->Cost=Input::get('PNumer');
   
   if(Input::file('pic') !=null)
   {
   	  $file_img = Input::file('pic');
       



     ////////////Save Image//////////////////
           // $file_img = Request::file('pic');
       
            $imageName = $file_img->getClientOriginalName();


                $path=public_path("assets/dist/img/Purchase").$imageName;
              // notation octale : valeur du mode correcte
                  

                if (!file_exists($path)) {
                  $file_img->move(public_path("assets/dist/img/Purchase/"),$imageName);
                  $expensedata->ImgRecipt  ="/MIS/assets/dist/img/Purchase/".$imageName; 
                }
                else
                {
                   $random_string = md5(microtime());
                   $file_img->move(public_path("assets/dist/img/Purchase/"),$random_string.".jpg");
                   
                  $expensedata->ImgRecipt ="/MIS/assets/dist/img/Purchase/".$random_string.".jpg";
                
                }
   }
     


   
       //End Image 
       $expensedata->save();


return redirect('/PettyCash');

    }
}

