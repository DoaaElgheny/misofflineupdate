<?php

namespace App\Http\Controllers;

use Auth;
use App\Http\Requests;
use DB;
use App\Http\Controllers\Controller;
use Request;
use Excel;
use File;
use Illuminate\Support\Facades\Response;
use Validator;
use Illuminate\Support\Facades\Redirect;
use Input;
use Session;
use Geocode;
use Cache;
use URL;
use View;
use Khill\Lavacharts\Laravel\LavachartsFacade as Lava;
use Khill\Lavacharts\Lavacharts;
use TableView;
use App\Dategps;
use PHPExcel; 
use PHPExcel_IOFactory; 
use App\Government;
use App\District;

class DistrictController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
   {
       $this->middleware('auth');
   }
    public function index()
    {
         $showadditem=1;
         Session::put('Government_id',null);
         $districts = District::all();
          $governments=Government::select('Name','id')->lists('Name','id');
          $goverEngnments=Government::select('EngName','id')->lists('EngName','id');
        return view('Districts.index',compact('districts','governments','goverEngnments','showadditem'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
       public function checknameDistric()
    {
         $districtsname=Input::get('Name');
      $districts = District::where('districts.Name','=',$districtsname)
     ->select('*')->first();
     if($districts==null)     
        $isAvailable = true;
        else
         $isAvailable = false;

    return \Response::json(array('valid' =>$isAvailable,));
    }
  public function checkEngNameDistric()
    {
         $Districname=Input::get('EngName');
      $districts = District::where('districts.EngName','=',$Districname)
     ->select('*')->first();
     if($districts==null)     
        $isAvailable = true;
        else
         $isAvailable = false;

    return \Response::json(array('valid' =>$isAvailable,));
    }
    
    public function store(Request $request)
    {
          try {

 if (Auth::check())
  {


    if(Auth::user()->can('store Districts'))
      {

        $districtname=Input::get('Name');
        $districtEngname=Input::get('EngName');
        $governmentname=Input::get('governments');
        $districts = New District;
        $districts->Name=$districtname;
         $districts->EngName=$districtEngname;
        $districts->government_id =$governmentname;  
        

         $districts->save();
             return redirect('/Districts');
                 }
    else 
      {
    return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }
    public function updateDistrict()
    {
    try {

 if (Auth::check())
  {


    if(Auth::user()->can('update District'))
      {


    $district_id= Input::get('pk');  
        $column_name = Input::get('name');
        $column_value = Input::get('value');
    
        $districts = District::whereId($district_id)->first();
        $districts-> $column_name=$column_value;

        if($districts->save())

        return \Response::json(array('status'=>1));
    else 
        return \Response::json(array('status'=>0));
     }
    else 
      {
     return \Response::json(array('status'=>'You do not have permission.'));
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }

    }
 public function showallDistriGovernment($id)
    {
        
try {

    if (Auth::check())
       {
    if(Auth::user()->can('show all Distric Government'))
      {
         Session::put('Government_id', $id);
        $showadditem=0;
        $districts = District::where('government_id','=',$id)->get(); 
         $governments=Government::select('Name','id')->lists('Name','id');
          $goverEngnments=Government::select('EngName','id')->lists('EngName','id');  
        return view('Districts.index',compact('districts','governments','goverEngnments','showadditem'));
           }
    else 
      {
    return view('errors.403');
      }

   }
   //endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

  try {

 if (Auth::check())
  {


    if(Auth::user()->can('Delete Districts'))
      {
        $Districts=District::find($id);
        $Districts->delete();
        
       $marketid=Session::get('Government_id');
       if($marketid==null)
        return redirect('/Districts');
        else
        return redirect('/showallDistriGovernment/'.$marketid);
      }
    else 
      {
    return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
    }
}
