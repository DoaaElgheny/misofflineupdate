<?php

namespace App\Http\Controllers;
use Auth;
use App\Http\Requests;
use App\User;
use DB;
use App\Http\Controllers\Controller;
use Request;
use Excel;
use File;
use Illuminate\Support\Facades\Response;
use Validator;
use Illuminate\Support\Facades\Redirect;
use Input;
use \PDF;
use App\Contractor;
use App\RetailersReview;
use App\Retailer;
use App\Product;
use App\Kids;
class RetailerReviewController extends Controller
{public function __construct()
   {
       $this->middleware('auth');
   }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }
public function showupdateretailers($id)
{

  $RetailersReview=RetailersReview::find($id);
       
       return view('RetailerReview.EditRetailers',compact('RetailersReview'));
}



public function RetailersReview()
{
  $RetailersReview=RetailersReview::all();
 return view ('RetailerReview.RetailersReview',compact('RetailersReview'));
}
public function ImportReviewRet()
{

     try {

 // if (Auth::check())
 //  {


 //    if(Auth::user()->can('import Review Data'))
 //      {

        try
        {
  $temp= Request::get('submit'); 
   if(isset($temp))

 { 

   $filename = Input::file('file')->getClientOriginalName();
     $Dpath = base_path();


     $upload_success =Input::file('file')->move( $Dpath, $filename);

       Excel::load($upload_success, function($reader)
       {  

    $results = $reader->get()->all();


       foreach ($results as $data)
        {

   
        $Retailersid=Retailer::where('Code', '=',$data['code'])
        ->select('id')->first();
     
     $ExsistsID=RetailersReview::where('Retailer_Id',$Retailersid->id)->first();
     
     if($ExsistsID==Null)
     {

        $RetailersReview =new RetailersReview();
        $RetailersReview->Has_Truncks =$data['has_trunks'];
        $RetailersReview->Workers =$data['workers'];
        $RetailersReview->No_Truncks =$data['no_trunks'];
        $RetailersReview->Has_Used_Computer =$data['has_computeryes_no'];
        $RetailersReview->Equipment =$data['equipment'];
        $RetailersReview->Status =$data['status'];
        $RetailersReview->Call_Status =$data['call_status'];
        $RetailersReview->Area =$data['area'];
        // $RetailersReview->Cont_Type=$data['Cont_Type'];
        $RetailersReview->Retailer_Id =$Retailersid->id;
        $RetailersReview->save();

        //Kids 
       for ($i=1; $i <11; $i++) { 

        $kids= new Kids();
        if($data['kid'.$i.'_name'] !=Null)
        {
        $kids->Name=$data['kid'.$i.'_name'];
        $kids->Age=$data['kid'.$i.'_ageno_only'];
        $kids->Retailer_Id=$Retailersid->id;
        $kids->save();
        }
       
       }

       //Products add For Retailers

      //Fanar
if($data['fanar_usage']=='yes')
{
      $Product=Product::where('Code',288)->first();
           
      $Product->HasProductRet()->attach($Retailersid->id); 
}
if($data['moqawem_usage']=='yes')
{
      $Product=Product::where('Code',99)->first();
           
      $Product->HasProductRet()->attach($Retailersid->id); 
}
if($data['mohandes_usage']=='yes')
{
      $Product=Product::where('Code',286)->first();
           
      $Product->HasProductRet()->attach($Retailersid->id); 
}
if($data['saeed_usage']=='yes')
{
      $Product=Product::where('Code',287)->first();
           
      $Product->HasProductRet()->attach($Retailersid->id); 
}
if($data['fahd_usage']=='yes')
{
      $Product=Product::where('Code',284)->first();
           
      $Product->HasProductRet()->attach($Retailersid->id); 
}
if($data['opc_usage']=='yes')
{
      $Product=Product::where('Code',285)->first();
           
      $Product->HasProductRet()->attach($Retailersid->id); 
}
         

      //Fahd

//opc

//Saeed

//Mohands 

// Moquem

    }
     else
     {

        $RetailersReview =RetailersReview::where('Retailer_Id',$Retailersid->id)->first();
// dd($RetailersReview);
        $RetailersReview->Has_Truncks =$data['has_trunks'];
        $RetailersReview->Workers =$data['workers'];
        $RetailersReview->No_Truncks =$data['no_trunks'];
        $RetailersReview->Has_Used_Computer =$data['has_computeryes_no'];
        $RetailersReview->Equipment =$data['equipment'];
        $RetailersReview->Status =$data['status'];
        $RetailersReview->Call_Status =$data['call_status'];
        $RetailersReview->Area =$data['area'];
        // $RetailersReview->Cont_Type=$data['Cont_Type'];
        $RetailersReview->Retailer_Id =$Retailersid->id;
        $RetailersReview->save();
      
//Delete Kids and store it gdid
       $KidsDelete=DB::table('kids_retailers')
      ->where('Retailer_Id',$Retailersid->id)
      ->delete();

        for ($i=1; $i <11; $i++) { 
        $kids= new Kids();
        if($data['kid'.$i.'_name'] !=Null)
        {
        $kids->Name=$data['kid'.$i.'_name'];
        $kids->Age=$data['kid'.$i.'_ageno_only'];
        $kids->Retailer_Id=$Retailersid->id;
        $kids->save();
        }
       
       }
      
       $ProDelete=DB::table('products_retailers')
      ->where('Retailer_Id',$Retailersid->id)
      ->delete();
if($data['fanar_usage']=='yes')
{
      $Product=Product::where('Code',288)->first();
           
      $Product->HasProductRet()->attach($Retailersid->id); 
}
if($data['moqawem_usage']=='yes')
{
      $Product=Product::where('Code',99)->first();
           
      $Product->HasProductRet()->attach($Retailersid->id); 
}
if($data['mohandes_usage']=='yes')
{
      $Product=Product::where('Code',286)->first();
           
      $Product->HasProductRet()->attach($Retailersid->id); 
}
if($data['saeed_usage']=='yes')
{
      $Product=Product::where('Code',287)->first();
           
      $Product->HasProductRet()->attach($Retailersid->id); 
}
if($data['fahd_usage']=='yes')
{
      $Product=Product::where('Code',284)->first();
           
      $Product->HasProductRet()->attach($Retailersid->id); 
}
if($data['opc_usage']=='yes')
{
      $Product=Product::where('Code',285)->first();
           
      $Product->HasProductRet()->attach($Retailersid->id); 
}

        }
     }
       
   
    });


    }

     return redirect('/RetailersReview'); 
   
 }
 catch(Exception $e)
 {
     return redirect('/RetailersReview'); 
 }
 // }
 //    else 
 //      {
 //   return view('errors.403');
 //      }

//    }//endAuth

// else
// {
// return redirect('/login');
// }

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
}


   public function ShowKids($id)
        {

            $Kids=DB::table('kids_retailers')
                        ->where('Retailer_Id', '=',$id)
                       ->select('Name','Age','id')->get();
                       $Retailer_Id=$id;
                       return view('RetailerReview.KidsRetailers',compact('Kids','Retailer_Id'));
        }
         public function ShowProduct($id)
        {
          $Retailer_Id=$id;
           $products=DB::table('products_retailers')
           ->join('products','products.id','=','products_retailers.Product_Id')
          ->where('Retailer_Id', '=',$id)
         ->select('Quantity','products.Cement_Name','products_retailers.id')->get();
         $Cement_Name=Product::lists('Cement_Name','id');
         return view('RetailerReview.ProductsRetailers',compact('products','Cement_Name','Retailer_Id'));
         
        }
        public function updateKidsRetailers()
        {
            $Kids_id= Input::get('pk');

        $column_name = Input::get('name');
        $column_value = Input::get('value');
         $KidsData=DB::table('kids_retailers')
          ->where('id',$Kids_id)
          ->update([$column_name =>$column_value]);
          return \Response::json(array('status'=>1));
        
        }
        public function CreateKidsRetailers()
        {
          try
          {
            $retid=Input::get('Retailer_Id');
            $Kid = New Kids();
            $Kid->Name=Input::get('Name');
            $Kid->Age=Input::get('Age');
            $Kid->Retailer_Id=$retid;
            $Kid->save();
            return redirect('/ReviewRet/ShowKids/'.$retid);
          }
          catch(Exception $e) 
          {
            return redirect('/login');
          }

        }
        public function CreateProductRetailers()
        {
          try
          {
            $retid=Input::get('Retailer_Id');
            $Retailer = Retailer::find($retid);
            $Retailer->ProductRet()->attach(Input::get('Product'), ["Quantity" =>Input::get('Quentity')]);
            return redirect('/ReviewRet/ShowProduct/'.$retid);
          }
          catch(Exception $e) 
          {
            return redirect('/login');
          }

        }
        
          public function updateProductRetailers()
        {
            $Kids_id= Input::get('pk');

        $column_name = Input::get('name');
        $column_value = Input::get('value');
         $KidsData=DB::table('products_retailers')
          ->where('id',$Kids_id)
          ->update([$column_name =>$column_value]);
          return \Response::json(array('status'=>1));
        
        }
  public function DeleteKidsRetailers($id)
        {
            
         $KidsData=DB::table('kids_retailers')
          ->delete(['id' =>$id]);
          return redirect('/RetailersReview');
        
        }
          public function DeleteProductRetailers($id)
        {
            $productsData=DB::table('products_retailers')
          ->delete(['id' =>$id]);
          return redirect('/RetailersReview');
        
        }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       
               try {

        
            $RetailersReview=RetailersReview::find($id);
            $RetailersReview->Has_Truncks=Input::get('Has_Truncks');
            $RetailersReview->Workers=Input::get('Workers');   
            $RetailersReview->No_Truncks=Input::get('No_Truncks'); 
            $RetailersReview->Has_Used_Computer=Input::get('Has_Used_Computer');
            $RetailersReview->Equipment=Input::get('Equipment');  
            $RetailersReview->Status=Input::get('Status');
            $RetailersReview->Call_Status=Input::get('Call_Status');       
            $RetailersReview->Area=Input::get('Area');
            $RetailersReview->Cont_Type=Input::get('Cont_Type');
            $RetailersReview->save();              
            return redirect('/RetailersReview');


}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
        $RetailersReview=RetailersReview::find($id);
        $RetailersReview->delete();
        return redirect('/RetailersReview');

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
    }
}
