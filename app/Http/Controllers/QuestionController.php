<?php

namespace App\Http\Controllers;

// use Illuminate\Http\Request;

use App\Http\Requests;
use Request;
// use App\Permission;
// use App\Role;
use App\User;
use App\Question;
use App\MatrixData;
use Input;
use Auth;
use Session;
use Redirect;
use Carbon;
use App\UserCustome;
use App\UserOption;
class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

       return view("TESTT");           

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function storequestion(Request $request)
    {
        $data=Input::all();
    // dd($data);
       $id=Input::get('Survey_id');
        $Name=[];
        $Title=[];
        $Weight=[];
        
        $IsRequired=[];
        $Text=[];
        $Value=[];
        $Textcol=[];
        $Valuecol=[];
        foreach ($data as $key => $value) {
        	if (strpos($key, 'Name') !== false){
				    array_push($Name, $value);
				}
					if (strpos($key, 'Title') !== false){
				    array_push($Title, $value);
				}
                if (strpos($key, 'Weight') !== false){
                    array_push($Weight, $value);
                }
                
					if (strpos($key, 'IsRequired') !== false){
				    array_push($IsRequired, $value);
				}
					if (strpos($key, 'Text') !== false){
				        if (strpos($key, 'TextCol') !== false){
				            array_push($Textcol, $value);
						}
						else
						{
							array_push($Text, $value);
						}

				}
				if (strpos($key, 'Value') !== false){
				        if (strpos($key, 'ValueCol') !== false){
				            array_push($Valuecol, $value);
						}
						else
						{
							array_push($Value, $value);
						}

				}
        }
        for ($i=0; $i < count($Name); $i++) { 
        	$Question = new Question();
        	$Question->Name=$Name[$i];
        	$Question->Label=$Title[$i];
          $Question->Weight=$Weight[$i];
        	$Question->isRequired=$IsRequired[$i];
        	$Question->Type="matrix";
        	// $Question->Weight=10;
        	$Question->PageName="page".$i;
        	$Question->Survey_Id=$id;
        	$Question->save();
        	for ($j=0; $j < count($Text[$i])-1; $j++) { 
                   $MatrixData=new MatrixData();
                   $MatrixData->Text=$Text[$i][$j];
                   $MatrixData->Value=$Text[$i][$j];
                   $MatrixData->Weight=$Value[$i][$j]; 
                   $MatrixData->Type="rows";
                   $MatrixData->Question_Id=$Question->id; 
                   $MatrixData->save();
        	}
        	for ($j=0; $j < count($Textcol[$i])-1; $j++) { 
                   $MatrixData=new MatrixData();
                   $MatrixData->Text=$Textcol[$i][$j];
                   $MatrixData->Value=$Valuecol[$i][$j]; 
                   $MatrixData->Type="columns";
                   $MatrixData->Question_Id=$Question->id; 
                   $MatrixData->save();
        	}
        }
        // $link='/AddUsertoServey/'.$id;
        // return redirect ($link);

  return view('Survey/AssignServey',compact('id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
   

  

}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showsuervy($id)
    {
        $id=1;
        $user_id=1;
        return view('/test',compact('id','user_id'));
    }



public function getsurveydata()
    {

$survey_id=Input::get('survey_id');



$Question =  Question::where('Survey_Id',$survey_id)->orderBy('id')->get();

// dd($Question);



$elements=[];
$pages=[];

foreach ($Question as $value) {

// dd($value->id);
$rows=[];
$columns=[];
$Data=[];

 $MatrixData= MatrixData::where('Question_Id',$value->id)->get();


foreach ($MatrixData as $Matrixvalue) {

	if($Matrixvalue->Type === 'rows')
	{
		array_push($rows, array('value'=>$Matrixvalue->Value,
			'text'=>$Matrixvalue->Text));

	}
	else
	{
	array_push($columns, array('value'=>$Matrixvalue->Value,
			'text'=>$Matrixvalue->Text));
	}
}//end for each for rows and columns for question

$Name=$value->Name. '(  ' .$value->Label. ')';
array_push($Data, array('name'=>$value->Name,
'title'=>$Name,
'type'=>$value->Type,
'rows'=>$rows,
'columns'=>$columns,
'isRequired'=>true,
 'isAllRowRequired'=> true,
));


array_push($elements, array('elements'=>$Data,'name'=>$value->PageName ));







 }


array_push($pages, array('pages'=>$elements ));



// dd($pages);

        return \Response::json($pages);


        // return view('/test',compact('id'));
    }


    


  public function Savesurveydata(Request $request )
    { 
    

  // $data =  \Input::all(); //read json in request
  //send json respond
         
         
           

                  
         $survey_id=Input::get('survey_id'); 
         $data=Input::get('survey_Data'); 
         $user_id=Input::get('user_id');
         

                // $survey_id=40;
                // $user_id=10;
                 // $data='{"Quality":{"affordable":"1","does what it claims":"2","better then others":"3","easy to use":"5"}}';
        $Data=json_decode($data,true);
        
    //return \Response::json($Data);
        foreach ($Data as $key => $value) {
            $Question=Question::where('Name',$key)->where('Survey_Id',$survey_id)->first();
            foreach ($value as $index => $MatrixData) {
            $mytime = Carbon::now();
            // $Question->GetUser()->attach($user_id, ['data'=>$MatrixData,'Row'=>$index,'answerdate'=>$mytime]);
            $answer=new UserOption();
            $answer->Row=$index;
            $answer->answerdate=$mytime;
            $answer->data=$MatrixData;
            $answer->Question_Id=$Question->id;
            $answer->User_Id=$user_id;
            $answer->save();
        
            }
        }


          }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

    }


     public function login(){ 

       
  }

  public function logout(){
        Auth::logout();
        return Redirect::to('/login');
  }
}
