<?php

namespace App\Http\Controllers;


use Auth;
use DB;
use App\Http\Controllers\Controller;
use Excel;
use File;
use Illuminate\Support\Facades\Response;
use Validator;
use Illuminate\Support\Facades\Redirect;
use Input;
use Session;
use Geocode;
use Cache;
use URL;
use View;
use Khill\Lavacharts\Laravel\LavachartsFacade as Lava;
use Khill\Lavacharts\Lavacharts;
use TableView;
use App\Dategps;
use PHPExcel; 
use PHPExcel_IOFactory; 
use App\Item ;
use App\Brand;
use App\Category;
use App\Datawarehouse_items;
use image;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\FlagItems;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
       public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
          try {

 if (Auth::check())
  {


    if(Auth::user()->can('show-items'))
      {
        $showadditem=1;
        $items = Item::all(); 
        $brand = Brand::select('Name','id')->lists('Name','id');
        $category=Category::select('Name','id')->lists('Name','id');

        $flagitem=FlagItems::select('Name','id')->lists('Name','id');

        return view('Items.index',compact('items','brand','showadditem','category','flagitem'));
      }
    else 
      {
        
    return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
    
    }

function getTimeInterval($ts1)
{
          
}

  public function UpdateImageItem(Request $request)
    {
     
  $id=Input::get('id');
        
         $file_img = Input::file('ImageItem');
        
        $Items =Item::whereId($id)->first();
     
        if($file_img!==null)
        {

            $imageName = $file_img->getClientOriginalName();



            $path=public_path("assets/dist/img/").$imageName;

 
                if (!file_exists($path)) {
                  $file_img->move(public_path("/assets/dist/img/Items"),$imageName);
                  $Items->ImageItem="/MIS/assets/dist/img/Items/".$imageName;
                
                  
                }
                else
                {
                   
                   $random_string = md5(microtime());

                   $file_img->move(public_path("/assets/dist/img/Items/"),$random_string.".jpg");

                  $Items->ImageItem="/MIS/assets/dist/img/Items/".$random_string.".jpg";

              
                }    
             
        }   
       
  
        $Items->save();
        return redirect('/Items');


















    }
     public function EditPhotoItem($id)
   {

     $Items=Item::where('id',$id)
     ->first();
    return view('Items.EditphotoItems',compact('Items'));
   }
    public function showallitems($id)
    {
       
        $showadditem=0;
        $items = Item::where('brand_id','=',$id)->get(); 
        $brand = Brand::where('Active','=','1')->lists('Name','id');
        return view('Items.index',compact('items','brand','showadditem'));

    }
    public function updateItems()
    {
  try {

 if (Auth::check())
  {


    if(Auth::user()->can('edit-items'))
      {

           $item_id= Input::get('pk');  
        $column_name = Input::get('name');
        $column_value = Input::get('value');


        $gpsData = Item::whereId($item_id)->first();
        $gpsData-> $column_name=$column_value;

        if($gpsData->save())

        return \Response::json(array('status'=>1));
        else 
        return \Response::json(array('status'=>0));

      }
    else 
      {
     return \Response::json(array('status'=>'You do not have permission.'));
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }

    

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    //Doaa Elgheny 
    public function MinLimtItems()
    {
       try {

 if (Auth::check())
  {


    if(Auth::user()->can('show-Min Limit Items'))
      {
           
      // $Items=Item::select('Name','id')->lists('Name','id');
 if(Auth::user()->can('Show All Warehouse'))
      {
        $warehouseID =DB::table('datawarehouses')
      // ->where('Government','=',Auth::user()->Government)
       ->select ('Name', 'id')->get();
      }
      else //if Show Specific Warehouse
      {
        $warehouseID =DB::table('datawarehouses')
      ->where('Government','=',Auth::user()->Government)
       ->select ('Name', 'id')->get();

      } 
      $WareID=[];
       foreach ($warehouseID as  $value) {
         array_push($WareID, $value->id);
       }

 $Items = item::join('datawarehouse_items','datawarehouse_items.item_id','=','items.id')
    ->join('datawarehouses','datawarehouses.id','=','datawarehouse_items.datawarehouse_id')
    ->whereIN('datawarehouse_items.datawarehouse_id',$WareID)
     ->Where('datawarehouse_items.quantityinstock','!=',0)
     ->Where('datawarehouse_items.quantityinstock','<',10)
        ->select('items.Name','items.id')
         ->lists('Name', 'id');


    $AllItemLimit = item::join('datawarehouse_items','datawarehouse_items.item_id','=','items.id')
    ->join('datawarehouses','datawarehouses.id','=','datawarehouse_items.datawarehouse_id')
    ->whereIN('datawarehouse_items.datawarehouse_id',$WareID)
     ->Where('datawarehouse_items.quantityinstock','!=',0)
     ->Where('datawarehouse_items.quantityinstock','<',10)
        ->select('datawarehouses.Name As NameStock ','items.Name As Name','items.totalmstock')->get();




      return view('StockReports.MinLimitItems',compact('Items','AllItemLimit'));

      }
    else 
      {
  return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
  return redirect('/login');
    }
    }
    //
    public function MinLimitQuatityItems()
    {
          if(Auth::user()->can('Show All Warehouse'))
      {
        $warehouseID =DB::table('datawarehouses')
      // ->where('Government','=',Auth::user()->Government)
       ->select ('Name', 'id')->get();
      }
      else //if Show Specific Warehouse
      {
        $warehouseID =DB::table('datawarehouses')
      ->where('Government','=',Auth::user()->Government)
       ->select ('Name', 'id')->get();

      } 
      $WareID=[];
       foreach ($warehouseID as  $value) {
         array_push($WareID, $value->id);
       }

      //Case2 Specific Items
      if (input::get("item_id")!=Null)
       {
       $MinLimitItems = DB::table('datawarehouse_items')
         ->join('items','items.id','=','datawarehouse_items.item_id')
         ->join('datawarehouses','datawarehouses.id','=','datawarehouse_items.datawarehouse_id')
          ->whereIN('datawarehouse_items.datawarehouse_id',$WareID)
         ->where('datawarehouse_items.quantityinstock','<=',input::get('MinLimit'))
         // ->where('datawarehouse_items.quantityinstock','=',input::get('MinLimit'))
         ->where('datawarehouse_items.item_id','=',input::get("item_id"))
         ->select('items.Name As ItemName','datawarehouse_items.quantityinstock','datawarehouses.Name As Name')
         ->get();
   }
   // Case1 All All Items
    else 
   
   {
       $MinLimitItems = DB::table('datawarehouse_items')
         ->join('items','items.id','=','datawarehouse_items.item_id')
         ->join('datawarehouses','datawarehouses.id','=','datawarehouse_items.datawarehouse_id')
          ->whereIN('datawarehouse_items.datawarehouse_id',$WareID)
         ->where('datawarehouse_items.quantityinstock','<=',input::get("MinLimit"))
         // ->where('datawarehouse_items.quantityinstock','=',input::get("MinLimit"))
         ->select('items.Name As ItemName','datawarehouse_items.quantityinstock','datawarehouses.EngName AS Name')
         ->get();

   }


         $result=[];
    
            foreach ($MinLimitItems as  $MinLimitItems) 
           {    
             array_push($result,array('item_Name'=>$MinLimitItems->ItemName,'quantity'=>$MinLimitItems->quantityinstock,'Name'=>$MinLimitItems->Name)); 
           }

        

return $result;

      

}
    public function store()
    {
          try {

 if (Auth::check())
  {


    if(Auth::user()->can('create-items'))
      {

       $Item= new Item;

        $Item->Name =Input::get('Name');
        $Item->Partition =Input::get('Partition');
        $Item->EngName =Input::get('EngName');
        $Item->Notes =Input::get('Notes');
        $Item->totalmstock =0;
        $Item->brand_id =Input::get('brand');
        $Item->category_id =Input::get('category');
        
        // dd(Input::get('brand'));
        $Item->flagitmtotal =Input::get('flagitem');
        $Item->Code =Input::get('Code');
        $Item->MinLimit =Input::get('MinLimit');

          $file_img = Input::file('pic');

 if ($file_img !=Null)
          {
             $imageName = $file_img->getClientOriginalName();


                $path=public_path("assets/dist/img/Items").$imageName;
              // notation octale : valeur du mode correcte
                  

                if (!file_exists($path)) {
                  $file_img->move(public_path("assets/dist/img/Items"),$imageName);
                  $Item->ImageItem="/MIS/assets/dist/img/Items/".$imageName; 
                }
                else
                {
                   $random_string = md5(microtime());
                   $file_img->move(public_path("assets/dist/img/Items/"),$random_string.".jpg");
                   
                  $Item->ImageItem="/MIS/assets/dist/img/Items/".$random_string.".jpg";
                
                }
          }

         
  
        $Item->save();
        return redirect('/Items');

      }
    else 
      {
    return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
     
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       $items=Item::find($id);
       
        $pic = \Image::make($items->ImageItem);


        $response = \Response::make($pic->encode('jpeg'));
        $response->header('Content-Type', 'image/jpeg');

        return $response;
    }
Public function NotificationStock()
{
 
    $data=[];


       if(Auth::user()->can('Show All Warehouse'))
      {
        $warehouseID =DB::table('datawarehouses')
      // ->where('Government','=',Auth::user()->Government)
       ->select ('Name', 'id')->get();
      }
      else //if Show Specific Warehouse
      {
        $warehouseID =DB::table('datawarehouses')
      ->where('Government','=',Auth::user()->Government)
       ->select ('Name', 'id')->get();

      } 
      $WareID=[];
       foreach ($warehouseID as  $value) {
         array_push($WareID, $value->id);
       }

    $ItemsMinLimit = item::join('datawarehouse_items','datawarehouse_items.item_id','=','items.id')
    ->whereIN('datawarehouse_items.datawarehouse_id',$WareID)
     ->Where('datawarehouse_items.quantityinstock','!=',0)
     ->Where('datawarehouse_items.quantityinstock','<',10)
        ->select('*')->get();
 

       //Doaa Elgheny I want Return Items Min Limit
           

            if ($ItemsMinLimit!=Null)
            {
               $CountNotification=count($ItemsMinLimit);

               array_push($data,array('CountNotification'=>$CountNotification));
            }
             
  return $data;
}    
public function checkCode()
{
     $Code=Input::get('Code');
      $CodeItem = Item::where('items.Code','=',$Code)
     ->select('*')->get();
     if($CodeItem==null)     
        $isAvailable = true;
        else
         $isAvailable = false;

    return \Response::json(array('valid' =>$isAvailable,));
}
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
      public function checknameitem()
    {
         $brandname=Input::get('Name');
      $brand = Item::where('items.Name','=',$brandname)
     ->select('*')->first();
     if($brand==null)     
        $isAvailable = true;
        else
         $isAvailable = false;

    return \Response::json(array('valid' =>$isAvailable,));
    }
    

      public function checkEngNameitem()
    {
         $brandname=Input::get('EngName');
      $brand = Item::where('items.EngName','=',$brandname)
     ->select('*')->first();
     if($brand==null)     
        $isAvailable = true;
        else
         $isAvailable = false;

    return \Response::json(array('valid' =>$isAvailable,));
    }
    public function destroy($id)
    {
          try {

 if (Auth::check())
  {


    if(Auth::user()->can('delete-items'))
      {

       $items=Item::find($id);
         
                    $items->delete();
                    return redirect('/Items');
      }
    else 
      {
    return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
      
    }
}
