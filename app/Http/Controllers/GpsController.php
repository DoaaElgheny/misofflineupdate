<?php


namespace App\Http\Controllers;
use Auth;
use App\Http\Requests;
use DB;
use App\Http\Controllers\Controller;
use Request;
use Excel;
use File;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Input;
use Validator;
use Illuminate\Support\Facades\Redirect;
use App\User;
use App\Gps;
use App\Product;
use App\Company;
use App\sale_product;
use App\sale_product_details;
use App\Contractor;
use App\Price;
use Session;
use Geocode;
use App\Order;
use App\Activity;
use Image;
use PHPExcel; 
use App\Government;
use App\District;
use App\Cafe;
use App\CafeSigns;
use App\Sign;

class GpsController extends Controller
{


   
// login user
public function doLogin(Request $request)
 {


$username=$request::json()->get('username');
$password=$request::json()->get('password');

      $userdata = array(
        'Username'     => $username,
        'password'  => $password
    );
        $titles=User::where('Username',$username)->pluck('role')->first();

       if (Auth::attempt($userdata) && $titles=='admin')
        {
             $user=[];

                    $user_id=Auth::user()->id;
                    $username=Auth::user()->Username;
                    $government=Auth::user()->Government; 
                    $role=Auth::user()->role;

                      array_push($user,array(
                        'id'=>$user_id,
                        'username'=> $username,
                        'government'=>$government,
                        'role'=> $role
                        ));

              return json_encode($user);

        }
       elseif (Auth::attempt($userdata) && $titles=='user')

        {    

          $user=[];
              $user_id=Auth::user()->id;
              $username=Auth::user()->Username;
              $government=Auth::user()->Government; 
              $role=Auth::user()->role;


                        array_push($user,array(
                           'id'=>$user_id,
                           'username'=> $username,
                            'government'=>$government,
                            'role'=> $role
                            ));


                                                    
                         return json_encode($user);
             }

                                                
              else {
                      $user=0;
                      return json_encode($user);         
                    }
   }


//get sale _products in gpscontroler

public function getproduct(request $request)
    {

$government=$request::json()->get('government');
        // $company_id=Company::where('EnName','CEMEX')->pluck('id')->first();
     
        $cement=product::join('companies','companies.id','=','Company_id')
                        -> select('products.Type','products.Cement_Name as Name','products.id')
                        ->where('companies.EnName', 'CEMEX')
                        ->distinct()
                        ->get();

        $sale_product_avalable=sale_product_details::join('sale_products','sale_products.id','=','SaleProduct_id')
                                                    ->select('sale_products.Type','sale_products.Name','sale_products.id')
                                                    ->where('Available','1')
                                                    ->where('Government',$government)
                                                    ->distinct()
                                                    ->get();

        $products=[];

        foreach ($sale_product_avalable as $item) {

          array_push($products,$item);

        }

        foreach ($cement as $item) {

          array_push($products,$item);

        }


        

            return json_encode($products);
    }




//store check in

public function GpsStore(Request $request)
    {
//var_dump(json_decode(file_get_contents("php://input")));
date_default_timezone_set("Africa/Cairo");


$Code=$request::json()->get('Code');
$date = date('Y-m-d');

$tdate=date("H:i:s");

$id=$request::json()->get('id');
$lng=$request::json()->get('lng');
$lat=$request::json()->get('lat');
$Name=$request::json()->get('Name');
$Type=$request::json()->get('Type');
$status=$request::json()->get('status');

$n=count($status);

 $Contractor= Contractor::select('Name','id','Code','Tele1','Tele2')
                                    ->where('Code',$Code)
                                    ->orWhere('Tele1',$Code)
                                    ->orWhere('Tele2',$Code)
                                    ->first();

if($Type==='حصر كميات')
  {

  if($Contractor!== null)
    {
    $Visits=gps::select('id')
       ->where('contractor_id',$Contractor->id)
         ->where('NDate',$date)
         ->where('Type','=','حصر كميات')
         ->get();

         $HVisits=gps::select('id')
         ->where('contractor_id',$Contractor->id)
         ->where('NDate',$date)
         ->where('Type','=','تسويق')
         ->get();

    $v=count($Visits); 
    $Hv=count($HVisits); 


    if($v < 3 && $Hv== 0 ){

           $myLocation = New Gps ;
          $myLocation->Long =$lng;
          $myLocation->Lat=$lat;
          $myLocation->user_id=$id;
          $myLocation->NDate=$date;
          $myLocation->NTime=$tdate;                            
          $myLocation->Type=$Type;
          $myLocation->Contractor_Name=$Contractor->Name;
           $myLocation->contractor_id=$Contractor->id;
           $myLocation->Contractor_Code=$Contractor->Code;
           $myLocation->status=0;

           $myLocation->save(); 

      foreach($status as $product){


                                   $parts = explode('*', $product['product']);

                                    $productid=$parts[0];

                                    $type=$parts[1];
                                    $amount=$product['amount'];
                                    if($type==='Cement')
                                      {

                                          $order=new Order;
                                          $order->Product_id=$productid;
                                          $order->Amount=$amount;
                                          $order->IsDone=1;
                                          $order->Type='حصر كميات';
                                          $order->gps_id=$myLocation->id;
                                          $order->save();

                                      }
                           
                        }

       $done=1;
       return json_encode($done); 

    }//end v
    else
    {
               $done=0;
           return json_encode($done); 
    }
    }//end null
    else
    {

    $done=2;
    return json_encode($done); 
    }
}//end marketing

if($Type ==='تسويق'){
  
    $Contractor= Contractor::select('Name','id','Code','Tele1','Tele2')
                                    ->where('Code',$Code)
                                    ->orWhere('Tele1',$Code)
                                    ->orWhere('Tele2',$Code)
                                    ->first();


            if($Contractor!== null)
                {

                     $Marketing=gps::select('id')
               ->where('contractor_id',$Contractor->id)
               ->where('NDate',$date)
               ->where('type','تسويق')  
               ->get();

                 $hMarketing=gps::select('id')
                 ->where('contractor_id',$Contractor->id)
                 ->where('NDate',$date)
                 ->where('type','حصر كميات')  
                 ->get();

           $M=count($Marketing);
           $HM=count($hMarketing);
             if($M == 0 && $HM == 0 )
               {

                          $myLocation = New Gps ;
                          $myLocation->Long =$lng;
                          $myLocation->Lat=$lat;
                          $myLocation->user_id=$id;
                          $myLocation->NDate=$date;
                          $myLocation->NTime=$tdate;                            
                          $myLocation->Type=$Type;
                          $myLocation->Contractor_Name=$Contractor->Name;
                          $myLocation->contractor_id=$Contractor->id;
                          $myLocation->Contractor_Code=$Contractor->Code;
                      
                      
                       $myLocation->status=0;
                      



                       $myLocation->save();


                  $done=1;
                 return json_encode($done); 



             }
             else
             {
                   $done=0;
                   return json_encode($done); 
             }

      }//end null
      else
      {
          $done=2;
                         return json_encode($done); 
      }

   }//end marketing

if($Type==='المبيعات'){
 
  if($Contractor!== null)
                {


          $Visits=gps::select('id')              
          ->where('contractor_id',$Contractor->id)
          ->where('NDate',$date)
          ->where('Type','المبيعات')
          ->get();
          $v=count($Visits);

      if($v == 0)
      {
          $myLocation = New Gps ;
          $myLocation->Long =$lng;
          $myLocation->Lat=$lat;
          $myLocation->user_id=$id;
          $myLocation->NDate=$date;
          $myLocation->NTime=$tdate;                            
          $myLocation->Type=$Type;
          $myLocation->Contractor_Name=$Contractor->Name;
          $myLocation->contractor_id=$Contractor->id;
          $myLocation->Contractor_Code=$Contractor->Code;

          if($n > 0)
          {
          $myLocation->status=1;
          }
          else
          {
          $myLocation->status=0;
          }



          $myLocation->save();

              foreach($status as $product){


                
                                   $parts = explode('*', $product['product']);

                                    $productid=$parts[0];

                                    $type=$parts[1];
                                    $amount=$product['amount'];

                                    if($type==='Cement')
                                      {

                                          $order=new Order;
                                          $order->Product_id=$productid;
                                          $order->Amount=$amount;
                                          $order->IsDone=0;
                                          $order->Type='المبيعات';
                                          $order->gps_id=$myLocation->id;
                                          $order->save();

                                      }
                                   if($type==='Sale')
                                      {

                                          $order=new Order;
                                          $order->SaleProduct_id=$productid;
                                          $order->Amount=$amount;
                                          $order->IsDone=0;
                                          $order->Type='المبيعات';
                                          $order->gps_id=$myLocation->id;
                                          $order->save();

                                      }

          }



    $done=1;
    return json_encode($done); 


    }//end$v
    else
    {
      $done=0;
    return json_encode($done);
    }

    }//end null
    else
    {
      $done=2;
    return json_encode($done);
    }

}//end Sale


     }
  
//allgovernment
public function Gpsgovrnment(Request $request)
{


  $governments=Government::select('Name','id')->lists('Name','id');
    $result=[];
    $data=[];
     foreach ($governments as $key => $value) {

         $Districts=District::join('governments','governments.id','=','government_id')
                          ->select('districts.Name','districts.id')
                          ->where('government_id',$key )
                          ->distinct()
                          ->get();
                   
               
                 foreach ($Districts as $item) {

                    array_push($data,array( 'id'=>$item->id,
                                           'name'=>$item->Name 
                                         ));                                          
                                   }
                                           

                                     
                array_push($result,array(
                                    'Government'=>$value,
                                    'District' => $data
                                  ));
                        $data=[];
                      


     }


       


        

            return json_encode($result);



}



























//all company
public function Gpscompany(request $request)
{

     $company=company::select('Name','id')->lists('Name','id');

 // $governments=Government::select('Name','id')->lists('Name','id');
    $result=[];
    $data=[];
     foreach ($company as $key => $value) {

         $products=product::select('products.Cement_Name as Name','products.id')
                          ->where('Company_id',$key )
                          ->distinct()
                          ->get();
                   
               
                 foreach ($products as $item) {

                    array_push($data,array( 'id'=>$item->id,
                                        'name'=>$item->Name 
                                         ));                                          
                                   }
                                           

                                     
                array_push($result,array(
                                    'Company'=>$value,
                                    'Products' => $data
                                  ));
                        $data=[];
                      


     }


       


        

            return json_encode($result);



}



public function GpsContractor(Request $request)
{
  date_default_timezone_set("Africa/Cairo");
          // var_dump(json_decode(file_get_contents("php://input")));


$id=$request::json()->get('id');
$Government=$request::json()->get('government');
$District=$request::json()->get('district');

$Tele=$request::json()->get('Tele');
$date = date('Y-m-d');
$tdate=date("H:i:s");
$lng=$request::json()->get('lng');
$lat=$request::json()->get('lat');
$Name=$request::json()->get('Name');
$Type=$request::json()->get('Type');
$status=$request::json()->get('status');

$n=count($status);

 $Contractor= Contractor::select('Name','id','Code','Tele1','Tele2')
                                    ->Where('Tele1',$Tele)
                                    ->orWhere('Tele2',$Tele)
                                    ->first();

if($Contractor!== null)
    {
        $done=$Contractor;

       return json_encode($done);
    }
 else
 {

            $ContractorsData = New Contractor;
            $ContractorsData->Name=$Name; 
            $ContractorsData->District=$District;
            $ContractorsData->Government=$Government; 
            $ContractorsData->Tele1=$Tele;
            $ContractorsData->Code=$Tele; 
            $ContractorsData->Status='New';
            $ContractorsData->save();              

if($Type==='حصر كميات')
  {

 $Visits=gps::select('id')
       ->where('contractor_id',$ContractorsData->id)
         ->where('NDate',$date)
         ->where('Type','=','حصر كميات')
         ->get();

         $HVisits=gps::select('id')
         ->where('contractor_id',$ContractorsData->id)
         ->where('NDate',$date)
         ->where('Type','=','تسويق')
         ->get();

    $v=count($Visits); 
    $Hv=count($HVisits); 


    if($v < 3 && $Hv== 0 ){

           $myLocation = New Gps ;
          $myLocation->Long =$lng;
          $myLocation->Lat=$lat;
          $myLocation->user_id=$id;
          $myLocation->NDate=$date;
          $myLocation->NTime=$tdate;                            
          $myLocation->Type=$Type;
          $myLocation->Contractor_Name=$ContractorsData->Name;
           $myLocation->contractor_id=$ContractorsData->id;
           $myLocation->Contractor_Code=$ContractorsData->Code;
           $myLocation->status=0;
           $myLocation->save(); 

      foreach($status as $product){


                                   $parts = explode('*', $product['product']);

                                    $productid=$parts[0];

                                    $type=$parts[1];
                                    $amount=$product['amount'];
                                    if($type==='Cement')
                                      {

                                          $order=new Order;
                                          $order->Product_id=$productid;
                                          $order->Amount=$amount;
                                          $order->IsDone=1;
                                          $order->Type='حصر كميات';
                                          $order->gps_id=$myLocation->id;
                                          $order->save();

                                      }
                           
                        }

       $done=1;
       return json_encode($done); 

    }//end v
  else
             {
                   $done=0;
                   return json_encode($done); 
             }
 }//end marketing

if($Type ==='تسويق'){


               $Marketing=gps::select('id')
               ->where('contractor_id',$ContractorsData->id)
               ->where('NDate',$date)
               ->where('type','تسويق')  
               ->get();

                 $hMarketing=gps::select('id')
                 ->where('contractor_id',$ContractorsData->id)
                 ->where('NDate',$date)
                 ->where('type','حصر كميات')  
                 ->get();

           $M=count($Marketing);
           $HM=count($hMarketing);
             if($M == 0 && $HM == 0 )
               {

                          $myLocation = New Gps ;
                          $myLocation->Long =$lng;
                          $myLocation->Lat=$lat;
                          $myLocation->user_id=$id;
                          $myLocation->NDate=$date;
                          $myLocation->NTime=$tdate;                            
                          $myLocation->Type=$Type;
                          $myLocation->Contractor_Name=$ContractorsData->Name;
                          $myLocation->contractor_id=$ContractorsData->id;
                          $myLocation->Contractor_Code=$ContractorsData->Code;
                      
                      
                       $myLocation->status=0;
                      



                       $myLocation->save();


                  $done=1;
                 return json_encode($done); 



             }
               else
             {
                   $done=0;
                   return json_encode($done); 
             }


}//end tasweq

if($Type==='المبيعات'){

 $Visits=gps::select('id')              
          ->where('contractor_id',$ContractorsData->id)
          ->where('NDate',$date)
          ->where('Type','المبيعات')
          ->get();
          $v=count($Visits);

      if($v == 0)
      {
          $myLocation = New Gps ;
          $myLocation->Long =$lng;
          $myLocation->Lat=$lat;
          $myLocation->user_id=$id;
          $myLocation->NDate=$date;
          $myLocation->NTime=$tdate;                            
          $myLocation->Type=$Type;
          $myLocation->Contractor_Name=$ContractorsData->Name;
          $myLocation->contractor_id=$ContractorsData->id;
          $myLocation->Contractor_Code=$ContractorsData->Code;

          if($n > 0)
          {
          $myLocation->status=1;
          }
          else
          {
          $myLocation->status=0;
          }



          $myLocation->save();

              foreach($status as $product){


                
                                   $parts = explode('*', $product['product']);

                                    $productid=$parts[0];

                                    $type=$parts[1];
                                    $amount=$product['amount'];

                                    if($type==='Cement')
                                      {

                                          $order=new Order;
                                          $order->Product_id=$productid;
                                          $order->Amount=$amount;
                                          $order->IsDone=0;
                                          $order->Type='المبيعات';
                                          $order->gps_id=$myLocation->id;
                                          $order->save();

                                      }
                                   if($type==='Sale')
                                      {

                                          $order=new Order;
                                          $order->SaleProduct_id=$productid;
                                          $order->Amount=$amount;
                                          $order->IsDone=0;
                                          $order->Type='المبيعات';
                                          $order->gps_id=$myLocation->id;
                                          $order->save();

                                      }

          }



    $done=1;
    return json_encode($done); 


    }//end$v
    else
    {
      $done=0;
    return json_encode($done);
    }




}//end mape3at

 }





}

//get products price
public function GpsPrice(Request $request)
  {

     date_default_timezone_set("Africa/Cairo");
          // var_dump(json_decode(file_get_contents("php://input")));


                            $date = date('Y-m-d');
                            $id=$request::json()->get('id');
                            $Government=$request::json()->get('government');
                            $District=$request::json()->get('district');
                            $price_no=$request::json()->get('price');
                            $product_name=$request::json()->get('product_name');

                            $productid=Product::select('id')->where('Cement_Name',$product_name)->first();
                            
                           $exit=price::where('Government',$Government)
                                      ->where('District',$District)
                                      ->where('Date',$date)
                                       ->where('Product_id',$productid->id)
                                       ->where('Price',$price_no)
                                       ->select('*')
                                       ->first();



                             if($exit != null)
                             {
                                 $done=0;
                                   return json_encode($done);
                             }
                        else
                        {
                           $price = new Price;
                            $price->Government=$Government;
                            $price->District=$District;
                            $price->Date=$date ;
                            $price->Price=$price_no;
                            $price->Product_id=$productid->id;
                            $price->User_id= $id;
                            $price->save();

                           $done=1;
                           return json_encode($done);
                         }



  }



//activity
 


public function activity()
          {
   




   date_default_timezone_set("Africa/Cairo");


    //I am storing the image in the public/images folder 
    $destinationPath = 'assets/img/';

    $newImageName='MyImage.png';
    //return json_encode($_POST);

    //Rename and move the file to the destination folder 
     Input::file('file')->move($destinationPath,$newImageName);
     
$file=Input::file('file');
          
$Government=$_POST['Government'];
$District=$_POST['District'];
$company=$_POST['company'];                             
$Type=$_POST['Type'];                           
$Target=$_POST['Target'];                            
$Name=$_POST['Name'];                            
$description=$_POST['description'];                            
$Date=$_POST['Date'];                           
$Duration=$_POST['Duration'];
$user_id=$_POST['user_id'];
$companyID=company::select('id')->where('Name',$company)->first();

// Check if this Activity Exsists or Not 
$ActivityExsit=Activity::select('id')

->where('Government','=',$Government)
->where('District','=',$District)

->where('company_id','=',$companyID->id)
->where('Type','=',$Type)
->where('Target_Segmentation','=',$Target)

->where('Activity_type','=',$Name)
->where('Start_date','=',$Date)
->where('User_id','=',$user_id)
->first();

if($ActivityExsit == Null)

{

    $logo = base_path() .'\public\assets\img\MyImage.png'; // Provide path to your logo file
                                          
    $img = Image::make( $logo)->resize(200,200);
    Response::make($img->encode('png')); 


//companyID  
      $companyID=company::select('id')->where('Name',$company)->first();

// //new
        $ActivityData=new Activity();
        $ActivityData->Government=$Government;
        $ActivityData->District=$District;
        $ActivityData->Start_date=$Date;
        $ActivityData->company_id=$companyID->id;
        $ActivityData->Description=$description;
        $ActivityData->Activity_type=$Name;
        $ActivityData->User_id=$user_id;
        $ActivityData->Target_Segmentation=$Target;
        $ActivityData->Duration=$Duration;
        $ActivityData->Type=$Type;
        $ActivityData->Img=$img;
//            // dd($img);
       $ActivityData->save();

$done=1;

       return json_encode($done);

}
else
{
  $NotDone=0;
  return json_encode($NotDone);
}


}
//Cafes
public function GpsCafes(Request $request)
{
   date_default_timezone_set("Africa/Cairo");

                            $date = date('Y-m-d');
                            $tdate=date("H:i:s");
                            $id=$request::json()->get('id');
                            $Government=$request::json()->get('government');
                            $District=$request::json()->get('district');
                            $lng=$request::json()->get('lng');
                            $lat=$request::json()->get('lat');
                            $Type='مقاهي';
                          $distances =Cafe::select('Lat','Long')
                                          ->where('Government',$Government)
                                          ->where('District',$District)
                                          ->get();

foreach($distances as $distance)
    {
       $lat1= $lat;
       $lng1=$lng;
       $lat2=$distance->Lat; 
       $lng2=$distance->Long;

  $pi80 = M_PI / 180;
  $lat1 *= $pi80;
  $lng1 *= $pi80;
  $lat2 *= $pi80;
  $lng2 *= $pi80;
 
  $r = 6378137; // mean radius of Earth in km
  $dlat = $lat2 - $lat1;
  $dlng = $lng2 - $lng1;
  $a = sin($dlat / 2) * sin($dlat / 2) + cos($lat1) * cos($lat2) * sin($dlng / 2) * sin($dlng / 2);
  $c = 2 * atan2(sqrt($a), sqrt(1 - $a));
  $meters = $r * $c;
  
      
   if($meters <= 20)
   {
        $cafesigns =DB::table('cafesigns')
                              ->where('NDate',$date)
                              ->where('Government',$Government)
                              ->where('District',$District)
                              ->where('user_id',$id)
                              ->where('Type',$Type)
                              ->select('Lat','Long')
                              ->get();
  

   if($cafesigns ==null)
   {

 $URl="https://maps.google.com/?q=".$lat.','.$lng;
   $Cafes =new CafeSigns;
   $Cafes->Long =$lng;
   $Cafes->Lat=$lat;
   $Cafes->user_id=$id;
   $Cafes->NDate=$date;
   $Cafes->NTime=$tdate;                            
   $Cafes->Type=$Type;
   $Cafes->Target=' ';
   $Cafes->Adress=$URl;
   $Cafes->Government=$Government;
   $Cafes->District=$District;
   $Cafes->save();

$done=1;

       return json_encode($done);


   }
   else
   {
foreach ($cafesigns as $cafe_signs) {


       $lat1= $lat;
       $lng1=$lng;
       $lat2=$cafe_signs->Lat; 
       $lng2=$cafe_signs->Long;

 $pi80 = M_PI / 180;
  $lat1 *= $pi80;
  $lng1 *= $pi80;
  $lat2 *= $pi80;
  $lng2 *= $pi80;
 
  $r = 6378137; // mean radius of Earth in km
  $dlat = $lat2 - $lat1;
  $dlng = $lng2 - $lng1;
  $a = sin($dlat / 2) * sin($dlat / 2) + cos($lat1) * cos($lat2) * sin($dlng / 2) * sin($dlng / 2);
  $c = 2 * atan2(sqrt($a), sqrt(1 - $a));
  $resend = $r * $c;
if($resend <= 15)
{
$done=20;

       return json_encode($done);

}
else
{

   $URl="https://maps.google.com/?q=".$lat.','.$lng;
   $Cafes =new CafeSigns;
   $Cafes->Long =$lng;
   $Cafes->Lat=$lat;
   $Cafes->user_id=$id;
   $Cafes->NDate=$date;
   $Cafes->NTime=$tdate;                            
   $Cafes->Type=$Type;
   $Cafes->Target='';
   $Cafes->Adress=$URl;
   $Cafes->Government=$Government;
   $Cafes->District=$District;
   $Cafes->save();

$done=1;

       return json_encode($done);

}

}
}

   }

    }
 

}
//Signs

public function GpsSigns()
{
   date_default_timezone_set("Africa/Cairo");


   //I am storing the image in the public/images folder 
    $destinationPath = 'assets/img/';

    $newImageName='Signs.png';
    //return json_encode($_POST);

    //Rename and move the file to the destination folder 
Input::file('file')->move($destinationPath,$newImageName);
   
$file=Input::file('file');         
$Government=$_POST['Government'];
$District=$_POST['District'];
$company=$_POST['company'];                             
$Type='Signs';
$Status=$_POST['Status']; 
$Target=$_POST['Target'];
$lng=$_POST['lng'];                            
$lat=$_POST['lat'];                            
$user_id=$_POST['user_id'];
$date = date('Y-m-d');
$tdate=date("H:i:s");
$companyID=company::select('id')->where('Name',$company)->first();

 $logo = base_path() .'\public\assets\img\Signs.png'; // Provide path to your logo file
                                          
    $img = Image::make( $logo)->resize(200,200);
    Response::make($img->encode('png')); 

if($company==='أسمنت أسيوط')
{

$fdata=Sign::get();
      


foreach ($fdata as  $value) {

       
       $lat1= $lat;
       $lng1=$lng;
       $lat2=$value->Latitude; 
       $lng2=$value->Longitude;


$pi80 = M_PI / 180;
  $lat1 *= $pi80;
  $lng1 *= $pi80;
  $lat2 *= $pi80;
  $lng2 *= $pi80;
 
  $r = 6378137; // mean radius of Earth in km
  $dlat = $lat2 - $lat1;
  $dlng = $lng2 - $lng1;
  $a = sin($dlat / 2) * sin($dlat / 2) + cos($lat1) * cos($lat2) * sin($dlng / 2) * sin($dlng / 2);
  $c = 2 * atan2(sqrt($a), sqrt(1 - $a));
  $meters = $r * $c;

   if($meters <= 50)
   {

        $cafesigns =DB::table('cafesigns')
                              ->where('NDate',$date)
                              ->where('Government',$Government)
                              ->where('District',$District)
                              ->where('user_id',$user_id)
                              ->where('Type',$Type)
                              ->select('Lat','Long')
                              ->get();



if(count($cafesigns)=== 0)
{
 $URl="https://maps.google.com/?q=".$lat.','.$lng;
   $Cafes =new CafeSigns;
   $Cafes->Long =$lng;
   $Cafes->Lat=$lat;
   $Cafes->user_id=$user_id;
   $Cafes->NDate=$date;
   $Cafes->NTime=$tdate;                            
   $Cafes->Type=$Type;
   $Cafes->Adress=$URl;
   $Cafes->Status=$Status;
   $Cafes->Target=$Target;
   $Cafes->Img=$img;
   $Cafes->Government=$Government;
   $Cafes->District=$District;
   $Cafes->save();

$done=1;

       return json_encode($done);
}
else
{

foreach ($cafesigns as $cafe_signs) {


       $lat1= $lat;
       $lng1=$lng;
       $lat2=$cafe_signs->Lat; 
       $lng2=$cafe_signs->Long;

$pi80 = M_PI / 180;
  $lat1 *= $pi80;
  $lng1 *= $pi80;
  $lat2 *= $pi80;
  $lng2 *= $pi80;
 
  $r = 6378137; // mean radius of Earth in km
  $dlat = $lat2 - $lat1;
  $dlng = $lng2 - $lng1;
  $a = sin($dlat / 2) * sin($dlat / 2) + cos($lat1) * cos($lat2) * sin($dlng / 2) * sin($dlng / 2);
  $c = 2 * atan2(sqrt($a), sqrt(1 - $a));
  $resend = $r * $c;

if($resend <= 20)
{

$done=20;

       return json_encode($done);
}
else
{

   $URl="https://maps.google.com/?q=".$lat.','.$lng;
   $Cafes =new CafeSigns;
   $Cafes->Long =$lng;
   $Cafes->Lat=$lat;
   $Cafes->user_id=$user_id;
   $Cafes->NDate=$date;
   $Cafes->NTime=$tdate;                            
   $Cafes->Type=$Type;
   $Cafes->Adress=$URl;
   $Cafes->Status=$Status;
   $Cafes->Target=$Target;
   $Cafes->Img=$img;
   $Cafes->Government=$Government;
   $Cafes->District=$District;
   $Cafes->save();

$done=1;

       return json_encode($done);

}
}

}


   }
}


}//end campany
else
{
  

$ActivityExsit=Activity::where('Government','=',$Government)
->where('District','=',$District)
->where('company_id','=',$companyID->id)
->where('Type',$Type)
->Where('Long','!=',null)
->Where('Lat','!=',null)
->where('Sent_Date','=',$date)
->where('User_id','=',$user_id)->select('Long','Lat','id')
->get();


 if(count($ActivityExsit) === 0)
 {
     

        $ActivityData=new Activity;
        $ActivityData->Government=$Government;
        $ActivityData->District=$District;
        $ActivityData->company_id=$companyID->id;
        $ActivityData->Long=$lng;
        $ActivityData->Lat=$lat;
        $ActivityData->User_id=$user_id;
        $ActivityData->Type=$Type;
        $ActivityData->Target_Segmentation=$Target;
        $ActivityData->Sent_Date=$date;
        $ActivityData->Img=$img;
        $ActivityData->save();


$done=1;

       return json_encode($done);
 }
 else
 {
 

foreach ($ActivityExsit as $cafe_signs) {


       $lat1= $lat;
       $lng1=$lng;
       $lat2=$cafe_signs->Lat; 
       $lng2=$cafe_signs->Long;

$pi80 = M_PI / 180;
  $lat1 *= $pi80;
  $lng1 *= $pi80;
  $lat2 *= $pi80;
  $lng2 *= $pi80;
 
  $r = 6378137; // mean radius of Earth in km
  $dlat = $lat2 - $lat1;
  $dlng = $lng2 - $lng1;
  $a = sin($dlat / 2) * sin($dlat / 2) + cos($lat1) * cos($lat2) * sin($dlng / 2) * sin($dlng / 2);
  $c = 2 * atan2(sqrt($a), sqrt(1 - $a));
  $resend = $r * $c;
 

if($resend <= 20)
{

$done=20;

       return json_encode($done);
}
else
{
        $ActivityData=new Activity;
        $ActivityData->Government=$Government;
        $ActivityData->District=$District;
        $ActivityData->company_id=$companyID->id;
        $ActivityData->Long=$lng;
        $ActivityData->Lat=$lat;
        $ActivityData->User_id=$user_id;
        $ActivityData->Type=$Type;
        $ActivityData->Target_Segmentation=$Target;
        $ActivityData->Sent_Date=$date;
        $ActivityData->Img=$img;
        $ActivityData->save();

$done=1;

       return json_encode($done);


}
}


}

}//end other company

}












//get user in mapadmin

// public function index()
//           {
//               $users=user::select('Name')->where('role','user')->get();
              
//               return json_encode($users);
//           }





//countgps

public function getcountgps(Request $request)
    {
  
 date_default_timezone_set("Africa/Cairo");

$Count=0;
$id=$request::json()->get('userid');
      $date = date('Y-m-d');
$CountGps= DB::table('gps')
         ->select(DB::raw('count(gps.user_id ) As Count'))
         ->where('user_id','=',$id)
         ->where('Ndate','=',$date)
         ->groupBy(DB::raw('gps.user_id'))
         ->first();
         

if(count($CountGps) == 0)
{
  $Count=0;
}
else
{
 $Count=$CountGps->Count;
}


         return json_encode($Count);      
}
//get daily locations

public function get_location($date)
   {
      
        $loc =  DB::table('gps')
                    ->where('NDate','=',$date)
                    ->select('gps.*')->get();

       // $loc = (array) $loc;        
        foreach ($loc as $location) {
            $var = DB::table('users')
                    ->select('username')
                    ->where('id', '=', $location->user_id)
                    ->get();
            $location->user_id =$var[0]->username;             
        }
        
        return json_encode(['locations' =>$loc]);
        
    }
   

//filter by date and name to get locations
public function NameDateFilter($name, $date)
 { 
  dd($name);
            if ($name=="empty") {
                $loc =  DB::table('gps')
                        ->join('users','users.id','=','gps.user_id')
                        ->where('NDate','=',$date)
                        ->select('gps.*')->get();
             foreach ($loc as $location) {
                $var = DB::table('users')
                        ->select('username')
                        ->where('id', '=', $location->user_id)
                        ->get();
                $location->user_id =$var[0]->username;             
            }
                       
            }

            if($date=="empty") {
                $loc =  DB::table('gps')
                        ->join('users','users.id','=','gps.user_id')
                        ->where('users.username','=',$name)
                        ->select('gps.*')->get();
                        
         foreach ($loc as $location) {
                $var = DB::table('users')
                        ->select('username')
                        ->where('id', '=', $location->user_id)
                        ->get();
                $location->user_id =$var[0]->username;   
                }
                        
                       
                }

            else if ($date !="empty" && $name!="empty"){
                    $loc =  DB::table('gps')
                        ->join('users','users.id','=','gps.user_id')
                        ->where('users.username','=',$name)
                        ->where('NDate','=',$date)
                        ->select('gps.*')->get();
                foreach ($loc as $location) {
                $var = DB::table('users')
                        ->select('username')
                        ->where('id', '=', $location->user_id)
                        ->get();
                $location->user_id =$var[0]->username;             
            }
                        // echo "all";
                    // $loc =  DB::table('gps')->select('gps.*')->get();
                    // ('together');
                  }
        // dd($loc);
        return json_encode(['locations' =>$loc]);      
    }




 public function fupdate()
    {
   
     $myLocation = gps::all();

   // $myLocation->getproduct()->attach($product_id,['Amount'=>$n]);
          $i=0;
          // dd ($myLocation[0]->getproduct); 

                 

        foreach ($myLocation->getproduct as $value) 
          {
                 
              echo ($value->pivot->Amount.$value->Name); 

              $i++;
          }


    }







//backcheck ahmed_yussef

public function backcheck()
  {

          $lat=26.6157441;
          $lng=31.6412562;
          $date='2016-09-7';







          $distance =DB::table('gps')
                              ->join('users', 'users.id', '=', 'gps.user_id')

                             ->select('Lat','Long','user_id','NDate','name','NTime')
                             ->where('NDate','=',$date)
                             ->orderBy('NTime','Dec')
                               ->get();
             
                                              
                              $latTo= deg2rad(26.6157441);
                                $lonTo = deg2rad(31.6412562);
                                $b=[]; 
                              foreach($distance as $distance)
                              {
                                 $latFrom = deg2rad(26.6157441);
                                $lonFrom = deg2rad(31.6412562);
                                $latTo = deg2rad($distance->Lat);
                                $lonTo = deg2rad($distance->Long);

                                $latDelta = $latTo - $latFrom;
                                $lonDelta = $lonTo - $lonFrom;        
                                
                                $angle = (6371*2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
                              cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2))));

                                  if( $angle <=10)
                                  {
                                        array_push($b,array($angle,$distance->user_id,$distance->NDate,$distance->name,$distance->NTime));


                                  }

                             }
                                 

   }























































































































































































































































































































}