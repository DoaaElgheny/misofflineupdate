<?php

namespace App\Http\Controllers;

use Auth;
use App\Http\Requests;
use App\User;
use DB;
use App\Http\Controllers\Controller;
use Request;
use Excel;
use File;
use Illuminate\Support\Facades\Response;
use Validator;
use Illuminate\Support\Facades\Redirect;
use Input;
use Session;
use Geocode;
use Cache;
use URL;
use View;
use Khill\Lavacharts\Laravel\LavachartsFacade as Lava;
use Khill\Lavacharts\Lavacharts;
use TableView;
use App\Dategps;
use PHPExcel; 
use PHPExcel_IOFactory; 
use App\Government;
use App\District;
use App\Retailer;
use App\Type;
use App\EndType;
use App\SubType;

class EndTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
   {
       $this->middleware('auth');
   }
    public function index()
    {
        try
        {
            if (Auth::check())
            {
                // if(Auth::user()->can('show-Marketing_Activity'))
                // {
                        Session::put('SubType_id',null); 
                        $EndTypes=EndType::all();
                        $SubTypese=SubType::select('EngName','id')->lists('EngName','id');
                        return view('endtypes.index',compact('EndTypes','SubTypese'));

                // }
                // else 
                // {
                //     return view('errors.403');
                // }

            }//endAuth
            else
            {
                return redirect('/login');
            }

        }//endtry
        catch(Exception $e) 
        {
            return redirect('/login');
        }
    }
    public function showendtype($id)
    {
        try
        {
            if (Auth::check())
            {
                // if(Auth::user()->can('show-sub_activities'))
                // {

                        Session::put('SubType_id', $id);                    
                        $EndTypes=EndType::where('SubType_Id',$id)->get();
                        $SubTypese=SubType::select('EngName','id')->lists('EngName','id');
                        return view('endtypes.index',compact('EndTypes','SubTypese'));  
                // }
                // else 
                // {
                //     return view('errors.403');
                // }
            }//endAuth
            else
            {
                return redirect('/login');
            }
        }//endtry
        catch(Exception $e) 
        {
            return redirect('/login');
        }   
    }
    public function checkendtypename()
    {
    
        $EndTypename=Input::get('engname');
        $EndType=EndType::where('EngName','=',$EndTypename)->first();
        if($EndType==[])     
            $isAvailable = true;
        else
            $isAvailable = false;
        return \Response::json(array('valid' =>$isAvailable,));
    }
    public function checkendtypeengname()
    {
    
        $EndTypename=Input::get('name');
        $EndType=EndType::where('Name','=',$EndTypename)->first();
        if($EndType==[])     
            $isAvailable = true;
        else
            $isAvailable = false;
        return \Response::json(array('valid' =>$isAvailable,));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try
        {
            if (Auth::check())
            {
                // if(Auth::user()->can('show-Marketing_Activity'))
                // {
                        $EndType = New EndType;
                        $EndType->Name=Input::get('name');
                        $EndType->EngName=Input::get('engname');
                        $EndType->SubType_Id =Input::get('subType_id'); 
                        $EndType->save();
                        $SubTypeid=Session::get('SubType_id');
                       // dd($SubTypeid);
                        if($SubTypeid!=null)
                            return redirect('/showendtype/'.$SubTypeid);
                        else       
                            return redirect('/endtypes');         
                        
                // }
                // else 
                // {
                //     return view('errors.403');
                // }

            }//endAuth
            else
            {
                return redirect('/login');
            }

        }//endtry
        catch(Exception $e) 
        {
            return redirect('/login');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update()
    {
        try
        {
            if (Auth::check())
            {
                // if(Auth::user()->can('show-Marketing_Activity'))
                // {
                        $EndTypeid = Input::get('pk');  
                        $column_name = Input::get('name');
                        $column_value = Input::get('value');   
                        $EndType = EndType::whereId($EndTypeid)->first();
                        $EndType-> $column_name=$column_value;
                        if($EndType->save())
                            return \Response::json(array('status'=>1));
                        else 
                            return \Response::json(array('status'=>0));

                // }
                // else 
                // {
                //     return \Response::json(array('status'=>'You do not have permission.'));
                // }

            }//endAuth
            else
            {
                return redirect('/login');
            }

        }//endtry
        catch(Exception $e) 
        {
            return redirect('/login');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try
        {
            if (Auth::check())
            {
                // if(Auth::user()->can('show-Marketing_Activity'))
                // {
                        $EndType=EndType::find($id);
                        $EndType->delete();
                        $SubTypeid=Session::get('SubType_id');
                        if($SubTypeid!=null)
                            return redirect('/showendtype/'.$SubTypeid);
                        else       
                            return redirect('/endtypes');
                // }
                // else 
                // {
                //     return view('errors.403');
                // }

            }//endAuth
            else
            {
                return redirect('/login');
            }

        }//endtry
        catch(Exception $e) 
        {
            return redirect('/login');
        }
    }
}
