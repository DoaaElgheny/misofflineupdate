<?php

namespace App\Http\Controllers;


use App\Category;
use Auth;
use App\Http\Requests;
use App\User;
use App\Gps;
use DB;
use App\Http\Controllers\Controller;
use Request;
use Excel;
use File;
use Illuminate\Support\Facades\Response;
use Validator;
use Illuminate\Support\Facades\Redirect;
use Input;
use App\Item;
use App\Purchase;
use \PDF;
use App\District;
use App\Contractor;
use App\RetailersReview;
use App\Retailers;
use App\Product;


class ExportInventoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


public function SaveTypes(Request $request)
{
 
   $query='select * From contractors where ';
   $query=$query.Input::get('dodo');

  $awe = DB::select($query);
  // $awe=Contractor::where($asd["id"], '=', $asd["value"] AND 'Name', '=', 'م / صادق وديع');

dd($awe);
}

public function GetContAtr()
{
  $contractors= DB::select('show columns from contractors');
  $a=[];
  $e=[];
  foreach ($contractors as $value) {
    if($value->Field!="created_at" && $value->Field!="updated_at")
    {
    $type=DB::connection()->getDoctrineColumn('contractors',$value->Field)->getType()->getName();
    array_push($a,array('id' =>$value->Field ,'label' =>$value->Field,'type' =>$type ));
    array_push($e,array('id' =>$value->Field ,'field' =>$value->Field,'type' =>$type,"input"=>"text"));
   
      
  }
  }
  $data=[];
  $data["a"]=$a;
  $data["e"]=$e;

  return $data;
}
    public function Contractorasd()
    {
      $District=District::all();
      $asd=[];
      foreach ($District as  $value) {
        array_push($asd,  $value->Name);
        # code...
      }
      // dd($asd);
      return view('emails.ContractorReport',compact('$asd'));
    }
 public function showpdf()
 {
//   $filename = 'app\k.pdf';
// $path = storage_path($filename);

// return Response::make(file_get_contents($path), 200, [
//     'Content-Type' => 'application/pdf',
//     'Content-Disposition' => 'inline; filename="'.$filename.'"'
// ]);
 }
  public function view()
    {
       // $document_name = write your sql query here.
       //   if($document_name){
                  $file = 'C:\xampp\htdocs\cemexmarketring\storage\app\k.xlsx';
                if (file_exists($file)){

                   $ext =File::extension($file);
                  
                    if($ext=='pdf'){
                        $content_types='application/pdf';
                       }elseif ($ext=='doc') {
                         $content_types='application/msword';  
                       }elseif ($ext=='docx') {
                         $content_types='application/vnd.openxmlformats-officedocument.wordprocessingml.document';  
                       }elseif ($ext=='xls') {
                         dd("Dkfo");
                         $content_types='application/vnd.ms-excel';  
                       }elseif ($ext=='xlsx') {
                        
                         $content_types='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';  
                       }elseif ($ext=='txt') {
                         $content_types='application/octet-stream';  
                       }
                   
                    return response(file_get_contents($file),200)
                           ->header('Content-Type',$content_types);
                                                             
                }else{
                 exit('Requested file does not exist on our server!');
                }

           // }else{
           //   exit('Invalid Request');
           // } 
    }
 public  function importPurchesIN()
 {
  try {

   $temp= Request::get('submit'); 
   if(isset($temp))

 { 
   $filename = Input::file('file')->getClientOriginalName();
     $Dpath = base_path();
     $upload_success =Input::file('file')->move( $Dpath, $filename);
       Excel::load($upload_success, function($reader)
       {   
      $results = $reader->get()->all();

       foreach ($results as $data)
        {    
       $purchase  =Purchase::where('PurchesNo',$data["purchesno"])
       ->where('Type','In')
       ->first();

       if($purchase==[] || $purchase==Null)
       {
       
       $purchase= New Purchase; 
       $purchase->PurchesNo=$data["purchesno"];
       $purchase->Date=date("Y-m-d",strtotime($data["date"]));
       $purchase->Type='In';
       $purchase->users_id=Auth::user()->id;     
       $purchase->Warehouse_iD=$data["warehouse"];
       $purchase->Description=$data["notes"];
       $purchase->save();
       }
            $new_item=Item::where('Code',$data["itemid"])->first();
            if ( $new_item !=null ||  $new_item!=[])
            {
            $purchase->getpurchaseitem()->attach($data["itemid"], ['quantity'=>$data["quantity"]]);

          
  //We should update Stock Quantity Be Plus When Type Be in decrease when be out
              //Doaa Elgheny 17/10/2016
           $itemqantity =DB::table('datawarehouse_items')
                        ->where('datawarehouse_id', '=',$data["warehouse"])
                        ->where('item_id', '=',$data["itemid"])
                       ->select('quantityinstock')
                       ->pluck('quantityinstock');
                  
                    if ($itemqantity == null)//Msh mogod nd5al Wa7d gdid New Row at datawarehouse_items
                    {
                     
                    //insert New Row
        $gpsData =DB::table('datawarehouse_items')->where('item_id','=',$new_item->id)
                       ->where('datawarehouse_id','=',$data["warehouse"])
                       ->first();

                     $itemattch=Item::whereId($new_item->id)->first();              
$itemattch->getDatawerehouseitem()->attach($data["warehouse"],["quantityinstock" =>$data["quantity"]]);
                     
                        //Update Items 

                       $column_name ='totalmstock';
                       $ItemData =Item::whereId($new_item->id)->first();
                      
                      $ItemData->$column_name=$data["quantity"];
                      $ItemData->save();
                      
                       }
                  else //Update in Exsits items and datawarehouse_items
                    {

             
                       $NewQuantity=$itemqantity[0]+$data["quantity"];

                       
                         //Update Items 
                     
                       $column_name ='totalmstock';
                       $ItemData =Item::whereId($new_item->id)->first();
                      
                      $ItemData->$column_name=$NewQuantity;
                      $ItemData->save();
                    //  dd('ddddd');
                    //update datawarehouse_items
                       $gpsData =DB::table('datawarehouse_items')->where('item_id','=',$data["itemid"])
                       ->where('datawarehouse_id','=',$data["warehouse"])
                       ->first();
                      // dd($i);
                     $itemattch=Item::whereId($data["itemid"])->first();
                      
                      $itemattch->getDatawerehouseitem()->detach();
                   //  dd($NewQuantity);
                     
                      $NewData=(int)$data["warehouse"];

                       $itemattch->getDatawerehouseitem()->attach($NewData,["quantityinstock" => $NewQuantity]);
                     

              }
            }
           
else
{

  Redirect::to('/ExportInventory')->with('message', 'الصنف غير موجود '+$data["itemid"]);
   return redirect('/ExportInventory');
}
       
            // }
        }
 
      });


  }








     

      }
    catch(Exception $e) 
    {
    return redirect('/login');
    }
 }

  public function add() {
 
    $file = Request::file('filefield');
    $extension = $file->getClientOriginalExtension();
    \Storage::disk('local')->put($file->getClientOriginalName(),  File::get($file));
    $entry=User::find(1631);
    $entry->mime = $file->getClientMimeType();   
    $entry->originalfilename = $file->getClientOriginalName();
    $entry->FileName= $file->getClientOriginalName();
    $entry->save();
    return redirect('/ExportInventory');
    
  }

public function get(){
 $entry=User::find(1631);
// dd(storage_path().'/app'.$entry->FileName);
  $path = storage_path().'/app'.'/'.$entry->FileName;

return response()->download($path);

  
    
  }

public  function importPurchesOUT()
 {
  try {

   $temp= Request::get('submit'); 
   if(isset($temp))

 { 
   $filename = Input::file('file')->getClientOriginalName();
     $Dpath = base_path();
     $upload_success =Input::file('file')->move( $Dpath, $filename);
       Excel::load($upload_success, function($reader)
       {   
      $results = $reader->get()->all();

     foreach ($results as $data) //to check all quantity in sheet and
        { 
         $new_item=Item::where('Code',$data["itemid"])->first();
         if ( $new_item !=null ||  $new_item!=[])
            {
              $itemqantity =DB::table('datawarehouse_items')
              ->where('datawarehouse_id', '=',$data["warehouse"])
              ->where('item_id', '=',$data["itemid"])
              ->select('quantityinstock')
              ->pluck('quantityinstock');
              $NewQuantity=$itemqantity[0]-$data["quantity"];

              if($NewQuantity<0)
              {
                //flash Message
                Redirect::to('/ExportInventory')->with('message', 'الصنف غير موجود '+$data["itemid"]);
   return redirect('/ExportInventory');
              }

            }
            else
            {
//flash Message
              Redirect::to('/ExportInventory')->with('message', 'الصنف غير موجود '+$data["itemid"]);
   return redirect('/ExportInventory');
            }


        }




       foreach ($results as $data)
        {  

       $purchase  =Purchase::where('PurchesNo',$data["purchesno"])
       ->where('Type','Out')
       ->first();

       if($purchase==[] || $purchase==Null)
       {
       
       $purchase= New Purchase; 
       $purchase->PurchesNo=$data["purchesno"];
       $purchase->Date=date("Y-m-d",strtotime($data["date"]));
       $purchase->Type='Out';
       $purchase->users_id=Auth::user()->id;     
       $purchase->Warehouse_iD=$data["warehouse"];
       $purchase->Description=$data["notes"];
       $purchase->save();
       }

            $purchase->getpurchaseitem()->attach($data["itemid"], ['quantity'=>$data["quantity"]]); 

           $itemqantity =DB::table('datawarehouse_items')
                        ->where('datawarehouse_id', '=',$data["warehouse"])
                        ->where('item_id', '=',$data["itemid"])
                       ->select('quantityinstock')
                       ->pluck('quantityinstock');
                  
                   
                       $NewQuantity=$itemqantity[0]-$data["quantity"];//Update Items 
                       $column_name ='totalmstock';
                       $ItemData =Item::whereId($new_item->id)->first();
                      $ItemData->$column_name=$NewQuantity;
                      $ItemData->save();
                    //update datawarehouse_items
            $gpsData =DB::table('datawarehouse_items')->where('item_id','=',$new_item->id)
                       ->where('datawarehouse_id','=',$data["warehouse"])
                       ->first();
                     $itemattch=Item::whereId($new_item->id)->first();
                      $itemattch->getDatawerehouseitem()->detach();
                      $NewData=(int)$data["warehouse"];
                       $itemattch->getDatawerehouseitem()->attach($NewData,["quantityinstock" => $NewQuantity]);
            
           

       
  }  
       
 
      });


  }
     
}
    catch(Exception $e) 
    {
    return redirect('/login');
    }
 }
    public function index()
    {
          try {
          

 // if (Auth::check())
 //  {
     return view('ExportInventory.ExportInventory');
//    }//endAuth

// else
// {
// return redirect('/login');
// }

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
       
    }
public function exportDataware_Items()
{
   $exportbtn= input::get('submit'); 

    if(isset($exportbtn))
    { 
  
      Excel::create('datawarehouseitemsfile', function($excel)
       { 

        $excel->sheet('datawarehouseitemssheet',function($sheet)
        {        

          $sheet->appendRow(1, array('id',
               'quantityinstock','item_id','datawarehouse_id'));
          $data=[];

 $Items=DB::table('datawarehouse_items')->get();
    foreach ($Items as $Item) {
    array_push($data,array(

     $Item->id,
      $Item->quantityinstock,
      $Item->item_id,
      $Item->datawarehouse_id

      ));
     
     
}

  $sheet->fromArray($data, null, 'A2', false, false);
});
})->download('xls');
                           
        }
}
public function exportPurches()
{
  $exportbtn= input::get('submit'); 

    if(isset($exportbtn))
    { 
  
      Excel::create('Purchesfile', function($excel)
       { 

        $excel->sheet('Purchessheet',function($sheet)
        {        

          $sheet->appendRow(1, array('id',
               'Date','users_id','ActivityitemsID','Warehouse_iD','Type','RelatedDecomentLink','PurchesNo','Description'));
          $data=[];

 $Items=Purchase::all();
    foreach ($Items as $Item) {
    array_push($data,array(

     $Item->id,
      $Item->Date,
      $Item->users_id,
      $Item->ActivityitemsID,
       $Item->Warehouse_iD,
      $Item->Type,
      $Item->RelatedDecomentLink,
       $Item->PurchesNo,
      $Item->Description

      ));
     
     
}

  $sheet->fromArray($data, null, 'A2', false, false);
});
})->download('xls');
                           
        }
}
public function importItems()
{

   $temp= Request::get('submit'); 


   if(isset($temp))

 { 


   $filename = Input::file('file')->getClientOriginalName();
     

     $Dpath = base_path();


     $upload_success =Input::file('file')->move( $Dpath, $filename);
   

       Excel::load($upload_success, function($reader)
       {   
     
         


      $results = $reader->get()->all();

      // $Items=Item::truncate();


     DB::table('items')->delete();
       foreach ($results as $data)
        {    

          $Items=new Item();

         
           
          $Items->Name=$data["name"];
          $Items->EngName=$data["engname"];
          $Items->totalmstock=$data["totalmstock"];
          $Items->brand_id=$data["brand_id"];
          $Items->Code=$data["code"];
          $Items->category_id=$data["category_id"];
          $Items->MinLimit=$data["minlimit"];
          $Items->ImageItem=$data["imageitem"];
          $Items->save();
        
           $ExistID= (int)$data["id"];
             $update=DB::table('items')
            ->where('Code', $data["code"])
            ->update(['id' =>$ExistID]);
   
        }
 
      });


  }
       return redirect('/ExportInventory');  
     

}
public function importDataware_Items()
{
$temp= Request::get('submit'); 


   if(isset($temp))

 { 


   $filename = Input::file('file')->getClientOriginalName();
     

     $Dpath = base_path();


     $upload_success =Input::file('file')->move( $Dpath, $filename);
   

       Excel::load($upload_success, function($reader)
       {   
     
         


      $results = $reader->get()->all();

      // $Items=Item::truncate();


     DB::table('datawarehouse_items')->delete();
       foreach ($results as $data)
        {    

          

 $itemattch=Item::whereId($data["item_id"])->first();
                     
$itemattch->getDatawerehouseitem()->attach($data["datawarehouse_id"],["quantityinstock" => $data["quantityinstock"]]);

        // dd($itemattch->id);
           $ExistID= (int)$data["id"];
             $update=DB::table('datawarehouse_items')
             ->where('item_id', $data["item_id"])
             ->where('datawarehouse_id', $data["datawarehouse_id"])
            ->update(['id' =>$ExistID]);
   
        }
 
      });


  }
       return redirect('/ExportInventory');  
     

}
public function importPurches()
{


 $temp= Request::get('submit'); 


   if(isset($temp))

 { 


   $filename = Input::file('file')->getClientOriginalName();
     

     $Dpath = base_path();


     $upload_success =Input::file('file')->move( $Dpath, $filename);
   

       Excel::load($upload_success, function($reader)
       {   
     
         


      $results = $reader->get()->all();

      // $Items=Item::truncate();


     DB::table('purches')->delete();
       foreach ($results as $data)
        {    

          $purches=new Purchase();

          // dd($data);
           
          $purches->Date=$data["date"];
          $purches->users_id=$data["users_id"];
          $purches->ActivityitemsID=$data["activityitemsid"];
          $purches->Warehouse_iD=$data["warehouse_id"];
          $purches->Type=$data["type"];
          $purches->RelatedDecomentLink=$data["relateddecomentlink"];
          $purches->PurchesNo=$data["purchesno"];
          $purches->Description=$data["description"];
          $purches->save();
        
           $ExistID= (int)$data["id"];
             $updatenumberofvisits=DB::table('purches')
            ->where('id',$purches->id)
            ->update(['id' =>$ExistID]);
   
        }
 
      });


  }
       return redirect('/ExportInventory');  
  



}
public function importitemPurches()
{
$temp= Request::get('submit'); 


   if(isset($temp))

 { 


   $filename = Input::file('file')->getClientOriginalName();
     

     $Dpath = base_path();


     $upload_success =Input::file('file')->move( $Dpath, $filename);
   

       Excel::load($upload_success, function($reader)
       {   
     
         


      $results = $reader->get()->all();

      // $Items=Item::truncate();


     DB::table('item_purches')->delete();
       foreach ($results as $data)
        {    

          

 $ItemData=Item::whereId($data["item_id"])->first();
                     
 $ItemData->getitempurches()->attach($data["purches_id"], ['quantity'=>$data["quantity"]]);
  

        // dd($itemattch->id);
           $ExistID= (int)$data["id"];
             $update=DB::table('item_purches')
             ->where('item_id', $data["item_id"])
             ->where('purches_id', $data["purches_id"])
             ->where('quantity', $data["quantity"])
            ->update(['id' =>$ExistID]);
   
        }
 
      });


  }
       return redirect('/ExportInventory');  
     

}

public function exportitemPurches()
{
$exportbtn= input::get('submit'); 

    if(isset($exportbtn))
    { 
  
      Excel::create('Purchesitemfile', function($excel)
       { 

        $excel->sheet('PurchesItemsheet',function($sheet)
        {        

          $sheet->appendRow(1, array('id',
               'quantity','purches_id','item_id'));
          $data=[];

 $Items=DB::table('item_purches')->get();


    foreach ($Items as $Item) {


    array_push($data,array(

       $Item->id,
      $Item->quantity,
      $Item->purches_id,
      $Item->item_id

      ));
    
     
}

  $sheet->fromArray($data, null, 'A2', false, false);
});
})->download('xls');
                           
        }
}


    public function exportItems()
    {
 

        $exportbtn= input::get('submit'); 

    if(isset($exportbtn))
    { 
  
      Excel::create('Itemfile', function($excel)
       { 

        $excel->sheet('Itemssheetname',function($sheet)
        {        

          $sheet->appendRow(1, array('id',
               'Name','EngName','totalmstock','brand_id','Code','category_id','MinLimit','ImageItem'));
          $data=[];

 $Items=Item::all();


    foreach ($Items as $Item) {


    array_push($data,array(

     $Item->id,
      $Item->Name,
      $Item->EngName,
      $Item->totalmstock,
      $Item->brand_id,
      $Item->Code,
      $Item->category_id,
      $Item->MinLimit,
      $Item->ImageItem

      ));
    
     
}

  $sheet->fromArray($data, null, 'A2', false, false);
});
})->download('xls');
                           
        }
      }
   
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
          
      
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }
    
        /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     

    public function update(Request $request, $id)
    {
               try {

        
            $RetailersReview=RetailersReview::find($id);
            $RetailersReview->Has_Truncks=Input::get('Has_Truncks');
            $RetailersReview->Workers=Input::get('Workers');   
            $RetailersReview->No_Truncks=Input::get('No_Truncks'); 
            $RetailersReview->Has_Used_Computer=Input::get('Has_Used_Computer');
            $RetailersReview->Equipment=Input::get('Equipment');  
            $RetailersReview->Status=Input::get('Status');
            $RetailersReview->Call_Status=Input::get('Call_Status');       
            $RetailersReview->Area=Input::get('Area');
            $RetailersReview->Cont_Type=Input::get('Cont_Type');
            $RetailersReview->save();              
            return redirect('/RetailersReview');


}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {


 try {
        $RetailersReview=RetailersReview::find($id);
        $RetailersReview->delete();
        return redirect('/RetailersReview');

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
 



    }
}
