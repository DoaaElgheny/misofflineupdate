<?php

namespace App\Http\Controllers;
use Auth;
use App\Http\Requests;
use DB;
use App\Http\Controllers\Controller;
use Request;
use Excel;
use File;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Input;
use Validator;
use Illuminate\Support\Facades\Redirect;
use App\User;
use App\Gps;
use App\Product;
use App\Company;
use App\sale_product;
use App\sale_product_details;
use App\Contractor;
use App\Price;
use Session;
use Geocode;
use App\Order;
use App\Activity;
use Image;
use PHPExcel; 
use App\Government;
use App\District;
use App\Cafe;
use App\CafeSigns;
use App\Sign;
use Carbon;
class ReportController extends Controller
{
public function __construct()
   {
       $this->middleware('auth');
   }
 public function exportreportpro(Request $request)
   {  
    $exportbtn= Input::get('pro'); 

   // if(isset($exportbtn))
   // { 
  
      Excel::create('signsfile', function($excel)
       { 

        $excel->sheet('signsfile',function($sheet)
        {        

          $sheet->appendRow(1, array(
               'Name','NoDay','Hour','totalcontractor'
));
          $data=[];

   $stratdate="2018-08-14";
   $enddate="2018-08-26";
                            
    $start_time = "04:00:00";
    $end_time = "10:00:00";

    $allusers=Gps::join('users','users.id','=','gps.user_id')
    ->whereBetween('NDate',array($stratdate,$enddate))
    ->select(DB::raw('distinct(gps.user_id) as id'),'Name')->get();
$aa=[];
foreach ($allusers as $value) {
	$cont =Gps::whereBetween('NDate',array($stratdate,$enddate))
                                    ->select(array(DB::raw('count(distinct(Contractor_Code))  as count')))
                                    ->where('user_id',$value->id)
                                    ->first();

            $hour_Dayssystem=Gps::where('user_id',$value->id)
                        ->whereBetween('NDate',array($stratdate,$enddate))
                       // ->whereBetween('NTime',[$start_time ,$end_time])
                        ->select(DB::raw('count(distinct(NDate)) as NoDay'))
                        ->first();


           $b =Gps::whereBetween('NDate',array($stratdate,$enddate))
                 ->select(array(DB::raw('count(distinct(Contractor_Code))  as count')),'NDate')
                   ->where('user_id',$value->id)
                  
                       ->get();

$totalContractort=0;
foreach ($b as $key) {
$totalContractort+=$key->count;
} 



             $day_system=Gps::where('user_id',$value->id)
                        ->whereBetween('NDate',array($stratdate,$enddate))
                        ->select(DB::raw('distinct(NDate) as NDate'))
                        ->get();
                          $No_hours_day=[];
        $sumofmonth=0;
        foreach ($day_system as  $key) {
            $times_system=DB::table('gps')->where('user_id',$value->id)
                        ->where('NDate',$key->NDate)
                        ->select('NTime')
                        ->lists('NTime');
         $othervisit= DB::table('cafesigns')->where('cafesigns.user_id',$value->id)
                                 ->where('NDate',$key->NDate)
                                ->select('NTime')
                                ->lists('NTime');
       $totaltime=array_merge($times_system,$othervisit);
       sort($totaltime);
                $sum=0;
                for ($i=0; $i < count($totaltime)-1; $i++) { 
                    $time1 = Carbon::parse($key->NDate.$totaltime[$i]);
                    $time2 = Carbon::parse($key->NDate.$totaltime[$i+1]);
                    $hour = $time2->hour-$time1->hour;
                    if($hour<3)
                    {
                       $sum+=$hour;
                    }
                   
                }
                if($sum==0)
                {
                    $sum=1;
                }
               $sumofmonth+=$sum;
        }
        array_push($aa,array('Name' => $value->Name, 'NoDay' => $hour_Dayssystem->NoDay,'Hour' => $totalContractort,'totalcontractor'=>$cont->count));
}



    foreach ($aa as $Sign) {


    array_push($data,array(
      $Sign["Name"],
      $Sign["NoDay"],
      $Sign["Hour"],
      $Sign["totalcontractor"]
   

      ));
  
}
  $sheet->fromArray($data, null, 'A2', false, false);
});
})->download('xls');
                           
       // }

                          
                            }     

public function GpsDaliyReport(Request $request)
{

   $stratdate="2017-07-01";
   $enddate="2017-07-15";
                            
    $start_time = "04:00:00";
    $end_time = "10:00:00";
    $allusers=Gps::join('users','users.id','=','gps.user_id')
    ->whereBetween('NDate',array($stratdate,$enddate))
    ->select(DB::raw('distinct(gps.user_id) as id'),'Name')->get();
$aa=[];
foreach ($allusers as $value) {
	$cont =Gps::whereBetween('NDate',array($stratdate,$enddate))
                                    ->select(array(DB::raw('count(distinct(Contractor_Code))  as count')))
                                    ->where('user_id',$value->id)
                                    ->first();

    $hour_Dayssystem=Gps::where('user_id',$value->id)
                        ->whereBetween('NDate',array($stratdate,$enddate))
                        //->whereBetween('NTime',[$start_time ,$end_time])
                        ->select(DB::raw('count(distinct(NDate)) as NoDay'))
                        ->first();
                        $day_system=Gps::where('user_id',$value->id)
                        ->whereBetween('NDate',array($stratdate,$enddate))
                        ->select(DB::raw('distinct(NDate) as NDate'))
                        ->get();
                          $No_hours_day=[];
        $sumofmonth=0;
        foreach ($day_system as  $key) {
            $times_system=DB::table('gps')->where('user_id',$value->id)
                        ->where('NDate',$key->NDate)
                        ->select('NTime')
                        ->lists('NTime');
         $othervisit= DB::table('cafesigns')->where('cafesigns.user_id',$value->id)
                                 ->where('NDate',$key->NDate)
                                ->select('NTime')
                                ->lists('NTime');
       $totaltime=array_merge($times_system,$othervisit);
       sort($totaltime);
                $sum=0;
                for ($i=0; $i < count($totaltime)-1; $i++) { 
                    $time1 = Carbon::parse($key->NDate.$totaltime[$i]);
                    $time2 = Carbon::parse($key->NDate.$totaltime[$i+1]);
                    $hour = $time2->hour-$time1->hour;
                    if($hour<3)
                    {
                       $sum+=$hour;
                    }
                   
                }
                if($sum==0)
                {
                    $sum=1;
                }
               $sumofmonth+=$sum;
        }
        array_push($aa,array('Name' => $value->Name, 'NoDay' => $hour_Dayssystem->NoDay,'Hour' => $sumofmonth,'totalcontractor'=>$cont->count));
}


                      }


// public function GpsDaliyReport788(){ 
//  // if (Auth::check()){

//     // $date =Input::get('date');
//      $date ="2017-07-12";
//          $gps = Gps::select(array('Username','type' ,'Contractor_Code',DB::raw('count(type)  as visits')))
//                                     ->where('NDate','=',$date)
//                                     ->join('users', 'users.id', '=', 'gps.user_id')
//                                     ->groupBy('Username','Contractor_Code')
//                                     ->orderby('Username')
//                                    ->get();
// $data=array();
// $data['username']=0;
//    foreach ($gps as $key) {
//              if($key->type ==='حصر كميات')
//                    {

//                             $user_id=user::select('id')->where('Username',$key->Username)->pluck('id')->first();
      
//                             $togps = Gps::select(DB::raw('count(*)  as visits'))
//                                    ->where('NDate','=',$date)
//                                    ->where('user_id',$user_id)
//                                    ->where('Contractor_Code',$key->Contractor_Code)
//                                    ->where('type','حصر كميات')
//                                    ->first();
                                

//                             if($key->visits >  $togps->visits)
//                                 {
//                                       $key->visits= $togps->visits;
//                                 }
//                      }
//             if(($key->type ==='المبيعات'&& $key->visits===2)||($key->type ==='تسويق'&& $key->visits===2))
//                            {

//                                   $key->visits=$key->visits/2;
//                            }

//             if((array_search($key->Username,array_keys($data))))
//                       {
//                            $old=$data[$key->Username];
//                            $data[$key->Username]= $old+$key->visits; 
                                                                    
//                        }
//                   else{
//                         $data[$key->Username]=$key->visits;      

//                       }                                                      
                            
 

//                     }

//  unset($data['username']) ;


//                                       $a=[];
//                                       foreach ($data as $key => $value) {
//                                         array_push($a,array(
//                                           'label' => $key,
//                                           'y' => $value
//                                           ));
                                       
//                                       }
                                           

//                        $total_contractor = Gps::select(array('Username',DB::raw('count(distinct(Contractor_Code))  as visits')))
//                                    ->where('NDate','=',$date)
//                                    ->whereIn('type',['المبيعات','تسويق','حصر كميات'])
//                                    ->join('users', 'users.id', '=', 'gps.user_id')
//                                    ->groupBy('user_id')
//                                    ->orderby('Username')
//                                    ->get();
                       
//                                    $b=[];
//                                   foreach ($total_contractor as  $value) {
                             
//                                     array_push($b,array(
//                                       'label' => $value->Username,
//                                       'y' => $value->visits
//                                       ));
                                   
//                                   }
//                              $dis=[];
                            
//                              foreach ($b as  $value) {
//                              	$total_Gps = Gps::join('users', 'users.id', '=', 'gps.user_id')
//                              	   ->where('NDate','=',$date)
//                                    ->where('Username',$value["label"])
//                                    ->select('gps.*','users.Username')
//                                    ->get();
//                                 $resend =0;
//                                 for ($i=0; $i <count($total_Gps)-2 ; $i++) { 
                                	
// 								       $lat1= $total_Gps[$i]->Lat;
// 								       $lng1=$total_Gps[$i]->Long;
// 								       $lat2=$total_Gps[$i+1]->Lat;
// 								       $lng2=$total_Gps[$i+1]->Long;
// 						 if(($lat1== null && $lng1 == null) || ($lat2 == null &&  $lng2 == null))
// 								       {

//                                      continue;
 

// 								       }
// 								       else if($lat2 == null &&  $lng2 == null && $lat1== null && $lng1 == null)
// 								       {
								   
// $resend +=0;
// 								       }
// else {


// 								 $pi80 = M_PI / 180;
// 								  $lat1 *= $pi80;
// 								  $lng1 *= $pi80;
// 								  $lat2 *= $pi80;
// 								  $lng2 *= $pi80;
								 
// 								  $r = 6378137; // mean radius of Earth in km
// 								  $dlat = $lat2 - $lat1;
// 								  $dlng = $lng2 - $lng1;
// 								  $an = sin($dlat / 2) * sin($dlat / 2) + cos($lat1) * cos($lat2) * sin($dlng / 2) * sin($dlng / 2);
// 								  $c = 2 * atan2(sqrt($an), sqrt(1 - $an));
// 								  $resend += $r * $c;

// 								  }
//                                 }

//                                 array_push($dis,array(
//                                       'label' => $value["label"],
//                                       'y' => $resend/1000
//                                       ));



//                              }

//                         dd($dis);
// $data = array();
// $data['totalvisit']=$a;
// $data['totalcontractor']=$b;
                                  
//                           //  dd(json_encode($data)) ;  
//                       return Response::json($data,  200, [], JSON_NUMERIC_CHECK);
//               }
//           else{   
//                   return Redirect::to('/login');             
//               }
        
// }


 public function ShowPromoterDaliyReport()
                      {
                           $users=User::lists('Name','id');
                           $alldata=[];
            return view('Reportsnew.DailyReportPromoters',compact('users','alldata'));

                      }
 public function PromoterDaliyReport()
                      {
                        
  $users=User::lists('Name','id'); 
                       $stratdate=input::get('Start_Date');
// $stratdate="2018-01-29";
     $usersall=input::get('users');
     $alldata=[];
                            
if ($usersall[0]==='all') {



   ///calculate Contractors
  $countContractors =Gps::join('users', 'users.id', '=', 'gps.user_id')->where('gps.NDate','=',$stratdate)
                                    ->select(array('users.Name','users.id','gps.NDate AS DATE','gps.NTime',DB::raw('count(distinct(Contractor_Code))  as count')))
                                    ->groupBy('user_id')
                                    ->orderby('NTime')
                                    ->get();
                           



                                    ////calculateVisits

$cou=0;
$visists=0;
$amountArr=[];
$VisitsArr=[];
$TotalAmountArr=[];
$TotalDistanceArr=[];


 $All=Gps::join('users','users.id','=','gps.user_id')
    ->where('gps.NDate',$stratdate)
    ->select(DB::raw('distinct(gps.user_id) as id'),'Name')->get();

foreach ($All as $value) {
                    $TotalVistits=Gps::join('users', 'users.id', '=', 'gps.user_id')->where('gps.NDate',$stratdate)
                
                                    
                                    ->where('gps.user_id',$value->id)
                                     ->select('gps.*','users.Name AS UName')
                                    ->get(); 
if(count($TotalVistits)==1)
{

 $countVistits=1;

  array_push($VisitsArr,array('id'=>$TotalVistits[0]->user_id,'name'=>$TotalVistits[0]->UName,'Count'=> $countVistits));
}

else
{

                                    for($i=0; $i<count($TotalVistits)-1; $i++) {
                                      

                                      if($TotalVistits[$i]->Contractor_Code == $TotalVistits[$i+1]->Contractor_Code)
                                      {
                                
                                                 $visists++;               

                                                    
                                                    $a = strtotime($TotalVistits[$i+1]->NTime);
                                                    $b = strtotime($TotalVistits[$i]->NTime);
                                                   
 
        $c = $a - $b;
        $diff=date("H:i", $c);
      
                                                 if($diff >="00::05")

                                                 {
                                                  $cou++;

                                                      

                                                 }

                               



                                      }
                                       

                                                       
                                                     } 

                                                     $countVistits=count($TotalVistits)-$cou; 

                                                  array_push($VisitsArr,array('id'=>$TotalVistits[$i]->user_id,'name'=>$TotalVistits[$i]->UName,'Count'=> $countVistits));

                                                      

}


//array_push($amountArr, $TotalVistits[$i]->Amount);
/////calculateAmount
$amountArr=[];
  $TotalOrders=Gps::join('users', 'users.id', '=', 'gps.user_id')->join('orders','orders.gps_id','=','gps.id')->where('gps.NDate',$stratdate)
  ->where('gps.user_id',$value->id)
                                     ->select('gps.*','users.Name AS UName','orders.Product_id','orders.Amount')
                                    ->get(); 
                                 
                                    
  for($i=0; $i<count($TotalOrders)-1; $i++) {

                                     
                                                    $a = strtotime($TotalOrders[$i+1]->NTime);
                                                    $b = strtotime($TotalOrders[$i]->NTime);
                                                   
 
        $c = $a - $b;
        $diff=date("H:i", $c);
  if($TotalOrders[$i]->Contractor_Code == $TotalOrders[$i+1]->Contractor_Code && $TotalOrders[$i]->Amount == $TotalOrders[$i+1]->Amount && $TotalOrders[$i]->Product_id == $TotalOrders[$i+1]->Product_id && $diff<=5 )
                                      {

array_push($amountArr, 0);
                                      }
                                      else
                                      {
                                        

                                        array_push($amountArr, $TotalOrders[$i]->Amount);
                                      }
                                     

  }

$countAmount=array_sum($amountArr)+$TotalOrders->last()->Amount;

            array_push($TotalAmountArr,array('id'=>$TotalOrders[$i]->user_id,'name'=>$TotalOrders[$i]->UName,'CountAmount'=> $countAmount));




///calculateDistance
$arraydis=[];
      for($i=0; $i<count($TotalVistits)-1; $i++) {

$loc1        = new \Geodistance\Location((float)$TotalVistits[$i]->Lat,(float)$TotalVistits[$i]->Long);
$loc2      = new \Geodistance\Location((float)$TotalVistits[$i+1]->Lat, (float)$TotalVistits[$i+1]->Long);

$decimal_precision = 4;
$dis=\Geodistance\kilometers($loc1,$loc2,$decimal_precision); 
if($dis >= 4000)
{
 array_push($arraydis, 0.0);
}
else
{
   array_push($arraydis, $dis);
}

                                                  
}
$countDistance=array_sum($arraydis);

       array_push($TotalDistanceArr,array('id'=>$TotalVistits[$i]->user_id,'name'=>$TotalVistits[$i]->UName,'CountDistance'=> $countDistance));
}

}
else
{


    $countContractors =Gps::join('users', 'users.id', '=', 'gps.user_id')->where('gps.NDate',$stratdate)
                                    ->select(array('users.Name','users.id','gps.NDate AS DATE','gps.NTime',DB::raw('count(distinct(Contractor_Code))  as count')))
                                    ->whereIn('users.id',$usersall)
                                    ->groupBy('user_id')
                                    ->orderby('NTime')
                                    ->get();
                    


                                    ////calculateVisits

$cou=0;
$visists=0;
$amountArr=[];
$VisitsArr=[];
$TotalAmountArr=[];
$TotalDistanceArr=[];
 $All=Gps::join('users','users.id','=','gps.user_id')
 ->whereIn('users.id',$usersall)
       ->where('gps.NDate',$stratdate)
    ->select(DB::raw('distinct(gps.user_id) as id'),'Name')->get();

foreach ($All as $value) {
                    $TotalVistits=Gps::join('users', 'users.id', '=', 'gps.user_id')->whereBetween('gps.NDate',array($stratdate,$enddate))
                
                                    
                                    ->where('gps.user_id',$value->id)
                                     ->select('gps.*','users.Name AS UName')
                                    ->get(); 


if(count($TotalVistits)==1)
{

 $countVistits=1;

  array_push($VisitsArr,array('id'=>$TotalVistits[0]->user_id,'name'=>$TotalVistits[0]->UName,'Count'=> $countVistits));
}

else
{





                                    for($i=0; $i<count($TotalVistits)-1; $i++) {
                                      

                                      if($TotalVistits[$i]->Contractor_Code == $TotalVistits[$i+1]->Contractor_Code)
                                      {
                                
                                                 $visists++;               

                                                    
                                                    $a = strtotime($TotalVistits[$i+1]->NTime);
                                                    $b = strtotime($TotalVistits[$i]->NTime);
                                                   
 
        $c = $a - $b;
        $diff=date("H:i", $c);
      
                                                 if($diff >="00::05")

                                                 {
                                                  $cou++;

                                                      

                                                 }

                               



                                      }
                                       

                                                       
                                                     } 

                                                     $countVistits=count($TotalVistits)-$cou; 

                                                  array_push($VisitsArr,array('id'=>$TotalVistits[$i]->user_id,'name'=>$TotalVistits[$i]->UName,'Count'=> $countVistits));

                                                      
}


//array_push($amountArr, $TotalVistits[$i]->Amount);
/////calculateAmount
$amountArr=[];
  $TotalOrders=Gps::join('users', 'users.id', '=', 'gps.user_id')->join('orders','orders.gps_id','=','gps.id')   ->where('gps.NDate',$stratdate)
  ->where('gps.user_id',$value->id)
                                     ->select('gps.*','users.Name AS UName','orders.Product_id','orders.Amount')
                                    ->get(); 
                                    
  for($i=0; $i<count($TotalOrders)-1; $i++) {

                                     
                                                    $a = strtotime($TotalOrders[$i+1]->NTime);
                                                    $b = strtotime($TotalOrders[$i]->NTime);
                                                   
 
        $c = $a - $b;
        $diff=date("H:i", $c);
  if($TotalOrders[$i]->Contractor_Code == $TotalOrders[$i+1]->Contractor_Code && $TotalOrders[$i]->Amount == $TotalOrders[$i+1]->Amount && $TotalOrders[$i]->Product_id == $TotalOrders[$i+1]->Product_id && $diff<=5 )
                                      {

array_push($amountArr, 0);
                                      }
                                      else
                                      {
                                        

                                        array_push($amountArr, $TotalOrders[$i]->Amount);
                                      }
                                     

  }

$countAmount=array_sum($amountArr)+$TotalOrders->last()->Amount;;

            array_push($TotalAmountArr,array('id'=>$TotalOrders[$i]->user_id,'name'=>$TotalOrders[$i]->UName,'CountAmount'=> $countAmount));




///calculateDistance
$arraydis=[];
      for($i=0; $i<count($TotalVistits)-1; $i++) {

$loc1        = new \Geodistance\Location((float)$TotalVistits[$i]->Lat,(float)$TotalVistits[$i]->Long);
$loc2      = new \Geodistance\Location((float)$TotalVistits[$i+1]->Lat, (float)$TotalVistits[$i+1]->Long);

$decimal_precision = 4;
$dis=\Geodistance\kilometers($loc1,$loc2,$decimal_precision); 
if($dis >= 4000)
{
 array_push($arraydis, 0.0);
}
else
{
   array_push($arraydis, $dis);
}

                                                  
}
$countDistance=array_sum($arraydis);

       array_push($TotalDistanceArr,array('id'=>$TotalVistits[$i]->user_id,'name'=>$TotalVistits[$i]->UName,'CountDistance'=> $countDistance));
}

}

//dd($TotalDistanceArr,$TotalAmountArr,$countContractors,$VisitsArr);

for($i=0; $i<=count($countContractors)-1; $i++)
{


$keyDis = array_search($countContractors[$i]['id'], array_column($TotalDistanceArr, 'id'));


$keyVisits = array_search($countContractors[$i]['id'],array_column($VisitsArr, 'id'));

$keyAmount= array_search($countContractors[$i]['id'], array_column($TotalAmountArr, 'id'));


 array_push($alldata,array('Name'=>$countContractors[$i]->Name,'CountContractors'=>$countContractors[$i]->count,'CountDistance'=> $TotalDistanceArr[$keyDis]['CountDistance'],'CountAmount'=>$TotalAmountArr[$keyAmount]['CountAmount'],'CountVisits'=>$VisitsArr[$keyVisits]['Count'],'Date'=>$countContractors[$i]->DATE ,'Time'=>$countContractors[$i]->NTime));


  } 
 



      return view('Reportsnew.DailyReportPromoters',compact('users','alldata'));

                                  
                                  }
                 




}