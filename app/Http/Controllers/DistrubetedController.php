<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Auth;

use App\User;
use App\Gps;
use DB;
use App\Http\Controllers\Controller;

use Excel;
use File;
use Illuminate\Support\Facades\Response;
use Validator;
use Illuminate\Support\Facades\Redirect;
use Input;
use  App\EnergySource;
use App\Item;
use App\Brand;
use App\Category;
use App\Warehouse;
use App\SubActivity;
use App\Purchase;
class DistrubetedController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function SearchCairoMapItem($id)
    {

      $item=DB::table('datawarehouse_items')->where('item_id',$id)
      -> where('datawarehouse_id','3')
      ->select('Partition')->get();

            return \Response::json($item);
}
 public function SearchMapItem($id)
 {
  $item=item::where('id',$id)->
      select('Partition')->get();

      return \Response::json($item);
 }
public function ShowNewinvReport()
{
  $allCatogry=Brand::all();
  return view('StockReports.NewRepoertwithCatogry',compact('allCatogry'));

}

    public  function Marketinginventory()
    {
      return view('StockReports.Inventorywithmarketingactivity');
    }
    public function TotalflagReport()
    {
      $item=DB::table('datawarehouse_items')
         ->join('items','items.id','=','datawarehouse_items.item_id')
          ->join('flagitem','flagitem.id','=','items.flagitmtotal')
         ->join('datawarehouses','datawarehouses.id','=','datawarehouse_items.datawarehouse_id')
         ->where('datawarehouse_items.datawarehouse_id','=',1)    
         ->select('flagitem.Name',DB::raw('sum(datawarehouse_items.quantityinstock) as quantity'))
        ->groupBy('items.flagitmtotal')
         ->get();
return view('StockReports.TotalflagReport',compact('item'));
         
    }
    public function Mrketingiteminv()
    {
      $Start_Date=input::get('Start_Date');
      $End_Date=input::get('End_Date');
      $arrang=[];
      $Costitem=[];
      // array_push($arrang, var)
       $range=array( $Start_Date, $End_Date);

    
    
    
     $item_Purches=DB::table('item_purches')
         ->join('items','items.id','=','item_purches.item_id')
          ->join('purches','purches.id','=','item_purches.purches_id')
          ->join('sub_activities','sub_activities.id','=','item_purches.activity_id')
          ->wherebetween('purches.Date',$range)
          ->where('purches.Type','Out')
           ->where('purches.Warehouse_iD',1)
          
           ->select(DB::raw('sum(item_purches.quantity) as sumquantity' ),'items.Name as ItemName','sub_activities.Name as ActivityName')
           ->groupby('item_purches.activity_id','item_purches.item_id')->get();
   
          // foreach ($item_Purches as  $value) 
          // {
                   
          //   $items_plans=DB::table('items_plans')
          //   ->where('ActivityName',$value->activity_id)->where('Item_Id',$value->item_id)->first();
          //   if( $items_plans !=null)
          //   {
          //     $Cost=$items_plans->Cost/$items_plans->Quantity;
          //     $total=$Cost*($value->sumquantity);
          //     array_push($Costitem,array('itemName'=>$value->Name,'ActivityName'=>$value->ActivityName,'quantity'=>$value->sumquantity,'Cost'=>$total));
          //   }
           
            
          // }
         return $item_Purches;
    }
    public  function TotalStockReport()
    {

      $Warehouses=Warehouse::select('EngName AS Name','id')->lists('Name','id');
     
      return view('StockReports.TotalstockItem',compact('Warehouses'));

    }
  
    public function index()
    {
        try {

 if (Auth::check())
  {


    if(Auth::user()->can('show-Distrbuted'))
      {

       $Items=Item::select('EngName AS Name','id')->lists('Name','id');
      $Warehouses=Warehouse::select('EngName AS Name','id')->lists('Name','id');
      $Marktingactitvitys=SubActivity::select('EngName AS Name','id')->lists('Name','id');
      return view('StockReports.Distrbuted',compact('Items','Warehouses','Marktingactitvitys'));

      }
    else 
      {
  return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
  return redirect('/login');
    }
     
    }
    public  function Exportwithimage()
    {
ini_set('memory_limit','256M');

        $itemsarray=input::get("item_id");
        $Show=input::get("agree");

  if (input::get("Warehouse_iD")==Null && input::get("item_id")==Null)
  {
      $result=[] ; 
       $item=Item::join('item_purches','item_purches.item_id','=','items.id')
       ->Join('brands','brands.id','=','items.brand_id')
        ->Join('categories','categories.id','=','items.category_id')
        ->select('items.*','categories.Name as catName','brands.Name as bandName','items.id as item_id')
        ->groupBy(DB::raw('item_purches.item_id'))
         ->get();
         //($item);
         foreach ($item as  $value) {
          $In_items =Item::join('item_purches','item_purches.item_id','=','items.id')
        
           ->Join('purches','purches.id','=','item_purches.purches_id')

         ->where('purches.Type','In')
          ->where('item_purches.item_id',$value->id)
         ->select(DB::raw('sum(quantity) As INN'))
         ->groupBy(DB::raw('item_purches.item_id'))
         ->first();
         
         $out_items =Item::join('item_purches','item_purches.item_id','=','items.id')
         ->Join('purches','purches.id','=','item_purches.purches_id')
         ->where('purches.Type','Out')
          ->where('item_purches.item_id',$value->id)
         ->select(DB::raw('sum(quantity) As outt'))
         ->groupBy(DB::raw('item_purches.item_id'))
         ->first();

     
            if($out_items!=[]&&$In_items!=[])
            {
              $quantity=$In_items->INN-$out_items->outt;
            array_push($result,array('item_Name'=>$value->Name,'item_id'=>$value->item_id,'catName'=>$value->catName,'bandName'=>$value->bandName,'quantityIN'=>$In_items->INN,'quantityOut'=>$out_items->outt,'show'=>$Show,'quantity'=>($In_items->INN-$out_items->outt)));
            }
            else if($out_items==[]&&$In_items!=[])
            {
              $value->Image=  str_replace("/MIS/","",$value->ImageItem);
            array_push($result,array('item_Name'=>$value->Name,'item_id'=>$value->item_id,'catName'=>$value->catName,'bandName'=>$value->bandName,'quantityIN'=>$In_items->INN,'quantityOut'=>0,'show'=>$Show,'Image'=>$value->Image,'quantity'=>($In_items->INN)));
            }



}

         
   }
    else if (input::get("Warehouse_iD")!=Null   && input::get("item_id")==Null )
   
   {
   

       $result=[] ; 
       if(input::get("Catogry")==Null && input::get("Brand")==Null)
       {
          $itemids=DB::table('datawarehouse_items')
         ->join('items','items.id','=','datawarehouse_items.item_id')
          
         ->join('datawarehouses','datawarehouses.id','=','datawarehouse_items.datawarehouse_id')
         ->where('datawarehouse_items.datawarehouse_id','=',input::get("Warehouse_iD"))    
         ->select('datawarehouse_items.item_id')
        ->groupBy('datawarehouse_items.item_id')
         ->lists('datawarehouse_items.item_id');
       }
       else if(input::get("Catogry")!=Null && input::get("Brand")==Null)
       {
            $itemids=DB::table('datawarehouse_items')
         ->join('items','items.id','=','datawarehouse_items.item_id')
           ->Join('brands','brands.id','=','items.brand_id')
          ->Join('categories','categories.id','=','items.category_id')
         ->join('datawarehouses','datawarehouses.id','=','datawarehouse_items.datawarehouse_id')
         ->where('datawarehouse_items.datawarehouse_id','=',input::get("Warehouse_iD")) 
         ->where('items.category_id','=',input::get("Catogry"))     
         ->select('datawarehouse_items.item_id')
        ->groupBy('datawarehouse_items.item_id')
         ->lists('datawarehouse_items.item_id');       
       }
       else if(input::get("Catogry")!=Null && input::get("Brand")!=Null)
       {
       
     
            $itemids=DB::table('datawarehouse_items')
         ->join('items','items.id','=','datawarehouse_items.item_id')
           ->Join('brands','brands.id','=','items.brand_id')
          ->Join('categories','categories.id','=','items.category_id')
         ->join('datawarehouses','datawarehouses.id','=','datawarehouse_items.datawarehouse_id')
         ->where('datawarehouse_items.datawarehouse_id','=',input::get("Warehouse_iD")) 
         ->whereIn('items.brand_id',input::get("Brand"))     
         ->select('datawarehouse_items.item_id')
        ->groupBy('datawarehouse_items.item_id')
         ->lists('datawarehouse_items.item_id');       
       }
     

   $item=Item::join('item_purches','item_purches.item_id','=','items.id')
   ->Join('brands','brands.id','=','items.brand_id')
          ->Join('categories','categories.id','=','items.category_id')
    ->whereIn('item_purches.item_id',$itemids)
        ->select('items.*','categories.Name as catName','categories.Index_Cat','brands.Name as bandName','items.id as item_id')
        ->groupBy(DB::raw('item_purches.item_id'))
        ->orderBy(DB::raw('categories.Index_Cat'))
         ->get();

     
         foreach ($item as  $value) {

          $In_items =Item::join('item_purches','item_purches.item_id','=','items.id')
         ->Join('purches','purches.id','=','item_purches.purches_id')
         // ->Join('datawarehouse_items','datawarehouse_items.id','=','purches.Warehouse_iD')
         ->where('purches.Type','In')
          ->where('item_purches.item_id',$value->id)
           ->where('purches.Warehouse_iD',input::get("Warehouse_iD"))
         ->select(DB::raw('sum(quantity) As INN'))
         ->groupBy(DB::raw('item_purches.item_id'))
         ->first();

         $out_items =Item::join('item_purches','item_purches.item_id','=','items.id')
         ->Join('purches','purches.id','=','item_purches.purches_id')
         // ->Join('datawarehouse_items','datawarehouse_items.id','=','purches.Warehouse_iD')
         ->where('purches.Type','Out')
          ->where('item_purches.item_id',$value->id)
           ->where('purches.Warehouse_iD',input::get("Warehouse_iD"))
         ->select(DB::raw('sum(quantity) As outt'))
         ->groupBy(DB::raw('item_purches.item_id'))
         ->first();
    
       if($out_items!=[]&&$In_items!=[])
       {
         // dd("jk",$In_items,$value->EngName);
        $value->Image=  str_replace("/MIS/","",$value->ImageItem);
 array_push($result,array('item_Name'=>$value->Name,'item_id'=>$value->item_id,'catName'=>$value->catName,'bandName'=>$value->bandName,'quantityIN'=>$In_items->INN,'quantityOut'=>$out_items->outt,'show'=>$Show,'Image'=>$value->Image,'quantity'=>($In_items->INN-$out_items->outt)));
 }
else if($out_items==[]&&$In_items!=[])
         {
        $value->Image=  str_replace("/MIS/","",$value->ImageItem);
 array_push($result,array('item_Name'=>$value->Name,'item_id'=>$value->item_id,'catName'=>$value->catName,'bandName'=>$value->bandName,'quantityIN'=>$In_items->INN,'quantityOut'=>0,'show'=>$Show,'Image'=>$value->Image,'quantity'=>($In_items->INN)));
 }


}
    



       // $datawarehouse_items = DB::table('datawarehouse_items')
       //   ->join('items','items.id','=','datawarehouse_items.item_id')
       //   ->join('datawarehouses','datawarehouses.id','=','datawarehouse_items.datawarehouse_id')
       //   ->where('datawarehouse_items.datawarehouse_id','=',input::get("Warehouse_iD"))
         
       //   ->select('items.EngName As ItemName','items.Name As item_NameArabic','datawarehouse_items.quantityinstock','datawarehouses.EngName AS Name')
       //   ->orderBy('datawarehouse_items.quantityinstock','DESC')
       //   ->get();
   }
   

else  if (input::get("Warehouse_iD")!=Null && input::get("item_id")!==Null)  
   {

 $result=[] ; 
       $itemids=DB::table('datawarehouse_items')
         ->join('items','items.id','=','datawarehouse_items.item_id')
         ->join('datawarehouses','datawarehouses.id','=','datawarehouse_items.datawarehouse_id')
         ->where('datawarehouse_items.datawarehouse_id','=',input::get("Warehouse_iD"))
         ->whereIN('datawarehouse_items.item_id',$itemsarray)
         ->select('datawarehouse_items.item_id')
        ->groupBy('datawarehouse_items.item_id')
         ->lists('datawarehouse_items.item_id');
   $item=Item::join('item_purches','item_purches.item_id','=','items.id')

    ->Join('brands','brands.id','=','items.brand_id')
        ->Join('categories','categories.id','=','items.category_id')
       
    ->whereIn('item_purches.item_id',$itemids)
        ->select('items.*','categories.Name as catName','brands.Name as bandName','items.id as item_id','items.id as item_id')
        ->groupBy(DB::raw('item_purches.item_id'))
         ->get();

        
         foreach ($item as  $value) {
          $In_items =Item::join('item_purches','item_purches.item_id','=','items.id')
         ->Join('purches','purches.id','=','item_purches.purches_id')
         ->where('purches.Type','In')
          ->where('item_purches.item_id',$value->id)
            ->where('purches.Warehouse_iD',input::get("Warehouse_iD"))
         ->select(DB::raw('sum(quantity) As INN'))
         ->groupBy(DB::raw('item_purches.item_id'))
         ->first();
         $out_items =Item::join('item_purches','item_purches.item_id','=','items.id')
         ->Join('purches','purches.id','=','item_purches.purches_id')
         ->where('purches.Type','Out')
          ->where('purches.Warehouse_iD',input::get("Warehouse_iD"))
          ->where('item_purches.item_id',$value->id)
         ->select(DB::raw('sum(quantity) As outt'))
         ->groupBy(DB::raw('item_purches.item_id'))
         ->first();
     
       if($out_items!=[]&&$In_items!=[])
       {
        $value->Image=  str_replace("/MIS/","",$value->ImageItem);
         // dd("jk",$In_items,$value->EngName);
 array_push($result,array('item_Name'=>$value->Name,'item_id'=>$value->item_id,'catName'=>$value->catName,'bandName'=>$value->bandName,'quantityIN'=>$In_items->INN,'quantityOut'=>$out_items->outt,'show'=>$Show,'Image'=>$value->Image,'quantity'=>($In_items->INN-$out_items->outt)));
 }
 else if($out_items==[]&&$In_items!=[])
         {
          // dd("ggjhgj",$In_items,$value->EngName);
          $value->Image=  str_replace("/MIS/","",$value->ImageItem);
 array_push($result,array('item_Name'=>$value->Name,'item_id'=>$value->item_id,'catName'=>$value->catName,'bandName'=>$value->bandName,'quantityIN'=>$In_items->INN,'quantityOut'=>0,'show'=>$Show,'Image'=>$value->Image,'quantity'=>($In_items->INN)));
 }


}




   }
   else
   {

 $result=[] ; 
       $itemids=DB::table('datawarehouse_items')
         ->join('items','items.id','=','datawarehouse_items.item_id')
         ->join('datawarehouses','datawarehouses.id','=','datawarehouse_items.datawarehouse_id')
         // ->where('datawarehouse_items.datawarehouse_id','=',input::get("Warehouse_iD"))
         // ->where('datawarehouse_items.item_id','=',input::get("item_id"))
         ->select('datawarehouse_items.item_id')
        ->groupBy('datawarehouse_items.item_id')
         ->lists('datawarehouse_items.item_id');
   $item=Item::join('item_purches','item_purches.item_id','=','items.id')
    ->whereIn('item_purches.item_id',$itemids)
        ->select('items.*')
        ->groupBy(DB::raw('item_purches.item_id'))
         ->get();

        
         foreach ($item as  $value) {
          $In_items =Item::join('item_purches','item_purches.item_id','=','items.id')
         ->Join('purches','purches.id','=','item_purches.purches_id')
         ->where('purches.Type','In')
          ->where('item_purches.item_id',$value->id)
         ->select(DB::raw('sum(quantity) As INN'))
         ->groupBy(DB::raw('item_purches.item_id'))
         ->first();
         $out_items =Item::join('item_purches','item_purches.item_id','=','items.id')
         ->Join('purches','purches.id','=','item_purches.purches_id')
         ->where('purches.Type','Out')
          ->where('item_purches.item_id',$value->id)
         ->select(DB::raw('sum(quantity) As outt'))
         ->groupBy(DB::raw('item_purches.item_id'))
         ->first();
     
       if($out_items!=[]&&$In_items!=[])
       {
         // dd("jk",$In_items,$value->EngName);
        $value->Image=  str_replace("/MIS/","",$value->ImageItem);
 array_push($result,array('item_Name'=>$value->Name,'Notes'=>$value->Notes,'item_id'=>$value->item_id,'quantityIN'=>$In_items->INN,'quantityOut'=>$out_items->outt,'show'=>$Show,'Image'=>$value->Image,'quantity'=>($In_items->INN-$out_items->outt)));
 }
else if($out_items==[]&&$In_items!=[])
         {
          // dd("ggjhgj",$In_items,$value->EngName);
  $value->Image=  str_replace("/MIS/","",$value->ImageItem);
 array_push($result,array('item_Name'=>$value->Name,'Notes'=>$value->Notes,'item_id'=>$value->item_id,'quantityIN'=>$In_items->INN,'quantityOut'=>0,'show'=>$Show,'Image'=>$value->Image,'quantity'=>($In_items->INN)));
 }
}      
}


Excel::create('Item Export', function ($excel) use ( $result) {

                   $excel->sheet('Sheet1', function ($sheet) use ($result) {
                        // $data = array(
                            
                        //     'rows' => $bags
                        // );
                        $sheet->setWidth('A', 16);
                        $sheet->setWidth('B', 16);
                        $sheet->setWidth('C', 43);
                        $sheet->setWidth('D', 13);
                        $sheet->setWidth('E', 43);
                        $sheet->setWidth('F', 10.5);
                        $sheet->setWidth('G', 17.6);
                        $sheet->setWidth('H', 14);
                        $sheet->setWidth('I', 15.3);
                        $sheet->setWidth('J', 14);
                        $sheet->setWidth('K', 15.3);
                        $sheet->setWidth('L', 14);
                        $sheet->setWidth('M', 15.3);
                        $sheet->setWidth('N', 16);
                        $sheet->setWidth('O', 200);

                        $sheet->loadView('StockReports.viewwithimage')
                            ->with('result', $result);
                    });
                })->export('xls');

        return back();
    }
public function InventoryStock()
{
    try {

 if (Auth::check())
  {


    if(Auth::user()->can('show-InventoryStock'))
      {

       // $Items=Item::select('Name','id')->lists('Name','id');


    if(Auth::user()->can('Show All Warehouse'))
      {
        $catogry=Category::Lists('Name','id');
      $Brand=Brand::Lists('Name','id'); 
         $Warehouses=Warehouse::select('EngName AS Name','id')->lists('Name','id');
        $warehouseID =DB::table('datawarehouses')
      // ->where('Government','=',Auth::user()->Government)
       ->select ('Name', 'id')->get();
      }
      else //if Show Specific Warehouse
      {
          $catogry=Category::Lists('Name','id');
      $Brand=Brand::Lists('Name','id'); 
         $Warehouses=Warehouse::where('Government','=',Auth::user()->Government)->select('EngName AS Name','id')->lists('Name','id');
        $warehouseID =DB::table('datawarehouses')
      ->where('Government','=',Auth::user()->Government)
       ->select ('Name', 'id')->get();

      } 
      $WareID=[];
       foreach ($warehouseID as  $value) {
         array_push($WareID, $value->id);
       }

    $Items = item::join('datawarehouse_items','datawarehouse_items.item_id','=','items.id')
    ->whereIN('datawarehouse_items.datawarehouse_id',$WareID)
   
     ->Where('datawarehouse_items.quantityinstock','!=',0)
         ->select('items.Name','items.id')
         ->lists('Name', 'id');
 




     
      return view('StockReports.InventoryStock',compact('Items','Warehouses','catogry','Brand'));

      }
    else 
      {
  return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
  return redirect('/login');
    }
     
}
public function ShowReportIventoryStock()
{

  $itemsarray=input::get("item_id");
 $Show=input::get("agree");

  if (input::get("Warehouse_iD")==Null && input::get("item_id")==Null)
  {
      $result=[] ; 
       $item=Item::join('item_purches','item_purches.item_id','=','items.id')
       ->Join('brands','brands.id','=','items.brand_id')
        ->Join('categories','categories.id','=','items.category_id')
        ->select('items.*','categories.Name as catName','brands.Name as bandName','items.id as item_id')
        ->groupBy(DB::raw('item_purches.item_id'))
        ->orderBy(DB::raw('brands.Index'))
         ->get();
         //($item);
         foreach ($item as  $value) {
          $In_items =Item::join('item_purches','item_purches.item_id','=','items.id')
        
           ->Join('purches','purches.id','=','item_purches.purches_id')

         ->where('purches.Type','In')
          ->where('item_purches.item_id',$value->id)
         ->select(DB::raw('sum(quantity) As INN'))
         ->groupBy(DB::raw('item_purches.item_id'))
         ->first();
         
         $out_items =Item::join('item_purches','item_purches.item_id','=','items.id')
         ->Join('purches','purches.id','=','item_purches.purches_id')
         ->where('purches.Type','Out')
          ->where('item_purches.item_id',$value->id)
         ->select(DB::raw('sum(quantity) As outt'))
         ->groupBy(DB::raw('item_purches.item_id'))
         ->first();

     
            if($out_items!=[]&&$In_items!=[])
            {
              $quantity=$In_items->INN-$out_items->outt;
              if($value->Notes==null )
              {
                $value->Notes=  str_replace(" "," ",$value->Notes);
              }
             
            array_push($result,array('item_Name'=>$value->Name,'Notes'=>$value->Notes,'item_id'=>$value->item_id,'bandName'=>$value->bandName,'quantityIN'=>$In_items->INN,'quantityOut'=>$out_items->outt,'show'=>$Show,'quantity'=>($In_items->INN-$out_items->outt)));
            }
            else if($out_items==[]&&$In_items!=[])
            {
                if($value->Notes==null )
              {
                $value->Notes=  str_replace(" "," ",$value->Notes);
              }
            array_push($result,array('item_Name'=>$value->Name,'Notes'=>$value->Notes,'item_id'=>$value->item_id,'bandName'=>$value->bandName,'quantityIN'=>$In_items->INN,'quantityOut'=>0,'show'=>$Show,'Image'=>$value->ImageItem,'quantity'=>($In_items->INN)));
            }



}

         
   }
    else if (input::get("Warehouse_iD")!=Null   && input::get("item_id")==Null )
   
   {
   

       $result=[] ; 
       if( input::get("Brand")==0 )
       {
          $itemids=DB::table('datawarehouse_items')
         ->join('items','items.id','=','datawarehouse_items.item_id')
          
         ->join('datawarehouses','datawarehouses.id','=','datawarehouse_items.datawarehouse_id')
         ->where('datawarehouse_items.datawarehouse_id','=',input::get("Warehouse_iD"))    
         ->select('datawarehouse_items.item_id')
        ->groupBy('datawarehouse_items.item_id')
         ->lists('datawarehouse_items.item_id');
       }
       else if( input::get("Brand")!=Null)
       {
            $itemids=DB::table('datawarehouse_items')
         ->join('items','items.id','=','datawarehouse_items.item_id')
           ->Join('brands','brands.id','=','items.brand_id')
          
         ->join('datawarehouses','datawarehouses.id','=','datawarehouse_items.datawarehouse_id')
         ->where('datawarehouse_items.datawarehouse_id','=',input::get("Warehouse_iD")) 
         ->where('items.brand_id','=',input::get("Brand"))     
         ->select('datawarehouse_items.item_id')
        ->groupBy('datawarehouse_items.item_id')
        ->orderBy(DB::raw('brands.Index'))
         ->lists('datawarehouse_items.item_id');       
       }
     
     

   $item=Item::join('item_purches','item_purches.item_id','=','items.id')
   ->Join('brands','brands.id','=','items.brand_id')
          ->Join('categories','categories.id','=','items.category_id')
         ->Join('datawarehouse_items','datawarehouse_items.item_id','=','items.id')
    ->whereIn('item_purches.item_id',$itemids)
        ->select('items.*','categories.Name as catName','categories.Index_Cat','brands.Name as bandName','items.id as item_id','datawarehouse_items.quantityinstock')
        ->groupBy(DB::raw('item_purches.item_id'))
        ->orderBy(DB::raw('brands.Index'))
         ->get();

     
         foreach ($item as  $value) {

          $In_items =Item::join('item_purches','item_purches.item_id','=','items.id')
         ->Join('purches','purches.id','=','item_purches.purches_id')
         // ->Join('datawarehouse_items','datawarehouse_items.id','=','purches.Warehouse_iD')
         ->where('purches.Type','In')
          ->where('item_purches.item_id',$value->id)
           ->where('purches.Warehouse_iD',input::get("Warehouse_iD"))
         ->select(DB::raw('sum(quantity) As INN'))
         ->groupBy(DB::raw('item_purches.item_id'))
         ->first();

         $out_items =Item::join('item_purches','item_purches.item_id','=','items.id')
         ->Join('purches','purches.id','=','item_purches.purches_id')
         // ->Join('datawarehouse_items','datawarehouse_items.id','=','purches.Warehouse_iD')
         ->where('purches.Type','Out')
          ->where('item_purches.item_id',$value->id)
           ->where('purches.Warehouse_iD',input::get("Warehouse_iD"))
         ->select(DB::raw('sum(quantity) As outt'))
         ->groupBy(DB::raw('item_purches.item_id'))
         ->first();
    
       if($out_items!=[]&&$In_items!=[])
       {
         // dd("jk",$In_items,$value->EngName);
          if($value->Notes==null )
              {
                $value->Notes=  str_replace(" "," ",$value->Notes);
              }
 array_push($result,array('item_Name'=>$value->Name,'Notes'=>$value->Notes,'asd'=>$value->quantityinstock,'item_id'=>$value->item_id,'bandName'=>$value->bandName,'quantityIN'=>$In_items->INN,'quantityOut'=>$out_items->outt,'show'=>$Show,'Image'=>$value->ImageItem,'quantity'=>($In_items->INN-$out_items->outt)));
 }
else if($out_items==[]&&$In_items!=[])
         {
          if($value->Notes==null )
              {
                $value->Notes=  str_replace(" "," ",$value->Notes);
              }
 array_push($result,array('item_Name'=>$value->Name,'Notes'=>$value->Notes,'asd'=>$value->quantityinstock,'item_id'=>$value->item_id,'bandName'=>$value->bandName,'quantityIN'=>$In_items->INN,'quantityOut'=>0,'show'=>$Show,'Image'=>$value->ImageItem,'quantity'=>($In_items->INN)));
 }


}
    



       // $datawarehouse_items = DB::table('datawarehouse_items')
       //   ->join('items','items.id','=','datawarehouse_items.item_id')
       //   ->join('datawarehouses','datawarehouses.id','=','datawarehouse_items.datawarehouse_id')
       //   ->where('datawarehouse_items.datawarehouse_id','=',input::get("Warehouse_iD"))
         
       //   ->select('items.EngName As ItemName','items.Name As item_NameArabic','datawarehouse_items.quantityinstock','datawarehouses.EngName AS Name')
       //   ->orderBy('datawarehouse_items.quantityinstock','DESC')
       //   ->get();
   }
   

else  if (input::get("Warehouse_iD")!=Null && input::get("item_id")!==Null)  
   {

 $result=[] ; 
       $itemids=DB::table('datawarehouse_items')
         ->join('items','items.id','=','datawarehouse_items.item_id')
         ->join('datawarehouses','datawarehouses.id','=','datawarehouse_items.datawarehouse_id')
         ->where('datawarehouse_items.datawarehouse_id','=',input::get("Warehouse_iD"))
         ->whereIN('datawarehouse_items.item_id',$itemsarray)
         ->select('datawarehouse_items.item_id')
        ->groupBy('datawarehouse_items.item_id')
         ->lists('datawarehouse_items.item_id');
   $item=Item::join('item_purches','item_purches.item_id','=','items.id')

    ->Join('brands','brands.id','=','items.brand_id')
        ->Join('categories','categories.id','=','items.category_id')
       
    ->whereIn('item_purches.item_id',$itemids)
        ->select('items.*','categories.Name as catName','brands.Name as bandName','items.id as item_id','items.id as item_id')
        ->groupBy(DB::raw('item_purches.item_id'))
        ->orderBy(DB::raw('brands.Index'))
         ->get();

        
         foreach ($item as  $value) {
          $In_items =Item::join('item_purches','item_purches.item_id','=','items.id')
         ->Join('purches','purches.id','=','item_purches.purches_id')
         ->where('purches.Type','In')
          ->where('item_purches.item_id',$value->id)
            ->where('purches.Warehouse_iD',input::get("Warehouse_iD"))
         ->select(DB::raw('sum(quantity) As INN'))
         ->groupBy(DB::raw('item_purches.item_id'))
         ->first();
         $out_items =Item::join('item_purches','item_purches.item_id','=','items.id')
         ->Join('purches','purches.id','=','item_purches.purches_id')
         ->where('purches.Type','Out')
          ->where('purches.Warehouse_iD',input::get("Warehouse_iD"))
          ->where('item_purches.item_id',$value->id)
         ->select(DB::raw('sum(quantity) As outt'))
         ->groupBy(DB::raw('item_purches.item_id'))
         ->first();
     
       if($out_items!=[]&&$In_items!=[])
       {
         // dd("jk",$In_items,$value->EngName);
          if($value->Notes==null )
              {
                $value->Notes=  str_replace(" "," ",$value->Notes);
              }
 array_push($result,array('item_Name'=>$value->Name,'Notes'=>$value->Notes,'item_id'=>$value->item_id,'bandName'=>$value->bandName,'quantityIN'=>$In_items->INN,'quantityOut'=>$out_items->outt,'show'=>$Show,'Image'=>$value->ImageItem,'quantity'=>($In_items->INN-$out_items->outt)));
 }
 else if($out_items==[]&&$In_items!=[])
         {
          // dd("ggjhgj",$In_items,$value->EngName);
            if($value->Notes==null )
              {
                $value->Notes=  str_replace(" "," ",$value->Notes);
              }
 array_push($result,array('item_Name'=>$value->Name,'Notes'=>$value->Notes,'item_id'=>$value->item_id,'bandName'=>$value->bandName,'quantityIN'=>$In_items->INN,'quantityOut'=>0,'show'=>$Show,'Image'=>$value->ImageItem,'quantity'=>($In_items->INN)));
 }


}




   }
   else
   {

 $result=[] ; 
       $itemids=DB::table('datawarehouse_items')
         ->join('items','items.id','=','datawarehouse_items.item_id')
         ->join('datawarehouses','datawarehouses.id','=','datawarehouse_items.datawarehouse_id')
         // ->where('datawarehouse_items.datawarehouse_id','=',input::get("Warehouse_iD"))
         // ->where('datawarehouse_items.item_id','=',input::get("item_id"))
         ->select('datawarehouse_items.item_id')
        ->groupBy('datawarehouse_items.item_id')
         ->lists('datawarehouse_items.item_id');
   $item=Item::join('item_purches','item_purches.item_id','=','items.id')
    ->whereIn('item_purches.item_id',$itemids)
        ->select('items.*')
        ->groupBy(DB::raw('item_purches.item_id'))
         ->get();

        
         foreach ($item as  $value) {
          $In_items =Item::join('item_purches','item_purches.item_id','=','items.id')
         ->Join('purches','purches.id','=','item_purches.purches_id')
         ->where('purches.Type','In')
          ->where('item_purches.item_id',$value->id)
         ->select(DB::raw('sum(quantity) As INN'))
         ->groupBy(DB::raw('item_purches.item_id'))
         ->first();
         $out_items =Item::join('item_purches','item_purches.item_id','=','items.id')
         ->Join('purches','purches.id','=','item_purches.purches_id')
         ->where('purches.Type','Out')
          ->where('item_purches.item_id',$value->id)
         ->select(DB::raw('sum(quantity) As outt'))
         ->groupBy(DB::raw('item_purches.item_id'))
         ->first();
     
       if($out_items!=[]&&$In_items!=[])
       {
         // dd("jk",$In_items,$value->EngName);
          if($value->Notes==null )
              {
                $value->Notes=  str_replace(" "," ",$value->Notes);
              }
 array_push($result,array('item_Name'=>$value->Name,'Notes'=>$value->Notes,'item_id'=>$value->item_id,'quantityIN'=>$In_items->INN,'quantityOut'=>$out_items->outt,'show'=>$Show,'Image'=>$value->ImageItem,'quantity'=>($In_items->INN-$out_items->outt)));
 }
else if($out_items==[]&&$In_items!=[])
         {
          // dd("ggjhgj",$In_items,$value->EngName);
            if($value->Notes==null )
              {
                $value->Notes=  str_replace(" "," ",$value->Notes);
              }
 array_push($result,array('item_Name'=>$value->Name,'Notes'=>$value->Notes,'item_id'=>$value->item_id,'quantityIN'=>$In_items->INN,'quantityOut'=>0,'show'=>$Show,'Image'=>$value->ImageItem,'quantity'=>($In_items->INN)));
 }
}


        
}
return $result;
}
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    //Report Doaa Elgheny New Inventory Report 
    public function ShowReportIventoryStockNew($Brandid)
{


 $Show=1;


 

       $result=[] ; 
 
       
     
            $itemids=DB::table('datawarehouse_items')
         ->join('items','items.id','=','datawarehouse_items.item_id')
           ->Join('brands','brands.id','=','items.brand_id')
      
         ->join('datawarehouses','datawarehouses.id','=','datawarehouse_items.datawarehouse_id')
         ->where('datawarehouse_items.datawarehouse_id','=',1) 
         ->where('items.brand_id',$Brandid)     
         ->select('datawarehouse_items.item_id')
        ->groupBy('datawarehouse_items.item_id')
         ->lists('datawarehouse_items.item_id');       
  


   $item=Item::join('item_purches','item_purches.item_id','=','items.id')
   ->Join('brands','brands.id','=','items.brand_id')
     
         ->Join('datawarehouse_items','datawarehouse_items.item_id','=','items.id')
    ->whereIn('item_purches.item_id',$itemids)
        ->select('items.*','brands.Name as bandName','items.id as item_id','datawarehouse_items.quantityinstock')
        ->groupBy(DB::raw('item_purches.item_id'))
      
         ->get();

   
         foreach ($item as  $value) {

          $In_items =Item::join('item_purches','item_purches.item_id','=','items.id')
         ->Join('purches','purches.id','=','item_purches.purches_id')
         // ->Join('datawarehouse_items','datawarehouse_items.id','=','purches.Warehouse_iD')
         ->where('purches.Type','In')
          ->where('item_purches.item_id',$value->id)
           ->where('purches.Warehouse_iD',1)
         ->select(DB::raw('sum(quantity) As INN'))
         ->groupBy(DB::raw('item_purches.item_id'))
         ->first();

         $out_items =Item::join('item_purches','item_purches.item_id','=','items.id')
         ->Join('purches','purches.id','=','item_purches.purches_id')
         // ->Join('datawarehouse_items','datawarehouse_items.id','=','purches.Warehouse_iD')
         ->where('purches.Type','Out')
          ->where('item_purches.item_id',$value->id)
           ->where('purches.Warehouse_iD',1)
         ->select(DB::raw('sum(quantity) As outt'))
         ->groupBy(DB::raw('item_purches.item_id'))
         ->first();
    
       if($out_items!=[]&&$In_items!=[])
       {
         // dd("jk",$In_items,$value->EngName);
          if($value->Notes==null )
              {
                $value->Notes=  str_replace(" "," ",$value->Notes);
              }
              if($In_items->INN-$out_items->outt !=0)
              {
                 array_push($result,array('item_Name'=>$value->Name,'Notes'=>$value->Notes,'asd'=>$value->quantityinstock,'item_id'=>$value->item_id,'bandName'=>$value->bandName,'Image'=>$value->ImageItem,'quantity'=>($In_items->INN-$out_items->outt)));
              }

 }
else if($out_items==[]&&$In_items!=[])
         {
          if($value->Notes==null )
              {
                $value->Notes=  str_replace(" "," ",$value->Notes);
              }
                if($In_items->INN !=0)
              {
                 array_push($result,array('item_Name'=>$value->Name,'Notes'=>$value->Notes,'asd'=>$value->quantityinstock,'item_id'=>$value->item_id,'bandName'=>$value->bandName,'quantityIN'=>$In_items->INN,'Image'=>$value->ImageItem,'quantity'=>($In_items->INN)));
              }

 }


}
    


   


return $result;
}
    ///End 

   public function MapItem($Partition)
    {
      


       
$item=item::join('datawarehouse_items','datawarehouse_items.item_id','=','items.id')
        ->Where('datawarehouse_items.quantityinstock','!=',0)
        ->where('items.Partition',$Partition)
          ->where('datawarehouse_items.datawarehouse_id',1)
        ->select('items.Name','items.id','datawarehouse_items.quantityinstock','items.ImageItem')->get();


      //   $options = array();
       
       foreach ($item as $Value) {

      // $options += array($Value->id => $Value->Name);


        $Value->ImageItem=str_replace(" ","%20",$Value->ImageItem);
        }
       
     
        return \Response::json($item);
    }


  public function CairoMapItem($Partition)
    {
      $item=item::join('datawarehouse_items','datawarehouse_items.item_id','=','items.id')
        ->Where('datawarehouse_items.quantityinstock','!=',0)
        ->where('datawarehouse_items.Partition',$Partition)
          ->where('datawarehouse_items.datawarehouse_id',3)
        ->select('items.Name','items.id','datawarehouse_items.quantityinstock','items.ImageItem')->get();


      //   $options = array();
       
       foreach ($item as $Value) {

      // $options += array($Value->id => $Value->Name);


        $Value->ImageItem=str_replace(" ","%20",$Value->ImageItem);
        }
       
     
        return \Response::json($item);
      }














   public function ShowPurchaseIn()
   {
  try {

 if (Auth::check())
  {


    if(Auth::user()->can('show-PurchaseIn'))
      {

      $Warehouses=Warehouse::select('EngName AS Name','id')->lists('Name','id');
      return view('StockReports.PurchaseIn',compact('Warehouses','Marktingactitvitys'));

      }
    else 
      {
  return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
  return redirect('/login');
    }

   }
public function ShowReportPurchase()
{
 $range=array( date("Y-m-d",strtotime(Input::get('Start_Date'))), date("Y-m-d",strtotime(Input::get('End_Date'))));

 $type='In';
        if (input::get("Warehouse_iD")==Null )
       {
      $purches =Purchase::
       join('datawarehouses','datawarehouses.id','=','purches.Warehouse_iD')
      ->whereBetween('purches.Date',$range)
     ->where('purches.Type', '=',$type)
      ->select('purches.PurchesNo','purches.Date','purches.id','datawarehouses.EngName AS datawarehousesName')
     ->orderBy('purches.Date','ASC')
      ->get();
       }
       else
       {
       $purches =Purchase::
       join('datawarehouses','datawarehouses.id','=','purches.Warehouse_iD')
      ->whereBetween('purches.Date',$range)
      ->where('purches.Type', '=',$type)
      ->where('purches.Warehouse_iD', '=',input::get('Warehouse_iD'))
      ->select('purches.PurchesNo','purches.Date','purches.id','datawarehouses.EngName AS datawarehousesName')
      ->orderBy('purches.Date','ASC')
      ->get();
         
       }
    
$result=[];
$data=[];
       foreach ($purches as  $purches) {    
            $n=0;
            foreach($purches->getpurchaseitem as $item)
               {
                          
                   array_push($data,array( 

                  'item_Name'=>$item->EngName,
                  'itemName'=>$item->Name,
                  'quantity'=>$item->pivot->quantity
                    )
                 );

                 $n++; 
               }
        array_push($result,array(
            'rowspan'=>$n,
          'PurchesNo' => $purches->PurchesNo,
          
          'Date' => $purches->Date,
          'datawarehousesName' => $purches->datawarehousesName,
          'items' => $data
                                     )

                                    );
$data=[];

}
      return $result;

}

    public function ShowReport()
    {

 $range=array( date("Y-m-d",strtotime(Input::get('Start_Date'))), date("Y-m-d",strtotime(Input::get('End_Date'))));


 $type='Out';
        if (input::get("Warehouse_iD")==Null )
       {
          
                $purches =DB::table('item_purches')
      ->join('purches','purches.id','=','item_purches.purches_id')
        ->join('items','items.id','=','item_purches.item_id')
      // -> join('activity_items','activity_items.id','=','purches.ActivityitemsID')
      ->whereBetween('purches.Date',$range)
      ->where('purches.Type', '=',$type)
   ->select('purches.Date','purches.otherNote AS sub_activitiesName','items.Name','item_purches.quantity')
      ->orderBy('items.id')
      ->get();
      
      $result=[];
      $data=[];

       foreach ($purches as  $purches) {    
            $n=0;
            foreach($purches as $item)
               {
                         
                   array_push($data,array( 

                  'sub_activitiesName'=>$item->sub_activitiesName,
                  'Date'=>$item->Date,
                  'quantity'=>$item->quantity
                    )
                 );

                 $n++; 
               }



        array_push($result,array(
             'rowspan'=>$n,
          'PurchesNo' =>'',
          'item_Name' =>'',
          'itemName' => '',
          'datawarehousesName' => '',
          'items' => $data
                  
                                     )

                                    );
$data=[];
}

       }
       else
       {
        
    $purches =Purchase::
        // join('activity_items','activity_items.id','=','purches.ActivityitemsID')
        // ->join('sub_activities','sub_activities.id','=','activity_items.subactivity_id')
    join('datawarehouses','datawarehouses.id','=','purches.Warehouse_iD')
      ->whereBetween('purches.Date',$range)
      ->where('purches.Type', '=',$type)
      ->where('purches.Warehouse_iD', '=',input::get('Warehouse_iD'))
      ->select('purches.PurchesNo','purches.Date','purches.id','purches.otherNote AS sub_activitiesName ','datawarehouses.EngName AS datawarehousesName')
      ->orderBy('purches.Date','ASC')
      ->get();
     
      $result=[];
$data=[];
       foreach ($purches as  $purches) {    
            $n=0;
            foreach($purches->getpurchaseitem as $item)
               {
                          
                   array_push($data,array( 

                  'item_Name'=>$item->EngName,
                  'itemName'=>$item->Name,
                  'quantity'=>$item->pivot->quantity
                    )
                 );

                 $n++; 
               }



        array_push($result,array(
            'rowspan'=>$n,
          'PurchesNo' => $purches->PurchesNo,
          'sub_activitiesName' => $purches->sub_activitiesName,
          'Date' => $purches->Date,
          'datawarehousesName' => $purches->datawarehousesName,
          'items' => $data
                                     )

                                    );
$data=[];

}
         
       }
    

      return $result;
    }
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
      
       
}
