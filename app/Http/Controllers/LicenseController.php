<?php

namespace App\Http\Controllers;

use Auth;
use App\Http\Requests;
use App\User;
use DB;
use App\Http\Controllers\Controller;
use Request;
use Excel;
use File;
use Illuminate\Support\Facades\Response;
use Validator;
use Illuminate\Support\Facades\Redirect;
use Input;
use Session;
use Geocode;
use Cache;
use URL;
use View;
use Khill\Lavacharts\Laravel\LavachartsFacade as Lava;
use Khill\Lavacharts\Lavacharts;
use TableView;
use App\Dategps;
use PHPExcel; 
use PHPExcel_IOFactory; 
use App\Government;
use App\District;
use App\Retailer;
use App\Type;
use App\EndType;
use App\SubType;
use App\License;

class LicenseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
   {
       $this->middleware('auth');
   }
    public function index($id)
    {
        try
        {
            if (Auth::check())
            {
                // if(Auth::user()->can('show-Marketing_Activity'))
                // {
                        Session::put('LiRetailer_id',$id);
                        $Licenses=License::where('Retailer_Id',$id)->get();
                        $Government=Government::select('Name','id')->lists('Name','id');
                        $District=District::select('Name','id')->lists('Name','id');
                        return view('licenses.index',compact('Licenses','Government','District'));
                // }
                // else 
                // {
                //     return view('errors.403');
                // }

            }//endAuth
            else
            {
                return redirect('/login');
            }

        }//endtry
        catch(Exception $e) 
        {
            return redirect('/login');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeLicense()
    {
        try
        {
            if (Auth::check())
            {
                // if(Auth::user()->can('show-Marketing_Activity'))
                // {
                        $LRetailer_id=Session::get('LiRetailer_id'); 
                        $retailers_license=New License;
                        $retailers_license->Retailer_Id=$LRetailer_id;
                        $retailers_license->Name=input::get('NameLicence');
                        $retailers_license->License_Number=input::get('LicenseNumber');
                        $retailers_license->Creation_Date=input::get('Creation_Date');
                        $retailers_license->Government_Id=input::get('Government');
                        $retailers_license->District_Id=input::get('district');
                        $retailers_license->save();
                        return redirect('/license/'.$LRetailer_id);
                // }
                // else 
                // {
                //     return view('errors.403');
                // }

            }//endAuth
            else
            {
                return redirect('/login');
            }

        }//endtry
        catch(Exception $e) 
        {
            return redirect('/login');
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updatelicense()
    {
        try
        {
            if (Auth::check())
            {
                // if(Auth::user()->can('show-Marketing_Activity'))
                // {
                        $Licenseid = Input::get('pk');  
                        $column_name = Input::get('name');
                        $column_value = Input::get('value');   
                        $License = License::whereId($Licenseid)->first();
                        $License-> $column_name=$column_value;
                        if($License->save())
                            return \Response::json(array('status'=>1));
                        else 
                            return \Response::json(array('status'=>0));

                // }
                // else 
                // {
                //     return \Response::json(array('status'=>'You do not have permission.'));
                // }

            }//endAuth
            else
            {
                return redirect('/login');
            }

        }//endtry
        catch(Exception $e) 
        {
            return redirect('/login');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try
        {
            if (Auth::check())
            {
                // if(Auth::user()->can('show-Marketing_Activity'))
                // {
                        $License=License::find($id);
                        $License->delete();
                        $LRetailer_id=Session::get('LiRetailer_id');      
                        return redirect('/license/'.$LRetailer_id);
                // }
                // else 
                // {
                //     return view('errors.403');
                // }

            }//endAuth
            else
            {
                return redirect('/login');
            }

        }//endtry
        catch(Exception $e) 
        {
            return redirect('/login');
        }
    }
}
