<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon;
use App\Http\Requests;
use App\User;
use Excel;
use Input;
use File;
use Validator;
use DB;
use Auth;
use Symfony\Component\Debug\Exception\FatalThrowableError;
use Datatables;
use Illuminate\View\Middleware\ErrorBinder;
use Exception;
use App\Gps;
use App\Purchase;
use App\Item;
use App\Warehouse;
use Image;
use App\ActivityItem;
use Session;

class BillOutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
       public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {

  // try {

 if (Auth::check())
  {

       $WareID=[];
    

     

    if(Auth::user()->can('Show Bill out'))
      {
           if(Auth::user()->can('Show All Purches'))
      {

          $purchases= Purchase::
             select('*')
             ->where('Type','=','Out')
             
             ->whereBetween('Date', array('2015-12-28', '2018-12-28'))
             ->orderby('Date', 'DESC')
             ->get();
          // dd ($purchases);
      }
      else
      {
        // dd(Auth::user()->Government);
         $warehouse=Warehouse::where('Government',Auth::user()->Government)->first();
       
       $purchases= Purchase::
             select('*')
             ->where('Type','=','Out')
             ->where('Warehouse_iD','=',$warehouse->id)

             ->whereBetween('Date', array('2015-12-28', '2018-12-28'))
             ->orderby('Date', 'DESC')
             ->get();
                // dd ($purchases);
      }

      $activityitems =ActivityItem::lists('Code','id');
      $user = User::lists('Name', 'id');
      // $item = item::lists('Name', 'id');
      // if Show All Warehouse
      if(Auth::user()->can('Show All Warehouse'))
      {
        $warehouseID =DB::table('datawarehouses')
      // ->where('Government','=',Auth::user()->Government)
       ->select ('Name', 'id')->get();

        $warehouse =DB::table('datawarehouses')
       ->select ('Name', 'id')
       ->lists('Name', 'id');
       
      }
      else //if Show Specific Warehouse
      {
        $warehouseID =DB::table('datawarehouses')
      ->where('Government','=',Auth::user()->Government)
       ->select ('Name', 'id')->get();

       $warehouse =DB::table('datawarehouses')
      ->where('Government','=',Auth::user()->Government)
       ->select ('Name', 'id')
       ->lists('Name', 'id');
       
      }
        foreach ($warehouseID as  $value) {
         array_push($WareID, $value->id);
       }
       $item = item::join('datawarehouse_items','datawarehouse_items.item_id','=','items.id')
     ->whereIN('datawarehouse_items.datawarehouse_id',$WareID)
     ->Where('datawarehouse_items.quantityinstock','!=',0)
    ->lists('items.Name', 'items.id');
     
      return view('BillOut/index',compact('purchases','user','item','warehouse','activityitems'));
       }
    else 
      {
  return view('errors.403');
      }

   }//endAuth

else
{
  //dd("no auth index");
return redirect('/login');
}

// }//endtry
// catch(Exception $e) 
//     {
//     // dd("error");
//   return redirect('/login');
//     }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function UpdateImageOut(Request $request)
    {
     
        
        $id=Input::get('id');
         $file_img = Input::file('RelatedDecomentLink');
        
        $Purchase =Purchase::whereId($id)->first();
     
        if($file_img!==null)
        {

            $imageName = $file_img->getClientOriginalName();



            $path=public_path("/assets/dist/img/").$imageName;

 
                if (!file_exists($path)) {
                  $file_img->move(public_path("/assets/dist/img/BillOut/"),$imageName);
                  $Purchase->RelatedDecomentLink="/MIS/assets/dist/img/BillOut/".$imageName;
                
                  
                }
                else
                {
                   
                   $random_string = md5(microtime());

                   $file_img->move(public_path("/assets/dist/img/BillOut/"),$random_string.".jpg");

                  $Purchase->RelatedDecomentLink="/MIS/assets/dist/img/BillOut/".$random_string.".jpg";

              
                }      
             
        }
  
        $Purchase->save();
        return redirect('BillOut');
    }
     public function EditPhotoPurchesOut($id)
   {

     $Purchase=Purchase::where('id',$id)
     ->where('Type','Out')->first();

    return view('BillOut.EidtPhoto',compact('Purchase'));
   }
    public function showallDetails($id) 
    {

        try {

 if (Auth::check())
  {


    if(Auth::user()->can('show all Details Bill Out'))
      {
     // session::put('Type',$Type);
      $itemsId=[];
      $id=$id;
      
      // return Item Expect Exsist 
         $purchaseItem = DB::table('item_purches')
         ->join('items','items.id','=','item_purches.item_id')
         ->where('item_purches.purches_id', '=',$id)
           ->select('item_purches.item_id')->get();
        
           for ($i=0;$i<count($purchaseItem);$i++)
           {
            array_push( $itemsId, $purchaseItem[$i]->item_id);
           }
         
      if(Auth::user()->can('Show All Warehouse'))
      {
        $warehouseID =DB::table('datawarehouses')
      // ->where('Government','=',Auth::user()->Government)
       ->select ('Name', 'id')->get();
      }
      else //if Show Specific Warehouse
      {
        $warehouseID =DB::table('datawarehouses')
      ->where('Government','=',Auth::user()->Government)
       ->select ('Name', 'id')->get();

      } 
      $WareID=[];
       foreach ($warehouseID as  $value) {
         array_push($WareID, $value->id);
       }

    $item = item::join('datawarehouse_items','datawarehouse_items.item_id','=','items.id')
    ->whereIN('datawarehouse_items.datawarehouse_id',$WareID)
    -> whereNotIn('items.id',$itemsId)
     ->Where('datawarehouse_items.quantityinstock','!=',0)
         ->select('items.Name','items.id')
         ->lists('Name', 'id');
 
         $purchaseDetails = DB::table('item_purches')
         
         ->join('items','items.id','=','item_purches.item_id')
        // ->join('purches','purches.id','=','item_purches.purches_id')
         ->where('item_purches.purches_id', '=',$id)
           ->select('item_purches.quantity','item_purches.item_id','items.Name','item_purches.purches_id')->get();

     $Warehouse_iD=DB::table('purches')
       ->where('purches.id', '=',$id)
       ->select('purches.Warehouse_iD')->pluck('purches.Warehouse_iD');
       $Warehouse=$Warehouse_iD[0];

return view('BillOut.detailsPurches',compact('purchaseDetails','item','id','Warehouse'));

      }
    else 
      {
  return view('errors.403');
      }

   }//endAuth

else
{
  //dd("no auth");
  return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
     // dd("error");
  return redirect('/login');
    }
 }
    //Doaa Elgheny 
    public function storeDetails()
    {
      
        try {

 if (Auth::check())
  {


    if(Auth::user()->can('store Details Bill Out'))
      {

       $purchase_id=Input::get('id');  
     
     $purchase = Purchase::whereId($purchase_id)->first();
       $descraption=Input::get('descraption');
 
       $item=Input::get('item');
      
        $n=count($item);

         for($i=0;$i<$n-1;$i++)
        {

            $new_item=Item::where('id',$item[$i])->first();
             $descr= $descraption[$i];
  
          $purchase->getpurchaseitem()->attach($item[$i], ['quantity'=>$descr]);

         
  //We should update Stock Quantity Be Plus When Type Be in decrease when be out
              //Doaa Elgheny 17/10/2016
               //dd($item[$i]);
           $itemqantity = DB::table('datawarehouse_items')
                        ->where('datawarehouse_id', '=',$purchase->Warehouse_iD)
                        ->where('item_id', '=',$item[$i])
                       ->select('quantityinstock')
                       ->pluck('quantityinstock');
                 
                    if ($itemqantity == null)//Msh mogod nd5al Wa7d gdid New Row at datawarehouse_items
                    {

                    //insert New Row
                      
                      if (Session::get('NewQuantity')!=Null)
                      {
                     
                           $descr=Session::get('NewQuantity');
                      }
                    
                     $itemattch=Item::whereId($item[$i])->first();
                     
                     
                      $NewData=$purchase->Warehouse_iD;
                      $itemattch->getDatawerehouseitem()->attach($NewData,["quantityinstock" => $descr]);
                     
                        //Update Items 

                       $column_name ='totalmstock';
                       $ItemData =Item::whereId($new_item->id)->first();
                       $ItemData->$column_name=$descr;
                       $ItemData->save();
                      
                       }
                  else //Update in Exsits items and datawarehouse_items
                    { 
                      Session::put('NewQuantity',$itemqantity[0]-$descr);
                      Session::get('NewQuantity');
                      
                         $NewQuantity=$itemqantity[0]-$descr;
                        
                      //Update Items 
                    
                       $column_name ='totalmstock';
                       $ItemData =Item::whereId($new_item->id)->first();
                    

                      $ItemData->$column_name=($ItemData->totalmstock)-$descr ;
                      $ItemData->save();
                    //  dd('ddddd');
                    //update datawarehouse_items
                       
                      //dd($i);
                     $NewData=$purchase->Warehouse_iD;
                     $itemattch=Item::whereId($item[$i])->first();
                     $detachquntity=$itemqantity[0];
                      
 
                      $test= $itemattch->getDatawerehouseitem()->detach($NewData,["quantityinstock" => $detachquntity]);

                       if($test==1)
                       {
                        
                        $itemattch->getDatawerehouseitem()->attach($NewData,["quantityinstock" => $NewQuantity]);
                       }
               }
                     
        }
        //
       return redirect('BillOut');

      }
    else 
      {
  return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
  return redirect('/login');
    }
 }
   public function checkshaza()
    {


      $quantity=input::get('quantity');
      $itemid=Input::get('itemid'); 
       $OutQuantity = DB::table('datawarehouse_items')
                      ->where('datawarehouse_items.item_id','=',$itemid) 
                       ->where('datawarehouse_items.datawarehouse_id','=',input::get('Warehouse'))
                       ->select('quantityinstock')
                       ->pluck('quantityinstock');
        
      //Case one donot select reason check on warehouse only
if(input::get('ActivityRes')==0)
  {
    if(count($OutQuantity)!=0)   
      { 
        if($OutQuantity[0] >= $quantity) 
         { 
 
            $isAvailable = true;
         }
        
         else
         {
           
             $isAvailable = false;
         }  
     }
     else
     {
        $isAvailable = false;
     } 
  }
 

      //Case 2 select Reason
  
        return \Response::json(array('valid' =>$isAvailable,));                                                                  
    }
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
       public function store(Request $request)
    {
      // dd(Input::get('book_title'));
      //book_other
      //book_quantity
      //book_isbn
    
        // try {

 if (Auth::check())
  {


    if(Auth::user()->can('Store Bill Out'))
      {

 
       $purchase= New Purchase; 
        
       $purchase->PurchesNo=Input::get('PNumer');
       $purchase->Date=date("Y-m-d",strtotime(Input::get('Date')));

       $purchase->Type='Out';
       $purchase->users_id=Auth::user()->id;        
       $purchase->Warehouse_iD=Input::get('warehouse');
       $purchase->otherNote=Input::get('Notes');


        // if (Input::get('Descrption') !=null)
        // {
        //   $purchase->Description=Input::get('Descrption');
          
        // }
        // else
        // {

        //   $purchase->ActivityitemsID=Input::get('Reason');
        // }
       //Image Store
        if(Input::file('pic') !=Null)
  {

       $file_img = Input::file('pic');

     ////////////Save Image//////////////////
           // $file_img = Request::file('pic');
       
            $imageName = $file_img->getClientOriginalName();


                $path=public_path("assets/dist/img/BillOut").$imageName;
              // notation octale : valeur du mode correcte
                  

                if (!file_exists($path)) {
                  $file_img->move(public_path("assets/dist/img/BillOut"),$imageName);
                  $purchase->RelatedDecomentLink="/MIS/assets/dist/img/BillOut/".$imageName; 
                }
                else
                {
                   $random_string = md5(microtime());
                   $file_img->move(public_path("assets/dist/img/BillOut/"),$random_string.".jpg");
                   
                  $purchase->RelatedDecomentLink="/MIS/assets/dist/img/BillOut/".$random_string.".jpg";
                
                }
}
  // dd(Input::get('ActivityRes')[0],Input::get('Other'));
  
 
       //End Image 
     $purchase->save();

       $descraption=Input::get('book_quantity');
       $other=Input::get('book_other');
       $ActivityRes=Input::get('book_isbn');

       $item=Input::get('book_title');

        $n=count($item);
       
// dd(Input::get('item'));
        for($i=0;$i<$n;$i++)
        {

            $new_item=Item::where('id',$item[$i])->first();
             $descr= $descraption[$i];
             // if ($other[$i]!=Null)
             // {
             //   $ActivityName=$other[$i];
             // }
             // else
             // {
           
               $ActivityName=DB::table('sub_activities')->where('id',$ActivityRes[$i])->select('Name','id')
                       ->first();
             // }
          $purchase->getpurchaseitem()->attach($item[$i], ['quantity'=>$descr,'ActivityName'=>$ActivityName->Name,'activity_id'=>$ActivityName->id]);

          
  //We should update Stock Quantity Be Plus When Type Be in decrease when be out
              //Doaa Elgheny 17/10/2016
               //dd($item[$i]);
           $itemqantity = DB::table('datawarehouse_items')
                        ->where('datawarehouse_id', '=',Input::get('warehouse'))
                        ->where('item_id', '=',$item[$i])
                       ->select('quantityinstock')
                       ->pluck('quantityinstock');
                 
                    if ($itemqantity == null)//Msh mogod nd5al Wa7d gdid New Row at datawarehouse_items
                    {

                    //insert New Row
                      
                      if (Session::get('NewQuantity')!=Null)
                      {
                     
                           $descr=Session::get('NewQuantity');
                      }
                    
                     $itemattch=Item::whereId($item[$i])->first();
                     
                     
                      $NewData=(int)Input::get('warehouse');
                       $itemattch->getDatawerehouseitem()->attach($NewData,["quantityinstock" => $descr]);
                     
                        //Update Items 

                       $column_name ='totalmstock';
                       $ItemData =Item::whereId($new_item->id)->first();
                   
                     
                      
                     $ItemData->$column_name=$descr;
                      $ItemData->save();
                      
                       }
                  else //Update in Exsits items and datawarehouse_items
                    { 
                      Session::put('NewQuantity',$itemqantity[0]-$descr);
                      Session::get('NewQuantity');
                      
                         $NewQuantity=$itemqantity[0]-$descr;
                        
                      //Update Items 
                    
                       $column_name ='totalmstock';
                       $ItemData =Item::whereId($new_item->id)->first();
                    

                      $ItemData->$column_name=($ItemData->totalmstock)-$descr ;
                      $ItemData->save();
                    //  dd('ddddd');
                    //update datawarehouse_items
                       
                      //dd($i);
                     $NewData=(int)Input::get('warehouse');
                     $itemattch=Item::whereId($item[$i])->first();
                     $detachquntity=$itemqantity[0];
                      
 
                      $test= $itemattch->getDatawerehouseitem()->detach($NewData,["quantityinstock" => $detachquntity]);

                       if($test==1)
                       {
                        
                        $itemattch->getDatawerehouseitem()->attach($NewData,["quantityinstock" => $NewQuantity]);
                       }
                        

               }
                     
        }

        //
        
      return redirect('BillOut');
     
        }
    else 
      {
  return view('errors.403');
      }

   }//endAuth

else
{
    //dd('not auth ');
return redirect('/login');
}

// }//endtry
// catch(Exception $e) 
//     {
      
//      // dd('Exception ');
//   return redirect('/login');
//     }
   }
    // * Display the specified resource.
     // *
     // * @param  int  $id
     // * @return \Illuminate\Http\Response
     // */
    //Doaa Elgheny 
  public function updatePurches()
    {
  try {

  if (Auth::check())
   {
    if(Auth::user()->can('Update Purches IN'))
      {
        $Purches_id= Input::get('pk');
        $column_name = Input::get('name');
        $column_value = Input::get('value');
        $gpsData = Purchase::whereId($Purches_id)->first();
        $gpsData-> $column_name=$column_value;
        if($gpsData->save())
        return \Response::json(array('status'=>1));
        else 
        return \Response::json(array('status'=>0));
       }
    else 
      {
    return \Response::json(array('status'=>'You do not have permission.'));
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
  return redirect('/login');
    }

    }
    public function updatePurchesDetails()
    {

        //Elmfrod ageb el Val El old 
      try {

 if (Auth::check())
  {


    if(Auth::user()->can('update Purches Details Bill Out'))
      {
      
        $Item_id= Input::get('pk'); 
        $PurchesDetails_id= Input::get('Purches'); 
     

        $oldQuantity= DB::table('item_purches')
                        ->where('purches_id', '=',$PurchesDetails_id)
                        ->where('item_id', '=',$Item_id)
                       ->select('quantity')
                       ->pluck('quantity');

                       //to get status for Bill
     $NewQuantity= Input::get('value'); 

     $WarehouseiD=DB::table('purches')
                        ->where('id', '=',$PurchesDetails_id)
                       ->select('Warehouse_iD')
                       ->pluck('Warehouse_iD');
      //i want Old Value in Item totalmstock
      $oldtotalmstockItems= DB::table('items')
                      ->where('id', '=',$Item_id)
                       ->select('totalmstock')
                       ->pluck('totalmstock');
       $oldtotaldatawrehouse= DB::table('datawarehouse_items')
                      ->where('item_id', '=',$Item_id)
                      ->where('datawarehouse_id', '=',$WarehouseiD[0])
                       ->select('quantityinstock')
                       ->pluck('quantityinstock');

       $updateQuantityWerhouse=$oldQuantity[0]-$NewQuantity;
       $updateQuantityItems=$oldQuantity[0]-$NewQuantity;
      if($oldtotaldatawrehouse[0] >$NewQuantity) 
      {

      
      $quantitymItems=$oldtotalmstockItems[0]+$updateQuantityItems;
      $quantityWarehouse=$oldtotaldatawrehouse[0]+$updateQuantityWerhouse;
      //update Item With New Quantity   

          $column_name ='totalmstock';
          $ItemData =Item::whereId($Item_id)->first(); 
          $ItemData->$column_name=$quantitymItems;
           $ItemData->save();
  
      // update table item_purches

       $ItemData->getitempurches()->detach($PurchesDetails_id);

       $ItemData->getitempurches()->attach($PurchesDetails_id, ['quantity'=>$NewQuantity]);
     // Update in datawarehouse_items
         
       
                      
        $test= $ItemData->getDatawerehouseitem()->detach($WarehouseiD[0]);  

     $ItemData->getDatawerehouseitem()->attach($WarehouseiD[0],["quantityinstock" => $quantityWarehouse]);

if( $test==1)
        return \Response::json(array('status'=>1));
        else 
        return \Response::json(array('status'=>0));
       }
    else 
      {
  return \Response::json(array('status'=>'total quantity not Valid'));
      }          

    }
    else 
      {
  return \Response::json(array('status'=>'You do not have permission.'));
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
  return redirect('/login');
    }
    }

    public function show($id)
    {
        $purchase=Purchase::find($id);
       
        $pic = \Image::make($purchase->RelatedDecomentLink);


        $response = \Response::make($pic->encode('jpeg'));
        $response->header('Content-Type', 'image/jpeg');

        return $response;

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,$itemid)
    {
        try {

 if (Auth::check())
  {


    if(Auth::user()->can('Delete Bill Out'))
      {



 $PurchesDetails_id = DB::table('item_purches')
                      ->where('purches_id', '=',$id)
                      ->where('item_id','=',$itemid)
                       ->select('id')
                      -> pluck('id');

 $PurchesDetails = DB::table('item_purches')
                      ->join('purches','purches.id','=','item_purches.purches_id')
                      ->join('items','items.id','=','item_purches.item_id')
                      ->where('purches_id', '=',$id)
                      ->where('item_id','=',$itemid)
                       ->select('quantity','Type','totalmstock','Warehouse_iD')
                      -> first();
 $QuantityWearehouse = DB::table('datawarehouse_items')
                      ->where('datawarehouse_id', '=',$PurchesDetails->Warehouse_iD)
                      ->where('item_id','=',$itemid)
                       ->select('quantityinstock')
                      -> pluck('quantityinstock');

       $itemattch=Item::whereId($itemid)->first(); 

    
    //Update Quantity

 $updateQuantityItems=($PurchesDetails->totalmstock) + ($PurchesDetails->quantity); 
 $updateQuantityWearehouse=($QuantityWearehouse[0]) + ($PurchesDetails->quantity); 
// dd($updateQuantity,($PurchesDetails->totalmstock) , ($PurchesDetails->quantity));
//Updae Quantity in Items Table 
 $PurchesDetcheck = DB::table('item_purches')
                      ->where('purches_id', '=',$id)
                                      ->select('id')->get();

   if(count($PurchesDetcheck)==1)
{
 return redirect()->back()->with('message', 'برجاء ادخال الصنف الجديد لهذه الفاتوره قبل حذف هذا الصنف'); //msh ed5ol 
}

else
{

      $column_name ='totalmstock';
      $ItemData =Item::whereId($itemid)->first();              
     $ItemData->$column_name=$updateQuantityItems;
      $ItemData->save();

//Update DataWewehouse_Items

  $test= $itemattch->getDatawerehouseitem()->detach($PurchesDetails->Warehouse_iD);  

   $ItemData->getDatawerehouseitem()->attach($PurchesDetails->Warehouse_iD,["quantityinstock" => $updateQuantityWearehouse]);

       $test= $itemattch->getitempurches()->detach($id);
       return redirect('BillOut');
     }

}
    else 
      {
 return view('errors.403');
      }

   }//endAuth

else
{
 // dd("no auth");
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
     // dd("error");
  return redirect('/login');
    }

    }
    function checkpurscahsenumber()
    {
   
      $PNumer=Input::get('PNumer');
      // dd($username);
    $res = DB::table('purches')
            ->select('PurchesNo')
            ->where('PurchesNo','=',$PNumer)
         ->where('Type','=','Out')
            ->get(); 

   if (count($res)<= 0)

        $isAvailable = true;
        else
         $isAvailable = false;
    return \Response::json(array('valid' =>$isAvailable));
  
    }
}
