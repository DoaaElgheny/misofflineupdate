<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use Excel;
use Input;
use File;
use Validator;
use DB;
use Auth;
use Symfony\Component\Debug\Exception\FatalThrowableError;
use Datatables;
use Illuminate\View\Middleware\ErrorBinder;
use Exception;
use App\Gps;
use App\Purchase;
use App\Item;
use App\Warehouse;
use Image;
use App\ActivityItem;
use Session;
use Carbon;
use App\Datawarehouse;

class PurchaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
       public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        try {

 // if (Session::has('user'))
 //  {
 $WareID=[];
    

    if(Auth::user()->can('Purches IN Show'))
      {
       

       if(Auth::user()->can('Show All Purches'))
      {
         $purchases= Purchase::
             select('*')
             ->where('Type','=','In')
              ->whereBetween('Date', array('2014-12-28','2018-12-28'))
             ->orderby('Date', 'DESC')
             ->get();
            

      } 
      else
      {
         $warehouse=Warehouse::where('Government',Auth::user()->Government)->first();
         $purchases= Purchase::
             select('*')
             ->where('Type','=','In')
               ->where('Warehouse_iD','=',$warehouse->id)

              ->whereBetween('Date', array('2014-12-28', '2018-12-28'))
             ->orderby('Date', 'DESC')
             ->get();
      }

      $user=Auth::user();
      $activityitems =ActivityItem::lists('Code','id');
      // $user = User::lists('Name', 'id');
      $item = item::lists('Name', 'id');

     if(Auth::user()->can('Show All Warehouse'))
      {
         $warehouseID =DB::table('datawarehouses')
    
       ->select ('Name', 'id')->get();
        $warehouse =Datawarehouse::select ('Name', 'id')
       ->lists('Name', 'id');
       
      }
      else //if Show Specific Warehouse
      {
         $warehouseID =DB::table('datawarehouses')
      ->where('Government','=',Auth::user()->Government)
       ->select ('Name', 'id')->get();
       $warehouse =Datawarehouse::where('Government','=',Auth::user()->Government)
       ->select ('Name', 'id')
       ->lists('Name', 'id');
       
      }
   foreach ($warehouseID as  $value) {
         array_push($WareID, $value->id);
       }
       $item = item::lists('items.Name', 'items.id');
     
      return view('purchase.index',compact('purchases','user','item','warehouse','activityitems'));

      }
    else 
      {

  return view('errors.403');
      }

//    }//endAuth

// else
// {
//   dd ("not Auth");
// return redirect('/login');
// }

}//endtry
catch(Exception $e) 
    {
     // dd($e);
  return redirect('/login');
    }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function showallDetails($id)
    {
       try {

 if (Session::has('user'))
  {
    if(Auth::user()->can('showallDetailsIN'))
      {

       $itemsId=[];
      $id=$id;
      // return Item Expect Exsist 
     $purchaseItem =DB::table('item_purches')
         ->join('items','items.id','=','item_purches.item_id')
         ->where('item_purches.purches_id', '=',$id)

           ->select('item_purches.item_id')->get();
          
           for ($i=0;$i<count($purchaseItem);$i++)
           {
            array_push( $itemsId, $purchaseItem[$i]->item_id);
           }
         if(Auth::user()->can('Show All Warehouse'))
      {
        $warehouseID =DB::table('datawarehouses')
      // ->where('Government','=',Auth::user()->Government)
       ->select ('Name', 'id')->get();
      }
      else //if Show Specific Warehouse
      {
        $warehouseID =DB::table('datawarehouses')
      ->where('Government','=',Auth::user()->Government)
       ->select ('Name', 'id')->get();

      } 
      $WareID=[];
       foreach ($warehouseID as  $value) {
         array_push($WareID, $value->id);
       }

    $item = item::whereNotIn('items.id',$itemsId)
    
         ->select('items.Name','items.id')
         ->lists('Name', 'id');
 
         $purchaseDetails =DB::table('item_purches')
         ->join('items','items.id','=','item_purches.item_id')
         ->where('item_purches.purches_id', '=',$id)
           ->select('item_purches.quantity','item_purches.item_id','items.Name','item_purches.purches_id')->get();

// dd($purchaseDetails,$item,$id);
       return view('purchase.detailsPurches',compact('purchaseDetails','item','id'));

      }
    else 
      {
        
  return view('errors.403');
      }

   }//endAuth

else
{
  
 // dd("no auth");
return redirect('/login');

}

}//endtry
catch(Exception $e) 
    {
     // dd($e);
  return redirect('/login');
    }
    }
    //Doaa Elgheny 
    public function storeDetailsIN()
    {
      

  //try {

 if (Session::has('user'))
  {

    if(Auth::user()->can('StorePurchesBillDetails'))
      {

       $purchase_id=Input::get('id');  

     
     $purchase = Purchase::where('id', '=',$purchase_id)
                       ->first();


       $descraption=Input::get('descraption');

       $item=Input::get('item');
      
        $n=count($item);

       for($i=0;$i<$n-1;$i++)
        {

            $new_item=Item::where('id',$item[$i])->first();
             $descr= $descraption[$i];
  // dd($descraption[$i]);
          $purchase->getpurchaseitem()->attach($item[$i], ['quantity'=>$descr]);

          
  //We should update Stock Quantity Be Plus When Type Be in decrease when be out
              //Doaa Elgheny 17/10/2016
               //dd($item[$i]);

           $itemqantity =DB::table('datawarehouse_items')
                        ->where('datawarehouse_id', '=',$purchase->Warehouse_iD)
                        ->where('item_id', '=',$item[$i])
                       ->select('quantityinstock')
                       ->pluck('quantityinstock');
                  
                    if ($itemqantity == null)//Msh mogod nd5al Wa7d gdid New Row at datawarehouse_items
                    {
                      
                      $NewData=$purchase->Warehouse_iD;
                     $itemattch=Item::whereId($item[$i])->first();
                     
                       $itemattch->getDatawerehouseitem()->attach($NewData,["quantityinstock" => $descr]);
                     
                        //Update Items 

                       $column_name ='totalmstock';
                       $ItemData =Item::whereId($new_item->id)->first();
                      
                      $ItemData->$column_name=$descr;
                      $ItemData->save();
                      
                       }
                  else //Update in Exsits items and datawarehouse_items
                    {
                       $NewData=$purchase->Warehouse_iD;
                       $NewQuantity=$itemqantity[0]+$descr;
                         //Update Items 
                     
                       $column_name ='totalmstock';
                       $ItemData =Item::whereId($new_item->id)->first();
                      
                      $ItemData->$column_name=$NewQuantity;
                      $ItemData->save();
                    //  dd('ddddd');
                    //update datawarehouse_items
                       $gpsData =DB::table('datawarehouse_items')->where('item_id','=',$item[$i])
                       ->where('datawarehouse_id','=',$NewData)
                       ->first();
                      // dd($i);
                     $itemattch=Item::whereId($item[$i])->first();
                      
                      $itemattch->getDatawerehouseitem()->detach();
                   //  dd($NewQuantity);
                     
                       $itemattch->getDatawerehouseitem()->attach($NewData,["quantityinstock" => $NewQuantity]);
                     

               }
                     
        }

        //
       
       return redirect('purchase');
          }
    else 
      {
       
   return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

// }//endtry
// catch(Exception $e) 
//     {
//   return redirect('/login');
//     }
    }
     public function UpdateImage(Request $request)
    {
     
        
        $id=Input::get('id');
        
         $file_img = Input::file('RelatedDecomentLink');
        
        $Purchase =Purchase::whereId($id)->first();
     
        if($file_img!==null)
        {

            $imageName = $file_img->getClientOriginalName();



            $path=public_path("assets/dist/img/").$imageName;

 
                if (!file_exists($path)) {
                  $file_img->move(public_path("/assets/dist/img/Purchase"),$imageName);
                  $Purchase->RelatedDecomentLink="/MIS/assets/dist/img/Purchase/".$imageName;
                
                  
                }
                else
                {
                   
                   $random_string = md5(microtime());

                   $file_img->move(public_path("/assets/dist/img/Purchase/"),$random_string.".jpg");

                  $Purchase->RelatedDecomentLink="/MIS/assets/dist/img/Purchase/".$random_string.".jpg";

              
                }    
             
        }   
       
  
        $Purchase->save();
        return redirect('purchase');
    


    }
     public function EditPhotoPurchesIN($id)
   {
     $Purchase=Purchase::where('id',$id)
     ->where('Type','IN')->first();

    return view('purchase.EditPhoto',compact('Purchase'));
   }
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
     
        try {

 if (Session::has('user'))
  {
 

    // if(Auth::user()->can('StorePurchesBill'))
    //   {


       $purchase= New Purchase; 
        
       $purchase->PurchesNo=Input::get('PNumer');       
       $purchase->Date=date("Y-m-d",strtotime(Input::get('Date')));
       $purchase->Type='In';
       $purchase->users_id=Auth::user()->id;     
       $purchase->Warehouse_iD=Input::get('warehouse');
       $purchase->Description=Input::get('Note');
   
       $file_img = Input::file('pic');
       



     ////////////Save Image//////////////////
           // $file_img = Request::file('pic');
       
            $imageName = $file_img->getClientOriginalName();


                $path=public_path("assets/dist/img/Purchase").$imageName;
              // notation octale : valeur du mode correcte
                  

                if (!file_exists($path)) {
                  $file_img->move(public_path("assets/dist/img/Purchase/"),$imageName);
                  $purchase->RelatedDecomentLink="/MIS/assets/dist/img/Purchase/".$imageName; 
                }
                else
                {
                   $random_string = md5(microtime());
                   $file_img->move(public_path("assets/dist/img/Purchase/"),$random_string.".jpg");
                   
                  $purchase->RelatedDecomentLink="/MIS/assets/dist/img/Purchase/".$random_string.".jpg";
                
                }


   
       //End Image 
       $purchase->save();
 // dd($purchase);

       $descraption=Input::get('quantity');

       $item=Input::get('item');
        $n=count($item);
       

        for($i=0;$i<$n-1;$i++)
        {

            $new_item=Item::where('id',$item[$i])->first();
             $descr= $descraption[$i];
  // dd($descraption[$i]);
          $purchase->getpurchaseitem()->attach($item[$i], ['quantity'=>$descr]);

          
  //We should update Stock Quantity Be Plus When Type Be in decrease when be out
              //Doaa Elgheny 17/10/2016
               //dd($item[$i]);

           $itemqantity =DB::table('datawarehouse_items')
                        ->where('datawarehouse_id', '=',Input::get('warehouse'))
                        ->where('item_id', '=',$item[$i])
                       ->select('quantityinstock')
                       ->pluck('quantityinstock');
                  
                    if ($itemqantity == null)//Msh mogod nd5al Wa7d gdid New Row at datawarehouse_items
                    {
                     
                    //insert New Row
                     $gpsData =DB::table('datawarehouse_items')->where('item_id','=',$item[$i])
                       ->where('datawarehouse_id','=',Input::get('warehouse'))
                       ->first();

                     $itemattch=Item::whereId($item[$i])->first();
                     
                      $NewData=(int)Input::get('warehouse');
                       $itemattch->getDatawerehouseitem()->attach($NewData,["quantityinstock" => $descr]);
                     
                        //Update Items 

                       $column_name ='totalmstock';
                       $ItemData =Item::whereId($new_item->id)->first();
                      
                      $ItemData->$column_name=$descr;
                      $ItemData->save();
                      
                       }
                  else //Update in Exsits items and datawarehouse_items
                    {

             
                       $NewQuantity=$itemqantity[0]+$descr;

                       
                         //Update Items 
                     
                       $column_name ='totalmstock';
                       $ItemData =Item::whereId($new_item->id)->first();
                      
                      $ItemData->$column_name=$NewQuantity;
                      $ItemData->save();
                    //  dd('ddddd');
                    //update datawarehouse_items
                       $gpsData =DB::table('datawarehouse_items')->where('item_id','=',$item[$i])
                       ->where('datawarehouse_id','=',Input::get('warehouse'))
                       ->first();
                      // dd($i);
                     $itemattch=Item::whereId($item[$i])->first();
                      
                      $itemattch->getDatawerehouseitem()->detach();
                   //  dd($NewQuantity);
                     
                      $NewData=(int)Input::get('warehouse');

                       $itemattch->getDatawerehouseitem()->attach($NewData,["quantityinstock" => $NewQuantity]);
                     

               }
                     
        }

        //
       return redirect('purchase');
  //       }
  //   else 
  //     {
        
  // return view('errors.403');
  //     }

   }//endAuth

else
{
  
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
  return redirect('/login');
    }
   }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    //Doaa Elgheny 
public function updatePurches()
    {

 try {

 if (Session::has('user'))
  {


    if(Auth::user()->can('UpdatePurchesIN'))
      {
         $Purches_id= Input::get('pk');

        $column_name = Input::get('name');
        $column_value = Input::get('value');

        $gpsData = Purchase::whereId($Purches_id)->first();
        $gpsData-> $column_name=$column_value;

        if($gpsData->save())

        return \Response::json(array('status'=>1));
        else 
        return \Response::json(array('status'=>0));
      
      }
    else 
      {
  return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
  return redirect('/login');
    }

    }
    public function updatePurchesDetails()
    {

        //Elmfrod ageb el Val El old 
       try {

 if (Session::has('user'))
  {


    if(Auth::user()->can('updatePurchesDetails'))
      {

  
        $Item_id= Input::get('pk'); 
        $PurchesDetails_id= Input::get('Purches'); 
     
   
        $oldQuantity=DB::table('item_purches')
                        ->where('purches_id', '=',$PurchesDetails_id)
                        ->where('item_id', '=',$Item_id)
                       ->select('quantity')
                       ->pluck('quantity');
         
                       //to get status for Bill
         
        $Warehouseid= Purchase::where('id', '=',$PurchesDetails_id)
                       ->select('Warehouse_iD')
                       ->pluck('Warehouse_iD'); 

     $NewQuantity= Input::get('value');
     
      //i want Old Value in Item totalmstock
     $oldtotalmstockItems=Item::where('id', '=',$Item_id)
                       ->select('totalmstock')
                       ->pluck('totalmstock');
 
       $oldtotaldatawrehouse=DB::table('datawarehouse_items')
                      ->where('item_id', '=',$Item_id)
                      ->where('datawarehouse_id', '=',$Warehouseid[0])
                       ->select('quantityinstock')
                       ->pluck('quantityinstock');


       $updateQuantityWerhouse=$NewQuantity-$oldQuantity[0];
       $updateQuantityItems=$NewQuantity-$oldQuantity[0];


      $quantitymItems=$oldtotalmstockItems[0]+$updateQuantityItems;
      $quantityWarehouse=$oldtotaldatawrehouse[0]+$updateQuantityWerhouse;
       if($quantityWarehouse<0)
       {
      
         return \Response::json(array('status'=>'total quantity not Valid.'));
       }
       else if( $NewQuantity < $oldtotaldatawrehouse[0]){
    
        
       return \Response::json(array('status'=>'total quantity not Valid.'));
                     
        }
        else
        {
//update Item With New Quantity    
          $column_name ='totalmstock';
          $ItemData =Item::whereId($Item_id)->first(); 
          $ItemData->$column_name=$quantitymItems;
          $ItemData->save();
        
      // update table item_purches

     $updateitem=DB::table('item_purches')
                                        ->where('purches_id', $PurchesDetails_id)
                                        ->where('item_id', $Item_id)
                                        ->update(['quantity' =>$NewQuantity]);


       // $ItemData->getitempurches()->detach($PurchesDetails_id);       
       // $ItemData->getitempurches()->attach($PurchesDetails_id, ['quantity'=>$NewQuantity]);
  
     //  $test= $ItemData->getDatawerehouseitem()->detach($Warehouseid[0]);  
     // $ItemData->getDatawerehouseitem()->attach($Warehouseid[0],["quantityinstock" =>$quantityWarehouse]);

   $updatewarehouse=DB::table('datawarehouse_items')
                                        ->where('datawarehouse_id', $Warehouseid[0])
                                        ->where('item_id', $Item_id)
                                        ->update(['quantityinstock' =>$quantityWarehouse]);

       if( $updatewarehouse==1)
        return \Response::json(array('status'=>1));
        else 
        return \Response::json(array('status'=>0));
        }
      }
    else 
      {
  return \Response::json(array('status'=>'You do not have permission.'));
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
  return redirect('/login');
    }

   }

    public function show($id)
    {
        $purchase=Purchase::find($id);
       
        $pic = Image::make($purchase->RelatedDecomentLink);


        $response = \Response::make($pic->encode('jpeg'));
        $response->header('Content-Type', 'image/jpeg');

        return $response;

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,$itemid)
    {


   // try {

 if (Session::has('user'))
  {


    if(Auth::user()->can('Delete Purches'))
      {


 $PurchesDetcheck = DB::table('item_purches')
                      ->where('purches_id', '=',$id)
                                      ->select('id')->get();

 $PurchesDetails_id = DB::table('item_purches')
                      ->where('purches_id', '=',$id)
                      ->where('item_id','=',$itemid)
                      ->select('id')
                      -> pluck('id');

 $PurchesDetails = DB::table('item_purches')
                      ->join('purches','purches.id','=','item_purches.purches_id')
                      ->join('items','items.id','=','item_purches.item_id')
                      ->where('purches_id', '=',$id)
                      ->where('item_id','=',$itemid)
                      ->select('quantity','Type','totalmstock','Warehouse_iD')
                      -> first();
  $QuantityWearehouse = DB::table('datawarehouse_items')
                      ->where('datawarehouse_id', '=',$PurchesDetails->Warehouse_iD)
                      ->where('item_id','=',$itemid)
                       ->select('quantityinstock')
                      -> pluck('quantityinstock');
  $itemattch=Item::whereId($itemid)->first(); 

    
    //Update Quantity

  
   $updateQuantityItems=($PurchesDetails->totalmstock) - ($PurchesDetails->quantity); 
 $updateQuantityWearehouse=($QuantityWearehouse[0]) - ($PurchesDetails->quantity); 
  


//Update DataWewehouse_Items
 if(count($PurchesDetcheck)==1)
{
 return redirect()->back()->with('message', 'برجاء ادخال الصنف الجديد لهذه الفاتوره قبل حذف هذا الصنف'); //msh ed5ol 
}
else if ($updateQuantityWearehouse <0)
{
  
return redirect()->back()->with('message', 'لا يمكنك مسح الكمية'); //
}

else
{
     $column_name ='totalmstock';
      $ItemData =Item::whereId($itemid)->first();              
     $ItemData->$column_name=$updateQuantityItems;
       $ItemData->save();
    $test= $itemattch->getDatawerehouseitem()->detach($PurchesDetails->Warehouse_iD);  

   $ItemData->getDatawerehouseitem()->attach($PurchesDetails->Warehouse_iD,["quantityinstock" => $updateQuantityWearehouse]);

       $test= $itemattch->getitempurches()->detach($id);
       
       return redirect('purchase');
}

  }
    else 
      {

  return view('errors.403');
      }

   }//endAuth
else
{
 // dd("string");
return redirect('/login');
}

// }//endtry
// catch(Exception $e) 
//     {
//   return redirect('/login');
//     }

}
    function checkpurscahsenumber()
    {
    
      $PNumer=Input::get('PNumer');
      // dd($username);
    $res = Purchase::select('PurchesNo')
            ->where('PurchesNo','=',$PNumer)
          ->where('Type','=','In')
            ->get(); 

   if (count($res)<= 0)

        $isAvailable = true;
        else
         $isAvailable = false;




    return \Response::json(array('valid' =>$isAvailable));
  
    }
}
