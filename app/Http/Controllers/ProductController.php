<?php

namespace App\Http\Controllers;

//use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Session;
use Auth;
use App\Company;
use App\User;
use App\Activity;
use Image;
use App\FrontOffice;
use App\Product;
use Collection;
use Excel;
use App\Promoter;
use App\Price;
use Request;
use DB;
use Exception;
use File;

class ProductController extends Controller
{ public function __construct()
   {
       $this->middleware('auth');
   }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
          try {

 if (Auth::check())
  {


    if(Auth::user()->can('show-products'))
      {
        Session::put('comefrom', 'index');
        $product=product::get();
        $company =company::lists('name', 'id');
        return view('products.index',compact('product','company'));  

      }
    else 
      {
    return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
         
  }
    public function showproductphoto($id)
    {
                try {

 if (Auth::check())
  {
      $product=Product::whereId($id)->first();
      return view('products.productphoto',compact('product'));
         }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
    }

public function get($id){

$entry=Product::find($id);
// dd(storage_path().'/app'.$entry->FileName);
 $path = storage_path().'/app'.'/'.$entry->FileName;
 
if($entry->FileName !="")
{
  return response()->download($path);
}
else
{
return view('products.Showdatasheet',compact('id'));

}
 
 }
 public function add() {

   $file = Request::file('filefield');
   $extension = $file->getClientOriginalExtension();
   \Storage::disk('local')->put($file->getClientOriginalName(),  File::get($file));
  
   $entry=Product::find(Input::get('id'));
   $entry->mime = $file->getClientMimeType();   
   $entry->originalfilename = $file->getClientOriginalName();
   $entry->FileName= $file->getClientOriginalName();
   $entry->save();
   return redirect('/ShowAllProducts');
   
 }

public function showCV($id)
{

 return view('products.Showdatasheet',compact('id'));
}

public function ViewPDF($id)

{
  $orginalName=Product::where('id',$id)->first();
   $file = 'file:///var/www/html/offlineneew/'.$orginalName->FileName;



                if (file_exists($file)){
                  

                   $ext =File::extension($file);
                 
                    if($ext=='pdf'){
                        $content_types='application/pdf';
                        return response(file_get_contents($file),200)
                           ->header('Content-Type',$content_types);
                       }
                       else if ($ext=='xls')
                       {


                       }
                   
                    
                                                             
                }else{
                 exit('Requested file does not exist on our server!');
                }
}
   public function shoecementproduct()
    {
          try {

 if (Auth::check())
  {


    if(Auth::user()->can('show-cementproducts'))
      {
        Session::put('comefrom', 'cementproduct');
        $cc=company::where('Name','=','أسمنت أسيوط')->first();
        $product= Product::where('Company_id','=',$cc->id)->get();
        $company =company::lists('name', 'id');
        return view('products.index',compact('product','company'));  

      }
    else 
      {
    return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
         
  }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
          try {

 if (Auth::check())
  {


    if(Auth::user()->can('create-products'))
      {

        
         $file_frontimg = Input::file('frontimg');
         $file_backimg = Input::file('backimg');
         

       $backimg=null;
       $frontimg=null;

        $cement_name=Input::get('cement_name');
        $Enname=Input::get('Enname');
         $Code=Input::get('Code');
        $weight=Input::get('weight');
        $rank=Input::get('rank');
        $company_id=Input::get('company');
        $specifications=Input::get('specifications');
        $guidlines=Input::get('guidlines');
        $security=Input::get('security');
        $others=Input::get('others');
        $producation_date=Input::get('producation_date');
        $packing_date=Input::get('packing_date');



        $product = new product;
        $product->cement_name=$cement_name;
        $product->Enname=$Enname;
        $product->Code=$Code;
        $product->Type='Cement';
        $product->weight=$weight;
        $product->rank=$rank;
        $product->company_id=$company_id;
        $product->specifications=$specifications;
        $product->guidlines=$guidlines;
        $product->security=$security;
        $product->others=$others;
        $product->producation_date=$producation_date;
        $product->packing_date=$packing_date;
        if($file_frontimg!==null)
        {

             $imageName = $file_frontimg->getClientOriginalName();


                $path=public_path("/offlineneew/public/assets/dist/img/Product/").$imageName;
              // notation octale : valeur du mode correcte
                if (!file_exists($path)) {
                  $file_frontimg->move(public_path("offlineneew/public/assets/dist/img/Product"),$imageName);   
                  $product->Frontimg="/assets/dist/img/Product/".$imageName;           
                }
                else
                {
                  $random_string = md5(microtime());
                  $file_frontimg->move(public_path("/offlineneew/public/assets/dist/img/Product"),$random_string.".jpg");
                  $product->Frontimg="/assets/dist/img/Product/".$random_string.".jpg";                
                }
             
        }  
         if($file_backimg!==null)
        {
             $imageName = $file_backimg->getClientOriginalName();


                $path=public_path("/offlineneew/public/assets/dist/img/Product/").$imageName;
              // notation octale : valeur du mode correcte
                  

                if (!file_exists($path)) {
                  $file_backimg->move(public_path("/offlineneew/public/assets/dist/img/Product"),$imageName);   
                  $product->Backimg="/assets/dist/img/Product/".$imageName;           
                }
                else
                {
                  $random_string = md5(microtime());
                  $file_backimg->move(public_path("/offlineneew/public/assets/dist/img/Product"),$random_string.".jpg");
                  $product->Backimg="/offlineneew/public//assets/dist/img/Product/".$random_string.".jpg";                
                }
        }
  
        $product->save();
        $back=Session::get('comefrom');
        if($back==='index')
        return redirect('/products');
      else
        return redirect('/ShowAllProducts');


      }
    else 
      {
    return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
      

    }
 public function checkProductCode()
    {
       $Productcode=Input::get('Code');
      $product = product::where('Code','=',$Productcode)->first();
     if($product==null)     
        $isAvailable = true;
        else
         $isAvailable = false;

    return \Response::json(array('valid' =>$isAvailable,));


       
    }
 
 public function UpdateProductall(Request $request)
    {
                  try {

 if (Auth::check())
  {
    // dd(input::get('id'));
        $id=Input::get('id');
         $file_frontimg = Input::file('frontimg');
         $file_backimg = Input::file('backimg');
         // dd( $file_frontimg,   $file_backimg);

       $backimg=null;
       $frontimg=null;

        $cement_name=Input::get('cement_name');
        $Enname=Input::get('Enname');
        $weight=Input::get('weight');
        $rank=Input::get('rank');
        $company_id=Input::get('company');
        $specifications=Input::get('specifications');
        $guidlines=Input::get('guidlines');
        $security=Input::get('security');
        $others=Input::get('others');
        $producation_date=Input::get('producation_date');
        $packing_date=Input::get('packing_date');



        $product =product::whereId($id)->first();
        $product->cement_name=$cement_name;
        $product->Enname=$Enname;
        $product->weight=$weight;
        $product->rank=$rank;
        $product->company_id=$company_id;
        $product->specifications=$specifications;
        $product->guidlines=$guidlines;
        $product->security=$security;
        $product->others=$others;
        $product->producation_date=$producation_date;
        $product->packing_date=$packing_date;
        if($file_frontimg!==null)
        {
          
          $imageName = $file_frontimg->getClientOriginalName();


                $path=public_path("assets/dist/img/Product/").$imageName;
              // notation octale : valeur du mode correcte
               

                if (!file_exists($path)) {
                  $file_frontimg->move(public_path("assets/dist/img/Product/"),$imageName);   
                  $product->Frontimg="/offlineneew/public/assets/dist/img/Product/".$imageName;           
                }
                else
                {
                  $random_string = md5(microtime());
                  $file_frontimg->move(public_path("assets/dist/img/Product/"),$random_string.".jpg");
                  $product->Frontimg="/offlineneew/public/assets/dist/img/Product/".$random_string.".jpg";                
                }
             
             
        }  
         if($file_backimg!==null)
        {
           $imageName = $file_backimg->getClientOriginalName();


                $path=public_path("assets/dist/img/Product/").$imageName;
              // notation octale : valeur du mode correcte
                  

                if (!file_exists($path)) {
                  $file_backimg->move(public_path("assets/dist/img/Product/"),$imageName);   
                  $product->Backimg="/offlineneew/public/assets/dist/img/Product/".$imageName;           
                }
                else
                {
                  $random_string = md5(microtime());
                  $file_backimg->move(public_path("assets/dist/img/Product/"),$random_string.".jpg");
                  $product->Backimg="/offlineneew/public/assets/dist/img/Product/".$random_string.".jpg";                
                }
        }
  
        $product->save();
             $back=Session::get('comefrom');
        if($back==='index')
        return redirect('/products');
      else
        return redirect('/ShowAllProducts');  
         }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }



    }
public function checkProductname()
 {
            try {

 if (Auth::check())
  {
      $id=Input::get('id');
      if($id==null)
      {
          $productname=Input::get('cement_name');
          $product = Product::where('cement_name','=',$productname)->first();
         if($product==[])     
            $isAvailable = true;
            else
             $isAvailable = false;
       }
       else
       {
         $productname=Input::get('cement_name');
          $product = Product::where('cement_name','=',$productname)
         ->where('id','!=',$id)
         ->first();
         if($product==[])     
            $isAvailable = true;
            else
             $isAvailable = false;
       }

    return \Response::json(array('valid' =>$isAvailable,));
       }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
 }
 
  public function checkproductenglishname()
 { 
            try {

 if (Auth::check())
  {
      $id=Input::get('id');
      if($id==null)
      {
           $productname=Input::get('Enname');
          $product = Product::where('Enname','=',$productname)
         ->first();
         if($product==[])    
            $isAvailable = true;
            else
             $isAvailable = false;
      }
      else
      {
         $productname=Input::get('Enname');
          $product = Product::
         where('Enname','=',$productname)
         ->where('id','!=',$id)
         ->first();
         if($product==[])    
            $isAvailable = true;
            else
             $isAvailable = false;
      }

    return \Response::json(array('valid' =>$isAvailable,));
       }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
 }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      
        try {

 if (Auth::check())
  {


    if(Auth::user()->can('edit-products'))
      {

        $product=product::find($id);
               $company =company::lists('Name', 'id');
               return view('products.Edit',compact('product','company'));

      }
    else 
      {
    return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
             
  
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {

 if (Auth::check())
  {


    if(Auth::user()->can('delete-products'))
      {

      $product = product::find($id);
            $product->delete();
                $back=Session::get('comefrom');
        if($back==='index')
        return redirect('/products');
      else
        return redirect('/ShowAllProducts');

      }
    else 
      {
    return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
            
 
        
    }
     public function importproduct()
  {
    // try
    // {
ini_set('xdebug.max_nesting_level', 120);
ini_set('max_execution_time', 300);


    $pic=[];

       $i=0;
  $temp= Request::get('asdasd'); 


   if(isset($temp))

 { 


   $filename = Input::file('file')->getClientOriginalName();
     

     $Dpath = base_path();


       $upload_success =Input::file('file')->move( $Dpath, $filename);
          $objPHPExcel = PHPExcel_IOFactory::load($upload_success);

       dd($objPHPExcel);

                  $objWorksheet = $objPHPExcel->getActiveSheet();
                  $highestRow = $objWorksheet->getHighestRow();

                    for ($row=2; $row <= ($highestRow); $row++) { 


              foreach ($objWorksheet->getDrawingCollection()as $drawing) {

                 $string = $drawing->getCoordinates();

                  if(($string ==\PHPExcel_Cell::stringFromColumnIndex(1).$row))
                       {
                                         ob_start();

                                        call_user_func(
                                        $drawing->getRenderingFunction(),
                                        $drawing->getImageResource()
                                        );
                                        $imageContents = ob_get_contents();
                                        ob_end_clean();
                                        $extension = 'jpeg';

                                        $myFileName = 'BImage_'.++$i.'.'.$extension;
                                        file_put_contents($myFileName,$imageContents);
                                        $img = Image::make($myFileName)->resize(200,200);
                                        Response::make($img->encode('jpeg'));  
                                        
                                         $n=count($pic);

                                       
                                         array_push($pic, $myFileName); 

                           } 


                          elseif(($string ==\PHPExcel_Cell::stringFromColumnIndex(0).$row))
                            {
                                      
                                        ob_start();

                                        call_user_func(
                                        $drawing->getRenderingFunction(),
                                        $drawing->getImageResource()
                                        );
                                        $imageContents = ob_get_contents();
                                        ob_end_clean();
                                        $extension = 'jpeg';

                                        $myFileName = 'FImage_'.++$i.'.'.$extension;
                                        file_put_contents($myFileName,$imageContents);
                                        $img = Image::make($myFileName)->resize(200,200);
                                        Response::make($img->encode('jpeg'));  
                                        $n=count($pic);
                                    
                                     

                            
                                     if ($n <= ((($row-1)*2)-1)) {

                                           
                                            while ( $n < ((($row-1)*2)-1)) {
                                                       $logo = base_path() . '\public\assets\img\Cancel.jpg'; 
                                                       $img = Image::make( $logo)->resize(200,200);
                                                       Response::make($img->encode('png'));  
                                                       

                                                        array_push($pic,$logo);

                                                         $n=count($pic);
                                                      }

                                       }






                                         array_push($pic, $myFileName); 
                                       



                           } 


                    }


                           $n=count($pic);


                                          while ($n < (($row-1)*2)) {

                                                       $logo = base_path() . '\public\assets\img\Cancel.jpg'; 
                                                       $img = Image::make( $logo)->resize(200,200);
                                                       Response::make($img->encode('png'));  
                                                       

                                                        array_push($pic,$logo);

                                                         $n=count($pic);
                                                      }

                           
              }


            // xls to csv conversion
            $nameOnly = explode(".",$filename);

            $newCSV =$nameOnly[0]."."."csv";

            $PathnewCSV= $Dpath."\\".$newCSV ;

            //chmod($PathnewCSV, 0777);
            $myfile = fopen($PathnewCSV, "w");

        app('App\Http\Controllers\ProductController')->convertXLStoCSV($upload_success, $PathnewCSV);
$document = PHPExcel_IOFactory::load($upload_success);

// Get the active sheet as an array
$activeSheetData = $document->getActiveSheet()->toArray(null, true, true, true);
  
      // Excel::filter('chunk')->selectSheetsByIndex(0)->load($PathnewCSV)->chunk(150, function($results) use($pic){  
     
        // dd($activeSheetData);
      $n=0;
       $j=0;         

                   
        

                
               

  // $results = $reader->get()->all();
 // dd($results);
    for ($i=2;$i<=count($activeSheetData);$i++)
        {
            $bimg=$pic[$n];
            $fimg=$pic[$n+1];
            
               // app('App\Http\Controllers\ProductController')->ValidateCompany($activeSheetData,$i,$j);
               // app('App\Http\Controllers\ProductController')->ValidateOffice($activeSheetData,$i,$j);
               // app('App\Http\Controllers\ProductController')->ValidateProduct($activeSheetData,$i,$fimg,$bimg,$j);
                                  
                               
                  
                 
          $company=Company::where('Name','=',$activeSheetData[$i][array_search("Company Name",$activeSheetData[1])])->first();
          // if($company==[])
          // {
          //   $company =new Company();
          //   $company->Name=$activeSheetData[$i][array_search("Company Name",$activeSheetData[1])];
          //   $company->EnName=$activeSheetData[$i][array_search("Company Name",$activeSheetData[1])];
          //   $company->Government=$activeSheetData[$i][array_search("Company Gov",$activeSheetData[1])];
          //   $company->District=$activeSheetData[$i][array_search("Company City",$activeSheetData[1])];
          //   $company->Address=$activeSheetData[$i][array_search("Company Address",$activeSheetData[1])];
          //   $company->HeadOfficeGov=$activeSheetData[$i][array_search("Company Gov",$activeSheetData[1])];
          //   $company->HeadOfficeAddre=$activeSheetData[$i][array_search("Company Address",$activeSheetData[1])];
          //   // $company->ProductionLines=5;
          //   $company->save();
          // }
          // $sellerOffice=FrontOffice::where('Telephone','=',$activeSheetData[$i][array_search("Phone",$activeSheetData[1])])->first();
          // if($sellerOffice==[])
          // {
          //   $sellerOffice=new FrontOffice();
          //   $sellerOffice->Location=$activeSheetData[$i][array_search("Office Street",$activeSheetData[1])];
          //   $sellerOffice->Telephone=$activeSheetData[$i][array_search("Phone",$activeSheetData[1])];
          //   $sellerOffice->Government=$activeSheetData[$i][array_search("Office Gov",$activeSheetData[1])];
          //   $sellerOffice->District=$activeSheetData[$i][array_search("Office City",$activeSheetData[1])];
          //   $sellerOffice->Address=$activeSheetData[$i][array_search("Office Street",$activeSheetData[1])];
          //   $sellerOffice->company_id=$company->id;
          //   $sellerOffice->save();
          // }
            
            $product=new Product();
            $product->Cement_Name=$activeSheetData[$i][array_search("Cement Name",$activeSheetData[1])];
            $product->Enname=$activeSheetData[$i][array_search("Cement Name",$activeSheetData[1])];
            $product->Type="Type";
            $product->Weight=$activeSheetData[$i][array_search("Weight",$activeSheetData[1])];
            $product->Rank=$activeSheetData[$i][array_search("Rank",$activeSheetData[1])];
            $product->Specifications=$activeSheetData[$i][array_search("Specifications",$activeSheetData[1])];
            $product->Producation_date=$activeSheetData[$i][array_search("Production Date",$activeSheetData[1])];
            $product->Packing_date=$activeSheetData[$i][array_search("Packing Date",$activeSheetData[1])];
            $product->Guidlines=$activeSheetData[$i][array_search("Guidelines",$activeSheetData[1])];
            $product->Security=$activeSheetData[$i][array_search("Security",$activeSheetData[1])];
            $product->Others=$activeSheetData[$i][array_search("Others",$activeSheetData[1])];
            $product->Frontimg=$fimg;
            $product->Backimg=$bimg;
            $product->Company_id=$company->id;
            $product->Avialable=$activeSheetData[$i][array_search("Avialable",$activeSheetData[1])];
            $product->save();
          //   $product->getforntofficeproducts()->attach($sellerOffice->id);
          //   $price=new Price();
          //   $price->Government=$activeSheetData[$i][array_search("Office Gov",$activeSheetData[1])];
          //   $price->District=$activeSheetData[$i][array_search("Office City",$activeSheetData[1])];
          //   $price->Date=new \DateTime();
          //   $price->Price=$activeSheetData[$i][array_search("Price",$activeSheetData[1])];
          //   $price->Product_id=$product->id;
          //   $user=User::where('Name','=',$activeSheetData[$i][array_search("Promoter Name",$activeSheetData[1])])->first();
          //   if($user!=[])
          //     $price->User_id=$user->id;
          //   $price->save();
          $n+=2;
          $j+=1;
        }
  
   
   // });


          

  }

   return redirect('/products'); 
   
 // }
 // catch(Exception $e)s
 // {
 //   return redirect('/users'); 
 // }

}
   public function convertXLStoCSV($infile,$outfile)
    {
        $fileType = PHPExcel_IOFactory::identify($infile);
        $objReader = PHPExcel_IOFactory::createReader($fileType);
        
        $objReader->setReadDataOnly(false);   
        $objPHPExcel = $objReader->load($infile);    
       
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
        $objWriter->save($outfile);

    }
         public function showfrontimg($id)
    {
        $product = product::findOrFail($id);

        $pic = Image::make($product->Frontimg);
        $response = Response::make($pic->encode('jpeg'));

        //setting content-type
        $response->header('Content-Type', 'image/jpeg');

        return $response;


       
    }
    public function showbackimg($id)
    {
        $product = product::findOrFail($id);
        $pic = Image::make($product->Backimg);
        $response = Response::make($pic->encode('jpeg'));
        //setting content-type
        $response->header('Content-Type', 'image/jpeg');
        return $response;
    }
    public function productofficedelete($sellerofficeid,$productid)
    {
        
         $product=product::find($productid);
         $product->getforntofficeproducts()->detach($sellerofficeid);
         return redirect('/showprodoff/'.$productid);
    }
    public function showproductoffice($id)
    {
              try {

 if (Auth::check())
  {


    if(Auth::user()->can('show-productoffices'))
      {

     
      $sellerofficeids = DB::table('frontoffice_products')
     ->join('products','products.id','=','frontoffice_products.product_id')
     ->join('front_offices','front_offices.id','=','frontoffice_products.frontoffice_id')
     ->where('products.id','=',$id)
     ->select('front_offices.id')->lists('front_offices.id');
      $selleroffice = FrontOffice::whereIn('id',$sellerofficeids)->get();
      return view('products.productoffice',compact('selleroffice','id'));
   

      }
    else 
      {
    return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
    }
         public function ValidateProduct($activeSheetData, $i,$fimg,$bimg,$j){

        $GLOBALS['product_valid']=0;        
        if(!isset($GLOBALS['product'])) { $GLOBALS['product']= array(); } 
        if(!isset($ProductErr)) { $ProductErr = 'البيانات غير صحيحة للمنتج: '; } 

        if(!isset($GLOBALS['Emptyproduct'])) { 
            $GLOBALS['Emptyproduct']= array(); 
        } 
        if(!isset($EmptyProductErr)) { 
            $EmptyProductErr = 'اسم المنتج غير موجود للصف رقم: '; 
        }  

        if ($activeSheetData[$i][array_search("Cement Name",$activeSheetData[1])] == " ") {
            $activeSheetData[$i][array_search("Cement Name",$activeSheetData[1])]= null;
        }
        $name_regex = preg_match('/^[\s\p{Arabic}]+$/u' ,$activeSheetData[$i][array_search("Cement Name",$activeSheetData[1])]);
        $weight_regex = preg_match('/^([a-zA-Z0-9 ]|[\p{Arabic}]|[_]|[\-\[\]\'.\.\\\\])*$/u', $activeSheetData[$i][array_search("Weight",$activeSheetData[1])]);
        $rank_regex = preg_match('/^([a-zA-Z0-9 ]|[\p{Arabic}]|[_]|[\-\[\]\'.\.\\\\])*$/u' , $activeSheetData[$i][array_search("Rank",$activeSheetData[1])]);
        $specifications_regex = preg_match('/^[\pL\s]+$/u' , $activeSheetData[$i][array_search("Specifications",$activeSheetData[1])]);
        $producation_regex = preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/' , $activeSheetData[$i][array_search("Production Date",$activeSheetData[1])]);
        $packing_regex = preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/' , $activeSheetData[$i][array_search("Packing Date",$activeSheetData[1])]);
        $guidlines_regex = preg_match('/^[\s\p{Arabic}]+$/u' , $activeSheetData[$i][array_search("Guidelines",$activeSheetData[1])]);
        $security_regex = preg_match('/^[\s\p{Arabic}]+$/u' , $activeSheetData[$i][array_search("Security",$activeSheetData[1])]);
        $others_regex = preg_match('/^[\s\p{Arabic}]+$/u' , $activeSheetData[$i][array_search("Others",$activeSheetData[1])]);
if (isset($activeSheetData[$i][array_search("Company Name",$activeSheetData[1])])) {
    if (isset($activeSheetData[$i][array_search("Cement Name",$activeSheetData[1])])) {
        if ($name_regex == 1 ) { // true name 
            if ($weight_regex==1 || ! isset($activeSheetData[$i][array_search("Weight",$activeSheetData[1])])) { // true weight 
                if ($rank_regex == 1 || ! isset($activeSheetData[$i][array_search("Rank",$activeSheetData[1])])) { // true rank 
                        if ($producation_regex == 1 || !isset($activeSheetData[$i][array_search("Production Date",$activeSheetData[1])])) { 
                            if ($packing_regex == 1 || !isset($activeSheetData[$i][array_search("Packing Date",$activeSheetData[1])])) { 
                                if ($guidlines_regex == 1 || !isset($activeSheetData[$i][array_search("Guidelines",$activeSheetData[1])]) ) {
                                    if ($security_regex == 1|| !isset($activeSheetData[$i][array_search("Security",$activeSheetData[1])]) ) {
                                        if ($others_regex == 1 || !isset($activeSheetData[$i][array_search("Others",$activeSheetData[1])])) { 

                          $product=new Product();
                          $product->Cement_Name=$activeSheetData[$i][array_search("Cement Name",$activeSheetData[1])];
                          $product->Enname=$activeSheetData[$i][array_search("Cement Name",$activeSheetData[1])];
                          //$product->Type="Type";
                          $product->Weight=$activeSheetData[$i][array_search("Weight",$activeSheetData[1])];
                          $product->Rank=$activeSheetData[$i][array_search("Rank",$activeSheetData[1])];
                          $product->Specifications=$activeSheetData[$i][array_search("Specifications",$activeSheetData[1])];
                          $product->Producation_date=$activeSheetData[$i][array_search("Production Date",$activeSheetData[1])];
                          $product->Packing_date=$activeSheetData[$i][array_search("Packing Date",$activeSheetData[1])];
                          $product->Guidlines=$activeSheetData[$i][array_search("Guidelines",$activeSheetData[1])];
                          $product->Security=$activeSheetData[$i][array_search("Security",$activeSheetData[1])];
                          $product->Others=$activeSheetData[$i][array_search("Others",$activeSheetData[1])];
                          $product->Frontimg=$fimg;
                          $product->Backimg=$bimg;
                         

                          if(isset($activeSheetData[$i][array_search("Company Name",$activeSheetData[1])])){
                              $comp_id= Company::where('Name',$activeSheetData[$i][array_search("Company Name",$activeSheetData[1])])->pluck('id')->first();
                          }    


                        $product->Company_id= $comp_id;
                       
                    try{
                                                  
                             $product->save(); // save product
                             $price_regex = preg_match('/^[0-9]+$/u' , $activeSheetData[$i][array_search("Price",$activeSheetData[1])]);
                             if($price_regex==1 && isset($activeSheetData[$i][array_search("Price",$activeSheetData[1])]) )
                             {
                               $date=new \DateTime();
                                $price=Price::where('Government','=',$activeSheetData[$i][array_search("Office Gov",$activeSheetData[1])])
                                ->where('District','=',$activeSheetData[$i][array_search("Office City",$activeSheetData[1])])
                                ->where('Product_id','=',$updated_product->id)
                                ->where('Price','=',$activeSheetData[$i][array_search("Price",$activeSheetData[1])])
                                ->where('Date','=',$date)->first();
                                if($price==[])
                                {
                                 $price=new Price();
                                 $price->Government=$activeSheetData[$i][array_search("Office Gov",$activeSheetData[1])];
                                 $price->District=$activeSheetData[$i][array_search("Office City",$activeSheetData[1])];
                                 $price->Date=new \DateTime();
                                 $price->Price=$activeSheetData[$i][array_search("Price",$activeSheetData[1])];
                                 $price->Product_id=$product->id;
                                 $price->save();
                                }
                             }
                            $comp= $product->getcompanyproduct;
                            $off= array();
                            foreach ($comp->companyget as $office) {
                                $off= array();
                                array_push($off, $product->id , $office->id);
                            }

                            $office->getsellerofficeproduct()->attach($off[0]);

                                                    $GLOBALS['product_valid']=1;
                        
                    }   
                    catch (Exception $e) { 
                     //update product

                        $repeted = $comp_id."-".$activeSheetData[$i][array_search("Cement Name",$activeSheetData[1])]; 
                        $exist_string= "Duplicate entry '".$repeted."' for key 'products_company_id_cement_name_unique'";
                     
                        if (isset($e->errorInfo)) {
                       if ($exist_string === $e->errorInfo[2])
                        {  
                           
                            $product_id= Product::where('Cement_Name',$activeSheetData[$i][array_search("Cement Name",$activeSheetData[1])])
                                        ->where('Company_id',$comp_id)
                                        ->pluck('id')->first();
                                      
                            $updated_product = Product::find($product_id);  
                            $updated_product->Cement_Name=$activeSheetData[$i][array_search("Cement Name",$activeSheetData[1])];
                            $updated_product->Enname=$activeSheetData[$i][array_search("Cement Name",$activeSheetData[1])];
                            $updated_product->Weight=$activeSheetData[$i][array_search("Weight",$activeSheetData[1])];
                            $updated_product->Rank=$activeSheetData[$i][array_search("Rank",$activeSheetData[1])];
                            $updated_product->Specifications=$activeSheetData[$i][array_search("Specifications",$activeSheetData[1])];
                            $updated_product->Producation_date=$activeSheetData[$i][array_search("Production Date",$activeSheetData[1])];
                            $updated_product->Packing_date=$activeSheetData[$i][array_search("Packing Date",$activeSheetData[1])];
                            $updated_product->Guidlines=$activeSheetData[$i][array_search("Guidelines",$activeSheetData[1])];
                            $updated_product->Security=$activeSheetData[$i][array_search("Security",$activeSheetData[1])];
                            $updated_product->Others=$activeSheetData[$i][array_search("Others",$activeSheetData[1])];
                            $updated_product->Frontimg=$fimg;
                            $updated_product->Backimg=$bimg;   

                            $updated_product->save();
                             //dd($updated_product);
                              $price_regex = preg_match('/^[0-9]+$/u' , $activeSheetData[$i][array_search("Price",$activeSheetData[1])]);
                             if($price_regex==1 && isset($activeSheetData[$i][array_search("Price",$activeSheetData[1])]) )
                             {
                                $date=new \DateTime();
                                $price=Price::where('Government','=',$activeSheetData[$i][array_search("Office Gov",$activeSheetData[1])])
                                ->where('District','=',$activeSheetData[$i][array_search("Office City",$activeSheetData[1])])
                                ->where('Product_id','=',$updated_product->id)
                                ->where('Price','=',$activeSheetData[$i][array_search("Price",$activeSheetData[1])])->first();
                                if($price==[])
                                {
                                  
                                   $price=new Price();
                                   $price->Government=$activeSheetData[$i][array_search("Office Gov",$activeSheetData[1])];
                                   $price->District=$activeSheetData[$i][array_search("Office City",$activeSheetData[1])];
                                   $price->Date=$date;
                                   $price->Price=$activeSheetData[$i][array_search("Price",$activeSheetData[1])];
                                   $price->Product_id=$updated_product->id;
                                   $price->save();
                               }
                               else
                               {
                                 $price->Date=$date;
                                  $price->save();
                               }
                             
                             }
               

                        }
                      }
                    }  // end catch

                                        }

                                        else {
                                            array_push($GLOBALS['product'],$activeSheetData[$i][array_search("Cement Name",$activeSheetData[1])]); 
                                            
                                        }                                        
                                    }
                                    else {
                                        array_push($GLOBALS['product'],$activeSheetData[$i][array_search("Cement Name",$activeSheetData[1])]);
                                        
                                    }                                
                                }
                                else {
                                    array_push($GLOBALS['product'],$activeSheetData[$i][array_search("Cement Name",$activeSheetData[1])]); 
                               
                                }                                 
                        }
                        else {
                            array_push($GLOBALS['product'],$activeSheetData[$i][array_search("Cement Name",$activeSheetData[1])]); 
                     
                        }                 
                    }
                    else {
                        array_push($GLOBALS['product'],$activeSheetData[$i][array_search("Cement Name",$activeSheetData[1])]); 
                   
                    } 
                }
                else {
                    array_push($GLOBALS['product'],$activeSheetData[$i][array_search("Cement Name",$activeSheetData[1])]);
                    
                }           
            }
            else {
                array_push($GLOBALS['product'],$activeSheetData[$i][array_search("Cement Name",$activeSheetData[1])]); 
                
            }    
        }
        else {
            array_push($GLOBALS['product'],$activeSheetData[$i][array_search("Cement Name",$activeSheetData[1])]); 

        }
    }
    else {
         array_push($GLOBALS['Emptyproduct'],$i); 
    }
}
    if ( !empty ($GLOBALS['Emptyproduct'] )) {
            $GLOBALS['Emptyproduct'] = array_unique($GLOBALS['Emptyproduct']);
            $EmptyProductErr= $EmptyProductErr.implode(" \n ",$GLOBALS['Emptyproduct']);
            $EmptyProductErr = nl2br($EmptyProductErr);  
            $cookie_name = 'EmptyProductErr';
            $cookie_value = $EmptyProductErr;
            setcookie($cookie_name, $cookie_value, time() + (60), "/");
        }
        if ( !empty ($GLOBALS['product'] )) {
            $GLOBALS['product'] = array_unique($GLOBALS['product']);
            $ProductErr = $ProductErr.implode(" \n ",$GLOBALS['product']);
            $ProductErr = nl2br($ProductErr);  
            $cookie_name = 'ProductErr';
            $cookie_value = $ProductErr;
            setcookie($cookie_name, $cookie_value, time() + (60), "/");
        }


}
   public function ValidateOffice($activeSheetData, $i,$j){
        // dd($data);
        $GLOBALS['office_valid']=0;
        $GLOBALS['office_id']=0;
        $GLOBALS['seller_office']=0;

        if(!isset($GLOBALS['office'])) { $GLOBALS['office']= array(); } 
        if(!isset($OfficeErr)) { 
            $OfficeErr = 'البيانات غير صحيحة لمكتب البيع صاحب رقم التليفون: '; 
        }      

        if(!isset($GLOBALS['Emptyoffice'])) { 
            $GLOBALS['Emptyoffice']= array(); 
        } 
        if(!isset($EmptyOfficeErr)) { 
            $EmptyOfficeErr = 'رقم تليفون مكتب البيع غير موجود للصف رقم: '; 
        }   

        $address_regex = preg_match('/\p{Arabic}/u' , $activeSheetData[$i][array_search("Office Street",$activeSheetData[1])]);
        $Government_regex = preg_match('/\p{Arabic}/u' , $activeSheetData[$i][array_search("Office Gov",$activeSheetData[1])]);
        $District_regex = preg_match('/\p{Arabic}/u' , $activeSheetData[$i][array_search("Office City",$activeSheetData[1])]);
        $phone_regex = preg_match('/^[0-9]{5,11}$/' , $activeSheetData[$i][array_search("Phone",$activeSheetData[1])]);

        if ($activeSheetData[$i][array_search("Phone",$activeSheetData[1])] == " ") {
            $activeSheetData[$i][array_search("Phone",$activeSheetData[1])]= null;
        }
    if(isset($activeSheetData[$i][array_search("Company Name",$activeSheetData[1])])){
    if (isset($activeSheetData[$i][array_search("Phone",$activeSheetData[1])])) {        
        if ($phone_regex == 1)
        {
            if ($address_regex == 1  || !isset($activeSheetData[$i][array_search("Office Street",$activeSheetData[1])]))
            {
              if ($Government_regex == 1  || !isset($activeSheetData[$i][array_search("Office Gov",$activeSheetData[1])]))
              {

                if ($District_regex == 1 || !isset($activeSheetData[$i][array_search("Office City",$activeSheetData[1])])  || !isset($activeSheetData[$i][array_search("Company Gov",$activeSheetData[1])]))
            {
            $sellerOffice=new FrontOffice();
            $sellerOffice->Location=$activeSheetData[$i][array_search("Office Street",$activeSheetData[1])];
            $sellerOffice->Telephone=$activeSheetData[$i][array_search("Phone",$activeSheetData[1])];
            $sellerOffice->Government=$activeSheetData[$i][array_search("Office Gov",$activeSheetData[1])];
            $sellerOffice->District=$activeSheetData[$i][array_search("Office City",$activeSheetData[1])];
            $sellerOffice->Address=$activeSheetData[$i][array_search("Office Street",$activeSheetData[1])];
            
                    if(isset($activeSheetData[$i][array_search("Company Name",$activeSheetData[1])])){
                        $comp_id= Company::where('Name',$activeSheetData[$i][array_search("Company Name",$activeSheetData[1])])
                                            ->pluck('id')->first();
                    }       
                    $sellerOffice->company_id= $comp_id;
                    try{
                        $sellerOffice->save();
                        $GLOBALS['office_valid']=1;
                    }
                    catch (Exception $e){  
                        
                        $exist_string= "Duplicate entry '".$activeSheetData[$i][array_search("Phone",$activeSheetData[1])]."' for key 'front_offices_telephone_unique'";
                        if (isset($e->errorInfo)) {
                        if ($exist_string == $e->errorInfo[2]) {  // company exist 
                            $office_id= FrontOffice::where('Telephone',$activeSheetData[$i][array_search("Phone",$activeSheetData[1])])
                                        ->pluck('id')->first();
                            $updated_office = FrontOffice::find($office_id);
                        $updated_office->Location=$activeSheetData[$i][array_search("Office Street",$activeSheetData[1])];
                        $updated_office->Telephone=$activeSheetData[$i][array_search("Phone",$activeSheetData[1])];
                        $updated_office->Government=$activeSheetData[$i][array_search("Office Gov",$activeSheetData[1])];
                        $updated_office->District=$activeSheetData[$i][array_search("Office City",$activeSheetData[1])];
                        $updated_office->Address=$activeSheetData[$i][array_search("Office Street",$activeSheetData[1])];
                            $updated_office->save();

                            $GLOBALS['office_valid']=1;
                        }
                      }
                    }
                }
                else 
                    array_push($GLOBALS['office'],$activeSheetData[$i][array_search("Phone",$activeSheetData[1])]); 
                   
            }
                else 
                    array_push($GLOBALS['office'],$activeSheetData[$i][array_search("Phone",$activeSheetData[1])]); 
            }
                  else 
                    array_push($GLOBALS['office'],$activeSheetData[$i][array_search("Phone",$activeSheetData[1])]); 
                   
            }
                else 
                    array_push($GLOBALS['office'],$activeSheetData[$i][array_search("Phone",$activeSheetData[1])]); 
            }

     // if isset phone
    else {
        array_push($GLOBALS['Emptyoffice'],$j); 
    }
}
    if ( !empty ($GLOBALS['Emptyoffice'] )) {
            $GLOBALS['Emptyoffice'] = array_unique($GLOBALS['Emptyoffice']);
            $EmptyOfficeErr= $EmptyOfficeErr.implode(" \n ",$GLOBALS['Emptyoffice']);
            $EmptyOfficeErr = nl2br($EmptyOfficeErr);  
            $cookie_name = 'EmptyOfficeErr';
            $cookie_value = $EmptyOfficeErr;
            setcookie($cookie_name, $cookie_value, time() + (60), "/");
        }
        if ( !empty ($GLOBALS['office'] )) {
            $GLOBALS['office'] = array_unique($GLOBALS['office']);
            $OfficeErr = $OfficeErr.implode(" \n ",$GLOBALS['office']);
            $OfficeErr = nl2br($OfficeErr);  
            $cookie_name = 'OfficeErr';
            $cookie_value = $OfficeErr;
            setcookie($cookie_name, $cookie_value, time() + (60), "/");
        }

}
    public function ValidateCompany($activeSheetData, $i,$j){

        $GLOBALS['company_valid']=0;        
        if(!isset($GLOBALS['company'])) { $GLOBALS['company']= array(); } 
        if(!isset($GLOBALS['Emptycompany'])) { 
            $GLOBALS['Emptycompany']= array(); 
        } 
        if(!isset($CompanyErr)) { $CompanyErr = 'البيانات غير صحيحة للشركة: '; }      
        if(!isset($EmptyCompanyErr)) { 
            $EmptyCompanyErr = 'اسم الشركة غير موجود للصف رقم: '; 
        }   

        $name_regex = preg_match('/^[\pL\s]+$/u' , $activeSheetData[$i][array_search("Company Name",$activeSheetData[1])]);
        $gov_regex = preg_match('/^[\pL\s]+$/u' , $activeSheetData[$i][array_search("Company Gov",$activeSheetData[1])]);
        $city_regex = preg_match('/^[\pL\s]+$/u' , $activeSheetData[$i][array_search("Company City",$activeSheetData[1])]);
        $address_regex = preg_match('/\p{Arabic}/u' , $activeSheetData[$i][array_search("Company Address",$activeSheetData[1])]);
   
        if ($activeSheetData[$i][array_search("Company Name",$activeSheetData[1])] == " ") {
            $activeSheetData[$i][array_search("Company Name",$activeSheetData[1])]= null;
        }
         
    if ($activeSheetData[$i][array_search("Company Name",$activeSheetData[1])] != null) { // company has name
        if ($name_regex == 1) { // true name 
            if ($gov_regex == 1 || !isset($activeSheetData[$i][array_search("Company Gov",$activeSheetData[1])])) { // true government 
                if ($city_regex == 1 || !isset($activeSheetData[$i][array_search("Company City",$activeSheetData[1])])) { // true city 
                   if ($address_regex == 1 || !isset($activeSheetData[$i][array_search("Company Address",$activeSheetData[1])])) { 
                      $company =new Company();
                      $company->Name=$activeSheetData[$i][array_search("Company Name",$activeSheetData[1])];
                      $company->EnName=$activeSheetData[$i][array_search("Company Name",$activeSheetData[1])];
                      $company->Government=$activeSheetData[$i][array_search("Company Gov",$activeSheetData[1])];
                      $company->District=$activeSheetData[$i][array_search("Company City",$activeSheetData[1])];
                      $company->Address=$activeSheetData[$i][array_search("Company Address",$activeSheetData[1])];
                      $company->HeadOfficeGov=$activeSheetData[$i][array_search("Company Gov",$activeSheetData[1])];
                      $company->HeadOfficeAddre=$activeSheetData[$i][array_search("Company Address",$activeSheetData[1])];
                    try{ 
                     // true company
                        $company->save(); // true company
                        $GLOBALS['company_valid']=1;
                    }  
                    catch (Exception $e) { 
                     
                        $exist_string= "Duplicate entry '".$activeSheetData[$i][array_search("Company Name",$activeSheetData[1])]."' for key 'companies_name_unique'";
                       if (isset($e->errorInfo)) {

                        if ($exist_string == $e->errorInfo[2]) {  
                        // company exist 
                            $Comp_Id= Company::where('Name',$activeSheetData[$i][array_search("Company Name",$activeSheetData[1])])
                                        ->pluck('id')->first();
  
                                $updated_comp = Company::find($Comp_Id);
                                $updated_comp->Name=$activeSheetData[$i][array_search("Company Name",$activeSheetData[1])];
                                $updated_comp->EnName=$activeSheetData[$i][array_search("Company Name",$activeSheetData[1])];
                                $updated_comp->Government=$activeSheetData[$i][array_search("Company Gov",$activeSheetData[1])];
                                $updated_comp->District=$activeSheetData[$i][array_search("Company City",$activeSheetData[1])];
                                $updated_comp->Address=$activeSheetData[$i][array_search("Company Address",$activeSheetData[1])];
                                $updated_comp->HeadOfficeGov=$activeSheetData[$i][array_search("Company Gov",$activeSheetData[1])];
                                $updated_comp->HeadOfficeAddre=$activeSheetData[$i][array_search("Company Address",$activeSheetData[1])];
                            
                            $updated_comp->save();
                            
                            $GLOBALS['company_valid']=1;
                        }
                      }
                    }
                }
                    else {
                        array_push($GLOBALS['company'],$activeSheetData[$i][array_search("Company Name",$activeSheetData[1])]); 
                    }
                }
                else {
                    array_push($GLOBALS['company'],$activeSheetData[$i][array_search("Company Name",$activeSheetData[1])]); 
                }
            }
            else {
                    array_push($GLOBALS['company'],$activeSheetData[$i][array_search("Company Name",$activeSheetData[1])]); 
                }
        }
        else {
                array_push($GLOBALS['company'],$activeSheetData[$i][array_search("Company Name",$activeSheetData[1])]); 
            }   
    }
    else {
            array_push($GLOBALS['Emptycompany'],$j); 
        } 

    if ( !empty ($GLOBALS['Emptycompany'] )) {
            $GLOBALS['Emptycompany'] = array_unique($GLOBALS['Emptycompany']);
            $EmptyCompanyErr= $EmptyCompanyErr.implode(" \n ",$GLOBALS['Emptycompany']);
            $EmptyCompanyErr = nl2br($EmptyCompanyErr);  
            $cookie_name = 'EmptyCompanyErr';
            $cookie_value = $EmptyCompanyErr;
            setcookie($cookie_name, $cookie_value, time() + (60), "/");
        }
        if ( !empty ($GLOBALS['company'] )) {
            $GLOBALS['company'] = array_unique($GLOBALS['company']);
            $CompanyErr = $CompanyErr.implode(" \n ",$GLOBALS['company']);
            $CompanyErr = nl2br($CompanyErr);  
            $cookie_name = 'CompanyErr';
            $cookie_value = $CompanyErr;
            setcookie($cookie_name, $cookie_value, time() + (60), "/");
        }

}
}
