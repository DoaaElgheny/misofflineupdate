<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use App\Survey;
use App\UserCustome;
use App\UserOption;
use App\Question;
use App\MatrixData;
use DB;

class SurveyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function LoginSurvey($id)
    {

    $servey=Survey::where('id',$id)->first();
     return view('SurveyShow/LoginSurvey',compact('servey'));   
    }
    public function index()
    {
      return view('Survey/createsurvey');
    }
    public function EditSurvey($id)
    {
        $Survey=Survey::where('id',$id)->select('*')->first();
       return view('Survey/EditSurvey',compact('Survey')); 
    }
    public function ShowDashboard()
    {
        return view('Survey/DashBoard');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function ShowAllServey()
    {
        try
        {
            $AllServey=Survey::all();
            return view('Survey/AllServey',compact('AllServey'));
        }
        catch(Exception $e) 
        {
            return redirect('/login');
        }
        
    }
     public function updateQuestion()
    {
         $Question = Input::get('pk');  
        $column_name = Input::get('name');
        $column_value = Input::get('value');
    
        $QuestionData = Question::whereId($Question)->first();

        $QuestionData-> $column_name=$column_value;

        if($QuestionData->save())

        return \Response::json(array('status'=>1));
    else 
        return \Response::json(array('status'=>0));
 
    }
    public function deleteQuestion($id)
    {
        try
        {
           $QuestionData=Question::find($id);
         $QuestionData->delete();
        return redirect('/ShowAllServey');
        }
        catch(Exception $e) 
        {
            return redirect('/login');
        } 
    }
    public function ShowAllQuestion($id)
    {
       
        $AllQuestion=Question::where('Survey_Id',$id)->get();
       
        return view('Survey/AllQuestion',compact('AllQuestion')); 
    }
     public function ShowAllAnswers($id)
    {
       
        $ShowAllAnswers= UserCustome::where('Survey_Id',$id)->get();
       
        return view('Survey/ShowAllAnswers',compact('ShowAllAnswers')); 
    }


    public function DeleteSurvey($id)
    {
        try
        {
             $SurveyData=Survey::find($id);
             $SurveyData->delete();
        return redirect('/ShowAllServey');
        }
        catch(Exception $e) 
        {
            return redirect('/login');
        }
    }
    public function ShowDropSurvey($id)
    {

        $Question=Question::Where('Survey_Id',$id)
        ->get();
        $options = array(); 
      foreach ($Question as $Value) {
      $options += array($Value->id => $Value->Name);
        }
        
        return \Response::json($options);
    }



  

    public function analysissurvey()
    {
       
       $questionid=Input::get('id');
      
          $usernumber=UserOption::where('Question_Id',$questionid)
       ->select(DB::raw('count(User_Id) as count'))->groupBy('Question_Id','Row')->first();
    
       $useroption=UserOption::where('Question_Id',$questionid)
       ->select('Question_Id','Row','data',DB::raw('count(data) as count'))->groupBy('Question_Id','Row','data')->get();

       $rows=MatrixData::where('Question_Id',$questionid)->where('Type','rows')->get();       
       $colmuns=MatrixData::where('Question_Id',$questionid)->where('Type','columns')->get();
       $data=[];
       $average=[];
       $total_avg=0;
$TotalData=[];

       foreach ($rows as  $value)   {
              $top=0;
               $middle=0;
               $bottom=0;
               $na=0;
               $SumTotal=0;
               $N=0;
               // $Summiddle=0;
               // $Sumbottom=0;
            foreach ($colmuns as $key ){
                foreach ($useroption as $user) {
                    // dd($value,$user,$key);
                    if($value->Value==$user->Row && $key->Value==$user->data)
                    {
                          if($user->data > 0 && $user->data < 4)
                          {
                           
                               $SumTotal+=($user->count*$user->data);
                                 $bottom+=$user->count;
                          }
                          elseif($user->data > 3 && $user->data < 7)
                          {
                            $SumTotal+=($user->count*$user->data);
                                $middle+=$user->count;
                          }
                           elseif($user->data > 6 && $user->data < 11)
                           {
                            $SumTotal+=($user->count*$user->data);
                              $top+=$user->count;
                           }
                           else {
                              $na+=$user->count;
                           }
                    }
                }
            }

            // dd($top,$middle,$bottom,$na);
           $N=$top+$middle+$bottom;
           if($SumTotal==0 &&$N==0)
                   $N=1;
           $total_avg=($SumTotal/$N);

          $total_avg=(float)number_format((float)$total_avg, 2, '.', '');
           
            array_push($data, array(
                "Name"=>$value->Value,
                "Top"=>$top,
                "Middle"=>$middle,
                "Bottom"=>$bottom,
                "Na"=>$na

            ));

             array_push($average, array(
                 "y"=>[0,$total_avg]
               

            ));
             $value->average=$total_avg;
         $value->save();
        }
      // dd($data);
$ss=[];
$top=[];
$middle=[];
$bottom=[];
$na=[];

    foreach ($data as $dd) {
         $Nam='';
       foreach ($dd as $key => $value) {
        if($key == "Name")
        {
             $Nam= $value;
        }
        if($key == "Top")
        {
          array_push($top,array('y' => $value,'label' => $Nam));
        }
        elseif ($key == "Middle")
        {
            array_push($middle,array('y' => $value,'label' => $Nam));
        }
        elseif ($key == "Bottom")
        {
            array_push($bottom,array('y' => $value,'label' => $Nam));
        }
        elseif ($key == "Na")
        {
            array_push($na,array('y' => $value,'label' => $Nam));
        }
       }
       

}

array_push($ss,array('Top' =>$top ,'Middle' =>$middle,'Bottom' =>$bottom ,'Na' =>$na  ));
array_push($TotalData, array("normal"=>$ss,
"avg"=>$average,"data"=>$data));

     return \Response::json($TotalData);  
    }
    public function showanalysissurvey()
    {
        $servey=Survey::Lists('Name','id');
        $Question=Question::Lists('Name','id'); 
      return view('Survey/analysis',compact('servey','Question'));
    }
    public function AddUsertoServey()
    {
        // dd(input::all());
        try
        {
          $n=count(Input::get('Email'));
            $id=Input::get('id');
             // $id=1;
          for ($i=0; $i <$n-1 ; $i++) { 
            $AddUsertoServey = New UserCustome;
            $AddUsertoServey->Email=Input::get('Email')[$i];
            $AddUsertoServey->Telephone=Input::get('Tel')[$i];
            $AddUsertoServey->servey_id=$id;
             $AddUsertoServey->save(); 
        //  $link='http://217.139.86.100/MIS/LoginSurvey/'.$id;

        //     $data=array('Email'=>$AddUsertoServey->Email,'Link'=>$link);
        //    \Mail::send('emails.ActiveSucess', $data, function($message) use ($data) {
        //     // $message->to($data['Email']);
        //     $message->to($data['Email']);
        //     $message->subject('CEMEX Survey');
        // }); 
          }
           $link='http://10.43.144.19/offlineneew/public/LoginSurvey/'.$id;
          return view('Survey/CompleteServay',compact('id','link'));
        }
        catch(Exception $e) 
        {
        return redirect('/login');
        }
    }
    public function AssginServey($id)
    {
        try
        {
            $id=$id;
          return view('Survey/AssignServey',compact('id'));
        }
        catch(Exception $e) 
        {
        return redirect('/login');
        }

    }
    public function CheckLogin(Request $request)
    {
        $id=Input::get('survey_id');
        $Question=Question::where('Survey_Id',$id)->first();
        $user=UserCustome::where('Email',Input::get('Email'))
         ->Where('Servey_id',$id)
        ->first();
           if( $user!="" )
           {
            $Query=DB::table('useroption')->where('User_Id',$user->id)->where('Question_Id',$Question->id)->first();
            if($Query == []||$Query == "" )
            {
               $user_id=$user->id;

               return view('/test',compact('id','user_id'));
             }
             else
             {
              return redirect('/LoginSurvey/'.$id);
             }
           }
           else
           {
            
            //Add New User
            $AddUsertoServey = New UserCustome;
            $AddUsertoServey->Email=Input::get('Email');
            $AddUsertoServey->Telephone=Input::get('Tel');
            $AddUsertoServey->Servey_id= $id;
            $AddUsertoServey->save();
            $user_id=$AddUsertoServey->id;
            return view('/test',compact('id','user_id')); 

           }


          
       
    }
    public function store(Request $request)
    {
        try
        {
            $Surveydata = New Survey;
            $Surveydata->Name=Input::get('Name');
            $Surveydata->Description=Input::get('Desc'); 
            $Surveydata->StartDate=Input::get('Start_Date'); 
            $Surveydata->EndDate=Input::get('End_Date'); 
            if(Input::file('pic') !=null)
            {
                    $file_img = Input::file('pic');
                    $imageName = $file_img->getClientOriginalName();
                    $path=public_path("/assets/dist/img/").$imageName;
                    if (!file_exists($path)) {
                    $file_img->move(public_path("/assets/dist/img"),$imageName);
                    $Surveydata->Logo="/offlineneew/public/assets/survayLayout/dist/img/".$imageName;
                    }
                    else
                    {
                    $random_string = md5(microtime());
                    $file_img->move(public_path("/assets/dist/img"),$random_string.".jpg");
                    $Surveydata->Logo="/offlineneew/public/assets/survayLayout/dist/img/".$random_string.".jpg";
                    }
            }

             
            $Surveydata->save();         
            $id=$Surveydata->id;
            // dd($id);
        // return redirect('/Attribute',compact('id'));
            return view('Survey/Matrixsurvy',compact('id'));
        }
        catch(Exception $e) 
        {
        return redirect('/login');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try
        {
            $Surveydata = Survey::find($id);
            $Surveydata->Name=Input::get('Name');
            $Surveydata->Description=Input::get('Desc'); 
            $Surveydata->StartDate=Input::get('Start_Date'); 
            $Surveydata->EndDate=Input::get('End_Date'); 
            if(Input::file('pic') !=null)
            {
                    $file_img = Input::file('pic');
                    $imageName = $file_img->getClientOriginalName();
                    $path=public_path("/assets/dist/img/").$imageName;
                    if (!file_exists($path)) {
                    $file_img->move(public_path("/assets/dist/img"),$imageName);
                    $Surveydata->Logo="/assets/dist/img/".$imageName;
                    }
                    else
                    {
                    $random_string = md5(microtime());
                    $file_img->move(public_path("/assets/dist/img"),$random_string.".jpg");
                    $Surveydata->Logo="/assets/dist/img/".$random_string.".jpg";
                    }
            }

             
            $Surveydata->save();         
            $id=$Surveydata->id;
            // dd($id);
         return redirect('/ShowAllServey');
        }
        catch(Exception $e) 
        {
        return redirect('/login');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
  
public function surveyuser($id)
    {

   $questionID=[];
   $AllUserOptin=[];
   $AllUserAnswer=[];
   $surver=Survey::find($id);

   $question=DB::table('questions')->where('Survey_Id',$id)->get();
   
   foreach ($question as  $value) {

       array_push($questionID, $value->id);
   }
   $UserOption=UserOption::whereIn('Question_Id',$questionID)->groupBy('User_Id')->get();
   $users=UserCustome::where('Servey_id',$id)->get();


 
 foreach ($users as $value) {
 $true=0;

     foreach ($UserOption as  $Answer)
    {

    
       if(($value->id)==($Answer->User_Id))
       {
      

          array_push($AllUserAnswer,array('Email'=>$value->Email,'Tele'=>$value->Telephone,'id'=>$value->id,'Aswer'=>1));
           $true=1;

       }
     

      
     }

       if($true==0)
       {
       array_push($AllUserAnswer,array('Email'=>$value->Email,'id'=>$value->id,'Tele'=>$value->Telephone,'Aswer'=>0));
       }
    

   }

   return view('Survey/AllUserForSurvey',compact('AllUserAnswer'));
    }
    public function destroy($id)
    {
        //
    }
    public function showquarter()
{
  $Survey=Survey::lists('Name','id');
  return view('ReportSurvey.Reportquarter',compact('Survey'));
}
    public function AnaylsisQuadrantsCharts(Request $request)
    {

        $id=$request->get('Servey_ID');

$Query=MatrixData::join('questions','questions.id','=','matrixdata.Question_Id')
->join('survies','survies.id','=','questions.Survey_Id')->where('matrixdata.Type','rows')
->where('survies.id',$id)
->select('questions.id',DB::raw ('AVG(matrixdata.average) as average'))
->groupBy('questions.id')->get();
$dataset=[];
$MaxY = Question::where('Survey_Id',$id)->max('BudgetCost');
$MinY = Question::where('Survey_Id',$id)->min('BudgetCost');
$MaxX =  Question::where('Survey_Id',$id)->max('index');
$MinX =  Question::where('Survey_Id',$id)->min('index');
$averageY=($MaxY+$MinY)/2;
$averageX=($MaxX+$MinX)/2;
$Xaxis=[];
$Xaxis2=[];


foreach ($Query as  $value) {

 $Question=Question::find($value->id);
 $Question->index=$value->average;
 $Question->save();

$Actvalue=$Question->index*$Question->BudgetCost;

 $Actvalue=(float)number_format((float)$Actvalue, 2, '.', ''); 

 array_push($dataset,array(
  'x' =>(float)number_format((float)$Question->index, 2, '.', '')  ,
  'y' => (float)number_format((float)$Question->BudgetCost, 2, '.', '') ,
  'z' => $Actvalue*100,
'name' => 'ACT'));
  array_push($Xaxis,array(
  'label' =>(float)number_format((float)$Question->index, 2, '.', '') ,
  'x' =>(float)number_format((float)$Question->index, 2, '.', ''),'showverticalline' =>1,
   "linedashed" =>1));


}

$AllData=[];
array_push($AllData, array("dataset"=>$dataset,'Xaxis'=>$Xaxis,
"MaxY"=>$MaxY,"MinY"=>$MinY,"MaxX"=>$MaxX,"MinX"=>$MinX,"averageY"=>$averageY,"averageX"=>$averageX ));


return \Response::json($AllData);

    }
}
