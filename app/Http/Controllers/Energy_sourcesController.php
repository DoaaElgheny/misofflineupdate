<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Auth;

use App\User;
use App\Gps;
use DB;
use App\Http\Controllers\Controller;

use Excel;
use File;
use Illuminate\Support\Facades\Response;
use Validator;
use Illuminate\Support\Facades\Redirect;
use Input;
use  App\EnergySource;

class Energy_sourcesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
   {
       $this->middleware('auth');
   }
    public function index()
    { 
      try {

 if (Auth::check())
  {


    if(Auth::user()->can('Show Energy Sources'))
      {
 
        //Show data 
       $energy_sources = EnergySource::all(); 

       return view('Energy_sources.index',compact('energy_sources'));
    }
    else 
      {
    return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
          try {

 if (Auth::check())
  {


    if(Auth::user()->can('Store Energy sources'))
      {

 $energysourcesName=Input::get('name');

        for($i=0; $i<count($energysourcesName)-1;$i++)
        {
        $Energy_sources = New EnergySource;
        $Energy_sources->Name=$energysourcesName[$i];
        $Energy_sources->save();       
        }
          return redirect('/Energy_sources');
  }
    else 
      {
    return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
    }
public function updateEnergysources()
    {
          try {

 if (Auth::check())
  {


    if(Auth::user()->can('update Energy sources'))
      {

        $energysourcesid = Input::get('pk');   
        $column_name = Input::get('name');
       
        $column_value = Input::get('value');
    
        $energysourData = EnergySource::whereId($energysourcesid)->first();
        $energysourData-> $column_name=$column_value;

        if($energysourData->save())

        return \Response::json(array('status'=>1));
    else 
        return \Response::json(array('status'=>0));
        }
    else 
      {
    
return \Response::json(array('status'=>'You do not have permission.'));
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }

    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
public function checkEnergySource()
    {
         $energySourcename=Input::get('name');
      $energySource =EnergySource::where('energy_sources.name','=',$energySourcename[0])
     ->select('*')->first();
     if($energySource==null)     
        $isAvailable = true;
        else
         $isAvailable = false;

    return \Response::json(array('valid' =>$isAvailable,));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
          try {

 if (Auth::check())
  {


    if(Auth::user()->can('Delete Energy sources'))
      {
      $Energysources=EnergySource::find($id);
         
                    $Energysources->delete();
                    return redirect('/Energy_sources');
                       }
    else 
      {
    return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
    }
}
