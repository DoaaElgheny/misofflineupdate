<?php

namespace App\Http\Controllers;

use Auth;
use App\Http\Requests;
use App\User;
use App\Gps;
use DB;
use App\Http\Controllers\Controller;
use Request;
use Excel;
use File;
use Illuminate\Support\Facades\Response;
use Validator;
use Illuminate\Support\Facades\Redirect;
use Input;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use App\Gallery;
use App\MarketingActivity;
class MarktingactitvityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
   {
       $this->middleware('auth');
   }
  public function checkmarketactivityname(){
             try {

 if (Auth::check())
  {

    $marktingactitvityname=Input::get('name');
    $marktingactitvity = MarketingActivity::where('Name','=',$marktingactitvityname)->first();
     if($marktingactitvity==[])     
        $isAvailable = true;
        else
         $isAvailable = false;

    return \Response::json(array('valid' =>$isAvailable,));
       }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
 }

    public function index()
    {
         try {

 if (Auth::check())
  {


    if(Auth::user()->can('show-Marketing_Activity'))
      {

        $marktingactitvities = MarketingActivity::all(); 
        $marketingactitvities = MarketingActivity::all()->lists('Name','id');         
         return view('marktingactitvities.index',compact('marktingactitvities','marketingactitvities'));

      }
    else 
      {
   return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
    }


    public function storephoto(Request $request)
    {
         try {

 if (Auth::check())
  {


    if(Auth::user()->can('create-galleries'))
      {

        $marktingactitvitiesid=Input::get('activity');
        $file_frontimg = Request::file('img');
        for($i=0;$i<count($file_frontimg)-1;$i++) {
            $imageName = $file_frontimg[$i]->getClientOriginalName();


                $path=public_path("/assets/dist/img/").$imageName;
              // notation octale : valeur du mode correcte
                  

                if (!file_exists($path)) {
                  $file_frontimg[$i]->move(public_path("/assets/dist/img"),$imageName);
                  $gallery=new Gallery();

                  $gallery->img="/assets/dist/img/".$imageName;
                  $gallery->marketingactivity_id=$marktingactitvitiesid;
                
                  $gallery->save();
                  
                }
                else
                {
                   $random_string = md5(microtime());
                   $file_frontimg[$i]->move(public_path("/assets/dist/img"),$random_string.".jpg");
                   $gallery=new Gallery();
                  $gallery->img="/assets/dist/img/".$random_string.".jpg";
                  $gallery->marketingactivity_id=$marktingactitvitiesid;
                  $gallery->save();
                
                }

        }
        
   return redirect('/marktingactitvities');

      }
    else 
      {
    return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }   
    }
 public function updatemarktingactitvityname()
    {
         try {

 if (Auth::check())
  {


    if(Auth::user()->can('edit-Marketing_Activity'))
      {

         $marktingactitvityid = Input::get('pk');  
        $column_name = Input::get('name');
        $column_value = Input::get('value');
    
        $Marktingactitvity = MarketingActivity::whereId($marktingactitvityid)->first();
        $Marktingactitvity-> $column_name=$column_value;

        if($Marktingactitvity->save())

        return \Response::json(array('status'=>1));
    else 
        return \Response::json(array('status'=>0));

      }
    else 
      {
    return \Response::json(array('status'=>'You do not have permission.'));
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         try {

 if (Auth::check())
  {


    if(Auth::user()->can('create-Marketing_Activity'))
      {

       $Marktingactitvityname=Input::get('name');
            $Marktingactitvity = New MarketingActivity;
            $Marktingactitvity->Name=$Marktingactitvityname; 
            $Marktingactitvity->save();         
        
        return redirect('/marktingactitvities');

      }
    else 
      {
   return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
         
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         try {

 if (Auth::check())
  {


    if(Auth::user()->can('delete-Marketing_Activity'))
      {

      $category=MarketingActivity::find($id);
        $category->delete();
        return redirect('/marktingactitvities');

      }
    else 
      {
        return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
      
    }
}
