<?php

namespace App\Http\Controllers;

use Auth;
use App\Http\Requests;
use DB;
use App\Http\Controllers\Controller;
use Request;
use Excel;
use File;
use Illuminate\Support\Facades\Response;
use Validator;
use Illuminate\Support\Facades\Redirect;
use Input;
use Session;
use Geocode;
use Cache;
use URL;
use View;
use Khill\Lavacharts\Laravel\LavachartsFacade as Lava;
use Khill\Lavacharts\Lavacharts;
use TableView;
use App\Dategps;
use PHPExcel; 
use PHPExcel_IOFactory; 
use App\Brand;
use App\Category;
class BrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
       public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
          try {

 if (Auth::check())
  {


    if(Auth::user()->can('show-brands'))
      {

      $showadditem=1;
        $Brands = Brand::all();

        $Category = Category::where('Active','=','1')->lists('Name','id');
                
        return view('brands.index',compact('Brands','Category','showadditem'));

      }
    else 
      {
    return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
        
        
    }
    public function showallbrandcategory($id)
   
    {
       
         $Category = Category::whereId($id)->first(); 

          return view('brands.showcategory',compact('Category'));
    }

    public function checknamebrand()
    {
         $brandname=Input::get('Name');
      $brand = Brand::where('brands.Name','=',$brandname)
     ->select('*')->first();
     if($brand==[])     
        $isAvailable = true;
        else
         $isAvailable = false;

    return \Response::json(array('valid' =>$isAvailable,));
    }
      public function checkEngNamebrand()
    {
         $brandname=Input::get('EngName');
      $brand =Brand::where('brands.EngName','=',$brandname)
     ->select('*')->first();
     if($brand==[])     
        $isAvailable = true;
        else
         $isAvailable = false;

    return \Response::json(array('valid' =>$isAvailable,));
    }












    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()

    {
       
        return view('Brands.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
          try {

 if (Auth::check())
  {


    if(Auth::user()->can('create-brands'))
      {

        $Brandname=Input::get('Name');
        $Brandengname=Input::get('EngName');
        
        $Brands = New Brand;
        $Brands->Name=$Brandname;
        $Brands->EngName=$Brandengname;
         $Brands->Catogry_ID=Input::get('Category');
         $Brands->save();

             return redirect('/Brands');

      }
    else 
      {
    return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
      
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateBrands()
    {
  try {

 if (Auth::check())
  {


    if(Auth::user()->can('edit-brands'))
      {

        $brand_id= Input::get('pk');  
        $column_name = Input::get('name');
        $column_value = Input::get('value');
    
        $gpsData = Brand::whereId($brand_id)->first();
        $gpsData-> $column_name=$column_value;

        if($gpsData->save())

        return \Response::json(array('status'=>1));
    else 
        return \Response::json(array('status'=>0));

      }
    else 
      {
     return \Response::json(array('status'=>'You do not have permission.'));
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }

    }
public function updateactiveBrand()
    {
          try {

 if (Auth::check())
  {


    if(Auth::user()->can('edit-brands'))
      {

       $brandid = Input::get('id'); 
        $active= Input::get('active');
        
        $brand = brand::whereId($brandid)->first();

        $brand->Active=$active;

        if($brand->save())

          return \Response::json(array('status'=>1));
        else 
          return \Response::json(array('status'=>0));

      }
    else 
      {
     return \Response::json(array('status'=>'You do not have permission.'));
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
       

    }
    public function edit($id)
    {
      
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
          try {

 if (Auth::check())
  {


    if(Auth::user()->can('delete-brands'))
      {

        $tasks=Brand::find($id);
         
                    $tasks->delete();
                    return redirect('/Brands');

      }
    else 
      {
    return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
       
    
    }
}
