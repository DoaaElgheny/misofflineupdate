<?php

namespace App\Http\Controllers;

use Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Session;
use App\Company;
use App\Activity;
use Image;
use Excel;
use DB;
use PHPExcel; 
use PHPExcel_IOFactory;
use PHPExcel_Worksheet_Drawing;
use PHPExcel_Worksheet_MemoryDrawing;
use App\User;
use App\Government;
use App\District;
use Auth;

class ActivityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
   {
       $this->middleware('auth');
   }

   
    public function index()
    {
      try {

 if (Auth::check())
  {


    if(Auth::user()->can('Show Activity'))
      {
        try
           {
            $activity=Activity::where('Type','!=','Signs')->get();

            $governments=Government::select('Name','id')->lists('Name','id');
            $districts=District::select('Name','id')->lists('Name','id');
            $company =company::lists('Name', 'id');
            // dd( $company);

         
            $Users =User::lists('Name','id');
            return view('activity.index',compact('activity','company','Users','governments','districts'));
            }   
                catch (Exception $e)
            {
                return redirect('/');
                
            }
                  }
    else 
      {
        
  return view('errors.403');
      }

   }//endAuth

else
{
  
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
  return redirect('/login');
    }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }


public function Competitors()
    {
         $activity=Activity::all()->where('Type','Signs');
            $governments=Government::select('Name','id')->lists('Name','id');
            $districts=District::select('Name','id')->lists('Name','id');
            $company =company::lists('Name', 'id');
            $Users =User::lists('Name','id');
            return view('activity.Signs',compact('activity','company','Users','governments','districts'));

    }









    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
 try {

 if (Auth::check())
  {


    if(Auth::user()->can('Store Activity'))
      {
                

                  $District=District::
                  where('id', '=',Input::get('District'))
                  ->select('Name')->first();
                    $column_value = $District->Name;

          

                $Government=Government::where('id', '=',Input::get('Government'))
                    ->select('Name')->first();
                     $column_value = $Government->Name;
                    

        $ActivityData=new Activity();
        $ActivityData->Government=$Government->Name;
        $ActivityData->District=$District->Name;
        $ActivityData->Start_date=Input::get('start_date');
        $ActivityData->company_id=Input::get('company');
        $ActivityData->Description=Input::get('description');
        $ActivityData->Duration =Input::get('duration');
        $ActivityData->Activity_type=Input::get('activity_type');
        $ActivityData->User_id=Auth::user()->id;
        $ActivityData->Target_Segmentation=Input::get('Target_Segmentation');
        $ActivityData->Type=Input::get('Type');
  

          $file_img = Input::file('pic');
          if ($file_img !=Null)
          {
             $imageName = $file_img->getClientOriginalName();


                $path=public_path("assets/dist/img/Activity").$imageName;
              // notation octale : valeur du mode correcte
                  

                if (!file_exists($path)) {
                  $file_img->move(public_path("assets/dist/img/Activity/"),$imageName);
                  $ActivityData->Img="/assets/dist/img/Activity/".$imageName; 
                }
                else
                {
                   $random_string = md5(microtime());
                   $file_img->move(public_path("assets/dist/img/Activity/"),$random_string.".jpg");
                   
                  $ActivityData->Img="/assets/dist/img/Activity/".$random_string.".jpg";
                
                }
          }

         





    // $destinationPath = '/assets/img/';

    // $newImageName='Activity.png';
    // $file = Input::file('pic')->move($destinationPath,$newImageName);

    // //return json_encode($_POST);
    // //Rename and move the file to the destination folder 
    //     if($file!==null)
    //       {
    // $logo = base_path() .'\public\assets\img\Activity.png'; // Provide path to your logo file
                                          
    // $img = \Image::make( $logo)->resize(200,200);
    // Response::make($img->encode('png'));
    //       // $img = Image::make($file->getRealPath())->resize(200,200);
    //       //  Response::make($img->encode('jpeg'));
    //         $ActivityData->Img=$img;
    //       }
      //New 

      //End     
       $ActivityData->save();
       return redirect('activity');
             }
    else 
      {
        
  return view('errors.403');
      }

   }//endAuth

else
{
  
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
  return redirect('/login');
    }
  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $activity = activity::findOrFail($id);
        $pic = \Image::make($activity->Img);
        $response = Response::make($pic->encode('jpeg'));

        //setting content-type
        $response->header('Content-Type', 'image/jpeg');

        return $response;
    
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }






    public  function ACupdate()
    {
      try {

 if (Auth::check())
  {


    if(Auth::user()->can('Update Activity'))
      {
            $Activity = Input::get('pk');   
        $column_name = Input::get('name');
        $column_value = Input::get('value');
      if (Input::get('name')=='District')
        {
          
            // $District = DB::table('districts')
            //         ->where('id', '=',Input::get('value'))
            //         ->select('Name')->first();
            //         $column_value = $District->Name;

$District=District::where('id', '=',Input::get('value'))
                    ->select('Name')->first();
                    $column_value = $District->Name;

          

                




        }
     if (Input::get('name')=='Government')
        {
           $Government=Government::where('id', '=',Input::get('value'))
                    ->select('Name')->first();
                     $column_value = $Government->Name;
                     
            // $Government = DB::table('governments')
            //         ->where('id', '=',Input::get('value'))
            //         ->select('Name')->first();
            //          $column_value = $Government->Name;
        }
        $activityData = Activity::whereId($Activity)->first();
        

       $activityData-> $column_name=$column_value;


        if($activityData->save()) 
        return \Response::json(array('status'=>1));
    else 
        return \Response::json(array('status'=>0));
               }
    else 
      {
        
   return \Response::json(array('status'=>'You do not have permission.'));
      }

   }//endAuth

else
{
  
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
  return redirect('/login');
    }
 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       try {

 if (Auth::check())
  {


    if(Auth::user()->can('Delete Activity'))
      {
        try
        { $activity = activity::find($id);
        $activity->delete();
        return redirect('/activity');
    }
    catch(Exception $e)
    {
            return redirect('/activity');

    }
             }
    else 
      {
        
  return view('errors.403');
      }

   }//endAuth

else
{
  
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
  return redirect('/login');
    }
        
    }

       public function convertXLStoCSV($infile,$outfile)
    {
        $fileType = PHPExcel_IOFactory::identify($infile);
        $objReader = PHPExcel_IOFactory::createReader($fileType);

        $objReader->setReadDataOnly(false);   
        $objPHPExcel = $objReader->load($infile);    

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
        $objWriter->save($outfile);
    }



   public function ValidateActivity($data, $i,$img){ 
    // dd($img);
        if(!isset($GLOBALS['activity'])) { $GLOBALS['activity']= array(); } 
        if(!isset($ActivityErr)) { $ActivityErr = 'البيانات غير صحيحة لنشاط شركة: '; } 

        if(!isset($GLOBALS['Emptyactivity'])) { 
            $GLOBALS['Emptyactivity']= array(); 
        } 
        if(!isset($EmptyActivityErr)) { 
            $EmptyActivityErr = 'لا يوجد اسم شركة للنشاط التسويقي للصف رقم: '; 
        }     

        $name_regex = preg_match('/\p{Arabic}/u' , $data['promoter_name']);
        $gov_regex = preg_match('/\p{Arabic}/u' , $data['government']);
        $city_regex = preg_match('/\p{Arabic}/u' , $data['city']);
        $activity_type_regex = preg_match('/\p{Arabic}/u' , $data['activity_type']);
        $desc_regex = preg_match('/\p{Arabic}/u' , $data['description']);
        $duration_regex = preg_match('/\p{Arabic}/u' , $data['duration']);
        $start_date_regex = preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/' , $data['start_date']);

                
    $comp_id= (Company::where('name',$data['company_name'])->pluck('id')->first())? Company::       where('name',$data['company_name'])->pluck('id')->first():null;
        
    if($comp_id != null){
        if ($gov_regex == 1 && isset($data['government'])) {
            if ($city_regex == 1 && isset($data['city'])) {
                if ($activity_type_regex == 1 && isset($data['activity_type'])) {
                    if ($desc_regex == 1 || !isset($data['description'])) {
                            if ($start_date_regex == 1 && isset($data['start_date'])) {
                                if ($duration_regex==1 || !isset($data['duration'])) {
                                    if ($name_regex == 1) {

                         $activity = new Activity();
                                    $activity->goverment = $data['government'];
                                    $activity->city = $data['city']; 
                                    $activity->activity_type = $data['activity_type'];
                                    $activity->description = $data['description'];
                                    $activity->duration = $data['duration']; 
                                    $activity->start_date = $data['start_date'];
                                    $activity->img=$img;
                    if(isset($data['company_name'])){
                                $comp_id= Company::where('name',$data['company_name'])
                                        ->pluck('id')->first();
                    }                   
                                    $activity->company_id = $comp_id;

                                    try{
                                        $activity->save();
                                    }
                                    catch (\Exception $e) {
                                         // dd($e);

                                    $repeted = $data['government']."-".$data['city']."-".$data['start_date']."-".$data['activity_type'];
      
                                    $exist_string= "Duplicate entry '".$repeted."' for key 'activity_goverment_city_start_date_activity_type_unique'";

                        if ($exist_string == $e->errorInfo[2]) {  // activity exist 
                               $activity_id=Activity::where('start_date',$data['start_date'])
                                        ->where('goverment',$data['government'])
                                        ->where('city',$data['city'])
                                        ->where('activity_type',$data['activity_type'])
                                        ->pluck('id'); 


                            $updated_activity = Activity::find($activity_id[0]); 
                            // dd($updated_activity);           
                            $updated_activity->Government = $data['government'];
                            $updated_activity->District = $data['district']; 
                            $updated_activity->Activity_type = $data['activity_type'];
                            $updated_activity->Description = $data['description'];
                            $updated_activity->Duration = $data['duration']; 
                            $updated_activity->Start_date = $data['start_date'];
                            $updated_activity->Img=$img;

                            $updated_activity->save();
                        } 

                            } // enf catch
                            

                                    }
                                    else {
                                        array_push($GLOBALS['activity'],$data['company_name']);
                                    } 
                                }
                                else {
                                    array_push($GLOBALS['activity'],$data['company_name']); 
                                }                                
                            }
                            else {
                                array_push($GLOBALS['activity'],$data['company_name']); 
                            }
                        }
                        else {
                             array_push($GLOBALS['activity'],$data['company_name']); 
                        }
                    }
                    else {
                         array_push($GLOBALS['activity'],$data['company_name']); 
                    }
                
            }
            else {
                 array_push($GLOBALS['activity'],$data['company_name']);
            }
        }
        else {
            array_push($GLOBALS['activity'],$data['company_name']);
        }
    }
    else { // no company name
        array_push($GLOBALS['Emptyactivity'],$i); 
    }

    if ( !empty ($GLOBALS['Emptyactivity'] )) {
            $GLOBALS['Emptyactivity'] = array_unique($GLOBALS['Emptyactivity']);
            $EmptyActivityErr= $EmptyActivityErr.implode(" \n ",$GLOBALS['Emptyactivity']);
            $EmptyActivityErr = nl2br($EmptyActivityErr);  
            $cookie_name = 'EmptyActivityErr';
            $cookie_value = $EmptyActivityErr;
            setcookie($cookie_name, $cookie_value, time() + (60), "/");
        }
        if ( !empty ($GLOBALS['activity'] )) {
            $GLOBALS['activity'] = array_unique($GLOBALS['activity']);
            $ActivityErr = $ActivityErr.implode(" \n ",$GLOBALS['activity']);
            $ActivityErr = nl2br($ActivityErr);  
            $cookie_name = 'ActivityErr';
            $cookie_value = $ActivityErr;
            setcookie($cookie_name, $cookie_value, time() + (60), "/");
        }


    }
//Doaa Elgheny Import Acyvity
     public function importActivities()
    {
           try {

 if (Auth::check())
  {


    if(Auth::user()->can('import Activity Data'))
      {

       
        try
        {
         ini_set('xdebug.max_nesting_level', 120);
       ini_set('max_execution_time', 300);
     
                 $pic=[];

       $i=0;
         $file = Input::file('file');
        if($file == null){  //if no file selected  
                $errFile = "الرجاء اختيار الملف الملطلوب تحميلة";                
                $cookie_name = 'FileError';
                $cookie_value = $errFile;
                setcookie($cookie_name, $cookie_value, time() + (60), "/"); 
                return redirect('/activity');
            } 
        unset ($_COOKIE['FileError']);

        $importbtn= Request::get('submit'); 

        if(isset($importbtn))
        {   


            $filename = Input::file('file')->getClientOriginalName();     

            $Dpath = base_path();
            $upload_success =Input::file('file')->move( $Dpath, $filename); 
                  $objPHPExcel = PHPExcel_IOFactory::load($upload_success);
                  $objWorksheet = $objPHPExcel->getActiveSheet();
                  $highestRow = $objWorksheet->getHighestRow();


                
              for ($row=2; $row <= ($highestRow); $row++) { 

                

              foreach ($objWorksheet->getDrawingCollection()as $drawing) {


                 $string = $drawing->getCoordinates();


                            if(($string ==\PHPExcel_Cell::stringFromColumnIndex(7).$row))
                            {
                                        ob_start();

                                        call_user_func(
                                        $drawing->getRenderingFunction(),
                                        $drawing->getImageResource()
                                        );
                                        $imageContents = ob_get_contents();
                                        ob_end_clean();
                                        $extension = 'jpeg';

                                        $myFileName = 'Image_'.++$i.'.'.$extension;
                                        file_put_contents($myFileName,$imageContents);
                                        $img = \Image::make($myFileName)->resize(200,200);
                                        Response::make($img->encode('jpeg'));  

                                         array_push($pic, $myFileName); 
                                       



                           } 

                           }
                 $n=count($pic);

              if($n != ($row-1))
                                {

                                       
                                       
                                          $logo = base_path() . '\public\assets\img\Cancel.jpg'; // Provide path to your logo file
                                          


                                           $img = \Image::make( $logo)->resize(200,200);
                                             Response::make($img->encode('png'));  
                                       

                                            array_push($pic,$logo); 

                                        
                                }




                        
                           
              }



            // xls to csv conversion
//             $nameOnly = explode(".",$filename);

//             $newCSV =$nameOnly[0]."."."csv";

//             $PathnewCSV= $Dpath."\\".$newCSV ;

//            // chmod($PathnewCSV, 0777);
//             $myfile = fopen($PathnewCSV, "w");
// // dd($myfile);
//         app('App\Http\Controllers\ActivityController')->convertXLStoCSV($upload_success, $PathnewCSV);
       
      //End Image---------------------------------------------

       // Excel::load($upload_success, function($reader)
       // { 
$document = PHPExcel_IOFactory::load($upload_success);
// Get the active sheet as an array
$activeSheetData = $document->getActiveSheet()->toArray(null, true, true, true);  
    // $results 

 $j=0;
       for ($i=2;$i<=count($activeSheetData);$i++)
        {

        $Companyid=Company::where('Name', '=',$activeSheetData[$i][array_search("Company_Name",$activeSheetData[1])])
                        ->select('id')->first();
        
         $Userid=User::where('Name', '=',$activeSheetData[$i][array_search("Promoter_Name",$activeSheetData[1])])
                        ->select('id')->first();
                  
      $Activitys =new Activity();

$Activitys->Government=$activeSheetData[$i][array_search("Government",$activeSheetData[1])];

$Activitys->District =$activeSheetData[$i][array_search("City",$activeSheetData[1])];
$Activitys->Start_date =$activeSheetData[$i][array_search("Start Date",$activeSheetData[1])];
$Activitys->Duration =$activeSheetData[$i][array_search("Duration",$activeSheetData[1])];
$Activitys->Activity_type =$activeSheetData[$i][array_search("Activity_Type",$activeSheetData[1])];
$Activitys->Description =$activeSheetData[$i][array_search("Description",$activeSheetData[1])];
$Activitys->company_id =$Companyid->id;
$Activitys->User_id =$Userid->id;

$Activitys->Img=$pic[$j];

$Activitys->save();
$j+=1;

      }
   
    // });


    }

     return redirect('/activity'); 
   
 }
 catch(Exception $e)
 {
     return redirect('/activity'); 
 }
 }
    else 
      {
   return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
}
    //End


    public function importactivity(){

      ini_set('xdebug.max_nesting_level', 120);
       ini_set('max_execution_time', 300);
     
                 $pic=[];

       $i=0;
         $file = Input::file('file');
        if($file == null){  //if no file selected  
                $errFile = "الرجاء اختيار الملف الملطلوب تحميلة";                
                $cookie_name = 'FileError';
                $cookie_value = $errFile;
                setcookie($cookie_name, $cookie_value, time() + (60), "/"); 
                return redirect('/activity');
            } 
        unset ($_COOKIE['FileError']);

        $importbtn= Request::get('submit'); 

        if(isset($importbtn))
        {   


            $filename = Input::file('file')->getClientOriginalName();            
            $Dpath = base_path();
            $upload_success =Input::file('file')->move( $Dpath, $filename); 
                  $objPHPExcel = PHPExcel_IOFactory::load($upload_success);
                  $objWorksheet = $objPHPExcel->getActiveSheet();
                  $highestRow = $objWorksheet->getHighestRow();


                
              for ($row=2; $row <= ($highestRow); $row++) { 

                

              foreach ($objWorksheet->getDrawingCollection()as $drawing) {


                 $string = $drawing->getCoordinates();


                            if(($string ==\PHPExcel_Cell::stringFromColumnIndex(7).$row))
                            {
                                        ob_start();

                                        call_user_func(
                                        $drawing->getRenderingFunction(),
                                        $drawing->getImageResource()
                                        );
                                        $imageContents = ob_get_contents();
                                        ob_end_clean();
                                        $extension = 'jpeg';

                                        $myFileName = 'Image_'.++$i.'.'.$extension;
                                        file_put_contents($myFileName,$imageContents);
                                        $img = \Image::make($myFileName)->resize(200,200);
                                        Response::make($img->encode('jpeg'));  

                                         array_push($pic, $myFileName); 
                                       



                           } 

                           }
                 $n=count($pic);

              if($n != ($row-1))
                                {

                                       
                                       
                                          $logo = base_path() . '\public\assets\img\Cancel.jpg'; // Provide path to your logo file
                                          


                                           $img = \Image::make( $logo)->resize(200,200);
                                             Response::make($img->encode('png'));  
                                       

                                            array_push($pic,$logo); 

                                        
                                }




                        
                           
              }


            $nameOnly = explode(".",$filename);
            $newCSV =$nameOnly[0]."."."csv";
            $PathnewCSV= $Dpath."/".$newCSV ;
            $myfile = fopen($PathnewCSV, "w");
     
            
        app('App\Http\Controllers\ActivityController')->convertXLStoCSV($upload_success, $PathnewCSV);

        Excel::filter('chunk')->selectSheetsByIndex(0)->load($PathnewCSV)->chunk(150, function($results) use($pic){ 
                    


                    $data = $results->toArray();
                     $i=0;
                    foreach($data as $data) {

                   
                        $img=$pic[$i];


                         $i+=1;
                        app('App\Http\Controllers\ActivityController')->ValidateActivity($data, $i,$img);
                    } // end foreach data
            });
              
        } 
        return redirect('/activity');  
   }


}