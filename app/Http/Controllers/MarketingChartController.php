<?php

namespace App\Http\Controllers;

use App\Item;
use Auth;
use App\Http\Requests;
use App\Government;
use App\Contractor;
use DB;
use App\Http\Controllers\Controller;
use Request;
use Excel;
use File;
use DateTime;
use Illuminate\Support\Facades\Response;
use Validator;
use Illuminate\Support\Facades\Redirect;
use Input;
use Carbon;
use App\Gallery;
use App\District;
use App\Gps;

class MarketingChartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

  // view show marketing activity
  public function index()
    {  
      try 
        {
          if (Auth::check())
            {
              if(Auth::user()->can('show-activitychart'))
                {
                  return view('marketingchart.index'); 
                }//end if permission
              else 
                {
                  return view('errors.403');
                }
            }//endAuth
          else
            {
              return redirect('/login');
            }
        }//endtry
      catch(Exception $e) 
        {
          return redirect('/login');
        }     
    }


  public function  getchartnewdata()
    {
      $Start=Input::get('StartDate');
      $End=Input::get('EndDate');
      $Gov=Input::get('Government');
      $Government=explode(",",$Gov);
      $range=array($Start,$End);

      $activityitemids= DB::table('activityitem_attributes')
                          ->join('activity_items','activity_items.id','=','activityitem_attributes.activityitem_id')
                          ->join('sub_activities','sub_activities.id','=','activity_items.subactivity_id')
                          ->whereBetween('activityitem_attributes.Value',$range)
                          ->select('activityitem_attributes.activityitem_id')
                          ->groupBy('activityitem_attributes.activityitem_id')
                          ->lists('activityitem_attributes.activityitem_id');
      
      $data=DB::table('activity_contractors_retailers')
              ->join('activity_items','activity_items.id','=','activity_contractors_retailers.activityitem_id')
              ->join('contractors','contractors.id','=','activity_contractors_retailers.contractor_id')
              ->join('sub_activities','sub_activities.id','=','activity_items.subactivity_id')
              ->whereIn('activity_contractors_retailers.activityitem_id',$activityitemids)
              ->whereIn('contractors.Government',$Government)
              ->select('contractors.id','contractors.Name',DB::raw('count(DISTINCT(activity_contractors_retailers.activityitem_id)) as Count'),'contractors.Code')
              ->groupBy('contractors.id','contractors.Name','contractors.Code')
              ->get();

      $b=[];
      foreach ($data as  $value) 
        {
         if(array_key_exists($value->Count, $b))
          {
            $b[$value->Count]=$b[$value->Count]+1;
          }
          else
          {                
            $b[$value->Count]= 1;         
          }
        }
      $a=[];
      foreach ($b as $key => $value) 
        {
          array_push($a,array('label' => $key,'y' => $value));
        }
      usort($a, array($this, "cmp"));       
      return Response::json($a,  200, [], JSON_NUMERIC_CHECK);
    }


  public function cmp($a, $b)
    {
      return strcmp($a["label"], $b["label"]);
    }


  public function NewReport()
    {  
      try 
        {
          if (Auth::check())
            {
              if(Auth::user()->can('show-activitychart'))
                {
                  $Government=Government::all()->lists('Name','Name');
                  return view('marketingchart.NewCharts',compact('Government')); 
                }
              else 
                {
                  return view('errors.403');
                }//end else
            }//endAuth
          else
            {
              return redirect('/login');
            }
        }//endtry
      catch(Exception $e) 
        {
          return redirect('/login');
        }       
    }


  public function showbestcont()
    {  
      try 
        {
          if (Auth::check())
            {
              if(Auth::user()->can('show-bestcontractor'))
                {
                  $Government=Government::all()->lists('Name','Name');
                  return view('marketingchart.chartbestcont',compact('Government'));
                }
              else 
                {
                  return view('errors.403');
                }
             }//endAuth
          else
          {
          return redirect('/login');
          }
        }//endtry
      catch(Exception $e) 
        {
          return redirect('/login');
        }
    }


  public function contractorindex()
    {  
      try 
        {
          if (Auth::check())
            {
              if(Auth::user()->can('show-contractorchart'))
                {
                  return view('marketingchart.contractorindex');  
                }
              else 
                {
                  return view('errors.403');
                }
            }//endAuth
          else
            {
              return redirect('/login');
            }
        }//endtry
      catch(Exception $e) 
        {
          return redirect('/login');
        }         
    }


  public function gallery($id)
    { 
      try 
        {
          if (Auth::check())
            {
              if(Auth::user()->can('show-galleries'))
                {
                  $photos=Gallery::where('marketingactivity_id','=',$id)->get();
                  return view('marketingchart.Gallery',compact('photos'));  
                }
              else 
                {
                  return view('errors.403');
                }
            }//endAuth
          else
            {
              return redirect('/login');
            }
        }//endtry
      catch(Exception $e) 
        {
          return redirect('/login');
        }       
    }


  public function activitychartdata()
    {
      $Start=Input::get('StartDate');
      $End=Input::get('EndDate');
      $activityitemids=DB::table('activityitem_attributes')
                        ->join('activity_items','activity_items.id','=','activityitem_attributes.activityitem_id')
                        ->join('sub_activities','sub_activities.id','=','activity_items.subactivity_id')
                        ->whereBetween('activityitem_attributes.Value',[$Start,$End])
                        ->select('activityitem_attributes.activityitem_id')
                        ->groupBy('activityitem_attributes.activityitem_id')
                        ->lists('activityitem_attributes.activityitem_id');

  	  $data=DB::table('activity_contractors_retailers')
              ->join('activity_items','activity_items.id','=','activity_contractors_retailers.activityitem_id')
              ->join('sub_activities','sub_activities.id','=','activity_items.subactivity_id')
              ->whereIn('activity_contractors_retailers.activityitem_id',$activityitemids)
              ->select('sub_activities.Name',DB::raw('count(DISTINCT(activity_contractors_retailers.contractor_id)) as ContractorCount'),DB::raw('count(DISTINCT(activity_contractors_retailers.Retailer_id)) as RetailerCount'))
              ->groupBy('sub_activities.id','sub_activities.Name')
              ->get();

      $b=[];
      $a=[];
      foreach ($data as  $value) 
        {
         	array_push($b,array('label' => $value->Name,'y' => $value->ContractorCount));
          array_push($a,array('label' => $value->Name,'y' => $value->RetailerCount));
        }
      $data = array();
      $data['totalcontractor']=$b;
      $data['totalretailer']=$a;
      return Response::json($data,  200, [], JSON_NUMERIC_CHECK);
    }


  public function BestContractorNew()
    {

      $StartDate=Input::get('Start').'-1-1';
      $EndDate=Input::get('Start').'-12-31';
      $Start=new Carbon($StartDate);
      $End=new Carbon($EndDate);

      $user= DB::table("activity_contractors_retailers")
              ->join('activityitem_attributes','activityitem_attributes.activityitem_id','=','activity_contractors_retailers.activityitem_id')
              ->select(DB::raw("MONTH(activity_contractors_retailers.JoinDate) as Month"),DB::raw('count(DISTINCT(activity_contractors_retailers.activityitem_id))  as countactivity'),DB::raw('count(DISTINCT(activity_contractors_retailers.contractor_id))  as countcontractor'),DB::raw('count(DISTINCT(activity_contractors_retailers.Retailer_id))  as countretailer'))
              ->whereBetween(DB::raw("Date(activity_contractors_retailers.JoinDate)"),[$Start,$End])
              ->orderBy('activity_contractors_retailers.JoinDate')
              ->groupBy(DB::raw("MONTH(activity_contractors_retailers.JoinDate)"))
              ->get();

      $a=[];
      foreach ($user as  $value) 
        {

          $dateObj   = DateTime::createFromFormat('!m', $value->Month);
          $monthName = $dateObj->format('F');
          array_push($a,array('label' => $monthName,'y' => $value->countcontractor));
        }
      $data = array();
      $data['totalcontractor']=$a;
      return Response::json($data,  200, [], JSON_NUMERIC_CHECK);
    }


  public function BestContractor()
    {

      $StartDate=Input::get('Start').'-1-1';
      $EndDate=Input::get('Start').'-12-31';
      $Start=new Carbon($StartDate);
      $End=new Carbon($EndDate);

      $user= DB::table("activity_contractors_retailers")
              ->join('activityitem_attributes','activityitem_attributes.activityitem_id','=','activity_contractors_retailers.activityitem_id')
              ->select(DB::raw("MONTH(activity_contractors_retailers.JoinDate) as Month"),DB::raw('count(DISTINCT(activity_contractors_retailers.activityitem_id))  as countactivity'),DB::raw('count(DISTINCT(activity_contractors_retailers.contractor_id))  as countcontractor'),DB::raw('count(DISTINCT(activity_contractors_retailers.Retailer_id))  as countretailer'))
              ->whereBetween(DB::raw("Date(activity_contractors_retailers.JoinDate)"),[$Start,$End])
              ->orderBy('activity_contractors_retailers.JoinDate')
              ->groupBy(DB::raw("MONTH(activity_contractors_retailers.JoinDate)"))
              ->get();
      $b=[];
      $a=[];
      $c=[];           
      foreach ($user as  $value) 
        {

          $dateObj   = DateTime::createFromFormat('!m', $value->Month);
          $monthName = $dateObj->format('F');
          array_push($b,array('label' => $monthName,'y' => $value->countactivity));
          array_push($a,array('label' => $monthName,'y' => $value->countcontractor));
          array_push($c,array('label' => $monthName,'y' => $value->countretailer));
        }
      $data = array();
      $data['totalcontractor']=$a;
      $data['totalactivity']=$b;
      $data['totalretailer']=$c;
      return Response::json($data,  200, [], JSON_NUMERIC_CHECK);
    }


  public function BestContractorwithdate()
    {
      $value=Input::all();
      $Start=Input::get('StartDate');
      $End=Input::get('EndDate');
      $Government=Input::get('Government');
      $district=Input::get('district');
      if($district=="all")
        {
          $districts=District::get()->lists('Name');
        }
      else
        {
          $districts=explode(',', $district);
        }
      $activityitemids= DB::table('activityitem_attributes')
                          ->join('activity_items','activity_items.id','=','activityitem_attributes.activityitem_id')
                          ->join('sub_activities','sub_activities.id','=','activity_items.subactivity_id')
                          ->whereBetween('activityitem_attributes.Value',[$Start,$End])
                          ->select('activityitem_attributes.activityitem_id')
                          ->groupBy('activityitem_attributes.activityitem_id')
                          ->lists('activityitem_attributes.activityitem_id');
        
      $data=DB::table('activity_contractors_retailers')
              ->join('activity_items','activity_items.id','=','activity_contractors_retailers.activityitem_id')
              ->join('contractors','contractors.id','=','activity_contractors_retailers.contractor_id')
              ->join('sub_activities','sub_activities.id','=','activity_items.subactivity_id')
              ->whereIn('activity_contractors_retailers.activityitem_id',$activityitemids)
              ->where('contractors.Government',$Government)
              ->whereIn('contractors.District',$districts)
              ->select('contractors.id','contractors.Name',DB::raw('count(DISTINCT(activity_contractors_retailers.activityitem_id)) as Count'),'contractors.Code')
              ->groupBy('contractors.id','contractors.Name','contractors.Code')
              ->havingRaw('Count > 1')
              ->get();
      $b=[];
      $a=[];          
      foreach ($data as  $value) 
        {
          array_push($b,array('label' => $value->Name."\n".$value->Code,'y' => $value->Count));
          $query=DB::table('activitables')
                    ->join('activity_items','activity_items.id','=','activitables.activity_item_id')
                    ->join('contractors','contractors.id','=','activitables.activitable_id')
                    ->whereIn('activitables.activity_item_id',$activityitemids)
                    ->where('activitables.activitable_id',$value->id)
                    ->where('activitables.activitable_type','App\Contractor')
                    ->select('activitables.Quantity')
                    ->first();
          if($query!=null)
            array_push($a,array('label' => $value->Name."\n".$value->Code,'y' => $query->Quantity));
          else
            array_push($a,array('label' => $value->Name."\n".$value->Code,'y' => 0));
        }
      $data = array();
      $data['totalprize']=$a;
      $data['totalactivity']=$b;
      return Response::json($data,  200, [], JSON_NUMERIC_CHECK);
    }


  public function Bestretailerwithdate()
    {
      $value=Input::all();
      $Start=Input::get('StartDate');
      $End=Input::get('EndDate');
      $activityitemids= DB::table('activityitem_attributes')
                          ->join('activity_items','activity_items.id','=','activityitem_attributes.activityitem_id')
                          ->join('sub_activities','sub_activities.id','=','activity_items.subactivity_id')
                          ->whereBetween('activityitem_attributes.Value',[$Start,$End])
                          ->select('activityitem_attributes.activityitem_id')
                          ->groupBy('activityitem_attributes.activityitem_id')
                          ->lists('activityitem_attributes.activityitem_id');
         
      $data=DB::table('activity_contractors_retailers')
              ->join('activity_items','activity_items.id','=','activity_contractors_retailers.activityitem_id')
              ->join('retailers','retailers.id','=','activity_contractors_retailers.Retailer_id')
              ->join('sub_activities','sub_activities.id','=','activity_items.subactivity_id')
              ->whereIn('activity_contractors_retailers.activityitem_id',$activityitemids)
              ->select('retailers.id',DB::raw('count(DISTINCT(activity_contractors_retailers.activityitem_id)) as Count'),'retailers.Code')
              ->groupBy('retailers.id','retailers.Code')
              ->havingRaw('Count > 2')
              ->get();

      $b=[];
      $a=[];
      foreach ($data as  $value) 
        {
          array_push($b,array('label' => $value->Code,'y' => $value->Count));
          $query=DB::table('activitables')
                    ->join('activity_items','activity_items.id','=','activitables.activity_item_id')
                    ->join('retailers','retailers.id','=','activitables.activitable_id')
                    ->whereIn('activitables.activity_item_id',$activityitemids)
                    ->where('activitables.activitable_id',$value->id)
                    ->where('activitables.activitable_type','App\Retailer')
                    ->select(DB::raw('count(activitables.Quantity) as Count'))
                    ->first();
          array_push($a,array('label' =>$value->Code,'y' => $query->Count));
        }
      $data = array();
      $data['totalprize']=$a;
      $data['totalactivity']=$b;
      return Response::json($data,200, [], JSON_NUMERIC_CHECK);
    }


  public function productcontractor()
    {
    	$Educationdata=Contractor::whereNotNull('Education')
                                ->select('contractors.Education',DB::raw('count(contractors.id) as Count'))
                                ->groupBy('contractors.Education')
                                ->get();
      $E=[];
      foreach ($Educationdata as  $value) 
        {
          array_push($E,array('legendText' => $value->Education,'indexLabel' => $value->Education,'y' => $value->Count));
        }
      return Response::json($E,  200, [], JSON_NUMERIC_CHECK);
    }


  public function govermentcontractor()
    {
    	$data=Contractor::select('contractors.Government',DB::raw('count(contractors.id) as Count'))->groupBy('contractors.Government')->get();
      $b=[];
      foreach ($data as  $value) 
        {
          array_push($b,array('label' => $value->Government,'y' => $value->Count));
        }
      return Response::json($b,  200, [], JSON_NUMERIC_CHECK);
    }


  public function upperlowercontractor()
    {
    	$data=Contractor::join('governments','governments.Name','=','contractors.Government')
                      ->select('governments.GType',DB::raw('count(contractors.id) as Count'))
                      ->groupBy('governments.GType')
                      ->get();
      $b=[];
      foreach ($data as  $value) 
        {
          array_push($b,array('legendText' => $value->GType,'indexLabel' => $value->GType,'y' => $value->Count));
        }
      return Response::json($b,  200, [], JSON_NUMERIC_CHECK);
    }

  public function reviewedcontractor()
    {
    	$data=Contractor::join('reviews','reviews.Contractor_Id','=','contractors.id')
                      ->select('reviews.Status',DB::raw('count(contractors.id) as Count'))
                      ->groupBy('reviews.Status')
                      ->get();
      $b=[];
      foreach ($data as  $value) 
        {
          array_push($b,array('label' => $value->Status,'y' => $value->Count));
        }
      return Response::json($b,  200, [], JSON_NUMERIC_CHECK);
    }
    

  public function analysisContractor()
    {
    	$facedata=Contractor::whereNotNull('Has_Facebook')
                          ->select('contractors.Has_Facebook',DB::raw('count(contractors.id) as Count'))
                          ->groupBy('contractors.Has_Facebook')
                          ->get();
      $f=[];
      foreach ($facedata as  $value) 
        {
          array_push($f,array('label' => $value->Has_Facebook,'y' => $value->Count));
        }
      $phonedata=Contractor::whereNotNull('Phone_Type')
                            ->select('contractors.Phone_Type',DB::raw('count(contractors.id) as Count'))
                            ->groupBy('contractors.Phone_Type')
                            ->get();
      $p=[];
      foreach ($phonedata as  $value) 
        {
          array_push($p,array('label' => $value->Phone_Type,'y' => $value->Count));
        }
      $nullemail=Contractor::whereNull('Email')
                            ->select(DB::raw('count(contractors.id) as Count'))
                            ->first();

      $notnullemail=Contractor::whereNotNull('Email')
                              ->select(DB::raw('count(contractors.id) as Count'))
                              ->first();
      $b=[];
      array_push($b,array('label' => 'Yes','y' => $notnullemail->Count));
      array_push($b,array('label' => 'No','y' => $nullemail->Count));

      $moqaulcont=Contractor::where('Job','مقاول')
                            ->select(DB::raw('count(contractors.id) as Count'))
                            ->first();
      $notmoqaulcont=Contractor::where('Job','!=','مقاول')
                                ->select(DB::raw('count(contractors.id) as Count'))
                                ->first();
      $nodatacont=Contractor::whereNull('Job')
                            ->select(DB::raw('count(contractors.id) as Count'))
                            ->first();
      $E=[];
      array_push($E,array('label' => 'مقاول','y' => $moqaulcont->Count));
      array_push($E,array('label' => 'مقاول تكميلى','y' => $notmoqaulcont->Count));
      array_push($E,array('label' => 'لا يوجد بيانات','y' => $nodatacont->Count));
		  $data['Has_Facebook']=$f;       
		  $data['Phone_Type']=$p;
		  $data['Has_Email']=$b;
      $data['Education']=$E;
      return Response::json($data,  200, [], JSON_NUMERIC_CHECK);
    }
}
