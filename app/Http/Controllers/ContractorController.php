<?php

namespace App\Http\Controllers;

use Auth;

use App\User;
use App\Gps;
use App\Task;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Request;
use Excel;
use File;
use Illuminate\Support\Facades\Response;
use Validator;
use Illuminate\Support\Facades\Redirect;
use Input;
use Session;
use Geocode;
use Cache;
use URL;
use View;
use Khill\Lavacharts\Laravel\LavachartsFacade as Lava;
use Khill\Lavacharts\Lavacharts;
use TableView;
use App\Dategps;
use PHPExcel; 
use PHPExcel_IOFactory; 
use App\Government;
use App\District;
use App\CompanyWarehouses;
use App\Company;
use App\Contractor;

//Creadited By :Doaa Elgheny  27/10/2016
class ContractorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function __construct()
   {
       $this->middleware('auth');
   }
   public function showNewContractor()
   {

  $datetimeNow=\Carbon::now();
        $datetimeNow=$datetimeNow->todatestring();
    
    $range=array($datetimeNow,$datetimeNow);
     $Contractor=Contractor::where('Status','New')
    ->wherebetween('created_at',$range)
    ->get();
     return view('Contractors.ContractorNew',compact('Contractor'));
   }
   public function getNewcontractor(Request $request)
   {
    $range=array( input::get('Start_Date'),input::get('End_Date'));
   
    $Contractor=Contractor::where('Status','New')
    ->wherebetween('created_at',$range)
    ->get();

        return view('Contractors.ContractorNew',compact('Contractor'));
   
   }
public function allPosts(Request $request)
    {
      error_reporting(E_ALL);
ini_set('display_errors', '1');
        $columns = array( 
                                    0 =>'id', 
                                    1 =>'Name',
                                    2 =>'Government',
                                    3 =>'Tele1',     
                                    4 =>'District',
                                    5=>'Code',
                                    6=> 'Tele2',
                                    7 => 'Address',
                                    8=> 'Education',
                                    9 => 'Has_Facebook',
                                    10=> 'Facebook_Account',
                                    11=> 'Computer',
                                    12 => 'Email',
                                    13=> 'Birthday',
                                    14=>  'Job',
                                    15 => 'Intership_No',
                                    16 => 'Phone_Type',
                                    17 => 'Nickname',
                                    18=> 'Religion',
                                    19 => 'Home_Phone',    
                                    20=> 'Fame',
                                    21=>'Status'

                        );
  
        $totalData = Contractor::count();
            
        $totalFiltered = $totalData; 
        $order_columns=Request::get('order');

       $limit = Request::get('length');
       $start = Request::get('start');
       $order = $columns[ $order_columns[0]['column']];
       $dir = $order_columns[0]['dir'];
       $searchall=Request::get('search');

   
        if(empty($searchall['value']))
        {            
            $Contractors = Contractor::offset($start)
                         ->limit($limit)
                         ->orderBy($order,$dir)
                         ->get();
        }
        else {
            $search =$searchall['value']; 

            $Contractors =  Contractor::where('id','LIKE',"%{$search}%")
                            ->orWhere('Name', 'LIKE',"%{$search}%")
                            ->orWhere('Government', 'LIKE',"%{$search}%")
                            ->orWhere('District', 'LIKE',"%{$search}%")
                            ->orWhere('Tele1', 'LIKE',"%{$search}%")
                            ->orWhere('Code', 'LIKE',"%{$search}%")
                            ->orWhere('Tele2', 'LIKE',"%{$search}%")
                             ->orWhere('Address', 'LIKE',"%{$search}%")
                             ->orWhere('Education', 'LIKE',"%{$search}%")
                             ->orWhere('Has_Facebook', 'LIKE',"%{$search}%")
                             ->orWhere('Facebook_Account', 'LIKE',"%{$search}%")
                             ->orWhere('Computer', 'LIKE',"%{$search}%")
                             ->orWhere('Email', 'LIKE',"%{$search}%")
                             ->orWhere('Birthday', 'LIKE',"%{$search}%")
                             ->orWhere('Job', 'LIKE',"%{$search}%")
                             ->orWhere('Intership_No', 'LIKE',"%{$search}%")
                             ->orWhere('Phone_Type', 'LIKE',"%{$search}%")
                             ->orWhere('Nickname', 'LIKE',"%{$search}%")
                             ->orWhere('Religion', 'LIKE',"%{$search}%")
                             ->orWhere('Home_Phone', 'LIKE',"%{$search}%")
                             ->orWhere('Fame', 'LIKE',"%{$search}%")
                             ->orWhere('Status', 'LIKE',"%{$search}%")
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order,$dir)
                            ->get();
                           

            $totalFiltered = Contractor::where('id','LIKE',"%{$search}%")
                             ->orWhere('Name', 'LIKE',"%{$search}%")
                             ->orWhere('Government', 'LIKE',"%{$search}%")
                            ->orWhere('District', 'LIKE',"%{$search}%")
                            ->orWhere('Tele1', 'LIKE',"%{$search}%")
                            ->orWhere('Code', 'LIKE',"%{$search}%")
                              ->orWhere('Address', 'LIKE',"%{$search}%")
                             ->orWhere('Education', 'LIKE',"%{$search}%")
                             ->orWhere('Has_Facebook', 'LIKE',"%{$search}%")
                             ->orWhere('Facebook_Account', 'LIKE',"%{$search}%")
                             ->orWhere('Computer', 'LIKE',"%{$search}%")
                             ->orWhere('Email', 'LIKE',"%{$search}%")
                             ->orWhere('Birthday', 'LIKE',"%{$search}%")
                             ->orWhere('Job', 'LIKE',"%{$search}%")
                             ->orWhere('Intership_No', 'LIKE',"%{$search}%")
                             ->orWhere('Phone_Type', 'LIKE',"%{$search}%")
                             ->orWhere('Nickname', 'LIKE',"%{$search}%")
                             ->orWhere('Religion', 'LIKE',"%{$search}%")
                             ->orWhere('Home_Phone', 'LIKE',"%{$search}%")
                             ->orWhere('Fame', 'LIKE',"%{$search}%")
                             ->orWhere('Status', 'LIKE',"%{$search}%")
                             ->count();
        }

        $data = array();
        if(!empty($Contractors))
        {
            foreach ($Contractors as $Contractor)
            {
                
            
                $edit = '/offlineneew/public/Contractors/'.$Contractor->id.'/edit';
                $Delete='/offlineneew/public/Contractors/destroy/'.$Contractor->id;
                $nestedData['id'] = $Contractor->id;
                $nestedData['Name'] = $Contractor->Name;
                $nestedData['Government'] = $Contractor->Government;
                $nestedData['District'] = $Contractor->District;
                $nestedData['Tele1'] = $Contractor->Tele1;
                $nestedData['Code'] = $Contractor->Code;
                $nestedData['Tele2'] =$Contractor->Tele2;
                $nestedData['Address'] =$Contractor->Address;
                $nestedData['Education'] =$Contractor->Education;
                $nestedData['Has_Facebook'] =$Contractor->Has_Facebook;
                $nestedData['Facebook_Account'] =$Contractor->Facebook_Account;
                $nestedData['Computer'] =$Contractor->Computer;
                $nestedData['Email'] =$Contractor->Email;
                $nestedData['Birthday'] =$Contractor->Birthday;
                $nestedData['Job'] =$Contractor->Job;
                $nestedData['Intership_No'] =$Contractor->Intership_No;
                $nestedData['Phone_Type'] =$Contractor->Phone_Type;
                $nestedData['Nickname'] =$Contractor->Nickname;
                $nestedData['Religion'] =$Contractor->Religion;
                $nestedData['Home_Phone'] =$Contractor->Home_Phone;
                $nestedData['Fame'] =$Contractor->Fame;
                $nestedData['Status'] =$Contractor->Status;
                $nestedData['Actions'] = "&emsp;<a href='{$edit}' title='EDIT' ><span class='glyphicon glyphicon-edit'></span></a>
                &emsp;<a href='{$Delete}' title='Delete' ><span class='glyphicon glyphicon-trash'></span></a>";
         
                $data[] = $nestedData;
            }
        }
          
         // dd(Request::input('draw'));
        $json_data = array(
                    "draw"            => intval(Request::get('draw')),  
                    "recordsTotal"    => intval($totalData),  
                    "recordsFiltered" => intval($totalFiltered),
                   
                    "data"            => $data   
                    );
        return json_encode($json_data); 
        
    }
    public function index()
    {
           try {

 if (Auth::check())
  {


    if(Auth::user()->can('Show Contractors'))
      {ini_set('memory_limit', '-1');


       
       $Contractors=Contractor::all();
       $governments=Government::select('Name','id')->lists('Name','id');
       $districts=District::select('Name','id')->lists('Name','id');
       return view('Contractors.index',compact('Contractors','governments','districts'));

      }
    else 
      {
    return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    //Doaa Elgheny Function Import File
    public function importContractors()
    {
             try {

 if (Auth::check())
  {


    if(Auth::user()->can('import Contractors Data'))
      {
       
        try
        {
  $temp= Request::get('submit'); 
  // $file=input::get('file');
  //  if($file == null){  //if no file selected  
  //               $errFile = "الرجاء اختيار الملف الملطلوب تحميلة";                
  //               $cookie_name = 'FileError';
  //               $cookie_value = $errFile;
  //               setcookie($cookie_name, $cookie_value, time() + (60), "/"); 
  //               return redirect('/Contractors');
  //           } 
  //       unset ($_COOKIE['FileError']);
   if(isset($temp))

 { 
   $filename = Input::file('file')->getClientOriginalName();
     $Dpath = base_path();


     $upload_success =Input::file('file')->move( $Dpath, $filename);

       Excel::load($upload_success, function($reader)
       {   
    $results = $reader->get()->all();


       foreach ($results as $data)
        {

       
       // $Prmoterid=User::where('Name', '=',$data['pormoter_id'])->select('id')->first();
                        // dd($Prmoterid->id);

        $Cont =Contractor::where('Code',$data['code'])->first();
       if($Cont==[])
       {
         $Contractors =New Contractor;
       
        $Contractors->Name =$data['name'];
       //$Contractors->EngName=$data['englishname'];
       $Contractors->Government =$data['government'];
       $Contractors->District =$data['district'];
       // $Contractors->Address =$data['address'];
       //  $Contractors->Education =$data['education'];
       //  $Contractors->Has_Facebook =$data['has_facebook'];
       //  $Contractors->Phone_Type  =$data['phonetype'];
        
       //  $Contractors->Facebook_Account =$data['facebook_account'];
       //  $Contractors->Computer =$data['computer'];
       //  $Contractors->Email=$data['email'];
       //  $Contractors->Birthday=$data['birthdate'];
        $Contractors->Tele1 =$data['telephone1'];
        $Contractors->Tele2 =$data['telephone2'];
      //  $Contractors->Job =$data['job'];
      //   $Contractors->Intership_No =$data['intership_no'];
      // $Contractors->Nickname =$data['nickname'];
      //   if($Prmoterid!=[])
      //   $Contractors->Pormoter_Id =$Prmoterid->id;
      // else
         $Contractors->Pormoter_Id =null;
       // $Contractors->Religion=$data['religion'];
       // $Contractors->Home_Phone =$data['home_phone'];
     //  $Contractors->Fame =$data['fame'];
         $Contractors->Code=$data['code'];
         $Contractors->Status="New";
        $Contractors->save();
        }
      }
   
    });


    }

     return redirect('/Contractors'); 
   
 }
 catch(Exception $e)
 {
     return redirect('/Contractors'); 
 }
 }
    else 
      {
    return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
}
    //End Import
    public function store(Request $request)
    {
         try {

 if (Auth::check())
  {


    if(Auth::user()->can('store Contractors'))
      {
          
       //to get Name of Government
           $Government = Government::where('id', '=',Input::get('Government'))
                        ->select('Name')->first();
        //to get Name of districts               
           $District = District::where('id', '=',Input::get('district'))
                        ->select('Name')->first();
                       
                       
            $ContractorsData = New Contractor;
            $ContractorsData->Name=Input::get('Name');

            $ContractorsData->EngName=Input::get('EngName'); 

            $ContractorsData->District=$District->Name;
            $ContractorsData->Government=$Government->Name; 
            $ContractorsData->Address=Input::get('Address');
            $ContractorsData->Education=Input::get('Education');  
            $ContractorsData->Has_Facebook=Input::get('Has_Facebook');
            $ContractorsData->Facebook_Account=Input::get('Facebook_Account');  
            $ContractorsData->Computer=Input::get('Computer');
            $ContractorsData->Email=Input::get('Email');
            $ContractorsData->Birthday=Input::get('Birthday');
            $ContractorsData->Tele1=Input::get('Tele1');  
            $ContractorsData->Tele2=Input::get('Tele2');
            $ContractorsData->Job=Input::get('Job');
            $ContractorsData->Intership_No=Input::get('Intership_No');
            $ContractorsData->Phone_Type=Input::get('Phone_Type');  
            $ContractorsData->Nickname=Input::get('Nickname');
            $ContractorsData->Religion=Input::get('Religion');
            $ContractorsData->Home_Phone=Input::get('Home_Phone');
            $lastcontractor=Contractor::orderBy('Code', 'desc')->first();
         
            $ContractorsData->Code=(int)($lastcontractor->Code)+1;

            $ContractorsData->Fame=Input::get('Fame');
            $ContractorsData->Status='New'; 
            $ContractorsData->Pormoter_Id=Auth::user()->id;

            $ContractorsData->save();              
            return redirect('/Contractors');
          }
    else 
      {
    return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
       }
    public function updateContractors()

     {
          try {

 if (Auth::check())
  {


    if(Auth::user()->can('update Contractors'))
      {

        $Contractorid = Input::get('pk');   
        $column_name = Input::get('name');
        $column_value = Input::get('value');
    //if it be government or districts
     if (Input::get('name')=='district')
        {
          
            $District =District::where('id', '=',Input::get('value'))
                    ->select('Name')->first();
                    $column_value = $District->Name;
        }
     if (Input::get('name')=='Government')
        {
           
            $Government = Government::where('id', '=',Input::get('value'))
                    ->select('Name')->first();
                     $column_value = $Government->Name;
        }
        $ContractorData = Contractor::whereId($Contractorid)->first();
        $ContractorData-> $column_name=$column_value;
        if($ContractorData->save())
        return \Response::json(array('status'=>1));
        else 
        return \Response::json(array('status'=>0));
     }
    else 
      {
    return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
}
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         try {

 if (Auth::check())
  {


    if(Auth::user()->can('Edit contractors'))
      {
          
        $Contractors=Contractor::find($id);
       $government=Government::select('Name','id')->lists('Name','Name');
       $districts=District::select('Name','id')->lists('Name','Name');
       return view('Contractors.Edit',compact('Contractors','government','districts'));
        }
    else 
      {
    return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {

        $ContractorsData=Contractor::find($id);

        //to get Name of districts               
       
            $ContractorsData->Name=Request::get('Name');
            $ContractorsData->EngName=Request::get('EngName');  
            $ContractorsData->District=Request::get('District');
            $ContractorsData->Government=Request::get('Government'); 
            $ContractorsData->Address=Request::get('Address');
            $ContractorsData->Education=Request::get('Education');  
            $ContractorsData->Has_Facebook=Request::get('Has_Facebook');
            $ContractorsData->Facebook_Account=Request::get('Facebook_Account');  
            $ContractorsData->Computer=Request::get('Computer');
            $ContractorsData->Email=Request::get('Email');
            $ContractorsData->Birthday=Request::get('Birthday');
            $ContractorsData->Tele1=Request::get('Tele1');  
            $ContractorsData->Tele2=Request::get('Tele2');
            $ContractorsData->Job=Request::get('Job');
            $ContractorsData->Intership_No=Request::get('Intership_No');
            $ContractorsData->Phone_Type=Request::get('Phone_Type');  
            $ContractorsData->Nickname=Request::get('Nickname');
            $ContractorsData->Religion=Request::get('Religion');
            $ContractorsData->Home_Phone=Request::get('Home_Phone');
            $ContractorsData->Fame=Request::get('Fame');
            $ContractorsData->save();  
return redirect('/Contractors');


    }
    public function checkContractorEmail()
    {
      $energySourcename=Input::get('Email');
        $id=Input::get('id');
      if($id==null)
      {
      $contractorsData = Contractor::where('contractors.Email','=',$energySourcename)
                       ->select('*')->first();

        if($contractorsData==null) 
            $isAvailable = true;
        else
            $isAvailable = false;
          }
       else
       {
         $contractorsData =Contractor::where('contractors.Email','=',$energySourcename)
                       ->where('contractors.id','!=',$id)
                       ->select('*')->first();

        if($contractorsData==null) 
            $isAvailable = true;
        else
            $isAvailable = false;
    }

        return \Response::json(array('valid' =>$isAvailable,));
    }

    public function checkContractorTel1()
    {
        $ContractorEngName=Input::get('Tele1');
        $id=Input::get('id');
          if($id==null)
          {
                $contractorsData = Contractor::where('contractors.Tele1','=',$ContractorEngName)
                                 ->orWhere('contractors.Tele2','=',$ContractorEngName)
                                 ->select('*')->first();
                              
                if($contractorsData==null)
                       
                    $isAvailable = true;
                else
                     $isAvailable = false;
           }
           else
           { 
                $contractorsData = Contractor::Where('contractors.Tele2','=',$ContractorEngName)
                                  ->where('contractors.id','!=',$id)
                                 ->select('*')->first();
                 $contractorsData1 = Contractor::where('contractors.Tele1','=',$ContractorEngName)
                  ->where('contractors.id','!=',$id)
                 ->select('*')->first();
                    
                if($contractorsData1==null && $contractorsData==null)
                       
                    $isAvailable = true;
                else
                     $isAvailable = false;
            }

            return \Response::json(array('valid' =>$isAvailable,));
    }
     public function checkContractorTel2()
    {
        $ContractorEngName=Input::get('Tele2');
             $id=Input::get('id');
          if($id==null)
          {
                $contractorsData = Contractor::where('contractors.Tele1','=',$ContractorEngName)
                                 ->orWhere('contractors.Tele2','=',$ContractorEngName)
                                 ->select('*')->first();   
                if($contractorsData==null)
                       
                    $isAvailable = true;
                else
                     $isAvailable = false;

           }
           else
           { 
                $contractorsData = Contractor::Where('contractors.Tele2','=',$ContractorEngName)
                                  ->where('contractors.id','!=',$id)
                                 ->select('*')->first();
                 $contractorsData1 = Contractor::where('contractors.Tele1','=',$ContractorEngName)
                  ->where('contractors.id','!=',$id)
                 ->select('*')->first();
                           
                if($contractorsData==null && $contractorsData==null)
                       
                    $isAvailable = true;
                else
                     $isAvailable = false;
            }

            return \Response::json(array('valid' =>$isAvailable,));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         try {

 if (Auth::check())
  {


    if(Auth::user()->can('Delete Contractors'))
      {

         $Contractors=Contractor::find($id);
         $Contractors->delete();
         return redirect('/Contractors');

      }
    else 
      {
    return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
    }
}
