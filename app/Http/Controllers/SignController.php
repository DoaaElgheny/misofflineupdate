<?php

namespace App\Http\Controllers;

use Auth;
use App\Http\Requests;
use App\User;
use App\Gps;
use App\Contractor;
use App\Task;
use DB;
use App\Http\Controllers\Controller;
use Request;
use Excel;
use File;
use Illuminate\Support\Facades\Response;
use Validator;
use Illuminate\Support\Facades\Redirect;
use Input;
use Session;
use Geocode;
use Cache;
use URL;
use View;
use Khill\Lavacharts\Laravel\LavachartsFacade as Lava;
use Khill\Lavacharts\Lavacharts;
use TableView;
use App\Dategps;
use PHPExcel; 
use PHPExcel_IOFactory; 
use App\Marktingactitvity;
use App\SubActivity;
use App\Attribute;
use App\ActivityItem;
use App\District;
use App\Government;
use App\Event;
use App\Sign;

class SignController extends Controller
{  public function __construct()
   {
       $this->middleware('auth');
   }
        public function importsign()
    {
        $temp= Request::get('submit'); 


   if(isset($temp))

 { 


   $filename = Input::file('file')->getClientOriginalName();
     

     $Dpath = base_path();


     $upload_success =Input::file('file')->move( $Dpath, $filename);
   

       Excel::load($upload_success, function($reader)
       {   
     
         


   $results = $reader->get()->all();

       foreach ($results as $data)
        {
          $subactivityid=Session::get('subactivity_id');        
          $Sign=new Sign();
          $Sign->Code=$data["code"];
          $Sign->Government=$data["government"];
          $Sign->District=$data["district"];
          $Sign->Address=$data["address"];
          $Sign->Longitude=$data["longitude"];
          $Sign->Latitude=$data["latitude"];
          $Sign->Trader_Name=$data["trader_name"];
          $Sign->Contractor_Name=$data["contractor_name"];
          $Sign->Status=$data["status"];
          $Sign->Project_Type=$data["project_type"];
          $Sign->Date=$data["date"];
          // $Sign->Road=$data["road"];
          // $Sign->Signs_Shape=$data["signs_shape"];
          // $Sign->Type=$data["type"];
          $Sign->subactivity_id=$subactivityid;
          $Sign->save();
        
   
        }
 
      });


  }

     $subactivityid=Session::get('subactivity_id');
       return redirect('/showallactivityitems/'.$subactivityid);  
    }
    public function showsignimg($id)
    {
                try {

 if (Auth::check())
  {
      $sign=Sign::whereId($id)->first();
      return view('activityitem.ImageSign',compact('sign'));
         }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
    }
    public function deletesign($id)
  {
    try
    { 
      
      if (Auth::check())
      {
       $Sign=Sign::find($id);
          $Sign->delete();
       $subactivityid=Session::get('subactivity_id');
       return redirect('/showallactivityitems/'.$subactivityid);
      }
        else {
        return redirect('/login');
      }

      }
     catch(Exception $e)
    {
      return redirect('/login');

    }
  }
  public function store(Request $request)
    {  
       try
    { 
      
      if (Auth::check())
      {
         $subactivityid=Session::get('subactivity_id');        
          $Sign=new Sign();
          $Sign->Code=Input::get("Code");
          $Sign->Government=Input::get("Government");
          $Sign->District=Input::get("District");
          $Sign->Address=Input::get("Address");
          $Sign->Longitude=Input::get("Longitude");
          $Sign->Latitude=Input::get("Latitude");
          $Sign->Trader_Name=Input::get("Trader_Name");
          $Sign->Contractor_Name=Input::get("Contractor_Name");
          $Sign->Status=Input::get("Status");
          $Sign->Project_Type=Input::get("Project_Type");
          $Sign->Date=Input::get("Date");
          $Sign->subactivity_id=$subactivityid;
          $file_frontimg = Request::file('Img');
          if($file_frontimg!=null)
          {
                $imageName = $file_frontimg->getClientOriginalName();
                $path=public_path("/assets/dist/img/").$imageName;
                if (!file_exists($path)) {
                  $file_frontimg->move(public_path("/assets/dist/img"),$imageName);
                  $Sign->Img="/assets/dist/img/".$imageName;

                }
                else
                {
                   $random_string = md5(microtime());
                   $file_frontimg->move(public_path("/assets/dist/img"),$random_string.".jpg");
                  $Sign->Img="/assets/dist/img/".$random_string.".jpg";
                }
              }
          $Sign->save();
       return redirect('/showallactivityitems/'.$subactivityid);
      }
        else {
        return redirect('/login');
      }

      }
     catch(Exception $e)
    {
      return redirect('/login');

    }
    }
      public function Update()
    {  
       try
    { 
      
      if (Auth::check())
      {
        $id=Input::get("id");
         $subactivityid=Session::get('subactivity_id');        
          $Sign=Sign::whereId($id)->first();
          $Sign->Code=Input::get("Code");
          $Sign->Government=Input::get("Government");
          $Sign->District=Input::get("District");
          $Sign->Address=Input::get("Address");
          $Sign->Longitude=Input::get("Longitude");
          $Sign->Latitude=Input::get("Latitude");
          $Sign->Trader_Name=Input::get("Trader_Name");
          $Sign->Contractor_Name=Input::get("Contractor_Name");
          $Sign->Status=Input::get("Status");
          $Sign->Project_Type=Input::get("Project_Type");
          $Sign->Date=Input::get("Date");
          $Sign->subactivity_id=$subactivityid;
           $marktingactitvitiesid=Input::get('activity');
        $file_frontimg = Request::file('Img');
        if($file_frontimg!=null)
        {
                $imageName = $file_frontimg->getClientOriginalName();
                $path=public_path("/assets/dist/img/").$imageName;
                if (!file_exists($path)) {
                  $file_frontimg->move(public_path("/assets/dist/img"),$imageName);
                  $Sign->Img="/assets/dist/img/".$imageName;
                }
                else
                {
                   $random_string = md5(microtime());
                   $file_frontimg->move(public_path("/assets/dist/img"),$random_string.".jpg");
                  $Sign->Img="/assets/dist/img/".$random_string.".jpg";
                }
}
        
          $Sign->save();
       return redirect('/showallactivityitems/'.$subactivityid);
      }
        else {
        return redirect('/login');
      }

      }
     catch(Exception $e)
    {
      return redirect('/login');

    }
    }
    public function showupdatesign($id)
    {
        try
    { 
      
      if (Auth::check())
      {
         $Government=Government::lists('name', 'name');
         $District=District::lists('name', 'name');
         $sign=Sign::find($id);
         return view('activityitem.UpdateSign',compact('sign','Government','District')); 
      }
        else {
        return redirect('/login');
      }

      }
     catch(Exception $e)
    {
      return redirect('/login');

    } 
    }
      public function CheckCodeSign()
    {
        try
    { 
      
      if (Auth::check())
      {
         $id=Input::get('id');
      if($id==null)
      {
      
        $Code=Input::get('Code');
        $res = Sign::where('Code','=',$Code)->get(); 

        if (count($res)<= 0)
          $isAvailable = true;
        else
          $isAvailable = false;
      }
      else
      {
        $Code=Input::get('Code');
        $res = Sign::where('Code','=',$Code) 
             ->where('id','!=',$id)->get();
               if (count($res)<= 0)

        $isAvailable = true;
        else
         $isAvailable = false;

         }

    return \Response::json(array('valid' =>$isAvailable));
      }
        else {
        return redirect('/login');
      }

      }
     catch(Exception $e)
    {
      return redirect('/login');

    } 
    }
   
}
