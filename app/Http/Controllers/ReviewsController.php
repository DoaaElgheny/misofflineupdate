<?php

namespace App\Http\Controllers;

use Auth;
use App\Http\Requests;
use App\User;
use App\Gps;
use App\Task;
use DB;
use App\Http\Controllers\Controller;
use Request;
use Excel;
use File;
use Illuminate\Support\Facades\Response;
use Validator;
use Illuminate\Support\Facades\Redirect;
use Input;
use Session;
use Geocode;
use Cache;
use URL;
use View;
use Khill\Lavacharts\Laravel\LavachartsFacade as Lava;
use Khill\Lavacharts\Lavacharts;
use TableView;
use App\Dategps;
use PHPExcel; 
use PHPExcel_IOFactory; 
use App\Contractor;
use App\Review;
use App\Government;
use App\District;
use App\Product;
use Carbon;

class ReviewsController extends Controller
{ public function __construct()
   {
       $this->middleware('auth');
   }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
          try {

 if (Auth::check())
  {


    if(Auth::user()->can('Show Reviews'))
      {
       
        $Reviews=Review::where('Show',1)->get();

     

      $governments=Government::select('Name','id')->lists('Name','id');
       $districts=District::select('Name','id')->lists('Name','id');
      ;
       $Contractors=Contractor::select('Name','id')->lists('Name','id');
                    

       $product=Product::select('Cement_Name','id')->lists('Cement_Name','id');
;

    // $product=Product::select('Cement_Name','id')->lists('Cement_Name','id');

       return view('Reviews.index',compact('Reviews','Contractors','product','governments','districts'));
          }
    else 
      {
    return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */



public function uploadBackCheck()
{
    try
      {
        $temp= Request::get('BackCheck'); 

       if(isset($temp))

       { 

         $filename = Input::file('BackCheck')->getClientOriginalName();

         $Dpath = base_path();


         $upload_success =Input::file('BackCheck')->move( $Dpath, $filename);


         Excel::load($upload_success, function($reader)
         {   




          $results = $reader->get()->all();


          foreach ($results as $data)
          {
            
            $order =new dashboardcheck();
           $productcode=$data['product_code'];
           $Gpscode=$data['gps_code'];
            $SaleProductcode=$data['saleproduct_code'];
            $gpsid=Gps::where('Code',$Gpscode)->first();
            if($gpsid!=[])
         {
         if($productcode!=null )
    {
            $productN=Product::where('Code',$productcode)->pluck('id')->first();
            $order->Product_id= $productN;
            $order->Amount= $data['amount'];
             
      $order->gps_id= $gpsid->id;
      $order->Type= $data['type'];
      $order->IsDone= $data['isdone'];
      $order->Order_Status= $data['order_status'];
            $order->Notes= $data['notes'];
           
      $order->save();

}
    else
{
           
            $SalesproductN=SaleProduct::where('Code',$SaleProductcode)->first();
  $order->SaleProduct_id= $SalesproductN->id;
    $order->Amount= $data['amount'];
             $gpsid=Gps::where('Code',$Gpscode)->pluck('id')->first();
      $order->gps_id= $gpsid;
      $order->Type= $data['type'];
      $order->IsDone= $data['isDone'];
      $order->Order_Status= $data['order_status'];
            $order->Notes= $data['notes'];
          
      $order->save();

}
}

        }

      });


       }

       return back();

     }
     catch(Exception $e)
     {
         return back();
     }
}



    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {

 if (Auth::check())
  {


    if(Auth::user()->can('Store Review'))
      {
            $ReviewData = New Review;
            $ReviewData->Long=Input::get('Long');
            $ReviewData->Lat=Input::get('Lat');   
            $ReviewData->Project_NO=Input::get('Project_NO'); 
            $ReviewData->Workers=Input::get('Workers');
            $ReviewData->Cement_Consuption=Input::get('Cement_Consuption');  
            $ReviewData->Cement_Bricks=Input::get('Cement_Bricks');
            $ReviewData->Steel_Consumption=Input::get('Steel_Consumption');  
            $ReviewData->Has_Wood=Input::get('Has_Wood');
            $ReviewData->Wood_Meters=Input::get('Wood_Meters');
            $ReviewData->Wood_Consumption=Input::get('Wood_Consumption');
            $ReviewData->Has_Mixers=Input::get('Has_Mixers');
            $ReviewData->No_Of_Mixers=Input::get('No_Of_Mixers');
            $ReviewData->Capital=Input::get('Capital');
            $ReviewData->Credit_Debit=Input::get('Credit_Debit');
            $ReviewData->Has_Sub_Contractor=Input::get('Has_Sub_Contractor');
            $ReviewData->Sub_Contractor1=Input::get('Sub_Contractor1');
            $ReviewData->Sub_Contractor2=Input::get('Sub_Contractor2');
            $ReviewData->Class=Input::get('Class');
            $ReviewData->Seller1=Input::get('Seller1');
            $ReviewData->Seller2=Input::get('Seller1');
            $ReviewData->Seller3=Input::get('Seller3');
            $ReviewData->Seller4=Input::get('Seller4');
            $ReviewData->Status=Input::get('Status'); 
            $ReviewData->Call_Status=Input::get('Call_Status');
            $ReviewData->Area=Input::get('Area');
            $ReviewData->Cont_Type=Input::get('Cont_Type');
            $ReviewData->Contractor_Id=Input::get('Contractor_Id');
            $ReviewData->save(); 
            $quantity=Input::get('quantity');
            $product=Input::get('item');
            for($i=0;$i<count($product)-1;$i++)
            {
              $ReviewData->getcontractproduct()->attach($product[$i], ['Value'=>$quantity[$i]]); 
            }              
            return redirect('/Reviews');
               }
    else 
      {
    return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
    }

  public function checkcontractorexist(){
    
    $Contractorid=Input::get('Contractor_Id');
      $review=Review::where('Contractor_Id','=',$Contractorid)->first();

     if($review==[])     
        $isAvailable = true;
        else
         $isAvailable = false;

    return \Response::json(array('valid' =>$isAvailable,));
 }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     //Doaa Elgheny Function Import File
    public function importReview()
    {
           // try {

 if (Auth::check())
  {


    if(Auth::user()->can('import Review Data'))
      {

       
        try
          {
  $temp= Request::get('submit'); 
   if(isset($temp))

 { 
   $filename = Input::file('file')->getClientOriginalName();
     $Dpath = base_path();


     $upload_success =Input::file('file')->move( $Dpath, $filename);

     $hadeel=  Excel::load($upload_success, function($reader)
       { 
     
    $results = $reader->get()->all();


       foreach ($results as $data)
        {

      
        $Contractorid=Contractor::where('Code', '=',$data['code'])
                        ->select('id')->first();
                        // dd($Prmoterid->id);
        if($Contractorid!=[])
        {
        $Reviews =new Review();
        $Reviews->Long =$data['long'];
        $Reviews->Lat =$data['lat'];
        $Reviews->Project_NO =$data['project_no'];
        $Reviews->Workers =$data['workers'];
        $Reviews->Cement_Consuption =$data['cement_consuption'];

        $Reviews->Cement_Bricks =$data['cement_bricks'];
      
        $Reviews->Steel_Consumption =$data['steel_consumption'];
        $Reviews->Has_Wood =$data['has_wood'];
        $Reviews->Wood_Meters=$data['wood_meters'];
        $Reviews->Wood_Consumption=$data['wood_consumption'];
        $Reviews->Has_Mixers =$data['has_mixers'];
        $Reviews->No_Of_Mixers =$data['no_of_mixers'];
        $Reviews->Capital =$data['capital'];
 
        $Reviews->Credit_Debit =$data['credit_debit'];
        $Reviews->Has_Sub_Contractor =$data['has_sub_contractor'];

      
        $Reviews->Sub_Contractor1=$data['sub_contractor1'];
        $Reviews->Sub_Contractor2 =$data['sub_contractor2'];
        // dd($data);
        $Reviews->Class=$data['class'];
        $Reviews->Seller1 =$data['seller1'];
        $Reviews->Seller2 =$data['seller2'];
        $Reviews->Seller3 =$data['seller3'];
        $Reviews->Seller4 =$data['seller4'];

        $Reviews->Status =$data['status'];
        $Reviews->Call_Status =$data['call_status'];
        $Reviews->Area =$data['area'];
        $Reviews->Contractor_Id =$Contractorid->id;
        $Reviews->save();
    }
        }
   
    });


    }

     return redirect('/Reviews'); 
   
 }
 catch(Exception $e)
 {
//dd($e);
 }
 }
    else 
      {
   return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

// }//endtry
// catch(Exception $e) 
//     {
//     return redirect('/login');
//     }
}
    //End Import
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $Review=Review::find($id);
    
         $ContractorMain=Contractor::where('id',$Review->Contractor_Id)->first();
  //  dd($ContractorMain);
    
     //     $GovName=Government::where('Name',$ContractorMain->Government)->first();
     // //    dd($GovName);
     //     $Govid=$GovName->id;


// $DisName=District::where('Name',$ContractorMain->District)->first();
//          $disid=$DisName->id;

      $product=Product::select('Cement_Name','id')->lists('Cement_Name','id');


       $governments=Government::select('Name','id')->lists('Name','Name');
        $Contractors=Contractor::select('Name','id')->lists('Name','id');
       $districts=District::select('Name','id')->lists('Name','Name');
     
       return view('Reviews.Edit',compact('Review','governments','districts','Contractors','product','ContractorMain'));
    }


   
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
public function CalculateBackCheck()
{
      if (Auth::check())
  {

     if(Auth::user()->can('Update Review'))
      {
 $Data=[];
        $range=array( date("Y-m-d",strtotime(Input::get('Start_Date'))), date("Y-m-d",strtotime(Input::get('End_Date'))));


      $AllReview=Review::whereBetween('BackcheckDate',$range)
                ->select('UpdatedBy',DB::raw('count(Call_Status) as total'))
                ->groupBy('UpdatedBy')
                ->get();




              


             foreach ($AllReview as  $value) 
             {

           $user=User::where('id',$value->UpdatedBy)->first();

             $total=Review::whereBetween('BackcheckDate',$range)->where('Call_Status','Updated')->where('UpdatedBy',$user->id)
                ->get();

$updatedcall=count($total);
                $target=($updatedcall/$value->total)*100;

                 array_push($Data,array('label'=>$user->Username,'y'=>$target));   
             }    


$data['b']=$Data;
                   return Response::json($data,  200, [], JSON_NUMERIC_CHECK);





      }

  
   else 
      {
   return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}



}


public  function  ExportPerformanceBack(Request $request)
{




$exportbtn= input::get('submit'); 

    if(isset($exportbtn))
    { 
  
      Excel::create('PerformanceBackcheck', function($excel)
       { 

        $excel->sheet('PerformanceBackcheck',function($sheet)
        {        

          $sheet->appendRow(1, array('Name','Updated','Total',
               'Performance','Closed','Busy','Wrong','Refused','Stop'));
          $Data=[];

 $range=array( date("Y-m-d",strtotime(Input::get('Start_Date'))), date("Y-m-d",strtotime(Input::get('End_Date'))));


      $AllReview=Review::whereBetween('BackcheckDate',$range)
                ->select('UpdatedBy',DB::raw('count(Call_Status) as total'))
                ->whereIN('BackcheckDate',$range)
                ->groupBy('UpdatedBy')
                ->get();



             foreach ($AllReview as  $value) 
             {

           $user=User::where('id',$value->UpdatedBy)->first();

             $total=Review::whereBetween('BackcheckDate',$range)->where('Call_Status','Updated')->where('UpdatedBy',$user->id)
              ->whereIN('BackcheckDate',$range)
                ->get();


     $Updated=Review::where('UpdatedBy',$value->UpdatedBy)
       ->where('Call_Status','Updated')
       ->where('UpdatedBy',$value->UpdatedBy)
        ->whereIN('BackcheckDate',$range)
          ->select('*')
          ->get();
        $Updated=count($Updated);
         
    $Closedline=Review::where('Call_Status','Closed Line')
         ->where('UpdatedBy',$value->UpdatedBy)
          ->whereIN('BackcheckDate',$range)
          ->select('*')
          ->get();

          if($Closedline==null)
          {
            $Closed=0;
          }
          else
          {
            $Closed=count($Closedline);
          }

           $total=Review::where('UpdatedBy',$value->UpdatedBy)
        ->where('UpdatedBy',$value->UpdatedBy)
         ->whereIN('BackcheckDate',$range)
          ->select('*')
          ->get();
           if($total==null)
          {
            $total=0;
          }
          else
          {
            $total=count($total);
          }

        $UserBusy=Review::where('Call_Status','User Busy')
          ->where('UpdatedBy',$value->UpdatedBy)
           ->whereIN('BackcheckDate',$range)
          ->select('*')
          ->get();

          if($UserBusy==null)
          {
            $Busy=0;
          }
          else
          {
            $Busy=count($UserBusy);
          }

  $WrongNumber=Review::where('Call_Status','Wrong Number')
         ->where('UpdatedBy',$value->UpdatedBy)
          ->whereIN('BackcheckDate',$range)
          ->select('*')
          ->get();
           if($WrongNumber==null)
          {
            $Wrong=0;
          }
          else
          {
            $Wrong=count($WrongNumber);
          }
         
           $refused=Review::where('Call_Status','refused')
            ->whereIN('BackcheckDate',$range)
          ->where('UpdatedBy',$value->UpdatedBy)
          ->select('*')
          ->get();
        
           if($refused==null)
          {
            $refused=0;
          }
          else
          {
            $refused=count($refused);
          }

      $StopWork=Review::where('Call_Status','Stop Work')
       ->whereIN('BackcheckDate',$range)
        ->where('UpdatedBy',$value->UpdatedBy)
          ->select('*')
          ->get();
            if($StopWork==null)
          {
            $Stop=0;
          }
          else
          {
            $Stop=count($StopWork);
          }
        
$updatedcall=$Updated;



               
                $target=($updatedcall/$total)*100;

                 array_push($Data,array('label'=>$user->Username,'Updated'=>$Updated,'Total'=>$total,'Performance'=>$target,'Closed'=>$Closed,'Busy'=>$Busy,'Wrong'=>$Wrong,'Refused'=>$refused,'Stop'=>$Stop));   
             }  


  $sheet->fromArray($Data, null, 'A2', false, false);
});
})->download('xls');
                           
        }






}



public function ExportDilyBackcheck(Request $request)

{
    $datetimeNow=\Carbon::now();
        $datetimeNow=$datetimeNow->todatestring();

$exportbtn= input::get('submit'); 

    if(isset($exportbtn))
    { 
  
      Excel::create('PerformanceBackcheck', function($excel)
       { 

        $excel->sheet('PerformanceBackcheck',function($sheet)
        {        

          $sheet->appendRow(1, array('Name',
'Updated','Closed','total','Busy','Wrong','refused','Stop'));
  $BackCheck=[];
        $datetimeNow=\Carbon::now();
        $datetimeNow=$datetimeNow->todatestring();
     
        //Select users with sum
        $allusers=Review::where('BackcheckDate',$datetimeNow)->where('UpdatedBy','!=',0)->distinct()
          ->select('UpdatedBy')
         ->groupBy('UpdatedBy')
          ->get();




          foreach ($allusers as $value) 

          {
          
       $user=User::where('id',$value->UpdatedBy)->first();

          $Updated=Review::where('UpdatedBy',$value->UpdatedBy)
       ->where('Call_Status','Updated')
       ->where('UpdatedBy',$value->UpdatedBy)
          ->select('*')
          ->get();
        $Updated=count($Updated);
         
    $Closedline=Review::where('BackcheckDate',$datetimeNow)->where('UpdatedBy',$value->UpdatedBy)
       ->where('Call_Status','Closed Line')
         ->where('UpdatedBy',$value->UpdatedBy)
          ->select('*')
          ->get();

          if($Closedline==null)
          {
            $Closed=0;
          }
          else
          {
            $Closed=count($Closedline);
          }

           $total=Review::where('BackcheckDate',$datetimeNow)->where('UpdatedBy',$value->UpdatedBy)
        ->where('UpdatedBy',$value->UpdatedBy)
          ->select('*')
          ->get();
           if($total==null)
          {
            $total=0;
          }
          else
          {
            $total=count($total);
          }

        $UserBusy=Review::where('BackcheckDate',$datetimeNow)
        ->where('Call_Status','User Busy')
          ->where('UpdatedBy',$value->UpdatedBy)
          ->select('*')
          ->get();

          if($UserBusy==null)
          {
            $Busy=0;
          }
          else
          {
            $Busy=count($UserBusy);
          }

  $WrongNumber=Review::where('BackcheckDate',$datetimeNow)->where('Call_Status','Wrong Number')
         ->where('UpdatedBy',$value->UpdatedBy)
          ->select('*')
          ->get();
           if($WrongNumber==null)
          {
            $Wrong=0;
          }
          else
          {
            $Wrong=count($WrongNumber);
          }
         
           $refused=Review::where('BackcheckDate',$datetimeNow)->where('Call_Status','refused')
          ->where('UpdatedBy',$value->UpdatedBy)
          ->select('*')
          ->get();
        
           if($refused==null)
          {
            $refused=0;
          }
          else
          {
            $refused=count($refused);
          }

      $StopWork=Review::where('BackcheckDate',$datetimeNow)->where('Call_Status','Stop Work')
        ->where('UpdatedBy',$value->UpdatedBy)
          ->select('*')
          ->get();
            if($StopWork==null)
          {
            $Stop=0;
          }
          else
          {
            $Stop=count($StopWork);
          }
        

        array_push($BackCheck,array('Name'=>$user->Username,'Updated'=>$Updated,'Closed'=>$Closed,'Total'=>$total,'Busy'=>$Busy,'Wrong'=>$Wrong,'Refused'=>$refused,'Stop'=>$Stop));
            

          }


  $sheet->fromArray($BackCheck, null, 'A2', false, false);
});
})->download('xls');
                           
        }

}

public function ShowDashBack()
{
   
     if (Auth::check())
  {

     if(Auth::user()->can('Update Review'))
      {
         $BackCheck=[];
        $datetimeNow=\Carbon::now();
        $datetimeNow=$datetimeNow->todatestring();
     
        //Select users with sum
        $allusers=Review::where('BackcheckDate',$datetimeNow)->where('UpdatedBy','!=',0)->distinct()
          ->select('UpdatedBy')
         ->groupBy('UpdatedBy')
          ->get();




          foreach ($allusers as $value) 

          {
          
       $user=User::where('id',$value->UpdatedBy)->first();

          $Updated=Review::where('UpdatedBy',$value->UpdatedBy)
       ->where('Call_Status','Updated')
       ->where('BackcheckDate',$datetimeNow)
       ->where('UpdatedBy',$value->UpdatedBy)
          ->select('*')
          ->get();
        $Updated=count($Updated);
    
         
    $Closedline=Review::where('BackcheckDate',$datetimeNow)->where('UpdatedBy',$value->UpdatedBy)
       ->where('Call_Status','Closed Line')
         ->where('UpdatedBy',$value->UpdatedBy)
          ->select('*')
          ->get();

          if($Closedline==null)
          {
            $Closed=0;
          }
          else
          {
            $Closed=count($Closedline);
          }

           $total=Review::where('BackcheckDate',$datetimeNow)->where('UpdatedBy',$value->UpdatedBy)
        ->where('UpdatedBy',$value->UpdatedBy)
          ->select('*')
          ->get();
           if($total==null)
          {
            $total=0;
          }
          else
          {
            $total=count($total);
          }

        $UserBusy=Review::where('BackcheckDate',$datetimeNow)
        ->where('Call_Status','User Busy')
          ->where('UpdatedBy',$value->UpdatedBy)
          ->select('*')
          ->get();

          if($UserBusy==null)
          {
            $Busy=0;
          }
          else
          {
            $Busy=count($UserBusy);
          }

  $WrongNumber=Review::where('BackcheckDate',$datetimeNow)->where('Call_Status','Wrong Number')
         ->where('UpdatedBy',$value->UpdatedBy)
          ->select('*')
          ->get();
           if($WrongNumber==null)
          {
            $Wrong=0;
          }
          else
          {
            $Wrong=count($WrongNumber);
          }
         
           $refused=Review::where('BackcheckDate',$datetimeNow)->where('Call_Status','refused')
          ->where('UpdatedBy',$value->UpdatedBy)
          ->select('*')
          ->get();
        
           if($refused==null)
          {
            $refused=0;
          }
          else
          {
            $refused=count($refused);
          }

      $StopWork=Review::where('BackcheckDate',$datetimeNow)->where('Call_Status','Stop Work')
        ->where('UpdatedBy',$value->UpdatedBy)
          ->select('*')
          ->get();
            if($StopWork==null)
          {
            $Stop=0;
          }
          else
          {
            $Stop=count($StopWork);
          }
        

        array_push($BackCheck,array('Name'=>$user->Username,'Updated'=>$Updated,'Closed'=>$Closed,'Total'=>$total,'Busy'=>$Busy,'Wrong'=>$Wrong,'Refused'=>$refused,'Stop'=>$Stop));
            

          }

          return view('Reviews.BackChekDashboard',compact('BackCheck'));

      }

  
   else 
      {
   return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}


}




    public function update( $id)
    {
      
          try {

 if (Auth::check())
  {


    if(Auth::user()->can('Update Review'))
      {
        
            $ReviewData=Review::find($id);

            $ReviewData->Long=Input::get('Long');
            $ReviewData->Lat=Input::get('Lat');   
            $ReviewData->Project_NO=Input::get('Project_NO'); 
            $ReviewData->Workers=Input::get('Workers');
            $ReviewData->Cement_Consuption=Input::get('Cement_Consuption');  
            $ReviewData->Cement_Bricks=Input::get('Cement_Bricks');
            $ReviewData->Steel_Consumption=Input::get('Steel_Consumption');       
            $ReviewData->Has_Wood=Input::get('Has_Wood');
            $ReviewData->Wood_Meters=Input::get('Wood_Meters');
            $ReviewData->Wood_Consumption=Input::get('Wood_Consumption');
            $ReviewData->Has_Mixers=Input::get('Has_Mixers');
            $ReviewData->No_Of_Mixers=Input::get('No_Of_Mixers');
            $ReviewData->Capital=Input::get('Capital');
            $ReviewData->Credit_Debit=Input::get('Credit_Debit');
            $ReviewData->Has_Sub_Contractor=Input::get('Has_Sub_Contractor');
            $ReviewData->Sub_Contractor1=Input::get('Sub_Contractor1');
            $ReviewData->Sub_Contractor2=Input::get('Sub_Contractor2');
            $ReviewData->Class=Input::get('Class');
            $ReviewData->Seller1=Input::get('Seller1');
            $ReviewData->Seller2=Input::get('Seller1');
            $ReviewData->Seller3=Input::get('Seller3');
            $ReviewData->Seller4=Input::get('Seller4');
            $ReviewData->Status=Input::get('Status'); 
            $ReviewData->Call_Status=Input::get('Call_Status');
            $ReviewData->Area=Input::get('Area');
            $ReviewData->Cont_Type=Input::get('Cont_Type');
 $datetimeNow=\Carbon::now();
        $datetimeNow=$datetimeNow->todatestring();

$ReviewData->UpdatedBy=Auth::user()->id;
$ReviewData->BackcheckDate=$datetimeNow;

 
     
        //Select users with sum
      



//
$ContID=$ReviewData->Contractor_Id;


            $ReviewData->save(); 


            //Update Contractor 


 $ContractorsData=Contractor::find($ContID);

        //to get Name of districts               
       
             
            $ContractorsData->District=Input::get('district');
            $ContractorsData->Government=Input::get('Government'); 
            $ContractorsData->Address=Input::get('Address');
            $ContractorsData->Education=Input::get('Education');  
            $ContractorsData->Has_Facebook=Input::get('Has_Facebook');
            $ContractorsData->Facebook_Account=Input::get('Facebook_Account');  
            $ContractorsData->Computer=Input::get('Computer');
            $ContractorsData->Email=Input::get('Email');


            $ContractorsData->Birthday=Input::get('Birthday');
            $ContractorsData->Tele1=Input::get('Tele1');  
            $ContractorsData->Tele2=Input::get('Tele2');
            $ContractorsData->Job=Input::get('Job');
            $ContractorsData->Intership_No=Input::get('Intership_No');

            $ContractorsData->Phone_Type=Input::get('Phone_Type');  

            $ContractorsData->Nickname=Input::get('Nickname');

            $ContractorsData->Religion=Input::get('Religion');

            $ContractorsData->Home_Phone=Input::get('Home_Phone');
          //  $ContractorsData->Fame=Input::get('Fame');
            $ContractorsData->save(); 








            return redirect('/Reviews');
 }
    else 
      {
   return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
          try {

 if (Auth::check())
  {


    if(Auth::user()->can('Delete Reviews'))
      {
         $Review=Review::find($id);
         $Review->delete();
         return redirect('/Reviews');
          }
    else 
      {
    return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
    }


     public function showExportReview()
    {
          return view('Reviews.ReportReview');
    }

     public function ExportReview(Request $request)
    {


          $exportbtn= input::get('submit'); 

          if(isset($exportbtn))
          { 
        
            Excel::create('ReviewBackcheck', function($excel)
             { 

              $excel->sheet('ReviewBackcheck',function($sheet)
              {        

             $sheet->appendRow(1, array('Contractors','Education','Government','District','Address',
             'Project_NO','Workers','Cement_Consuption','Cement_Bricks','Steel_Consumption',
             'Has_Wood','Wood_Meters','Wood_Consumption','Has_Mixers','No_Of_Mixers',
             'Capital','Credit_Debit','Has_Sub_Contractor','Sub_Contractor1',
             'Sub_Contractor2','Class','Seller1','Seller2','Seller3', 'Seller4',
             'Status', 'Call_Status','Area','Cont_Type'));
                $Data=[];

       $range=array( date("Y-m-d",strtotime(Input::get('Start_Date'))), date("Y-m-d",strtotime(Input::get('End_Date'))));


            $AllReview=Review::join('contractors','contractors.id','=','reviews.Contractor_Id')
            ->whereBetween('BackcheckDate',$range)
            ->select('*')->get();


          foreach ($AllReview as $Review) 

          {
          

 array_push($Data,array(
          'Contractors'=> $Review->Name,
             'Education'=> $Review->Education,
             'Government'=>  $Review->Government,
             'District'=>$Review->District,
             'Address'=>  $Review->Address,
             'Project_NO'=>$Review->Project_NO,
             'Workers'=>$Review->Workers,
             'Cement_Consuption'=>$Review->Cement_Consuption,
             'Cement_Bricks'=> $Review->Cement_Bricks,
             'Steel_Consumption'=>$Review->Steel_Consumption,
             'Has_Wood'=>$Review->Has_Wood,
             'Wood_Meters'=>  $Review->Wood_Meters,
             'Wood_Consumption'=>$Review->Wood_Consumption,
             'Has_Mixers'=>$Review->Has_Mixers,
             'No_Of_Mixers'=>$Review->No_Of_Mixers,
             'Capital'=>$Review->Capital,
             'Credit_Debit'=>$Review->Credit_Debit,
             'Has_Sub_Contractor'=>$Review->Has_Sub_Contractor ,
             'Sub_Contractor1'=>$Review->Sub_Contractor1,
             'Sub_Contractor2'=>$Review->Sub_Contractor2,
             'Class'=>$Review->Class,
             'Seller1'=>$Review->Seller1,
             'Seller2'=>$Review->Seller2,
             'Seller3'=>$Review->Seller3,
             'Seller4'=>$Review->Seller4,
             'Status'=> $Review->Status,
             'Call_Status'=>$Review->Call_Status,
             'Area'=> $Review->Area,
             'Cont_Type'=> $Review->Cont_Type



));

 

}

  $sheet->fromArray($Data, null, 'A2', false, false);
});

})->download('xls');
                           
        }
       }
        
}
