<?php

namespace App\Http\Controllers;

use Auth;
use App\Http\Requests;
use App\User;
use App\Gps;
use App\Contractor;
use App\Task;
use DB;
use App\Http\Controllers\Controller;
use Request;
use Excel;
use File;
use Illuminate\Support\Facades\Response;
use Validator;
use Illuminate\Support\Facades\Redirect;
use Input;
use Session;
use Geocode;
use Cache;
use URL;
use View;
use Khill\Lavacharts\Laravel\LavachartsFacade as Lava;
use Khill\Lavacharts\Lavacharts;
use TableView;
use App\Dategps;
use PHPExcel; 
use PHPExcel_IOFactory; 
use App\Government;
use App\District;
use App\CompanyWarehouses;
use App\Company;
use App\FrontOffice;

class CompanyWarehousesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
   {
       $this->middleware('auth');
   }
    public function index()
    {
         try {

 if (Auth::check())
  {


    if(Auth::user()->can('Show Company Warehouses'))
      {
       
      $CompanyWarehouses=CompanyWarehouses::all();
       $governments=Government::select('Name','id')->lists('Name','id');
        $districts=District::select('Name','id')->lists('Name','id');
       $companies=Company::select('Name','id')->lists('Name','id');
       //dd($companies);
       return view('CompanyWarehouses.index',compact('CompanyWarehouses','governments','districts','companies'));

      }
    else 
      {
    return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
    }
public function updateCompanyWarehouses()
    {
         try {

 if (Auth::check())
  {


    if(Auth::user()->can('update Company Warehouses'))
      {

        $CompanyWarehousesid = Input::get('pk');   
        $column_name = Input::get('name');
        $column_value = Input::get('value');
    //if it be government or districts
       // dd(Input::get('name'));
         if (Input::get('name')=='District')
        {
          
        $District = District::where('id', '=',Input::get('value'))
                    ->select('Name')->first();
                    $column_value = $District->Name;
         }
        if (Input::get('name')=='Government')
        {
           
            $Government = Government::where('id', '=',Input::get('value'))
                    ->select('Name')->first();
                     $column_value = $Government->Name;

        }
        $CompanyWarehousesData = CompanyWarehouses::whereId($CompanyWarehousesid)->first();
        $CompanyWarehousesData-> $column_name=$column_value;

        if($CompanyWarehousesData->save())

        return \Response::json(array('status'=>1));
    else 
        return \Response::json(array('status'=>0));
      }
    else 
      {
    return \Response::json(array('status'=>'You do not have permission.'));
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
 try {

 if (Auth::check())
  {


    if(Auth::user()->can('store Company Warehouses'))
      {
          $Government=Government::whereId(Input::get('Government'))->select('Name')->first();
         $District=District::whereId(Input::get('District'))->select('Name')->first();
        $quey=CompanyWarehouses::where('company_id','=',Input::get('company_id'))->where('Government','=',$Government->Name)
        ->where('District','=',$District->Name)->first();

         if($quey==[])
        {
        $Location=Input::get('Location');
       $Address=Input::get('Address');
       
            $CompanyWarehouses = New CompanyWarehouses;
            $CompanyWarehouses->Location=$Location;
            $CompanyWarehouses->Address=$Address;  
             $CompanyWarehouses->District=$District->Name;
            $CompanyWarehouses->Government=$Government->Name; 
            $CompanyWarehouses->company_id=Input::get('company_id');
            $CompanyWarehouses->save(); 
            }
                else
        {
          $GLOBALS['company_warehouses']= array();
           array_push($GLOBALS['company_warehouses'],'The Data Already Exist');
            $UserErr =implode(" \n ",$GLOBALS['company_warehouses']);
            $UserErr = nl2br($UserErr);  
            $cookie_name = 'company_warehousesErr';
            $cookie_value = $UserErr;
            setcookie($cookie_name, $cookie_value, time() + (60), "/");
        }        
      
        return redirect('/CompanyWarehouses');
          }
    else 
      {
    return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         try {

 if (Auth::check())
  {


    if(Auth::user()->can('Delete Company Warehouses'))
      {
         $CompanyWarehouses=CompanyWarehouses::find($id);
         $CompanyWarehouses->delete();
         return redirect('/CompanyWarehouses');
            }
    else 
      {
    return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
    }
}
