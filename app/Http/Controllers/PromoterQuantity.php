<?php



namespace App\Http\Controllers;

//use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Auth;
use Session;
use App\Company;
use App\Activity;
use Image;
use App\SellerOffice;
use App\Product;
use Collection;
use PHPExcel_IOFactory;
use Excel;
use App\Promoter;
use App\Price;
use Request;
use DB;
use Exception;
use App\Government;
use App\Contractor;
use App\Gps;
class PromoterQuantity extends Controller
{
   public function __construct()
   {
       $this->middleware('auth');
   } /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {  
      try {

 if (Auth::check())
  {


    if(Auth::user()->can('show-ChartQuanyity'))
      {

     $Government=Government::select('Name','id')->lists('Name','id');
      return view('ChartQuanyity.index',compact('Government'));

      }
    else 
      {
    return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }  
      
    }
     public function showpromoterchart()
     {

$government=input::get("Government");
           $governments=DB::table('governments')
                      ->whereIn('governments.id',$government)
                       ->select('governments.Name')->lists('governments.Name');
           
//Number of Contractor Per District

         $Contractors=DB::table('contractors')
                       ->whereIn('contractors.Government',$governments)
                       ->select(DB::raw('count(contractors.id)  as count')
                       ,'contractors.District')
                       ->groupBy('contractors.District')
                       ->get();
 // dd($Contractors);
           $PromotorsContractor = DB::table('gps')
         ->join('contractors','contractors.id','=','gps.contractor_id')->distinct()
          ->whereIn('contractors.Government',$governments)
         ->select(DB::raw('count(DISTINCT(gps.contractor_id))  as count'),'contractors.District')
         ->groupBy('contractors.District')
          ->get();

           $a=[]; 
           $b=[];
         foreach ($Contractors as  $value) {


            array_push($a, array('label'=>$value->District,'y'=>$value->count));

 $j=0;
         foreach ($PromotorsContractor as  $valuepro) {


if ($value->District == $valuepro->District) {
$j=1;

            array_push($b, array('label'=>$value->District,'y'=>$valuepro->count));
          }
       
      }
if($j==0)
{
   array_push($b, array('label'=>$value->District,'y'=>null));
}
      

         
  }

        

          $data = array();
                  $data['Contractors']=$a;
                  $data['PromotorsContractor']=$b;
                 
       return Response::json($data,  200, [], JSON_NUMERIC_CHECK);
     }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
