<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Auth;
use App\User;
use App\District;
use DB;
use App\Http\Controllers\Controller;
use Request;
use Excel;
use File;
use Illuminate\Support\Facades\Response;
use Validator;
use Illuminate\Support\Facades\Redirect;
use Input;
use App\Product;
use App\Government;
use Session;
use Geocode;
use Cache;
use URL;
use View;
use Khill\Lavacharts\Laravel\LavachartsFacade as Lava;
use Khill\Lavacharts\Lavacharts;
use TableView;
use App\Detail;

class DetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
   {
       $this->middleware('auth');
   }
   
      public function showproductdetails($id)
    {
          try {

 if (Auth::check())
  {


    if(Auth::user()->can('show-details'))
      {

        Session::put('Product_id', $id);
        $details = detail::where('Product_id','=',$id)->get();
        $Government= Government::lists('Name','id');
        $government= Government::lists('Name');
        $products =product::lists('Enname', 'id');  
        $product =product::whereId($id)->lists('Cement_Name', 'id');      
        return view('detail/index',compact('details','product','Government','products','government'));   
   

      }
    else 
      {
    return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {  try {

 if (Auth::check())
  {


    if(Auth::user()->can('create-details'))
      {
        $government=Government::whereId(Input::get('Government'))->select('Name')->first();
        $districts=District::whereIn('id',Input::get('District'))->select('Name')->get();
     
         foreach ($districts as $district) {
             # code...
         
        $quey=Detail::where('Product_id','=',Input::get('product'))->where('Government','=',$government->Name)
        ->where('District','=',$district->Name)->first();
        if($quey==[])
        {
                $detail = New Detail();
                $detail->District =$district->Name;
                $detail->Government=$government->Name;
                $detail->Product_id=Input::get('product');
                $detail->Points=Input::get('Points');
                $detail->save();
        }
        else
        {
          $GLOBALS['product_details']= array();
           array_push($GLOBALS['product_details'],'The Data Already Exist');
            $UserErr =implode(" \n ",$GLOBALS['product_details']);
            $UserErr = nl2br($UserErr);  
            $cookie_name = 'product_detailsErr';
            $cookie_value = $UserErr;
            setcookie($cookie_name, $cookie_value, time() + (10), "/");
        }
    }
        $detailid=Session::get('Product_id');
        return redirect('/showproductdetails/'.$detailid);

      }
    else 
      {
    return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }

       
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
          try {

 if (Auth::check())
  {


    if(Auth::user()->can('delete-details'))
      {

        $detail=Detail::find($id);
        $detail->delete();
         $detailid=Session::get('Product_id');
         return redirect('/showproductdetails/'.$detailid);

      }
    else 
      {
    return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
      
    }
  

                     
    
    
    public function Drfupdate( )
 { 
  try {

 if (Auth::check())
  {


    if(Auth::user()->can('edit-details'))
      {

      
        $detailid = Input::get('pk');  
        $column_name = Input::get('name');
        $column_value = Input::get('value');

        $userData = Detail::whereId($detailid)->first();

        $userData-> $column_name=$column_value;

        if($userData->save())

        return \Response::json(array('status'=>1));
    else 
        return \Response::json(array('status'=>0));

      }
    else 
      {
    return \Response::json(array('status'=>'You do not have permission.'));
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }



}
}
