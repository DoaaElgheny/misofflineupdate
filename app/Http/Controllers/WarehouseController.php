<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Warehouse;
use Input;
use App\Task;
use App\User;

use Excel;
use File;
use Validator;
use DB;
use Auth;
use Symfony\Component\Debug\Exception\FatalThrowableError;
use Datatables;
use Illuminate\View\Middleware\ErrorBinder;
use Exception;
use App\Government;

use App\District;
class WarehouseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
       public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
          try {

 if (Auth::check())
  {


    if(Auth::user()->can('show-datawarehouses'))
      {

      

       $warehouse = Warehouse::all();

            $Government=Government::lists('name', 'id');
            $District=District::lists('name', 'id');
            return view('warehouse/index',compact('warehouse','Government','District'));

      }
    else 
      {
    return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
           

             }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
          try {

 if (Auth::check())
  {


    if(Auth::user()->can('create-datawarehouses'))
      {

        $Warehouse= new Warehouse;
            $Warehouse->Name =Input::get('Name');
            $Warehouse->EngName =Input::get('EName');
            $Government=Input::get('Government');
            $District=Input::get('District');
            $Districtname=District::where('id','=',$District)->first();
            $Governmentname=Government::where('id','=',$Government)->first();
            $Warehouse->Government=$Governmentname->Name;
            $Warehouse->District=$Districtname->Name;
            $Warehouse->Address =Input::get('Address');
            $Warehouse->location =Input::get('Location');

     
        $Warehouse->save();

     return redirect('/warehouse'); 

      }
    else 
      {
    return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
           
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function checkArabicnameWarehouse()
    {


        $Name=Input::get('Name');
        // dd($username);
    $res = Warehouse::where('Name','=',$Name)->select('Name')->get(); 

   if (count($res)<= 0)

        $isAvailable = true;
        else
         $isAvailable = false;

    return \Response::json(array('valid' =>$isAvailable,));
    }
public function checkEnglisnameWarehouse()
    {


        $EName=Input::get('EName');
        // dd($username);
    $res = Warehouse::select('EngName')
            ->where('EngName','=',$EName)
         
            ->get(); 

   if (count($res)<= 0)

        $isAvailable = true;
        else
         $isAvailable = false;
     
    return \Response::json(array('valid' =>$isAvailable,));
    }


    public function destroy($id)
    {
          try {

 if (Auth::check())
  {


    if(Auth::user()->can('delete-datawarehouses'))
      {

         $warehouse=Warehouse::find($id);
                    $warehouse->delete();
                    return redirect('/warehouse');

      }
    else 
      {
    return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
             
    }
    public function updatewarehouse( )
 { 
      // try {

 if (Auth::check())
  {


    if(Auth::user()->can('edit-datawarehouses'))
      {

       $warehouse = Input::get('pk');  
        $column_name = Input::get('name');
        $column_value = Input::get('value');
    //
   if (Input::get('name')=='District')
        {
          
            $District = District::where('id', '=',Input::get('value'))
                    ->select('Name')->first();
                    $column_value = $District->Name;
        }
     if (Input::get('name')=='Government')
        {
           
            $Government =Government::where('id', '=',Input::get('value'))
                    ->select('Name')->first();
                     $column_value = $Government->Name;
        }


        $userData = Warehouse::whereId($warehouse)->first();
        $userData-> $column_name=$column_value;

        if($userData->save())

        return \Response::json(array('status'=>1));
    else 
        return \Response::json(array('status'=>0));
 
    

      }
    else 
      {
   return \Response::json(array('status'=>'You do not have permission.'));
      }

   }//endAuth

else
{
return redirect('/login');
}

// }//endtry
// catch(Exception $e) 
//     {
//     return redirect('/login');
//     }

}
}
