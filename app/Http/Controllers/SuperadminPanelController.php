<?php
namespace App\Http\Controllers;
use Auth;
use App\Http\Requests;
use App\User;
use App\Gps;
use App\Contractor;
use App\Task;
use DB;
use App\Http\Controllers\Controller;
use Request;
use Excel;
use File;
use Illuminate\Support\Facades\Response;
use Validator;
use Illuminate\Support\Facades\Redirect;
use Input;
use Session;
use Geocode;
use Cache;
use URL;
use App\Sign;
use View;
use Khill\Lavacharts\Laravel\LavachartsFacade as Lava;
use Khill\Lavacharts\Lavacharts;
use TableView;
use App\Dategps;
use App\MarketingActivity;
use App\SubActivity;
use App\CafeSigns;
use App\ActivityItem;
use App\Order;
use App\SaleProduct;
use App\Product;
use App\Cafe;
use App\Company;
use App\Activity;
use App\Price;


class SuperadminPanelController extends Controller
{

  public function __construct()
   {
       $this->middleware('auth');
   }
	public function showadminpanel()
    {

    	return view('ACL.adminpanel');
    }
	
     public function importnewgps()
    {
      try
      {

        $temp= Request::get('newgps'); 


       if(isset($temp))

       { 


         $filename = Input::file('newgps')->getClientOriginalName();


         $Dpath = base_path();
         $upload_success =Input::file('newgps')->move( $Dpath, $filename);
         Excel::load($upload_success, function($reader)
         {   
          $results = $reader->get()->all();
          foreach ($results as $data)
          {
            $gps =new Gps();
            $gps->NDate =$data['date'];
            $gps->NTime =$data['time'];
            $gps->Code =$data['code'];
            $gps->Type =$data['type'];
            $gps->Lat=$data['lat'];;
            $gps->Long =$data['long'];
            $Pormoter_Id= User::where('Code',$data['user_id'])->pluck('id')->first();

            $gps->user_id =$Pormoter_Id;
            $Contractor= Contractor::where('Code',$data['contractor_code'])->first();

     
          $gps->contractor_id=$Contractor->id;
          $gps->Contractor_Code=$Contractor->Code;
          $gps->Contractor_Name=$Contractor->Name;

          $gps->save();
        }

      });


       }
 return back();

     }
     catch(Exception $e)
     {
       return back();
     }

   }
   
   public function importorder()
    {
      try
      {
        $temp= Request::get('order'); 

       if(isset($temp))

       { 

         $filename = Input::file('order')->getClientOriginalName();

         $Dpath = base_path();


         $upload_success =Input::file('order')->move( $Dpath, $filename);


         Excel::load($upload_success, function($reader)
         {   




          $results = $reader->get()->all();


          foreach ($results as $data)
          {
            
            $order =new Order();
           $productcode=$data['product_code'];
           $Gpscode=$data['gps_code'];
            $SaleProductcode=$data['saleproduct_code'];
            $gpsid=Gps::where('Code',$Gpscode)->first();
            if($gpsid!=[])
         {
         if($productcode!=null )
    {
            $productN=Product::where('Code',$productcode)->pluck('id')->first();
            $order->Product_id= $productN;
            $order->Amount= $data['amount'];
             
			$order->gps_id= $gpsid->id;
			$order->Type= $data['type'];
			$order->IsDone= $data['isdone'];
			$order->Order_Status= $data['order_status'];
            $order->Notes= $data['notes'];
           
			$order->save();

}
    else
{
           
            $SalesproductN=SaleProduct::where('Code',$SaleProductcode)->first();
  $order->SaleProduct_id= $SalesproductN->id;
    $order->Amount= $data['amount'];
             $gpsid=Gps::where('Code',$Gpscode)->pluck('id')->first();
			$order->gps_id= $gpsid;
			$order->Type= $data['type'];
			$order->IsDone= $data['isdone'];
			$order->Order_Status= $data['order_status'];
            $order->Notes= $data['notes'];
          
			$order->save();

}
}

        }

      });


       }

       return back();

     }
     catch(Exception $e)
     {
         return back();
     }

   }
    public function importcafesign()
    {
      try
      {

        $temp= Request::get('cafesign'); 
       if(isset($temp))

       { 

         $filename = Input::file('cafesign')->getClientOriginalName();
         $Dpath = base_path();
         $upload_success =Input::file('cafesign')->move( $Dpath, $filename);
         Excel::load($upload_success, function($reader)
         {   

          $results = $reader->get()->all();
          foreach ($results as $data)
          {
            $gps =new CafeSigns();
            $gps->NDate =$data['date'];
            $gps->NTime =$data['time'];
            $gps->Adress =$data['adress'];
            $gps->Status =$data['status'];
            $gps->Target =$data['target'];
            $gps->Type =$data['type'];
            $gps->Lat=$data['lat'];;
            $gps->Long =$data['long'];
            $Pormoter_Id= User::where('Code',$data['user_id'])->pluck('id')->first();
            $gps->user_id =$Pormoter_Id;
            $gps->Img=$data['img'];
            $gps->Government=$data['government'];
            $gps->District=$data['district'];
            $gps->save();
        }

      });


     }

       return back();

     }
     catch(Exception $e)
     {
         return back();
     }

   }
public function ImportActivity()
{

         
  $temp= Request::get('Activity'); 

  if(isset($temp))

 { 

   $filename = Input::file('Activity')->getClientOriginalName();
     $Dpath = base_path();


     $upload_success =Input::file('Activity')->move( $Dpath, $filename);
    

       Excel::load($upload_success, function($reader)
       {  

       $results = $reader->get()->all();

       
      
       foreach ($results as $data)
        {
    
            $Activity =new Activity();
            $Activity->Government=$data['government'];
            $Activity->District=$data['district'];
            $Activity->Start_date=$data['start_date'];
             if($data['duration']==null)
              $Activity->Duration='';
              else
            $Activity->Duration=$data['duration'];
            $Activity->Activity_type=$data['activity_type'];
            $Activity->Description=$data['description'];
            $Activity->Description=$data['quantity'];
            
            $Activity->Img=$data['img'];
            $Activity->Target_Segmentation=$data['target_segmentation'];
            $Activity->Type=$data['type'];
           
            $Activity->Sent_Date=$data['sent_date'];
            $Activity->Long=$data['long'];
            $Activity->Lat=$data['lat'];
            $companies=Company::where('Code','=',$data['company'])->pluck('id');
            $userid=User::where('Code','=',$data['user'])->pluck('id');
            $Activity->company_id=$companies[0];
            $Activity->User_id=$userid[0];
           
             $Activity->save();
             

        }
   
    });


    }

     return redirect('/showadminpanel'); 
   
 

}
public function ImportnewPrice()
{

         
  $temp= Request::get('Price'); 

  if(isset($temp))

 { 

   $filename = Input::file('Price')->getClientOriginalName();
     $Dpath = base_path();


     $upload_success =Input::file('Price')->move( $Dpath, $filename);
    

       Excel::load($upload_success, function($reader)
       {  

       $results = $reader->get()->all();

       
      
       foreach ($results as $data)
        {
          
        
            $Price =new Price();
           
            $Price->Government=$data['government'];
            $Price->District=$data['district'];
            $Price->Price=$data['price'];
            $Price->Date=$data['date'];
            $Product=Product::where('Code','=',$data['productcode'])->pluck('id');
            $userid=User::where('Code','=',$data['usercode'])->pluck('id');
            $Price->Product_id=$Product[0];
            $Price->User_id=$userid[0];
             $Price->save();
        }
   
    });


    }

     return redirect('/showadminpanel'); 
   
 

}
 public function exportsign()
   {  
    $exportbtn= input::get('submit'); 

    
  
      Excel::create('signsfile', function($excel)
       { 

        $excel->sheet('signsfile',function($sheet)
        {        

          $sheet->appendRow(1, array(
               'Code','Government','District','Lat','Long','Address','Trader_Name','Contractor_Name','Status','Date'
));
          $data=[];

 $Signs=Sign::where('Read',0)->get();


    foreach ($Signs as $Sign) {


    array_push($data,array(
      $Sign->Code,
      $Sign->Government,
      $Sign->District,
      $Sign->Latitude,
      $Sign->Longitude,
      $Sign->Address,
      $Sign->Trader_Name,
      $Sign->Contractor_Name,
      $Sign->Status,
      $Sign->Date   

      ));
  
}
foreach ($Signs as $Sign) {
     $Sign->Read=1;
     $Sign->save();
}
  $sheet->fromArray($data, null, 'A2', false, false);
});
})->download('xls');
                           
       

                          
                            }        

                             
  public function exportproduct()
   {  
    $exportbtn= input::get('product'); 

  
      Excel::create('productfile', function($excel)
       { 

        $excel->sheet('productfile',function($sheet)
        {        

          $sheet->appendRow(1, array(
               'Name','Government','District','Address','Education','Has Facebook','Facebook Account','Computer','Email','Birthday','Tele1','Tele2','Job','Intership_No','Nickname','Religion','Home_Phone','Code','Status','Fame'));
          $data=[];

 $Products=Contractor::all();


    foreach ($Products as $Product) {


    array_push($data,array(
      $Product->Name,
      $Product->Government ,
      $Product->District,
      $Product->Address,
      $Product->Education,
      $Product->Has_Facebook,
      $Product->Facebook_Account,
      $Product->Computer,
      $Product->Email,
      $Product->Birthday,
      $Product->Tele1,
      $Product->Tele2,
      $Product->Job,
      $Product->Intership_No,
      $Product->Nickname,
      $Product->Religion,
      $Product->Home_Phone,
      $Product->Code,
      $Product->Status,
      $Product->Fame

      ));
  
}
// foreach ($Products as $Product) {
//      $Product->Read=1;
//      $Product->save();
// }
  $sheet->fromArray($data, null, 'A2', false, false);
});
})->download('xls');
                           
       

                          
                             
    
}
public function ExportMultiProduct()
   {

    $exportbtn= input::get('MultiProduct');
  
  
      Excel::create('MultiProduct', function($excel)
       { 

        $excel->sheet('MultiProduct',function($sheet)
        {        

          $sheet->appendRow(1, array(
            'Name','Type','Code'
));
          $data=[];

 $SaleProduct=SaleProduct::where('Read',0)->get();


    foreach ($SaleProduct as $SalePro) {
    array_push($data,array(
      $SalePro->Name,
      $SalePro->Type,
      $SalePro->Code
      ));
   
     
}
foreach ($SaleProduct as $SalePro) {
     $SalePro->Read=1;
     $SalePro->save();
}

  $sheet->fromArray($data, null, 'A2', false, false);
});
})->download('xls');
                           
      
        
                          }
   
 public function exportlocalcompany()
   {  

    $exportbtn= input::get('submit'); 
 //   if(isset($exportbtn))
   // { 
    
      Excel::create('localcompany', function($excel)
       { 

        $excel->sheet('localcompany',function($sheet)
        {        

          $sheet->appendRow(1, array(
               'Name','EnName','Government','District','Address','Head Office Gov','Head Office Addre','Production Lines','Monthly Capacity','Code'
));
          $data=[];


 $Company=Company::all();


    foreach ($Company as $Company) {
if($Company->Read==0)
{ 

    array_push($data,array(

     
      $Company->Name,
      $Company->EnName,
      $Company->Government,
      $Company->District,
      $Company->Address,
      $Company->HeadOfficeGov,
      $Company->HeadOfficeAddre,
      $Company->ProductionLines,
      $Company->MonthlyCapacity,
      $Company->Code

      ));
    $updateread= DB::table('companies')->where('id',$Company->id)->update(array('Read' => 1));
    
    } 
}

  $sheet->fromArray($data, null, 'A2', false, false);
});
})->download('xls');
                           
       // }
        
                          
                            }                        

public function ExportCafe()
   {  
    $exportbtn= input::get('Cafe'); 

    //if(isset($exportbtn))
   // { 
  
      Excel::create('Cafefile', function($excel)
       { 

        $excel->sheet('Cafefile',function($sheet)
        {        

          $sheet->appendRow(1, array(
               'Code','Name','Address','Longitude','Latitude','Government','District'
));
          $data=[];

 $Cafes=Cafe::where('Read',0)->get();


    foreach ($Cafes as $Cafe) {


    array_push($data,array(
      $Cafe->Code,
      $Cafe->Name,
      $Cafe->Address,
      $Cafe->Long,
      $Cafe->Lat,
      $Cafe->Government,
      $Cafe->District
      ));
  
}
foreach ($Cafes as $Cafe) {
     $Cafe->Read=1;
     $Cafe->save();
}
  $sheet->fromArray($data, null, 'A2', false, false);
});
})->download('xls');
                           
       // }

                          
                             
    
}
}