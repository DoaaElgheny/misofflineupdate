<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use Excel;
use Input;
use File;
use Validator;
use DB;
use Auth;
use Symfony\Component\Debug\Exception\FatalThrowableError;
use Datatables;
use Illuminate\View\Middleware\ErrorBinder;
use Illuminate\Support\Facades\Response;
use Exception;
use App\Item;
use Image;
use Session;
use DateTime;
use Carbon;
use App\OrderItems;
use App\SubActivity;
use App\Category;
use App\Brand;
use App\Warehouse;
use App\Supplier;
class OrderItemsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
public function ChartItem()
{
  




    // try
    // {

//   $ids=explode(',',Input::get('Brand'));
// dd($ids);
    // dd($ids);
 if(in_array("all", Input::get('Brand')))
{
    
         $Items= $item=item::join('datawarehouse_items','datawarehouse_items.item_id','=','items.id')
        ->Where('datawarehouse_items.quantityinstock','!=',0)
         ->Where('datawarehouse_items.datawarehouse_id','=',Input::get('Warehouse_iD'))
       
        ->select('items.Name','datawarehouse_items.quantityinstock')
         ->where('category_id',Input::get('Catogry'))
        ->get();

}
else{

       

       $Items= $item=item::join('datawarehouse_items','datawarehouse_items.item_id','=','items.id')
        ->Where('datawarehouse_items.quantityinstock','!=',0)
        ->select('items.Name','datawarehouse_items.quantityinstock')
        ->Where('datawarehouse_items.datawarehouse_id','=',Input::get('Warehouse_iD'))
       
         ->whereIn('brand_id',Input::get('Brand'))
        ->get();
}

            $c=[];
            
           foreach ($Items as  $value) {

          
            array_push($c,array('label' =>$value->Name,'y' => $value->quantityinstock));
          
           }
              $data = array();
$data['Items']=$c;

           return Response::json($data,  200, [], JSON_NUMERIC_CHECK);
    // }
    // catch(Exception $e) 
    //     {
    //    return redirect('/login');
    //     }


}
public function UpdatePos(Request $request)
{
    try {
       $updateOrder=OrderItems::find(input::get('id'));
       $updateOrder->PosNo=input::get('PosNo');
       $updateOrder->Pos_Expense=input::get('Pos_Expense');
       $updateOrder->Res_Pos=input::get('Res_Pos');
       $updateOrder->Pos_OtherNotes=input::get('Pos_OtherNotes');
       $updateOrder->Supplier_ID=input::get('Supplier_ID');
      $updateOrder->Pos_Date=input::get('Pos_Date');
      // $updateOrder->Taxes=input::get('Taxes');
      
       
       //Image Update
  if(Input::file('pic') !=null)
{

     $file_img = Input::file('pic');
         
                $imageName = $file_img->getClientOriginalName();

                $path=public_path("/MIS/public/assets/dist/img/Po").$imageName;



                if (!file_exists($path)) {

                    // $img = \Image::make($path)->resize(200,200);
                  $file_img->move(public_path("/MIS/public/assets/dist/img/Po"),$imageName);

                  $updateOrder->Pos_Image="/MIS/public/assets/dist/img/Po/".$imageName;
                  
                }
                else
                {
                    
                   $random_string = md5(microtime());
                   // $img = \Image::make($path->getRealPath())->resize(200,200);
                   $file_img->move(public_path("/MIS/public/assets/dist/img/Po"),$random_string.".jpg");
                   
                  $updateOrder->Pos_Image="/MIS/public/assets/dist/img/Po".$random_string.".jpg";
                
                }
}
       //End Saving Image
       $updateOrder->save(); 
       return redirect('/OrderItems');  
    }
     catch (Exception $e)
    {
      return redirect('/login');  
    }
}
public function ConnectOrder($id)
{
    // try
    // {

        $OrderItems=OrderItems::where('id',$id)->first();

        $suppliers=Supplier::lists('Name','id');
     
       return view('OrderItems/ConnectOrder',compact('OrderItems','suppliers'));
        
    // }
    //  catch (Exception $e) {
        
    // }
}
public function ActivityCost()
{

$OrderItems= DB::table('items_plans')->join('sub_activities','sub_activities.id','=','items_plans.ActivityName')
      ->select(DB::raw("sub_activities.Name "),DB::raw('sum((Cost*Quantity))  as Cost'))
     ->groupBy(DB::raw("(sub_activities.Name)"))
        ->get();

            $M=[];
            
           foreach ($OrderItems as  $value) {

           
            array_push($M,array('label' => $value->Name,'y' => $value->Cost));
          
           }
              $data = array();
$data['activityBudget']=$M;

           return Response::json($data,  200, [], JSON_NUMERIC_CHECK);
}

 public function BrandItems($id)
    {
       $ids=explode(',', $id);

    // dd($ids);
 if(in_array("all", $ids))
{
        $item=item::join('datawarehouse_items','datawarehouse_items.item_id','=','items.id')
        ->Where('datawarehouse_items.quantityinstock','!=',0)
        ->select('items.Name','items.id')->get();

}
else{

       
$item=item::join('datawarehouse_items','datawarehouse_items.item_id','=','items.id')
        ->Where('datawarehouse_items.quantityinstock','!=',0)
        ->whereIn('brand_id',$ids)
        ->select('items.Name','items.id')->get();


}
        $options = array();
       
      foreach ($item as $Value) {

      $options += array($Value->id => $Value->Name);
        }
        // dd($item); 
     
        return \Response::json($options);
    }

    public function BrandCat($id)
    {
         $Brand=Brand::where('Catogry_ID','=',$id) ->get();

        $options = array();
      foreach ($Brand as $Bran) {
      $options += array($Bran->id => $Bran->Name);
        }
     
        return \Response::json($options);
    }
    public function index()
    {
        $item=Item::Lists('Name','id');
        $SubActivity=SubActivity::Lists('Name','id');
        $OrderItems=OrderItems::all();
     
       return view('OrderItems.OrderItems',compact('item','OrderItems','SubActivity'));
    }
    public function StockChart()
    {
        try
        { 


      $catogry=Category::Lists('Name','id');
      $Brand=Brand::Lists('Name','id'); 
      $Warehouses=Warehouse::select('EngName AS Name','id')->lists('Name','id');          
        $Items=item::join('datawarehouse_items','datawarehouse_items.item_id','=','items.id')
        ->Where('datawarehouse_items.quantityinstock','!=',0)->select('items.Name','items.id')->lists('items.Name','items.id');

        return view('StockReports.stockitemsChart',compact('Items','catogry','Brand','Warehouses'));
         }
       catch(Exception $e) 
        {
       return redirect('/login');
        }

    }
    public function ReportStockMoney()
    {
        try
        { 
            $Items=Item::Lists('Name','id');
            return view('StockReports.ReportOrderCost',compact('Items'));
      
         }
       catch(Exception $e) 
        {
       return redirect('/login');
        }
    }
    public function TotalitemCost()
    {

      $StartDate=Input::get('Start').'-1-1';
      $EndDate=Input::get('Start').'-12-31';
        $Start=new Carbon($StartDate);
        $End=new Carbon($EndDate);

       $OrderItems= OrderItems::select(DB::raw("MONTH(Order_Date) as Month"),DB::raw('sum((TotalCost))  as TotalCost'))
        ->whereBetween(DB::raw("Date(Order_Date)"),[$Start,$End])
        ->orderBy('Order_Date')
        ->groupBy(DB::raw("MONTH(Order_Date)"))
        ->get();


            $b=[];
            
           foreach ($OrderItems as  $value) {

            $dateObj   = DateTime::createFromFormat('!m', $value->Month);
            $monthName = $dateObj->format('F');
            array_push($b,array('label' => $monthName,'y' => $value->TotalCost));
          
           }
              $data = array();
$data['totalBudget']=$b;

           return Response::json($data,  200, [], JSON_NUMERIC_CHECK);
    }


public function CostPerItem()
{
    // try
    // {
        $StartDate=Input::get('Start2').'-1-1';
      $EndDate=Input::get('Start2').'-12-31';
        $Start=new Carbon($StartDate);
        $End=new Carbon($EndDate);
        $itemsarray=Input::get('item_id');

      // /  $dodo=Item::whereIN('id',$itemsarray)->get();
       

$OrderItems= DB::table('items_plans')->join('orderitems','orderitems.id','=','items_plans.OrderItem_Id')
       ->join('items','items.id','=','items_plans.Item_Id')

      ->select(DB::raw("items.Name as ItemName"),DB::raw('sum((Cost))  as Cost'))
     //  ->whereBetween(DB::raw("Date(Order_Date)"),[$Start,$End])
         ->whereIN('items_plans.Item_Id',$itemsarray)
      
        ->groupBy(DB::raw("items_plans.Item_Id"))
        ->get();


            $c=[];
            
           foreach ($OrderItems as  $value) {

          
            array_push($c,array('label' =>$value->ItemName,'y' => $value->Cost));
          
           }
              $data = array();
$data['totalBudgetItems']=$c;

           return Response::json($data,  200, [], JSON_NUMERIC_CHECK);
    // }
    // catch(Exception $e) 
    //     {
    //    return redirect('/login');
    //     }

}
    public function storeDetailsOrder()
    {
        try
        {

$OrderItems=OrderItems::where('id',Input::get('id'))->first(); 

$descraption=Input::get('quantity');
 $item=Input::get('item');
 $Activity=Input::get('Activity');
  $Cost=Input::get('Cost');
//Save To OrserDetails
 // dd($item,$Activity,$descraption)  ; 
$n=count($item);
 for($i=0;$i<$n-1;$i++)
{
     $OrderItems->getOrdersitem()->attach($item[$i],['Quantity' =>$descraption[$i],'ActivityName'=>$Activity[$i],'Cost'=>$Cost[$i]]);

 }

return redirect('/OrderItems'); 

        }
       catch(Exception $e) 
        {
       return redirect('/login');
        }
    }


public function OriginalPlan($id)
    {
         try
        {
            
            $items_plans=DB::table('items_plans')
         ->join('items','items.id','=','items_plans.Item_Id')
            ->where('Item_Id','=',$id)->get();
            return view('OrderItems.OriginalPlan',compact('items_plans'));

        }
       catch(Exception $e) 
        {
       return redirect('/login');
        }
      }
    public function ActualPlan($id)
    {
       try
        {
            
            $items_plans=DB::table('item_purches')
            ->join('items','items.id','=','item_purches.item_id')

            ->where('item_id','=',$id)->get();
            return view('OrderItems.ActualPlan',compact('items_plans'));
        }
       catch(Exception $e) 
        {
       return redirect('/login');
        }
    }

public function OrderItemsDropDown($id)
    {
       
        $items_plans=DB::table('items_plans')->where('Item_Id','=',$id) ->get();

        $options = array();
      foreach ($items_plans as $Value) {
      $options += array($Value->id => $Value->ActivityName);
        }
     
        return \Response::json($options);
    }

public function OrderQuantity($id,$ActivityName)
    {
       
        $items_plans=DB::table('items_plans')->where('Item_Id','=',$id) 
        ->where('id','=',$ActivityName) 
        ->first();

      
     
        return \Response::json($items_plans->Quantity );
    }
    

    public function OrderDetails($id)
    {
        try
        {
            $item=Item::Lists('Name','id');
            $id=$id;
         $ItemPlans=DB::table('items_plans')
         ->join('items','items.id','=','items_plans.Item_Id')
         ->where('OrderItem_Id',$id)->select('Name','ActivityName','Cost','Quantity','items_plans.id','items.id as itemid')->get();


         return view('OrderItems.DetailsOrderItems',compact('item','ItemPlans','id'));

        }
       catch(Exception $e) 
        {
       return redirect('/login');
        }
    }
    public function DeleteOrderItem($id)
    {   try
        {
        $updateitem=DB::table('items_plans')
        ->where('id', $id)
        ->delete();
       return redirect('/OrderItems');
        }
     catch(Exception $e) 
        {
       return redirect('/login');
        }
    }
    public function updateOrderItem()
    {
        try
        {
        $OrderItem_id= Input::get('pk');

        $column_name = Input::get('name');
        $column_value = Input::get('value');
        $OrderItem = OrderItems::whereId($OrderItem_id)->first();
        $OrderItem-> $column_name=$column_value;
        if($OrderItem->save())
        return \Response::json(array('status'=>1));
        else 
        return \Response::json(array('status'=>0));
        }
       catch(Exception $e) 
        {
       return redirect('/login');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
               try {

 if (Auth::check())
  {
    //Save To Order Items
 $OrderItems= new OrderItems;
 $OrderItems->ReasonName =Input::get('ReasonName');
  $OrderItems->TotalCost =Input::get('TotalCost');
 
 $OrderItems->Order_Date =date("Y-m-d");
  $OrderItems->save();
 $descraption=Input::get('quantity');
 $item=Input::get('item');
 $Activity=Input::get('Activity');
 $Cost=Input::get('Cost');
//Save To OrserDetails

$n=count($item);
 for($i=0;$i<$n-1;$i++)
{
     $OrderItems->getOrdersitem()->attach($item[$i],['Quantity' =>$descraption[$i],'ActivityName'=>$Activity[$i],'Cost' =>$Cost[$i]]);

 }

return redirect('/OrderItems');

  }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
     
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       try
       {
 $OrderItems=OrderItems::find($id);
         
                    $OrderItems->delete();
                    return redirect('/OrderItems');
      }//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
    }
}
