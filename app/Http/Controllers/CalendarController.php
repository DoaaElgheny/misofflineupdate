<?php

namespace App\Http\Controllers;
use Auth;
use App\Http\Requests;
use App\User;
use App\Gps;
use App\Contractor;
use DB;
use App\Http\Controllers\Controller;
use Request;
use Excel;
use File;
use Illuminate\Support\Facades\Response;
use Validator;
use Illuminate\Support\Facades\Redirect;
use Input;
use Session;
use Geocode;
use Cache;
use URL;
use View;
use TableView;
use App\Event;


class CalendarController extends Controller
{

    public function __construct()
   {
       $this->middleware('auth');
   }
public function calenderEvents()
    {
          if (Auth::check())
      {
        return view('calender/calender');
           }
        else {
        return redirect('/login');
      }
    }

public function MarketingActivity()
{
    
 
 $data = array(); //declaramos un array principal que va contener los datos
        $id = Event::all()
        ->where('Type','MarketingActivity')
        ->lists('id'); //listamos todos los id de los eventos
        Session::put('Type', 'MarketingActivity'); 
        $title = Event::all()->where('Type','MarketingActivity')
        ->lists('title'); //lo mismo para lugar y fecha
        $start = Event::all()->where('Type','MarketingActivity')
        ->lists('start');
        $end = Event::all()->where('Type','MarketingActivity')
        ->lists('end');
        $allDay = Event::all()->where('Type','MarketingActivity')
        ->lists('allDay');
        $background = Event::all()->where('Type','MarketingActivity')
        ->lists('color');
        $count = count($id); //contamos los ids obtenidos para saber el numero exacto de eventos
 
        //hacemos un ciclo para anidar los valores obtenidos a nuestro array principal $data
        for($i=0;$i<$count;$i++){
            $data[$i] = array(
                "title"=>$title[$i], //obligatoriamente "title", "start" y "url" son campos requeridos
                "start"=>$start[$i], //por el plugin asi que asignamos a cada uno el valor correspondiente
                "end"=>$end[$i],
                "allDay"=>$allDay[$i],
                "backgroundColor"=>$background[$i],
                "borderColor"=>$background[$i],
                "id"=>$id[$i]
                //"url"=>"cargaEventos".$id[$i]
                //en el campo "url" concatenamos el el URL con el id del evento para luego
                //en el evento onclick de JS hacer referencia a este y usar el método show
                //para mostrar los datos completos de un evento
            );
        }
 
        json_encode($data); //convertimos el array principal $data a un objeto Json 
       return $data; //para luego retornarlo y estar listo para consumirlo
}
public function Tasks()
{
     $data = array(); //declaramos un array principal que va contener los datos
        $id = Event::all()
        ->where('Type','Tasks')
        ->lists('id'); //listamos todos los id de los eventos
        // dd($id);
        Session::put('Type', 'Tasks'); 
        $title = Event::all()->where('Type','Tasks')
        ->lists('title'); //lo mismo para lugar y fecha
        $start = Event::all()->where('Type','Tasks')
        ->lists('start');
        $end = Event::all()->where('Type','Tasks')
        ->lists('end');
        $allDay = Event::all()->where('Type','Tasks')
        ->lists('allDay');
        $background = Event::all()->where('Type','Tasks')
        ->lists('color');
        $count = count($id); //contamos los ids obtenidos para saber el numero exacto de eventos
 
        //hacemos un ciclo para anidar los valores obtenidos a nuestro array principal $data
        for($i=0;$i<$count;$i++){
            $data[$i] = array(
                "title"=>$title[$i], //obligatoriamente "title", "start" y "url" son campos requeridos
                "start"=>$start[$i], //por el plugin asi que asignamos a cada uno el valor correspondiente
                "end"=>$end[$i],
                "allDay"=>$allDay[$i],
                "backgroundColor"=>$background[$i],
                "borderColor"=>$background[$i],
                "id"=>$id[$i]
                //"url"=>"cargaEventos".$id[$i]
                //en el campo "url" concatenamos el el URL con el id del evento para luego
                //en el evento onclick de JS hacer referencia a este y usar el método show
                //para mostrar los datos completos de un evento
            );
        }
 
        json_encode($data); //convertimos el array principal $data a un objeto Json 
       return $data; //para luego retornarlo y estar listo para consumirlo
}
public function Events()
{

  $data = array(); //declaramos un array principal que va contener los datos
        $id = Event::all()
        ->where('Type','Events')
        ->lists('id');
         Session::put('Type', 'Events'); 
        //listamos todos los id de los eventos
        $title = Event::all()->where('Type','Events')->lists('title'); //lo mismo para lugar y fecha
        $start = Event::all()->where('Type','Events')->lists('start');
        $end = Event::all()->where('Type','Events')->lists('end');
        $allDay = Event::all()->where('Type','Events')->lists('allDay');
        $background = Event::all()->where('Type','Events')->lists('color');
        $count = count($id); //contamos los ids obtenidos para saber el numero exacto de eventos
 
        //hacemos un ciclo para anidar los valores obtenidos a nuestro array principal $data
        for($i=0;$i<$count;$i++){
            $data[$i] = array(
                "title"=>$title[$i], //obligatoriamente "title", "start" y "url" son campos requeridos
                "start"=>$start[$i], //por el plugin asi que asignamos a cada uno el valor correspondiente
                "end"=>$end[$i],
                "allDay"=>$allDay[$i],
                "backgroundColor"=>$background[$i],
                "borderColor"=>$background[$i],
                "id"=>$id[$i]
                //"url"=>"cargaEventos".$id[$i]
                //en el campo "url" concatenamos el el URL con el id del evento para luego
                //en el evento onclick de JS hacer referencia a este y usar el método show
                //para mostrar los datos completos de un evento
            );
        }
 
        json_encode($data); //convertimos el array principal $data a un objeto Json 
       return $data; //para luego retornarlo y estar listo para consumirlo  
}
    public function create(){

        $Type=Session::get('Type');
       if($Type==null)
       {
        $Type="MarketingActivity";
       }

        //Valores recibidos via ajax
        $title = $_POST['title'];
        $start = $_POST['start'];
        $back = $_POST['background'];

        //Insertando evento a base de datos
        $evento=new Event;
        $evento->start=$start;
        //$evento->end=$end;
        $evento->allDay=true;
        $evento->color=$back;
        $evento->title=$title;
        $evento->Type=$Type;

        $evento->save();
        dd("kkk");
        if($Type==='Events')
        {
           return redirect('/calenderMarketingActivity');
        }
     
       else if($Type==='Tasks')
       {
        return redirect('/calenderTasks');

       }
      else
      {
        return redirect('/calenderMarketingActivity');

      }
   }

   public function update(){
        //Valores recibidos via ajax
        $id = $_POST['id'];
        $title = $_POST['title'];
        $start = $_POST['start'];
        $end = $_POST['end'];
        $allDay = $_POST['allday'];
        $back = $_POST['background'];
        
        $evento=Event::find($id);
        if($end=='NULL'){
            $evento->end=NULL;
        }else{
            $evento->end=$end;
        }
        $evento->start=$start;
        $evento->allDay=$allDay;
        $evento->color=$back;
        $evento->title=$title;
        
        //$evento->end=$end;

        $evento->save();
   }

   public function delete(){
        //Valor id recibidos via ajax
        $id = $_POST['id'];

        Event::destroy($id);
   }

}