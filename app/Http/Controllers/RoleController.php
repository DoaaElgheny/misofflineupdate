<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Auth;
use App\User;
use App\Gps;
use DB;
use App\Http\Controllers\Controller;
use Request;
use Excel;
use File;
use Illuminate\Support\Facades\Response;
use Validator;
use Illuminate\Support\Facades\Redirect;
use Input;
use App\Product;
use Session;
use Geocode;
use Cache;
use URL;
use View;
use TableView;
use App\Role;
use App\Permission;


class RoleController extends Controller
{
   public function __construct()
   {
       $this->middleware('auth');
   } /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        try {
           
          if (Auth::check()){


                
                         $permissions =Permission::lists('name', 'id');
                            $permission_all =Permission::all();
                         $roles=Role::all();
                        return view('ACL/role',compact('permissions','roles','permission_all'));

                     

            }
            
            else {
                return redirect('/login');
            }
    }
    catch(Exception $e) 
    {
        return redirect('/login');
    }
        
            }












public function role_update( )
 { 



        $users = Input::get('pk');  
        $column_name = Input::get('name');
        $column_value = Input::get('value');
    
        $userData = Role::whereId($users)->first();
        $userData-> $column_name=$column_value;

        if($userData->save())

        return \Response::json(array('status'=>1));
    else 
        return \Response::json(array('status'=>0));
 
    


}



 public function role_name()
    { 
        $Name=Input::get('name');
    $res = Role::select('name')
            ->where('name','=',$Name)
            ->get(); 
   if (count($res)<= 0)

     return \Response::json(true);
   else
  return \Response::json( false);
}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }


  public function role_permission()
    {
         $test=input::all();
         $permission=Request::get('item');
         $id=Request::get('id');
         $role = Role::whereId($id)->first();

 $Exit_permission=$role->perms()->select('id')->get();        // 
   
         if(count( $Exit_permission)!== 0 ){

             foreach ($Exit_permission as $permission_id) 
              {


                  $role->detachPermission($permission_id->id);
              }

         }

   if(count( $permission)!== 0 ){

         foreach ($permission as $permission_id) 
              {


                   $role->attachPermission($permission_id);
              }
}
           return redirect('/roles');     

    }










    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
                $role= new Role;
                $role->name =Request::get('name');
                $role->display_name =Request::get('display_name');
                $role->description =Request::get('description');
                $role->save();

             


     return redirect('/roles');     



   }

  public function chose($id)
    {

        try {
           
          if (Auth::check()){

                    $id=$id;   

        $permission=DB::table('permission_role')->select('permission_id')->where('role_id',$id)->get();

if($permission==Null){

    
                         $permissions=Permission::select('display_name')->distinct()->get();
                       
                          // dd($permissions);
                               
                          
                      $result=[];
                        $data=[];

                               foreach ($permissions as  $value) {   

                                    $permission_name=Permission::Select('*')
                                    ->where('display_name','=',$value->display_name)
                                    ->get();
                                 
                                 // dd($permission_name);

                                 foreach ($permission_name as $item) {
                                       array_push($data,array( 

                                         'id'=>$item->id,
                                        'name'=>$item->name,
                                        'checked'=>'Off' 
                                         )
                                         );                                          
                                   }
                                           

                                     
                                array_push($result,array(
                                    'Table'=>$value->display_name,
                                    'permissions' => $data
                                  )

                                                            );
                        $data=[];

                        }


$permissions= $result;





                        return view('ACL/chose',compact('permissions','id'));
} 
else
{


        $permissions=Permission::select('display_name')->distinct()->get();

                $result=[];
                 $data=[];


 foreach ($permissions as  $value) {  


                                    $permission_name=Permission::select('*')
                                    ->where('display_name','=',$value->display_name)
                                    ->get();
                                 
                       

                                    $permission_exitname=DB::table('permission_role')
                                    ->join('permissions','permissions.id','=','permission_role.permission_id')
                                    ->Select('permissions.*')
                                    ->where('permissions.display_name','=',$value->display_name)
                                    ->where('permission_role.role_id',$id)
                                    ->get();



                                        $No_all =  count($permission_name);
                                        $No_exit=  count($permission_exitname);

                         foreach ($permission_name as $item) 

                                 {
                                           $n=0;
                                           foreach ($permission_exitname as $exist_item) 
                                            {
                                                                     
                                                if($item->id===$exist_item->id)
                                                  {
                                                    
                                                    $n=1;
                                                      array_push($data,array( 
                                                                           'id'=>$item->id,
                                                                           'name'=>$item->name,
                                                                           'checked'=>'On'
                                                                            )
                                                                         );   


                                                     }
                                                 }

                                          if($n==0){
                                                     array_push($data,array( 

                                                                           'id'=>$item->id,
                                                                           'name'=>$item->name,
                                                                           'checked'=>'Off'

                                                                            )
                                                                         );
                                                                               

                                                                 }




                                }

                        

            
                                   array_push($result,array(
                                    'Table'=>$value->display_name,
                                    'permissions' => $data
                                  ));

                        $data=[];



  }

$permissions= $result;





                        return view('ACL/chose',compact('permissions','id'));

}

 }
            
            else {
                return redirect('/login');
            }
    }
    catch(Exception $e) 
    {
        return redirect('/login');
    }
        
            }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

      $permission=DB::table('permission_role')->select('permission_id')->where('role_id',$id)->get();
      // dd( $permission);
      $item_id=[];
            foreach ($permission as  $value)
             {
             array_push($item_id,$value->permission_id);
            }

  $permissions=permission::select('display_name')->whereIn('id',$item_id)->distinct()->get();

                       
                               
                          
                      $result=[];
                        $data=[];

                               foreach ($permissions as  $value) {   

                                    $permission_name=DB::table('permission_role')
                                    ->join('permissions','permissions.id','=','permission_role.permission_id')

                                    ->Select('permissions.*')
                                    ->where('permissions.display_name','=',$value->display_name)
                                    ->where('permission_role.role_id',$id)
                                    ->get();
                                 
                                 // dd($permission_name);

                                 foreach ($permission_name as $item) {
                                       array_push($data,array( 

                                         'id'=>$item->id,
                                         'name'=>$item->name 
                                         )
                                         );                                          
                                   }
                                           

                                     
                                array_push($result,array(
                                    'Table'=>$value->display_name,
                                    'permissions' => $data
                                  )

                                                            );
                        $data=[];

                        }








$permissions= $result;






        return view('ACL.show',compact('permissions'));

     }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try
        { 

            $role = Role::find($id);


            $role->delete();


        return redirect('/roles');
    }
    catch(Exception $e)
    {
            return redirect('/roles');

    }  
      }
}
