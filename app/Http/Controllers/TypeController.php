<?php

namespace App\Http\Controllers;

use Auth;
use App\Http\Requests;
use App\User;
use DB;
use App\Http\Controllers\Controller;
use Request;
use Excel;
use File;
use Illuminate\Support\Facades\Response;
use Validator;
use Illuminate\Support\Facades\Redirect;
use Input;
use Session;
use Geocode;
use Cache;
use URL;
use View;
use Khill\Lavacharts\Laravel\LavachartsFacade as Lava;
use Khill\Lavacharts\Lavacharts;
use TableView;
use App\Dategps;
use PHPExcel; 
use PHPExcel_IOFactory; 
use App\Government;
use App\District;
use App\Retailer;
use App\Type;
use App\EndType;
use App\SubType;

class TypeController extends Controller
{public function __construct()
   {
       $this->middleware('auth');
   }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try
        {
            if (Auth::check())
            {
                // if(Auth::user()->can('show-Marketing_Activity'))
                // {
                        $types=Type::all();
                        return view('types.index',compact('types'));

                // }
                // else 
                // {
                //     return view('errors.403');
                // }

            }//endAuth
            else
            {
                return redirect('/login');
            }

        }//endtry
        catch(Exception $e) 
        {
            return redirect('/login');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    public function checktypename()
    {
    
        $typename=Input::get('name');
        $type=Type::where('Name','=',$typename)->first();
        if($type==[])     
            $isAvailable = true;
        else
            $isAvailable = false;
        return \Response::json(array('valid' =>$isAvailable,));
    }
     public function checktypeengname()
    {
    
        $typeengname=Input::get('engname');
        $type=Type::where('EngName','=',$typeengname)->first();
        if($type==[])     
            $isAvailable = true;
        else
            $isAvailable = false;
        return \Response::json(array('valid' =>$isAvailable,));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try
        {
            if (Auth::check())
            {
                // if(Auth::user()->can('show-Marketing_Activity'))
                // {
                        $Type = New Type;
                        $Type->Name=Input::get('name');
                        $Type->EngName=Input::get('engname'); 
                        $Type->save();         
                        return redirect('/types');
                // }
                // else 
                // {
                //     return view('errors.403');
                // }

            }//endAuth
            else
            {
                return redirect('/login');
            }

        }//endtry
        catch(Exception $e) 
        {
            return redirect('/login');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update()
    {
        try
        {
            if (Auth::check())
            {
                // if(Auth::user()->can('show-Marketing_Activity'))
                // {
                        $Typeid = Input::get('pk');  
                        $column_name = Input::get('name');
                        $column_value = Input::get('value');   
                        $Type = Type::whereId($Typeid)->first();
                        $Type-> $column_name=$column_value;
                        if($Type->save())
                            return \Response::json(array('status'=>1));
                        else 
                            return \Response::json(array('status'=>0));

                // }
                // else 
                // {
                //     return \Response::json(array('status'=>'You do not have permission.'));
                // }

            }//endAuth
            else
            {
                return redirect('/login');
            }

        }//endtry
        catch(Exception $e) 
        {
            return redirect('/login');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try
        {
            if (Auth::check())
            {
                // if(Auth::user()->can('show-Marketing_Activity'))
                // {
                        $Type=Type::find($id);
                        $Type->delete();
                        return redirect('/types');
                // }
                // else 
                // {
                //     return view('errors.403');
                // }

            }//endAuth
            else
            {
                return redirect('/login');
            }

        }//endtry
        catch(Exception $e) 
        {
            return redirect('/login');
        }
    }
}
