<?php

namespace App\Http\Controllers;

use App\sale_product;
use Auth;
use App\Http\Requests;
use App\User;
use App\Gps;
use DB;
use App\Http\Controllers\Controller;
use Request;
use Excel;
use File;
use Illuminate\Support\Facades\Response;
use Validator;
use Illuminate\Support\Facades\Redirect;
use Input;


class sale_productController extends Controller
{
   public function __construct()
   {
       $this->middleware('auth');
   } /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
          try {

 if (Auth::check())
  {


    if(Auth::user()->can('show-sale_products'))
      {

        $sale_products= sale_product::all();         
         return view('sale_products.index',compact('sale_products'));  

      }
    else 
      {
    return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
      
    }
      public function updatesaleproduct()
    {
  try {

 if (Auth::check())
  {


    if(Auth::user()->can('edit-sale_products'))
      {

      $sale_productid = Input::get('pk');  
        $column_name = Input::get('name');
        $column_value = Input::get('value');
    
        $category = sale_product::whereId($sale_productid)->first();
        $category-> $column_name=$column_value;

        if($category->save())

        return \Response::json(array('status'=>1));
    else 
        return \Response::json(array('status'=>0));

      }
    else 
      {
     return \Response::json(array('status'=>'You do not have permission.'));
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function checksaleProductCode()
    {
       $saleProductcode=Input::get('Code');
      $sale_product = sale_product::where('Code','=',$saleProductcode)->first();
    
     if($sale_product==null)     
        $isAvailable = true;
        else
         $isAvailable = false;

    return \Response::json(array('valid' =>$isAvailable,));


       
    }

    public function store(Request $request)
    {
          try {

 if (Auth::check())
  {


    if(Auth::user()->can('create-sale_products'))
      {

        $sale_productname=Input::get('name');
        $sale_productengname=Input::get('Engname');
        $Code=Input::get('Code');
        $sale_product = New sale_product;
        $sale_product->Name=$sale_productname; 
        $sale_product->EngName=$sale_productengname;
        $sale_product->Code=$Code;
        $sale_product->Type="Sale"; 
        $sale_product->save();         
        
        return redirect('/sale_products');

      }
    else 
      {
    return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
      
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }
     public function checknamesaleproduct()
    {
              try {

 if (Auth::check())
  {

      $sale_productname=Input::get('name');
      $sale_product = sale_product::where('Name','=',$sale_productname)
     ->first();
     if($sale_product==[])     
        $isAvailable = true;
        else
         $isAvailable = false;

    return \Response::json(array('valid' =>$isAvailable,));
       }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
 }

     public function checkenglishnamesaleproduct()
 {
              try {

 if (Auth::check())
  {

    $sale_productname=Input::get('Engname');

      $sale_product = sale_product::where('EngName','=',$sale_productname)
     ->first();
     if($sale_product==[])     
        $isAvailable = true;
        else
         $isAvailable = false;

    return \Response::json(array('valid' =>$isAvailable,));
       }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
 }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
          try {

 if (Auth::check())
  {


    if(Auth::user()->can('delete-sale_products'))
      {

       $sale_product=sale_product::find($id);
        $sale_product->delete();
        return redirect('/sale_products'); 

      }
    else 
      {
    return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
          }
}
