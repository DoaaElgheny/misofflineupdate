<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Http\Requests;
use App\User;
use App\Gps;
use App\Contractor;
use App\Task;
use DB;
use App\Http\Controllers\Controller;

use Excel;
use File;
use Illuminate\Support\Facades\Response;
use Validator;
use Illuminate\Support\Facades\Redirect;
use Input;
use Session;
use Geocode;
use Cache;
use URL;
use View;
use Khill\Lavacharts\Laravel\LavachartsFacade as Lava;
use Khill\Lavacharts\Lavacharts;
use TableView;
use App\Dategps;
use PHPExcel; 
use PHPExcel_IOFactory; 
use App\Government;
use App\District;
use App\Company;
use App\Product;
use App\FrontOffice;
use App\Activity;
class CompaniesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
   {
       $this->middleware('auth');
   }
    public function index()
    {
         try {

 if (Auth::check())
  {
    if(Auth::user()->can('show-companies'))
      {
      $Companies=Company::all();
      $Government=Government::lists('name', 'name');
      $District=District::lists('name', 'name');
      return view('sediemenu.inventory',compact('Companies','Government','District'));
      }
    else 
      {
    return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
     
    }
      public function ShowCompanyDeteils($compid)
    {
          try {

 if (Auth::check())
  {


    if(Auth::user()->can('show-companiesDetails'))
      {

      
      $Companyname=Company::whereId($compid)->select('EnName')->first();
      $Products=Product::where('Company_id','=',$compid)->select('Cement_Name','id','Frontimg','Backimg')->get();
      $Activity= Activity::where('company_id','=',$compid)->select('*')->get();
      $sellerofices=FrontOffice::where('company_id','=',$compid)->select('*')->get();
      return view('Companies.Details',compact('Products','sellerofices','Companyname','Activity'));

      }
    else 
      {
    return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
 public function checkCompanyCode()
    {
       $Companycode=Input::get('Code');
      $Company = Company::where('Code','=',$Companycode)->first();
     if($Company==null)     
        $isAvailable = true;
        else
         $isAvailable = false;

    return \Response::json(array('valid' =>$isAvailable,));


       
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
  try {

 if (Auth::check())
  {


    if(Auth::user()->can('create-companies'))
      {

      
       
            $Company = New Company;

            $Company->Name=Input::get('Name');
            $Company->EnName=Input::get('EnName');  
            $Company->Code=Input::get('Code'); 
            $Company->Address=Input::get('Address');
            $Company->Government=Input::get('Government'); 
            $Company->District=Input::get('District');

            $Company->HeadOfficeGov=Input::get('HeadOfficeGov'); 
            $Company->HeadOfficeAddre=Input::get('HeadOfficeAddre');
            $Company->ProductionLines=Input::get('ProductionLines');
            $Company->MonthlyCapacity=Input::get('MonthlyCapacity');
            $Company->save();         
      
        return redirect('/Companies');

      }
    else 
      {
    return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
       
    }
    public function checkCompanyEngName()
    {
         $companiesname=Input::get('EnName');
      $companies =Company::where('companies.EnName','=',$companiesname)
     ->select('*')->first();
     if($companies==null)     
        $isAvailable = true;
        else
         $isAvailable = false;

    return \Response::json(array('valid' =>$isAvailable,));
    }

public function checkCompanyName()
    {
         $companiesname=Input::get('Name');
      $companies = Company::where('companies.Name','=',$companiesname)
     ->select('*')->first();
     if($companies==null)     
        $isAvailable = true;
        else
         $isAvailable = false;

    return \Response::json(array('valid' =>$isAvailable,));
    }
    public function updateCompanies()
    {
          try {

 if (Auth::check())
  {


    if(Auth::user()->can('edit-companies'))
      {

     $Companiesid = Input::get('pk');

        $column_name = Input::get('name');
        $column_value = Input::get('value');
    //if it be government or districts
     
        //  if (Input::get('name')=='District')
        // {
          
        // $District = DB::table('districts')
        //             ->where('id', '=',Input::get('value'))
        //             ->select('Name')->first();
        //             $column_value = $District->Name;
        //  }
        // if (Input::get('name')=='Government')
        // {
        //     dd(Input::get('value'));
        //     $Government = DB::table('governments')
        //             ->where('id', '=',Input::get('value'))
        //             ->select('Name')->first();
        //              $column_value = $Government->Name;
        //              dd($Government);

        // }
      
        $CompaniesData = Company::whereId($Companiesid)->first();
        $CompaniesData-> $column_name=$column_value;

        if($CompaniesData->save())

        return \Response::json(array('status'=>1));
    else 
        return \Response::json(array('status'=>0));

      }
    else 
      {
   return \Response::json(array('status'=>'You do not have permission.'));
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }

        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
          try {

 if (Auth::check())
  {


    if(Auth::user()->can('delete-companies'))
      {

          $Companies=Company::find($id);
         
         $Companies->delete();
        return redirect('/Companies');

      }
    else 
      {
    return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
    
    }
}
