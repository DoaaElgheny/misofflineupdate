<?php

namespace App\Http\Controllers;

use Auth;
use App\Http\Requests;
use App\User;
use App\Gps;
use App\Contractor;
use App\Task;
use App\sale_product_details;
use App\sale_product;
use DB;
use App\Http\Controllers\Controller;
use Request;
use Excel;
use File;
use Illuminate\Support\Facades\Response;
use Validator;
use Illuminate\Support\Facades\Redirect;
use Input;
use Session;
use Geocode;
use Cache;
use URL;
use View;
use Khill\Lavacharts\Laravel\LavachartsFacade as Lava;
use Khill\Lavacharts\Lavacharts;
use TableView;
use App\Dategps;
use PHPExcel; 
use PHPExcel_IOFactory; 
use App\Order;
use App\Product;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

public function __construct()
   {
       $this->middleware('auth');
   }
public function indexContractors_Program()
    {
          try {

 if (Auth::check())
  {


    if(Auth::user()->can('show-orders'))
      {
        Session::put('comefrom','Contractors_Program');
       $sale_product = sale_product::all()->lists('Name','id');
        $Product = Product::all()->lists('Enname','id');
        $gps = Gps::all()->lists('Adress','id'); 
        $orders=Order::select('orders.*','gps.Code','users.Name')
        ->join('gps','gps.id','=','orders.gps_id')
        ->join('users','users.id','=','gps.user_id')
         ->where('orders.Type','حصر كميات')
        ->get();
        // $orders=DB::table('orders')->join('gps','gps.id','=','orders.gps_id')->
        // // ->join('products','products.id','=','orders.')
        // lists()
        $header=false;

        return view('orders.index',compact('sale_product','Product','gps','orders','header')); 

      }
    else 
      {
    return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
         
    }






























    public function index()
    {
          try {

 if (Auth::check())
  {


    if(Auth::user()->can('show-orders'))
      {
        Session::put('comefrom','index');
       $sale_product = sale_product::all()->lists('Name','id');
        $Product = Product::all()->lists('Enname','id');
        $gps = Gps::all()->lists('Adress','id'); 
        $orders=Order::select('orders.*','gps.Contractor_Name','users.Name')
        ->join('gps','gps.id','=','orders.gps_id')
        ->join('users','users.id','=','gps.user_id')
         ->join('contractors','contractors.id','=','gps.contractor_id')
        ->where('orders.Type','المبيعات')
        ->get();
        // $orders=DB::table('orders')->join('gps','gps.id','=','orders.gps_id')->
        // // ->join('products','products.id','=','orders.')
        // lists()
        $header=true;

        return view('orders.index',compact('sale_product','Product','gps','orders','header')); 

      }
    else 
      {
    return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
         
    }
     public function updateactiveorder()
    {
          try {

 if (Auth::check())
  {


    if(Auth::user()->can('edit-orders'))
      {

       $orderid = Input::get('id'); 
        $active= Input::get('active');
        
        $order= Order::whereId($orderid)->first();
        $order->IsDone=$active;

        if($order->save())

          return \Response::json(array('status'=>1));
        else 
          return \Response::json(array('status'=>0));


      }
    else 
      {
     return \Response::json(array('status'=>'You do not have permission.'));
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
       
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $order = New Order();
        $order->SaleProduct_id =Input::get('sale_product');
        $order->Product_id =Input::get('Product');
        $order->gps_id=Input::get('gps');
        $order->Amount =Input::get('Amount');
        $order->Type =Input::get('Type');
        $order->Order_Status =Input::get('Order_Status');
        $order->Notes =Input::get('Notes');
        $order->IsDone=0;
        $order->save();
        return redirect('/orders');
    }
     public function updateorder()
    {
                try {

 if (Auth::check())
  {


    if(Auth::user()->can('edit-orders'))
      {

      
        $order_id= Input::get('pk');  
        $column_name = Input::get('name');
        $column_value = Input::get('value');
    
        $gpsData = Order::whereId($order_id)->first();
        $gpsData-> $column_name=$column_value;

        if($gpsData->save())

            return \Response::json(array('status'=>1));
        else 
            return \Response::json(array('status'=>0));

      }
    else 
      {
     return \Response::json(array('status'=>'You do not have permission.'));
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
          try {

 if (Auth::check())
  {


    if(Auth::user()->can('delete-orders'))
      {
        
      $order=Order::find($id);
        $order->delete(); 
        $comefrom=Session::get('comefrom');
        if($comefrom=="index")
        return redirect('/orders'); 
      else
        return redirect('/Contractors_Program'); 

      }
    else 
      {
    return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
       
    
   }
}
