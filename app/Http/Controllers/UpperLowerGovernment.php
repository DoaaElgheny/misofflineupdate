<?php

namespace App\Http\Controllers;

//use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use Auth;
use Session;
use App\Company;
use App\Activity;
use Image;
use App\SellerOffice;
use App\Product;
use Collection;
use PHPExcel_IOFactory;
use Excel;
use App\Promoter;
use App\Price;
use Request;
use App\Government;
use DB;
use Exception;
use App\Order;

class UpperLowerGovernment extends Controller
{public function __construct()
   {
       $this->middleware('auth');
   }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
          try {

 if (Auth::check())
  {


    if(Auth::user()->can('show-ChartUpper'))
      {

     return view('ChartQuanyity.LowerUpperGov');

      }
    else 
      {
    return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
        
    }
     public function showupperchart()
     {
        
        $range=array(input::get("StartDate"),input::get("EndDate"));
         $UpperQuantity = Order::join('gps','gps.id','=','orders.gps_id')
         ->join('users','users.id','=','gps.user_id')
          ->join('governments','governments.Name','=','users.Government')
         ->select(DB::raw('users.Government '), DB::raw('sum(orders.Amount) As Amount'))
          ->where ('governments.GType','=','Upper')
           ->whereBetween('gps.NDate',$range)
         ->where('orders.IsDone',1)
         ->groupBy(DB::raw('users.Government'))
         ->get();
       
            $c=[];
         foreach ($UpperQuantity as  $value) {
            array_push($c, array('label'=>$value->Government,'y'=>$value->Amount));
         }
       return Response::json($c,  200, [], JSON_NUMERIC_CHECK);
     }
       public function showulowerchart()
     {
       
        $range=array(input::get("StartDate"),input::get("EndDate"));
         $LowerQuantity =Order::join('gps','gps.id','=','orders.gps_id')
         ->join('users','users.id','=','gps.user_id')
           ->join('governments','governments.Name','=','users.Government')
         ->select(DB::raw('users.Government '), DB::raw('sum(orders.Amount) As Amount'))
           ->where ('governments.GType','=','Lower')
           ->whereBetween('gps.NDate',$range)
          ->where('orders.IsDone',1)
         ->groupBy(DB::raw('users.Government'))
         ->get();
         // dd("hjgh",$LowerQuantity);
            $c=[];
           
         foreach ($LowerQuantity as  $value) {
            array_push($c, array('label'=>$value->Government,'y'=>$value->Amount));
         }
       return Response::json($c,  200, [], JSON_NUMERIC_CHECK);
     }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
