<?php

namespace App\Http\Controllers;

use Auth;
use App\Http\Requests;
use App\User;
use App\Gps;
use App\Task;
use DB;
use App\Http\Controllers\Controller;
use Request;
use Excel;
use File;
use Illuminate\Support\Facades\Response;
use Validator;
use Illuminate\Support\Facades\Redirect;
use Input;
use Session;
use Geocode;
use Cache;
use URL;
use View;
use Khill\Lavacharts\Laravel\LavachartsFacade as Lava;
use Khill\Lavacharts\Lavacharts;
use TableView;
use App\Dategps;
use PHPExcel; 
use PHPExcel_IOFactory; 
use App\Government;
use App\District;
use App\CompanyWarehouses;
use App\Company;
use App\Contractor;
use App\Supplier;

class SupplierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
       public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
          try {

 if (Auth::check())
  {


    if(Auth::user()->can('show-datawarehouses'))
      {

      

            $Supplier = Supplier::all();
            return view('Supplier/index',compact('Supplier'));

      }
    else 
      {
    return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
           

             }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

 public function importSupplier()
    {
 


   $filename = Input::file('file')->getClientOriginalName();
     $Dpath = base_path();


     $upload_success =Input::file('file')->move( $Dpath, $filename);

       Excel::load($upload_success, function($reader)
       {   
    $results = $reader->get()->all();


       foreach ($results as $data)
        {

$Supplier =New Supplier;       
$Supplier->companyName =$data['companyname'];
$Supplier->Spcailzation =$data['specialization'];
$Supplier->credtional =$data['credentials'];  
$Supplier->Fax =$data['fax'];

$Supplier->landline1 =$data['landline1'];
$Supplier->landline2 =$data['landline2'];
$Supplier->landline3=$data['landline3'];
$Supplier->Whatsapp=$data['whatsapp'];

$Supplier->ownerName =$data['ownername'];  
$Supplier->Name=$data['name'];
$Supplier->Tele=$data['tele'];
$Supplier->adress=$data['address'];

$Supplier->Email=$data['email'];
$Supplier->website=$data['website'];
$Supplier->save();

      }
   
    });



     return redirect('/Supplier'); 
   

}
  




    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
          try {

 if (Auth::check())
  {


    if(Auth::user()->can('create-datawarehouses'))
      {

        $Supplier= new Supplier;
            $Supplier->Name =Input::get('Name');
            $Supplier->Email =Input::get('Email');
            $Supplier->Tele =Input::get('Tele');
     
           $Supplier->save();

     return redirect('/Supplier'); 

      }
    else 
      {
    return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
           
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   



    public function destroy($id)
    {
          try {

 if (Auth::check())
  {


    if(Auth::user()->can('delete-datawarehouses'))
      {

         $Supplier=Supplier::find($id);
                    $Supplier->delete();
                    return redirect('/Supplier');

      }
    else 
      {
    return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
             
    }
    public function updateSupplier( )
 { 
      // try {

 if (Auth::check())
  {


    if(Auth::user()->can('edit-datawarehouses'))
      {

       $Supplier = Input::get('pk');  
        $column_name = Input::get('name');
        $column_value = Input::get('value');
 


        $SupplierData = Supplier::whereId($Supplier)->first();
        $SupplierData-> $column_name=$column_value;

        if($SupplierData->save())

        return \Response::json(array('status'=>1));
    else 
        return \Response::json(array('status'=>0));
 
    

      }
    else 
      {
   return \Response::json(array('status'=>'You do not have permission.'));
      }

   }//endAuth

else
{
return redirect('/login');
}

// }//endtry
// catch(Exception $e) 
//     {
//     return redirect('/login');
//     }

}
}
