<?php

namespace App\Http\Controllers;

use Auth;
use App\Http\Requests;
use App\User;
use App\Gps;
use App\Contractor;
use App\Task;
use App\sale_product_details;
use App\sale_product;
use DB;
use App\Http\Controllers\Controller;
use Request;
use Excel;
use File;
use Illuminate\Support\Facades\Response;
use Validator;
use Illuminate\Support\Facades\Redirect;
use Input;
use Session;
use Geocode;
use Cache;
use URL;
use View;
use Khill\Lavacharts\Laravel\LavachartsFacade as Lava;
use Khill\Lavacharts\Lavacharts;
use TableView;
use App\Dategps;
use PHPExcel; 
use PHPExcel_IOFactory; 
use App\Marktingactitvity;
use App\Subactitvity;
use App\Government;
use App\ActivityItem;

class sale_product_detailsController extends Controller
{
    public function __construct()
   {
       $this->middleware('auth');
   }
   /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function index()
    // {
    //     //
    // }
public function saleproductdetails($id)
    {
          try {

 if (Auth::check())
  {


    if(Auth::user()->can('show-sale_product__details'))
      {

         Session::put('sale_productid', $id);
        $sale_product = sale_product::whereId($id)->lists('Name','id');
        $sale_productdetials = sale_product_details::where('SaleProduct_id','=',$id)->get();
        $Sale_products = sale_product::all()->lists('Name','id');  
        $government= Government::all()->lists('Name'); 
         $governments= Government::all()->lists('Name','id');   
        return view('sale_product_details.index',compact('sale_product','sale_productdetials','Sale_products','government','governments'));   
   

      }
    else 
      {
    return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
      }
    public function updateactivesaleproduct()
    {
          try {

 if (Auth::check())
  {

  
    if(Auth::user()->can('edit-sale_product__details'))
      {

      $sale_productdetialsid = Input::get('id'); 
        $active= Input::get('active');
        
        $sale_productdetials= sale_product_details::whereId($sale_productdetialsid)->first();
        $sale_productdetials->Available=$active;
        if($sale_productdetials->save())

          return \Response::json(array('status'=>1));
        else 
          return \Response::json(array('status'=>0));

      }
    else 
      {
      return \Response::json(array('status'=>'You do not have permission.'));
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
        

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
          try {

 if (Auth::check())
  {


    if(Auth::user()->can('create-sale_product__details'))
      {
        $government=Government::whereId(Input::get('government'))->select('Name')->first();
        $quey=sale_product_details::where('SaleProduct_id','=',Input::get('sale_product'))->where('Government','=',$government->Name)->first();
        if($quey==[])
        {
          $sale_product_detials = New sale_product_details();
          $sale_product_detials->SaleProduct_id =Input::get('sale_product');
          $sale_product_detials->Government=$government->Name;
          $sale_product_detials->Available=1;
          $sale_product_detials->save();
        }
        else
        {
          $GLOBALS['sale_product_details']= array();
           array_push($GLOBALS['sale_product_details'],'The Data Already Exist');
            $UserErr =implode(" \n ",$GLOBALS['sale_product_details']);
            $UserErr = nl2br($UserErr);  
            $cookie_name = 'sale_product_detailsErr';
            $cookie_value = $UserErr;
            setcookie($cookie_name, $cookie_value, time() + (10), "/");
        }
        $saleid=Session::get('sale_productid');
        return redirect('/saleproductdetails/'.$saleid);

      }
    else 
      {
    return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
     
        
   }
  public function updateSaleproductdetails()
    {
  try {

 if (Auth::check())
  {

    if(Auth::user()->can('edit-sale_product__details'))
      {

        $Sale_product_details_id= Input::get('pk');  
        $column_name = Input::get('name');
        $column_value = Input::get('value');
    
        $gpsData = sale_product_details::whereId($Sale_product_details_id)->first();
        $gpsData-> $column_name=$column_value;

        if($gpsData->save())

            return \Response::json(array('status'=>1));
        else 
            return \Response::json(array('status'=>0));

      }
    else 
      {
    return \Response::json(array('status'=>'You do not have permission.'));
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }

      

    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
           try {

 if (Auth::check())
  {


    if(Auth::user()->can('delete-sale_product__details'))
      {

        $sale_productdetails=sale_product_details::find($id);
        $sale_productdetails->delete();  
         $saleid=Session::get('sale_productid');
        return redirect('/saleproductdetails/'.$saleid);  

      }
    else 
      {
    return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
      
           }
}
