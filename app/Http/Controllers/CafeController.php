<?php

namespace App\Http\Controllers;

use Auth;
use App\Http\Requests;
use App\User;
use App\Gps;
use App\Contractor;
use App\Task;
use App\sale_product_details;
use App\sale_product;
use DB;
use App\Http\Controllers\Controller;
use Request;
use Excel;
use File;
use Illuminate\Support\Facades\Response;
use Validator;
use Illuminate\Support\Facades\Redirect;
use Input;
use Session;
use Geocode;
use Cache;
use URL;
use View;
use Khill\Lavacharts\Laravel\LavachartsFacade as Lava;
use Khill\Lavacharts\Lavacharts;
use TableView;
use App\Dategps;
use PHPExcel; 
use App\Cafe;
use PHPExcel_IOFactory; 
use App\Kpi;
use App\Sign;
use App\users;
use App\Government;
use App\CafeSigns;
use App\District;





class CafeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
   {
       $this->middleware('auth');
   }
    public function index()
    {
         try {

 if (Auth::check())
  {


    // if(Auth::user()->can('show-cafe'))
    //   {
            $cafe=Cafe::all();
            $Government=Government::lists('name', 'id');
            $District=District::lists('name', 'id');
            return view('cafes.index',compact('cafe','Government','District'));

   // }
   //  else 
   //    {
   //  return view('errors.403');
   //    }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
    }

public function importCafe()
  {
    try
    {

  $temp= Request::get('submit'); 


   if(isset($temp))

 { 


   $filename = Input::file('file')->getClientOriginalName();
     

     $Dpath = base_path();


     $upload_success =Input::file('file')->move( $Dpath, $filename);
    

       Excel::load($upload_success, function($reader)
       {   
     
         


      $results = $reader->get()->all();


       foreach ($results as $data)
        {
        
$cafes =new Cafe();
$cafes->District =$data['district'];
$cafes->Name =$data['name'];
$cafes->Government=$data['government'];
$adress =$data['gps'];
$cafes->Adress =$data['adress'];

 if($adress=="")
     {
       $cafes->Long ="";
       $cafes->Lat ="";
     }
     else{
    
        preg_match_all('!\d+(?:\.\d+)?!', $adress, $matches);
      $floats = array_map('floatval', $matches[0]);

     $cafes->Long =$floats[1];
     $cafes->Lat =$floats[0];
     }
   // dd($data['name'],$Contractor_Id);
$cafes->save();
       
        }
   
    });


  }

   return redirect('/cafes'); 
   
 }
 catch(Exception $e)
 {
   return redirect('/cafes'); 
 }

}






























    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }




    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
  try {

 if (Auth::check())
  {


    if(Auth::user()->can('store-cafe'))
      {           $cafe = new Cafe;

        $Address =input::get('Address');
         $Code =input::get('Code');
         $cafe->Code = $Code;
         $cafe->Adress = $Address;
   
        preg_match_all('!\d+(?:\.\d+)?!', $Address, $matches);
      $floats = array_map('floatval', $matches[0]);
      if($floats !=Null)
      {
         $cafe->Lat =$floats[0];

     $cafe->Long =$floats[1];
      }
    
   
 $Government=Input::get('Government');
   $District=Input::get('District');
 $Districtname=District::where('id','=',$District)->first();
 $Governmentname=Government::where('id','=',$Government)->first();
$cafe->Government=$Governmentname->Name;
$cafe->District=$Districtname->Name;

$cafe->save();
        return redirect('/cafes'); 

       
     }
    else 
      {
    return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
    }
    public function fupdatecafe()
    {
        try {

 if (Auth::check())
  {


    if(Auth::user()->can('edit-cafe'))
      {

       $cafe = Input::get('pk');  
        $column_name = Input::get('name');
        $column_value = Input::get('value');
    //
           if (Input::get('name')=='Adress')
        {
            $add=Input::get('value');
   
        preg_match_all('!\d+(?:\.\d+)?!', $add, $matches);
      $floats = array_map('floatval', $matches[0]);
     $Lat =$floats[0];

     $Long =$floats[1];
     $updateLongLat=Cafe::where('id', $cafe)
          
            ->update(['Adress'=>$add,'Lat' =>$Lat,'Long'=>$Long]);

        }
   if (Input::get('name')=='District')
        {
          
            $District =District::where('id', '=',Input::get('value'))
                    ->select('Name')->first();
                    $column_value = $District->Name;
        }
     if (Input::get('name')=='Government')
        {
           
            $Government = Government::where('id', '=',Input::get('value'))
                    ->select('Name')->first();
                     $column_value = $Government->Name;
        }


        $userData = Cafe::whereId($cafe)->first();
        $userData-> $column_name=$column_value;

        if($userData->save())

        return \Response::json(array('status'=>1));
     else 
        return \Response::json(array('status'=>0));
 
    

      }
    else 
      {
   return \Response::json(array('status'=>'You do not have permission.'));
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
    }
       
    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
 

    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
           try {

 if (Auth::check())
  {


    if(Auth::user()->can('delete-cafe'))
      {
        $cafe = Cafe::find($id);
        $cafe->delete();
        return redirect('/cafes');
    }
    else 
      {
    return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
    }
  public function ShowCafeMap(){   

   return view('cafes.cafemap');
 
}
  public function getCafeMap(){

 
    
     $cafes =Cafe::get();
     
                 
        return json_encode(['cafes'=>$cafes]);
 
}

 
  public function allSignsMap(){
      $Project=Sign::where('subactivity_id',25)->get();
      $Signs=Sign::where('subactivity_id','!=',25)->get();
     // dd($Signs);
      return json_encode(['Signs' =>$Signs,'Project'=>$Project]); 
 
}


  public function SignCafe()
  {
    $SignCafe= CafeSigns::all();
    // foreach ($SignCafe as $value) {
    // dd( $value->getcafe_users->Name);
    // }
   
 return view('cafes.Signcafe',compact('SignCafe'));  
 
}

  public function showSignCafeimg($id)
    {
        $Signcafe = CafeSigns::findOrFail($id);

        $pic = \Image::make($Signcafe->Img);
        $response = Response::make($pic->encode('jpeg'));

        //setting content-type
        $response->header('Content-Type', 'image/jpeg');

        return $response;


       
    }
    public function checkCafeCode()
    {
       $cafecode=Input::get('Code');
      $cafe = Cafe::where('Code','=',$cafecode)->first();
     if($cafe==null)     
        $isAvailable = true;
        else
         $isAvailable = false;

    return \Response::json(array('valid' =>$isAvailable,));


       
    }
    








     public function showsignphoto($id)
    {
               

 if (Auth::check())
  {
      $CafeSigns=CafeSigns::whereId($id)->first();
     
      return view('cafes.showsignphoto',compact('CafeSigns'));
         }//endAuth

else
{
return redirect('/login');
}



}










}
