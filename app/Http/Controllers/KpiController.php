<?php

namespace App\Http\Controllers;
use Auth;
use App\Http\Requests;
use App\User;
use App\Gps;
use App\Contractor;
use App\Task;
use App\sale_product_details;
use App\sale_product;
use DB;
use App\Http\Controllers\Controller;
use Request;
use Excel;
use File;
use Illuminate\Support\Facades\Response;
use Validator;
use Illuminate\Support\Facades\Redirect;
use Input;
use Session;
use Geocode;
use Cache;
use URL;
use View;
use Khill\Lavacharts\Laravel\LavachartsFacade as Lava;
use Khill\Lavacharts\Lavacharts;
use TableView;
use App\Dategps;
use PHPExcel; 
use PHPExcel_IOFactory; 
use App\Kpi;


class KpiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::check())
  {
        $Kpis=kpi::all();
        //dd($Kpis);
        return view('KPI.index',compact('Kpis'));
    }

else
{
return redirect('/login');
}
}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
              if (Auth::check())
  {
    
        $Kpi= new Kpi;
        $Kpi->Name =Input::get('Name');
        $Kpi->EngName =Input::get('EngName');
        $Kpi->Weight =Input::get('Weight');
        $Kpi->Target =Input::get('Target');
        $Kpi->Code =Input::get('Code');
        $Kpi->Active=1;
  
        $Kpi->save();
        return redirect('/KPI');
        }

else
{
return redirect('/login');
}
    }


public function Active($id)
{
   
     $kpi=Kpi::find($id);
     

    if($kpi->Active == 1)
    {
        $kpi->Active=0;
        $kpi->save();

    }
    else
    {
        $kpi->Active=1;
        $kpi->save();

    }

return back();
}
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }
       public function updateKPI()
    {
  if (Auth::check())
  {
      
        $KPI_id= Input::get('pk');  
       
        $column_name = Input::get('name');
        $column_value = Input::get('value');
        $KPIData = kpi::whereId($KPI_id)->first();
        $KPIData-> $column_name=$column_value;
        if($KPIData->save())
        return \Response::json(array('status'=>1));
        else 
        return \Response::json(array('status'=>0));
}
    }
        public function checknameKPI()
    {
         if (Auth::check())
  {
         $KPIname=Input::get('Name');
      $KPI = Kpi::where('kpis.Name','=',$KPIname)
     ->select('*')->first();
     if($KPI==null)     
        $isAvailable = true;
        else
         $isAvailable = false;

    return \Response::json(array('valid' =>$isAvailable,));
    }
   } 

      public function checkEngNameKPI()
    {
    if (Auth::check())
  {
      

         $KPIname=Input::get('EngName');
      $KPI = Kpi::where('kpis.EngName','=',$KPIname)
     ->select('*')->first();
     if($KPI==null)     
        $isAvailable = true;
        else
         $isAvailable = false;

    return \Response::json(array('valid' =>$isAvailable,));
}
    }

 public function checkCodeKPI()
    {
    if (Auth::check())
  {
      

         $KPIname=Input::get('Code');
      $KPI = Kpi::where('kpis.Code','=',$KPIname)
     ->select('*')->first();
     if($KPI==null)     
        $isAvailable = true;
        else
         $isAvailable = false;

    return \Response::json(array('valid' =>$isAvailable,));
}
    }

























    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
                     if (Auth::check())
  {
      
        $Kpis=Kpi::find($id);
        $Kpis->delete();
        return redirect('/KPI');
    }
    else
    {
return redirect('/login');

    }
}


}
