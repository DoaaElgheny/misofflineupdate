<?php

namespace App\Http\Controllers;
use Auth;
use App\Http\Requests;
use App\User;
use App\Gps;
use App\Contractor;
use DB;
use App\Http\Controllers\Controller;
use Request;
use Excel;
use File;
use Illuminate\Support\Facades\Response;
use Validator;
use Illuminate\Support\Facades\Redirect;
use Input;
use Session;
use Geocode;
use Cache;
use URL;
use View;
use Khill\Lavacharts\Laravel\LavachartsFacade as Lava;
use Khill\Lavacharts\Lavacharts;
use TableView;
use App\Dategps;
use PHPExcel; 
use PHPExcel_IOFactory; 
use App\Product;
use App\Order;
use App\SaleProduct;
use Mail;

use Sms;

class AdminController extends Controller
{//new


  public function  chartadmin()
    {
        try {
            $users=user::where('role','user')->lists('username','id');

            if (Auth::check()){
            $GLOBALS['username'] =array();
           array_push($GLOBALS['username'],Auth::user()->Username);
            $UserErr = implode(" \n ",$GLOBALS['username']);
            $UserErr = nl2br($UserErr);  
            $cookie_name = 'username';
            $cookie_value = $UserErr;
            setcookie($cookie_name, $cookie_value, time() + (160), "/");
      if(Auth::user()->can('show-chartadmin'))
      {
                 return view('admin/chartadmin');
               }
                 else
                 {
                  return view('admin/welcome');
                }
            }
            else {
              return redirect('/login');
            }
        } catch (Exception $e) {
            return redirect('/login');
        }
    }


 public function  newcharts()
    {


   return view('admin/newcharts');    
       
    }

  public function  VisitsGovLOE()
    {
$dat=date('Y-m-d');

$gps=
DB::table('users')->select(array('users.Name','Type' ,'Contractor_Code','Government','GType',DB::raw('count(Type)  as visits')))
->join('gps', 'gps.user_id', '=', 'users.id')
->join('governments', 'governments.Name', '=', 'users.Government')
->where('governments.GType','=','Lower')
->where('NDate','=',$dat)
->groupBy('users.Name','Contractor_Code')
->orderby('users.Name')
->get();

$data=array();
$sdata=array();
$data['ntype']=0;
$sdata['nsalle']=0;

   foreach ($gps as $key) {

             if($key->Type ==='حصر كميات')
                   {

                            $user_id=user::select('id')->where('Name',$key->Name)->pluck('id')->first();
      
                            $togps = DB::table('gps')
                                   ->select(DB::raw('count(*)  as visits'))
                                   ->where('NDate','=',$dat)
                                   ->where('user_id',$user_id)
                                   ->where('Contractor_Code',$key->Contractor_Code)
                                   ->where('Type','حصر كميات')
                                   ->first();
                                

                            if($key->visits >=  $togps->visits)
                                {
                                     $salle=($key->visits)-($togps->visits);

                                      $key->visits= $togps->visits;

                                if((array_search($key->Type,array_keys($data))))
                                      {
                                           $old=$data[$key->Type];
                                           $data[$key->Type]= $old+$key->visits; 
                                                                                    
                                       }
                                  else{
                                        $data[$key->Type]=$key->visits;      

                                      }             
                             if((array_search($key->Type,array_keys($sdata))))
                                      {
                                           $old=$sdata[$key->Type];
                                           $sdata[$key->Type]= $old+$salle; 
                                                                                    
                                       }
                                  else{
                                        $sdata[$key->Type]= $salle;      

                                      }

                                }


                     }

            if(($key->Type ==='المبيعات'&& $key->visits===2)||($key->Type ==='تسويق'&& $key->visits===2))
                           {

                                  $key->visit=$key->visits/2;

                                  $ketype ='تسويق';

                            if((array_search($ketype,array_keys($data))))
                                      {
                                           $old=$data[$ketype];
                                           $data[$ketype]= $old+$key->visit; 
                                                                                    
                                       }
                                  else{
                                        $data[$ketype]=$key->visit;      

                                      }  

                             if((array_search($ketype,array_keys($sdata))))
                                      {
                                           $old=$sdata[$ketype];

                                           $sdata[$ketype]= $old+$key->visit; 
                                                                                    
                                       }
                                  else{
                                        $sdata[$ketype]=$key->visit;      

                                      }



                           }

    if(($key->Type ==='المبيعات'&& $key->visits < 2))
    {


                                  $ktype ='تسويق';

                            if((array_search($ktype,array_keys($data))))
                                      {
                                           $old=$data[$ktype];

                                           $data[$ktype]= $old+$key->visits; 
                                                                                    
                                       }
                                  else{
                                        $data[$ktype]=$key->visits;      

                                      }  

                             if((array_search($ktype,array_keys($sdata))))
                                      {
                                           $old=$sdata[$ktype];
                                           $sdata[$ktype]= $old+$key->visits; 
                                                                                    
                                       }
                                  else{
                                        $sdata[$ktype]=$key->visits;      

                                      }


   }


                           if(($key->Type ==='تسويق'&& $key->visits<2))
                          {

                                                  

                                                  if((array_search($key->Type,array_keys($data))))
                                                            {
                                                                 $old=$data[$key->Type];

                                                                 $data[$key->Type]= $old+$key->visits; 
                                                                                                          
                                                             }
                                                        else{
                                                              $data[$key->Type]=$key->visits;      

                                                            }  

                                                   if((array_search($key->Type,array_keys($sdata))))
                                                            {
                                                                 $old=$sdata[$key->Type];

                                                                 $sdata[$key->Type]= $old; 
                                                                                                          
                                                             }
                                                        else{
                                                              $sdata[$key->Type]=0;      

                                                            }


                            }



                      }                                                      
                            
 

                   


 unset($data['ntype']) ;
 unset($sdata['nsalle']) ;
                                      $a=[];
                                      $b=[];
                                      foreach ($data as $key => $value) {
if($key =='حصر كميات')
{
  array_push($a,array(
                                          'label' => 'Contractors Program',
                                          'y' => $value
                                          ));
}
else
  { 
   array_push($a,array(
                                          'label' => 'Regular Visits',
                                          'y' => $value
                                          ));
 }

                                       
                                       
                                      }
                                 

                                      foreach ($sdata as $key => $value) {
                                      if($key =='حصر كميات')
{
  array_push($b,array(
                                          'label' => 'Contractors Program',
                                          'y' => $value
                                          ));
}
else
  { 
   array_push($b,array(
                                          'label' => 'Regular Visits',
                                          'y' => $value
                                          ));
 }
                                       
                                      }
                $data = array();
                $data['type']=$a;
                $data['salle']=$b;
     return Response::json($data,  200, [], JSON_NUMERIC_CHECK);

  }

public function  VisitsGovUPE()
    {
$dat=date('Y-m-d');

$gps=
DB::table('users')->select(array('users.Name','Type' ,'Contractor_Code','Government','GType',DB::raw('count(Type)  as visits')))
->join('gps', 'gps.user_id', '=', 'users.id')
->join('governments', 'governments.Name', '=', 'users.Government')
->where('governments.GType','=','Upper')
->where('NDate','=',$dat)
->groupBy('users.Name','Contractor_Code')
->orderby('users.Name')
->get();

$data=array();
$sdata=array();
$data['ntype']=0;
$sdata['nsalle']=0;

   foreach ($gps as $key) {

             if($key->Type ==='حصر كميات')
                   {

                            $user_id=user::select('id')->where('Name',$key->Name)->pluck('id')->first();
      
                            $togps = DB::table('gps')
                                   ->select(DB::raw('count(*)  as visits'))
                                   ->where('NDate','=',$dat)
                                   ->where('user_id',$user_id)
                                   ->where('Contractor_Code',$key->Contractor_Code)
                                   ->where('Type','حصر كميات')
                                   ->first();
                                

                            if($key->visits >=  $togps->visits)
                                {
                                     $salle=($key->visits)-($togps->visits);

                                      $key->visits= $togps->visits;

                                if((array_search($key->Type,array_keys($data))))
                                      {
                                           $old=$data[$key->Type];
                                           $data[$key->Type]= $old+$key->visits; 
                                                                                    
                                       }
                                  else{
                                        $data[$key->Type]=$key->visits;      

                                      }             
                             if((array_search($key->Type,array_keys($sdata))))
                                      {
                                           $old=$sdata[$key->Type];
                                           $sdata[$key->Type]= $old+$salle; 
                                                                                    
                                       }
                                  else{
                                        $sdata[$key->Type]= $salle;      

                                      }

                                }


                     }

            if(($key->Type ==='المبيعات'&& $key->visits===2)||($key->Type ==='تسويق'&& $key->visits===2))
                           {

                                  $key->visit=$key->visits/2;

                                  $ketype ='تسويق';

                            if((array_search($ketype,array_keys($data))))
                                      {
                                           $old=$data[$ketype];
                                           $data[$ketype]= $old+$key->visit; 
                                                                                    
                                       }
                                  else{
                                        $data[$ketype]=$key->visit;      

                                      }  

                             if((array_search($ketype,array_keys($sdata))))
                                      {
                                           $old=$sdata[$ketype];

                                           $sdata[$ketype]= $old+$key->visit; 
                                                                                    
                                       }
                                  else{
                                        $sdata[$ketype]=$key->visit;      

                                      }



                           }

    if(($key->Type ==='المبيعات'&& $key->visits < 2))
    {


                                  $ktype ='تسويق';

                            if((array_search($ktype,array_keys($data))))
                                      {
                                           $old=$data[$ktype];

                                           $data[$ktype]= $old+$key->visits; 
                                                                                    
                                       }
                                  else{
                                        $data[$ktype]=$key->visits;      

                                      }  

                             if((array_search($ktype,array_keys($sdata))))
                                      {
                                           $old=$sdata[$ktype];
                                           $sdata[$ktype]= $old+$key->visits; 
                                                                                    
                                       }
                                  else{
                                        $sdata[$ktype]=$key->visits;      

                                      }


   }


                           if(($key->Type ==='تسويق'&& $key->visits<2))
                          {

                                                  

                                                  if((array_search($key->Type,array_keys($data))))
                                                            {
                                                                 $old=$data[$key->Type];

                                                                 $data[$key->Type]= $old+$key->visits; 
                                                                                                          
                                                             }
                                                        else{
                                                              $data[$key->Type]=$key->visits;      

                                                            }  

                                                   if((array_search($key->Type,array_keys($sdata))))
                                                            {
                                                                 $old=$sdata[$key->Type];

                                                                 $sdata[$key->Type]= $old; 
                                                                                                          
                                                             }
                                                        else{
                                                              $sdata[$key->Type]=0;      

                                                            }


                            }



                      }                                                      
                            
 

                   


 unset($data['ntype']) ;
 unset($sdata['nsalle']) ;
                                      $a=[];
                                      $b=[];
                                      foreach ($data as $key => $value) {
if($key =='حصر كميات')
{
  array_push($a,array(
                                          'label' => 'Contractors Program',
                                          'y' => $value
                                          ));
}
else
  { 
   array_push($a,array(
                                          'label' => 'Regular Visits',
                                          'y' => $value
                                          ));
 }

                                       
                                       
                                      }
                                 

                                      foreach ($sdata as $key => $value) {
                                      if($key =='حصر كميات')
{
  array_push($b,array(
                                          'label' => 'Contractors Program',
                                          'y' => $value
                                          ));
}
else
  { 
   array_push($b,array(
                                          'label' => 'Regular Visits',
                                          'y' => $value
                                          ));
 }
                                       
                                      }
                $data = array();
                $data['type']=$a;
                $data['salle']=$b;
     return Response::json($data,  200, [], JSON_NUMERIC_CHECK);

  }

public function charttotal(){



      $date =date('Y-m-d');
                                    $gps =Gps::select(array('Name','type' ,'Contractor_Code',DB::raw('count(type)  as visits')))
                                    ->where('NDate','=',$date)
                                    ->join('users', 'users.id', '=', 'gps.user_id')
                                    ->groupBy('Name','Contractor_Code')
                                    ->orderby('Name')
                                   ->get();



$data=array();
$data['username']=0;
   foreach ($gps as $key) {
             if($key->type ==='حصر كميات')
                   {

                            $user_id=user::select('id')->where('Name',$key->Name)->pluck('id')->first();
      
                            $togps =Gps::select(DB::raw('count(*)  as visits'))
                                   ->where('NDate','=',$date)
                                   ->where('user_id',$user_id)
                                   ->where('Contractor_Code',$key->Contractor_Code)
                                   ->where('type','حصر كميات')
                                   ->first();
                                

                            if($key->visits >  $togps->visits)
                                {
                                      $key->visits= $togps->visits;
                                }
                     }
            if(($key->type ==='المبيعات'&& $key->visits===2)||($key->type ==='تسويق'&& $key->visits===2))
                           {

                                  $key->visits=$key->visits/2;
                           }

            if((array_search($key->Name,array_keys($data))))
                      {
                           $old=$data[$key->Name];
                           $data[$key->Name]= $old+$key->visits; 
                                                                    
                       }
                  else{
                        $data[$key->Name]=$key->visits;      

                      }                                                      
                            
 

                    }
 unset($data['username']) ;
 $totalvisit=0;

foreach ($data as $key => $value) {

  $totalvisit +=$value;

}

                                     $b =Gps::select(array(DB::raw('count(distinct(Contractor_Code))  as code')))
                                     
                                    ->where('NDate','=',$date)
                                   
                                     ->first();
                                      
                               
                             
                                 
                   $totalpormoter = Gps::select(array('Name',DB::raw('count(distinct(Name))  as Name')))
                                   ->where('NDate','=',$date)
                                   ->join('users', 'users.id', '=', 'gps.user_id')
                                   ->first();
                  $totalorder = Gps::select(array(DB::raw('count(Type)  as type')))
                                   ->where('NDate','=',$date)
                                    ->where('Type','=','المبيعات')

                                   ->join('users', 'users.id', '=', 'gps.user_id')
                                   ->first();



                      $login=Gps::join('users', 'users.id', '=', 'gps.user_id')->select('NTime','Name','Government')
                                     ->where('NDate','=',$date)
                                    
                                    ->groupBy('Name')
                                    ->orderby('NTime')
                                    ->get();

//dd(  $login);

$data = array();
 
$data['visits']=$totalvisit;
$data['contractors']=$b;
$data['totalpormoter']=$totalpormoter;
$data['totalorder']=$totalorder; 
$data['login'] =$login;                


                      return json_encode($data);
                             
                                  

}

public function chartcont(){
   $date = date('Y-m-d');
   $data=array();

   $products =Product::
                  join('orders', 'orders.Product_id', '=', 'products.id')
                    ->join('gps', 'gps.id', '=', 'orders.gps_id')
                    ->select('products.Cement_Name','products.Enname',DB::raw('Count(orders.Amount) as count'),DB::raw('Sum(orders.Amount) as Amount'), DB::raw('CONCAT(products.Cement_Name , "  (", products.Enname ,")") AS product_name'))
                    ->where('NDate','=',$date)
                    ->where('gps.Type','=','المبيعات')
                    ->groupby ('products.Cement_Name')
                    ->orderBy('products.Cement_Name')
                    ->get();

                   $b=[];$c=[];

     foreach ($products as  $value) {
                             
                                    array_push($b,array(
                                      'label' => $value->product_name,
                                      'y' => $value->count
                                      ));
                    
                             
                                    array_push($c,array(
                                      'label' => $value->product_name,
                                      'y' => $value->Amount
                                      ));
                                    }


   $approve =Product::
                   join('orders', 'orders.Product_id', '=', 'products.id')
                    ->join('gps', 'gps.id', '=', 'orders.gps_id')
                  ->select('products.Cement_Name',DB::raw('Count(orders.Amount) as done'),DB::raw('CONCAT(products.Cement_Name, "  (", products.Enname,")") AS product_name'))
                    ->where('NDate','=',$date)
                    ->where('gps.Type','=','المبيعات')
                    ->where('IsDone','=','1')
                    ->groupby ('products.Cement_Name')
                     ->orderBy('products.Cement_Name')
                    ->get();
                 
               $a=[];
     foreach ($approve as  $value) {
                             
                                    array_push($a,array(
                                      'label' => $value->product_name,
                                      'y' => $value->done
                                      ));


              }
        $data['approves']=$a;       
  $data['products']=$b;
   $data['Amount']=$c;
     return Response::json($data,  200, [], JSON_NUMERIC_CHECK);
    

           

               
}

public function saleschart(){
 $data=array();
     //$date = date('Y-m-d');
  $date =date('Y-m-d');
      $products = 
                 Order::join('gps', 'gps.id', '=', 'orders.gps_id')
                ->join('products', 'products.id', '=', 'orders.Product_id')

              ->select('products.Cement_Name','products.Enname',DB::raw('SUM(orders.Amount) as count'), DB::raw('CONCAT(products.Cement_Name , "  (", products.Enname,")") AS product_name'))
                   ->where('gps.NDate','=',$date)
                    ->where('gps.Type','حصر كميات')
                  ->groupby ('orders.Product_id')
                  ->orderBy('count', 'desc')->get();
                  // ->take(10)
                     $b=[];
                   foreach ($products as  $value) {
                             
                                    array_push($b,array(
                                      'label' => $value->product_name,
                                      'y' => $value->count
                                      ));
}
  $data['products']=$b;
     return Response::json($data,  200, [], JSON_NUMERIC_CHECK);
         

  }
  public function typechart(){
               //$date = date('Y-m-d');
  $date = date('Y-m-d');

         $gps = Gps::select(array('Name','Type' ,'Contractor_Code',DB::raw('count(type)  as visits')))
                                    ->where('NDate','=',$date)
                                    ->join('users', 'users.id', '=', 'gps.user_id')
                                    ->groupBy('Name','Contractor_Code')
                                    ->orderby('Name')
                                   ->get();
$data=array();
$sdata=array();
$data['ntype']=0;
$sdata['nsalle']=0;

   foreach ($gps as $key) {

             if($key->Type ==='حصر كميات')
                   {

                            $user_id=user::select('id')->where('Name',$key->Name)->pluck('id')->first();
      
                            $togps =Gps::select(DB::raw('count(*)  as visits'))
                                   ->where('NDate','=',$date)
                                   ->where('user_id',$user_id)
                                   ->where('Contractor_Code',$key->Contractor_Code)
                                   ->where('Type','حصر كميات')
                                   ->first();
                                

                            if($key->visits >=  $togps->visits)
                                {
                                     $salle=($key->visits)-($togps->visits);

                                      $key->visits= $togps->visits;

                                if((array_search($key->Type,array_keys($data))))
                                      {
                                           $old=$data[$key->Type];
                                           $data[$key->Type]= $old+$key->visits; 
                                                                                    
                                       }
                                  else{
                                        $data[$key->Type]=$key->visits;      

                                      }             
                             if((array_search($key->Type,array_keys($sdata))))
                                      {
                                           $old=$sdata[$key->Type];
                                           $sdata[$key->Type]= $old+$salle; 
                                                                                    
                                       }
                                  else{
                                        $sdata[$key->Type]= $salle;      

                                      }

                                }


                     }

            if(($key->Type ==='المبيعات'&& $key->visits===2)||($key->Type ==='تسويق'&& $key->visits===2))
                           {

                                  $key->visit=$key->visits/2;

                                  $ketype ='تسويق';

                            if((array_search($ketype,array_keys($data))))
                                      {
                                           $old=$data[$ketype];
                                           $data[$ketype]= $old+$key->visit; 
                                                                                    
                                       }
                                  else{
                                        $data[$ketype]=$key->visit;      

                                      }  

                             if((array_search($ketype,array_keys($sdata))))
                                      {
                                           $old=$sdata[$ketype];

                                           $sdata[$ketype]= $old+$key->visit; 
                                                                                    
                                       }
                                  else{
                                        $sdata[$ketype]=$key->visit;      

                                      }



                           }

    if(($key->Type ==='المبيعات'&& $key->visits < 2))
    {


                                  $ktype ='تسويق';

                            if((array_search($ktype,array_keys($data))))
                                      {
                                           $old=$data[$ktype];

                                           $data[$ktype]= $old+$key->visits; 
                                                                                    
                                       }
                                  else{
                                        $data[$ketype]=$key->visits;      

                                      }  

                             if((array_search($ktype,array_keys($sdata))))
                                      {
                                           $old=$sdata[$ktype];
                                           $sdata[$ktype]= $old+$key->visits; 
                                                                                    
                                       }
                                  else{
                                        $sdata[$ktype]=$key->visits;      

                                      }


   }


                           if(($key->Type ==='تسويق'&& $key->visits<2))
                          {

                                                  

                                                  if((array_search($key->Type,array_keys($data))))
                                                            {
                                                                 $old=$data[$key->Type];

                                                                 $data[$key->Type]= $old+$key->visits; 
                                                                                                          
                                                             }
                                                        else{
                                                              $data[$key->Type]=$key->visits;      

                                                            }  

                                                   if((array_search($key->Type,array_keys($sdata))))
                                                            {
                                                                 $old=$sdata[$key->Type];

                                                                 $sdata[$key->Type]= $old; 
                                                                                                          
                                                             }
                                                        else{
                                                              $sdata[$key->Type]=0;      

                                                            }


                            }



                      }                                                      
                            
 

                   


 unset($data['ntype']) ;
 unset($sdata['nsalle']) ;
                                      $a=[];
                                      $b=[];
                                      foreach ($data as $key => $value) {
                                        array_push($a,array(
                                          'label' => $key,
                                          'y' => $value
                                          ));
                                       
                                      }

                                      foreach ($sdata as $key => $value) {
                                        array_push($b,array(
                                          'label' => $key,
                                          'y' => $value
                                          ));
                                       
                                      }
                $data = array();
                $data['type']=$a;
                $data['salle']=$b;
     return Response::json($data,  200, [], JSON_NUMERIC_CHECK);

  }
public function chartadminpanelUpper(){ 
  if (Auth::check()){
                  // The user is logged in...



           $date = date('Y-m-d');
           // $date="2016-11-17";    

         $gps = DB::table('gps')
                                    
                                    ->where('NDate','=',$date)
                                    ->join('users', 'users.id', '=', 'gps.user_id')
                                    ->select(array('users.Name','type' ,'Contractor_Code','GType',DB::raw('count(type)  as visits')))
                                    ->join('governments as g', 'g.Name', '=', 'users.Government')
->where('g.GType','=','Upper')
                                    ->groupBy('Name','Contractor_Code')
                                    ->orderby('Name')
                                   ->get();
                                   

// dd($gps);


$data=array();
$data['username']=0;
   foreach ($gps as $key) {
             if($key->type ==='حصر كميات')
                   {

             $user_id=user::join('governments as g', 'g.Name', '=', 'users.Government')
                           ->where('g.GType','=','Upper')
                           ->select('users.id')
                           ->where('users.Name',$key->Name)
                           ->pluck('users.id')
                           ->first();

             $togps = DB::table('gps')
                         ->select(DB::raw('count(*)  as visits'))
                         ->where('NDate','=',$date)
                         ->where('user_id',$user_id)
                         ->where('Contractor_Code',$key->Contractor_Code)
                         ->where('type','حصر كميات')
                         ->first();
                             

                            if($key->visits >  $togps->visits)
                                {
                                      $key->visits= $togps->visits;
                                }
                     }
            if(($key->type ==='المبيعات'&& $key->visits===2)||($key->type ==='تسويق'&& $key->visits===2))
                           {

                                  $key->visits=$key->visits/2;
                           }

            if((array_search($key->Name,array_keys($data))))
                      {
                           $old=$data[$key->Name];
                           $data[$key->Name]= $old+$key->visits; 
                                                                    
                       }
                  else{
                        $data[$key->Name]=$key->visits;      

                      }                                                      
                            
 

                    }
 unset($data['username']) ;
                                      $a=[];
                                      foreach ($data as $key => $value) {
                                        array_push($a,array(
                                          'label' => $key,
                                          'y' => $value
                                          ));
                                       
                                      }
                                          

                       $total_contractor = DB::table('gps')
                                   ->select(array('users.Name','g.GType',DB::raw('count(distinct(Contractor_Code))  as visits')))
                                   ->where('NDate','=',$date)
                                   ->whereIn('type',['المبيعات','تسويق','حصر كميات'])
                                   ->join('users', 'users.id', '=', 'gps.user_id')
                                                                   ->join('governments as g', 'g.Name', '=', 'users.Government')
                                                 ->where('g.GType','=','Upper')
                                   ->groupBy('user_id')
                                   ->orderby('Name')
                                   ->get();
                       
                                   $b=[];
                                  foreach ($total_contractor as  $value) {
                             
                                    array_push($b,array(
                                      'label' => $value->Name,
                                      'y' => $value->visits
                                      ));
                                   
                                  }



$data = array();
$data['totalvisit']=$a;
$data['totalcontractor']=$b;

                                  
                           // dd(json_encode($data)) ;  
                      return Response::json($data,  200, [], JSON_NUMERIC_CHECK);
              }
          else{   
                  return Redirect::to('/login');             
              }
        
}
public function chartadminpanel(){ 
  if (Auth::check()){
                  // The user is logged in...



           $date = date('Y-m-d');
           // $date="2016-11-17";    

         $gps = DB::table('gps')
                                    
                                    ->where('NDate','=',$date)
                                    ->join('users', 'users.id', '=', 'gps.user_id')
                                    ->select(array('users.Name','type' ,'Contractor_Code','GType',DB::raw('count(type)  as visits')))
                                    ->join('governments as g', 'g.Name', '=', 'users.Government')
->where('g.GType','=','Lower')
                                    ->groupBy('Name','Contractor_Code')
                                    ->orderby('Name')
                                   ->get();
                                   

// dd($gps);


$data=array();
$data['username']=0;
   foreach ($gps as $key) {
             if($key->type ==='حصر كميات')
                   {

                            $user_id=user::join('governments as g', 'g.Name', '=', 'users.Government')
->where('g.GType','=','Lower')->select('users.id')->where('users.Name',$key->Name)->pluck('users.id')->first();
      
                            $togps = DB::table('gps')
                                   ->select(DB::raw('count(*)  as visits'))
                                   ->where('NDate','=',$date)
                                   ->where('user_id',$user_id)
                                   ->where('Contractor_Code',$key->Contractor_Code)
                                   ->where('type','حصر كميات')
                                   ->first();
                             

                            if($key->visits >  $togps->visits)
                                {
                                      $key->visits= $togps->visits;
                                }
                     }
            if(($key->type ==='المبيعات'&& $key->visits===2)||($key->type ==='تسويق'&& $key->visits===2))
                           {

                                  $key->visits=$key->visits/2;
                           }

            if((array_search($key->Name,array_keys($data))))
                      {
                           $old=$data[$key->Name];
                           $data[$key->Name]= $old+$key->visits; 
                                                                    
                       }
                  else{
                        $data[$key->Name]=$key->visits;      

                      }                                                      
                            
 

                    }
 unset($data['username']) ;
                                      $a=[];
                                      foreach ($data as $key => $value) {
                                        array_push($a,array(
                                          'label' => $key,
                                          'y' => $value
                                          ));
                                       
                                      }
                                          

                       $total_contractor = DB::table('gps')
                                   ->select(array('users.Name','g.GType',DB::raw('count(distinct(Contractor_Code))  as visits')))
                                   ->where('NDate','=',$date)
                                   ->whereIn('type',['المبيعات','تسويق','حصر كميات'])
                                   ->join('users', 'users.id', '=', 'gps.user_id')
                                                                   ->join('governments as g', 'g.Name', '=', 'users.Government')
                                                 ->where('g.GType','=','Lower')
                                   ->groupBy('user_id')
                                   ->orderby('Name')
                                   ->get();
                       
                                   $b=[];
                                  foreach ($total_contractor as  $value) {
                             
                                    array_push($b,array(
                                      'label' => $value->Name,
                                      'y' => $value->visits
                                      ));
                                   
                                  }



$data = array();
$data['totalvisit']=$a;
$data['totalcontractor']=$b;

                                  
                           // dd(json_encode($data)) ;  
                      return Response::json($data,  200, [], JSON_NUMERIC_CHECK);
              }
          else{   
                  return Redirect::to('/login');             
              }
        
}

  //azhar
   public function report()
    {
        try {
            $users=user::where('role','user')->lists('username','id');
            if (Auth::check()){
              return view('reports/reports',compact('users'));
            }
            else {
              return redirect('/login');
            }
        } catch (Exception $e) {
            return redirect('/login');
        }
    }

  public function reports()
    {
        try {
              $rules = array(
                'Title'=>array('required','regex:/^(?:[\p{L}\p{Mn}\p{Pd}\'\x{2019}{0-9}]+(?:$|\s+)){1,}$/u'),
                'Start_Date'    =>'required|date', 
                'End_Date'      => 'required|date|after:Start_Date',
                'users'  => array('required','not_in:null'),           
              );
              $messages = [
                        'required'           =>     'please Enter Data ',
                        'End_Date.after'     =>     'Please,Enter Correct Date ',
                        'Start_Date.after'   =>     'Enter Correct Date',
                        'regex'              =>     'Enter Correct Data',
                        'date_format'        =>     'Enter Correct Date',
                        'not_in'             =>    'Please Choose '
                    ];
            $Validator=Validator :: make(Input::all(),$rules,$messages);
            if($Validator->fails()){
                return redirect('/reports')->withErrors($Validator)->withInput();
            }
            else
                {
                  $Title=Request::get('Title');                
                  $Start_Date=date("Y-m-d",strtotime(Request::get('Start_Date')));
                  $End_Date=date("Y-m-d",strtotime(Request::get('End_Date')));        
                  $pormoters=Request::get('users');           
                  return $this->generatecharts($Title, $Start_Date,$End_Date,$pormoters);
                }
        }
      catch (Exception $e) {
          return redirect('/login');       
      }   
  }

public function generatecharts($Title, $Start_Date,$End_Date,$pormoters)
    {
       try {

          if (Auth::check()){

              $stocksTable = \Lava::DataTable();
              $stocksTable->addDateColumn('Date');
              $range=array($Start_Date,  $End_Date);
              $date=gps::whereBetween('NDate',$range)->groupby('NDate')->get();

              if($pormoters[0]==='all')
              { 
                $user_ids= gps::whereBetween('NDate',$range)->select('user_id')->distinct('user_id')->get();
                $datatable=gps::whereBetween('NDate',$range)->select('*')->get();
                $data=[];
                
                foreach ($user_ids as $keys) {
                                                $username=user::select('username')->where('id','=',$keys->user_id)->first();       
                                                $data[$keys->user_id]=$username->username ;
                                                $stocksTable->addNumberColumn($username->username);
                                              }  
              }
        else
        {
          $user_ids=$pormoters; 

          $data=[];

          $datatable=gps::whereBetween('NDate',$range)->whereIn('user_id',$user_ids)->select('*')->get();
         
          foreach ($user_ids as $keys) {
                                        $username=user::select('username')->where('id','=',$keys)->first();
                                        $data[$keys]=$username->username ;
                                        $stocksTable->addNumberColumn($username->username);
                                       }
  
      }
    $n=count($data);
    $a=array();
    $rowData=array();
    for ($i=0; $i < count($date); $i++) { 

        array_push($a,$date[$i]->NDate);

        
         $gps = Gps::select(array('username','type' ,'Contractor_Code',DB::raw('count(type)  as visits')))
                                    ->where('NDate','=',$date[$i]->NDate)
                                    ->join('users', 'users.id', '=', 'gps.user_id')
                                    ->groupBy('username','Contractor_Code')
                                    ->orderby('username')
                                    ->get();





$pro_data=array();
$pro_data['username']=0;

   foreach ($gps as $key) {
             if($key->type ==='حصر كميات')
                   {

                            $user_id=user::select('id')->where('username',$key->username)->pluck('id')->first();
      
                            $togps =Gps::select(DB::raw('count(*)  as visits'))
                                   ->where('NDate','=',$date[$i]->NDate)
                                   ->where('user_id',$user_id)
                                   ->where('Contractor_Code',$key->Contractor_Code)
                                   ->where('type','حصر كميات')
                                   ->first();
                                

                            if($key->visits >  $togps->visits)
                                {
                                      $key->visits= $togps->visits;
                                }
                     }
            if(($key->type ==='المبيعات'&& $key->visits===2)||($key->type ==='تسويق'&& $key->visits===2))
                           {

                                  $key->visits=$key->visits/2;
                           }

            if((array_search($key->username,array_keys($pro_data))))
                      {
                           $old=$pro_data[$key->username];
                           $pro_data[$key->username]= $old+$key->visits; 
                                                                    
                       }
                  else{
                        $pro_data[$key->username]=$key->visits;      

                      }                                                      
                            
 

                    }

// dd(array_keys($pro_data));

            // for ($j=0; $j < count($pro_data); $j++) { 
                    $j=0;
foreach ($pro_data as $p_key => $p_value) {


                $No=0;
                foreach ($data as $key => $value) {

                     $No++;

                      if($value===$p_key)
                          {
                            $a[$No]=$p_value;
                          }
                      if((($j+1) === count($pro_data)) && ($No === $n))
                          {
                            for ($s=1; $s<= $No; $s++) 
                              { 
                                 if(!(array_search($s,array_keys($a))))
                                    {
                                      $a[$s]=0;                                     
                                    }
                                    ksort($a);                                                          
                              }
                              $stocksTable->addRow($a);                                    
                        }                                   
                   }
                   $j++;
            }
  unset($data['username']) ;
// dd(key($pro_data));
           $a=array();
    }
 
    $chart = \Lava::LineChart('MyStocks', $stocksTable,[
              'titleTextStyle' => [
                    'color'    => '#eb6b2c',
                    'fontSize' => 15,
                     'height'  => 700,
                     
        ]]);
    return view('charts/analytics',compact('Title','datatable'));
   } 
   else { return redirect('/login');}
  }
     catch (Exception $e) 
      {
          return redirect('/login');
       }
      
}


public function logout(){
        Auth::logout();
        return Redirect::to('/login');
  }

public function latest()
{
  $done1=Order::
  join('gps', 'gps.id', '=', 'orders.gps_id')
  ->join('users', 'users.id', '=', 'gps.user_id')
  ->select('gps.user_id','users.Name')
  ->where('orders.IsDone','0')
  ->where('orders.Type','المبيعات')
  ->where('gps.Status','1')
  ->groupby('gps.user_id')
  ->get();
  $options = array();
  $data=[];
  foreach ($done1 as $done) {
  $count=Gps::
  join('orders', 'orders.gps_id', '=', 'gps.id')
  ->join('users', 'users.id', '=', 'gps.user_id')
  ->where('orders.IsDone','0')
  ->where('orders.Type','المبيعات')
  ->where('gps.Status','1')
  ->where('gps.user_id',$done->user_id)
  ->count('gps.id');
      $name= $done->Name;
    
      array_push($data,array($count=>$name));

  }
  return Response::json($data);
}

public function updatelatest($name)
{
  
  $user_id=user::select('id')->where('Name',$name)->pluck('id')->first();
  $gps=Gps:: 
  join('orders', 'orders.gps_id', '=', 'gps.id')
  ->join('users', 'users.id', '=', 'gps.user_id')
  ->select('gps.*','users.Name') 
  ->where('orders.IsDone','0')
  ->where('orders.Type','المبيعات')
  ->where('gps.Status','1')
  ->where('gps.user_id',$user_id)
  ->get(); 

  $data=[];
   foreach ($gps as $done) {

$result=[];

$items=Order::
  select('orders.*')
  ->where('orders.IsDone','0')
  ->where('orders.Type','المبيعات')
  ->where('orders.gps_id','=',$done->id)
  ->get(); 
 
foreach($items as $item)
   {
         if($item->SaleProduct_id !== null)
         {

          $sale_products=SaleProduct::
           select('Name')
            ->where('id',$item->SaleProduct_id)
             ->first(); 
             array_push($result,$sale_products->Name.$item->Amount);
         }
         if($item->Product_id !== null)
         {
           $products=Product::
           select('Cement_Name')
            ->where('id',$item->Product_id)
            ->first();
        array_push($result,$products->Cement_Name.$item->Amount);
         }
      
      $ISDone=Order::where('orders.id',$item->id)
 ->update(['ISDone' => 1]);
  }
  


    array_push($data,array( 
      'Name'=>$done->Name,
      'Date'=>$done->NDate,
      'ContractorCode'=>$done->Contractor_Name,
      'Products'=>$result

      )
    );
  
    }
   return Response::json($data);
}

public function adminpanel(){
      try 
      {      
 $gps = gps::all()->sortByDesc('NDate');
 
           if (Auth::check()){
                  // The user is logged in...
                  return view('admin/adminpanel',compact('gps'));
              }
          else{   
                  return Redirect::to('/login');             
                  // dd("nott logged in...");
              }
      }
     catch (Exception $e){ 
          return redirect('/login');    
        }       
}
  
public function index()
    {
      try {
            $users=user::where('role','user')->get();   
            return json_encode($users);
        } 
      catch (Exception $e) {
          return redirect('/login');  
      }

  }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

  public function generateRandomString($length) {
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }


    public function login(){ 

     try {
       $data = Input::all();      


        $rules = array(
            'username' => 'required',
            'password' => 'required',
             );


        $validator = Validator::make($data, $rules);

        if ($validator->fails()){
          return Redirect::to('/login')->withInput(Input::except('password'))->withErrors($validator);
        }
        else {
          $userdata = array(
                'username' => Input::get('username'),
                'password' => Input::get('password')
              );
            $titles=User::where('username',$userdata['username'])->pluck('role')->first();

            if (Auth::attempt($userdata))
            {
                 Session::put('user', $titles);

                  session(['user' => $titles]);

                  Auth::loginUsingId(Auth::user()->id);


                  //Send Verification Email before Login 
$randserial=$this->generateRandomString(5);

                  DB::table('users')
                  ->where('id', Auth::user()->id)
                  ->update(['SerialNumber' =>$randserial]);



// $message  = "Hello ".Auth::user()->Name .' your verification code is:'.$randserial ;
// $to       = ["+20".Auth::user()->Tele];
// $from     = "+1 424-351-9475 ";
// $response = Sms::sendMany($message,$to,$from);



//      return view('LoginSerialnumber');




                  return Redirect::to('/chartadmin');
            }
          // doing login.
          if (Auth::validate($userdata)) {
            if (Auth::attempt($userdata)) {
                Session::flash('error', 'You are not allowed to login'); 
                return Redirect::to('/login');
            }
          } 
          else {

            Session::flash('error', 'Something went wrong'); 
            return Redirect::to('/login');
            }

        }
       
     } catch (Exception $e) {
        return redirect('/login');

       
     }
       
  }
 public function importgpsreport()
    {
      try
      {

        $temp= Request::get('submit'); 


        if(isset($temp))

        { 


         $filename = Input::file('file')->getClientOriginalName();

         $Dpath = base_path();


         $upload_success =Input::file('file')->move( $Dpath, $filename);


         Excel::load($upload_success, function($reader)
         {   




          $results = $reader->get()->all();


          foreach ($results as $data)
          {
            $gps =new Gps();
            $order =new Order();
            // dd($gps);
            $gps->NDate =$data['date'];
            $gps->NTime =$data['time'];
            $gps->Status =$data['status'];

     $gps->Type =$data['type'];
  
        
            $productName=$data['product'];
            $productQuantity=$data['quantity'];

            // $gps->Contractor_code =$data['contractor_code'];
            // $adress =$data['adress'];



          

            $gps->Long =$data['lat'];;
            $gps->Lat =$data['long'];
      
          $Pormoter_Id= User::where('Name',$data['name'])->pluck('id')->first();
          // dd($Pormoter_Id);
          $gps->user_id =$Pormoter_Id;
          $Contractor_Id= Contractor::where('Code',$data['code'])->first();
          $gps->contractor_id=$Contractor_Id->id;
          $gps->Contractor_Code=$Contractor_Id->Code;
          $gps->Contractor_Name=$Contractor_Id->Name;

          $gps->save();
         if($data['type']=='حصر كميات')
{
            $productN=Product::where('Cement_Name',$data['product'])->pluck('id')->first();
  $order->Product_id= $productN;
    $order->Amount= $data['quantity'];

  $order->gps_id= $gps->id;
  $order->Type= $data['type'];
  $order->save();

}
         if($data['type']=='المبيعات')
{
           
            $SalesproductN=SaleProduct::where('Name',$data['product'])->first();
  $order->SaleProduct_id= $SalesproductN->id;
    $order->Amount= $data['quantity'];

  $order->gps_id= $gps->id;
  $order->Type= $data['type'];
  $order->save();

}


        }

      });


       }

       return redirect('/gps'); 

     }
     catch(Exception $e)
     {
       return redirect('/gps'); 
     }

   }

public function NameDateFilter($name, $date, $date2){ 
      try {
            if ($name=="empty") { 
              
              if ($date !="empty" && $date2 =="empty") 
              { // only from date
                  $date2 = date('Y-m-d'); 
                    
                  if ($date2 > $date){
                                     $range=array($date,  $date2);
                              }
                   else{
                         $range=array($date2,  $date);
                       }
                  

                   $loc = DB::table('gps')
                        ->join('users','users.id','=','gps.user_id')
                        ->whereBetween('NDate',$range)
                        ->select('gps.*')
                        ->get();
       
              }
              if ($date !="empty" && $date2 !="empty") 
              { // only from date
             

           if ($date2 > $date){
                                     $range=array($date,  $date2);
                              }
                         else{
                                $range=array($date2,  $date);
                              }

                   $loc = DB::table('gps')
                        ->join('users','users.id','=','gps.user_id')
                        ->whereBetween('NDate',$range)
                        ->select('gps.*')
                        ->get();
       
              }


              if (($date =="empty" && $date2 !="empty"))  
              { 
                  // only to date
                  $date = $date2;  

                    $loc = DB::table('gps')
                        ->join('users','users.id','=','gps.user_id')
                        ->where('NDate',$date)
                        ->select('gps.*')
                        ->get();          
// dd( $loc );

              }

              if ($date =="empty" && $date2=="empty" ) 
              { // from and to date
                        $date = date('Y-m-d');

                                 $loc = DB::table('gps')
                                          ->join('users','users.id','=','gps.user_id')
                                          ->where('NDate',$date)
                                          ->select('gps.*')
                                          ->get();          


              }

         

 foreach ($loc as $location) {
                      $var = DB::table('users')
                              ->select('Name')
                              ->where('id', '=', $location->user_id)
                              ->get();
                      $location->user_id =$var[0]->Name;             
                  }

$users=User::where('Type_User','Outdoor Promoters')->get();

          }
          
          if($name!="empty") 
          {

            $users=User::where('Type_User','Outdoor Promoters')
            ->where('users.Name','=',$name)->first();

          if ($date !="empty" && $date2 =="empty") 
              { // only from date
                  $date2 = date('Y-m-d'); 

                 if ($date2 > $date){
                                     $range=array($date,  $date2);
                                     }
                                else{
                                     $range=array($date2,  $date);
                                     }
                   $loc = DB::table('gps')
                        ->join('users','users.id','=','gps.user_id')
                        ->whereBetween('NDate',$range)
                        ->where('users.Name','=',$name)
                        ->select('gps.*')
                        ->get();

       
              }
              if ($date !="empty" && $date2 !="empty") 
              { // only from date
             

               if ($date2 > $date){ 
                                     $range=array($date,  $date2);
                                  }
                   else{
                         $range=array($date2,  $date);
                       }
                   $loc = DB::table('gps')
                        ->join('users','users.id','=','gps.user_id')
                        ->whereBetween('NDate',$range)
                        ->where('users.Name','=',$name)
                        ->select('gps.*')
                        ->get();
       
              }


              if (($date =="empty" && $date2 !="empty"))  
              { 
                  // only to date
                  $date = $date2;  

                    $loc = DB::table('gps')
                        ->join('users','users.id','=','gps.user_id')
                        ->where('users.Name','=',$name)
                        ->where('NDate',$date)
                        ->select('gps.*')
                        ->get();          


              }

              if ($date =="empty" && $date2=="empty" ) 
              { // from and to date
                        $date = date('Y-m-d');

                                 $loc = DB::table('gps')
                                          ->join('users','users.id','=','gps.user_id')
                                          ->where('users.Name',$name)
                                          ->where('NDate',$date)
                                          ->select('gps.*')
                                          ->get();          
                         

              }

$users=User::where('Name',$name)->get();


            foreach ($loc as $location) {
                      $var = DB::table('users')
                              ->select('Name')
                              ->where('id', '=', $location->user_id)
                              ->get();
                      $location->user_id =$var[0]->Name;   
                      
                  }


          }              
               
               

         
          
        return json_encode(['locations' =>$loc,'homes'=>$users]);      
      } 

      catch (Exception $e) {
            return redirect('/login');          
        }
  
}






public function DrawMap(){

    try{
    
         $users=User::where('Type_User','Outdoor Promoters')->get();
        return json_encode(['locations'=>$users]);
      }
        catch (Exception $e) {
          return redirect('/login');
         }
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $users=user::where('id','=',$id)->first();
  
       
        return $users;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }
    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try
        { 
            $gps= gps::find($id);
            $gps->delete();
        return redirect('/adminpanel');
    }
    catch(Exception $e)
    {
            return redirect('/adminpanel');

    }

    }

  
    
 
public function Gpsupdate( )
 { 



    $gps = Input::get('pk');  
        $column_name = Input::get('name');
        $column_value = Input::get('value');
    
        $gpsData = gps::whereId($gps)->first();
        $gpsData-> $column_name=$column_value;

        if($gpsData->save())

        return \Response::json(array('status'=>1));
    else 
        return \Response::json(array('status'=>0));
 
  


}
   



   public function check( )
 { 

      $id = Input::get('id');
      $checkvalue = Input::get('checkvalue');

      $checkData = gps::whereId($id)->first();
       
      $checkData->Approve=$checkvalue;
      $checkData->save();
  


    }  
    

public function importgps()
  {
    try
    {

  $temp= Request::get('submit'); 


   if(isset($temp))

 { 


   $filename = Input::file('file')->getClientOriginalName();
     

     $Dpath = base_path();


     $upload_success =Input::file('file')->move( $Dpath, $filename);
    

       Excel::load($upload_success, function($reader)
       {   
     
         


    $results = $reader->get()->all();


       foreach ($results as $data)
        {
        
$gps =new Gps();
$gps->NDate =$data['ndate'];
$gps->NTime =$data['ntime'];
$gps->Status =$data['status'];
$gps->Type =$data['type'];
$gps->Contractor_name =$data['contractor_name'];
$gps->Contractor_code =$data['contractor_code'];
$adress =$data['adress'];

 if($adress=="")
     {
       $gps->Long ="";
       $gps->Lat ="";
     }
     else{
        preg_match_all('!\d+(?:\.\d+)?!', $adress, $matches);
      $floats = array_map('floatval', $matches[0]);

     $gps->Long =$floats[1];
     $gps->Lat =$floats[0];
     }
     $Pormoter_Id= User::where('Name',$data['name'])->pluck('id')->first();

$gps->user_id =$Pormoter_Id;
   $gps->user_id =$Pormoter_Id;
$Contractor_Id= Contractor::where('Code',$data['contractor_code'])->pluck('id')->first();
   $gps->contractor_id=$Contractor_Id;
   // dd($data['name'],$Contractor_Id);
$gps->save();
       
        }
   
    });


  }

   return redirect('/gps'); 
   
 }
 catch(Exception $e)
 {
   return redirect('/gps'); 
 }

}


    
public function ValidateAdmin($data, $i ){ 
        $GLOBALS['admin_valid']=0;        
   if(!isset($GLOBALS['admin'])) {
         $GLOBALS['admin']= array();
          } 
        if(!isset($GLOBALS['Emptyadmin'])) 
        { 
            $GLOBALS['Emptyadmin']= array(); 
        } 
        if(!isset($AdminErr)) { 
           $AdminErr = 'Name uncorrect for cell: ';
             }      
        if(!isset($EmptyAdminErr)) { 
           $EmptyAdminErr = 'Empty Name for cell: '; 
        } 
        if(!isset ($GLOBALS['Adress'])) 
        { 
    $GLOBALS['Adress']= array(); 
    } 
    if(!isset($AdressErr)){
       $AdressErr = 'Uncorrect Adress for cell: '; 
    }  
 if(!isset($GLOBALS['contractorname'])){
    $GLOBALS['contractorname']=array(); 
  }
  if(!isset($contractornameErr))
  { 
   $contractornameErr= 'Uncorrect contractor Name for cell: '; 
}
  
  if(!isset($GLOBALS['contractorcode'])){
    $GLOBALS['contractorcode']=array(); 
  }
  if(!isset($contractorcodeErr))
  { 
   $contractorcodeErr= 'Uncorrect contractor code for cell: '; 
}
  if(!isset($GLOBALS['date'])){
    $GLOBALS['date']=array(); 
  }
  if(!isset($dateErr))
  { 
   $dateErr= 'Uncorrect date for cell ex"1-1-1991": '; 
}
if(!isset($GLOBALS['noadmin'])){
    $GLOBALS['noadmin']=array(); 
  }

 if(!isset($noadminErr))
  { 
   $noadminErr= 'No Promoter name for cell: '; 
}

      $name_regex = preg_match('/\p{Arabic}/u', $data['name']);
      $adress_regex = preg_match('#[-a-zA-Z0-9@:%_\+.~\#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~\#?&//=]*)?#si' , $data['adress']);
       $contractor_name_regex = preg_match('/\p{Arabic}/u' , $data['contractor_name']);
      $contractor_code_regex = preg_match('/^[0-9]/' , $data['contractor_code']);
      $date_regex = preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/' , $data['ndate']);

        if ($data['name'] == " " || $data['name']== null) {
            $data['name']= null;
        }
         if ($data['contractor_name'] == " " || $data['contractor_name']== null) {
            $data['contractor_name']= null;
        }
          if ($data['contractor_code'] == " " || $data['contractor_code']== null) {
            $data['contractor_code']= null;
        }
  if ($data['ndate'] == " " || $data['ndate']== null) {
            $data['ndate']= null;
        }
           if ($data['name'] != null) { // company has name
        if ($name_regex == 1) {
        if ( $adress_regex == 1 ||$data['adress'] == null ||$data['adress'] ==" ") { 
      if ($contractor_name_regex== 1 ) { 
     if ($contractor_code_regex== 1 ) 
     { 
       if ($date_regex== 1 ) 
     { 

        $gps =new Gps();
        $gps->Contractor_Name =$data['contractor_name'];
        $gps->Contractor_Code =$data['contractor_code'];
        $adress =$data['adress'];
    if($adress=="")
     {
       $gps->Long ="";
       $gps->Lat ="";
     }
     else{
        preg_match_all('!\d+(?:\.\d+)?!', $adress, $matches);
      $floats = array_map('floatval', $matches[0]);

     $gps->Long =$floats[1];
     $gps->Lat =$floats[0];
     }
     $gps->Ndate =$data['ndate'];
$Pormoter_Id= User::where('Username',$data['name'])->pluck('id')->first();
$gps->user_id =$Pormoter_Id;
   $gps->user_id =$Pormoter_Id;
$Contractor_Id= Contractor::where('Contractor_Code',$data['contractor_code'])->pluck('id')->first();
   $gps->contractor_id=$Contractor_Id;
      $gpsq=gps::where('Contractor_Code',$data['contractor_code'])
              ->where('NDate',$data['ndate'])  
              ->get(); 
              $count=count($gpsq);
// dd($gpsq);
                  if($count=='0' && $Pormoter_Id !=null)                 
                    {    
                        $gps->save(); 
                        $GLOBALS['admin_valid']=1;
                    } 

                  elseif($Pormoter_Id ==null)
                  {

                    array_push($GLOBALS['noadmin'],$i); 

                  }

         
        }
           
     else {
              array_push($GLOBALS['date'],$i); 
      
        } 
            }
          else {
              array_push($GLOBALS['contractorcode'],$i); 
      
        } 
            }
           
             else {
             array_push($GLOBALS['contractorname'],$i); 
         
        } 
      }
         
            else {
                array_push($GLOBALS['Adress'],$i); 
          
         
        } 
      }
      else {
              array_push($GLOBALS['admin'],$i); 
         
        } 
    }
     
    else {
            array_push($GLOBALS['Emptyadmin'],$i); 
        } 

    if ( !empty ($GLOBALS['Emptyadmin'] )) {
            $GLOBALS['Emptyadmin'] = array_unique($GLOBALS['Emptyadmin']);
            $EmptyAdminErr= $EmptyAdminErr.implode(" \n ",$GLOBALS['Emptyadmin']);
            $EmptyAdminErr = nl2br($EmptyAdminErr);  
            $cookie_name = 'EmptyAdminErr';
            $cookie_value = $EmptyAdminErr;
            setcookie($cookie_name, $cookie_value, time() + (60), "/");
        }
            if ( !empty ($GLOBALS['Adress'] )) {
            $GLOBALS['Adress'] = array_unique($GLOBALS['Adress']);
            $AdressErr = $AdressErr.implode(" \n ",$GLOBALS['Adress']);
            $AdressErr = nl2br($AdressErr);  
            $cookie_name = 'AdressErr';
            $cookie_value = $AdressErr;
            setcookie($cookie_name, $cookie_value, time() + (60), "/");
        }
        if ( !empty ($GLOBALS['admin'] )) {
            $GLOBALS['admin'] = array_unique($GLOBALS['admin']);
            $AdminErr = $AdminErr.implode(" \n ",$GLOBALS['admin']);
            $AdminErr = nl2br($AdminErr);  
            $cookie_name = 'AdminErr';
            $cookie_value = $AdminErr;
            setcookie($cookie_name, $cookie_value, time() + (60), "/");
        }
       if ( !empty ($GLOBALS['contractorname'] )) {
            $GLOBALS['contractorname'] = array_unique($GLOBALS['contractorname']);
            $contractornameErr = $contractornameErr.implode(" \n ",$GLOBALS['contractorname']);
            $contractornameErr = nl2br($contractornameErr);  
            $cookie_name = 'contractornameErr';
            $cookie_value = $contractornameErr;
            setcookie($cookie_name, $cookie_value, time() + (60), "/");
        }
          if ( !empty ($GLOBALS['contractorcode'] )) {
            $GLOBALS['contractorcode'] = array_unique($GLOBALS['contractorcode']);
            $contractorcodeErr =  $contractorcodeErr.implode(" \n ",$GLOBALS['contractorcode']);
             $contractorcodeErr = nl2br($contractorcodeErr);  
            $cookie_name = 'contractorcodeErr';
            $cookie_value =  $contractorcodeErr;
            setcookie($cookie_name, $cookie_value, time() + (60), "/");
        }
         if ( !empty ($GLOBALS['date'] )) {
            $GLOBALS['date'] = array_unique($GLOBALS['date']);
            $dateErr =  $dateErr.implode(" \n ",$GLOBALS['date']);
             $dateErr = nl2br($dateErr);  
            $cookie_name = 'dateErr';
            $cookie_value =  $dateErr;
            setcookie($cookie_name, $cookie_value, time() + (60), "/");
        }
          if ( !empty ($GLOBALS['noadmin'] )) {
            $GLOBALS['noadmin'] = array_unique($GLOBALS['noadmin']);
            $noadminErr = $noadminErr.implode(" \n ",$GLOBALS['noadmin']);
            $noadminErr = nl2br($noadminErr);  
            $cookie_name = 'noadminErr';
            $cookie_value = $noadminErr;
            setcookie($cookie_name, $cookie_value, time() + (60), "/");
        }


}



public function convertXLStoCSV($infile,$outfile)
    {
        $fileType = PHPExcel_IOFactory::identify($infile);
        $objReader = PHPExcel_IOFactory::createReader($fileType);

        $objReader->setReadDataOnly(false);   
        $objPHPExcel = $objReader->load($infile);    

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
        $objWriter->save($outfile);
    }
public function importfile()
    {    
        ini_set('max_execution_time', 300);
        $GLOBALS['admin']= array();  

        $file = Input::file('file');
        if($file == null){ 
                $errFile = "Please Choose file";                
                $cookie_name = 'FileError';
                $cookie_value = $errFile;
                setcookie($cookie_name, $cookie_value, time() + (60), "/"); 
                return Redirect::to('/adminpanel');
            } 
        unset ($_COOKIE['FileError']);
        $importbtn= Request::get('submit');  
        if(isset($importbtn))
        {   
            $filename = Input::file('file')->getClientOriginalName();            
            $Dpath = base_path();
            $upload_success =Input::file('file')->move( $Dpath, $filename); 

            // xls to csv conversion
            $nameOnly = explode(".",$filename);
            $newCSV =$nameOnly[0]."."."csv";
            $PathnewCSV= $Dpath."/".$newCSV ;
            $myfile = fopen($PathnewCSV, "w");
            
        app('App\Http\Controllers\AdminController')->convertXLStoCSV($upload_success, $PathnewCSV);

        Excel::filter('chunk')->selectSheetsByIndex(0)->load($PathnewCSV)->chunk(1500, function($results){ 
                    $i=0;
                    
                    $data = $results->toArray();
                    foreach($data as $data) {
                        $i+=1;
                       
                        app('App\Http\Controllers\AdminController')->ValidateAdmin($data,$i);
                    }
  
            });
            //remove temperorary csv file
       chmod($PathnewCSV, 0777); 
        } 
        return Redirect::to('/adminpanel');            
    } 
 




public function TestSerialNumber()
{
  $serialnumber=input::get('SerialNumber');
  if ($serialnumber == Auth::user()->SerialNumber) {
return view('admin.chartadmin');
}
else
  { return redirect('/login');
 } 
}
    





}