<?php

namespace App\Http\Controllers;
use Auth;
use App\Http\Requests;
use App\User;
use App\Gps;
use App\Contractor;
use DB;
use App\Http\Controllers\Controller;
use Request;
use Excel;
use File;
use Illuminate\Support\Facades\Response;
use Validator;
use Illuminate\Support\Facades\Redirect;
use Input;
use Session;
use Geocode;
use Cache;
use URL;
use View;
use Khill\Lavacharts\Laravel\LavachartsFacade as Lava;
use Khill\Lavacharts\Lavacharts;
use TableView;
use App\Dategps;
use PHPExcel; 
use PHPExcel_IOFactory; 
use App\Product;
use App\Order;

class AdminGpsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
public function __construct()
   {
       $this->middleware('auth');
   }
  public function index()
    {
      try {

 if (Auth::check())
  {


    if(Auth::user()->can('show-Visits'))
      {
      //  $users=user::where('role','User')->get();
        $gps=Gps::join('users','users.id','=','gps.user_id')->
        where('NDate','2017-10-10')->select('gps.*','users.Name as username')->get();
        return view('gps.index',compact('gps'));
  }
    else 
      {
  return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
  return redirect('/login');
    }

    }

 public function updategps()
     {
        $gpsid = Input::get('pk');  
        $column_name = Input::get('name');
        $column_value = Input::get('value');
    
        $gps = gps::whereId($gpsid)->first();
        $gps-> $column_name=$column_value;

        if($gps->save())

        return \Response::json(array('status'=>1));
    else 
        return \Response::json(array('status'=>0));
     }






















public function destroy($id)
    {
        try {

 if (Auth::check())
  {


    if(Auth::user()->can('delete-Adminpanel'))
      {
        $gps=Gps::find($id);
        $gps->delete();
        return redirect('/gps');

      }
    else 
      {
  return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
  return redirect('/login');
    }

    }

 public function reportgpsdata()
    { 
    //  $users=User::withTrashed()->lists('Name','id');
       $users=User::lists('Name','id');
        $usersall=input::get('users');
      
        $date = Input::get('End_Date');  
        $date2=Input::get('Start_Date');



        $range=array($date2,$date);

if ($usersall[0]==='all') {
  # code...

$reportgps=DB::table('gps')
->leftjoin('contractors','contractors.id','=','gps.contractor_id')

->join('users','users.id','=','gps.user_id')

->leftjoin('orders', 'orders.gps_id', '=', 'gps.id')

->leftjoin('products','products.id','=','orders.Product_id')
->leftjoin('sale_products','sale_products.id','=','orders.SaleProduct_id')
->whereBetween('gps.NDate',$range)
->select('gps.*','orders.*','users.Name As PName','products.Cement_Name','sale_products.Name AS Sname','gps.Type as GpsType','contractors.Name','contractors.Tele1','contractors.Government As Cgov','users.Government As Pgov')->get();



 }

         else
         {

          $reportgps=DB::table('gps')
->leftjoin('contractors','contractors.id','=','gps.contractor_id')

->join('users','users.id','=','gps.user_id')

->leftjoin('orders', 'orders.gps_id', '=', 'gps.id')

->leftjoin('products','products.id','=','orders.Product_id')
->leftjoin('sale_products','sale_products.id','=','orders.SaleProduct_id')
->whereBetween('gps.NDate',$range)
->whereIn('gps.user_id',$usersall)
->select('gps.*','orders.*','users.Name As PName','products.Cement_Name','sale_products.Name AS Sname','gps.Type as GpsType','contractors.Name','contractors.Tele1','contractors.Government As Cgov','users.Government As Pgov')

->get();
          
         }

            
             return view( 'gps/reportgps',compact('reportgps','users'));

    }
 public function reportgps()
    {
        //try {

 // if (Auth::check())
 //  {


 //    if(Auth::user()->can('show-reportgps'))
 //      {
  $users=User::lists('Name','id'); 
$reportgps=[];
return view('gps/reportgps',compact('users','reportgps')); 
   }
  //   else 
  //     {
  // return view('errors.403');
  //     }

  //  }//endAuth

// else
// {
// return redirect('/login');
// }

// }//endtry
// catch(Exception $e) 
//     {
//   return redirect('/login');
//     }
       // }


}
