<?php

namespace App\Http\Controllers;

use App\Item;
use Auth;
use App\Http\Requests;
use App\Government;
use App\SubActivity;
use DB;
use App\Http\Controllers\Controller;
use Request;
use Excel;
use File;
use Illuminate\Support\Facades\Response;
use Validator;
use Illuminate\Support\Facades\Redirect;
use Input;
use App\Contractor;
use App\ActivityItem;
use App\Userdolist;
use Crypt;
use Mail;
use App\Detail;
use App\Gps;
use App\Datawarehouse;
use Carbon;
use App\District;
class TestController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {       
    try 
    {
      if (Auth::check())
      {
        if(Auth::user()->can('show-winer_contractors'))
        {
          $subactivty=SubActivity::all()->lists('Name','id');
          return view('tests.index',compact('subactivty'));
        }
        else 
        {
          return view('errors.403');
        }
      }//endAuth
      else
      {
        return redirect('/login');
      }
    }//endtry
    catch(Exception $e) 
    {
      return redirect('/login');
    }  
  }

  public function ContractorAmountReport()
  {    
    try 
    {
      if (Auth::check())
      {
        if(Auth::user()->can('show-winer_contractors'))
        {
          $reportactivites=[];
          return view('marktingactitvities.ReportContractor',compact('reportactivites'));
        }
        else 
        {
          return view('errors.403');
        }
      }//endAuth
      else
      {
        return redirect('/login');
      }
    }//endtry
    catch(Exception $e) 
    {
      return redirect('/login');
    } 
  }
  public function ShowContractorAmountReport()
  {
    $date = Input::get('End_Date');  
    $date2=Input::get('Start_Date');
    $range=array($date2,$date);
    $reportactivites=Contractor::join('gps','gps.contractor_id','=','contractors.id')
                               ->join('orders','orders.gps_id','=','gps.id')
                               ->join('products','products.id','=','orders.Product_id')
                               ->whereBetween('gps.NDate',$range)
                               ->select('contractors.id','contractors.Name','contractors.Code','products.id','products.Cement_Name',DB::raw('Sum(orders.Amount) as AVGAmount'),DB::raw("MONTH(gps.NDate) as Month"),'contractors.Government')
                               ->groupBy('contractors.id','contractors.Name','contractors.Code','products.id','products.Cement_Name',DB::raw("MONTH(gps.NDate)"),'contractors.Government')
                               ->get();
    return view('marktingactitvities.ReportContractor',compact('reportactivites'));
  }

  //show blade for participated contractor
  public function MarketingActivityReport()
  {
    try 
    {
      if (Auth::check())
      {
        if(Auth::user()->can('show-winer_contractors'))
        {
          $reportactivites=[];
          return view('marktingactitvities.Report',compact('reportactivites'));
        }
        else 
        {
          return view('errors.403');
        }
      }//endAuth
      else
      {
        return redirect('/login');
      }
    }//endtry
    catch(Exception $e) 
    {
      return redirect('/login');
    } 
  }

  //show report for participated contractor
  public function ShowMarketingReport()
  {
    $date = Input::get('End_Date');  
    $date2=Input::get('Start_Date');
    $range=array($date2,$date);

    $activityitemids= DB::table('activityitem_attributes')
                      ->join('activity_items','activity_items.id','=','activityitem_attributes.activityitem_id')
                      ->join('sub_activities','sub_activities.id','=','activity_items.subactivity_id')
                      ->whereBetween('activityitem_attributes.Value',$range)
                      ->select('activityitem_attributes.activityitem_id')
                      ->groupBy('activityitem_attributes.activityitem_id')
                      ->lists('activityitem_attributes.activityitem_id');

    $reportactivites=DB::table('activitables')
                      ->join('activity_items','activity_items.id','=','activitables.activity_item_id')
                      ->join('contractors','contractors.id','=','activitables.activitable_id')
                      ->join('sub_activities','sub_activities.id','=','activity_items.subactivity_id')
                      ->whereIn('activitables.activity_item_id',$activityitemids)
                      ->where('activitables.activitable_type',"App\Contractor")
                      ->select('contractors.Code','contractors.Tele1','contractors.Tele2',
                      'contractors.Name','contractors.Government','contractors.District',
                      'activity_items.Code as activityname','activitables.created_at as Date',
                      'sub_activities.EngName as SubActivity','activitables.Item_Name as Prize',
                      'activitables.Quantity')
                      ->get();

    // foreach ($reportactivites as $value) {
    //  $ddate=date_parse($value->Date);
    //  $value->Date=$ddate["day"]."-".$ddate["month"]."-".$ddate["year"];
    // }
    return view('marktingactitvities.Report',compact('reportactivites'));
  }

  //to get activity item based on subactivity id or ids
  public function getactivity($id,$start,$end)
  {
    $subactivityids=explode(',', $id);
    $range=array($start,$end);

    if(in_array("all", $subactivityids))
    {         
      $activityitemids= DB::table('activityitem_attributes')
                          ->join('activity_items','activity_items.id','=','activityitem_attributes.activityitem_id')
                          ->whereBetween('activityitem_attributes.Value',$range)
                          ->select('activity_items.id','activity_items.Code')
                          ->groupBy('activity_items.id','activity_items.Code')
                          ->lists('activity_items.Code','activity_items.id');
    }
    else
    {
      $activityitemids= DB::table('activityitem_attributes')
                          ->join('activity_items','activity_items.id','=','activityitem_attributes.activityitem_id')
                          ->join('sub_activities','sub_activities.id','=','activity_items.subactivity_id')
                          ->whereIn('sub_activities.id',$subactivityids)
                          ->whereBetween('activityitem_attributes.Value',$range)
                          ->select('activity_items.id','activity_items.Code')
                          ->groupBy('activity_items.id','activity_items.Code')
                          ->lists('activity_items.Code','activity_items.id');
    }
    return \Response::json($activityitemids);
  }

  //to get district  based on government id or ids
  public function getdistrictbygovernment($id)
  {
    $governmentids=explode(',', $id);

    if(in_array("all", $governmentids))
    {         
      $districtids= $District=District::lists('Name','id');
    }
    else
    {
      $districtids=$District=District::whereIn('government_id',$governmentids)
                                      ->lists('Name','id');
    }
    return \Response::json($districtids);
  }

  //show blade for Winner contractor
  public function ActivityReport()
  {
    try 
    {
      if (Auth::check())
      {
        if(Auth::user()->can('show-winer_contractors'))
        {
          $reportactivites=[];
          $activity=ActivityItem::lists('Code','id');
          $subactivties=SubActivity::lists('Name','id');
          return view('marktingactitvities.ActivityReport',compact('reportactivites','subactivties','activity')); 
        }
        else 
        {
          return view('errors.403');
        }
      }//endAuth
      else
      {
        return redirect('/login');
      }
    }//endtry
    catch(Exception $e) 
    {
      return redirect('/login');
    } 
  }
  //show report for Winner contractor
  public function ShowActivityReport()
  {
    $subactivties=SubActivity::lists('Name','id');
    $activity=ActivityItem::lists('Code','id');
    $date = Input::get('End_Date');  
    $date2=Input::get('Start_Date');
    $activityitemids=Input::get('Activity');
    $range=array($date2,$date); 
    $reportactivites=DB::table('activity_contractors_retailers')
                        ->join('activity_items','activity_items.id','=','activity_contractors_retailers.activityitem_id')
                        ->join('contractors','contractors.id','=','activity_contractors_retailers.contractor_id')
                        ->join('sub_activities','sub_activities.id','=','activity_items.subactivity_id')
                        ->whereBetween('activity_contractors_retailers.JoinDate',$range)
                        ->whereIn('activity_contractors_retailers.activityitem_id',$activityitemids)
                        ->select('contractors.Code','contractors.Tele1','contractors.Tele2','contractors.Name','contractors.Government','contractors.District','activity_items.Code as activityname','activity_contractors_retailers.JoinDate as Date','sub_activities.EngName as SubActivity')
                        ->get();
    return view('marktingactitvities.ActivityReport',compact('reportactivites','subactivties','activity'));
  }

  //show blade for Not Participated contractor
  public function NotParticipatedReport()
  {
    try 
    {
      if (Auth::check())
      {
        if(Auth::user()->can('show-winer_contractors'))
        {
          $reportactivites=[];
          $Government=Government::lists('Name','id');
          $District=District::lists('Name','id');
          return view('marktingactitvities.NotParticipatedReport',compact('reportactivites','Government','District'));
         }
        else 
        {
          return view('errors.403');
        }
      }//endAuth
      else
      {
        return redirect('/login');
      }
    }//endtry
    catch(Exception $e) 
    {
      return redirect('/login');
    } 
  }
  //show report for Not Participated contractor
  public function ShowNotParticipatedReport()
  {
    $Government=Government::lists('Name','id');
    $District=District::lists('Name','id');
    $date = Input::get('End_Date');  
    $date2=Input::get('Start_Date');
    $districtids = Input::get('District'); 
    $range=array($date2,$date);

    $contractorids=DB::table('activity_contractors_retailers')
                      ->whereBetween('activity_contractors_retailers.JoinDate',$range)
                      ->groupBy('contractor_id')
                      ->lists('contractor_id');

    unset($contractorids[0]); 
    $districtnames = District::whereIn('id', $districtids)->lists('Name');
    $reportactivites=Contractor::whereIn('District', $districtnames)
                                ->whereNotIn('id', $contractorids)
                                ->select('contractors.Code','contractors.Tele1','contractors.Tele2','contractors.Name','contractors.Government','contractors.District')                       
                                ->get();
    return view('marktingactitvities.NotParticipatedReport',compact('reportactivites','Government','District'));
  }

  //show blade for Analysis activity Report
  public function AnalysisactivityReport()
  {
    try 
    {
      if (Auth::check())
      {
        if(Auth::user()->can('show-winer_contractors'))
        {
          $reportactivites=[];
          return view('marktingactitvities.Analysisactivity',compact('reportactivites'));
         }
        else 
        {
          return view('errors.403');
        }
      }//endAuth
      else
      {
        return redirect('/login');
      }
    }//endtry
    catch(Exception $e) 
    {
      return redirect('/login');
    } 
  }
  //show report for Show Analysis activity Report
  public function ShowAnalysisactivityReport()
  {
    $date = Input::get('End_Date');  
    $date2=Input::get('Start_Date');
    $range=array($date2,$date);

    $activityitemids= DB::table('activityitem_attributes')
                        ->join('activity_items','activity_items.id','=','activityitem_attributes.activityitem_id')
                        ->whereBetween('activityitem_attributes.Value',$range)
                        ->select('activityitem_attributes.activityitem_id')
                        ->groupBy('activityitem_attributes.activityitem_id')
                        ->lists('activityitem_attributes.activityitem_id');
    $Contractordata= DB::table("activity_contractors_retailers")
                        ->join('contractors','contractors.id','=','activity_contractors_retailers.contractor_id')
                        ->select('contractors.Government',DB::raw('count(DISTINCT(activity_contractors_retailers.activityitem_id))  as countactivity'),DB::raw('count(DISTINCT(activity_contractors_retailers.contractor_id))  as countcontractor'))
                        ->whereIn('activity_contractors_retailers.activityitem_id',$activityitemids)
                        ->groupBy('contractors.Government')
                        ->get();
    $reportactivites=[];         
    foreach ($Contractordata as  $value) 
      {
        $query=DB::table('activitables')
                  ->join('activity_items','activity_items.id','=','activitables.activity_item_id')
                  ->join('contractors','contractors.id','=','activitables.activitable_id')
                  ->whereIn('activitables.activity_item_id',$activityitemids)
                  ->where('contractors.Government',$value->Government)
                  ->where('activitables.activitable_type','App\Contractor')
                  ->select(DB::raw('count(DISTINCT(activitables.activitable_id))  as countcontractor'))
                  ->first();
        if($query!=null)
          array_push($reportactivites,array(
                                      'Government' =>$value->Government,
                                      'Countactivity' =>$value->countactivity,
                                      'ParticipatedCount' =>$value->countcontractor,
                                      'WinnerCount' => $query->countcontractor));
        else
          array_push($reportactivites,array(
                                      'Government' =>$value->Government,
                                      'Countactivity' =>$value->countactivity,
                                      'ParticipatedCount' =>$value->countcontractor,
                                      'WinnerCount' => 0));
      } 


    return view('marktingactitvities.Analysisactivity',compact('reportactivites'));
  }

  //show blade for Analysis activity Report
  public function PriceperGovernmentReport()
  {
    try 
    {
      if (Auth::check())
      {
        if(Auth::user()->can('show-winer_contractors'))
        {
          $reportactivites=[];
          return view('marktingactitvities.PriceperGovernmentReport',compact('reportactivites'));
         }
        else 
        {
          return view('errors.403');
        }
      }//endAuth
      else
      {
        return redirect('/login');
      }
    }//endtry
    catch(Exception $e) 
    {
      return redirect('/login');
    } 
  }
  //show report for Show Analysis activity Report
  public function ShowPriceperGovernmentReport()
  {
    $date = Input::get('End_Date');  
    $date2=Input::get('Start_Date');
    $range=array($date2,$date);

    $Gpsdata=Gps::join('orders','orders.gps_id','=','gps.id')
                ->join('products','products.id','=','orders.Product_id')
                ->join('contractors','contractors.id','=','gps.contractor_id')
                ->whereBetween('gps.NDate',$range)
                ->whereNotNull('orders.Product_id')
                ->select('contractors.Government','orders.Product_id','products.Cement_Name',DB::raw('Sum(orders.Amount) as Amount'),DB::raw('count(DISTINCT(gps.contractor_id))  as countcontractor'))
                ->groupBy('contractors.Government','orders.Product_id','products.Cement_Name')
                ->get();
    $reportactivites=[];         
    foreach ($Gpsdata as  $value) 
      {
        $query=DB::table('prices')
                  ->whereBetween('Date',$range)
                  ->where('Government',$value->Government)
                  ->where('Product_id',$value->Product_id)
                  ->select(DB::raw('avg(Price)  as avaragePrice'))
                  ->first();
        if($query!=null)
          array_push($reportactivites,array(
                                      'Government' =>$value->Government,
                                      'Name' =>$value->Cement_Name,
                                      'ConsumedQuantity' =>$value->Amount,
                                      'NumofCont' =>$value->countcontractor,
                                      'AveragePrice' => $query->avaragePrice));
        else
          array_push($reportactivites,array(
                                      'Government' =>$value->Government,
                                      'Name' =>$value->Cement_Name,
                                      'ConsumedQuantity' =>$value->Amount,
                                      'NumofCont' =>$value->countcontractor,
                                      'AveragePrice' => 0));
      } 


    return view('marktingactitvities.PriceperGovernmentReport',compact('reportactivites'));
  }
  //show blade for Best Contractor Report
  public function BestContractorReport()
  {
    try 
    {
      if (Auth::check())
      {
        if(Auth::user()->can('show-winer_contractors'))
        {
          $reportactivites=[];
          $Government=Government::lists('Name','id');
          $District=District::lists('Name','id');
          return view('marktingactitvities.BestContractorReport',compact('reportactivites','Government','District'));
         }
        else 
        {
          return view('errors.403');
        }
      }//endAuth
      else
      {
        return redirect('/login');
      }
    }//endtry
    catch(Exception $e) 
    {
      return redirect('/login');
    } 
  }
  //show report for Best Contractor Report
  public function ShowBestContractorReport()
  {
    $Government=Government::lists('Name','id');
    $District=District::lists('Name','id');
    $date = Input::get('End_Date');  
    $date2=Input::get('Start_Date');
    $NumOfActivity=Input::get('NumOfActivity');
    $districtids = Input::get('District'); 
    $range=array($date2,$date);
    $districtnames = District::whereIn('id', $districtids)->lists('Name');
    if($NumOfActivity==null)
      $NumOfActivity=1;


    $activityitemids= DB::table('activityitem_attributes')
                        ->join('activity_items','activity_items.id','=','activityitem_attributes.activityitem_id')
                        ->join('sub_activities','sub_activities.id','=','activity_items.subactivity_id')
                        ->whereBetween('activityitem_attributes.Value',$range)
                        ->select('activityitem_attributes.activityitem_id')
                        ->groupBy('activityitem_attributes.activityitem_id')
                        ->lists('activityitem_attributes.activityitem_id');
      
    $data=DB::table('activity_contractors_retailers')
            ->join('activity_items','activity_items.id','=','activity_contractors_retailers.activityitem_id')
            ->join('contractors','contractors.id','=','activity_contractors_retailers.contractor_id')
            ->join('sub_activities','sub_activities.id','=','activity_items.subactivity_id')
            ->whereIn('activity_contractors_retailers.activityitem_id',$activityitemids)
            ->whereIn('contractors.District',$districtnames)
            ->select('contractors.id','contractors.Name','contractors.Tele1','contractors.District','contractors.Government',DB::raw('count(DISTINCT(activity_contractors_retailers.activityitem_id)) as Count'),'contractors.Code')
            ->groupBy('contractors.id','contractors.Name','contractors.Code')
            ->havingRaw('Count >='.$NumOfActivity)          
            ->get();
    
    $reportactivites=[];         
    foreach ($data as  $value) 
      {
        $query=DB::table('activitables')
                  ->join('activity_items','activity_items.id','=','activitables.activity_item_id')
                  ->join('contractors','contractors.id','=','activitables.activitable_id')
                  ->whereIn('activitables.activity_item_id',$activityitemids)
                  ->where('activitables.activitable_id',$value->id)
                  ->where('activitables.activitable_type','App\Contractor')
                  ->select('activitables.Quantity')
                  ->first();
        if($query!=null)
          array_push($reportactivites,array('Name' => $value->Name,
                                      'Code' =>$value->Code,
                                      'District' =>$value->District,
                                      'Government' =>$value->Government,
                                      'Telephone' =>$value->Tele1,
                                      'ActivityCount' =>$value->Count,
                                      'QuantityPrize' => $query->Quantity));
        else
          array_push($reportactivites,array('Name' => $value->Name,
                                      'Code' =>$value->Code,
                                      'District' =>$value->District,
                                      'Government' =>$value->Government,
                                      'Telephone' =>$value->Tele1,
                                      'ActivityCount' =>$value->Count,
                                      'QuantityPrize' => 0));
      } 
    return view('marktingactitvities.BestContractorReport',compact('reportactivites','Government','District'));
  }


  //show blade for Promoter Activity Report
  public function PromoterActivityReport()
  {
    try 
    {
      if (Auth::check())
      {
        if(Auth::user()->can('show-winer_contractors'))
        {
          $reportactivites=[];
          $Government=Government::lists('Name','id');
          $District=District::lists('Name','id');
          return view('marktingactitvities.PromoterActivityReport',compact('reportactivites','Government','District'));
         }
        else 
        {
          return view('errors.403');
        }
      }//endAuth
      else
      {
        return redirect('/login');
      }
    }//endtry
    catch(Exception $e) 
    {
      return redirect('/login');
    } 
  }
  //show report for Promoter Activity Report
  public function ShowPromoterActivityReport()
  {
    $Government=Government::lists('Name','id');
    $District=District::lists('Name','id');
    $date = Input::get('End_Date');  
    $date2=Input::get('Start_Date');
    $districtids = Input::get('District'); 
    $range=array($date2,$date);

    $districtnames = District::whereIn('id', $districtids)->lists('Name');
    $Contractordata=Gps::join('users','users.id','=','gps.user_id')
                        ->join('activity_contractors_retailers','activity_contractors_retailers.contractor_id','=','gps.contractor_id')
                        ->whereBetween('gps.NDate',$range)
                        ->whereIn('users.District',$districtnames)
                        ->select('users.Name','users.id',DB::raw('COUNT(DISTINCT activity_contractors_retailers.contractor_id) as ParticipatedCount'))
                        ->groupBy('users.id','users.Name')
                        ->get();
    $reportactivites=[];
    foreach ($Contractordata as $cont) 
      {
        $promotercontractorids=Gps::where('gps.user_id',$cont->id)
                                  ->whereBetween('gps.NDate',$range)
                                  ->groupBy('gps.contractor_id')
                                  ->lists('gps.contractor_id');
        $countwinnercontractor=DB::table('activitables') 
                                  //->join('activity_items','activity_items.id','=','activitables.activitable_id')
                                  //->join('activityitem_attributes','activityitem_attributes.activityitem_id','=','activity_items.id')
                                  //->whereBetween('activityitem_attributes.Value',$range)
                                  ->where('activity_item_id','!=',236) 
                                  ->where('activitable_type','App\Contractor') 
                                  ->whereIn('activitable_id',$promotercontractorids)
                                  ->select(DB::raw('COUNT(DISTINCT activitable_id) as count'))
                                  ->first();
                                               
        array_push($reportactivites,array('Name' => $cont->Name,
                                 'WinnerCount' => $countwinnercontractor->count,
                                 'ParticipatedCount' => $cont->ParticipatedCount
                                 ));
      }
    return view('marktingactitvities.PromoterActivityReport',compact('reportactivites','Government','District'));
  }
  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function sendEmailReminder()
  {
    $user = Userdolist::findOrFail(5);
    $message="ssdjsoicjpc";
    $data["mail_message"] = "Hello!";
    Mail::send('emails.welcome', $data, function($message)
    {
      $message->to('hadeel.mostafa.cs@gmail.com')->from('doaa.elgheny2016@gmail.com')->subject('TEST');
    });
    return "Your email has been sent successfully";  
  }

  public function ENCrypt()
  {      
    $data="asd";
    $encrypted = Crypt::encrypt($data);
    dd($encrypted);
    $decrypted = Crypt::decrypt($encrypted);
    dd($decrypted);
  }

  public function getwinners($startdate,$enddate)
  {
    $start=$startdate;
    $end=$enddate;

    $activityitemids= DB::table('activityitem_attributes')
                        ->join('activity_items','activity_items.id','=','activityitem_attributes.activityitem_id')
                        ->join('sub_activities','sub_activities.id','=','activity_items.subactivity_id')
                        ->where('sub_activities.EngName','=','Contractors Program')
                        ->whereBetween('activityitem_attributes.Value',array($start,$end))
                        ->select('activityitem_attributes.activityitem_id')
                        ->groupBy('activityitem_attributes.activityitem_id')
                        ->lists('activityitem_attributes.activityitem_id');

    $Contractordata=DB::table('activity_contractors_retailers')
                      ->join('activity_items','activity_items.id','=','activity_contractors_retailers.activityitem_id')
                      ->join('contractors','contractors.id','=','activity_contractors_retailers.contractor_id')
                      ->whereIn('activity_contractors_retailers.activityitem_id',$activityitemids)
                      ->select('contractors.id','contractors.Name','contractors.Government','contractors.District','activity_contractors_retailers.activityitem_id')
                      ->groupBy('contractors.id','contractors.Name','contractors.Government','contractors.District','activity_contractors_retailers.activityitem_id')
                      ->get();
            
    $Contractorids=DB::table('activity_contractors_retailers')
                      ->join('activity_items','activity_items.id','=','activity_contractors_retailers.activityitem_id')
                      ->join('contractors','contractors.id','=','activity_contractors_retailers.contractor_id')
                      ->whereIn('activity_contractors_retailers.activityitem_id',$activityitemids)
                      ->select('contractors.id')
                      ->groupBy('contractors.id')
                      ->lists('contractors.id');

    $Gpsdata=Gps::join('orders','orders.gps_id','=','gps.id')
                ->join('products','products.id','=','orders.Product_id')
                ->whereBetween('gps.NDate',array($startdate,$enddate))
                ->whereIn('gps.contractor_id',$Contractorids)
                ->whereNotNull('orders.Product_id')
                ->select('gps.contractor_id','orders.Product_id','products.Cement_Name',DB::raw('Sum(orders.Amount) as Amount'))
                ->groupBy('gps.contractor_id','orders.Product_id','products.Cement_Name')
                ->get();

    $data=[];
    foreach ($Gpsdata as $gps) 
    {
      foreach ($Contractordata as $cont) 
      {
        if($gps->contractor_id==$cont->id)
        {
          array_push($data,array('id' => $cont->id,
                                 'Name' => $cont->Name,
                                 'Government' => $cont->Government,
                                 'District' => $cont->District,
                                 'Product_id' => $gps->Product_id,
                                 'Cement_Name' => $gps->Cement_Name,
                                 'Amount' => $gps->Amount,
                                 'activityitem_id' => $cont->activityitem_id
                                 ));
          break;
        }
      }
    }

    foreach ($data as  &$value) 
    {
      $point=Detail::where('Government','=',$value["Government"])
                    ->where('District','=',$value["District"])
                    ->where('Product_id','=',$value["Product_id"])
                    ->select('Points')
                    ->first();
      if($point!=null)
        $value["Amount"]*=$point->Points;
    }

    $fdata=[];
    foreach ($data as  &$value) 
    {
      if(count($fdata)==0)
      {              
        array_push($fdata,$value);
      }
      else
      {         
        foreach ($fdata as  &$key) 
        {
          $flage=0;
          if($key["id"]==$value["id"])
          {                  
            $key["Amount"]+=$value["Amount"];                  
            $flage=1;
          }
        }

        if($flage==0)
        {
          array_push($fdata,$value);
        }
      }
    }

    $governments=Government::lists('Name');
    $cities=[];
    foreach ($governments as  $value) 
    {
      $b=[]; 
      foreach ($fdata as $key ) 
      {
        if($key["Government"]==$value)
        {
          array_push($b,$key);                       
        }         
      }
      array_push($cities, $b);   
    }
    return $cities;
  }


  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
      $itemsid=Input::get('item');
      $itemsquantity=Input::get('quantity');
      $contractid=Input::get('id');
      $activityitem_id=Input::get('activityitem_id');
      $contractor=Contractor::whereId($contractid)->first();
      $activityitem=ActivityItem::find($activityitem_id);
      for($i=0; $i<count($itemsid)-1;$i++)
      {
        $itemsname=Item::whereId($itemsid[$i])->select('Name')->first();
        $activityitem->contractors()->attach($contractid,["Item_Name"=>$itemsname->Name,"Quantity"=>$itemsquantity[$i]]);        
      }
      return redirect('/tests');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id,$activityitem_id)
  {
    try 
    {
      if (Auth::check())
      {
        if(Auth::user()->can('create-winer_contractors'))
        {
          $items=item::join('datawarehouse_items','datawarehouse_items.item_id','=','items.id')
                      ->Where('datawarehouse_items.quantityinstock','!=',0)
                      ->select('items.Name','items.id')
                      ->lists('Name', 'id');
          $warehouse =Datawarehouse::select ('Name', 'id')
                                    ->lists('Name', 'id');
          return   view('tests.create',compact('id','activityitem_id','items','warehouse'));
        }
        else 
        {
          return view('errors.403');
        }
      }//endAuth
      else
      {
        return redirect('/login');
      }
    }//endtry
    catch(Exception $e) 
    {
      return redirect('/login');
    }
  }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
