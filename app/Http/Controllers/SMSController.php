<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;

use App\Http\Requests;

use Input;
use Auth;
use Session;
use Redirect;
use Carbon;
use DB;
use App\SMS;


class SMSController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $SMS=[];
        return view("sms.index",compact('SMS')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function showsmsreport()
    {
        $date = Input::get('End_Date');  
        $date2=Input::get('Start_Date');
        $range=array($date2,$date);
        $SMS=SMS::whereBetween(DB::raw('date(Date)'),$range)->get(); 
        return view("sms.index",compact('SMS'));

    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function StoreSms(Request $request)
    {
         $Data=Input::all();
//          foreach ($Data as $key => $value) {
//             $seconds = $value['date'] / 1000;
// $value_date= date("d-m-Y H:i:s", $seconds);
//             return json_encode($value_date);
//          }

         foreach ($Data as  $value) {


            $old_date_timestamp = strtotime($value["date"]);
            $new_date = date('Y-m-d H:i:s', $old_date_timestamp);
             $query=SMS::where('Sender_Tele',$value["address"])
              ->where('Body',$value["body"])
                ->where('Date',$new_date)
                ->first();
            // return json_encode($query);
            if($query == null || $query == [])
            {

                $newsms=new SMS();
                $newsms->Sender_Tele=$value["address"];
                $newsms->Body=$value["body"];
                $newsms->Date=$value["date"];
                $newsms->save();
            }
        }

        return json_encode(1);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
