<?php

namespace App\Http\Controllers;

use Auth;
use App\Http\Requests;
use App\User;
use DB;
use App\Http\Controllers\Controller;
use Request;
use Excel;
use File;
use Illuminate\Support\Facades\Response;
use Validator;
use Illuminate\Support\Facades\Redirect;
use Input;
use Session;
use Geocode;
use Cache;
use URL;
use View;
use Khill\Lavacharts\Laravel\LavachartsFacade as Lava;
use Khill\Lavacharts\Lavacharts;
use TableView;
use App\Dategps;
use PHPExcel; 
use PHPExcel_IOFactory; 
use App\Government;
use App\District;
use App\Retailer;
use App\Type;
use App\EndType;
use App\SubType;

class SubTypeController extends Controller
{
   public function __construct()
   {
       $this->middleware('auth');
   } /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try
        {
            if (Auth::check())
            {
                // if(Auth::user()->can('show-Marketing_Activity'))
                // {
                        Session::put('Type_id',null);
                        $SubTypes=SubType::all();
                        $types=Type::all()->lists('EngName','id');
                        return view('subtypes.index',compact('SubTypes','types'));

                // }
                // else 
                // {
                //     return view('errors.403');
                // }

            }//endAuth
            else
            {
                return redirect('/login');
            }

        }//endtry
        catch(Exception $e) 
        {
            return redirect('/login');
        }
    }
    public function showsubtype($id)
    {
        try
        {
            if (Auth::check())
            {
                // if(Auth::user()->can('show-sub_activities'))
                // {

                        Session::put('Type_id', $id);
                        $SubTypes=SubType::where('Type_Id',$id)->get();
                        $types=Type::all()->lists('EngName','id');
                        return view('subtypes.index',compact('SubTypes','types'));   
                // }
                // else 
                // {
                //     return view('errors.403');
                // }
            }//endAuth
            else
            {
                return redirect('/login');
            }
        }//endtry
        catch(Exception $e) 
        {
            return redirect('/login');
        }   
    }
    public function checksubtypename()
    {
    
        $SubTypename=Input::get('engname');
        $SubType=SubType::where('EngName','=',$SubTypename)->first();
        if($SubType==[])     
            $isAvailable = true;
        else
            $isAvailable = false;
        return \Response::json(array('valid' =>$isAvailable,));
    }
    public function checksubtypeengname()
    {
    
        $SubTypename=Input::get('name');
        $SubType=SubType::where('Name','=',$SubTypename)->first();
        if($SubType==[])     
            $isAvailable = true;
        else
            $isAvailable = false;
        return \Response::json(array('valid' =>$isAvailable,));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try
        {
            if (Auth::check())
            {
                // if(Auth::user()->can('show-Marketing_Activity'))
                // {
                        $SubType = New SubType;
                        $SubType->Name=Input::get('name');
                        $SubType->EngName=Input::get('engname'); 
                        $SubType->Type_Id=Input::get('type_id'); 
                        $SubType->save();
                        $Typeid=Session::get('Type_id');
                        if($Typeid!=null)
                            return redirect('/showsubtype/'.$Typeid);
                        else       
                            return redirect('/subtypes');
                // }
                // else 
                // {
                //     return view('errors.403');
                // }

            }//endAuth
            else
            {
                return redirect('/login');
            }

        }//endtry
        catch(Exception $e) 
        {
            return redirect('/login');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update()
    {
        try
        {
            if (Auth::check())
            {
                // if(Auth::user()->can('show-Marketing_Activity'))
                // {
                        $SubTypeid = Input::get('pk');  
                        $column_name = Input::get('name');
                        $column_value = Input::get('value');   
                        $SubType = SubType::whereId($SubTypeid)->first();
                        $SubType-> $column_name=$column_value;
                        if($SubType->save())
                            return \Response::json(array('status'=>1));
                        else 
                            return \Response::json(array('status'=>0));

                // }
                // else 
                // {
                //     return \Response::json(array('status'=>'You do not have permission.'));
                // }

            }//endAuth
            else
            {
                return redirect('/login');
            }

        }//endtry
        catch(Exception $e) 
        {
            return redirect('/login');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try
        {
            if (Auth::check())
            {
                // if(Auth::user()->can('show-Marketing_Activity'))
                // {
                        $SubType=SubType::find($id);
                        $SubType->delete();
                        $Typeid=Session::get('Type_id');
                        if($Typeid!=null)
                            return redirect('/showsubtype/'.$Typeid);
                        else       
                            return redirect('/subtypes');
                // }
                // else 
                // {
                //     return view('errors.403');
                // }

            }//endAuth
            else
            {
                return redirect('/login');
            }

        }//endtry
        catch(Exception $e) 
        {
            return redirect('/login');
        }
    }
}
