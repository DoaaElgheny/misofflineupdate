-- phpMyAdmin SQL Dump
-- version 4.6.5.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Aug 07, 2018 at 12:47 AM
-- Server version: 5.5.54
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `newonlinedatabase`
--

-- --------------------------------------------------------

--
-- Table structure for table `safetransaction`
--

CREATE TABLE `safetransaction` (
  `id` int(10) UNSIGNED NOT NULL,
  `Datetrans` date DEFAULT NULL,
  `Balance` int(11) DEFAULT NULL,
  `Notes` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `safetransaction`
--

INSERT INTO `safetransaction` (`id`, `Datetrans`, `Balance`, `Notes`, `created_at`, `updated_at`) VALUES
(3, '2018-07-19', 50000, 'بداية الخزنة', '2018-07-19 12:00:33', '2018-07-19 12:00:33');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `safetransaction`
--
ALTER TABLE `safetransaction`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `safetransaction`
--
ALTER TABLE `safetransaction`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
