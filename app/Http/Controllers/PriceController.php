<?php

namespace App\Http\Controllers;

use Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use App\Product;
use App\Price;
use DB;
use Auth;
use App\Company;
use App\Government;
use App\District;
use App\User;
use App\Activity;
use Excel;
use DateTime;
use Carbon;

use App\ActivityItem;
use App\Contractor;

class PriceController extends Controller
{public function __construct()
   {
       $this->middleware('auth');
   }
    public function FirstReport()
    {
    
        $product_id = Price::groupby('product_id')->get();
        $product_ids= [];
        for ($i =0; $i<count($product_id) ;$i++) {
            $id = $product_id[$i]->product_id;
            $product_ids[$i] = $id;
        } 
        $products = Product::groupby('cement_name')->whereIn('id', $product_ids)->get();    
        return view('price.first_form', compact('products'));
    }
     public function FirstResult()
     {
        $inputs =Input:: all();
        $messages = array(
            'required'      => 'This value is required',
        );
        $rules = array(
            'cement_name'   => array('required'),
            'from'   => array('required'),
            'to'   => array('required'),
            'gov'   => array('required')
        );

        $validator = Validator::make(Input::all(), $rules,$messages);
        if ($validator->fails()) {
            return redirect('/FirstReport')
                            ->withErrors($validator)->withInput();
        }
        else{
        $dates = Price::whereBetween('date', array($inputs['from'], $inputs['to']))
                ->select('date')->groupby('date')->get();

        $cities = Price::where('goverment',$inputs['gov'])
                ->select('city')->groupby('city')->get();

        $product_id = Price::where('product_id',$inputs['cement_name'])
                ->select('product_id')->groupby('product_id')->get();

        $stocksTable = \Lava::DataTable();
        $stocksTable->addDateColumn('Date');
        for ($col=0; $col<count($cities) ; $col++) { 
            $stocksTable->addNumberColumn($cities[$col]->city);
        }

        //if there is null or 0 value change it with the previous or next not null value
    foreach ($cities as $city) {
        for ($cc=0; $cc<count($dates); $cc++) { 
            $OneProductPrice =Price::where('product_id', $product_id[0]->product_id)
                                             ->where('city', $city->city)
                                             // ->where('date', $dates[$cc]->date)
                                             ->get();
            // dd($OneProductPrice);
            if( isset($OneProductPrice[$cc]->price )){    
                $OldVla = $OneProductPrice[$cc]->price;
                // dd($OldVla);
                if(isset($OneProductPrice[$cc+1])){
                    $NewVla = $OneProductPrice[$cc+1]->price;
                    // dd($NewVla);
                    if ($NewVla == null || $NewVla == 0 ) {
                        $tmp_id = $OneProductPrice[$cc+1]->id;
                        $tmp_price = Price::find($tmp_id);
                        $tmp_price->price = $OneProductPrice[$cc]->price;
                        $tmp_price->save();
                    }
                    if ($OldVla == null || $OldVla == 0 ) {
                        $tmp_id = $OneProductPrice[$cc]->id;
                        $tmp_price = Price::find($tmp_id);
                        $tmp_price->price = $OneProductPrice[$cc+1]->price;
                        $tmp_price->save();
                    }
                }
            }

        }   
   
    } 
        for ($y=0; $y<count($dates); $y++) { 
            $Rowprices = [];
            $rowData = [];
            array_push($rowData, $dates[$y]->date);
            
            foreach ($cities as $city) {
                $Rowprices = Price::where('date', $dates[$y]->date)
                                ->where('city', $city->city)
                                ->where('product_id', $product_id[0]->product_id)
                                ->select('price')->get(); 
                foreach ($Rowprices as $Rowprices) {
                    array_push($rowData, $Rowprices->price);
                }
                
            }
            $stocksTable->addRow($rowData);
        }         
        $product_name =Product::where('id', $inputs['cement_name'])
                                ->select('cement_name')->get(); 

        $chart = \Lava::LineChart('MyStocks', $stocksTable,[
                'title'=>"محافظة ".$inputs['gov']." --  نوع الاسمنت ".$product_name[0]->cement_name,
                'titleTextStyle' => [
                    'color'    => '#eb6b2c',
                    'fontSize' => 25, 
                ],

        ]);
        $PricesTable =Price::where('product_id', $inputs['cement_name'])
                                ->where('goverment', $inputs['gov'])
                                ->get(); 
        return view('price.charts', compact('PricesTable')); 
        }
    } 
     public function ReportForm(){
        $product_id = Price::groupby('product_id')->get();
        $product_ids= [];
        for ($i =0; $i<count($product_id) ;$i++) {
            $id = $product_id[$i]->product_id;
            $product_ids[$i] = $id;
        }  
        $products =Product::groupby('cement_name')->whereIn('id', $product_ids)->get();
       
        $govs =Price::groupby('goverment')->get();        
        return view('price.report_form', compact('products','govs'));
    }
     public function GenerateReport(){
        $inputs = Input :: all();
        // dd($inputs);
        $messages = array(
            'required'      => 'This value is required',
        );
        $rules = array(
            'cement_name'   => array('required'),
            'from'   => array('required'),
            'to'   => array('required'),
        );

    $validator = Validator::make(Input::all(), $rules,$messages);
    if ($validator->fails()) {
        return redirect('/ReportForm')
                        ->withErrors($validator)->withInput();
    }
    else{
        $companies = Company::all();
        $company_names = Company::select('name')->get();
        $product_ids = [];
        $Rowprices = [];
        $rowData = [];
        for ($i =0; $i<count($companies) ;$i++) {
            $id =  Product::where('cement_name',$inputs['cement_name'])
                    ->where('company_id',$companies[$i]->id)->select('id')
                    ->get();
            // dd($id);
            if (isset($id[$i]->id)) {
              $product_ids[$i] = $id[$i]->id;
            }
            
        } 
        // dd($product_ids);    
        $OneProductPrice = [];
        $dates = Price::whereBetween('date', array($inputs['from'], $inputs['to']))
                ->select('date')->groupby('date')->get();   

//if there is null or 0 value change it with the previous or next not null value
    foreach ($product_ids as $product_id) {
        $OneProductPrice =Price::where('product_id', $product_id)->get(); 
            for ($cc=0; $cc<count($dates); $cc++) { 
            if( isset($OneProductPrice[$cc]->price )){    
                $OldVla = $OneProductPrice[$cc]->price;
                if(isset($OneProductPrice[$cc+1])){
                    $NewVla = $OneProductPrice[$cc+1]->price;
                    if ($NewVla == null || $NewVla == 0 ) {
                        $tmp_id = $OneProductPrice[$cc+1]->id;
                        $tmp_price = Price::find($tmp_id);
                        $tmp_price->price = $OneProductPrice[$cc]->price;
                        $tmp_price->save();
                    }
                    if ($OldVla == null || $OldVla == 0 ) {
                        $tmp_id = $OneProductPrice[$cc]->id;
                        $tmp_price = Price::find($tmp_id);
                        $tmp_price->price = $OneProductPrice[$cc+1]->price;
                        $tmp_price->save();
                    }
                }
            }

        }   
   
    }
        $stocksTable = \Lava::DataTable();
        $stocksTable->addDateColumn('Date');
        for ($col=0; $col<count($company_names) ; $col++) { 
            $stocksTable->addNumberColumn($company_names[$col]->name);
        }

        for ($y=0; $y<count($dates); $y++) { 
            $Rowprices = [];
            $rowData = [];
            array_push($rowData, $dates[$y]->date);
            $Rowprices = Price::where('date', $dates[$y]->date)
                                ->select('price')->get();               
            for ($z=0; $z<count($Rowprices) ; $z++) { 
                array_push($rowData, $Rowprices[$z]->price);
            }
            $stocksTable->addRow($rowData);
        }
        // dd($stocksTable);
        $chart = \Lava::LineChart('MyStocks', $stocksTable,[
                'title'=>"نوع الاسمنت  ".$inputs['cement_name'],
                'titleTextStyle' => [
                    'color'    => '#eb6b2c',
                    'fontSize' => 25, 
                ],

        ]);


        return view('price.PriceCharts'); 
        } 
    }    

    public function index()
    {
          try {

 if (Auth::check())
  {


    if(Auth::user()->can('Show Price'))
      {

      //your code

     $prices=Price::all();   
       $company= Company::lists('Name','id');
        $cement =product::lists('cement_name', 'id');
        $products = Product::all();  
        //Doaa Elgheny 27/10/2016  
        $governments=Government::select('Name','id')->lists('Name','id');
        $districts=District::select('Name','id')->lists('Name','id');
        $user=user::select('Name','id')->lists('Name','id');
        
        $ProductShow=Product::select('Cement_Name','id')->lists('Cement_Name','id');
       // dd($prices);
        return view('price.index',compact('prices','company','cement','products','governments','districts','user','ProductShow'));   
    }
    else 
      {
    return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
      
  	}


public function getproducts($id)
    {
       
        $products=product::where('company_id','=',$id) ->get();
        $options = array();
        foreach ($products as $product) {
           $options += array($product->id => $product->cement_name);

        }
     
        return Response::json($options);
    }


  	public function destroy($id)
    {

  try {

 if (Auth::check())
  {


    if(Auth::user()->can('Delete Price'))
      {
        $price = Price::find($id);
        $price->delete();
        return redirect('/price'); 
          }
    else 
      {
    return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }      
    }

	public function priceupdate(){


  try {

 if (Auth::check())
  {


    if(Auth::user()->can('Update Price'))
      {

      //your code

    


        $price_id = Input::get('pk');  
        $column_name = Input::get('name');
        $column_value = Input::get('value'); 
          if (Input::get('name')=='District')
        {
          
        $District = District::where('id', '=',Input::get('value'))
                    ->select('Name')->first();
                    $column_value = $District->Name;
         }
        if (Input::get('name')=='Government')
        {
           
            $Government =Government::where('id', '=',Input::get('value'))
                    ->select('Name')->first();
                $column_value = $Government->Name;

        }
        $compData = Price::whereId($price_id)->first();  
        $compData-> $column_name=$column_value; 
        
        if($compData->save()) 
            return \Response::json(array('status'=>1));
        else 
            return \Response::json(array('status'=>0));
       
          }
    else 
      {
   return \Response::json(array('status'=>'You do not have permission.'));
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
    }

    public function store(Request $request){

  try {

 if (Auth::check())
  {


    if(Auth::user()->can('Store Price'))
      {
         $GLOBALS['doubleprice']= array();
          $Government =Government::where('id', '=',Input::get('Government'))
                    ->select('Name')->first();
        //to get Name of districts               
       $District = District::where('id', '=',Input::get('District'))
                    ->select('Name')->first();

       $pricedouble = Price::where('Government', '=', $Government->Name)
                        ->where('District', '=',$District->Name)
                        ->where('Date', '=',Input::get('Date'))
                        ->where('Product_id', '=',Input::get('Product_id'))
                        ->where('Price', '=',Input::get('price'))
                        ->first();
                       // dd( $pricedouble);
        if($pricedouble==null)
        {
            	$price = new Price;
                $price->Government=$Government->Name;
                $price->District=$District->Name;
            	$price->Date=Input::get('Date');
            	$price->Price=Input::get('price');
                $price->Product_id=Input::get('Product_id');    
          	    $price->User_id=\Auth::user()->id;
                $history = $price->logs;
            	$price->save();
            }
            else
                array_push($GLOBALS['doubleprice'],'هذا السعر تم ادخاله مسبقا ');

               if ( !empty ($GLOBALS['doubleprice'] )) {
            $GLOBALS['doubleprice'] = array_unique($GLOBALS['doubleprice']);
            $PriceCompErr=implode(" \n ",$GLOBALS['doubleprice']);
            $PriceCompErr = nl2br($PriceCompErr);  
            $cookie_name = 'doubleprice';
            $cookie_value = $PriceCompErr;
            setcookie($cookie_name, $cookie_value, time() + (10), "/");
        }
            	return redirect('/price');

         }
    else 
      {
    return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }

        
    }
  public function audtiting(){
       
        $cement =product::lists('cement_name', 'id');

           $company =company::lists('name', 'id');

    
        return view('price/audtiting',compact('cement','company'));
        
    }





  public function  importpricefile()
{
    try
    {

  $temp= Request::get('submit'); 


   if(isset($temp))

 { 


   $filename = Input::file('file')->getClientOriginalName();
     

     $Dpath = base_path();


     $upload_success =Input::file('file')->move( $Dpath, $filename);
    

       Excel::load($upload_success, function($reader)
       {   
     
         


    $results = $reader->get()->all();
// dd( $results);
$i=0;
       foreach ($results as $data)
        {
        app('App\Http\Controllers\PriceController')->ValidatePrice($data,$i);
        $i+=1;
// $price =new Price();
// $price->Date =$data['date'];
// $price->Price =$data['price'];
// $price->Government =$data['government'];
// $price->District =$data['district'];


 
//      $product_name= Product::where('Cement_Name',$data['product_name'])->pluck('id')->first();

// $price->Product_id =$product_name;
// $User_id= User::where('Name',$data['name'])->pluck('id')->first();

//    $price->User_id =$User_id;

// $price->save();
       
        }
   
    });


  }

   return redirect('/price'); 
   
 }
 catch(Exception $e)
 {
   return redirect('/price'); 
 }

}

   public function history(){

    try {
              $rules = array(
                
                'company'  => array('required','not_in:null'),  
                'product'  => array('required','not_in:0')
      
              );
              $messages = [
                        'required'           =>     'please Enter Data ',
                        
                        'not_in'             =>    'please Enter Data'
                    ];
            $Validator=Validator:: make(Input::all(),$rules,$messages);
            if($Validator->fails()){
                return redirect('/audtiting')->withErrors($Validator)->withInput();
            }
            else
                {
    $data=[];
        $details=[];
    $company=Input::get('company');
     $company_name=Company::select('name')
             ->where('id','=',$company)
              ->value('name');

    $cement=Input::get('product');
    $cement_name=Product::select('cement_name')
             ->where('id','=',$cement)
              ->value('cement_name');
              
    $id= Price::select('id')
             ->where('product_id','=',$cement)
              ->pluck('id');
// dd($id);
     

foreach ($id as $key => $id) {
 $price= Price::find($id)->logs;
// $o= $price->pluck('price');
 $details1= Price::select('date','city','goverment')
             ->where('id','=',$id)
             ->get(); 
             // dd($details);
        array_push($data,$price);
          array_push($details,$details1);
  
      


}
 
       
       
       return view('price/historyreport',compact('data','details','cement_name','company_name'));
        }
    }
         catch (Exception $e) {
          return redirect('/price');       
      } 
    }
     public function ValidatePrice($data, $i){
        // dd($data);
        if(!isset($GLOBALS['price'])) { $GLOBALS['price']= array(); } 
        if(!isset($PriceErr)) { $PriceErr = 'البيانات غير صحيحة او غير مكتملة للصف رقم: '; }
        
        if(!isset($GLOBALS['price_product'])) { $GLOBALS['price_product']= array(); } 
        if(!isset($PriceProErr)) { $PriceProErr = 'لا يوجد منتج مسجل للصف رقم: '; }
        
        if(!isset($GLOBALS['price_comp'])) { $GLOBALS['price_comp']= array(); } 
        if(!isset($PriceCompErr)) { $PriceCompErr = 'لا توجد شركة مسجل للصف رقم: '; }

        $date_regex = preg_match('/^(?=\d)(?:(?!(?:1582(?:\.|-|\/)10(?:\.|-|\/)(?:0?[5-9]|1[0-4]))|(?:1752(?:\.|-|\/)0?9(?:\.|-|\/)(?:0?[3-9]|1[0-3])))(?=(?:(?!000[04]|(?:(?:1[^0-6]|[2468][^048]|[3579][^26])00))(?:(?:\d\d)(?:[02468][048]|[13579][26]))\D0?2\D29)|(?:\d{4}\D(?!(?:0?[2469]|11)\D31)(?!0?2(?:\.|-|\/)(?:29|30))))(\d{4})([-\/.])(0?\d|1[012])\2((?!00)[012]?\d|3[01])(?:$|(?=\x20\d)\x20))?((?:(?:0?[1-9]|1[012])(?::[0-5]\d){0,2}(?:\x20[aApP][mM]))|(?:[01]\d|2[0-3])(?::[0-5]\d){1,2})?$/' , $data['date']);
        $price_regex = preg_match('/^[0-9]*\.?[0-9]*$/' , $data['price']);
        $gov_regex = preg_match('/^[\pL\s]+$/u' , $data['government']);
        $city_regex = preg_match('/^[\pL\s]+$/u' , $data['district']);
       
        if(isset($data['company'])){

            $comp_name= Company::where('Name','=',$data['company'])->pluck('Name')->first();
        }
        else{
            array_push($GLOBALS['price_comp'],$i);
        }
    
        if ($data['date'] == " ") {
            $data['date']= null;
        }
        if ($data['price'] == " ") {
            $data['price']= null;
        }
        if ($data['government'] == " ") {
            $data['government']= null;
        }
        if ($data['district'] == " ") {
            $data['district']= null;
        }
        if($data['product_name']){
            $product_id= Product::where('Cement_Name',$data['product_name'])->pluck('id')->first();
        }
        else{
               array_push($GLOBALS['price_product'],$i); 
            }

    if(isset($comp_name)) {
       
        if($gov_regex == 1 && isset($data['government'])) {

             if($city_regex == 1 && isset($data['district'])) {

                 if($price_regex == 1 && isset($data['price'])) {

                    if($date_regex == 1 && isset($data['date'])) {
                    
                        if (isset($product_id)) {
                            $price =new Price();
                            $price->Date =$data['date'];
                            $price->Price =$data['price'];
                            $price->Government =$data['government'];
                            $price->District =$data['district'];
                            $product_name= Product::where('Cement_Name',$data['product_name'])->pluck('id')->first();
                            $price->Product_id =$product_name;
                            $User_id= User::where('Name',$data['name'])->pluck('id')->first();
                            $price->User_id =$User_id;
                            
                            try{

                                $price->save(); 
                            }
                            catch(\Exception $e ) {
                                // dd($e);
                                $parts = explode(' ', $data['date']);
                                
                            $repeted = $data['government']."-".$data['district']."-".$product_id."-".$data['price'].".00-".$parts[0];
                            $exist_string= "Duplicate entry '".$repeted."' for key 'prices_government_district_product_id_price_date_unique'";
                             
                            if ($exist_string == $e->errorInfo[2]) {  // product exist 
                                array_push($GLOBALS['price'],$i); 
                               // dd("dssd");
                                // $price_id= DB::table('prices')
                                //             ->where('Government',$data['government'])
                                //             ->where('District',$data['district'])
                                //             ->where('Product_id',$product_id)
                                //             ->pluck('id');
                                // $updated_price = Price::find($price_id[0]);                  
                                // $updated_price->Date =$data['date'];
                                // $updated_price->Price =$data['price'];
                                // $updated_price->Government =$data['government'];
                                // $updated_price->District =$data['district'];
                                // $product_name= Product::where('Cement_Name',$data['product_name'])->pluck('id')->first();
                                // $updated_price->Product_id =$product_name;
                                // $User_id= User::where('Name',$data['name'])->pluck('id')->first();
                                // $updated_price->User_id =$User_id;        
                                // $updated_price->save();
                                }
                            } //end catch
                        }
                        else{
                            array_push($GLOBALS['price_product'],$i); 
                        }
                        
                    }
                    else{
                        array_push($GLOBALS['price'],$i);  
                    }
                }
                else{
                    array_push($GLOBALS['price'],$i);
                }
            }
            else{
                    array_push($GLOBALS['price'],$i); 
            }
        }
        else{
             array_push($GLOBALS['price'],$i);
        }
    }
    else{

    }

         if ( !empty ($GLOBALS['price_comp'] )) {
            $GLOBALS['price_comp'] = array_unique($GLOBALS['price_comp']);
            $PriceCompErr= $PriceCompErr.implode(" \n ",$GLOBALS['price_comp']);
            $PriceCompErr = nl2br($PriceCompErr);  
            $cookie_name = 'PriceCompErr';
            $cookie_value = $PriceCompErr;
            setcookie($cookie_name, $cookie_value, time() + (60), "/");
        }
        if ( !empty ($GLOBALS['price'] )) {
            $GLOBALS['price'] = array_unique($GLOBALS['price']);
            $PriceErr= $PriceErr.implode(" \n ",$GLOBALS['price']);
            $PriceErr = nl2br($PriceErr);  
            $cookie_name = 'PriceErr';
            $cookie_value = $PriceErr;
            setcookie($cookie_name, $cookie_value, time() + (60), "/");
        }
        if ( !empty ($GLOBALS['price_product'] )) {
            $GLOBALS['price_product'] = array_unique($GLOBALS['price_product']);
            $PriceProErr= $PriceProErr.implode(" \n ",$GLOBALS['price_product']);
            $PriceProErr = nl2br($PriceProErr);  
            $cookie_name = 'PriceProErr';
            $cookie_value = $PriceProErr;
            setcookie($cookie_name, $cookie_value, time() + (60), "/");
        }

     
    }
    public function importprice(){

        $file = Input::file('file');
        if($file == null){  //if no file selected  
                $errFile = "الرجاء اختيار الملف الملطلوب تحميلة";                
                $cookie_name = 'FileError';
                $cookie_value = $errFile;
                setcookie($cookie_name, $cookie_value, time() + (60), "/"); 
                return redirect('/price');
            } 
        unset ($_COOKIE['FileError']);
        $importbtn= Request::get('submit'); 
        if(isset($importbtn))
        {   
            $filename = Input::file('file')->getClientOriginalName();            
            $Dpath = base_path();
            $upload_success =Input::file('file')->move( $Dpath, $filename); 

            // xls to csv conversion
            $nameOnly = explode(".",$filename);
            $newCSV =$nameOnly[0]."."."csv";
            $PathnewCSV= $Dpath."\\".$newCSV ;
            // chmod($PathnewCSV, 0777);
            $myfile = fopen($PathnewCSV, "w");

        app('App\Http\Controllers\ProductController')->convertXLStoCSV($upload_success, $PathnewCSV);

        Excel::filter('chunk')->selectSheetsByIndex(0)->load($PathnewCSV)->chunk(150, function($results){ 
                    $data = $results->toArray();
                    $i=0;
                foreach($data as $data) {
                        $i+=1;
                        app('App\Http\Controllers\PriceController')->ValidatePrice($data,$i);
                    } // end foreach data
            });
            //remove temperorary csv file
            chmod($PathnewCSV, 0777);
            // unlink($PathnewCSV);   
        } 
        return redirect('/price');  

                    
    }

    //show  price per Day
public function ShowReportPrice()
{
    try {
        
 

     $data=array();


// dd(Input::get('product'));
         $product=Input::get('product');
         $Start=Input::get('StartDate');
         $End=Input::get('EndDate');
         $range=[$Start,$End];
          // dd($range);
          $government=Government::find(Input::get('Government'));
      
           $District=District::find(Input::get('District'));
      $a=[];  
        if($District == null)
        {
            $Dates= Price::whereIn('Product_id',$product)
            ->where('Government',$government->Name)
            ->whereBetween('Date', $range)
            ->select('prices.Date')
            ->groupby('Date')
            ->orderBy('Date', 'asc')->get();
// dd($Dates);
    }
    else
    {

        $Dates= Price::whereIn('Product_id',$product)
            ->where('Government',$government->Name)
           ->where('District',$District->Name)
            ->whereBetween('Date', $range)
            ->select('prices.Date')
            ->groupby('Date')
            ->orderBy('Date', 'asc')->get();

    }
foreach ($product as  $value) {
        # code...
$product= Product::where('id','=',$value)->value('Cement_Name');
$b=[];
$prevprice=0; 
    foreach ($Dates as $Date) {

  $price= Price::where('prices.Product_id','=',$value)
    // ->where('District',$District->Name)
    ->where('Government',$government->Name)
    ->where('Date', $Date->Date)
    ->join('products','products.id','=','prices.Product_id')
    ->select('products.Cement_Name',DB::raw( 'AVG(prices.Price) as Price') )  
   ->first();

  
if ($price->Price==null) {
    if($prevprice==0)
    {
array_push($b,array(
                                      'label' => $Date->Date,
                                      'y' => ''
                                      ));
    }
    else{

                                    array_push($b,array(
                                      'label' => $Date->Date,
                                      'y' => $prevprice
                                      ));
                     
           }                        
                     
      
    }  
else{
      
 
     $prevprice=$price->Price;            

                                    array_push($b,array(
                                      'label' => $Date->Date,
                                      'y' => $price->Price
                                      ));
                     
                                   
                    
                      

}
  }
                            array_push($a,array(
                                     'name' => $product,
                                      'data' =>$b
                                      ));
 $data['price']=$a;
  }
  


     return Response::json($data,  200, [], JSON_NUMERIC_CHECK);

        } catch (Exception $e) {
            
            $data=null;
             return Response::json($data,  200, [], JSON_NUMERIC_CHECK);

    }
}


   //show  price per Month
public function ShowReportMPrice()
{
    try {
        
 

     $data=array();


// dd(Input::get('product'));
    $product=Input::get('product');

         $year=Input::get('Start');
          // $End=Input::get('EndDate');
          // $range=[$Start,$End];
         
         $government_sent=Input::get('Government');
         if($government_sent != 'all')
      {
          $government=Government::find(Input::get('Government'));
      }
           $District=District::find(Input::get('District'));


          
      $a=[];  


      if($government_sent != 'all')
      {
        if($District == null)
        {
            $Dates= Price::whereIn('Product_id',$product)
            ->where('Government',$government->Name)
            ->where(DB::raw('YEAR(Date)'), $year)
            ->select('prices.Date',DB::raw("MONTH(prices.Date) as Month"))
            ->groupby('Month')
            ->orderBy('Date', 'asc')->get();
// dd($Dates);
    }
    else
    {

        $Dates= Price::whereIn('Product_id',$product)
            ->where('Government',$government->Name)
           ->where('District',$District->Name)
            ->where(DB::raw('YEAR(Date)'), $year)
            ->select('prices.Date',DB::raw("MONTH(prices.Date) as Month"))
            ->groupby('Month')
            ->orderBy('Date', 'asc')->get();


    }
}
else
{
     $Dates= Price::whereIn('Product_id',$product)
            ->where(DB::raw('YEAR(Date)'), $year)
            ->select('prices.Date',DB::raw("MONTH(prices.Date) as Month"))
            ->groupby('Month')
            ->orderBy('Date', 'asc')->get();

}








foreach ($product as  $value) {
        # code...
$product= Product::where('id','=',$value)->value('Cement_Name');
$b=[];
$prevprice=0; 
    foreach ($Dates as $Date) {


if($government_sent != 'all')
      {


  $price= Price::where('prices.Product_id','=',$value)
    // ->where('District',$District->Name)
    ->where('Government',$government->Name)
    ->where(DB::raw('MONTH(Date)'), $Date->Month)
    ->join('products','products.id','=','prices.Product_id')
    ->select('products.Cement_Name',DB::raw( 'AVG(prices.Price) as Price') )  
   ->first();

}
else
{
     $price= Price::where('prices.Product_id','=',$value)
    // ->where('District',$District->Name)
 //   ->where('Government',$government->Name)
    ->where(DB::raw('MONTH(Date)'), $Date->Month)
    ->join('products','products.id','=','prices.Product_id')
    ->select('products.Cement_Name',DB::raw( 'AVG(prices.Price) as Price') )  
   ->first();


}


           $dateObj   = DateTime::createFromFormat('!m', $Date->Month);
            //dd($value->Month);
           $monthName = $dateObj->format('F');
  
if ($price->Price==null) {
    if($prevprice==0)
    {
array_push($b,array(
                                      'label' => $monthName,
                                      'y' => ''
                                      ));
    }
    else{

                                    array_push($b,array(
                                      'label' => $monthName,
                                      'y' => $prevprice
                                      ));
                     
           }                        
                     
      
    }  
else{
 
     $prevprice=$price->Price;            

                                    array_push($b,array(
                                      'label' => $monthName,
                                      'y' => $price->Price
                                      ));
                                             

}
  }
                            array_push($a,array(
                                     'name' => $product,
                                      'data' =>$b
                                      ));
 $data['price']=$a;
  }
  


     return Response::json($data,  200, [], JSON_NUMERIC_CHECK);

        } catch (Exception $e) {
            
            $data=null;
             return Response::json($data,  200, [], JSON_NUMERIC_CHECK);

    }
}







public function CompanyDropDownData($id)
{
   $ids=explode(',', $id);
    // dd($ids);
 if(in_array("all", $ids))
{
        $Product=product::get();

}
else{

        $Product=Product::whereIn('Company_id',$ids) ->get();
}
        $options = array();
      foreach ($Product as $Product) {
      $options += array($Product->id => $Product->Cement_Name);
        }
     
        return \Response::json($options);
}


public function showchartprice(){
      try {

 if (Auth::check())
  {


   $company= Company::lists('Name','id');
  $product= Product::lists('Cement_Name','id');
   $governments= Government::lists('Name','id');
  $districts= District::lists('Name','id');

  return view('price.chartprices',compact('company','product','governments','districts'));

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }


}

public function showactivitycharts(){
      try {

 if (Auth::check())
  {


    if(Auth::user()->can('show-activitypricechart'))
      {

  return view('price.activitycharts');

      }
    else 
      {
    return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }


}
public function Activitycharts(){
          $Start=Input::get('StartDate');
          $End=Input::get('EndDate');
          $range=[$Start,$End];

  $activity =Activity::join('companies', 'companies.id', '=', 'activities.company_id')
  ->whereBetween('Start_date', $range)
                                      ->select(array('*',DB::raw('count(company_id)  as company')))
                                      ->groupBy('activities.company_id')
                                   ->get();
                                        $b=[];
                   foreach ($activity as  $value) {
                                    array_push($b,array(
                                      'label' => $value->Name,
                                      'y' => $value->company
                                      ));
}
  $data['activity']=$b;
     return Response::json($data,  200, [], JSON_NUMERIC_CHECK);
                                
}

//PriceReport///
public function showpricesanalysis()
{

  return view('price.shpricesanalysis');

}

public function chartpricesanalysis()
{

try{


          
         
 // Excel::create(' Price Report', function($excel)
 //       { 
$excel = new \PHPExcel();

   $government_sen_upper='Upper';
   $government_sen_lower='Lower';

   $Start=Input::get('StartDate');
   $End=Input::get('EndDate');

    $range=[$Start,$End];
        
         if($government_sen_upper == 'Upper')
      {


     $government_upper=Price::join('governments','governments.Name','=','prices.Government')
                            ->where('governments.GType','=','Upper')
                            ->whereBetween('prices.Date', $range)
                            ->groupBy('governments.Name')
                            ->lists('prices.Government');



        }//'Upper


         if($government_sen_lower == 'Lower')
      {


     $government_lower=Price::join('governments','governments.Name','=','prices.Government')
                            ->where('governments.GType','=','Lower')
                            ->whereBetween('prices.Date', $range)
                            ->groupBy('governments.Name')
                            ->lists('prices.Government');



        }//Lower
 // $a=[];  


if($government_upper != null)
      {

        // $excel->sheet('Upper Gov',function($sheet)
        // { 
        $excel->createSheet();
        $excel->setActiveSheetIndex(1);
        $excel->getActiveSheet()->setTitle('Upper Gov');

  $i=1;
  $a=[];  
  $fm=20;
           // $Start='2018-05-10';
           // $End='2018-05-18';
          $Start=Input::get('StartDate');
          $End=Input::get('EndDate');


$mon=explode("-", $Start);
$number = cal_days_in_month(CAL_GREGORIAN,$mon[1] ,$mon[0]);

$monend=explode("-", $End);
$numberend = cal_days_in_month(CAL_GREGORIAN,$monend[1] ,$monend[0]);


$range1=[];$range2=[];$range3=[];$range4=[];
$month=[];
if($mon[1]==$monend[1]){

    $month=[$mon[1]];
    $year=$mon[0];

//when in same month
$day = new Carbon($End);
$countfor=floor($day->day /7);
$startdateformonth=$mon[0]."-".$mon[1]."-01";

for ($q=0; $q <$countfor ; $q++) { 


   $dt1 = new Carbon($startdateformonth);

   $dtst []= $dt1->addWeeks($q)->toDateString();
   $dten []= $dt1->addWeek()->toDateString();

if($q==0)
{

           $range1=[$dtst[0],$dten[0]];

}
else if ($q==1) {
           $range2=[$dtst[1],$dten[1]];
} 
else if($q==2)
 {
           $range3=[$dtst[2],$dten[2]];
}
else if($q==3)
{
           $range4=[$dtst[3],$dten[3]];

}

}
$allrange=[$dtst[0],$dten[$countfor-1]];

}
else
{
     $month=[$mon[1],$monend[1]];
     $year=$monend[0];

$dtst []=$mon[0]."-".$mon[1]."-01";
$dten []=$mon[0]."-".$mon[1]."-".$number;

$day = new Carbon($End);
$countfor=1;
$startdateforEndmonth=$monend[0]."-".$monend[1]."-01";



   $dt1 = new Carbon($startdateforEndmonth);

   $dtst []= $dt1->addWeeks(0)->toDateString();
   $dten []= $dt1->addWeek()->toDateString();

 $range1=[$dtst[0],$dten[0]];
 $range2=[$dtst[1],$dten[1]];
$allrange=[$dtst[0],$dten[1]];

 // dd($allrange,$range1,$range2);


}

     $government=Price::join('governments','governments.Name','=','prices.Government')
                            ->where('governments.GType','=','Upper')
                            ->whereBetween('Date', $allrange)
                            ->whereIn(DB::raw('MONTH(Date)'), $month)
                            ->where(DB::raw('YEAR(Date)'), $year)
                            ->groupBy('governments.Name')
                            ->lists('prices.Government');


$m=0;
foreach ($government as $key => $G_value) {
  
$Dates1=[];
$Dates2=[];
$Dates3=[];
$Dates4=[];

       $get_products=Price::where('Government',$G_value)
       ->whereBetween('Date', $allrange)
       ->select('Product_id')
       ->groupBy('Product_id')
       ->lists('Product_id');


 

        if($range1 !=[]){

            $Dates1= Price::whereIn('Product_id',$get_products)
            ->where('Government',$G_value)
            ->whereBetween('Date', $range1)
            ->select('prices.Date')
            ->orderBy('Date', 'asc')->lists('Date');
        }
        if($range2 !=[]){

            $Dates2= Price::whereIn('Product_id',$get_products)
            ->where('Government',$G_value)
            ->whereBetween('Date', $range2)
            ->select('prices.Date')
            ->orderBy('Date', 'asc')->lists('Date');
        } 
     if($range3!=[]){

            $Dates3= Price::whereIn('Product_id',$get_products)
            ->where('Government',$G_value)
            ->whereBetween('Date', $range3)
            ->select('prices.Date')
            ->orderBy('Date', 'asc')->lists('Date');
        }
    if($range4 !=[]){

            $Dates4= Price::whereIn('Product_id',$get_products)
            ->where('Government',$G_value)
            ->whereBetween('Date', $range4)
            ->select('prices.Date')
            ->orderBy('Date', 'asc')->lists('Date');
        }
  

$cement_name=[];
$cement_price=[];
$cement_price_seconde_week=[];
$cement_price_third_week=[];
$cement_price_End=[];
$cement_name[0]="Date";
foreach ($get_products as  $value) {
   
$product= Product::where('id','=',$value)->value('Cement_Name');

array_push($cement_name,$product);

if($Dates1 !=[])
{
  $price1= Price::where('prices.Product_id','=',$value)
    ->where('Government',$G_value)
    ->whereIn('prices.Date', $Dates1)
    ->join('products','products.id','=','prices.Product_id')
    ->select('products.Cement_Name',DB::raw( 'AVG(prices.Price) as Price') )  
    ->first();
   
   $cement_price[0]= $dtst[0];

if($price1!=[] && $price1!=null){

  array_push($cement_price, round($price1->Price, 1));

}
else
{
    $spr=0;
     array_push($cement_price,$spr);
}

}

///second week
if($Dates2 !=[])
{
  $price2= Price::where('prices.Product_id','=',$value)
    ->where('Government',$G_value)
    ->whereIn('prices.Date', $Dates2)
    ->join('products','products.id','=','prices.Product_id')
    ->select('products.Cement_Name',DB::raw( 'AVG(prices.Price) as Price') )  
    ->first();
   
   $cement_price_seconde_week[0]= $dten[1];

if($price2!=[] && $price2!=null){

  array_push($cement_price_seconde_week, round($price2->Price, 1));

}
else
{
    $spr=0;
     array_push($cement_price_seconde_week,$spr);
}

}
//third Price
if($Dates3 !=[])
{
  $price3= Price::where('prices.Product_id','=',$value)
    ->where('Government',$G_value)
    ->whereIn('prices.Date',$Dates3)
    ->join('products','products.id','=','prices.Product_id')
    ->select('products.Cement_Name',DB::raw( 'AVG(prices.Price) as Price') )  
    ->first();
   
   $cement_price_third_week[0]= $dten[2];

if($price3!=[] && $price3!=null){

  array_push($cement_price_third_week, round($price3->Price, 1));

}
else
{
    $spr=0;
     array_push($cement_price_third_week,$spr);
}

}


if($Dates4 !=[])
{
  $price4= Price::where('prices.Product_id','=',$value)
    ->where('Government',$G_value)
    ->whereIn('prices.Date', $Dates4)
    ->join('products','products.id','=','prices.Product_id')
    ->select('products.Cement_Name',DB::raw( 'AVG(prices.Price) as Price') )  
    ->first();
   
   $cement_price_End[0]= $dten[3];

if($price4!=[] && $price4!=null){

  array_push($cement_price_End, round($price4->Price, 1));

}
else
{
    $spr=0;
     array_push($cement_price_End,$spr);
}

}



}//end el price

$data=[];

$columnCount = count($cement_name);
        // dd($price_end);

if($mon[1] == $monend[1]){

  for ($j =0; $j < $columnCount; $j++)
        {
            if ($countfor==2) {

            $data[]=array($cement_name[$j] ,
            $cement_price[$j],
            $cement_price_seconde_week[$j]);           

             }
             if ($countfor==3) {

            $data[]=array($cement_name[$j] ,
            $cement_price[$j],
            $cement_price_seconde_week[$j],
            $cement_price_third_week[$j]);  

            }
             if ($countfor==4) {
            $data[]=array($cement_name[$j] ,
            $cement_price[$j],
            $cement_price_seconde_week[$j],
            $cement_price_third_week[$j],
            $cement_price_End[$j]);           

             }
             
           
        }
}
else
{


     for ($j =0; $j < $columnCount; $j++)
           {
             $data[]=array($cement_name[$j],
             $cement_price[$j],$cement_price_seconde_week[$j]);
           }

}


        $curt='A'.($i);
        $objWorksheet = $excel->getActiveSheet();
        $objWorksheet->fromArray($data,null,$curt);


        $n=$i+1;
        $s=$columnCount;

        $rowCount='$A'.$i.':'.'$A'.($columnCount);

      if ($countfor==1){
 
          $labels= array(new \PHPExcel_Chart_DataSeriesValues('String', '!$B$'.$i, null, 1),
                         new \PHPExcel_Chart_DataSeriesValues('String', '!$C$'.$i, null, 1));

          }
          else if($countfor==2){
             
           $labels= array(new \PHPExcel_Chart_DataSeriesValues('String', '!$B$'.$i, null, 1),
                        new \PHPExcel_Chart_DataSeriesValues('String', '!$C$'.$i, null, 1),
                        new \PHPExcel_Chart_DataSeriesValues('String', '!$D$'.$i, null, 1));
          }
          else{

           $labels= array(new \PHPExcel_Chart_DataSeriesValues('String', '!$B$'.$i, null, 1),
                          new \PHPExcel_Chart_DataSeriesValues('String', '!$C$'.$i, null, 1),
                          new \PHPExcel_Chart_DataSeriesValues('String', '!$D$'.$i, null, 1),
                          new \PHPExcel_Chart_DataSeriesValues('String', '!$E$'.$i, null, 1));
          }

    $categories[] = new \PHPExcel_Chart_DataSeriesValues('String', '!'.$rowCount, null, $columnCount);

 for ($k = 1; $k <count($labels)+1; $k++)
        {
          
        $col = \PHPExcel_Cell::stringFromColumnIndex($k);
        $values[]= new \PHPExcel_Chart_DataSeriesValues('Number', '!$'.$col.'$'.$n.':$'.$col.'$'.($s), null, $columnCount);

  

        }     

  $series = new \PHPExcel_Chart_DataSeries(
                \PHPExcel_Chart_DataSeries::TYPE_BARCHART, // plotType
                \PHPExcel_Chart_DataSeries::GROUPING_CLUSTERED, // plotGrouping
                range(0, count($labels)-1),                     // plotOrder
                $labels,                                     // plotLabel
                $categories,                                 // plotCategory
                $values                                         // plotValues
        );
//dd($series);
            $series->setPlotDirection(\PHPExcel_Chart_DataSeries::DIRECTION_COL);
            $layout2 = new \PHPExcel_Chart_Layout();
            $layout2->setShowVal(TRUE);
            $layout2->setShowCatName(FALSE);
            // $layout2->setWidth(200);

        $title= new \PHPExcel_Chart_Title($G_value);
        $plotarea = new \PHPExcel_Chart_PlotArea($layout2, array($series));
        $legend = new \PHPExcel_Chart_Legend(\PHPExcel_Chart_Legend::POSITION_BOTTOM, null, false);
        $chart = new \PHPExcel_Chart(
          'chart'.uniqid(),                               // name
          $title,                                           // title
          $legend,                                        // legend
          $plotarea,                                      // plotArea
          true,                                           // plotVisibleOnly
          0,                                              // displayBlanksAs
          null,                                           // xAxisLabel
          null                                            // yAxisLabel
        );

      if($m==0)
        {
                $left='G'.(2);
                $right='AA'.(20);
                $lm=20;
        }
         else{
             
             
             $left='G'.($fm);

             $right='AA'.($lm);
         }     
        $chart->setTopLeftPosition($left);

         $chart->setBottomRightPosition( $right);

        $objWorksheet->addChart($chart);

        $writer = new \PHPExcel_Writer_Excel2007($excel);
        $writer->setIncludeCharts(TRUE);

$m++;
$fm=$lm+3;
$lm+=25;
 $i=$i+$columnCount+4;
   $data=[];
 $categories=[];
 $values=[];
 $labels=[];





        }//gov
    }
        //Lower


if($government_lower != null)
      {

    // $excel->sheet('Upper Gov',function($sheet)
        // { 
        $excel->createSheet();
        $excel->setActiveSheetIndex(2);
        $excel->getActiveSheet()->setTitle('Lower Gov');

  $i=1;
  $a=[];  
  $fm=20;
  $n=0;
           // $Start='2018-05-10';
           // $End='2018-05-18';
          $Start=Input::get('StartDate');
          $End=Input::get('EndDate');


$mon=explode("-", $Start);
$number = cal_days_in_month(CAL_GREGORIAN,$mon[1] ,$mon[0]);

$monend=explode("-", $End);
$numberend = cal_days_in_month(CAL_GREGORIAN,$monend[1] ,$monend[0]);


$range1=[];$range2=[];$range3=[];$range4=[];
$month=[];
if($mon[1]==$monend[1]){

    $month=[$mon[1]];
    $year=$mon[0];

//when in same month
$day = new Carbon($End);
$countfor=floor($day->day /7);
$startdateformonth=$mon[0]."-".$mon[1]."-01";

for ($w=0; $w <$countfor ; $w++) { 


   $dt1 = new Carbon($startdateformonth);

   $dtst []= $dt1->addWeeks($w)->toDateString();
   $dten []= $dt1->addWeek()->toDateString();

if($w==0)
{

           $range1=[$dtst[0],$dten[0]];

}
else if ($w==1) {
           $range2=[$dtst[1],$dten[1]];
} 
else if($w==2)
 {
           $range3=[$dtst[2],$dten[2]];
}
else if($w==3)
{
           $range4=[$dtst[3],$dten[3]];

}

}
$allrange=[$dtst[0],$dten[$countfor-1]];

}
else
{
     $month=[$mon[1],$monend[1]];
     $year=$monend[0];

$dtst []=$mon[0]."-".$mon[1]."-01";
$dten []=$mon[0]."-".$mon[1]."-".$number;

$day = new Carbon($End);
$countfor=1;
$startdateforEndmonth=$monend[0]."-".$monend[1]."-01";



   $dt1 = new Carbon($startdateforEndmonth);

   $dtst []= $dt1->addWeeks(0)->toDateString();
   $dten []= $dt1->addWeek()->toDateString();

 $range1=[$dtst[0],$dten[0]];
 $range2=[$dtst[1],$dten[1]];
$allrange=[$dtst[0],$dten[1]];



}


   // $dt1 = new Carbon($Start);

   // $dt2= new Carbon($End);
   // $dtst = $dt1->subWeeks(1)->toDateString();
   // $dten = $dt2->subWeeks(1)->toDateString();

   //         $range1=[$dtst,$Start];
   //         $range2=[$dten,$End];
//$allrange=[$dtst,$End];

     $government=Price::join('governments','governments.Name','=','prices.Government')
                            ->where('governments.GType','=','Lower')
                             ->whereBetween('Date', $allrange)
                            ->whereIn(DB::raw('MONTH(Date)'), $month)
                            ->where(DB::raw('YEAR(Date)'), $year)
                            ->groupBy('governments.Name')
                            ->lists('prices.Government');



$m=0;
foreach ($government as $key => $G_value) {
  
$Dates1=[];
$Dates2=[];
$Dates3=[];
$Dates4=[];

       $get_products=Price::where('Government',$G_value)
       ->whereBetween('Date', $allrange)
       ->select('Product_id')
       ->groupBy('Product_id')
       ->lists('Product_id');


 

        if($range1 !=[]){

            $Dates1= Price::whereIn('Product_id',$get_products)
            ->where('Government',$G_value)
            ->whereBetween('Date', $range1)
            ->select('prices.Date')
            ->orderBy('Date', 'asc')->lists('Date');
        }
        if($range2 !=[]){

            $Dates2= Price::whereIn('Product_id',$get_products)
            ->where('Government',$G_value)
            ->whereBetween('Date', $range2)
            ->select('prices.Date')
            ->orderBy('Date', 'asc')->lists('Date');
        } 
     if($range3!=[]){

            $Dates3= Price::whereIn('Product_id',$get_products)
            ->where('Government',$G_value)
            ->whereBetween('Date', $range3)
            ->select('prices.Date')
            ->orderBy('Date', 'asc')->lists('Date');
        }
    if($range4 !=[]){

            $Dates4= Price::whereIn('Product_id',$get_products)
            ->where('Government',$G_value)
            ->whereBetween('Date', $range4)
            ->select('prices.Date')
            ->orderBy('Date', 'asc')->lists('Date');
        }


$b=[];
$cement_name=[];
$cement_price=[];
$cement_price_seconde_week=[];
$cement_price_third_week=[];
$cement_price_End=[];
$cement_name[0]="Date";
foreach ($get_products as  $value) {
   
$product= Product::where('id','=',$value)->value('Cement_Name');

array_push($cement_name,$product);

if($Dates1 !=[])
{
  $price1= Price::where('prices.Product_id','=',$value)
    ->where('Government',$G_value)
    ->whereIn('prices.Date', $Dates1)
    ->join('products','products.id','=','prices.Product_id')
    ->select('products.Cement_Name',DB::raw( 'AVG(prices.Price) as Price') )  
    ->first();
   
   $cement_price[0]= $dtst[0];

if($price1!=[] && $price1!=null){

  array_push($cement_price, round($price1->Price, 1));

}
else
{
    $spr=0;
     array_push($cement_price,$spr);
}

}

///second week
if($Dates2 !=[])
{
  $price2= Price::where('prices.Product_id','=',$value)
    ->where('Government',$G_value)
    ->whereIn('prices.Date', $Dates2)
    ->join('products','products.id','=','prices.Product_id')
    ->select('products.Cement_Name',DB::raw( 'AVG(prices.Price) as Price') )  
    ->first();
   
   $cement_price_seconde_week[0]= $dten[1];

if($price2!=[] && $price2!=null){

  array_push($cement_price_seconde_week, round($price2->Price, 1));

}
else
{
    $spr=0;
     array_push($cement_price_seconde_week,$spr);
}

}
//third Price
if($Dates3 !=[])
{
  $price3= Price::where('prices.Product_id','=',$value)
    ->where('Government',$G_value)
    ->whereIn('prices.Date', $Dates3)
    ->join('products','products.id','=','prices.Product_id')
    ->select('products.Cement_Name',DB::raw( 'AVG(prices.Price) as Price') )  
    ->first();
   
   $cement_price_third_week[0]= $dten[2];

if($price3!=[] && $price3!=null){

  array_push($cement_price_third_week, round($price3->Price, 1));

}
else
{
    $spr=0;
     array_push($cement_price_third_week,$spr);
}

}


if($Dates4 !=[])
{
  $price4= Price::where('prices.Product_id','=',$value)
    ->where('Government',$G_value)
    ->whereIn('prices.Date', $Dates4)
    ->join('products','products.id','=','prices.Product_id')
    ->select('products.Cement_Name',DB::raw( 'AVG(prices.Price) as Price') )  
    ->first();
   
   $cement_price_End[0]= $dten[3];

if($price4!=[] && $price4!=null){

  array_push($cement_price_End, round($price4->Price, 1));

}
else
{
    $spr=0;
     array_push($cement_price_End,$spr);
}

}













}//end el price
/*dd($cement_price_End,$cement_price_third_week,$cement_price_seconde_week,$cement_price);
*/// dd($countfor);

$data=[];

$columnCount = count($cement_name);

if($mon[1] == $monend[1]){

  for ($j =0; $j < $columnCount-1; $j++)
        {
            if ($countfor==2) {

            $data[]=array($cement_name[$j] ,
            $cement_price[$j],
            $cement_price_seconde_week[$j]);           

             }
             if ($countfor==3) {

            $data[]=array($cement_name[$j] ,
            $cement_price[$j],
            $cement_price_seconde_week[$j],
            $cement_price_third_week[$j]);  

            }
             if ($countfor==4) {
            $data[]=array($cement_name[$j] ,
            $cement_price[$j],
            $cement_price_seconde_week[$j],
            $cement_price_third_week[$j],
            $cement_price_End[$j]);           

             }
             
           
        }
}
else
{

  for ($j =0; $j < $columnCount-1; $j++)
        {
            $data[]=array($cement_name[$j] ,
            $cement_price[$j],
            $cement_price_seconde_week[$j]);
        }



}


 //dd($data);
$curt='A'.($i);

        $objWorksheet = $excel->getActiveSheet();
        $objWorksheet->fromArray(
             $data,null,$curt
        );


        $n=$i+1;
        $s=$n+$columnCount;
    // $rowCount='$A'.$n.':'.'$B'.($n+1);
     $rowCount='$A'.$n.':'.'$A'.($s);

      if ($countfor==1) {
 
       $labels= array(new \PHPExcel_Chart_DataSeriesValues('String', '!$B$'.$i, null, 1),
                       new \PHPExcel_Chart_DataSeriesValues('String', '!$C$'.$i, null, 1));

          }
          else if($countfor==2)
          {
             
       $labels= array(new \PHPExcel_Chart_DataSeriesValues('String', '!$B$'.$i, null, 1),
                       new \PHPExcel_Chart_DataSeriesValues('String', '!$C$'.$i, null, 1),
                        new \PHPExcel_Chart_DataSeriesValues('String', '!$D$'.$i, null, 1));
          }
          else
          {
           $labels= array(new \PHPExcel_Chart_DataSeriesValues('String', '!$B$'.$i, null, 1),
                           new \PHPExcel_Chart_DataSeriesValues('String', '!$C$'.$i, null, 1),
                           new \PHPExcel_Chart_DataSeriesValues('String', '!$D$'.$i, null, 1),
                            new \PHPExcel_Chart_DataSeriesValues('String', '!$E$'.$i, null, 1));
          }







     $categories[] = new \PHPExcel_Chart_DataSeriesValues('String', '!'.$rowCount, null, $columnCount);


 for ($k = 1; $k < count($labels)+1; $k++)
        {
          
    $col = \PHPExcel_Cell::stringFromColumnIndex($k);


    $values[] = new \PHPExcel_Chart_DataSeriesValues('Number', '!$'.$col.'$'.$n.':$'.$col.'$'.($s), null, $columnCount);

  

        }     
 

  $series = new \PHPExcel_Chart_DataSeries(
                \PHPExcel_Chart_DataSeries::TYPE_BARCHART, // plotType
                \PHPExcel_Chart_DataSeries::GROUPING_CLUSTERED, // plotGrouping
                range(0, count($labels)-1),                     // plotOrder
                $labels,                                     // plotLabel
                $categories,                                 // plotCategory
                $values                                         // plotValues
        );
// dd($series);
             $series->setPlotDirection(\PHPExcel_Chart_DataSeries::DIRECTION_COL);

            $layout2 = new \PHPExcel_Chart_Layout();
            $layout2->setShowVal(TRUE);
            $layout2->setShowCatName(FALSE);

            $title= new \PHPExcel_Chart_Title($G_value);

        $plotarea = new \PHPExcel_Chart_PlotArea($layout2, array($series));
        $legend = new \PHPExcel_Chart_Legend(\PHPExcel_Chart_Legend::POSITION_BOTTOM, null, false);
        $chart = new \PHPExcel_Chart(
          'chart'.uniqid(),                               // name
          $title,                                           // title
          $legend,                                        // legend
          $plotarea,                                      // plotArea
          true,                                           // plotVisibleOnly
          0,                                              // displayBlanksAs
          null,                                           // xAxisLabel
          null                                            // yAxisLabel
        );

                 if($m==0)
        {
                $left='G'.(2);
                $right='AA'.(20);
                $lm=20;
        }
         else{
             
             
             $left='G'.($fm);

             $right='AA'.($lm);
         }     
        $chart->setTopLeftPosition($left);

         $chart->setBottomRightPosition( $right);

        $objWorksheet->addChart($chart);

        $writer = new \PHPExcel_Writer_Excel2007($excel);
        $writer->setIncludeCharts(TRUE);

$m++;
$fm=$lm+3;
$lm+=25;

 $i=$i+$columnCount+4;
 $data=[];
 $categories=[];
 $values=[];
 $labels=[];





        }//gov
$file=str_random(5);
$writer->save(storage_path().'/'.$file.'.xlsx');
 
} //!= null

        return Response::download(storage_path().'/'.$file.'.xlsx');


 }

    catch(Exception $e){

    }
}



public function ShChartPrice(){

try{



   $Region=Input::get('Region');

   $Start=Input::get('StartDate');
   $End=Input::get('EndDate');
    $range=[$Start,$End];
        


     $government_upper=Price::join('governments','governments.Name','=','prices.Government')
                            ->where('governments.GType','=',$Region)
                            ->whereBetween('prices.Date', $range)
                            ->groupBy('governments.Name')
                            ->lists('prices.Government');



       




if($government_upper != null)
      {
  $i=1;
  $a=[];  
  $fm=20;

  $Start=Input::get('StartDate');
  $End=Input::get('EndDate');


  $mon=explode("-", $Start);
  $number = cal_days_in_month(CAL_GREGORIAN,$mon[1] ,$mon[0]);

  $monend=explode("-", $End);
  $numberend = cal_days_in_month(CAL_GREGORIAN,$monend[1] ,$monend[0]);


  $range1=[];$range2=[];$range3=[];$range4=[];
  $month=[];
  if($mon[1]==$monend[1]){

    $month=[$mon[1]];
    $year=$mon[0];

//when in same month
    $day = new Carbon($End);
    $countfor=floor($day->day /7);
    $startdateformonth=$mon[0]."-".$mon[1]."-01";
    for ($q=0; $q <=$countfor ; $q++) { 


       $dt1 = new Carbon($startdateformonth);

       $dtst []= $dt1->addWeeks($q)->toDateString();
       $dten []= $dt1->addWeek()->toDateString();
        if($q==0)
        {

                   $range1=[$dtst[0],$dten[0]];

        }
        else if ($q==1) {
                   $range2=[$dtst[1],$dten[1]];
        } 
        else if($q==2)
         {
                   $range3=[$dtst[2],$dten[2]];
        }
        else if($q==3)
        {
                   $range4=[$dtst[3],$dten[3]];

        }

        }
        $allrange=[$dtst[0],$dten[$countfor]];
    }else{

     $month=[$mon[1],$monend[1]];
     $year=$monend[0];

     $dtst []=$mon[0]."-".$mon[1]."-01";
     $dten []=$mon[0]."-".$mon[1]."-".$number;

     $day = new Carbon($End);
     $countfor=1;
     $startdateforEndmonth=$monend[0]."-".$monend[1]."-01";



     $dt1 = new Carbon($startdateforEndmonth);

     $dtst []= $dt1->addWeeks(0)->toDateString();
     $dten []= $dt1->addWeek()->toDateString();

     $range1=[$dtst[0],$dten[0]];
     $range2=[$dtst[1],$dten[1]];
     $allrange=[$dtst[0],$dten[1]];
}

     $government=Price::join('governments','governments.Name','=','prices.Government')
                            ->where('governments.GType','=',$Region)
                            ->whereBetween('Date', $allrange)
                            ->whereIn(DB::raw('MONTH(Date)'), $month)
                            ->where(DB::raw('YEAR(Date)'), $year)
                            ->groupBy('governments.Name')
                            ->lists('prices.Government');


$m=0;
foreach ($government as $key => $G_value) {
  
$Dates1=[];
$Dates2=[];
$Dates3=[];
$Dates4=[];

       $get_products=Price::where('Government',$G_value)
       ->whereBetween('Date', $allrange)
       ->select('Product_id')
       ->groupBy('Product_id')
       ->lists('Product_id');


 

        if($range1 !=[]){

            $Dates1= Price::whereIn('Product_id',$get_products)
            ->where('Government',$G_value)
            ->whereBetween('Date', $range1)
            ->select('prices.Date')
            ->orderBy('Date', 'asc')->lists('Date');
        }
        if($range2 !=[]){

            $Dates2= Price::whereIn('Product_id',$get_products)
            ->where('Government',$G_value)
            ->whereBetween('Date', $range2)
            ->select('prices.Date')
            ->orderBy('Date', 'asc')->lists('Date');
        } 
     if($range3!=[]){

            $Dates3= Price::whereIn('Product_id',$get_products)
            ->where('Government',$G_value)
            ->whereBetween('Date', $range3)
            ->select('prices.Date')
            ->orderBy('Date', 'asc')->lists('Date');
        }
    if($range4 !=[]){

            $Dates4= Price::whereIn('Product_id',$get_products)
            ->where('Government',$G_value)
            ->whereBetween('Date', $range4)
            ->select('prices.Date')
            ->orderBy('Date', 'asc')->lists('Date');
        }
  

$cement_name=[];
$cement_price=[];
$cement_price_seconde_week=[];
$cement_price_third_week=[];
$cement_price_End=[];
$a=[];$b=[];$b2=[];$b3=[];$b4=[];


$cement_name[0]="Date";
foreach ($get_products as  $value) {
   
$product= Product::where('id','=',$value)->value('Cement_Name');

array_push($cement_name,$product);

if($Dates1 !=[])
{
  $price1= Price::where('prices.Product_id','=',$value)
    ->where('Government',$G_value)
    ->whereIn('prices.Date', $Dates1)
    ->join('products','products.id','=','prices.Product_id')
    ->select('products.Cement_Name',DB::raw( 'AVG(prices.Price) as Price') )  
    ->first();
   
   $cement_price[0]= $dtst[0];

if($price1!=[] && $price1!=null){

  array_push($cement_price, round($price1->Price, 1));

}
else
{
    $spr=0;
     array_push($cement_price,$spr);
}

}

///second week
if($Dates2 !=[])
{
  $price2= Price::where('prices.Product_id','=',$value)
    ->where('Government',$G_value)
    ->whereIn('prices.Date', $Dates2)
    ->join('products','products.id','=','prices.Product_id')
    ->select('products.Cement_Name',DB::raw( 'AVG(prices.Price) as Price') )  
    ->first();
   
   $cement_price_seconde_week[0]= $dten[1];

if($price2!=[] && $price2!=null){

  array_push($cement_price_seconde_week, round($price2->Price, 1));

}
else
{
    $spr=0;
     array_push($cement_price_seconde_week,$spr);
}

}
//third Price
if($Dates3 !=[])
{
  $price3= Price::where('prices.Product_id','=',$value)
    ->where('Government',$G_value)
    ->whereIn('prices.Date',$Dates3)
    ->join('products','products.id','=','prices.Product_id')
    ->select('products.Cement_Name',DB::raw( 'AVG(prices.Price) as Price') )  
    ->first();
   
   $cement_price_third_week[0]= $dten[2];

if($price3!=[] && $price3!=null){

  array_push($cement_price_third_week, round($price3->Price, 1));

}
else
{
    $spr=0;
     array_push($cement_price_third_week,$spr);
}

}


if($Dates4 !=[])
{
  $price4= Price::where('prices.Product_id','=',$value)
    ->where('Government',$G_value)
    ->whereIn('prices.Date', $Dates4)
    ->join('products','products.id','=','prices.Product_id')
    ->select('products.Cement_Name',DB::raw( 'AVG(prices.Price) as Price') )  
    ->first();
   
   $cement_price_End[0]= $dten[3];

if($price4!=[] && $price4!=null){

  array_push($cement_price_End, round($price4->Price, 1));

}
else
{
    $spr=0;
     array_push($cement_price_End,$spr);
}

}



}//end el price
$data=[];

$columnCount = count($cement_name);
        // dd($price_end);

if($mon[1] == $monend[1]){

  for ($j =1; $j < $columnCount; $j++)
        {

 


     if ($countfor==2 ||$countfor==1) {

        $data[]=array($cement_name[$j],$cement_price[$j],$cement_price_seconde_week[$j]);
        $dates=array($cement_price[0],$cement_price_seconde_week[0]);

        array_push($b,array('label' => $cement_name[$j],
                      'y' => $cement_price[$j],'indexLabelFontColor'=>'black'));
        array_push($b2,array('label' => $cement_name[$j],
                      'y' => $cement_price_seconde_week[$j],'indexLabelFontColor'=>'red'));
             }
    if ($countfor==3) {

         $data[]=array($cement_name[$j],$cement_price[$j],$cement_price_seconde_week[$j],
                              $cement_price_third_week[$j]);  
                
                $dates=array($cement_price[0],$cement_price_seconde_week[0],
                              $cement_price_third_week[0]);

                  array_push($b,array('label' => $cement_name[$j],
                      'y' => $cement_price[$j],'indexLabelFontColor'=>'black'));
                  array_push($b2,array('label' => $cement_name[$j],
                      'y' => $cement_price_seconde_week[$j],'indexLabelFontColor'=>'red'));
                  array_push($b3,array('label' => $cement_name[$j],
                      'y' => $cement_price_third_week[$j],'indexLabelFontColor'=>'black'));
         
            }
             if ($countfor==4) {

                $data[]=array($cement_name[$j] , $cement_price[$j],$cement_price_seconde_week[$j],
                              $cement_price_third_week[$j], $cement_price_End[$j]);  


              array_push($b,array('label' => $cement_name[$j],
                      'y' => $cement_price[$j],'indexLabelFontColor'=>'black'));
              array_push($b2,array('label' => $cement_name[$j],
                      'y' => $cement_price_seconde_week[$j],'indexLabelFontColor'=>'red'));
              array_push($b3,array('label' => $cement_name[$j],
                      'y' => $cement_price_third_week[$j],'indexLabelFontColor'=>'black'));
              array_push($b4,array('label' => $cement_name[$j],
                      'y' => $cement_price_End[$j],'indexLabelFontColor'=>'black'));
            $dates=array($cement_price[0],$cement_price_seconde_week[0],
                             $cement_price_third_week[0],$cement_price_End[0]);
        
             }
             
           
        }


if ($countfor==2 ||$countfor==1) {
    array_push($a,array('name' => $cement_price[0], 'data' =>$b));
    array_push($a,array('name' => $cement_price_seconde_week[0], 'data' =>$b2));
    
}
if ($countfor==3) {
    array_push($a,array('name' => $cement_price[0], 'data' =>$b));
    array_push($a,array('name' => $cement_price_seconde_week[0], 'data' =>$b2));
    array_push($a,array('name' => $cement_price_third_week[0], 'data' =>$b3));
}

if ($countfor==4) {
array_push($a,array('name' => $cement_price[0], 'data' =>$b));
 array_push($a,array('name' => $cement_price_seconde_week[0], 'data' =>$b2));
array_push($a,array('name' => $cement_price_third_week[0], 'data' =>$b3));
 array_push($a,array('name' => $cement_price_End[0], 'data' =>$b4));
}
}else{


     for ($j =1; $j < $columnCount; $j++)
           {
             $data[]=array($cement_name[$j],
             $cement_price[$j],$cement_price_seconde_week[$j]);

              array_push($b,array('label' => $cement_name[$j],
                      'y' => $cement_price[$j],'indexLabelFontColor'=>'black'));
              array_push($b2,array('label' => $cement_name[$j],
                      'y' => $cement_price_seconde_week[$j],'indexLabelFontColor'=>'red'));

           }
 $dates=array($cement_price[0],$cement_price_seconde_week[0]);
 array_push($a,array('name' => $cement_price[0], 'data' =>$b));
 array_push($a,array('name' => $cement_price_seconde_week[0], 'data' =>$b2));

}
           
 $labels[]=array('Gov'=>$G_value,'price'=>$a);


 $data=[];
 // $values=[];
  // $labels[]=;

                                   
                     
                               



        }//gov
      $categories['Dates']=$dates; 
      $categories['data']=$labels;
 
 
             return Response::json($categories,  200, [], JSON_NUMERIC_CHECK);

    }
}
catch(Exception $e){

    }

}



public function showpricemap(){


$pricestotal=[];
$activitytotal=[];
    $priceQuery=Price::join('products','products.id','=','prices.Product_id')
    ->whereYear('prices.Date', '=', date('Y'))
    ->whereMonth('prices.Date', '=', date('m'))
    ->selectRaw('AVG(prices.Price) Paverage,products.Cement_Name,prices.Government,prices.Date')-> groupBy('prices.Product_id','prices.Government')
    ->limit('5')
    -> get();
   
     $activityitemids= DB::table('activityitem_attributes')
    ->join('activity_items','activity_items.id','=','activityitem_attributes.activityitem_id')
    ->join('sub_activities','sub_activities.id','=','activity_items.subactivity_id')
    ->select('activityitem_attributes.activityitem_id')
   ->groupBy('activityitem_attributes.activityitem_id')->lists('activityitem_attributes.activityitem_id');

$QueryActivity=ActivityItem::join('activityitem_attributes','activityitem_attributes.activityitem_id','=','activity_items.id')
->join('attributes','attributes.id','=','activityitem_attributes.attrbuti_id')
->where('activityitem_attributes.attrbuti_id','=', 5)
              ->OrWhere('attributes.Name','LIKE', '%'.'government'.'%')
              ->where('activityitem_attributes.Value', '!=', NULL)
             
              ->whereIn('activity_items.id',$activityitemids)
            ->groupBy('activityitem_attributes.Value')
              ->selectRaw('Distinct  Count(activityitem_attributes.activityitem_id) AS Count,activityitem_attributes.Value')
               ->get();

               $CoveredContractors=DB::table('activity_contractors_retailers')->join('contractors','contractors.id','=','activity_contractors_retailers.contractor_id')
               ->groupBy('contractors.Government')->distinct('activity_contractors_retailers.contractor_id')
              ->selectRaw(' Count(activity_contractors_retailers.contractor_id) AS Count,contractors.Government')->get()
               ;





     
              foreach ($priceQuery as $allprice) {

$governments=Government::where('governments.Name',$allprice->Government)->select('governments.Lat AS GLAT','governments.Long AS GLONG')->first();

               array_push( $pricestotal,array(
               'product' => $allprice->Cement_Name,
                'price' => $allprice->Paverage,
                'government' => $allprice->Government,
                'date' => $allprice->Date,
                'Glat' => $governments->GLAT,
                'Glong' => $governments->GLONG,
                
            )


               );
            }
          
                            foreach ($QueryActivity as $QueryAct) {


$governments=Government::where('governments.Name',$QueryAct->Value)->select('governments.Lat AS GLAT','governments.Long AS GLONG')->first();

if($governments !=null)
{
         array_push( $activitytotal,array(
               'Total' => $QueryAct->Count,
                'Glat' => $governments->GLAT,
                'Glong' => $governments->GLONG,
                
            )


               );

}


          
            }    
                            
$totalContractorsArray=[];
         
$totalContractors=Contractor::groupby('contractors.Government') ->selectRaw('  Count(contractors.id) AS Countractors,contractors.Government')->get();


                           foreach ($totalContractors as $totalContr) {


$governments=Government::where('governments.Name',$totalContr->Government)->select('governments.Lat AS GLAT','governments.Long AS GLONG')->first();

if($governments !=null)
{
         array_push( $totalContractorsArray,array(
               'Total' => $totalContr->Countractors,
               'Government' => $totalContr->Government,
                'Glat' => $governments->GLAT,
                'Glong' => $governments->GLONG,
                
            )


               );

}


          
            }  


 $CoveredContractorArray=[];


                           foreach ($CoveredContractors as $CoveredContractor) {


$governments=Government::where('governments.Name',$CoveredContractor->Government)->select('governments.Lat AS GLAT','governments.Long AS GLONG')->first();

if($governments !=null)
{
         array_push( $CoveredContractorArray,array(
               'Total' => $CoveredContractor->Count,
               'Government' => $totalContr->Government,
                'Glat' => $governments->GLAT,
                'Glong' => $governments->GLONG,
                
            )


               );

}


          
            } 


        
     
   return view('price.pricemap',compact('pricestotal','activitytotal','totalContractorsArray','CoveredContractorArray'));

}

}
