<?php

namespace App\Http\Controllers;

use App\Government;
use App\District;
use Auth;
use App\Http\Requests;
use App\User;
use App\Gps;
use DB;
use App\Http\Controllers\Controller;
use Request;
use Excel;
use File;
use Illuminate\Support\Facades\Response;
use Validator;
use Illuminate\Support\Facades\Redirect;
use Input;



class GovernmentController extends Controller
{

  public function __construct()
   {
       $this->middleware('auth');
   }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
          try {

 if (Auth::check())
  {


    if(Auth::user()->can('show-government'))
      {

    //$gtype=array list ('Upper' =>"Upper" ,'Lower'=>"Lower");
      $governments = Government::all();         
         return view('Governments.index',compact('governments'));

      }
    else 
      {
    return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }

 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
     try {

 if (Auth::check())
  {


    if(Auth::user()->can('storeGovernments'))
      {
       $governmentname=Input::get('Name');
       $governmentEname=Input::get('EngName');

       $governmentType=Input::get('GType');
            $government = New Government;
            $government->Name=$governmentname;
            $government->EngName=$governmentEname; 
            $government->GType=$governmentType;  
            $government->save();         
      
        return redirect('/Governments');

      }
    else 
      {
    return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
    }
      public function checknamegovernment()
    {
         $brandname=Input::get('Name');
      $brand = Government::where('governments.Name','=',$brandname)
     ->select('*')->first();
     if($brand==null)     
        $isAvailable = true;
        else
         $isAvailable = false;

    return \Response::json(array('valid' =>$isAvailable,));
    }
  public function checkEngNamegovernment()
    {
         $brandname=Input::get('EngName');
      $brand = Government::where('governments.EngName','=',$brandname)
     ->select('*')->first();
     if($brand==null)     
        $isAvailable = true;
        else
         $isAvailable = false;

    return \Response::json(array('valid' =>$isAvailable,));
    }
    public function updateGovernments()
    {

 try {

 if (Auth::check())
  {


    if(Auth::user()->can('update-Governments'))
      {

        $government_id = Input::get('pk'); 
        
        $column_name = Input::get('name');
        $column_value = Input::get('value');
    
        $gpsData = Government::whereId($government_id)->first();
        $gpsData-> $column_name=$column_value;

        if($gpsData->save())

        return \Response::json(array('status'=>1));
    else 
        return \Response::json(array('status'=>0)); 
  }
    else 
      {
     return \Response::json(array('status'=>'You do not have permission.'));
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
          try {

 if (Auth::check())
  {


    if(Auth::user()->can('DeleteGovernment'))
      {

        $governments=Government::find($id);
          $District = District::where('government_id','=',$id)->first();
          if ($District == null)
         {
              $governments->delete();
        return redirect('/Governments');
            
         }
         else
         {
           return redirect('/Governments'); 
         }
       
  }
    else 
      {
    return view('errors.403');
      }

   }//endAuth

else
{
return redirect('/login');
}

}//endtry
catch(Exception $e) 
    {
    return redirect('/login');
    }
    }
}
