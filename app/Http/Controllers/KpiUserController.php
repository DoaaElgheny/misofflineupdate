<?php

namespace App\Http\Controllers;

use Auth;
use App\Http\Requests;
use App\User;
use App\Gps;
use App\Contractor;
use App\Task;
use App\sale_product_details;
use App\sale_product;
use DB;
use App\Http\Controllers\Controller;
use Request;
use Excel;
use File;
use Illuminate\Support\Facades\Response;
use Validator;
use Illuminate\Support\Facades\Redirect;
use Input;
use Session;
use Geocode;
use Cache;
use URL;
use View;
use Khill\Lavacharts\Laravel\LavachartsFacade as Lava;
use Khill\Lavacharts\Lavacharts;
use TableView;
use App\Dategps;
use PHPExcel; 
use PHPExcel_IOFactory; 
use App\Kpi;
use App\users;
use Carbon;
use App\salary_user;
use App\CafeSigns;
use App\Activity;
use App\Price;
use App\Order;
use DateTime;

class KpiUserController extends Controller
{
/**
* Display a listing of the resource.
*
* @return \Illuminate\Http\Response
*/
public function index()
{


 if (Auth::check())
  {
 

$Kpis=Kpi::select('Name','id')->where('Active',1)->get();
$Users=User::where('Type_User','Outdoor Promoters')->select('Name','id')->lists('Name','id');
$kpi_users=DB::table('kpi_users')
->join('kpis','kpis.id','=','kpi_users.kpi_id')
->join('users','users.id','=','kpi_users.user_id')
->select('kpis.Name','kpi_users.user_id','users.Name As UserName')->get();
return view('KPI.KpiPerUser',compact('Kpis','Users','kpi_users'));
}
else
{
return redirect('/login');
}
}

public function indexPkisalary()

{
   if (Auth::check())
  {
 
$Users=User::where('Type_User','Outdoor Promoters')->select('Name','id')->lists('Name','id');

return view('KPI.KpiUser',compact('Users'));
}
else
{
return redirect('/login');
}
}
/**
* Show the form for creatig a new resource.
*
* @return \Illuminate\Http\Response
*/
public function create()
{
//
}
//To Show KPI 
public function ShowReportKPI()
{
   if (Auth::check())
  {
//    $users=User::all();
//    foreach ($users as  $value) {
//      $salary=salary_user::whereBetween('salary_user.Date',range(input::get("fromdate"), input::get("todate")))->where('salary_user.users_id',$value->id)->get();
//      foreach ($salary as $key) {

//        $key->DailyWeage=$value->Salary;
// $key->save();
//      }
//    }
 
$kpi_users=DB::table('salary_user')
->join('users','users.id','=','salary_user.users_id')
->whereBetween('salary_user.Date',array(input::get("fromdate"), input::get("todate")))
->select('users.Name','users.Government','salary_user.FinalSalary','users.id','salary_user.BackCheck','salary_user.Performance')
-> get();

$result=[];
foreach ($kpi_users as  $Value) 
{  

array_push($result,array('Name'=>$Value->Name,'FinalSalary'=>$Value->FinalSalary,'id'=>$Value->id,'backcheck'=>$Value->BackCheck,'Performance'=>$Value->Performance,'Government'=>$Value->Government)); 
}

return $result;
}
else
{
return redirect('/login');
}
}
public function ShowDetailsKpi($id,$from,$to)
{
   if (Auth::check())
  {
$userkpis= DB::Table('kpi_users')
->join('kpis','kpis.id','=', 'kpi_users.kpi_id')
->join('users','users.id','=', 'kpi_users.user_id')
->where('kpi_users.user_id',$id)
->where('kpi_users.From_Date',$from)
// ->where('kpi_users.To_Date',$to)
->select('users.Name',"kpis.Name as kpiname","kpi_users.Weight","kpi_users.Achieved","kpi_users.Target","kpi_users.Performance")->get();
return view('KPI.ShowDetails',compact('userkpis'));
}
else
{
return redirect('/login');
}
}
public function importKPI()
{
 if (Auth::check())
  {

$temp= Request::get('submit'); 

if(isset($temp))
{ 

$filename = Input::file('file')->getClientOriginalName();
$Dpath = base_path();
$upload_success =Input::file('file')->move( $Dpath, $filename);

// Excel::selectSheets('1')->load($upload_success, function($reader)
// {
//     $results = $reader->get()->all();
Excel::load($upload_success, function($reader)
{   

$results = $reader->get()->all(); 

// dd($results);
$GLOBAL['$KpiData']=[];
//Elgheny Update
$ExsitsKPI=true;
//End
 
foreach ($results as $promotersdata)
{ 
if(gettype($promotersdata['fromdate'])=="object")
{
$stratdate=$promotersdata['fromdate']->toDateString();
$enddate=$promotersdata['todate']->toDateString();
}
else
{
$stratdate=$promotersdata['fromdate'];
$enddate=$promotersdata['todate'];
}

$mon=explode("-", $stratdate);
$number = cal_days_in_month(CAL_GREGORIAN,$mon[1] ,$mon[0]);

$diff=Carbon::parse($enddate)->diffInDays(Carbon::parse($stratdate));
if($diff==0)
$diff=1;

$percent_of_target=$diff/$number ;
// dd( $mon);
$GLOBAL['$PromoterData']=[];

$user_code=(int)$promotersdata['code'];
$userId = User::withTrashed()->where('Code','=',$user_code)->first();

array_push($GLOBAL['$PromoterData'], $userId->Name);
//Doaa Update 

$startdateformonth=$mon[0]."-".$mon[1]."-01";
// dd($startdateformonth);
$KPICodes=DB::table('kpi_users')
->join('kpis','kpis.id','=', 'kpi_users.kpi_id')
->where('user_id',$userId->id)
->where('From_Date',$startdateformonth)
 // ->where('To_Date','>',$stratdate)
->select('kpi_users.*','kpis.Code')->get();

// dd($KPICodes);
// $numberofSign=0;  
// // $AmountofSaleOrders=0;
// // $Contractorsofactivity=0;
// // $gooddatacontractor=0;
// $percentageoftotalcontractor=0;
// $Start_Working=0;   
// $facebookId=0;   
// $instagramId=0;
// $Working_Hours=0;
// $reportsId=0;
// $No_Contractors=0;
// $numberofvisits=0;
// $Numberofdaywork=0;
// $numberofCafe=0;
// $contractorshomeId=0;
// $numberofnewcontractor=0;
// $numberofcompatitoractivity=0;
// $numberofprices=0;
// $AmountofCementOrder=0;
// $Safety=0;
// $Distance=0;
// $System=0;
//Hadeeel Fucnction

$facebook=$promotersdata['facebook'];
$instagram=$promotersdata['instagram'];
$Total_Distance=$promotersdata['distance'];
$User_safety=$promotersdata['safety'];
$reports=$promotersdata['reports'];
$contractors_home=$promotersdata['contractors_home'];
$NO_Days=$promotersdata['no_days'];
$off_Days=$promotersdata['off_days'];
$Bouns=$promotersdata['bouns'];
$Days_meeting=$promotersdata['meeting'];
$BAnaylsis=$promotersdata['backcheck'];
$Realwork=Gps::where('user_id',$userId->id)
->whereBetween('NDate',array($stratdate,$enddate))
->select(DB::raw('count(distinct(NDate)) as NoDay'))
->value('NoDay');



if($Realwork>0)
{

foreach ($KPICodes as $value) {


if($value->Code === '5')
{

$facebookId = Kpi::join('kpi_users','kpi_users.kpi_id','=', 'kpis.id')
->where('kpi_users.id',$value->id)
->select('kpi_users.*')->first();
$Performanceuser=($facebook/($facebookId->Target*$NO_Days))*100;
if($Performanceuser>120)
$Performanceuser=120;
$Achieved=($Performanceuser*$facebookId->Weight)/100;

$updateFacebok=DB::table('kpi_users')
->where('kpi_users.id',$value->id)
->update(['Performance' =>$Performanceuser,'Achieved'=>$Achieved]);

array_push($GLOBAL['$PromoterData'],$Achieved);
}
else if($value->Code === '20')
{

$Safety = Kpi::join('kpi_users','kpi_users.kpi_id','=', 'kpis.id')
->where('kpi_users.id',$value->id)
->select('kpi_users.*')->first();
// $Performanceuser=($User_safety/($Safety->Target*$NO_Days))*100;
// if($Performanceuser>120)
// $Performanceuser=120;
// $Achieved=($Performanceuser*$Safety->Weight)/100;

$Performanceuser=30;
$Achieved=3;
$updateSafety=DB::table('kpi_users')
->where('kpi_users.id',$value->id)
->update(['Performance' =>$Performanceuser,'Achieved'=>$Achieved]);

array_push($GLOBAL['$PromoterData'],$Achieved);


}

else if($value->Code === '21')
{

$Distance = Kpi::join('kpi_users','kpi_users.kpi_id','=', 'kpis.id')
->where('kpi_users.id',$value->id)
->select('kpi_users.*')->first();  
$Performanceuser=($Total_Distance/($Distance->Target*$Realwork))*100;
if($Performanceuser>120)
$Performanceuser=120;
$Achieved=($Performanceuser*$Distance->Weight)/100;

$updateDistance=DB::table('kpi_users')
->where('kpi_users.id',$value->id)
->update(['Performance' =>$Performanceuser,'Achieved'=>$Achieved]);

array_push($GLOBAL['$PromoterData'],$Achieved);

}
else if($value->Code === '22')
{

$System = Kpi::join('kpi_users','kpi_users.kpi_id','=', 'kpis.id')
->where('kpi_users.id',$value->id)
->select('kpi_users.*')->first();
$No_Dayssystem=Gps::where('user_id',$value->user_id)
->whereBetween('NDate',array($stratdate,$enddate))
->select(DB::raw('count(distinct(NDate)) as NoDay'))
->first();

$rate=($off_Days+$No_Dayssystem->NoDay);
if($diff==1)
  $Performanceuser=($rate*100);
else
$Performanceuser=($rate/($NO_Days))*100;
if($Performanceuser>120)
$Performanceuser=120; 
$Achieved=($Performanceuser*$System->Weight)/100;

$updateDistance=DB::table('kpi_users')
->where('kpi_users.id',$value->id)
->update(['Performance' =>$Performanceuser,'Achieved'=>$Achieved]);
array_push($GLOBAL['$PromoterData'],$Achieved);


}

else if($value->Code === '8')
{

$Start_Working = Kpi::join('kpi_users','kpi_users.kpi_id','=', 'kpis.id')
->where('kpi_users.id',$value->id)
->select('kpi_users.*')->first();
$start_time = "04:00:00";
$end_time = "10:00:00";

$hour_Dayssystem=Gps::where('user_id',$value->user_id)
->whereBetween('NDate',array($stratdate,$enddate))
->whereBetween('NTime',[$start_time ,$end_time])
->select(DB::raw('count(distinct(NDate)) as NoDay'))
->first();

$rate=$hour_Dayssystem->NoDay;
$Performanceuser=($rate/($Realwork))*100;
if($Performanceuser>120)
$Performanceuser=120;
$Achieved=($Performanceuser*$Start_Working->Weight)/100;

$updateTime=DB::table('kpi_users')
->where('kpi_users.id',$value->id)
->update(['Performance' =>$Performanceuser,'Achieved'=>$Achieved]);
array_push($GLOBAL['$PromoterData'],$Achieved);
}

else if($value->Code === '7')
{

$Instagramid = Kpi::join('kpi_users','kpi_users.kpi_id','=', 'kpis.id')
->where('kpi_users.id',$value->id)
->select('kpi_users.*')->first();
$Performanceuser=($instagram/($Instagramid->Target*$NO_Days))*100;
if($Performanceuser>120)
$Performanceuser=120;
$Achieved=($Performanceuser*$Instagramid->Weight)/100;

$updateInstagram=DB::table('kpi_users')
->where('kpi_users.id',$value->id)
->update(['Performance' =>$Performanceuser,'Achieved'=>$Achieved]);
array_push($GLOBAL['$PromoterData'],$Achieved);

}
// else if($value->Code === '6')
// {
// $day_system=Gps::where('user_id',$value->user_id)
// ->whereBetween('NDate',array($stratdate,$enddate))
// ->select(DB::raw('distinct(NDate) as NDate'))
// ->get();

// $No_hours_day=[];
// $sumofmonth=0;
// foreach ($day_system as  $key) {
// $times_system=DB::table('gps')->where('user_id',$value->user_id)
// ->where('NDate',$key->NDate)
// ->select('NTime')
// ->lists('NTime');
// $othervisit= DB::table('cafesigns')->where('cafesigns.user_id',$value->user_id)
// ->where('NDate',$key->NDate)
// ->select('NTime')
// ->lists('NTime');
// $totaltime=array_merge($times_system,$othervisit);
// sort($totaltime);
// $sum=0;
// for ($i=0; $i < count($totaltime)-1; $i++) { 
// $time1 = Carbon::parse($key->NDate.$totaltime[$i]);
// $time2 = Carbon::parse($key->NDate.$totaltime[$i+1]);
// $hour = $time2->hour-$time1->hour;
// if($hour<3)
// {
// $sum+=$hour;
// }

// }
// if($sum==0)
// {
// $sum=1;
// }
// $sumofmonth+=$sum;
// }
// $Working_Hours = Kpi::join('kpi_users','kpi_users.kpi_id','=', 'kpis.id')
// ->where('kpi_users.id',$value->id)
// ->select('kpi_users.*')->first();
// $totalhours=$sumofmonth+($off_Days*8);
// $Performanceuser=($totalhours/(($Realwork+$off_Days)*8))*100;
// if($Performanceuser>120)
// $Performanceuser=120;
// $Achieved=($Performanceuser*$Working_Hours->Weight)/100;

// $updateInstagram=DB::table('kpi_users')
// ->where('kpi_users.id',$value->id)
// ->update(['Performance' =>$Performanceuser,'Achieved'=>$Achieved]);
// array_push($GLOBAL['$PromoterData'],$Achieved);
// }
else if($value->Code === '2')
{

$reportsId = Kpi::join('kpi_users','kpi_users.kpi_id','=', 'kpis.id')
->where('kpi_users.id',$value->id)
->select('kpi_users.*')->first();
$Performanceuser=($reports/($reportsId->Target*$NO_Days))*100;
if($Performanceuser>120)
$Performanceuser=120;
$Achieved=($Performanceuser*$reportsId->Weight)/100;
$updateReports=DB::table('kpi_users')
->where('kpi_users.id',$value->id)
->update(['Performance' =>$Performanceuser,'Achieved'=>$Achieved]);
array_push($GLOBAL['$PromoterData'],$Achieved);

}     

else if($value->Code === '4')
{

$contractorshomeId = Kpi::join('kpi_users','kpi_users.kpi_id','=', 'kpis.id')
->where('kpi_users.id',$value->id)
->select('kpi_users.*')->first();
$Performanceuser=($contractors_home/($contractorshomeId->Target*$percent_of_target))*100;
if($Performanceuser>120)
$Performanceuser=120;
$Achieved=($Performanceuser*$contractorshomeId->Weight)/100;

$updateContractorshome=DB::table('kpi_users')
->where('kpi_users.id',$value->id)
->update(['Performance' =>$Performanceuser,'Achieved'=>$Achieved]);
array_push($GLOBAL['$PromoterData'],$Achieved);
}
else if($value->Code === '3')
{

$No_Contractors = Kpi::join('kpi_users','kpi_users.kpi_id','=', 'kpis.id')
->where('kpi_users.id',$value->id)
->select('kpi_users.*')->first();
$b =Gps::whereBetween('NDate',array($stratdate,$enddate))
->select(array(DB::raw('count(distinct(Contractor_Code))  as count')),'NDate')
->where('user_id',$value->user_id)
->groupBy('NDate')
->get();

$totalContractort=0;
foreach ($b as $key) {
$totalContractort+=$key->count;
}   
$Performanceuser=($totalContractort/($No_Contractors->Target*$Realwork))*100;
if($Performanceuser>120)
$Performanceuser=120;
$Achieved=($Performanceuser*$No_Contractors->Weight)/100;

$updateContractorsNo=DB::table('kpi_users')
->where('kpi_users.id',$value->id)
->update(['Performance' =>$Performanceuser,'Achieved'=>$Achieved]);
array_push($GLOBAL['$PromoterData'],$Achieved); 

}
else if($value->Code === '1')
{
$numberofvisits = Kpi::join('kpi_users','kpi_users.kpi_id','=', 'kpis.id')
->where('kpi_users.id',$value->id)
->select('kpi_users.*')->first();
$Days=Gps::whereBetween('NDate',array($stratdate,$enddate))
->select(DB::raw('distinct(NDate) as NDate'))->get();

$totalVisit_M=0;
foreach ($Days as $day) {
$gpses = Gps::select(array('type' ,'Contractor_Code',DB::raw('count(type)  as visits')))
->where('NDate',$day->NDate)
->where('gps.user_id',$value->user_id)
->groupBy('Contractor_Code')
->get();
$oldtotalvisit=0;
foreach ($gpses as $gps) {
if($gps->type ==='حصر كميات')
{
$togps = Gps::select(DB::raw('count(*)  as visits'))
->where('NDate',$day->NDate)
->where('gps.user_id',$value->user_id)
->where('Contractor_Code',$gps->Contractor_Code)
->where('type','حصر كميات')
->first();


if($gps->visits >  $togps->visits)
{

$gps->visits= $togps->visits;

}
}
if(($gps->type ==='المبيعات'&& $gps->visits===2)||($gps->type ==='تسويق'&& $gps->visits===2))
{

$gps->visits=$gps->visits/2;
}


$oldtotalvisit+=$gps->visits;


}
$totalVisit_M +=$oldtotalvisit;

}
// dd($totalVisit_M,$numberofvisits);

$Performanceuser=($totalVisit_M/($numberofvisits->Target*$Realwork))*100;
if($Performanceuser>120)
$Performanceuser=120;
$Achieved=($Performanceuser*$numberofvisits->Weight)/100;

$updatevisitNo=DB::table('kpi_users')
->where('kpi_users.id',$value->id)
->update(['Performance' =>$Performanceuser,'Achieved'=>$Achieved]);

array_push($GLOBAL['$PromoterData'],$Achieved);  



}
else if($value->Code === '10')


{

$Numberofdaywork = Kpi::join('kpi_users','kpi_users.kpi_id','=', 'kpis.id')
->where('kpi_users.id',$value->id)
->select('kpi_users.*')->first();

$Performanceuser=($NO_Days/($Numberofdaywork->Target*$percent_of_target))*100;
if($Performanceuser>120)
$Performanceuser=120;
$Achieved=($Performanceuser*$Numberofdaywork->Weight)/100;
//dd($Performanceuser);

$updateNumberofdaywork=DB::table('kpi_users')
->where('kpi_users.id',$value->id)
->update(['Performance' =>$Performanceuser,'Achieved'=>$Achieved]);

array_push($GLOBAL['$PromoterData'],$Achieved); 


}
else if($value->Code === '11')
{

$numberofnewcontractor = Kpi::join('kpi_users','kpi_users.kpi_id','=', 'kpis.id')
->where('kpi_users.id',$value->id)
->select('kpi_users.*')->first();

$NewContractor=Gps::join('contractors','contractors.id','=', 'gps.contractor_id')
->whereBetween('contractors.created_at',array($startdateformonth,$enddate))
->where('user_id',$value->user_id)
->where('contractors.Status','New')
->select(DB::raw('count(distinct(gps.contractor_id)) as count'))
->first();
$Performanceuser=($NewContractor->count/($numberofnewcontractor->Target*$percent_of_target))*100;
if($Performanceuser>120)
$Performanceuser=120;
$Achieved=($Performanceuser*$numberofnewcontractor->Weight)/100;

$updatenumberofnewcontractor=DB::table('kpi_users')
->where('kpi_users.id',$value->id)
->update(['Performance' =>$Performanceuser,'Achieved'=>$Achieved]); 
array_push($GLOBAL['$PromoterData'],$Achieved); 

}
else if($value->Code === '13')
{


$numberofCafe = Kpi::join('kpi_users','kpi_users.kpi_id','=', 'kpis.id')
->where('kpi_users.id',$value->id)
->select('kpi_users.*')->first();

$CafeCemex =CafeSigns::select(DB::raw('count(*)  as count'))
->where('cafesigns.user_id',$value->user_id)
->whereBetween('NDate',array($startdateformonth,$enddate))
->where('cafesigns.Type','Cafe')
->first();
$Performanceuser=($CafeCemex->count/($numberofCafe->Target*$percent_of_target))*100;
if($Performanceuser>120)
$Performanceuser=120;
$Achieved=($Performanceuser*$numberofCafe->Weight)/100;

$updatenumberofCafe = DB::table('kpi_users')
->where('kpi_users.id',$value->id)
->update(['Performance' =>$Performanceuser,'Achieved'=>$Achieved]);

array_push($GLOBAL['$PromoterData'],$Achieved); 
}
else if($value->Code === '12')
{

$numberofSign = Kpi::join('kpi_users','kpi_users.kpi_id','=', 'kpis.id')
->where('kpi_users.id',$value->id)
->select('kpi_users.*')->first();
$SignCemex = CafeSigns::select(DB::raw('count(*)  as count'))
->where('cafesigns.user_id',$value->user_id)
->whereBetween('NDate',array($startdateformonth,$enddate))
->where('cafesigns.Type','Signs')
->first();
$Performanceuser=($SignCemex->count/($numberofSign->Target*$percent_of_target))*100;
if($Performanceuser>120)
$Performanceuser=120;
$Achieved=($Performanceuser*$numberofSign->Weight)/100;
$updatenumberofSign = DB::table('kpi_users')
->where('kpi_users.id',$value->id)
->update(['Performance' =>$Performanceuser,'Achieved'=>$Achieved]);
array_push($GLOBAL['$PromoterData'],$Achieved); 

}
else if($value->Code === '14')
{

$numberofcompatitoractivity = Kpi::join('kpi_users','kpi_users.kpi_id','=', 'kpis.id')
->where('kpi_users.id',$value->id)
->select('kpi_users.*')->first();
$Activities = Activity::select(DB::raw('count(*)  as count'))
->where('activities.User_id',$value->user_id)
->whereBetween('Sent_Date',array($startdateformonth,$enddate))
->first(); 
$Performanceuser=($Activities->count/($numberofcompatitoractivity->Target*$percent_of_target))*100;
if($Performanceuser>120)
$Performanceuser=120;
$Achieved=($Performanceuser*$numberofcompatitoractivity->Weight)/100;

$updatenumberofcompatitoractivity = DB::table('kpi_users')
->where('kpi_users.id',$value->id)
->update(['Performance' =>$Performanceuser,'Achieved'=>$Achieved]);
array_push($GLOBAL['$PromoterData'],$Achieved);

}
else if($value->Code === '15')
{

 
$numberofprices = Kpi::join('kpi_users','kpi_users.kpi_id','=', 'kpis.id')
->where('kpi_users.id',$value->id)
->select('kpi_users.*')->first();


  $Num_Price=0;
$day = new Carbon($enddate);
$countfor=floor($day->day /7);
for ($i=0; $i <$countfor ; $i++) { 


   $dt1 = new Carbon($startdateformonth);

   $dtst = $dt1->addWeeks($i)->toDateString();
   $dten = $dt1->addWeek()->toDateString();


$Prices = Price::select(DB::raw('count(distinct(prices.Product_id))  as count'))
->where('prices.User_id',$value->user_id)
->whereBetween('Date',array( $dtst ,$dten))
->first(); 

if($Prices->count >5)
{
$Num_Price+=1;
}

}



  
$Performanceuser=($Num_Price/($numberofprices->Target*$countfor*$percent_of_target))*100;
if($Performanceuser>120)
$Performanceuser=120;
$Achieved=($Performanceuser*$numberofprices->Weight)/100;

$updatenumberofprices =DB::table('kpi_users')
->where('kpi_users.id',$value->id)
->update(['Performance' =>$Performanceuser,'Achieved'=>$Achieved]);
array_push($GLOBAL['$PromoterData'],$Achieved); 

}

else if($value->Code === '19')
{

$AmountofCementOrder = Kpi::join('kpi_users','kpi_users.kpi_id','=', 'kpis.id')
->where('kpi_users.id',$value->id)
->select('kpi_users.*')->first();
$CementOrder = Order::select(DB::raw('sum(orders.Amount)  as count'))
->join('gps','gps.id', '=', 'orders.gps_id')
->where('gps.user_id',$value->user_id)
->whereBetween('NDate',array($startdateformonth,$enddate))
->where('orders.Type','حصر كميات')
->first();


$Performanceuser=($CementOrder->count/($AmountofCementOrder->Target*$percent_of_target))*100;
if($Performanceuser>120)
$Performanceuser=120;
$Achieved=($Performanceuser*$AmountofCementOrder->Weight)/100;


$updateAmountofCementOrder =DB::table('kpi_users')
->where('kpi_users.id',$value->id)
->update(['Performance' =>$Performanceuser,'Achieved'=>$Achieved]);
array_push($GLOBAL['$PromoterData'],$Achieved); 

}

else if($value->Code === '9')
{

$percentageoftotalcontractor =Kpi::join('kpi_users','kpi_users.kpi_id','=', 'kpis.id')
->where('kpi_users.id',$value->id)
->select('kpi_users.*')->first();
$total_contractor =Gps::select(DB::raw('count(distinct(contractor_id))  as count'))
->whereBetween('NDate',array($startdateformonth,$enddate))
->where('gps.user_id','=',$value->user_id)
->first();

  // echo $value->user_id."\n".$percentageoftotalcontractor->Target;
$Performanceuser=($total_contractor->count/($percentageoftotalcontractor->Target*$percent_of_target))*100;
if($Performanceuser>120)
$Performanceuser=120;
$Achieved=($Performanceuser*$percentageoftotalcontractor->Weight)/100;
$updatetotalcontractor=DB::table('kpi_users')
->where('kpi_users.id',$value->id)
->update(['Performance' =>$Performanceuser,'Achieved'=>$Achieved]);
array_push($GLOBAL['$PromoterData'],$Achieved); 

}

}


$Anaylsis = floor(number_format($BAnaylsis, 2, '.', ''));
$usersalary=User::withTrashed()->find($userId->id);
if($diff==1)
  $totalsalary=($usersalary->Salary);
  else
$totalsalary=($usersalary->Salary*$NO_Days);
$FinalPerformance=0.0;
for($i=1;$i<count($GLOBAL['$PromoterData']);$i++) {
$FinalPerformance+=$GLOBAL['$PromoterData'][$i];
}

if($FinalPerformance>120)
{
$FinalPerformance=120;  
}

$sumation=($FinalPerformance/100)*$totalsalary;
$resumation=$sumation*($Anaylsis/100);

$resumation = floor(number_format($resumation, 2, '.', ''));
$resumation+=$Bouns;
$resumation+=$Days_meeting*$usersalary->Salary;
array_push($GLOBAL['$KpiData'],array('Name'=>$GLOBAL['$PromoterData'][0],'Salary'=>$resumation));
$salary_Exsits=salary_user::where('users_id',$usersalary->id)
->where('Date',$stratdate)
->first();
if($salary_Exsits !=null)
{

$deletesalary = salary_user::find($salary_Exsits->id);
$deletesalary->delete();

}

$salary_user= new salary_user;
$salary_user->DailyWeage =$usersalary;
$salary_user->DailyWeage =$usersalary->Salary;
$salary_user->Date =$stratdate;
$salary_user->Performance =$FinalPerformance;
$salary_user->BackCheck =$Anaylsis;
$salary_user->FinalSalary =$resumation;
$salary_user->users_id =$usersalary->id;
$salary_user->save();

}
}

});
}
return redirect('/userkpi');
// if($value->Code === '16')
// {

//             $gooddatacontractor = Kpi::join('kpi_users','kpi_users.kpi_id','=', 'Kpis.id')

//                              ->where('Kpis.Code',16) ->where('Kpis.id','=',$value->kpi_id)

//                              ->where('kpi_users.id',$value->id)
//                              ->select('kpi_users.*')->first();

// }
//  if($value->Code === '17')
// {

//             $AmountofSaleOrders  = Kpi::join('kpi_users','kpi_users.kpi_id','=', 'Kpis.id')

//                              ->where('Kpis.Code',17) ->where('Kpis.id','=',$value->kpi_id)

//                              ->where('kpi_users.id',$value->id)
//                              ->select('kpi_users.*')->first();

//    }
//    if($value->Code === '18')
// {

//             $Contractorsofactivity = Kpi::join('kpi_users','kpi_users.kpi_id','=', 'Kpis.id')

//                              ->where('Kpis.Code',18) ->where('Kpis.id','=',$value->kpi_id)

//                              ->where('kpi_users.id',$value->id)
//                              ->select('kpi_users.*')->first();
//  }








//                 //number of visits
//                 $gps = Gps::select(array('username','type' ,'Contractor_Code',DB::raw('count(type)  as visits')))
//                 ->join('users', 'users.id', '=', 'gps.user_id')
//                 ->whereBetween('NDate',array($stratdate,$enddate))
//                 ->where('users.Code',$user_code)
//                 ->groupBy('username','Contractor_Code')
//                 ->orderby('username')
//                 ->get();
//                 $data=array();
//                 $data['username']=0;

//                 foreach ($gps as $key) 
//                 {
//                     if($key->type ==='حصر كميات')
//                     {
//                         $user_id=user::select('id')->where('username',$key->username)->pluck('id')->first();      
//                         $togps =Gps::select(DB::raw('count(*)  as visits'))
//                                ->where('NDate','=',$date)
//                                ->where('user_id',$user_id)
//                                ->where('Contractor_Code',$key->Contractor_Code)
//                                ->where('type','حصر كميات')
//                                ->first();
//                         if($key->visits >  $togps->visits)
//                         {
//                             $key->visits= $togps->visits;
//                         }
//                     }
//                     if(($key->type ==='المبيعات'&& $key->visits===2)||($key->type ==='تسويق'&& $key->visits===2))
//                     {
//                         $key->visits=$key->visits/2;
//                     }
//                     if((array_search($key->username,array_keys($data))))
//                     {
//                        $old=$data[$key->username];
//                        $data[$key->username]= $old+$key->visits;                                                 
//                     }
//                     else
//                     {
//                         $data[$key->username]=$key->visits;      
//                     }                                                      
//                 }


//                 unset($data['username']) ;

//                 foreach ($data as $key => $value)
//                 {

//                     array_push($GLOBAL['$PromoterData'],$key);
//                     array_push($GLOBAL['$PromoterData'],($value/$numberofvisits->Target)*($numberofvisits->Weight/100));


//                     if($numberofvisits != null)
//                     {
//                     $ExsitsKPI=true;
//                     } 
//                     else
//                     {
//                     $ExsitsKPI=false;
//                     }                  

//                     if($ExsitsKPI==true)
//                     {

//                     $updatenumberofvisits=DB::table('kpi_users')
//                                         ->where('kpi_id', $numberofvisits->kpi_id)
//                                         ->where('user_id',$userId->id)
//                                         ->where('From_Date',$stratdate)
//                                         ->where('To_Date',$enddate)
//                                         ->update(['Performance' =>($value/$numberofvisits->Target)*($numberofvisits->Weight/100)]);
//                                     }
//                 }


// $total_contractor =Gps::select(array('username','Contractor_Code',DB::raw('count(distinct(contractor_id))  as contractors')))
// ->whereBetween('NDate',$range)
// ->where('users.id','=',$Users)
// ->join('users', 'users.id', '=', 'gps.user_id')
// ->groupBy('username','Contractor_Code')
// ->orderby('username')
// ->get();

//  if($No_Contractors!= null)
//                     {
//                     $ExsitsKPI=true;
//                     } 
//                     else
//                     {
//                     $ExsitsKPI=false;
//                     }                  

//                     if($ExsitsKPI==true)
//                     {

//                 $updatePhonecalls=DB::table('kpi_users')
//                                 ->where('kpi_id', $No_Contractors->kpi_id)
//                                 ->where('user_id',$userId->id)
//                                 ->where('From_Date',$stratdate)
//                                 ->where('To_Date',$enddate)
//                                 ->update(['Performance' =>($phone_calls/$No_Contractors->Target)*($No_Contractors->Weight/100)]);
//                 array_push($GLOBAL['$PromoterData'],($phone_calls/$No_Contractors->Target)*($No_Contractors->Weight/100));
//                 //Number of day work
//                    // $query = Gps::select(DB::raw('count(*)  as count'))
//                    //      ->join('users', 'users.id', '=', 'gps.user_id')
//                    //      ->whereBetween('NDate',array($stratdate,$enddate))
//                    //      ->where('users.Code',$user_code)
//                    //      ->groupBy('users.id','gps.NDate')
//                    //      ->get();



// }
//                 //number of new contractor

//                     if($numberofnewcontractor != null)
//                      {
//                        $ExsitsKPI=true;
//                      } 
//                      else
//                      {
//                         $ExsitsKPI=false;
//                      }                  

//             if($ExsitsKPI==true)
//              {     

//                 $NewContractor = Contractor::select(DB::raw('count(*)  as count'))
//                                 ->join('users', 'users.id', '=', 'contractors.Pormoter_Id')
//                                 ->where('users.Code',$user_code)
//                                 ->where('contractors.Status','New')
//                                 ->first();




//              }


//                     if($numberofCafe!= null)
//                      {
//                        $ExsitsKPI=true;
//                      } 
//                      else
//                      {
//                         $ExsitsKPI=false;
//                      } 




//                 //number of Cafe  
//             if($ExsitsKPI==true)
//              { 


//                }

//     if( $numberofSign != null)
//                      {
//                        $ExsitsKPI=true;
//                      } 
//                      else
//                      {
//                         $ExsitsKPI=false;
//                      } 



//   if($ExsitsKPI==true)
//              { 
//  //number of Sign  

//       }











//                 //number of compatitor activity 

//                     if($numberofcompatitoractivity != null)
//                      {
//                        $ExsitsKPI=true;
//                      } 
//                      else
//                      {
//                         $ExsitsKPI=false;
//                      }                  


//                   if($ExsitsKPI==true)
//              { 


// }

//                     if($numberofprices != null)
//                     {
//                     $ExsitsKPI=true;
//                     } 
//                     else
//                     {
//                     $ExsitsKPI=false;
//                     }                  

//        if($ExsitsKPI==true)
//              { 
//                 //number of prices





//                 //Amount of Cement Order 


//                     if($AmountofCementOrder != null)
//                     {
//                     $ExsitsKPI=true;
//                     } 
//                     else
//                     {
//                     $ExsitsKPI=false;
//                     }                  

//                     if($ExsitsKPI==true)
//                     {


//                 }

//Amount of SaleOrders 

//                     if($AmountofSaleOrders != null)
//                     {
//                     $ExsitsKPI=true;
//                     } 
//                     else
//                     {
//                     $ExsitsKPI=false;
//                     }                  

//                     if($ExsitsKPI==true)
//                     {

//                 $SaleOrders =Order::select(DB::raw('count(orders.id)  as count'))
//                                 ->join('gps','gps.id', '=', 'orders.gps_id')
//                                 ->join('users','users.id', '=', 'gps.user_id')
//                                 ->where('users.Code',$user_code)
//                                  ->whereBetween('NDate',array($stratdate,$enddate))
//                                  ->where('orders.Type','مبيعات')
//                                  ->where('IsDone',1)
//                                 ->first();
//                 array_push($GLOBAL['$PromoterData'],((int)$SaleOrders->count/$AmountofSaleOrders->Target)*($AmountofSaleOrders->Weight/100)); 
//                 $updateAmountofSaleOrders =DB::table('kpi_users')
//                                             ->where('kpi_id', $AmountofSaleOrders->kpi_id)
//                                             ->where('user_id',$userId->id)
//                                             ->where('From_Date',$stratdate)
//                                             ->where('To_Date',$enddate)
//                                             ->update(['Performance' =>((int)$SaleOrders->count/$AmountofSaleOrders->Target)*($AmountofSaleOrders->Weight/100)]);
// }
//Contractors of activity 

//                     if($Contractorsofactivity != null)
//                     {
//                     $ExsitsKPI=true;
//                     } 
//                     else
//                     {
//                     $ExsitsKPI=false;
//                     }                  

//                     if($ExsitsKPI==true)
//                     {



//                 $CreateStart = Carbon::parse($stratdate);
//                 $activityitemids= DB::table('activityitem_attributes')
//                                  ->join('activity_items','activity_items.id','=','activityitem_attributes.activityitem_id')
//                                  ->join('sub_activities','sub_activities.id','=','activity_items.subactivity_id')
//                                  ->where('sub_activities.EngName','=','School Of Contractors')
//                                  ->whereBetween('activityitem_attributes.Value',array($CreateStart->subMonths(1),$enddate))
//                                  ->select('activityitem_attributes.activityitem_id')
//                                  ->groupBy('activityitem_attributes.activityitem_id')->lists('activityitem_attributes.activityitem_id');

//                 $Contractors=DB::table('activity_contractors_retailers')
//                            ->join('activity_items','activity_items.id','=','activity_contractors_retailers.activityitem_id')
//                            ->join('contractors','contractors.id','=','activity_contractors_retailers.contractor_id')
//                            ->join('users', 'users.id', '=', 'contractors.Pormoter_Id')
//                            ->where('users.Code',$user_code)
//                            ->whereIn('activity_contractors_retailers.activityitem_id',$activityitemids)
//                            ->select('contractors.id','contractors.Name',DB::raw('count(contractors.id)  as count'))
//                            ->groupBy('contractors.id','contractors.Name')
//                            ->get();

//                 $Count=0;
//                 foreach ($Contractors  as  $value) 
//                 {
//                     if ($value->count>1)
//                     {
//                        $Count=$Count+1; 
//                     } 
//                 }
//                 if((int)$Count==0)
//                     $count=1;
//                 else if(((int)$Count/count($Contractors))==1)
//                     $count=0;
//                 else
//                     $count=(int)$Count/count($Contractors);




//                 array_push($GLOBAL['$PromoterData'],($count/$Contractorsofactivity->Target)*($Contractorsofactivity->Weight/100)); 
//                 $updateContractorsofactivity =DB::table('kpi_users')
//                                             ->where('kpi_id', $Contractorsofactivity->id)
//                                             ->where('user_id',$userId->id)
//                                             ->where('From_Date',$stratdate)
//                                             ->where('To_Date',$enddate)
//                                             ->update(['Performance' =>($count/$Contractorsofactivity->Target)*($Contractorsofactivity->Weight/100)]);
// }
///////////////////////////////////////
// $UserDistrict=DB::table('user_district')
//                            ->join('users','users.id','=','user_district.users_id')
//                            ->join('districts','districts.id','=','user_district.District_id')
//                            ->where('users.Code',$user_code)
//                            ->select('districts.Name')
//                            ->lists('districts.Name');


//                             if($percentageoftotalcontractor!= null)
//                             {
//                             $ExsitsKPI=true;
//                             } 
//                             else
//                             {
//                             $ExsitsKPI=false;

//                             }                  

//                     if($ExsitsKPI==true)
//                     {

//                 $ContractorActivitiyid= DB::table('activityitem_attributes')
//                                      ->join('activity_items','activity_items.id','=','activityitem_attributes.activityitem_id')
//                                      ->join('sub_activities','sub_activities.id','=','activity_items.subactivity_id')
//                                      ->whereBetween('activityitem_attributes.Value',array($stratdate,$enddate))
//                                      ->select('activityitem_attributes.activityitem_id')
//                                      ->groupBy('activityitem_attributes.activityitem_id')->lists('activityitem_attributes.activityitem_id');   
//                 $Contractors=DB::table('activity_contractors_retailers')
//                            ->join('activity_items','activity_items.id','=','activity_contractors_retailers.activityitem_id')
//                            ->join('contractors','contractors.id','=','activity_contractors_retailers.contractor_id')
//                            ->join('users', 'users.id', '=', 'contractors.Pormoter_Id')
//                            ->where('users.Code',$user_code)
//                            ->whereIn('activity_contractors_retailers.activityitem_id',$ContractorActivitiyid)
//                            ->select('contractors.id','contractors.Name',DB::raw('count(contractors.id)  as count'))
//                            ->groupBy('contractors.id','contractors.Name')
//                            ->get();

//                 $totalContractorinDistrict=Contractor::select(DB::raw('count(*)  as count'))
//                                             ->whereIn('contractors.District',$UserDistrict)
//                                             ->first();
//                                             if ($totalContractorinDistrict->count==0)
//                                             {
//                                                $totalContractorinDistrict->count=0.005; 
//                                             }
//                 $percentage=(count($Contractors)/$totalContractorinDistrict->count);
//                 array_push($GLOBAL['$PromoterData'],($percentage/$percentageoftotalcontractor->Target)*($percentageoftotalcontractor->Weight/100));
//                 $updatepercentageoftotalcontractor =DB::table('kpi_users')
//                                                     ->where('kpi_id', $percentageoftotalcontractor->id)
//                                                     ->where('user_id',$userId->id)
//                                                     ->where('From_Date',$stratdate)
//                                                     ->where('To_Date',$enddate)
//                                                     ->update(['Performance' =>($percentage/$percentageoftotalcontractor->Target)*($percentageoftotalcontractor->Weight/100)]);
//                 }




// //Safety
//                      if($Safety!= null)
//                     {
//                     $ExsitsKPI=true;
//                     } 
//                     else
//                     {
//                     $ExsitsKPI=false;
//                     }                  

//                     if($ExsitsKPI==true)
//                     {


// //Distance
//                      if($Distance!= null)
//                     {
//                     $ExsitsKPI=true;
//                     } 
//                     else
//                     {
//                     $ExsitsKPI=false;
//                     }                  

//                     if($ExsitsKPI==true)
//                     {






// //Number of day work

//  if($Numberofdaywork != null)
//                     {
//                      $ExsitsKPI=true;
//                     } 
//                     else
//                     {
//                      $ExsitsKPI=false;
//                     }                  

//                     if($ExsitsKPI==true)
//                     {




// }

//                     if($facebookId!= null)
//                     {
//                     $ExsitsKPI=true;
//                     } 
//                     else
//                     {
//                     $ExsitsKPI=false;
//                     }                  

//                     if($ExsitsKPI==true)
//                     {

//                 $updateFacebok=DB::table('kpi_users')
//                                 ->where('kpi_id', $facebookId->kpi_id)
//                                 ->where('user_id',$userId->id)
//                                 ->where('From_Date',$stratdate)
//                                 ->where('To_Date',$enddate)
//                                 ->update(['Performance' =>($facebook/$facebookId->Target)*($facebookId->Weight/100)]);
//                 array_push($GLOBAL['$PromoterData'],($facebook/$facebookId->Target)*($facebookId->Weight/100));

// }

//                     if($instagramId!= null)
//                     {
//                     $ExsitsKPI=true;
//                     } 
//                     else
//                     {
//                     $ExsitsKPI=false;
//                     }                  

//                     if($ExsitsKPI==true)
//                     {

//                 $updateInstagram=DB::table('kpi_users')
//                                 ->where('kpi_id', $instagramId->kpi_id)
//                                 ->where('user_id',$userId->id)
//                                 ->where('From_Date',$stratdate)
//                                 ->where('To_Date',$enddate)
//                                 ->update(['Performance' =>($instagram/$instagramId->Target)*($instagramId->Weight/100)]);
//                                 // dd( $updateInstagram);
//                 array_push($GLOBAL['$PromoterData'],($instagram/$instagramId->Target)*($instagramId->Weight/100));

//             }

//          //Start_Working
//                     if($Start_Working!= null)
//                     {
//                     $ExsitsKPI=true;
//                     } 
//                     else
//                     {
//                     $ExsitsKPI=false;
//                     }                  

//                     if($ExsitsKPI==true)
//                     {

//                 $updateYoutub=DB::table('kpi_users')
//                                 ->where('kpi_id', $Start_Working->kpi_id)
//                                 ->where('user_id',$userId->id)
//                                 ->where('From_Date',$stratdate)
//                                 ->where('To_Date',$enddate)
//                                 ->update(['Performance' =>($youtube/$Start_Working->Target)*($Start_Working->Weight/100)]);
//                 array_push($GLOBAL['$PromoterData'],($youtube/$Start_Working->Target)*($Start_Working->Weight/100));
//             }

// //Working_Hours

//                     if($Working_Hours!= null)
//                     {
//                     $ExsitsKPI=true;
//                     } 
//                     else
//                     {
//                     $ExsitsKPI=false;
//                     }                  

//                     if($ExsitsKPI==true)
//                     {

//                 $updateWebsite=DB::table('kpi_users')
//                                 ->where('kpi_id', $Working_Hours->kpi_id)
//                                 ->where('user_id',$userId->id)
//                                 ->where('From_Date',$stratdate)
//                                 ->where('To_Date',$enddate)
//                                 ->update(['Performance' =>($No_hours/$Working_Hours->Target)*($Working_Hours->Weight/100)]);
//                 array_push($GLOBAL['$PromoterData'],($No_hours/$Working_Hours->Target)*($Working_Hours->Weight/100));
//             }


//                     if($reportsId!= null)
//                     {
//                     $ExsitsKPI=true;
//                     } 
//                     else
//                     {
//                     $ExsitsKPI=false;
//                     }                  

//                     if($ExsitsKPI==true)
//                     {

//             }


//                     if($contractorshomeId != null)
//                     {
//                     $ExsitsKPI=true;
//                     } 
//                     else
//                     {
//                     $ExsitsKPI=false;
//                     }                  

//                     if($ExsitsKPI==true)
//                     {


// }
//                 //Goog Date Contractor
//                 $totalDistric_Contractors=Contractor::join('reviews','reviews.Contractor_Id','=','contractors.id')           
//                                         ->whereIn('contractors.District',$UserDistrict)
//                                         ->select('*')
//                                         ->get();
//                 $totalContractorst=Contractor::select(DB::raw('count(*)  as Concount'))   
//                                    ->whereIn('contractors.District',$UserDistrict)
//                                    ->first();
//                 $count=0;
//                 foreach ($totalDistric_Contractors as $totalContractorinDistrict)
//                 {
//                     if($totalContractorinDistrict->Status=='Reviewed')
//                     {

//                             if ((!empty($totalContractorinDistrict->Name))  ||
//                                 (!empty($totalContractorinDistrict->Government) ) ||
//                                 (!empty($totalContractorinDistrict->District)) ||
//                                 (!empty($totalContractorinDistrict->Tele1)))
//                             {
//                                 $count ++;
//                             }

//                     }
//                 } 
//                   if ($totalContractorst->Concount==0)
//                     {
//                        $totalContractorst->Concount=0.005; 
//                     }  
//                 $QuilatyContractor=( $count/$totalContractorst->Concount);


//     if($gooddatacontractor != null)
//     {
//     $ExsitsKPI=true;
//     } 
//     else
//     {
//     $ExsitsKPI=false;
//     }                  

//     if($ExsitsKPI==true)
//     {

// $updategooddatacontractor=DB::table('kpi_users')
//                         ->where('kpi_id', $gooddatacontractor->kpi_id)
//                         ->where('user_id',$userId->id)
//                         ->where('From_Date',$stratdate)
//                         ->where('To_Date',$enddate)
//                         ->update(['Performance' =>($QuilatyContractor/$gooddatacontractor->Target)*($gooddatacontractor->Weight/100)]);
// array_push($GLOBAL['$PromoterData'],($QuilatyContractor/$gooddatacontractor->Target)*($gooddatacontractor->Weight/100));

//  }
//backCheck
// $Anwser=0;
// $closed=0;
// $No_answer=0;
// $Repeat=0;
// $Wrong=0;
// $NO= 0;
// $callbackcheck =Gps::join('users','users.id', '=', 'gps.user_id')
//                  ->where('users.Code',$user_code)
//                  ->whereBetween('NDate',array($stratdate,$enddate))
//                  ->select(DB::raw('count(Backcheck) as total'),'Backcheck')
//                  ->groupBy('Backcheck')->get();
// foreach ($callbackcheck as $value)
// {
//     if($value->Backcheck=="نعم")
//     {
//         $Anwser=$value->total;
//     }
//     else if($value->Backcheck=="لا")
//     {
//         $NO=$value->total;
//     }
//     else if($value->Backcheck=="مغلق")
//     {
//         $closed=$value->total;
//     }
//     else if($value->Backcheck=='لم يرد')
//     {
//         $No_answer=$value->total;
//     }
//     else if($value->Backcheck=="متكرر")
//     {
//         $Repeat=$value->total/5;
//     }
//     else if($value->Backcheck=='رقم خطأ')
//     {
//         $Anwser=$Wrong->total;
//     }
// }

// if($Anwser>0)
// {
//     $Total=($Anwser/($Anwser+$NO));
// }
// else
// {
//     $Total=0.1;
// }

// $Sub=($No_answer*(0.01))+($Repeat*(0.05))+($closed*(0.01))+($Wrong*(0.05));    
// $BAnaylsis=($Total-$Sub)*100; 
//dd($BAnaylsis);     


// $sumation=$totalsalary*(
//                ($PERWorks_Day)+($PERGPS)+($PERCement_Quantity)+
//                ($PERCall_count)+($PERVisit_count)+ (($profacebook*$FB)/100)+
//                (($pronewcon*$NewCon)/100)+ (($proinstgram*$Inst)/100)+ (($proWebsite*$WebSite)/100)+
//                (($proreports*$Reports)/100)+(($proyoutube*$Youtube)/100)+
//                (.06*.5)+(.1*.06)+(.07*.8)+(.07*.1)+(.08*.8)+(1*.05)+($PERVisit_count));

//Save in Table New History Salary For User 
//Doaa Elgheny Update at 11/06/2017
}
else
{
return redirect('/login');
}

} 
public function getuserkpi()
{
   if (Auth::check())
  {
$start = new Carbon('first day of last month');
$end=new Carbon('last day of last month');
// $start = new Carbon('2017-12-01');
// $end=new Carbon('2017-12-31');
$userid=Input::get('user_id');

$userkpis=DB::table('kpi_users')
->where('user_id',$userid)
->whereBetween('To_Date',[$start->toDateString(),$end->toDateString()])
->whereBetween('From_Date',[$start->toDateString(),$end->toDateString()])
->get();
$kpi=Kpi::where('Code','9')->value('id');
foreach ($userkpis as $value) {
if($value->kpi_id==$kpi)
{
$Weight=$value->Target;
$Weight+=($Weight*.1);
$value->Target=floor($Weight);
}
}

return $userkpis;
}
else
{
return redirect('/login');
}
} 
/**
* Store a newly created resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @return \Illuminate\Http\Response
*/
public function store(Request $request)
{
 if (Auth::check())
  {
$Performance=0;
$Kpis=Input::get('Kpi');
$Users=Input::get('Users');
$Target=Input::get('Target');
$Weight=Input::get('Weight');



$n=count($Kpis);
$M=count($Users);


for($i=0;$i<$n;$i++) //KPI
{
$user=User::where('id',$Users)->first();
$querykpi=DB::table('kpi_users')->where('user_id',$user->id)
->where('From_Date',Input::get('From_Date'))
->where('To_Date',Input::get('End_Date'))
->where('kpi_id',$Kpis[$i])
->first();
if($querykpi!=[])
DB::table('kpi_users')->delete($querykpi->id);
$user->getUserKPI()->attach($Kpis[$i], ['Performance'=>$Performance,'From_Date'=>Input::get('From_Date'),'To_Date'=>Input::get('End_Date'),'Weight'=>$Weight[$i],'Target'=>$Target[$i]]);
}
$Users=User::where('Type_User','Outdoor Promoters')->select('Name','id')->lists('Name','id');

return redirect('/KPIUsers');
}
else
{
return redirect('/login');
}
}
/**
* Display the specified resource.
*
* @param  int  $id
* @return \Illuminate\Http\Response
*/
public function show($id)
{
//
}

/**
* Show the form for editing the specified resource.
*
* @param  int  $id
* @return \Illuminate\Http\Response
*/
public function edit($id)
{
//
}

/**
* Update the specified resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @param  int  $id
* @return \Illuminate\Http\Response
*/
public function updateactiveBrand()
{

$brandid = Input::get('id'); 
$active= Input::get('active');

$brand = brand::whereId($brandid)->first();

$brand->Active=$active;

if($brand->save())

return \Response::json(array('status'=>1));
else 
return \Response::json(array('status'=>0));

}

public function update(Request $request, $id)
{
//
}

/**
* Remove the specified resource from storage.
*
* @param  int  $id
* @return \Illuminate\Http\Response
*/
public function destroy()
{
if( Input::get('active')==0)
{
//We Should Delete KPI 
$user=User::where('id',input::get('user'))->first();
// $user=getUserKPI()->((int)$Kpis[$i], ['Performance'=>$Performance]);

}
}
public function calculmountofvisitor($code,$FromDate,$EnDate)
{


$gps = Gps::select(array('username','type' ,'Contractor_Code',DB::raw('count(type)  as visits')))
->join('users', 'users.id', '=', 'gps.user_id')
->whereBetween('NDate',array($FromDate,$EnDate))
->where('users.Code',$code)
->groupBy('username','Contractor_Code')
->orderby('username')
->get();


$data=array();
$data['username']=0;
foreach ($gps as $key) {
if($key->type ==='حصر كميات')
{

$user_id=user::select('id')->where('username',$key->username)->pluck('id')->first();

$togps = Gps::select(DB::raw('count(*)  as visits'))
->where('NDate','=',$date)
->where('user_id',$user_id)
->where('Contractor_Code',$key->Contractor_Code)
->where('type','حصر كميات')
->first();


if($key->visits >  $togps->visits)
{
$key->visits= $togps->visits;
}
}
if(($key->type ==='المبيعات'&& $key->visits===2)||($key->type ==='تسويق'&& $key->visits===2))
{

$key->visits=$key->visits/2;
}

if((array_search($key->username,array_keys($data))))
{
$old=$data[$key->username];
$data[$key->username]= $old+$key->visits; 

}
else{
$data[$key->username]=$key->visits;      

}                                                      



}
unset($data['username']) ;


foreach ($data as $key => $value) {
//     dd($key , $value);
array_push($GLOBAL['$PromoterData'],$key);
array_push($GLOBAL['$PromoterData'],$value);

}

}
public function calculnumofday($code,$FromDate,$EnDate)
{
$gps = Gps::select(DB::raw('count(*)  as visits'))
->join('users', 'users.id', '=', 'gps.user_id')
->whereBetween('NDate',array($FromDate,$EnDate))
->where('users.Code',$code)
->groupBy('users.id','gps.NDate')
->get();
dd($gps);
$GLOBAL['$PromoterData']=$gps->visits;
}
public function importkpipromoter()
{
  
if (Auth::check())
  {
try
{

$temp= Request::get('submit'); 


if(isset($temp))

{ 
 

$filename = Input::file('file')->getClientOriginalName();



$Dpath = base_path();


$upload_success =Input::file('file')->move( $Dpath, $filename);


Excel::load($upload_success, function($reader)
{   





$results = $reader->get()->all();

// dd($results);
foreach ($results as $data)
{


$Performance=0;
$Kpi_code=$data['codekpi'];
$Users=$data['code'];
$Target=$data['target'];
$Weight=$data['weight'];

 $From_Date= $data['fromdate'];
 $End_Date =$data['enddate'];
// $From_Date='2018-02-01';
// $End_Date ='2018-02-28';
$n=count($Kpi_code);
$M=count($Users);


$Kpi=Kpi::where('Code',$Kpi_code)
->select('*')->first();

$user=User::where('Code',$Users)->first();
$querykpi=DB::table('kpi_users')->where('user_id',$user->id)
->where('From_Date', $From_Date)
->where('To_Date',$End_Date)
->where('kpi_id',$Kpi->id)
->first();

if($querykpi!=[])

DB::table('kpi_users')->delete($querykpi->id);

$user->getUserKPI()->attach($Kpi->id, ['Performance'=>$Performance,'From_Date'=>$From_Date,'To_Date'=>$End_Date,'Weight'=>$Weight,'Target'=>$Target]);


$Users=User::where('Type_User','Outdoor Promoters')->select('Name','id')->lists('Name','id');





}

});


}

return redirect('/KPIUsers');

}
catch(Exception $e)
{
return redirect('/KPIUsers');
}
}
else
{
return redirect('/login');
}

}
}
