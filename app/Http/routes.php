<?php
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Methods: GET, POST, OPTIONS");   
 // header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
 header('Access-Control-Allow-Credentials: true');      

use App\User;
use App\District;
use App\Government;
use App\Contractor;
use App\Product;
use App\SubType;
use App\EndType;
use App\Item;
use App\Category;
use App\Brand;

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
//PromoterDaliyReport
Route::get('/ShowPromoterDaliyReport','ReportController@ShowPromoterDaliyReport');
Route::any('/PromoterDaliyReport','ReportController@PromoterDaliyReport');
//sms
Route::resource('/SMS','SMSController');
//pricemap
Route::get('/pricemap','PriceController@showpricemap');
Route::any('/showsmsreport','SMSController@showsmsreport');
//Questions
Route::resource('/Questions','QuestionController');
Route::any('/storequestion','QuestionController@storequestion');
Route::get('/showsurvey/{id}','QuestionController@showsuervy'); 
Route::get('/getsurveydata','QuestionController@getsurveydata');

Route::get('/showNewContractor','ContractorController@showNewContractor');
Route::any('/getNewcontractor','ContractorController@getNewcontractor');
    
Route::any('/Savesurveydata','QuestionController@Savesurveydata');
Route::any('/ShowNewinvReport','DistrubetedController@ShowNewinvReport');
Route::any('/ShowReportIventoryStockNew/{id}','DistrubetedController@ShowReportIventoryStockNew');

Route::any('/ExportPerformanceBack','ReviewsController@ExportPerformanceBack');

Route::any('/ExportDilyBackcheck','ReviewsController@ExportDilyBackcheck');

Route::any('/SearchMapItem/{id}', 'DistrubetedController@SearchMapItem');
Route::any('/SearchCairoMapItem/{id}', 'DistrubetedController@SearchCairoMapItem');


Route::get('/CairoInventoryMap', function () {
	 $Items=Item::lists('Name', 'id');
	   $catogry=Category::Lists('EngName','id');
      $Brand=Brand::Lists('Name','id');
    return view('InventoryMap.cairo',compact('Items','catogry','Brand'));
});



Route::get('/InventoryMap', function () {
	 $Items=Item::lists('Name', 'id');
	   $catogry=Category::Lists('EngName','id');
      $Brand=Brand::Lists('Name','id');
    return view('InventoryMap.index',compact('Items','catogry','Brand'));
});
Route::get('/MapItem/{id}', 'DistrubetedController@MapItem');
Route::get('/CairoMapItem/{id}', 'DistrubetedController@CairoMapItem');


//login servey
Route::any('/analysissurvey','SurveyController@analysissurvey');
Route::get('/showanalysissurvey','SurveyController@showanalysissurvey');
Route::get('/ShowSurveyReport','SurveyController@ShowSurveyReport');
Route::get('/servey/Question/{id}','SurveyController@ShowDropSurvey');
Route::resource('/createsurvey','SurveyController');
Route::get('/AssginServey/{id}','SurveyController@AssginServey');
 Route::any('/AddUsertoServey','SurveyController@AddUsertoServey');
Route::any('/ShowAllServey','SurveyController@ShowAllServey');
Route::get('/EditSurvey/{id}','SurveyController@EditSurvey');
Route::get('/DeleteSurvey/{id}','SurveyController@DeleteSurvey');
Route::get('/ShowDashboard','SurveyController@ShowDashboard');
Route::get('/ShowUser/{id}','SurveyController@surveyuser');
Route::get('/LoginSurvey/{id}','SurveyController@LoginSurvey');
Route::any('/CheckLogin','SurveyController@CheckLogin');
Route::get('/ShowAllQuestion/{id}','SurveyController@ShowAllQuestion');
Route::get('/ShowAllAnswers/{id}','SurveyController@ShowAllAnswers');
Route::get('/updateQuestion','SurveyController@updateQuestion');
Route::get('/deleteQuestion/{id}','SurveyController@deleteQuestion');
Route::get('/showquarter','SurveyController@showquarter');
Route::any('/AnaylsisQuadrantsCharts','SurveyController@AnaylsisQuadrantsCharts');

Route::any('allposts', 'ContractorController@allPosts' )->name('allposts');
//reportgps
Route::get('/reportgps','AdminGpsController@reportgps');
Route::any('/reportgpsdata','AdminGpsController@reportgpsdata');
//updateimage
//shaza Update
Route::get('/NewReport','MarketingChartController@NewReport');
Route::get('/TotalitemCost','OrderItemsController@TotalitemCost');
Route::get('/CostPerItem','OrderItemsController@CostPerItem');
Route::get('/getchartnewdata','MarketingChartController@getchartnewdata');
Route::get('/ReportStockMoney','OrderItemsController@ReportStockMoney');


Route::get('/BestContractorNew','MarketingChartController@BestContractorNew');
Route::get('/MarketingActivityReport','TestController@MarketingActivityReport');
Route::any('/ShowMarketingReport','TestController@ShowMarketingReport');
Route::get('/ActivityReport','TestController@ActivityReport');
Route::any('/ShowActivityReport','TestController@ShowActivityReport');
Route::get('/ContractorAmountReport','TestController@ContractorAmountReport');
Route::any('/getactivity/{id}/{start}/{end}','TestController@getactivity');
Route::any('/ShowContractorAmountReport','TestController@ShowContractorAmountReport');

Route::any('/updateimageg','UserController@updateimageg');
Route::get('/ActivityCost','OrderItemsController@ActivityCost');

Route::resource('/Questions','QuestionController');

Route::resource('/GetCurrentQuestion','QuestionController@GetCurrentQuestion');
Route::resource('/Questions','QuestionController');

//testserialnumber
Route::any('/testserialnumber','AdminController@TestSerialNumber');

//downloadfilescvpromoters
Route::post('/add','UserController@add');
Route::any('/get/{id}','UserController@get');

Route::get('/ShowFiles/{id}','UserController@showCV');

//Product data sheet doaa Elgheny
Route::post('/adddatasheet','ProductController@add');
Route::any('/getdatasheet/{id}','ProductController@get');

Route::get('/ShowFilesdatasheet/{id}','ProductController@showCV');
//End
Route::get('/ViewPDF/{id}','ProductController@ViewPDF');

Route::get('/ViewPDFUser/{id}','UserController@ViewPDF');
//importkpipromoter
Route::any('/importkpipromoter','KpiUserController@importkpipromoter');

Route::get('/', function () {
    return view('admin/login');
});
Route::any('/StockChart','OrderItemsController@StockChart');
Route::get('/ChartItem', 'OrderItemsController@ChartItem');
//////////////////////////Supplier/////////////////////////
Route::resource('/Supplier','SupplierController');
route::get('/updateSupplier','SupplierController@updateSupplier');
route::get('/DeleteSupplier/{id}','SupplierController@destroy');
Route::post('/importSupplier','SupplierController@importSupplier');
/////////////End Supplier//////////////////////////////////////

Route::any('/exportreportpro','ReportController@exportreportpro');
Route::any('/Competitors','ActivityController@Competitors');
Route::any('/importnewgps','SuperadminPanelController@importnewgps');
Route::any('/importorder','SuperadminPanelController@importorder');
Route::any('/importcafesign','SuperadminPanelController@importcafesign');
Route::any('/exportproduct','SuperadminPanelController@exportproduct');
Route::any('/exportsign','SuperadminPanelController@exportsign');
Route::any('/showadminpanel','SuperadminPanelController@showadminpanel');



Route::any('/ExportMultiProduct','SuperadminPanelController@ExportMultiProduct');
Route::any('/ExportCafe','SuperadminPanelController@ExportCafe');
Route::any('/exportlocalcompany','SuperadminPanelController@exportlocalcompany');

Route::any('/approvalitem','ItemController@approvalitem');
Route::any('/checkapproval/{id}','ItemController@checkapproval');
Route::any('/Noapprovalitem','ItemController@Noapprovalitems');
//Doaa Elgheny RetailersReview at 20/08/2017
Route::get('/RetailersReview','RetailerReviewController@RetailersReview');
Route::post('/ImportReviewRet','RetailerReviewController@ImportReviewRet');
Route::get('/ReviewRet/destroy/{id}','RetailerReviewController@destroy');
Route::resource('/ReviewRet','RetailerReviewController');

//Doaa Elgheny
Route::get('/ShowDashBack','ReviewsController@ShowDashBack');
Route::any('/CalculateBackCheck','ReviewsController@CalculateBackCheck');
Route::get('/showExportReview','ReviewsController@showExportReview');
Route::post('ExportReview','ReviewsController@ExportReview');

//End Elgheny



Route::any('/ReviewRet/edit/{id}','RetailerReviewController@showupdateretailers');
Route::any('/ReviewRet/ShowKids/{id}','RetailerReviewController@ShowKids');
Route::any('/CreateKidsRetailers','RetailerReviewController@CreateKidsRetailers');
Route::any('/CreateProductRetailers','RetailerReviewController@CreateProductRetailers');
Route::any('/ReviewRet/ShowProduct/{id}','RetailerReviewController@ShowProduct');
Route::any('/updateKidsRetailers','RetailerReviewController@updateKidsRetailers');
Route::any('/updateProductRetailers','RetailerReviewController@updateProductRetailers');
Route::get('/DeleteKids/{id}','RetailerReviewController@DeleteKidsRetailers');
Route::get('/DeletePro/{id}','RetailerReviewController@DeleteProductRetailers');
//End

 //Order Items Doaa Elgheny at 10/09/2017
Route::resource('/OrderItems','OrderItemsController');
Route::any('/OrderItem/Destroy/{id}','OrderItemsController@destroy');
Route::any('/OrderItem/Details/{id}','OrderItemsController@OrderDetails');
Route::any('/updateOrderItem','OrderItemsController@updateOrderItem');
Route::any('/OrderItemDetails/Destroy/{id}','OrderItemsController@DeleteOrderItem');
Route::any('/storeDetailsOrder','OrderItemsController@storeDetailsOrder');
Route::get('/Order/ItemsDropDown/{id}', 'OrderItemsController@OrderItemsDropDown');
Route::get('/Order/Quantity/{id}/{ActivityName}', 'OrderItemsController@OrderQuantity');
Route::any('/OriginalPlan/{id}','OrderItemsController@OriginalPlan');
Route::any('/ActualPlan/{id}','OrderItemsController@ActualPlan');
Route::get('/ConnectOrder/{id}','OrderItemsController@ConnectOrder');
Route::any('/UpdatePos','OrderItemsController@UpdatePos');
Route::get('/Brand/item/{id}', 'OrderItemsController@BrandItems');



//shaza Retailers
Route::resource('/retailers','RetailerController');
Route::any('/retailers/destroy/{id}','RetailerController@destroy');
Route::any('/updateretailers','RetailerController@updateretailers');
Route::any('/checkRetailerEmail','RetailerController@checkRetailerEmail');
Route::any('/checkLicenseNumber','RetailerController@checkLicenseNumber');
Route::get('/government/district/{id}', 'UserController@GovernmentDropDownData');
Route::any('/checkRetailerTele','RetailerController@checkRetailerTele');
Route::any('/checkRetailerName','RetailerController@checkRetailerName');
Route::any('/ReletailerSave','RetailerController@ShowSaveRetailers');
Route::any('/checCodeRetailers','RetailerController@checCodeRetailers');
Route::any('/Retailer/destroy/{id}','RetailerController@destroy');
route::get('/Retailer/edit/{id}','RetailerController@editRetailer');
route::any('/importretailers','RetailerController@importretailers');
 
//End Retailers

//shaza Type
Route::resource('/types','TypeController');
Route::any('/types/destroy/{id}','TypeController@destroy');
Route::any('/updateType','TypeController@update');
Route::any('/checktypename','TypeController@checktypename');
Route::any('/checktypeengname','TypeController@checktypeengname');

//End Type

//shaza SubType
Route::resource('/subtypes','SubTypeController');
Route::any('/subtypes/destroy/{id}','SubTypeController@destroy');
Route::any('/updateSubType','SubTypeController@update');
Route::any('/checksubtypename','SubTypeController@checksubtypename');
Route::any('/checksubtypeengname','SubTypeController@checksubtypeengname');

Route::any('/showsubtype/{id}','SubTypeController@showsubtype');
//End SubType

//shaza Location
Route::any('/locations/{id}','LocationController@index');
Route::any('/locations/destroy/{id}','LocationController@destroy');
Route::any('/updatelocation','LocationController@updatelocation');
Route::any('/storeLocation','LocationController@storeLocation');
//End Location

//shaza license
Route::any('/license/{id}','LicenseController@index');
Route::any('/license/destroy/{id}','LicenseController@destroy');
Route::any('/updatelicense','LicenseController@updatelicense');
Route::any('/storeLicense','LicenseController@storeLicense');
//End license

//shaza EndType
Route::resource('/endtypes','EndTypeController');
Route::any('/endtypes/destroy/{id}','EndTypeController@destroy');
Route::any('/updateEndType','EndTypeController@update');
Route::any('/checkendtypename','EndTypeController@checkendtypename');
Route::any('/checkendtypeengname','EndTypeController@checkendtypeengname');

Route::any('/showendtype/{id}','EndTypeController@showendtype');
//End EndType
Route::get('Type/typedropdown', function(){
   
    $SubType = SubType::where('Type_Id','=',Input::get('option'));
    return $SubType->lists('EngName', 'id');
});
Route::get('EndType/Endtypedropdown', function(){
   
    $EndType = EndType::where('SubType_Id','=',Input::get('option'));
    return $EndType->lists('EngName', 'id');
});

Route::get('Product/dropdownlist', function(){

     $id = Input::get('option');
    $Products = Product::where('Company_id',$id);

    return $Products->lists('Cement_Name', 'id');
});
Route::get('/importwinnerretailer','ActivityItemController@importwinnerretailer');
Route::resource('/activity','ActivityController');
Route::get('/testmy','TestController@doaa');
Route::get('/checkcontractorexist','ReviewsController@checkcontractorexist');
Route::get('/activity/destroy/{id}','ActivityController@destroy');
Route::get('/ACupdate','ActivityController@ACupdate');
Route::post('/importActivities','ActivityController@importActivities');
Route::get('/product/backimg/{id}','ProductController@showbackimg');
Route::any('/showproductphoto/{id}','ProductController@showproductphoto');
Route::get('/product/frontimg/{id}','ProductController@showfrontimg');
Route::get('/showimg/{id}','ProductController@showimg');


Route::post('/importContractors','ContractorController@importContractors');

Route::any('/importuser','UserController@importuser');	
Route::group(['middleware' => ['web']], function () {
Route::post('/importpricefile','PriceController@importpricefile');
Route::post('importgps','AdminController@importgps');
Route::any('/importproduct','ProductController@importproduct');	
Route::group(['prefix' => '/api/'], function()
	{

		Route::post('gps/Store', 'GpsController@GpsStore');
		Route::get('fupdate', 'GpsController@fupdate');
//get pormoter
		Route::get('gps/backcheck/{username?}/lang/{lang?}/lat/{lat?}','GpsController@backcheck');

		Route::get('gps/username/{username?}/password/{password?}','GpsController@doLogin');
		Route::get('gps/{date}','GpsController@get_location');

		Route::get('/gps/username/{username?}/password/{password?}/email/{email?}/role/{role?}/nameuser/{nameuser?}','GpsController@doRegister');
		// Route::get('gps/filter/name/{name}/date/{date}','GpsController@NameDateFilter');
		Route::get('list','GpsController@index');
		route::get('product','GpsController@getproduct');
        Route::post('/getallusers','ToDoListController@GetAllUser');
        Route::post('/getallprojects','ToDoListController@GetAllProject');
        Route::post('/createprojects','ToDoListController@createProject');
		Route::post('Todologin','ToDoListController@Todologin');
		Route::post('/deleteproject','ToDoListController@deleteproject');
		Route::post('/getalltasks','ToDoListController@GetAllTask');
		Route::post('/UpdateAdmin','ToDoListController@UpdateAdmin');
		Route::post('/UpdateUser','ToDoListController@UpdateUser');
		Route::post('/DeleteTask','ToDoListController@DeleteTask');
		Route::post('/EditTask','ToDoListController@EditTask');
		Route::post('createTask','ToDoListController@createTask');
		Route::post('UpdateToken','ToDoListController@UpdateToken');
        Route::post('/EditProject','ToDoListController@EditProject');
		Route::post('/showallUsers','ToDoListController@getUsersINProjectandOut');
		Route::post('/Myaccount','ToDoListController@ReturnPassword');
        Route::post('/GetAllUserinproject','ToDoListController@GetAllUserinproject');
     	Route::post('/UpdateMyProfile','ToDoListController@UpdateMyProfile');
     	Route::post('/GetUserReport','ToDoListController@GetUserReport');
     	Route::post('/ShowReport','ToDoListController@ShowReport');
	
	});
Route::get('/calenderMarketingActivity', function () {

 $Type='MarketingActivity';

     return view('calender/calender',compact('Type'));
   
     });
    // Route::get('/calenderEvents', function () {
    // 	 $Type='Events';
    //  return view('calender/calender',compact('Type'));
   
    //  });
 
	
//Eventos Calendario
// Route::get('cargaEventosEvents{id?}','CalendarController@Events');
Route::get('cargaEventosMarketingActivity','CalendarController@MarketingActivity');

Route::get('cargaEventosTasks{id?}','CalendarController@Tasks');

Route::post('guardaEventos', array('as' => 'guardaEventos','uses' => 'CalendarController@create'));

Route::post('actualizaEventos','CalendarController@update');
Route::post('eliminaEvento','CalendarController@delete');
	//Doaa Elgheny 3/11 Contractors 
Route::resource('/cafes','CafeController');
Route::get('/cafe/destroy/{id}','CafeController@destroy');
Route::get('/fupdatecafe','CafeController@fupdatecafe');
Route::get('/showsignphoto/{id}','CafeController@showsignphoto');
Route::post('importCafe','CafeController@importCafe');
Route::get('/checkCafeCode','CafeController@checkCafeCode');

Route::any('/ImportActivity','SuperadminPanelController@ImportActivity');
Route::any('/ImportnewPrice','SuperadminPanelController@ImportnewPrice');


	//Doaa Elgheny 3/11 Contractors 

Route::resource('/Contractors','ContractorController');
Route::any('/Contractors/destroy/{id}','ContractorController@destroy');
Route::any('/updateContractors','ContractorController@updateContractors');
Route::any('/checkContractorEmail','ContractorController@checkContractorEmail');
Route::any('/checkContractorTel1','ContractorController@checkContractorTel1');
Route::any('/checkContractorTel2','ContractorController@checkContractorTel2');
//Doaa Elgheny 30/10/2016 Reviews
Route::resource('/Reviews','ReviewsController');
Route::any('/Reviews/destroy/{id}','ReviewsController@destroy');
Route::any('/updateReview','ReviewsController@updateReview');
Route::any('/updatehaswood','ReviewsController@updatehaswood');
Route::any('/updateHas_Mixers','ReviewsController@updateHas_Mixers');
Route::any('/updateHasSubContractor','ReviewsController@updateHasSubContractor');
Route::any('/importReview','ReviewsController@importReview');
//End Elgheny
	
//Doaa Elgheny Companies

Route::resource('/Companies','CompaniesController');
Route::any('/Companies/destroy/{id}','CompaniesController@destroy');
Route::any('/updateCompanies','CompaniesController@updateCompanies');
Route::any('/checkCompanyCode','CompaniesController@checkCompanyCode');
Route::get('/checkCompanyEngName','CompaniesController@checkCompanyEngName');

Route::get('/updategps','AdminGpsController@updategps');

Route::get('/checkCompanyName','CompaniesController@checkCompanyName');
Route::get('/ShowCompanyDeteils/{compid}','CompaniesController@ShowCompanyDeteils');
	//Doaa Elgheny Price 
Route::resource('/price','PriceController');
Route::get('/Bestretailerwithdate','MarketingChartController@Bestretailerwithdate');







Route::get('/price/destroy/{id}','PriceController@destroy');
Route::get('/priceupdate','PriceController@priceupdate');
Route::post('/importprice','PriceController@importprice');
Route::get('/price/products/{id}','PriceController@getproducts');
Route::post('/chartpricesanalysis','PriceController@chartpricesanalysis');
Route::get('/showpricesanalysis','PriceController@showpricesanalysis');
Route::get('/ShChartPrice','PriceController@ShChartPrice');
//End Price 
Route::any('/ShowAllProducts','ProductController@shoecementproduct');
Route::any('/checkProductCode','ProductController@checkProductCode');
Route::get('/activitychart','MarketingChartController@index');
Route::get('/getchartdata','MarketingChartController@activitychartdata');
Route::get('/Contractorschart','MarketingChartController@contractorindex');
Route::get('/govermentcontractor','MarketingChartController@govermentcontractor');
Route::get('/upperlowercontractor','MarketingChartController@upperlowercontractor');
Route::get('/reviewedcontractor','MarketingChartController@reviewedcontractor');
Route::get('/analysisContractor','MarketingChartController@analysisContractor');
Route::get('/productcontractor','MarketingChartController@productcontractor');
Route::get('/showgallery/{id}','MarketingChartController@gallery');
Route::get('/BestContractor','MarketingChartController@BestContractor');
Route::get('/BestContractorwithdate','MarketingChartController@BestContractorwithdate');
Route::get('/showbestcont','MarketingChartController@showbestcont');
Route::any('/cafemap','CafeController@ShowCafeMap');
Route::any('/getcafemap','CafeController@getCafeMap');
Route::any('/allSignsMap','CafeController@allSignsMap');
//Doaa Elgheny KPI
Route::resource('/KPI','KpiController');
Route::get('/userkpi','KpiUserController@indexPkisalary');
Route::any('/getuserkpi','KpiUserController@getuserkpi');
Route::get('/Active/{id}','KpiController@Active');

Route::any('/KPI/destroy/{id}','KpiController@destroy');
Route::any('/updateKPI','KpiController@updateKPI');
Route::get('/checkEngNameKPI','KpiController@checkEngNameKPI');
Route::get('/checkNameKPI','KpiController@checknameKPI');
Route::get('/checkCodeKPI','KpiController@checkCodeKPI');
//Doaa Elgheny
Route::resource('/KPIUsers','KpiUserController');
Route::any('/KpiUserDelete','KpiUserController@destroy');
Route::any('/importKPI','KpiUserController@importKPI');
Route::any('/ShowReportKPI','KpiUserController@ShowReportKPI');
Route::any('/ShowDetailsKpi/{id}/{form}/{to}','KpiUserController@ShowDetailsKpi');

//End
//Doaa Elgheny Government
Route::resource('/Governments','GovernmentController');
Route::any('/Governments/destroy/{id}','GovernmentController@destroy');
Route::any('/updateGovernments','GovernmentController@updateGovernments');
Route::get('/showallDistriGovernment/{id}','DistrictController@showallDistriGovernment');
Route::get('/checknamegovernment','GovernmentController@checknamegovernment');
Route::get('/checkEngNamegovernment','GovernmentController@checkEngNamegovernment');
Route::get('/checkquantity','BillOutController@checkshaza');
//
//Doaa Elgheny districts
Route::resource('/Districts','DistrictController');
Route::any('/Districts/destroy/{id}','DistrictController@destroy');
Route::any('/updateDistrict','DistrictController@updateDistrict');

//shazza
//Doaa Elgheny energy_sources
Route::resource('/Energy_sources','Energy_sourcesController');
Route::any('/Energy_sources/destroy/{id}','Energy_sourcesController@destroy');
Route::any('/updateEnergysources','Energy_sourcesController@updateEnergysources');
Route::get('/checkEnergySource','Energy_sourcesController@checkEnergySource');
//
//Doaa Elgheny CompanyWarehouses
Route::resource('/CompanyWarehouses','CompanyWarehousesController');
Route::any('/CompanyWarehouses/destroy/{id}','CompanyWarehousesController@destroy');
Route::any('/updateCompanyWarehouses','CompanyWarehousesController@updateCompanyWarehouses');

Route::resource('/categories','CategoryController');
Route::get('/checkEnglishnamecategory','CategoryController@checkEnglishnamecategory');
Route::any('/updatecategory','CategoryController@updatecategoryname');
Route::any('/categories/destroy/{id}','CategoryController@destroy');
Route::get('/checknamecategory','CategoryController@checknamecategory');
Route::get('/checkEnglishnamecategory','CategoryController@checkEnglishnamecategory');

//Doaa Elgheny Saleroffice
Route::resource('/selleroffice','SellerOfficeController');
Route::get('/selleroffice/destroy/{id}','SellerOfficeController@destroy');
Route::get('/updateSellOffier','SellerOfficeController@updateSellOffier');
Route::get('/telephone','SellerOfficeController@telephone');
//
Route::any('/updateactive','CategoryController@updateactive');
Route::any('/brandcategory/{id}','BrandController@showallbrandcategory');
Route::get('/checkEngNamebrand','BrandController@checkEngNamebrand');
//shaza test
Route::resource('/tests','TestController');

Route::any('/getwinner/{startdate}/{enddate}', 'TestController@getwinners');
Route::any('/addprize/{id}/{activityitem_id}', 'TestController@show');
 	//shazzaproduct

Route::resource('/products','ProductController');


Route::any('checkProductname','ProductController@checkProductname');
Route::any('checkproductenglishname','ProductController@checkproductenglishname');
//
Route::get('/products/destroy/{id}','ProductController@destroy');
	
		Route::any('/products/UpdateProductall','ProductController@UpdateProductall');
		Route::any('/showproductdetails/{id}','DetailsController@showproductdetails');
		Route::any('/showprodoff/{id}','ProductController@showproductoffice');
		Route::get('api/dropdown', function(){
     $id = Input::get('option');
    $disticts = District::where('government_id','=',$id);
    return $disticts->lists('Name', 'id');
});
				Route::get('api/newdropdown', function(){
    $goverment=Government::where('Name','=',Input::get('option'))->first();
    $disticts = District::where('government_id','=',$goverment->id);
    return $disticts->lists('Name', 'Name');
});
	Route::get('api/Contractor_Id', function(){

    $disticts=District::where('id','=',Input::get('option'))->first();
    $Contractors = Contractor::where('District','=',$disticts->Name);
    return $Contractors->lists('Code', 'id');
});
		Route::get('/products/destroy/{id}','ProductController@destroy');

Route::any('/prodoffdelete/{sellerofficeid}/{productid}','ProductController@productofficedelete');
		Route::resource('/detail','DetailsController');
		Route::get('/salesview','DetailsController@salesview');
			Route::get('/Drfupdate','DetailsController@Drfupdate');
			Route::get('/detail/destroy/{id}','DetailsController@destroy');














//roles&&permission
				Route::resource('/roles','RoleController');
                Route::get('/role_update','RoleController@role_update');
                Route::get('/role_name','RoleController@role_name');
              
               	Route::get('/roles/destroy/{id}','RoleController@destroy');
                Route::post('/role_permission','RoleController@role_permission');
          	    Route::get('/show/{id}','RoleController@show');
          	    Route::get('/chose/{id}','RoleController@chose');			
//

//shazzasaleproduct_details
Route::resource('/sale_products','sale_productController');

Route::any('/checksaleProductCode','sale_productController@checksaleProductCode');
Route::any('/updatesaleproduct','sale_productController@updatesaleproduct');
Route::any('/sale_products/destroy/{id}','sale_productController@destroy');
Route::get('/checknamesaleproduct','sale_productController@checknamesaleproduct');
Route::get('/checkenglishnamesaleproduct','sale_productController@checkenglishnamesaleproduct');
Route::any('/saleproductdetails/{id}','sale_product_detailsController@saleproductdetails');

Route::resource('/sale_product_details','sale_product_detailsController');
Route::any('/updatesaleproductdetails','sale_product_detailsController@updateSaleproductdetails');
Route::any('/sale_product_details/destroy/{id}','sale_product_detailsController@destroy');
Route::any('/updateactivesaleproduct','sale_product_detailsController@updateactivesaleproduct');
Route::any('/Signcafe','CafeController@SignCafe');
Route::get('/Signcafe/img/{id}','CafeController@showSignCafeimg');
Route::resource('/orders','OrderController');
Route::any('/Contractors_Program','OrderController@indexContractors_Program');
Route::any('/updateactiveorder','OrderController@updateactiveorder');
Route::any('/orders/destroy/{id}','OrderController@destroy');
Route::any('importgpsreport','AdminController@importgpsreport');
Route::any('/updateorder','OrderController@updateorder');
//shazzagps
Route::any('/gps/destroy/{id}','AdminGpsController@destroy');
Route::resource('/gps','AdminGpsController');
//shazzMarktingactivity_subactivity
Route::resource('/marktingactitvities','MarktingactitvityController');
Route::any('/updatemarktingactitvity','MarktingactitvityController@updatemarktingactitvityname');
Route::any('/marktingactitvities/destroy/{id}','MarktingactitvityController@destroy');
Route::get('/checkmarketactivityname','MarktingactitvityController@checkmarketactivityname');
Route::any('/subacivity/{id}','SubactitvityController@showsubactivityofmarket');
Route::any('/checknamesubactivity','SubactitvityController@checkshaza');
Route::any('/checkenglishnamesubactivity','SubactitvityController@checkenglishnamesubactivity');
Route::any('/storephoto','MarktingactitvityController@storephoto');
Route::resource('/subactitvities','SubactitvityController');
Route::any('/subactitvities/destroy/{id}','SubactitvityController@destroy');
Route::any('updateSubactivity','SubactitvityController@updateSubactivity');
Route::any('/showallactivityitems/{id}','ActivityItemController@showallactivityitems');
Route::any('/importActivityItem','ActivityItemController@importActivityItem');
Route::any('/importwinnercontractor','ActivityItemController@importwinnercontractor');

Route::any('/showallcontractor/{code}','ActivityItemController@showallcontractor');
Route::any('/showallwinnercontractor/{code}','ActivityItemController@showallwinnercontractor');
Route::any('/showallwinnerretailer/{code}','ActivityItemController@showallwinnerretailer');

Route::any('/storeactivitycontractor','ActivityItemController@storeactivitycontractor');
Route::any('/delete/{id}','ActivityItemController@deleteactivitycontractor');
Route::any('/showallactivityitems/delete/{id}','SignController@deletesign');
 Route::any('/showallactivityitems/edit/{id}','SignController@showupdatesign');
  Route::any('/showallactivityitemsimage/{id}','SignController@showsignimg');
Route::any('/importsign','SignController@importsign');
Route::any('/checkCodeSign','SignController@checkCodeSign');
Route::any('/store','SignController@store');
Route::any('/Update','SignController@Update');
Route::resource('/activityitem','ActivityItemController');
Route::any('/creatitems','ActivityItemController@creatitems');
Route::any('/actattruibute','ActivityItemController@actattruibute');
Route::any('/activityitem/destroy/{code}','ActivityItemController@destroy');
Route::any('/UpdateItem/{code}','ActivityItemController@Updateitem');

               
	  Route::any('/activityitem/storeactivityitemattributes','ActivityItemController@storeactivityitemattributes');
Route::any('/activityitem/updateactivityitemattributes','ActivityItemController@updateactivityitemattributes');

//Doaa PettyCash
Route::any('/PettyCash','PettyCashController@ShowPetty');
Route::any('/storeData','PettyCashController@storeData');
Route::any('/ShowReportPetty','PettyCashController@ShowReportPetty');
Route::any('/showChartPetty','PettyCashController@showChartPetty');
Route::any('/ChartPettyCash','PettyCashController@ChartPettyCash');
Route::any('/ChartPettyCashUser','PettyCashController@ChartPettyCashUser');
Route::any('/Pettycashdelete/{id}','PettyCashController@Pettycashdelete');
Route::any('/Deletesafe/{id}','PettyCashController@Deletesafe');
Route::any('/CaculateFinal/{id}/{total}','PettyCashController@CaculateFinal');


Route::any('/PettycashdeleteIN/{id}','PettyCashController@PettycashdeleteIN');

Route::any('/Pettycashsidebar','PettyCashController@Pettycashsidebar');
Route::any('/Delivery','PettyCashController@Delivery');
Route::any('/storeDataIN','PettyCashController@storeDataIN');
Route::any('/storeDatsafetrans','PettyCashController@storeDatsafetrans');
Route::any('/safetransaction','PettyCashController@safetransaction');
Route::any('/totalPetty','PettyCashController@totalPetty');
//End 
//Doaa Elgheny Government
Route::resource('/Governments','GovernmentController');
Route::any('/Governments/destroy/{id}','GovernmentController@destroy');
Route::any('/updateGovernments','GovernmentController@updateGovernments');
Route::get('/showallDistriGovernment/{id}','DistrictController@showallDistriGovernment');
Route::get('/checknameDistric','DistrictController@checknameDistric');
Route::get('/checkEngNameDistric','DistrictController@checkEngNameDistric');

//
//Doaa Elgheny districts
Route::resource('/Districts','DistrictController');
Route::any('/Districts/destroy/{id}','DistrictController@destroy');
Route::any('/updateDistrict','DistrictController@updateDistrict');

Route::any('/updatePurches','PurchaseController@updatePurches');

Route::get('/showallDetails/{id}','PurchaseController@showallDetails');
Route::any('/checkpurscahsenumber','PurchaseController@checkpurscahsenumber');
Route::any('/storeDetailsIN','PurchaseController@storeDetailsIN');
Route::get('/EditPhotoPurchesIN/{id}','PurchaseController@EditPhotoPurchesIN');
Route::any('/UpdateImage','PurchaseController@UpdateImage');

//

//item Route Doaa
route::resource('/Items','ItemController'); 
Route::get('/Items/destroy/{id}','ItemController@destroy');
Route::get('/showallitems/{id}','ItemController@showallitems');
Route::any('/Items/store','ItemController@store');
route::get('/updateItems','ItemController@updateItems');
Route::any('/checkCode','ItemController@checkCode');
route::get('/checknameitem','ItemController@checknameitem');
Route::any('/checkEngNameitem','ItemController@checkEngNameitem');
Route::any('/NotificationStock','ItemController@NotificationStock');
Route::any('/MinLimitQuatityItems','ItemController@MinLimitQuatityItems');
Route::any('/MinLimtItems','ItemController@MinLimtItems');
Route::any('EditPhotoItem/{id}','ItemController@EditPhotoItem');
Route::any('UpdateImageItem','ItemController@UpdateImageItem');
//

// hadeel 
	    Route::resource('/warehouse','WarehouseController');
		Route::get('/warehouse/destroy/{id}','WarehouseController@destroy');
		Route::any('/updatewarehouse','WarehouseController@updatewarehouse');
		//Doaa Elgheny  purchase
		Route::resource('/purchase','PurchaseController');
		Route::get('/purchase/destroy/{purches_id}/{item_id}','PurchaseController@destroy');
		Route::any('/updatePurchesDetailsIn','PurchaseController@updatePurchesDetails');
		Route::any('/storeDetails','PurchaseController@storeDetails');
		//Doaa Elgheny  Bill Out
		Route::resource('/BillOut','BillOutController');
		Route::get('/checkquantityBillOut','BillOutController@checkshaza');
		Route::get('/BillOutDelete/destroy/{purches_id}/{item_id}','BillOutController@destroy');
		Route::get('/showallbillDetails/{id}','BillOutController@showallDetails');
		Route::any('/updatePurchesDetails','BillOutController@updatePurchesDetails');
		Route::any('/storeDetails','BillOutController@storeDetails');
		Route::any('/checknumberBillOut','BillOutController@checkpurscahsenumber');
		Route::get('/EditPhotoPurchesOut/{id}','BillOutController@EditPhotoPurchesOut');
        Route::any('/UpdateImageOut','BillOutController@UpdateImageOut');
//
Route::resource('/Distrbuted','DistrubetedController');
Route::any('/PurchaseIn','DistrubetedController@ShowPurchaseIn');
Route::any('/ShowReportPurchase','DistrubetedController@ShowReportPurchase');
Route::any('/InventoryStock','DistrubetedController@InventoryStock');
Route::any('/ShowReportIventoryStock','DistrubetedController@ShowReportIventoryStock');

Route::get('/Catogry/Brand/{id}', 'OrderItemsController@BrandCat');


Route::any('/ShowReport','DistrubetedController@ShowReport');
Route::any('/checkArabicnameWarehouse','WarehouseController@checkArabicnameWarehouse');
Route::any('/checkEnglisnameWarehouse','WarehouseController@checkEnglisnameWarehouse');
//Doaa Elgheny Chart 
route::resource('/ChartQuanyity','PromoterQuantity');
route::resource('/ChartUpper','UpperLowerGovernment');
route::resource('/ChartPrmotorcount','CountControllerPrmotor');

route::get('/showupperchart','UpperLowerGovernment@showupperchart');
route::get('/showulowerchart','UpperLowerGovernment@showulowerchart');

route::get('/showupperCountchart','CountControllerPrmotor@showupperCountchart');
route::get('/showLowerCountchart','CountControllerPrmotor@showLowerCountchart');

route::get('/showpromoterchart','PromoterQuantity@showpromoterchart');

//End 
				
				//End
route::resource('/Brands','BrandController');
route::get('/updateBrands','BrandController@updateBrands');
route::get('/updateactiveBrand','BrandController@updateactiveBrand');
Route::get('/Brands/destroy/{id}','BrandController@destroy');
Route::get('/checknamebrand','BrandController@checknamebrand');

//azhar routes
Route::get('reports','AdminController@report');
Route::post('reports','AdminController@reports');
Route::post('importfile','AdminController@importfile');
//new//

//product routes
Route::get('Prreports','ProductController@Prreports');
Route::post('Prreports','ProductController@reports');
//

Route::get('adminpanel','AdminController@adminpanel');
Route::get('chartadmin','AdminController@chartadmin');
Route::get('charttotal','AdminController@charttotal');


Route::get('chartadminpanel','AdminController@chartadminpanel');
Route::get('chartadminpanelUpper','AdminController@chartadminpanelUpper');
Route::get('/contchartt','AdminController@chartcont');
Route::get('/saleschart','AdminController@saleschart');
Route::get('/typechart','AdminController@typechart');

	Route::get('login', function() {
	  return View::make('admin/login');
	    });
	//POST route
	Route::post('/login', 'AdminController@login');
	Route::get('/logout','AdminController@logout');

	Route::get('/map','AdminController@DrawMap');

	Route::get('/map/show', function() {
	$users=user::where('role','user')->lists('Name','Name');
	if (Auth::check()){
		return view('admin/map',compact('users'));    
	}
	else{
		return redirect('/login');  
	}

	});

	//map filter
	Route::get('/gps/filter/name/{name}/date/{date}/to/{to}','AdminController@NameDateFilter');
	route::get('/latest','AdminController@latest');
    route::get('/updatelatest/{name?}','AdminController@updatelatest');
    Route::get('/Gpsupdate','AdminController@Gpsupdate');
	//all chart analysis
	// Route::get('api/chart/analysis','AdminController@generatecharts');
	
	//chart filter
	Route::get('/chart/api/chart/filter/{name}/{from}/{to}','GpsController@Filtercharts');
	Route::get('/chart/filter', function() {
		$users=user::where('role','user')->get();
		if (Auth::check()){
			return view('charts',compact('users')); 
		} 
		else{
			return redirect('/login');  
		}     
	});

		//hadeel routes
		Route::resource('/users','UserController');
		Route::get('/fupdate','UserController@fupdate');
		Route::get('/users/destroy/{id}','UserController@destroy');
		//shaza
		Route::get('/OfficeUser','UserController@OfficeUser');
        Route::get('/allactiveuser','UserController@AllActiveUser');
        route::get('/checkUsername','UserController@checkUsername');
		route::get('/checkEmailuser','UserController@checkEmailuser');
		route::get('/checkTelephoneUser','UserController@checkTelephoneUser');
		route::get('/checkCardNumberUser','UserController@checkCardNumberUser');
		route::get('/checkIDNumberUser','UserController@checkIDNumberUser');
   Route::get('/government/district/{id}', 'UserController@GovernmentDropDownData');
    Route::get('/users/government/district2/{id}', 'UserController@GovernmentDropDownData2');
//chechusername
		route::get('/checkUsername','UserController@checkUsername');
				route::get('/checkEmailuser','UserController@checkEmailuser');
								route::get('/checkTelephoneUser','UserController@checkTelephoneUser');
				route::get('/users/edit/{id}','UserController@edituser');


//roles
//

		Route::resource('/detail','DetailsController');
		Route::get('/salesview','DetailsController@salesview');
			Route::get('/Drfupdate','DetailsController@Drfupdate');
			Route::get('/contractorscode','GpsController@contractorscode');

		Route::resource('/task','TaskController');
		Route::get('/task/destroy/{id}','TaskController@destroy');

		Route::get('/addtask','TaskController@addtask');
		Route::get('/createtask','TaskController@createtask');
	  Route::any('/tasks/storetask','TaskController@storetask');

Route::any('/checkname','TaskController@Name');
Route::any('/tasks/admintask','TaskController@admintask');
Route::any('/tasks/usertask','TaskController@usertask');

Route::any('/updatetask','TaskController@updatetask');
Route::any('/updateadmintask','TaskController@updateadmintask');
Route::any('/updateusertask','TaskController@updateusertask');
Route::any('/officetask','TaskController@officetask');

Route::get('/charttask','TaskController@charttask');

Route::any('/promoterbackcheck','TaskController@promoterbackcheck');
Route::any('/achieved','TaskController@achieved');

Route::any('/videomakers','TaskController@videomakers');

Route::any('/loe','TaskController@loe');
//ShowReportPrice
Route::get('/ShowReportPrice', 'PriceController@ShowReportPrice');


//Show Price Per Month
Route::get('/ShowReportMPrice', 'PriceController@ShowReportMPrice');

//showActivitycharts
Route::get('/showactivitycharts', 'PriceController@showactivitycharts');
//Activitycharts
Route::get('/Activitycharts', 'PriceController@Activitycharts');
Route::get('/company/product/{id}', 'PriceController@CompanyDropDownData');
Route::get('/chartprices','PriceController@showchartprice');
//shownewcharts
Route::get('/newcharts', 'AdminController@newcharts');
Route::get('/VisitsGovUPE', 'AdminController@VisitsGovUPE');
Route::get('/VisitsGovLOE', 'AdminController@VisitsGovLOE');
//goverment

//Doaa Elgheny Bacup Inven

Route::any('/importItems','ExportInventoryController@importItems');




//import Data Inventory

Route::get('/AnalysisactivityReport','TestController@AnalysisactivityReport');
Route::any('/ShowAnalysisactivityReport','TestController@ShowAnalysisactivityReport');
Route::get('/PromoterActivityReport','TestController@PromoterActivityReport');
Route::any('/ShowPromoterActivityReport','TestController@ShowPromoterActivityReport');
Route::get('/ContractorAmountReport','TestController@ContractorAmountReport');
Route::any('/getactivity/{id}/{start}/{end}','TestController@getactivity');
Route::any('/getdistrict/{id}','TestController@getdistrictbygovernment');

Route::any('/ShowContractorAmountReport','TestController@ShowContractorAmountReport');

Route::get('/NotParticipatedReport','TestController@NotParticipatedReport');
Route::any('/ShowNotParticipatedReport','TestController@ShowNotParticipatedReport');
Route::get('/BestContractorReport','TestController@BestContractorReport');
Route::any('/ShowBestContractorReport','TestController@ShowBestContractorReport');

Route::get('/PriceperGovernmentReport','TestController@PriceperGovernmentReport');
Route::any('/ShowPriceperGovernmentReport','TestController@ShowPriceperGovernmentReport');




});




   Route::get('/calenderTasks', function () {
    	 $Type='Tasks';
    return view('calender/calender',compact('Type'));
     
});


Route::group(['middleware' => ['web']], function () {
    //
});
