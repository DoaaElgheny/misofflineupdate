<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kpi extends Model
{
    public $table="kpis";
// protected $connection = 'mysql';
   public function getUserKPI()
    {
        return $this->belongsToMany('App\User','kpi_users',
          'kpi_id','user_id')
        ->withPivot('From_Date')
          ->withPivot('To_Date')
        ->withPivot('Performance')
        ->withTimestamps();
    }
}
