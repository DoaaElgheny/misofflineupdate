<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CafeSigns extends Model
{
	// public $table ="gpss";



    public $table = "cafesigns";

    public function getcafe_users()
    {
        return $this->belongsTo('App\User','user_id')->withTrashed();
    }
   
}
